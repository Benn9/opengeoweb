/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */

describe('Viewpresets', () => {
  beforeEach(() => {
    cy.mockRadarGetCapabilities();
    cy.mockViewpresets();
    cy.mockWorkspaces();
    cy.mockExampleConfig();
    cy.mockCognitoLoginSuccess();

    // close the workspace dialog
    cy.findByTestId('workspaceMenuBackdrop').findByTestId('closeBtn').click();
  });

  afterEach(() => {
    cy.mockCognitoLogoutSuccess();
  });

  it('should select a preset and save a user preset', () => {
    cy.wait('@viewpresetlist');
    cy.findByTestId('open-viewpresets').click();
    cy.findAllByText('NL warnings').click();
    cy.wait('@viewpreset');
    cy.findAllByText('NL warnings').should('exist');
    cy.findAllByTestId('viewpreset-options-toolbutton').first().click();
    cy.findByText('Save').click();
    cy.wait('@saveViewpreset');
  });

  it('should delete a user preset and save a new preset as by pressing Enter', () => {
    cy.wait('@viewpresetlist');
    cy.findByTestId('open-viewpresets').click();
    cy.findAllByText('NL warnings').click();
    cy.wait('@viewpreset');
    cy.findAllByText('NL warnings').should('exist');
    cy.findAllByTestId('viewpreset-options-toolbutton').first().click();
    // press delete as option
    cy.findByText('Delete').click();
    // confirm delete in confirmation dialog
    cy.findByText('Delete').click();

    cy.wait('@deleteViewpreset');
    cy.wait('@viewpresetlist');
    cy.findAllByTestId('viewpreset-options-toolbutton').first().click();
    cy.findByText('Save as').click();

    cy.get('[name="title"]').type('Custom preset');
    cy.get('[name="title"]').should('have.value', 'Custom preset');
    cy.get('[name="title"]').type('{enter}');
    cy.wait('@saveViewpresetAs');
  });

  it('should select a preset from the viewpreset list dialog', () => {
    cy.findByTestId('open-viewpresets').click();
    cy.findAllByText('NL warnings').click();
    cy.wait('@viewpresetlist');
    cy.wait('@viewpreset');
    cy.findByTestId('layerManagerButton').click();
    cy.findAllByText('NL warnings').should('exist');
  });

  it('should duplicate a preset from the viewpreset list dialog', () => {
    cy.wait('@viewpresetlist');
    cy.findByTestId('open-viewpresets').click();
    // select open option menu
    cy.findAllByTestId('viewpreset-selectListRow')
      .eq(2)
      .findByTestId('viewpreset-listOptionsButton')
      .click();
    // select duplicate
    cy.findAllByText('Duplicate').click();
    // change name and save it
    cy.get('[name="title"]').type(' Custom preset');
    cy.findAllByText('Save').click();

    cy.wait('@saveViewpresetAs').then((interception) => {
      assert.isTrue(
        interception.request.body.title === 'NL warnings Custom preset',
      );
    });
  });

  it('should delete a preset from the viewpreset list dialog', () => {
    cy.wait('@viewpresetlist');
    cy.findByTestId('open-viewpresets').click();
    // select delete from option menu
    cy.findAllByTestId('viewpreset-selectListRow')
      .eq(2)
      .findByTestId('viewpreset-listDeleteButton')
      .click();

    // confirm delete in confirmation dialog
    cy.findAllByText('Delete').click();
    cy.wait('@deleteViewpreset').then((interception) => {
      assert.isTrue(interception.request.url.includes('NLwarnings'));
    });
  });

  it('should able to filter on filter chips and search', () => {
    cy.wait('@viewpresetlist');
    cy.findByTestId('open-viewpresets').click();
    // filter my presets
    cy.findAllByText('My presets').last().click();
    cy.findByTestId('viewpreset-selectList')
      .find('li')
      .should('have.length', 1);
    // filter system presets
    cy.findAllByText('My presets').last().click();
    cy.findAllByText('System presets').last().click();
    cy.findByTestId('viewpreset-selectList')
      .find('li')
      .should('have.length', 3);
    // filter search on title
    cy.findByTestId('viewpresetMenuBackdrop').find('input').type('rad');
    cy.findByTestId('viewpreset-selectList')
      .find('li')
      .should('have.length', 2);
    // filter search abstract
    cy.findByTestId('viewpresetMenuBackdrop').find('input').focus();
    cy.findByTestId('viewpresetMenuBackdrop').find('input').clear();
    cy.findByTestId('viewpresetMenuBackdrop').find('input').type('Advanced');
    cy.findByTestId('viewpreset-selectList')
      .find('li')
      .should('have.length', 1);
  });
});
