/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */
import React from 'react';
import { act, render, waitFor, screen, within } from '@testing-library/react';
import { ConfigType } from '@opengeoweb/shared';

import i18n from 'i18next';
import { I18nextProvider, initReactI18next } from 'react-i18next';
import App from './app';
import { createFakeApi } from './utils/api';

describe('App', () => {
  const storedFetch = global['fetch'];
  const storedWindowLocation = window.location;

  beforeEach(() => {
    // mock window.location
    // eslint-disable-next-line @typescript-eslint/ban-ts-comment
    // @ts-ignore
    delete window.location;
    // eslint-disable-next-line @typescript-eslint/ban-ts-comment
    // @ts-ignore
    window.location = {
      assign: (): void => {},
      origin: 'http://localhost',
      href: 'http://localhost/some-path',
    };
    jest.spyOn(console, 'warn').mockImplementation(() => {});
  });

  afterEach(() => {
    global['fetch'] = storedFetch;
    window.location = storedWindowLocation;
  });

  it('should render successfully', async () => {
    // mock the loadConfig fetch
    const mockConfig: ConfigType = {
      GW_INITIAL_PRESETS_FILENAME: 'test',
    };
    global['fetch'] = jest.fn().mockImplementation(() =>
      Promise.resolve({
        json: () => Promise.resolve(mockConfig),
      }),
    );

    const { baseElement } = render(<App createApi={createFakeApi} />);
    await act(async () => {
      expect(baseElement).toBeTruthy();
    });
    await waitFor(() => {
      expect(document.title).toEqual('GeoWeb');
    });
    expect(await screen.findByTestId('appLayout')).toBeTruthy();
  });

  it('should render successfully without a config file', async () => {
    // mock the loadConfig fetch
    global['fetch'] = jest.fn().mockImplementation(() =>
      Promise.resolve({
        json: () => Promise.resolve({}),
      }),
    );

    const { baseElement } = render(<App createApi={createFakeApi} />);
    await act(async () => {
      expect(baseElement).toBeTruthy();
    });
    await waitFor(() => {
      expect(document.title).toEqual('GeoWeb');
    });
    expect(await screen.findByTestId('appLayout')).toBeTruthy();
  });

  it('should display the configured page title', async () => {
    // mock the loadConfig fetch
    const mockConfig: ConfigType = {
      GW_INITIAL_PRESETS_FILENAME: 'test',
      GW_FEATURE_APP_TITLE: 'testTitle',
    };
    global['fetch'] = jest.fn().mockImplementation(() =>
      Promise.resolve({
        json: () => Promise.resolve(mockConfig),
      }),
    );

    const { baseElement } = render(<App createApi={createFakeApi} />);
    await act(async () => {
      expect(baseElement).toBeTruthy();
    });
    await waitFor(() => {
      expect(document.title).toEqual('testTitle');
    });
  });

  it('should redirect user to login screen', async () => {
    // mock the loadConfig fetch with a valid configuration
    const mockConfig: ConfigType = {
      GW_INITIAL_PRESETS_FILENAME: 'initialPresets.json',
      GW_AUTH_LOGIN_URL: 'login',
      GW_AUTH_LOGOUT_URL: 'logout',
      GW_AUTH_TOKEN_URL: 'token',
      GW_AUTH_CLIENT_ID: '123456',
      GW_INFRA_BASE_URL: '-',
      GW_APP_URL: 'http://fakeurl',
      GW_FEATURE_APP_TITLE: 'title',
      GW_FEATURE_FORCE_AUTHENTICATION: true,
    };
    global['fetch'] = jest.fn().mockImplementation(() =>
      Promise.resolve({
        json: () => Promise.resolve(mockConfig),
      }),
    );

    const pushStateSpy = jest.spyOn(window.history, 'pushState');
    render(<App />);
    await waitFor(() => {
      expect(pushStateSpy).toHaveBeenCalledWith(
        expect.objectContaining({
          idx: expect.any(Number),
          key: expect.any(String),
          usr: expect.objectContaining({ url: expect.any(String) }),
        }),
        '',
        '/login',
      );
    });
  });

  it('should show error when config is not valid', async () => {
    // mock the loadConfig fetch with a valid configuration
    const mockConfig: ConfigType = {
      GW_INITIAL_PRESETS_FILENAME: '',
      GW_AUTH_LOGIN_URL: '',
      GW_AUTH_LOGOUT_URL: '',
      GW_AUTH_TOKEN_URL: '',
      GW_AUTH_CLIENT_ID: '',
      GW_INFRA_BASE_URL: '-',
      GW_APP_URL: '',
      GW_FEATURE_APP_TITLE: 'title',
      GW_FEATURE_FORCE_AUTHENTICATION: true,
    };
    global['fetch'] = jest.fn().mockImplementation(() =>
      Promise.resolve({
        json: () => Promise.resolve(mockConfig),
      }),
    );
    render(<App />);
    await waitFor(() => {
      expect(
        screen.getByText(
          'Configuration (config.json) is not valid and has the following errors:',
        ),
      ).toBeTruthy();
    });
    expect(
      screen.getByText(
        'GW_INITIAL_PRESETS_FILENAME, GW_AUTH_LOGIN_URL, GW_AUTH_LOGOUT_URL, GW_AUTH_TOKEN_URL, GW_AUTH_CLIENT_ID, GW_APP_URL',
      ),
    ).toBeTruthy();
  });

  it('should show multiple types of errors when config is not valid', async () => {
    // mock the loadConfig fetch with a valid configuration
    const mockConfig: ConfigType = {
      GW_AUTH_LOGIN_URL: '',
      GW_AUTH_LOGOUT_URL: '',
      GW_I_DONT_EXIST: 'test',
      GW_I_DONT_EXIST2: 'test-2',
    } as unknown as ConfigType;
    global['fetch'] = jest.fn().mockImplementation(() =>
      Promise.resolve({
        json: () => Promise.resolve(mockConfig),
      }),
    );
    render(<App />);
    await waitFor(() => {
      expect(
        screen.getByText(
          'Configuration (config.json) is not valid and has the following errors:',
        ),
      ).toBeTruthy();
    });
    const nonExisting = screen.queryByText('NonExisting keys:');
    expect(nonExisting).toBeFalsy();

    const empty = screen.queryByText('Empty keys:');
    expect(empty).toBeTruthy();
    expect(
      within(screen.getByRole('alert')).getByText(
        'GW_AUTH_LOGIN_URL, GW_AUTH_LOGOUT_URL',
      ),
    ).toBeTruthy();
  });

  it('should set configured language', async () => {
    // mock the loadConfig fetch with a valid configuration
    const testLanguage = 'fi';
    const mockConfig: ConfigType = {
      GW_AUTH_LOGIN_URL: '',
      GW_AUTH_LOGOUT_URL: '',
      GW_LANGUAGE: testLanguage,
    } as unknown as ConfigType;
    global['fetch'] = jest.fn().mockImplementation(() =>
      Promise.resolve({
        json: () => Promise.resolve(mockConfig),
      }),
    );

    const defaultLanguage = 'en';
    const i18nInstance = i18n.createInstance();
    i18nInstance.use(initReactI18next).init({
      lng: defaultLanguage,
      interpolation: {
        escapeValue: false,
      },
      resources: {
        en: {},
        fi: {},
      },
    });

    expect(i18nInstance.language).toBe(defaultLanguage);
    render(
      <I18nextProvider i18n={i18nInstance}>
        <App />
      </I18nextProvider>,
    );
    await waitFor(() => expect(i18nInstance.language).toBe(testLanguage));
  });

  it('should use English as fallback language', async () => {
    // mock the loadConfig fetch with a valid configuration
    const testLanguage = 'xyz';
    const mockConfig: ConfigType = {
      GW_AUTH_LOGIN_URL: '',
      GW_AUTH_LOGOUT_URL: '',
      GW_LANGUAGE: testLanguage,
    } as unknown as ConfigType;
    global['fetch'] = jest.fn().mockImplementation(() =>
      Promise.resolve({
        json: () => Promise.resolve(mockConfig),
      }),
    );

    const defaultLanguage = 'en';
    const i18nInstance = i18n.createInstance();
    i18nInstance.use(initReactI18next).init({
      lng: defaultLanguage,
      fallbackLng: defaultLanguage,
      ns: 'translation',
      interpolation: {
        escapeValue: false,
      },
      resources: {
        en: {
          translation: {
            foo: 'bar',
          },
        },
      },
    });

    const mock = jest.spyOn(i18nInstance, 'changeLanguage');

    expect(i18nInstance.language).toBe(defaultLanguage);
    render(
      <I18nextProvider i18n={i18nInstance}>
        <App />
      </I18nextProvider>,
    );
    await waitFor(() => expect(mock).toHaveBeenCalledWith(testLanguage));
    expect(i18nInstance.resolvedLanguage).toBe(defaultLanguage);
    expect(i18nInstance.t('foo')).toBe('bar');
  });
});
