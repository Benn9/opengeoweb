/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */
import React from 'react';
import { fireEvent, render, waitFor, screen } from '@testing-library/react';
import { UserDocInfo } from './UserDocInfo';

describe('UserDocInfo', () => {
  const props = {
    documentTitle: 'Fake docs',
    onClick: jest.fn(),
  };

  it('should open the user doc page using the given URL', async () => {
    render(<UserDocInfo {...props} />);
    const link = screen.getByTestId('url-to-user-doc');
    expect(link).toBeTruthy();
    expect(link.innerHTML).toContain(`${props.documentTitle}`);

    fireEvent.click(link);
    await waitFor(() => {
      expect(screen.getByText(`${props.documentTitle}`)).toBeTruthy();
    });
    expect(props.onClick).toHaveBeenCalled();
  });
});
