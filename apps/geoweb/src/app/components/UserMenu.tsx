/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */
import React from 'react';
import { MenuItem, Grid } from '@mui/material';
import { useNavigate } from 'react-router-dom';
import {
  ConfigType,
  UserMenu as SharedUserMenu,
  useUserMenu,
  getInitials,
  UserMenuItemTheme,
  UserMenuItemCheatSheet,
} from '@opengeoweb/shared';
import { useApiContext, useApi } from '@opengeoweb/api';
import { UserDocInfo } from './UserDocInfo';
import { ProjectDocInfo } from './ProjectDocInfo';
import ReleaseNotes from './ReleaseNotes';
import { AppApi } from '../utils/api';

const styles = {
  username: {
    fontWeight: 500,
  },
  versionmenu: {
    textOverflow: 'ellipsis',
    overflow: 'hidden',
    whiteSpace: 'nowrap',
    display: 'block',
  },
};

interface UserProps {
  userName: string;
  userRole?: string;
  isAuthConfigured?: boolean;
  config?: ConfigType;
}

const UserMenu: React.FC<UserProps> = ({
  userName,
  userRole = '',
  isAuthConfigured = false,
  config,
}: UserProps) => {
  const { api } = useApiContext<AppApi>();
  const navigate = useNavigate();
  const { isOpen, onClose, onToggle } = useUserMenu();
  const { error, result } = useApi(api.getBeVersion, config);
  const [showReleaseNotes, setShowReleaseNotes] = React.useState(false);
  const [showFeReleaseNotes, setShowFeReleaseNotes] = React.useState(false);

  const beVersionLoadingMsg = error
    ? `Loading failed: ${error.message}`
    : 'Loading...';

  const feVersion =
    process.env.NX_GEOWEB_COMMIT || process.env.NX_GEOWEB_VERSION;

  const beReleasePageLink = `http://confluence.knmi.nl/display/GW/Release+notes`;
  const feReleasePageLink = process.env.NX_GEOWEB_COMMIT
    ? `https://gitlab.com/opengeoweb/opengeoweb/-/commits/${process.env.NX_GEOWEB_COMMIT}`
    : `https://gitlab.com/opengeoweb/opengeoweb/-/releases/v${process.env.NX_GEOWEB_VERSION}`;

  const handleLogout = (): void => {
    onClose();
    navigate('/logout');
  };

  const handleLogin = (): void => {
    onClose();
    navigate('/login');
  };

  const openReleaseNotes = (): void => {
    onClose();
    setShowReleaseNotes(true);
  };

  const openFeReleaseNotes = (): void => {
    onClose();
    setShowFeReleaseNotes(true);
  };

  const onOpenFeedback = (): void => {
    window.open('http://confluence.knmi.nl/display/GW/Feedback', '_blank');
  };

  const onOpenUserDoc = (): void => {
    window.open(config!.GW_FEATURE_MENU_USER_DOCUMENTATION_URL, '_blank');
  };

  const loginLogoutObject =
    userName === 'Guest' ? (
      <MenuItem onClick={handleLogin} data-testid="loginButton">
        Login
      </MenuItem>
    ) : (
      <MenuItem onClick={handleLogout} data-testid="logoutButton">
        Logout
      </MenuItem>
    );

  return (
    <>
      <SharedUserMenu
        isOpen={isOpen}
        onClose={onClose}
        onToggle={onToggle}
        initials={getInitials(userName)}
      >
        <MenuItem selected divider>
          <Grid container>
            {userRole && <Grid container>{userRole}</Grid>}
            <Grid item sx={styles.username}>
              {userName}
            </Grid>
          </Grid>
        </MenuItem>
        <UserMenuItemTheme />
        {config?.GW_FEATURE_MENU_VERSION && (
          <MenuItem
            data-testid="be-version-menu"
            onClick={openReleaseNotes}
            divider
            sx={styles.versionmenu}
          >
            {!result
              ? beVersionLoadingMsg
              : `v${result.version} has been released!`}
          </MenuItem>
        )}
        {config?.GW_FEATURE_MENU_FE_VERSION && feVersion && (
          <MenuItem
            data-testid="fe-version-menu"
            onClick={openFeReleaseNotes}
            divider
            sx={styles.versionmenu}
          >
            Version release notes
            <br />
            {`v${feVersion}`}
          </MenuItem>
        )}
        <UserMenuItemCheatSheet />
        {config?.GW_FEATURE_MENU_FEEDBACK && (
          <MenuItem
            data-testid="open-feedback"
            onClick={onOpenFeedback}
            divider
          >
            Feedback
          </MenuItem>
        )}
        {config?.GW_FEATURE_MENU_USER_DOCUMENTATION_URL && (
          <UserDocInfo
            documentTitle="User documentation"
            onClick={onOpenUserDoc}
          />
        )}
        {config?.GW_FEATURE_MENU_INFO && <ProjectDocInfo />}
        {isAuthConfigured ? loginLogoutObject : null}
      </SharedUserMenu>
      {config?.GW_FEATURE_MENU_VERSION && showReleaseNotes && (
        <ReleaseNotes
          version={!result ? beVersionLoadingMsg : result.version}
          handleClose={(): void => setShowReleaseNotes(false)}
          releasePage={beReleasePageLink}
        />
      )}
      {config?.GW_FEATURE_MENU_FE_VERSION && showFeReleaseNotes && (
        <ReleaseNotes
          version={feVersion!}
          handleClose={(): void => setShowFeReleaseNotes(false)}
          releasePage={feReleasePageLink}
        />
      )}
    </>
  );
};

export default UserMenu;
