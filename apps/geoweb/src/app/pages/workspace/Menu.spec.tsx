/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */
import React from 'react';
import {
  fireEvent,
  render,
  waitFor,
  screen,
  within,
} from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import configureStore from 'redux-mock-store';
import { Provider } from 'react-redux';
import { BrowserRouter } from 'react-router-dom';
import { actions as workspaceActions } from '@opengeoweb/workspace';
import { uiActions, uiReducer, uiTypes } from '@opengeoweb/store';
import { AppHeader, createMockStoreWithEggs } from '@opengeoweb/shared';
import { configureStore as configureReduxToolkitStore } from '@reduxjs/toolkit';
import Menu, {
  MenuProps,
  PUBLIC_WARNINGS,
  publicWarningDialogType,
} from './Menu';
import { TestWrapper } from '../../utils/mockdata';
import { AppStore } from '../../store';

describe('/components/Menu', () => {
  const props: MenuProps = {
    showSpaceWeather: jest.fn(),
    config: {
      GW_INITIAL_PRESETS_FILENAME: '',
      GW_FEATURE_APP_TITLE: 'TestTitle',
      GW_FEATURE_FORCE_AUTHENTICATION: false,
      GW_FEATURE_MODULE_SPACE_WEATHER: true,
    },
  };

  const user = userEvent.setup();

  it('should toggle the preset menu', async () => {
    const mockStore = configureStore();
    const store = mockStore();
    render(
      <BrowserRouter>
        <Provider store={store}>
          <TestWrapper>
            <AppHeader menu={<Menu {...props} />} />
          </TestWrapper>
        </Provider>
      </BrowserRouter>,
    );

    expect(screen.queryByTestId('drawer')).toBeFalsy();

    fireEvent.click(screen.getByTestId('menuButton'));

    await waitFor(() => {
      expect(screen.getByTestId('drawer')).toBeTruthy();
    });

    expect(screen.queryByText('Aviation')).toBeFalsy();
  });

  it('should open spaceweather when menu item clicked', async () => {
    const store = createMockStoreWithEggs({});
    render(
      <Provider store={store}>
        <TestWrapper>
          <BrowserRouter>
            <AppHeader menu={<Menu {...props} />} />
          </BrowserRouter>
        </TestWrapper>
      </Provider>,
    );

    fireEvent.click(screen.getByTestId('menuButton'));
    expect(await screen.findByText('Space Weather Forecast')).toBeTruthy();

    fireEvent.click(screen.getByText('Space Weather Forecast'));
    expect(props.showSpaceWeather).toHaveBeenCalled();
  });

  it('should support keyboard navigation', async () => {
    const store = createMockStoreWithEggs({});
    render(
      <Provider store={store}>
        <TestWrapper>
          <BrowserRouter>
            <AppHeader menu={<Menu {...props} />} />
          </BrowserRouter>
        </TestWrapper>
      </Provider>,
    );

    fireEvent.click(screen.getByTestId('menuButton'));

    await user.tab();
    const testTabIndex = (tabIndex = 0, expectedText = ''): void => {
      const element = within(screen.getByTestId('drawer')).getAllByRole(
        'button',
      )[tabIndex];
      expect(element.matches(':focus')).toBeTruthy();
      expect(within(element).getByText(expectedText));
    };

    testTabIndex(0, 'Auto Arrange Windows');
    await user.tab();
    testTabIndex(1, 'Sync Groups');
    await user.tab();
    testTabIndex(2, 'Space Weather Forecast');
    await user.tab({ shift: true });
    testTabIndex(1, 'Sync Groups');
    await user.tab({ shift: true });
    testTabIndex(0, 'Auto Arrange Windows');
  });

  it('should open the Sync groups', async () => {
    const mockStore = configureStore();
    const store = mockStore();
    render(
      <BrowserRouter>
        <Provider store={store}>
          <TestWrapper>
            <AppHeader menu={<Menu {...props} />} />
          </TestWrapper>
        </Provider>
      </BrowserRouter>,
    );

    const expectedAction = uiActions.setToggleOpenDialog({
      type: uiTypes.DialogTypes.SyncGroups,
      setOpen: true,
    });

    fireEvent.click(screen.getByTestId('menuButton'));

    fireEvent.click(screen.getByText('Sync Groups'));

    await waitFor(() => {
      expect(store.getActions()).toContainEqual(expectedAction);
    });
  });

  it('should auto arrange windows', async () => {
    const screenConfig = {
      id: 'preset1',
      title: 'Preset 1',
      views: {
        allIds: ['screen1', 'screen2', 'screen3', 'screen4'],
        byId: {
          screen1: {
            title: 'screen 1',
            componentType: 'MyTestComponent',
            initialProps: { mapPreset: [{}], syncGroupsIds: [] },
          },
          screen2: {
            title: 'screen 2',
            componentType: 'MyTestComponent',
            initialProps: { mapPreset: [{}], syncGroupsIds: [] },
          },
          screen3: {
            title: 'screen 3',
            componentType: 'MyTestComponent',
            initialProps: { mapPreset: [{}], syncGroupsIds: [] },
          },
          screen4: {
            title: 'screen 4',
            componentType: 'MyTestComponent',
            initialProps: { mapPreset: [{}], syncGroupsIds: [] },
          },
        },
      },
      mosaicNode: {
        direction: 'row' as const,
        second: {
          direction: 'column' as const,
          first: {
            direction: 'row' as const,
            second: 'screen1',
            first: 'screen2',
          },
          second: 'screen3',
        },
        first: 'screen4',
      },
    };

    const mockState: AppStore = {
      workspace: screenConfig,
    };

    const mockStore = configureStore();
    const store = mockStore(mockState);
    render(
      <BrowserRouter>
        <Provider store={store}>
          <TestWrapper>
            <AppHeader menu={<Menu {...props} />} />
          </TestWrapper>
        </Provider>
      </BrowserRouter>,
    );

    fireEvent.click(screen.getByTestId('menuButton'));

    fireEvent.click(screen.getByText('Auto Arrange Windows'));

    const expectedAction = workspaceActions.updateWorkspaceViews({
      mosaicNode: {
        direction: 'row',
        first: {
          direction: 'column',
          first: 'screen4',
          second: 'screen2',
        },
        second: {
          direction: 'column',
          first: 'screen1',
          second: 'screen3',
        },
      },
    });
    await waitFor(() => {
      expect(store.getActions()).toContainEqual(expectedAction);
    });
  });

  it('should show the aviation products when GW_PRESET_BACKEND_URL has set and fire correct actions', async () => {
    const mockStore = configureStore();
    const store = mockStore();
    const testProps = {
      ...props,
      config: {
        ...props.config,
        GW_PRESET_BACKEND_URL: 'test',
      },
    };
    render(
      <BrowserRouter>
        <Provider store={store}>
          <TestWrapper>
            <Menu {...testProps} />
          </TestWrapper>
        </Provider>
      </BrowserRouter>,
    );

    expect(screen.getByText('Aviation')).toBeTruthy();
    expect(store.getActions()).toEqual([]);

    fireEvent.click(screen.getByText('TAF'));
    expect(store.getActions()).toContainEqual(
      workspaceActions.fetchWorkspace({ workspaceId: 'screenConfigTaf' }),
    );

    fireEvent.click(screen.getByText('SIGMET'));
    expect(store.getActions()).toContainEqual(
      workspaceActions.fetchWorkspace({ workspaceId: 'screenConfigSigmet' }),
    );

    fireEvent.click(screen.getByText('AIRMET'));
    expect(store.getActions()).toContainEqual(
      workspaceActions.fetchWorkspace({ workspaceId: 'screenConfigAirmet' }),
    );
  });

  it('should show the public warnings when GW_DRAWINGS_BASE_URL has set and fire correct actions', async () => {
    const store = configureReduxToolkitStore({
      reducer: { ui: uiReducer },
    });
    const testProps = {
      ...props,
      config: {
        ...props.config,
        GW_DRAWINGS_BASE_URL: 'test',
      },
    };

    store.dispatch(
      uiActions.registerDialog({
        type: publicWarningDialogType,
        setOpen: false,
        source: 'app',
      }),
    );

    render(
      <BrowserRouter>
        <Provider store={store}>
          <TestWrapper>
            <Menu {...testProps} />
          </TestWrapper>
        </Provider>
      </BrowserRouter>,
    );

    expect(screen.getByText(PUBLIC_WARNINGS)).toBeTruthy();
    expect(
      store.getState().ui.dialogs[publicWarningDialogType]?.isOpen,
    ).toBeFalsy();

    fireEvent.click(screen.getByText(PUBLIC_WARNINGS));
    expect(
      store.getState().ui.dialogs[publicWarningDialogType]?.isOpen,
    ).toBeTruthy();
  });
});
