/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */
import React from 'react';
import { Provider } from 'react-redux';
import { BrowserRouter } from 'react-router-dom';
import configureStore from 'redux-mock-store';
import { fireEvent, screen, render, waitFor } from '@testing-library/react';

import { routerActions } from '@opengeoweb/store';
import { ConfigType, createMockStoreWithEggs } from '@opengeoweb/shared';
import {
  emptyMapWorkspace,
  actions as workspaceActions,
  workspaceRoutes,
} from '@opengeoweb/workspace';
import { TestWrapper } from '../../utils/mockdata';
import Header from './Header';

describe('components/Header', () => {
  it('should have the configured header text', async () => {
    const titleText = 'Test title';
    const mockStore = configureStore();
    const store = mockStore();
    render(
      <BrowserRouter>
        <Provider store={store}>
          <TestWrapper>
            <Header
              showSpaceWeather={jest.fn()}
              config={
                {
                  GW_FEATURE_APP_TITLE: titleText,
                } as unknown as ConfigType
              }
            />
          </TestWrapper>
        </Provider>
      </BrowserRouter>,
    );

    expect(await screen.findByText(titleText)).toBeTruthy();
  });

  it('should have the the default header', async () => {
    const mockStore = configureStore();
    const store = mockStore();
    render(
      <BrowserRouter>
        <Provider store={store}>
          <TestWrapper>
            <Header
              showSpaceWeather={jest.fn()}
              config={{} as unknown as ConfigType}
            />
          </TestWrapper>
        </Provider>
      </BrowserRouter>,
    );
    expect(await screen.findByText('GeoWeb')).toBeTruthy();
  });

  it('should switch to default workspace when logo clicked', async () => {
    const store = createMockStoreWithEggs({});
    render(
      <BrowserRouter>
        <Provider store={store}>
          <TestWrapper>
            <Header
              showSpaceWeather={jest.fn()}
              config={{} as unknown as ConfigType}
            />
          </TestWrapper>
        </Provider>
      </BrowserRouter>,
    );

    fireEvent.click(screen.getByText('GeoWeb'));
    const navigateAction = routerActions.navigateToUrl({
      url: workspaceRoutes.root,
    });
    const presetAction = workspaceActions.changePreset({
      workspacePreset: emptyMapWorkspace,
    });
    await waitFor(() => {
      expect(store.getActions()).toContainEqual(navigateAction);
    });
    expect(store.getActions()).toContainEqual(presetAction);
  });
});
