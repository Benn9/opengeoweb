/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */
import 'core-js/stable';
import 'regenerator-runtime/runtime';
import 'jest-canvas-mock';
import { TextEncoder } from 'util';
import '@testing-library/jest-dom';
import * as puppeteer from 'puppeteer-core';

// Fix to make SVGs work in tests, added to support frappe-gantt lib
Object.defineProperty(window.SVGElement.prototype, 'getBBox', {
  writable: true,
  value: () => ({
    x: 0,
    y: 0,
    width: 0,
    height: 0,
  }),
});

Object.defineProperty(window, 'crypto', {
  value: {
    getRandomValues: jest.fn(),
    subtle: {
      digest: jest.fn(),
    },
  },
});

global.TextEncoder = TextEncoder;

// mock puppeteer with puppeteer-core since storyshots tries to import former
jest.doMock(
  'puppeteer',
  () => {
    return puppeteer;
  },
  { virtual: true },
);

// make react-resize-detector work for tests
beforeEach(() => {
  global.ResizeObserver = jest.fn().mockImplementation(() => ({
    observe: jest.fn(),
    unobserve: jest.fn(),
    disconnect: jest.fn(),
  }));
});

let consoleHasErrorOrWarning = false;
const { error, warn } = console;

const ignoredMessages = [
  "Can't get DOM width or height", // ECharts print warning in tests when rendering canvas.
  'MSW_COOKIE_STORE', // msw prints warning in tests when mocked request sets a cookie
  'Could not parse CSS stylesheet', // jsdom has poor css support by design, will print an error for container queries,
  'Error: Could not parse CSS stylesheet',
];

global.console.warn = (msg: string | Error): void => {
  if (typeof msg === 'object') {
    const errorMessage = msg.message;
    const ignore = ignoredMessages.some((str) => errorMessage.includes(str));
    if (!ignore) {
      consoleHasErrorOrWarning = true;
      warn(msg);
    }
  } else {
    const ignore = ignoredMessages.some((str) => msg.includes(str));
    if (!ignore) {
      consoleHasErrorOrWarning = true;
      warn(msg);
    }
  }
};

global.console.error = (msg: string | Error): void => {
  if (typeof msg === 'object') {
    const errorMessage = msg.message;
    const ignore = ignoredMessages.some((str) => errorMessage.includes(str));
    if (!ignore) {
      consoleHasErrorOrWarning = true;
      error(msg);
    }
  } else {
    const ignore = ignoredMessages.some((str) => msg.includes(str));
    if (!ignore) {
      consoleHasErrorOrWarning = true;
      error(msg);
    }
  }
};

afterEach(() => {
  if (consoleHasErrorOrWarning) {
    consoleHasErrorOrWarning = false;
    throw new Error(
      'To keep the unit test output readable console errors and warnings are not allowed. Usually these are legitimate errors that should be fixed. If a console.error() or console.warn() is expected or caused by an underlying library, it can be mocked and asserted.',
    );
  }
  // make react-resize-detector work for tests
  global.ResizeObserver = ResizeObserver;
});
