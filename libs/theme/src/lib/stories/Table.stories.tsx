/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */
import * as React from 'react';
import {
  Typography,
  Table as MuiTable,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
} from '@mui/material';

import { StoryWrapper } from './StoryWrapper';

export default {
  title: 'demo/Table',
};

export interface ExampleRow {
  name: string;
  calories: number;
  fat: number;
  carbs: number;
  protein: number;
}

const items: ExampleRow[] = [
  { name: 'Frozen yoghurt', calories: 159, fat: 6.0, carbs: 24, protein: 4.0 },
  {
    name: 'Ice cream sandwich',
    calories: 237,
    fat: 9.0,
    carbs: 37,
    protein: 4.3,
  },
  { name: 'Eclair', calories: 262, fat: 16.0, carbs: 24, protein: 6.0 },
  { name: 'Cupcake', calories: 305, fat: 3.7, carbs: 67, protein: 4.3 },
  { name: 'Gingerbread', calories: 356, fat: 16.0, carbs: 49, protein: 3.9 },
];

// TODO: add more cell content variants like Select and (icon) Buttons https://gitlab.com/opengeoweb/opengeoweb/-/issues/1050
const CustomTableRow: React.FC<ExampleRow> = ({
  name,
  calories,
  fat,
  carbs,
  protein,
}: ExampleRow) => {
  return (
    <TableRow>
      <TableCell component="th" scope="row">
        {name}
      </TableCell>
      <TableCell>{calories}</TableCell>
      <TableCell>{fat}</TableCell>
      <TableCell>{carbs}</TableCell>
      <TableCell>{protein}</TableCell>
    </TableRow>
  );
};

const TableDemo: React.FC = () => {
  return (
    <TableContainer style={{ padding: '0 10px' }}>
      <MuiTable>
        <TableHead>
          <TableRow>
            <TableCell>Dessert (100g serving)</TableCell>
            <TableCell>Calories</TableCell>
            <TableCell>Fat&nbsp;(g)</TableCell>
            <TableCell>Carbs&nbsp;(g)</TableCell>
            <TableCell>Protein&nbsp;(g)</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {items.map((row) => (
            <CustomTableRow key={row.name} {...row} />
          ))}
        </TableBody>
      </MuiTable>
    </TableContainer>
  );
};

export const TableLight = (): React.ReactElement => (
  <StoryWrapper>
    <Typography
      variant="h2"
      style={{
        fontSize: 14,
        color: 'rgba(255, 0, 255, 0.98)',
        fontWeight: 500,
      }}
    >
      New & accessible table (14 pts)
    </Typography>
    <TableDemo />
  </StoryWrapper>
);
TableLight.storyName = 'Table light theme (takeSnapshot)';

TableLight.parameters = {
  zeplinLink: [
    {
      name: 'Light theme',
      link: 'https://app.zeplin.io/project/5ecf84a3c6ae1047a368f393/screen/5ecf927271b4154a3d0a25be',
    },
  ],
  layout: 'fullscreen',
};

export const TableDark = (): React.ReactElement => (
  <StoryWrapper isDarkTheme>
    <Typography
      variant="h2"
      style={{
        fontSize: 14,
        color: 'rgba(255, 0, 255, 0.98)',
        fontWeight: 500,
      }}
    >
      New & accessible table (14 pts)
    </Typography>
    <TableDemo />
  </StoryWrapper>
);
TableDark.storyName = 'Table dark theme (takeSnapshot)';

TableDark.parameters = {
  zeplinLink: [
    {
      name: 'Dark theme',
      link: 'https://app.zeplin.io/project/5ecf84a3c6ae1047a368f393/screen/6093e69005029c358090bd4e',
    },
  ],
  layout: 'fullscreen',
};
