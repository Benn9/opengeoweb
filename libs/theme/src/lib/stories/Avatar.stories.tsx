/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */
import * as React from 'react';
import { Avatar as MuiAvatar, Card, Grid } from '@mui/material';
import { StoryWrapper } from './StoryWrapper';

export default {
  title: 'demo/Avatar',
};

const AvatarDemo: React.FC = () => {
  return (
    <Grid container sx={{ width: 160 }}>
      <Card
        elevation={0}
        sx={{
          padding: 2,
          display: 'flex',
          div: {
            marginRight: 1,
          },
        }}
      >
        <Grid container>
          <MuiAvatar />
          <MuiAvatar>G</MuiAvatar>
          <MuiAvatar>GW</MuiAvatar>
        </Grid>
      </Card>
    </Grid>
  );
};

export const AvatarLight = (): React.ReactElement => (
  <StoryWrapper>
    <AvatarDemo />
  </StoryWrapper>
);

AvatarLight.storyName = 'Avatar light theme (takeSnapshot)';
AvatarLight.parameters = {
  zeplinLink: [
    {
      name: 'Light theme',
      link: 'https://app.zeplin.io/project/5ecf84a3c6ae1047a368f393/screen/60ec68c87ed4860e1d14f5dd',
    },
  ],
};

export const AvatarDark = (): React.ReactElement => (
  <StoryWrapper isDarkTheme>
    <AvatarDemo />
  </StoryWrapper>
);

AvatarDark.storyName = 'Avatar dark theme (takeSnapshot)';
AvatarDark.parameters = {
  zeplinLink: [
    {
      name: 'Dark theme',
      link: 'https://app.zeplin.io/project/5ecf84a3c6ae1047a368f393/screen/60ec68cb9e0ee610eaf967ec',
    },
  ],
};
