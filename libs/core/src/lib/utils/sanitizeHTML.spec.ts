/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */

import { sanitizeHTML } from './sanitizeHTML';

const validInfoHTML = `
<b>Coordinates</b> - (lon=7.26; lat=53.09)<br>
<hr><b>Air Temperature 1 Min Average (ID_10M/ta)</b><br>
<table><tbody><tr><td>&nbsp;</td><td>-</td><td>Air Temperature 1 Min Average</td><td><b>5.100000</b></td><td> degrees Celsius</td></tr>
<tr><td>&nbsp;</td><td>-</td><td>Station id</td><td><b>06286</b></td></tr>
</tbody></table>
`;

const xssAttemptUsingScript =
  '<script>alert("XSS attempt is here!");</script>OK';

const xssInfoHTMLTable = `<table onafterscriptexecute=alert(1)><script>1</script>`;

describe('src/lib/utils/sanitizeHTML', () => {
  describe('sanitize', () => {
    it('should filter out invalid XSS attempts', () => {
      expect(sanitizeHTML(xssAttemptUsingScript).__html).toBe('OK');
    });
    it('should filter out invalid XSS attempts out of table elements', () => {
      expect(sanitizeHTML(xssInfoHTMLTable).__html).toBe('<table></table>');
    });
    it('should leave valid HTML results intact', () => {
      expect(sanitizeHTML(validInfoHTML).__html).toBe(validInfoHTML);
    });
  });
});
