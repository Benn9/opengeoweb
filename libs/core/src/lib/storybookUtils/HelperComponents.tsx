/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import {
  ButtonGroup,
  Button,
  Card,
  CardContent,
  Typography,
  CardActions,
} from '@mui/material';
import { layerTypes } from '@opengeoweb/store';

type ExampleLayer = {
  title: string;
  layers: layerTypes.Layer[];
};

interface ActionCardProps {
  onClickBtn?: (action: ExampleLayer) => void;
  name: string;
  description: string;
  exampleLayers?: ExampleLayer[];
  children?: React.ReactChild;
}

export const ActionCard: React.FC<ActionCardProps> = ({
  onClickBtn,
  name,
  description,
  exampleLayers,
  children,
}: ActionCardProps) => {
  return (
    <Card
      sx={{
        fontFamily: 'Roboto, Helvetica, Arial',
        fontSize: 12,
        borderRadius: '5px',
        borderWidth: '2px',
        marginBottom: '20px',
        maxWidth: '440px',
      }}
    >
      <CardContent sx={{ paddingBottom: 0 }}>
        <Typography sx={{ fontSize: '1rem' }} variant="h5" component="h2">
          {name}
        </Typography>
        <Typography variant="body2" component="p">
          {description}
        </Typography>
      </CardContent>
      <CardActions>
        {children || (
          <ButtonGroup color="primary" variant="contained">
            {exampleLayers!.map((action) => (
              <Button
                key={action.title}
                onClick={(): void => onClickBtn!(action)}
              >
                {action.title}
              </Button>
            ))}
          </ButtonGroup>
        )}
      </CardActions>
    </Card>
  );
};
