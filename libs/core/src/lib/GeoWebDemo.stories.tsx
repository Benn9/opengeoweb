/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { connect } from 'react-redux';
import { ButtonGroup, Button } from '@mui/material';
import { ErrorBoundary } from '@opengeoweb/shared';
import {
  CoreAppStore,
  defaultLayers,
  layerActions,
  mapSelectors,
} from '@opengeoweb/store';
import { publicLayers, MapControls } from '@opengeoweb/webmap-react';
import { TimeSliderConnect } from '@opengeoweb/timeslider';
import { LayerSelectConnect } from '@opengeoweb/layer-select';
import { MapViewConnect } from './components/MapView';
import { store } from './storybookUtils/store';
import { useDefaultMapSettings } from './storybookUtils/defaultStorySettings';

import { LegendConnect, LegendMapButtonConnect } from './components/Legend';
import {
  LayerManagerConnect,
  LayerManagerMapButtonConnect,
} from './components/LayerManager';

import { CoreThemeStoreProvider } from './components/Providers/Providers';
import {
  MultiDimensionSelectMapButtonsConnect,
  MultiMapDimensionSelectConnect,
} from '.';

export default { title: 'application/demo' };

interface SimpleGeoWebPresetsProps {
  setLayers?: typeof layerActions.setLayers;
  mapId: string;
}

const enhance = connect(
  (state: CoreAppStore, props: SimpleGeoWebPresetsProps) => ({
    layers: mapSelectors.getMapLayers(state, props.mapId),
  }),
  {
    setLayers: layerActions.setLayers,
  },
);

const SimpleGeoWebPresets: React.FC<SimpleGeoWebPresetsProps> = ({
  setLayers,
  mapId,
}: SimpleGeoWebPresetsProps) => {
  useDefaultMapSettings({
    mapId,
    layers: [{ ...publicLayers.radarLayer, id: `radar-${mapId}` }],
    baseLayers: [
      { ...defaultLayers.baseLayerGrey, id: `baseGrey-${mapId}` },
      defaultLayers.overLayer,
    ],
  });

  const presetHarmonie = {
    layers: [publicLayers.harmonieAirTemperature],
  };
  const presetRadar = {
    layers: [{ ...publicLayers.radarLayer, id: `radar-${mapId}-2` }],
  };
  const presetHarmoniePrecipAndObs = {
    layers: [
      publicLayers.harmoniePrecipitation,
      { ...publicLayers.radarLayer, id: `radar-${mapId}-3` },
      publicLayers.harmoniePressure,
    ],
  };

  const presetRadarMSGCPP = {
    layers: [
      { ...publicLayers.radarLayer, id: `radar-${mapId}-4` },
      publicLayers.msgCppLayer,
    ],
  };

  const presetWind = {
    layers: [
      publicLayers.metNorwayWind1,
      publicLayers.metNorwayWind2,
      publicLayers.metNorwayWind3,
    ],
  };

  const presetHarmoniePL = {
    layers: [
      publicLayers.harmonieWindPl,
      publicLayers.harmonieRelativeHumidityPl,
    ],
  };

  return (
    <ButtonGroup
      variant="contained"
      color="primary"
      aria-label="outlined primary button group"
    >
      <Button
        onClick={(): void => {
          setLayers!({
            layers: presetHarmonie.layers,
            mapId,
          });
        }}
      >
        {' '}
        Harmonie
      </Button>
      <Button
        onClick={(): void => {
          setLayers!({
            layers: presetRadar.layers,
            mapId,
          });
        }}
      >
        Radar
      </Button>
      <Button
        onClick={(): void => {
          setLayers!({
            layers: presetHarmoniePrecipAndObs.layers,
            mapId,
          });
        }}
      >
        Precip + Obs
      </Button>
      <Button
        onClick={(): void => {
          setLayers!({
            layers: presetRadarMSGCPP.layers,
            mapId,
          });
        }}
      >
        Radar + MSGCPP
      </Button>
      <Button
        onClick={(): void => {
          setLayers!({
            layers: presetWind.layers,
            mapId,
          });
        }}
      >
        Wind
      </Button>
      <Button
        onClick={(): void => {
          setLayers!({
            layers: presetHarmoniePL.layers,
            mapId,
          });
        }}
      >
        HarmoniePL
      </Button>
    </ButtonGroup>
  );
};
const ConnectedSimpleGeoWebPresets = enhance(SimpleGeoWebPresets);

export const GeoWebDemo = (): React.ReactElement => {
  const mapId = 'mapid_1';

  return (
    <CoreThemeStoreProvider store={store}>
      <ErrorBoundary>
        <div style={{ height: '100vh' }}>
          <MapControls>
            <LayerManagerMapButtonConnect mapId={mapId} />
            <LegendMapButtonConnect mapId={mapId} />
            <MultiDimensionSelectMapButtonsConnect mapId={mapId} />
          </MapControls>
          <LayerSelectConnect />
          <LayerManagerConnect />
          <LayerManagerConnect mapId={mapId} isDocked />
          <LegendConnect mapId={mapId} />
          <MultiMapDimensionSelectConnect />
          <div
            style={{
              position: 'absolute',
              left: '0px',
              bottom: '0px',
              zIndex: 50,
              width: '100%',
            }}
          >
            <TimeSliderConnect mapId={mapId} sourceId="timeslider-1" />
          </div>
          <MapViewConnect mapId={mapId} showLayerInfo={false} />
        </div>
        <div
          style={{
            position: 'absolute',
            left: '50px',
            top: '10px',
            zIndex: 90,
          }}
        >
          <ConnectedSimpleGeoWebPresets mapId={mapId} />
        </div>
      </ErrorBoundary>
    </CoreThemeStoreProvider>
  );
};
