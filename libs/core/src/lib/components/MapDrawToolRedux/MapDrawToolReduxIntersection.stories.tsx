/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */

import React from 'react';
import { Grid, Divider } from '@mui/material';

import { useDispatch, useSelector } from 'react-redux';
import {
  CoreAppStore,
  defaultLayers,
  drawtoolActions,
  drawtoolSelectors,
  layerActions,
  layerSelectors,
} from '@opengeoweb/store';

import { CustomIconButton } from '@opengeoweb/shared';
import {
  DrawMode,
  EditModeButtonField,
  GeoJSONTextField,
  defaultModes,
  publicLayers,
  MapDrawToolOptions,
  StoryLayoutGrid,
  exampleIntersectionOptions,
  IntersectionSelect,
  getIntersectionToolIcon,
  exampleIntersections,
  exampleIntersectionWithShapeOptions,
  emptyGeoJSON,
} from '@opengeoweb/webmap-react';

import { CoreThemeStoreProvider } from '../Providers/Providers';

import { MapViewConnect } from '../MapView';
import { useDefaultMapSettings } from '../../storybookUtils/defaultStorySettings';
import { store } from '../../storybookUtils/store';

export default {
  title: 'components/MapDrawToolRedux/intersection',
};

const mapId = 'test-map-id';
const drawToolId = `drawtool-1`;

const BasicMapDrawToolStory: React.FC<{
  drawOptions?: MapDrawToolOptions;
}> = ({
  drawOptions = {
    defaultDrawModes: defaultModes,
  },
}) => {
  const dispatch = useDispatch();

  useDefaultMapSettings({
    mapId,
    layers: [{ ...publicLayers.radarLayer, id: `radar-${mapId}` }],
    baseLayers: [
      { ...defaultLayers.baseLayerGrey, id: `baseGrey-${mapId}` },
      defaultLayers.overLayer,
    ],
  });
  React.useEffect(() => {
    dispatch(
      drawtoolActions.registerDrawTool({
        mapId,
        drawToolId,
        geoJSONLayerId: 'draw-layer',
        geoJSONIntersectionLayerId: 'intersection-layer',
        geoJSONIntersectionBoundsLayerId: 'static-layer',
        ...drawOptions,
      }),
    );
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const drawTool = useSelector((store: CoreAppStore) =>
    drawtoolSelectors.selectDrawToolById(store, drawToolId),
  );

  const activeDrawMode = useSelector((store: CoreAppStore) =>
    drawtoolSelectors.getDrawModeById(
      store,
      drawToolId,
      drawTool?.activeDrawModeId!,
    ),
  ) as DrawMode;

  const isLayerInEditMode = useSelector((store: CoreAppStore) =>
    layerSelectors.getIsLayerInEditMode(store, drawTool?.geoJSONLayerId),
  );

  const layerFeatureGeoJSON = useSelector((store: CoreAppStore) =>
    layerSelectors.getFeatureLayerGeoJSON(store, drawTool?.geoJSONLayerId),
  );

  const layerFeatureIntersectionGeoJSON = useSelector((store: CoreAppStore) =>
    layerSelectors.getFeatureLayerGeoJSON(
      store,
      drawTool?.geoJSONIntersectionLayerId,
    ),
  );

  const activeToolId = drawTool?.activeDrawModeId;

  const onChangeDrawMode = (tool: DrawMode): void => {
    dispatch(
      drawtoolActions.changeDrawToolMode({
        drawToolId,
        drawModeId: tool.drawModeId,
      }),
    );
  };

  const onChangeEditMode = (isInEditMode: boolean): void => {
    dispatch(
      layerActions.toggleFeatureMode({
        layerId: drawTool?.geoJSONLayerId!,
        isInEditMode,
      }),
    );
  };

  const onChangeGeoJSON = (
    geoJSON: GeoJSON.FeatureCollection,
    layerId = drawTool?.geoJSONLayerId!,
  ): void => {
    dispatch(
      layerActions.layerChangeGeojson({
        layerId,
        geojson: geoJSON,
      }),
    );
  };

  const onChangeIntersection = (
    geoJSONIntersection: GeoJSON.FeatureCollection,
  ): void => {
    onChangeGeoJSON(
      geoJSONIntersection,
      drawTool?.geoJSONIntersectionBoundsLayerId,
    );
    onChangeGeoJSON(emptyGeoJSON, drawTool?.geoJSONIntersectionLayerId);
    onChangeGeoJSON(emptyGeoJSON, drawTool?.geoJSONLayerId);
  };

  return (
    <StoryLayoutGrid mapComponent={<MapViewConnect mapId={mapId} />}>
      <Grid item xs={12}>
        {drawTool?.drawModes.map((mode) => (
          <CustomIconButton
            key={mode.drawModeId}
            variant="tool"
            tooltipTitle={mode.title}
            onClick={(): void => onChangeDrawMode(mode)}
            isSelected={activeToolId === mode.drawModeId && mode.isSelectable}
            sx={{ marginRight: 1, marginBottom: 1 }}
          >
            {getIntersectionToolIcon(mode.drawModeId)}
          </CustomIconButton>
        ))}

        <Divider />
      </Grid>

      <IntersectionSelect
        intersections={exampleIntersections}
        onChangeIntersection={onChangeIntersection}
      />

      <EditModeButtonField
        isInEditMode={isLayerInEditMode}
        onToggleEditMode={onChangeEditMode}
        drawMode={activeDrawMode?.value}
      />

      <GeoJSONTextField
        title="drawshape geoJSON result"
        onChangeGeoJSON={onChangeGeoJSON}
        geoJSON={layerFeatureGeoJSON!}
      />

      <GeoJSONTextField
        title="intersection geoJSON result"
        geoJSON={layerFeatureIntersectionGeoJSON!}
      />
    </StoryLayoutGrid>
  );
};

export const IntersectionDemo = (): React.ReactElement => (
  <CoreThemeStoreProvider store={store}>
    <BasicMapDrawToolStory drawOptions={exampleIntersectionOptions} />
  </CoreThemeStoreProvider>
);

export const IntersectionWithShapeDemo = (): React.ReactElement => (
  <CoreThemeStoreProvider store={store}>
    <BasicMapDrawToolStory drawOptions={exampleIntersectionWithShapeOptions} />
  </CoreThemeStoreProvider>
);
