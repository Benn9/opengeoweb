/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */

import React from 'react';
import { Grid, Divider } from '@mui/material';

import { useDispatch, useSelector } from 'react-redux';
import {
  CoreAppStore,
  defaultLayers,
  drawtoolActions,
  drawtoolSelectors,
  layerActions,
  layerSelectors,
} from '@opengeoweb/store';

import { CustomIconButton } from '@opengeoweb/shared';
import {
  DrawMode,
  EditModeButtonField,
  GeoJSONTextField,
  defaultModes,
  publicLayers,
  MapDrawToolOptions,
  basicExampleDrawOptions,
  basicExampleMultipleShapeDrawOptions,
  getGeoJSONPropertyValue,
  FeatureLayers,
  StoryLayoutGrid,
  SelectField,
  fillOptions,
  getToolIcon,
  opacityOptions,
  strokeWidthOptions,
} from '@opengeoweb/webmap-react';
import { clamp } from 'lodash';
import { CoreThemeStoreProvider } from '../Providers/Providers';

import { MapViewConnect } from '../MapView';
import { useDefaultMapSettings } from '../../storybookUtils/defaultStorySettings';
import { store } from '../../storybookUtils/store';

export default {
  title: 'components/MapDrawToolRedux',
};

const mapId = 'test-map-id';
const drawToolId = `drawtool-1`;

const BasicMapDrawToolStory: React.FC<{
  drawOptions?: MapDrawToolOptions;
}> = ({
  drawOptions = {
    defaultDrawModes: defaultModes,
  },
}) => {
  const dispatch = useDispatch();

  useDefaultMapSettings({
    mapId,
    layers: [{ ...publicLayers.radarLayer, id: `radar-${mapId}` }],
    baseLayers: [
      { ...defaultLayers.baseLayerGrey, id: `baseGrey-${mapId}` },
      defaultLayers.overLayer,
    ],
  });
  React.useEffect(() => {
    dispatch(
      drawtoolActions.registerDrawTool({
        mapId,
        drawToolId,
        defaultDrawModes: drawOptions.defaultDrawModes || defaultModes,
        geoJSONLayerId: 'draw-layer',
        ...drawOptions,
      }),
    );
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const drawTool = useSelector((store: CoreAppStore) =>
    drawtoolSelectors.selectDrawToolById(store, drawToolId),
  );

  const activeDrawMode = useSelector((store: CoreAppStore) =>
    drawtoolSelectors.getDrawModeById(
      store,
      drawToolId,
      drawTool?.activeDrawModeId!,
    ),
  ) as DrawMode;

  const selectedFeatureIndex = useSelector((store: CoreAppStore) =>
    layerSelectors.getSelectedFeatureIndex(store, drawTool?.geoJSONLayerId),
  );

  const isLayerInEditMode = useSelector((store: CoreAppStore) =>
    layerSelectors.getIsLayerInEditMode(store, drawTool?.geoJSONLayerId),
  );

  const layerFeatureGeoJSON = useSelector((store: CoreAppStore) =>
    layerSelectors.getFeatureLayerGeoJSON(store, drawTool?.geoJSONLayerId),
  );

  const featureLayerGeoJSONProperties = useSelector((store: CoreAppStore) =>
    layerSelectors.getFeatureLayerGeoJSONProperties(
      store,
      drawTool?.geoJSONLayerId,
    ),
  );

  const activeToolId = drawTool?.activeDrawModeId;

  const onChangeDrawMode = (tool: DrawMode): void => {
    dispatch(
      drawtoolActions.changeDrawToolMode({
        drawToolId,
        drawModeId: tool.drawModeId,
      }),
    );
  };

  const onChangeEditMode = (isInEditMode: boolean): void => {
    dispatch(
      layerActions.toggleFeatureMode({
        layerId: drawTool?.geoJSONLayerId!,
        isInEditMode,
      }),
    );
  };

  const onChangeGeoJSON = (geoJSON: GeoJSON.FeatureCollection): void => {
    dispatch(
      layerActions.layerChangeGeojson({
        layerId: drawTool?.geoJSONLayerId!,
        geojson: geoJSON,
      }),
    );
  };

  const onChangeGeoJSONProperties = (
    properties: GeoJSON.GeoJsonProperties,
  ): void => {
    dispatch(
      layerActions.updateFeatureProperties({
        layerId: drawTool?.geoJSONLayerId!,
        properties,
        selectedFeatureIndex,
      }),
    );
  };

  const onChangeFeatureLayer = (newIndex: number): void => {
    dispatch(
      layerActions.setSelectedFeature({
        layerId: drawTool?.geoJSONLayerId!,
        selectedFeatureIndex: newIndex,
      }),
    );

    const selectionType =
      layerFeatureGeoJSON?.features[newIndex].properties?.selectionType;

    const tool = drawOptions.defaultDrawModes?.find(
      (mode) =>
        mode.shape.type === 'Feature' &&
        mode.shape.properties!.selectionType === selectionType,
    );

    if (selectionType && tool?.drawModeId) {
      dispatch(
        drawtoolActions.changeDrawToolMode({
          drawToolId,
          drawModeId: tool.drawModeId,
          shouldUpdateShape: false,
        }),
      );
    }
  };

  const defaultPolygonShape = drawTool?.drawModes.find(
    (mode) => mode.value === 'POLYGON',
  );
  // geoJSON properties
  const opacity = getGeoJSONPropertyValue(
    'fill-opacity',
    featureLayerGeoJSONProperties,
    defaultPolygonShape,
  ) as number;

  const fill = getGeoJSONPropertyValue(
    'fill',
    featureLayerGeoJSONProperties,
    defaultPolygonShape,
  ) as number;

  const strokeOpacity = getGeoJSONPropertyValue(
    'stroke-opacity',
    featureLayerGeoJSONProperties,
    defaultPolygonShape,
  ) as number;

  const strokeColor = getGeoJSONPropertyValue(
    'stroke',
    featureLayerGeoJSONProperties,
    defaultPolygonShape,
  ) as number;

  const strokeWidth = getGeoJSONPropertyValue(
    'stroke-width',
    featureLayerGeoJSONProperties,
    defaultPolygonShape,
  ) as number;

  // change geoJSON properties
  const onChangeOpacity = (value: string): void => {
    const newOpacity = parseInt(value, 10) / 100;
    const additionalStrokeOpacity = 0.8;
    onChangeGeoJSONProperties({
      'fill-opacity': newOpacity,
      'stroke-opacity': clamp(newOpacity + additionalStrokeOpacity, 0.01, 1),
    });
  };

  const onChangeFill = (value: string): void => {
    onChangeGeoJSONProperties({
      fill: value,
    });
  };

  const onChangeStrokeOpacity = (value: string): void => {
    const newOpacity = parseInt(value, 10) / 100;
    onChangeGeoJSONProperties({
      'stroke-opacity': newOpacity,
    });
  };

  const onChangeStroke = (value: string): void => {
    onChangeGeoJSONProperties({
      stroke: value,
    });
  };

  const onChangeStrokeWidth = (value: string): void => {
    onChangeGeoJSONProperties({
      'stroke-width': value,
    });
  };

  return (
    <StoryLayoutGrid mapComponent={<MapViewConnect mapId={mapId} />}>
      <Grid item xs={12}>
        {drawTool?.drawModes.map((mode) => (
          <CustomIconButton
            key={mode.drawModeId}
            variant="tool"
            tooltipTitle={mode.title}
            onClick={(): void => onChangeDrawMode(mode)}
            isSelected={activeToolId === mode.drawModeId}
            sx={{ marginRight: 1, marginBottom: 1 }}
          >
            {getToolIcon(mode.drawModeId)}
          </CustomIconButton>
        ))}

        <Divider />
      </Grid>
      <Grid item xs={12}>
        <SelectField
          label="Fill opacity"
          value={opacity * 100}
          onChangeValue={onChangeOpacity}
          options={opacityOptions}
          labelSuffix=" %"
        />

        <SelectField
          label="Fill color"
          value={fill}
          onChangeValue={onChangeFill}
          options={fillOptions}
          showValueAsColor
        />

        <SelectField
          label="Stroke opacity"
          value={strokeOpacity * 100}
          onChangeValue={onChangeStrokeOpacity}
          options={opacityOptions.filter((option) => option !== 0)}
          width="33.3%"
          labelSuffix=" %"
        />

        <SelectField
          label="Stroke color"
          value={strokeColor}
          onChangeValue={onChangeStroke}
          options={fillOptions}
          width="33.3%"
          showValueAsColor
        />

        <SelectField
          label="Stroke width"
          value={strokeWidth}
          onChangeValue={onChangeStrokeWidth}
          options={strokeWidthOptions}
          width="33.3%"
        />
      </Grid>

      <EditModeButtonField
        isInEditMode={isLayerInEditMode}
        onToggleEditMode={onChangeEditMode}
        drawMode={activeDrawMode?.value}
      />

      <FeatureLayers
        geojson={layerFeatureGeoJSON!}
        onChangeLayerIndex={onChangeFeatureLayer}
        activeFeatureLayerIndex={selectedFeatureIndex || 0}
      />

      <GeoJSONTextField
        onChangeGeoJSON={onChangeGeoJSON}
        geoJSON={layerFeatureGeoJSON!}
      />
    </StoryLayoutGrid>
  );
};

export const BasicMapDrawTool = (): React.ReactElement => (
  <CoreThemeStoreProvider store={store}>
    <BasicMapDrawToolStory />
  </CoreThemeStoreProvider>
);

export const BasicMapDrawToolShape = (): React.ReactElement => (
  <CoreThemeStoreProvider store={store}>
    <BasicMapDrawToolStory drawOptions={basicExampleDrawOptions} />
  </CoreThemeStoreProvider>
);

export const BasicMapDrawToolMultipleShape = (): React.ReactElement => (
  <CoreThemeStoreProvider store={store}>
    <BasicMapDrawToolStory drawOptions={basicExampleMultipleShapeDrawOptions} />
  </CoreThemeStoreProvider>
);
