/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { render, fireEvent, screen } from '@testing-library/react';
import { createMockStoreWithEggs } from '@opengeoweb/shared';
import { mapEnums, uiActions, uiTypes } from '@opengeoweb/store';
import GetFeatureInfoButtonConnect from './GetFeatureInfoButtonConnect';
import { CoreThemeStoreProvider } from '../Providers/Providers';

describe('src/components/FeatureInfo/GetFeatureInfoButtonConnect', () => {
  it('should dispatch action with passed in mapid when clicked, action should set isOpen to true if currently closed', () => {
    const mockState = {
      ui: {
        dialogs: {
          getfeatureinfo: {
            type: 'getfeatureinfo' as uiTypes.DialogType,
            activeMapId: 'map1',
            isOpen: false,
            source: 'app',
          },
        },
      },
    };
    const store = createMockStoreWithEggs(mockState);

    const props = {
      mapId: 'mapId_123',
    };
    render(
      <CoreThemeStoreProvider store={store}>
        <GetFeatureInfoButtonConnect {...props} />
      </CoreThemeStoreProvider>,
    );

    // button should be present
    expect(screen.getByTestId('open-getfeatureinfo')).toBeTruthy();

    // open the getfeatureinfo dialog
    fireEvent.click(screen.getByTestId('open-getfeatureinfo'));
    const expectedAction = [
      uiActions.setActiveMapIdForDialog({
        type: 'getfeatureinfo-mapId_123',
        mapId: props.mapId,
        setOpen: true,
        source: 'app',
        origin: mapEnums.MapActionOrigin.map,
      }),
    ];

    expect(store.getActions()).toEqual(expectedAction);
  });
  it('should dispatch action with passed in mapid when clicked, action should update source and mapId when already open', () => {
    const mockState = {
      ui: {
        dialogs: {
          getfeatureinfo: {
            type: 'getfeatureinfo' as uiTypes.DialogType,
            activeMapId: 'map1',
            isOpen: true,
            source: 'app',
          },
        },
      },
    };
    const store = createMockStoreWithEggs(mockState);

    const props = {
      mapId: 'mapId_123',
      source: 'module' as uiTypes.Source,
    };
    render(
      <CoreThemeStoreProvider store={store}>
        <GetFeatureInfoButtonConnect {...props} />
      </CoreThemeStoreProvider>,
    );

    // button should be present
    expect(screen.getByTestId('open-getfeatureinfo')).toBeTruthy();

    // open the getfeatureinfo dialog for another map
    fireEvent.click(screen.getByTestId('open-getfeatureinfo'));
    const expectedAction = [
      uiActions.setActiveMapIdForDialog({
        type: 'getfeatureinfo-mapId_123',
        mapId: props.mapId,
        setOpen: true,
        source: props.source,
        origin: mapEnums.MapActionOrigin.map,
      }),
    ];

    expect(store.getActions()).toEqual(expectedAction);
  });
  it('should dispatch action with passed in mapid when clicked, action should close the dialog when currently open for this map', () => {
    const mockState = {
      ui: {
        dialogs: {
          'getfeatureinfo-map3': {
            type: 'getfeatureinfo-map3' as uiTypes.DialogType,
            activeMapId: 'map3',
            isOpen: true,
            source: 'app',
          },
        },
      },
    };
    const store = createMockStoreWithEggs(mockState);

    const props = {
      mapId: 'map3',
    };
    render(
      <CoreThemeStoreProvider store={store}>
        <GetFeatureInfoButtonConnect {...props} />
      </CoreThemeStoreProvider>,
    );

    // button should be present
    expect(screen.getByTestId('open-getfeatureinfo')).toBeTruthy();

    // close the getfeatureinfo dialog
    fireEvent.click(screen.getByTestId('open-getfeatureinfo'));
    const expectedAction = [
      uiActions.setActiveMapIdForDialog({
        type: 'getfeatureinfo-map3',
        mapId: props.mapId,
        setOpen: false,
        source: 'app',
        origin: mapEnums.MapActionOrigin.map,
      }),
    ];

    expect(store.getActions()).toEqual(expectedAction);
  });
});
