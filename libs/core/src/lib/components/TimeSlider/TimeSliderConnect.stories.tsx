/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { publicLayers, MapControls } from '@opengeoweb/webmap-react';
import { TimeSliderConnect } from '@opengeoweb/timeslider';
import { store } from '../../storybookUtils/store';
import { useDefaultMapSettings } from '../../storybookUtils/defaultStorySettings';
import { MapViewConnect } from '../MapView';

import { LegendConnect, LegendMapButtonConnect } from '../Legend';
import { CoreThemeStoreProvider } from '../Providers/Providers';

export default { title: 'components/TimeSlider/TimeSliderConnect' };

interface ExampleComponentProps {
  mapId: string;
}

const ExampleComponent: React.FC<ExampleComponentProps> = ({
  mapId,
}: ExampleComponentProps) => {
  useDefaultMapSettings({
    mapId,
    layers: [
      {
        ...publicLayers.radarLayer,
        id: `radar-${mapId}`,
      },
    ],
    baseLayers: [
      {
        ...publicLayers.defaultLayers.baseLayerGrey,
        id: `baseGrey-${mapId}`,
      },
      publicLayers.defaultLayers.overLayer,
    ],
  });
  return (
    <div style={{ height: '100vh' }}>
      <LegendConnect mapId={mapId} />
      <MapControls>
        <LegendMapButtonConnect mapId={mapId} />
      </MapControls>
      <div style={{ height: '100vh' }}>
        <MapViewConnect mapId={mapId} />
      </div>
      <div
        style={{
          position: 'absolute',
          left: '0px',
          bottom: '0px',
          zIndex: 10,
          right: '0px',
        }}
      >
        <TimeSliderConnect sourceId="timeslider-1" mapId={mapId} />
      </div>
    </div>
  );
};

export const DemoTimeSliderConnect = (): React.ReactElement => (
  <CoreThemeStoreProvider store={store}>
    <ExampleComponent mapId="mapid_1" />
  </CoreThemeStoreProvider>
);

DemoTimeSliderConnect.storyName = 'Time Slider Connect';
DemoTimeSliderConnect.parameters = {
  zeplinLink: [
    {
      name: 'Light theme',
      link: 'https://app.zeplin.io/project/5ecf84a3c6ae1047a368f393/screen/609aac2d83b125bd6e2040b7/version/625e84ad2f0ac41432c8f568',
    },
    {
      name: 'Dark theme',
      link: 'https://app.zeplin.io/project/5ecf84a3c6ae1047a368f393/screen/5eea2a91efab124620a1c354/version/625e84c775ae1910e6caa462',
    },
  ],
};
