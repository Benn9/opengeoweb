/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import {
  render,
  fireEvent,
  waitForElementToBeRemoved,
  waitFor,
  act,
  screen,
} from '@testing-library/react';
import { mockGetCapabilities, getCapabilities } from '@opengeoweb/webmap';
import WMSLoader from './WMSLoader';
import { defaultTestServices } from '../../utils/defaultTestSettings';

describe('src/components/WMSLoader/WMSLoader', () => {
  jest
    .spyOn(getCapabilities, 'getLayersFromService')
    .mockImplementation(mockGetCapabilities.mockGetLayersFromService);

  it('should open and close the dialog when clicking the corresponding buttons', async () => {
    render(<WMSLoader preloadedServices={defaultTestServices} />);

    const openDialogButton = screen.getByRole('button');
    fireEvent.click(openDialogButton);

    const dialog = screen.getByRole('dialog');
    expect(dialog).toBeTruthy();

    const closeButton = screen.getByText('Close');
    fireEvent.click(closeButton);

    await waitForElementToBeRemoved(() => screen.queryByRole('dialog'));
  });

  it('should add a new service to the list when entering a valid service url and pressing the add button', async () => {
    render(<WMSLoader preloadedServices={defaultTestServices} />);
    const openDialogButton = screen.getByRole('button');

    fireEvent.click(openDialogButton);

    // simulate typing a service url in the inputfield
    const inputServiceUrl = screen.getByRole('textbox');
    fireEvent.change(inputServiceUrl, {
      target: { value: mockGetCapabilities.MOCK_URL_WITH_CHILDREN },
    });
    await waitFor(() =>
      expect(inputServiceUrl.getAttribute('value')).toEqual(
        mockGetCapabilities.MOCK_URL_WITH_CHILDREN,
      ),
    );

    // add the service to the list by clicking the add button
    fireEvent.click(screen.getByTestId('add-service'));
    expect(
      await screen.findByText(mockGetCapabilities.mockLayersWithChildren.title),
    ).toBeTruthy();

    // the service should be added to the list in alphabetical order
    const serviceList = screen.getAllByTestId('service-item');
    expect(serviceList[2].textContent).toEqual(
      mockGetCapabilities.mockLayersWithChildren.title,
    );
  });

  it('should add a new service to the list when entering a valid service url and using the enter key', async () => {
    render(<WMSLoader preloadedServices={defaultTestServices} />);
    const openDialogButton = screen.getByRole('button');

    fireEvent.click(openDialogButton);

    // simulate typing a service url in the inputfield
    const inputServiceUrl = screen.getByRole('textbox');
    fireEvent.change(inputServiceUrl, {
      target: { value: mockGetCapabilities.MOCK_URL_WITH_CHILDREN },
    });
    await waitFor(() =>
      expect(inputServiceUrl.getAttribute('value')).toEqual(
        mockGetCapabilities.MOCK_URL_WITH_CHILDREN,
      ),
    );

    // add the service to the list by pressing the Enter key
    fireEvent.keyPress(inputServiceUrl, {
      key: 'Enter',
      code: 'Enter',
      charCode: 13,
    });

    expect(
      await screen.findByText(mockGetCapabilities.mockLayersWithChildren.title),
    ).toBeTruthy();

    // the service should be added to the list in alphabetical order
    const serviceList = screen.getAllByTestId('service-item');
    expect(serviceList[2].textContent).toEqual(
      mockGetCapabilities.mockLayersWithChildren.title,
    );
  });

  it('should add a new service to the list using name if no title available', async () => {
    render(<WMSLoader preloadedServices={defaultTestServices} />);
    const openDialogButton = screen.getByRole('button');

    fireEvent.click(openDialogButton);

    // simulate typing a service url in the inputfield
    const inputServiceUrl = screen.getByRole('textbox');
    fireEvent.change(inputServiceUrl, {
      target: { value: mockGetCapabilities.MOCK_URL_WITH_NO_TITLE },
    });
    await waitFor(() =>
      expect(inputServiceUrl.getAttribute('value')).toEqual(
        mockGetCapabilities.MOCK_URL_WITH_NO_TITLE,
      ),
    );

    // add the service to the list by pressing the Enter key
    fireEvent.keyPress(inputServiceUrl, {
      key: 'Enter',
      code: 'Enter',
      charCode: 13,
    });

    expect(
      await screen.findByText(mockGetCapabilities.mockLayersWithNoTitle.name!),
    ).toBeTruthy();

    // the service should be added to the list in alphabetical order
    const serviceList = screen.getAllByTestId('service-item');
    expect(serviceList[2].textContent).toEqual(
      mockGetCapabilities.mockLayersWithNoTitle.name,
    );
  });

  it('should add a new service to the list using url if no title or name available', async () => {
    render(<WMSLoader preloadedServices={defaultTestServices} />);
    const openDialogButton = screen.getByRole('button');

    fireEvent.click(openDialogButton);

    // simulate typing a service url in the inputfield
    const inputServiceUrl = screen.getByRole('textbox');
    fireEvent.change(inputServiceUrl, {
      target: { value: mockGetCapabilities.MOCK_URL_WITH_NO_TITLE_OR_NAME },
    });
    await waitFor(() =>
      expect(inputServiceUrl.getAttribute('value')).toEqual(
        mockGetCapabilities.MOCK_URL_WITH_NO_TITLE_OR_NAME,
      ),
    );

    // add the service to the list by pressing the Enter key
    fireEvent.keyPress(inputServiceUrl, {
      key: 'Enter',
      code: 'Enter',
      charCode: 13,
    });

    const shortenedURL =
      mockGetCapabilities.MOCK_URL_WITH_NO_TITLE_OR_NAME.substring(
        mockGetCapabilities.MOCK_URL_WITH_NO_TITLE_OR_NAME.indexOf('//') + 2,
        mockGetCapabilities.MOCK_URL_WITH_NO_TITLE_OR_NAME.indexOf('?'),
      );

    expect(await screen.findByText(shortenedURL)).toBeTruthy();

    // the service should be added to the list in alphabetical order
    const serviceList = screen.getAllByTestId('service-item');
    expect(serviceList[2].textContent).toEqual(shortenedURL);
  });

  it('should update the active service after clicking a service', async () => {
    render(<WMSLoader preloadedServices={defaultTestServices} />);
    const openDialogButton = screen.getByRole('button');

    fireEvent.click(openDialogButton);
    const serviceList = await screen.findAllByTestId('service-item');

    // first one should be active
    expect(serviceList[0].getAttribute('class')).toContain('Mui-selected');

    // click the second one
    fireEvent.click(serviceList[1]);

    // now second one should be active
    await waitFor(() =>
      expect(serviceList[1].getAttribute('class')).toContain('Mui-selected'),
    );
  });

  it('should show an error message when the given service url is invalid', async () => {
    render(<WMSLoader preloadedServices={defaultTestServices} />);
    const openDialogButton = screen.getByRole('button');

    fireEvent.click(openDialogButton);

    // simulate typing an invalid service url in the inputfield
    const inputServiceUrl = screen.getByRole('textbox');
    fireEvent.change(inputServiceUrl, {
      target: { value: 'some-test-url' },
    });
    await waitFor(() =>
      expect(inputServiceUrl.getAttribute('value')).toEqual('some-test-url'),
    );

    // try to add the service to the list
    fireEvent.click(screen.getByTestId('add-service'));

    // error message should be displayed
    expect(await screen.findByText('Please enter a valid URL')).toBeTruthy();
  });

  it('should show an error message when the given service url is not a wms service', async () => {
    render(<WMSLoader preloadedServices={defaultTestServices} />);
    const openDialogButton = screen.getByRole('button');

    fireEvent.click(openDialogButton);

    // simulate typing a service url that is not a wms service
    const inputServiceUrl = screen.getByRole('textbox');
    fireEvent.change(inputServiceUrl, {
      target: { value: mockGetCapabilities.MOCK_URL_INVALID },
    });
    await waitFor(() =>
      expect(inputServiceUrl.getAttribute('value')).toEqual(
        mockGetCapabilities.MOCK_URL_INVALID,
      ),
    );

    // try to add the service to the list
    fireEvent.click(screen.getByTestId('add-service'));

    // error message should be displayed
    expect(
      await screen.findByText('Please enter a valid WMS service'),
    ).toBeTruthy();
  });

  it('should show an error message when the given service url starts with http instead of https', async () => {
    render(<WMSLoader preloadedServices={defaultTestServices} />);
    const openDialogButton = screen.getByRole('button');

    fireEvent.click(openDialogButton);

    // simulate typing a service url with http protocol
    const inputServiceUrl = screen.getByRole('textbox');
    fireEvent.change(inputServiceUrl, {
      target: { value: mockGetCapabilities.MOCK_URL_HTTP },
    });
    await waitFor(() =>
      expect(inputServiceUrl.getAttribute('value')).toEqual(
        mockGetCapabilities.MOCK_URL_HTTP,
      ),
    );

    // try to add the service to the list
    fireEvent.click(screen.getByTestId('add-service'));

    // error message should be displayed
    expect(
      await screen.findByText(
        'Use https or allow Mixed Content in your browser to use this WMS service',
      ),
    ).toBeTruthy();
  });

  it('should set the service as active when trying to add a service to the list twice', async () => {
    render(<WMSLoader preloadedServices={defaultTestServices} />);
    const openDialogButton = screen.getByRole('button');

    fireEvent.click(openDialogButton);

    // add a service to the list
    const inputServiceUrl = screen.getByRole('textbox');
    fireEvent.change(inputServiceUrl, {
      target: { value: mockGetCapabilities.MOCK_URL_WITH_CHILDREN },
    });
    await waitFor(() =>
      expect(inputServiceUrl.getAttribute('value')).toEqual(
        mockGetCapabilities.MOCK_URL_WITH_CHILDREN,
      ),
    );
    fireEvent.click(screen.getByTestId('add-service'));
    expect(
      await screen.findByText(mockGetCapabilities.mockLayersWithChildren.title),
    ).toBeTruthy();

    // check that it is not active yet
    const serviceList = screen.getAllByTestId('service-item');
    expect(serviceList[2].getAttribute('class')).not.toContain('Mui-selected');
    const expectedListLength = serviceList.length;

    // add the same service again
    fireEvent.change(inputServiceUrl, {
      target: { value: mockGetCapabilities.MOCK_URL_WITH_CHILDREN },
    });
    expect(inputServiceUrl.getAttribute('value')).toEqual(
      mockGetCapabilities.MOCK_URL_WITH_CHILDREN,
    );
    fireEvent.click(screen.getByTestId('add-service'));

    // check that it is active now
    expect(serviceList[2].getAttribute('class')).toContain('Mui-selected');
    expect(
      await screen.findByText(
        `Available layers for ${serviceList[2].textContent}`,
      ),
    ).toBeTruthy();

    // check that it wasn't added twice
    expect(screen.getAllByTestId('service-item').length).toEqual(
      expectedListLength,
    );
  });

  it('should be able to pass renderProp onRenderTree', async () => {
    const props = {
      onRenderTree: jest.fn(),
      preloadedServices: defaultTestServices,
    };
    render(<WMSLoader {...props} />);
    const openDialogButton = screen.getByRole('button');

    fireEvent.click(openDialogButton);

    expect(props.onRenderTree).toHaveBeenCalledWith(defaultTestServices[0]);
  });

  it('should use the preselected list of services if none passed in', async () => {
    render(<WMSLoader preloadedServices={defaultTestServices} />);
    const openDialogButton = screen.getByRole('button');

    fireEvent.click(openDialogButton);

    const list = await screen.findAllByTestId('service-item');
    expect(list.length).toEqual(defaultTestServices.length);

    for (let i = 0; i < defaultTestServices.length; i += 1) {
      expect(screen.getByText(defaultTestServices[i].name)).toBeTruthy();
    }
  });

  it('should use the passed in list of services', async () => {
    render(<WMSLoader preloadedServices={defaultTestServices} />);
    const openDialogButton = screen.getByRole('button');
    fireEvent.click(openDialogButton);

    const list = await screen.findAllByTestId('service-item');
    expect(list.length).toEqual(defaultTestServices.length);

    for (let i = 0; i < defaultTestServices.length; i += 1) {
      expect(screen.getByText(defaultTestServices[i].name)).toBeTruthy();
    }
  });

  it('should show the layers of the new service after selecting a new service and the previous service loads successfully in the background', async () => {
    jest.useFakeTimers();

    const services = [
      {
        name: 'Meteo',
        url: mockGetCapabilities.MOCK_URL_WITH_SUBCATEGORY,
        id: 'Meteo',
      },
      {
        name: 'Slow service',
        url: mockGetCapabilities.MOCK_URL_SLOW,
        id: 'SlowService',
      },
    ];

    render(<WMSLoader preloadedServices={services} />);
    const openDialogButton = screen.getByRole('button');

    fireEvent.click(openDialogButton);

    const serviceList = await screen.findAllByTestId('service-item');

    // click the second service, for which loading is slow
    fireEvent.click(serviceList[1]);
    expect(screen.getByRole('progressbar')).toBeTruthy();

    // click the first service, for which loading is fast
    fireEvent.click(serviceList[0]);

    // wait for both services to finish loading
    await act(async () => {
      jest.advanceTimersByTime(500);
    });
    await waitFor(() => expect(screen.queryByRole('progressbar')).toBeFalsy());

    // the content of the layer list should correspond to the first service
    const layerList = screen.getByTestId('layer-list');
    const { children } = mockGetCapabilities.mockLayersWithSubcategory;

    expect(layerList.textContent).toContain(children[0].title);
    jest.clearAllTimers();
    jest.useRealTimers();
  });

  it('should show the layers of the new service after selecting a new service and the previous service gives an error in the background', async () => {
    jest.useFakeTimers();

    const services = [
      {
        name: 'Meteo',
        url: mockGetCapabilities.MOCK_URL_WITH_SUBCATEGORY,
        id: 'Meteo',
      },
      {
        name: 'Slow service',
        url: mockGetCapabilities.MOCK_URL_SLOW_FAILS,
        id: 'SlowService',
      },
    ];

    render(<WMSLoader preloadedServices={services} />);
    const openDialogButton = screen.getByRole('button');

    fireEvent.click(openDialogButton);

    const serviceList = await screen.findAllByTestId('service-item');

    // click the second service, for which loading is slow
    fireEvent.click(serviceList[1]);
    expect(screen.getByRole('progressbar')).toBeTruthy();

    // click the first service, for which loading is fast
    fireEvent.click(serviceList[0]);

    // wait for both services to finish loading
    await act(async () => {
      jest.advanceTimersByTime(500);
    });
    await waitFor(() => expect(screen.queryByRole('progressbar')).toBeFalsy());

    // the content of the layer list should correspond to the first service
    const layerList = screen.getByTestId('layer-list');
    const { children } = mockGetCapabilities.mockLayersWithSubcategory;
    expect(layerList.textContent).toContain(children[0].title);
    jest.clearAllTimers();
    jest.useRealTimers();
  });
});
