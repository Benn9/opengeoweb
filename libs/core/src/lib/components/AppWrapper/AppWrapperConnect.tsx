/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { withEggs } from '@opengeoweb/shared';
import { useDispatch } from 'react-redux';
import { appActions, appModuleConfig } from '@opengeoweb/store';

interface AppWrapperProps {
  children: React.ReactElement;
}

export const AppWrapperConnect: React.FC<AppWrapperProps> = withEggs([
  appModuleConfig,
])(({ children }: AppWrapperProps) => {
  const dispatch = useDispatch();

  const initialiseApp = React.useCallback((): void => {
    dispatch(appActions.initialiseApp());
  }, [dispatch]);

  React.useEffect(() => {
    // Call initialisation
    initialiseApp();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return children;
});
