/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { createMockStoreWithEggs } from '@opengeoweb/shared';
import { LayerType, WMLayer, webmapUtils } from '@opengeoweb/webmap';
import {
  createTheme,
  colorsLight,
  GeowebColorPalette,
  Add,
  Clock,
  Success,
  ArrowDown,
} from '@opengeoweb/theme';
import { Box, Divider } from '@mui/material';
import { useSelector } from 'react-redux';
import {
  layerSelectors,
  serviceSelectors,
  CoreAppStore,
  storeTestUtils,
} from '@opengeoweb/store';
import LayerManager from './LayerManager';
import { CoreThemeStoreProvider } from '../Providers/Providers';
import { LayerManagerCustomSettings } from './LayerManagerUtils';
import { SingleValueComponentProps } from './LayerContainerRow/LayerRow/DimensionSelect/DimensionSelect';

export default { title: 'components/LayerManagerCustom' };

const customThemeColors: GeowebColorPalette = {
  ...colorsLight,
  buttons: {
    ...colorsLight.buttons,
    flat: {
      ...colorsLight.buttons.flat,
      default: {
        ...colorsLight.buttons.flat.default,
        color: '3A66E3',
      },
      mouseOver: {
        ...colorsLight.buttons.flat.mouseOver,
        fill: 'grey',
      },
    },
    tool: {
      ...colorsLight.buttons.tool,
      default: {
        ...colorsLight.buttons.tool.default,
        color: '3A66E3',
      },
      mouseOver: {
        ...colorsLight.buttons.tool.mouseOver,
        fill: 'grey',
      },
    },
  },
  iconButtons: {
    ...colorsLight.iconButtons,
    flat: {
      ...colorsLight.iconButtons.flat,
      default: {
        ...colorsLight.iconButtons.flat.default,
        color: '#3A66E3',
      },
      mouseOver: {
        ...colorsLight.iconButtons.flat.mouseOver,
        fill: 'grey',
      },
    },
    tool: {
      ...colorsLight.iconButtons.tool,
      default: {
        ...colorsLight.iconButtons.tool.default,
        color: '#3A66E3',
      },
      mouseOver: {
        ...colorsLight.iconButtons.tool.mouseOver,
        fill: 'grey',
      },
    },
  },
  layerManager: {
    ...colorsLight.layerManager,
    headerRowText: {
      opacity: 1,
      color: '#303193',
      fontSize: '14px',
      fontWeight: 500,
    },
    tableRowDefaultText: {
      fontSize: '14px',
      rgba: '#303193',
    },
    dragHandleHover: {
      backgroundColor: 'grey',
    },
    opacitySelect: {
      button: {
        whiteSpace: 'nowrap',
        '&.open': {
          outline: '1px solid #303193',
        },
      },
      popper: {
        border: '1px solid #303193',
      },
      root: {
        height: '36px',
      },
    },
    baseLayerRow: {
      root: {
        fontSize: '12px',
      },
    },
  },
  tooltips: {
    ...colorsLight.tooltips,
    tooltipSelect: {
      menu: {
        '.MuiPaper-root': {
          border: '1px solid #303193',
        },
      },
      select: {
        '&.Mui-focused': {
          outline: '1px solid #303193',
        },
      },
    },
  },
};

const customTheme = createTheme({
  geowebColors: customThemeColors,
});

const CustomRenderLayersConnect: React.FC<{ layerId: string }> = ({
  layerId,
}) => {
  const layer = useSelector((store: CoreAppStore) =>
    layerSelectors.getLayerById(store, layerId),
  );
  const layerName = useSelector((store: CoreAppStore) =>
    layerSelectors.getLayerName(store, layerId),
  );
  const layerService = useSelector((store: CoreAppStore) =>
    layerSelectors.getLayerService(store, layerId),
  );
  const layers = useSelector((store: CoreAppStore) => {
    return serviceSelectors.getLayersFromServiceSelector(store, layerService);
  });
  const serviceLayer = layers.find((l) => l.name === layerName) || layer;
  return (
    <Box
      sx={{
        fontSize: '12px',
      }}
    >
      {serviceLayer?.title ?? serviceLayer?.name ?? 'loading...'}
    </Box>
  );
};

const SingleValueComponent: React.FC<SingleValueComponentProps> = ({
  position,
  currentValue,
  name,
  nameMappings,
}) => {
  const label =
    position === 'value'
      ? `Single value: ${currentValue}`
      : (nameMappings && name && nameMappings[name]) || name;
  return (
    <Box
      sx={{
        fontSize: '12px',
        display: 'inline-flex',
        overflow: 'clip',
        textOverflow: 'ellipsis',
        width: '50%',
      }}
    >
      {label}
    </Box>
  );
};

const customSettings: LayerManagerCustomSettings = {
  toolbar: {
    dragHandle: {
      icon: <Add />,
    },
    controlButtonSettings: {
      small: {
        isDisabled: true,
      },
      medium: {
        icon: <Clock />,
      },
      large: {
        isDisabled: true,
      },
      dock: {
        isDisabled: true,
      },
      divider: {
        element: (
          <Divider
            sx={{ height: '24px', margin: '0 4px' }}
            orientation="vertical"
          />
        ),
      },
    },
    closeButton: {
      icon: <Success />,
    },
  },
  header: {
    addLayer: {
      tooltipTitle: 'Custom tooltip',
      icon: (
        <Box
          sx={{
            display: 'flex',
            fontSize: '14px',
            color: '#303193',
            alignItems: 'center',
          }}
        >
          <Add />
          Custom
          <br />
          text
        </Box>
      ),
    },
    layerName: {
      title: 'Custom name',
    },
    layerStyle: {
      title: 'Custom style',
    },
    opacity: {
      title: 'Custom opacity',
    },
    dimensions: {
      title: 'Custom dimensions',
    },
  },
  content: {
    dragHandle: {
      tooltipTitle: 'Custom tooltip',
      icon: <Add />,
    },
    activateLayer: {
      isDisabled: true,
    },
    enableLayer: {
      enabledTooltipTitle: 'Hide',
      disabledTooltipTitle: 'Show',
      enabledIcon: <Success />,
      disabledIcon: <Clock />,
    },
    renderLayer: {
      tooltipPrefix: 'Current layer: ',
      Element: CustomRenderLayersConnect,
    },
    layerStyle: {
      tooltipPrefix: 'Custom style: ',
      icon: ArrowDown,
    },
    opacity: {
      tooltipPrefix: 'Custom opacity: ',
    },
    dimensions: {
      dimensionsToShow: ['test', 'elevation'],
      SingleValueComponent,
      icon: ArrowDown,
      nameMappings: { test: 'Test dimension' },
      valueMappings: {
        reference_time: (value: string) => new Date(value).toString(),
        elevation: (value: string) => `ELEV: ${value}`,
      },
      tooltipPrefix: 'Tooltip: ',
    },
    deleteLayer: {
      tooltipTitle: 'Custom delete tooltip',
      icon: <Add />,
    },
    menu: {
      tooltipTitle: 'Custom menu tooltip',
      icon: <Add />,
    },
  },
  footer: {
    baseLayerRow: {
      title: 'Base layer',
      addLayersButton: {
        isDisabled: true,
      },
    },
    baseLayerDropdown: {
      tooltipPrefix: 'Custom tooltip: ',
      icon: Add,
    },
  },
};

export const LayerManagerCustomTheme = (): React.ReactElement => {
  const [isOpen, setIsOpen] = React.useState(true);
  const onClose = (): void => {
    setIsOpen(false);
  };

  return (
    <CoreThemeStoreProvider theme={customTheme} store={store}>
      <Box
        sx={
          {
            // if needed, you can use sx to overwrite style.
            //   '.layerRow': {
            //     paddingTop: 2,
            //     paddingBottom: 2,
            //   },
          }
        }
      >
        <LayerManager
          mapId={mapId}
          isOpen={isOpen}
          title="Custom Title"
          onClose={onClose}
          setFocused={(): void => {}}
          settings={customSettings}
        />
      </Box>
    </CoreThemeStoreProvider>
  );
};

const mapId = 'mapid_1';
const mockBaseLayer = {
  mapId,
  name: 'WorldMap_Light_Grey_Canvas',
  type: 'twms',
  dimensions: [],
  id: 'baseGrey_mapid_1',
  opacity: 1,
  enabled: true,
  layerType: LayerType.baseLayer,
};

const mockRadarLayer = {
  mapId,
  service: 'serviceId',
  name: 'RAD_NL25_PCP_CM',
  format: 'image/png',
  style: 'radar/nearest',
  dimensions: [
    {
      name: 'time',
      units: 'ISO8601',
      currentValue: '2021-12-13T16:00:00Z',
      maxValue: '2021-12-13T16:00:00Z',
      minValue: '2021-03-31T09:25:00Z',
      values: '2021-03-31T09:25:00Z/2021-12-13T16:00:00Z/PT5M',
    },
  ],
  styles: [
    {
      title: 'radar/nearest',
      name: 'radar/nearest',
      legendURL: 'url',
      abstract: 'No abstract available',
    },
    {
      title: 'precip-rainbow/nearest',
      name: 'precip-rainbow/nearest',
      legendURL: 'url',
      abstract: 'No abstract available',
    },
  ],
  id: 'layerid_41',
  opacity: 1,
  enabled: true,
  layerType: LayerType.mapLayer,
};

const mockElevationLayer = {
  mapId,
  service: 'serviceId',
  name: 'ELEVATION_LAYER',
  format: 'image/png',
  style: 'temp',
  dimensions: [
    {
      name: 'elevation',
      units: 'hPa',
      currentValue: '850',
      maxValue: '1000',
      minValue: '200',
      timeInterval: null!,
      values: '200,300,400,500,600,700,800,850,900,925,950,1000',
      synced: false,
    },
    {
      name: 'test',
      currentValue: '1',
      minValue: '1',
      maxValue: '1',
      values: '1',
      timeInterval: null!,
      synced: false,
    },
  ],
  styles: [
    {
      title: 'Temperature (wow) shaded + contours',
      name: 'temp',
      legendURL: 'url',
      abstract: 'No abstract available',
    },
    {
      title: 'Automatic minimum and maximum',
      name: 'min-max',
      legendURL: 'url',
      abstract: 'No abstract available',
    },
  ],
  id: 'layerid_test',
  opacity: 0.8,
  enabled: false,
  layerType: LayerType.mapLayer,
};

const mockAlternativeElevationLayer = {
  ...mockElevationLayer,
  id: 'layerid_3',
  dimensions: [
    {
      name: 'test',
      currentValue: '1',
      units: 'test_unit',
      minValue: '1',
      maxValue: '4',
      values: '1,2,3,4',
      timeInterval: null!,
    },
  ],
};

const mockPressureLayer = {
  mapId,
  service: 'serviceId',
  name: 'air_pressure_at_sea_level',
  dimensions: [
    {
      name: 'time',
      units: 'ISO8601',
      currentValue: '2022-03-29T09:00:00Z',
      minValue: '2022-03-29T09:00:00Z',
      maxValue: '2022-03-31T09:00:00Z',
      synced: false,
    },
    {
      name: 'reference_time',
      units: 'ISO8601',
      currentValue: '2022-03-29T09:00:00Z',
      minValue: '2022-03-27T00:00:00Z',
      maxValue: '2022-03-29T09:00:00Z',
      timeInterval: null!,
      synced: false,
    },
  ],
  id: 'layerid_20',
  opacity: 1,
  enabled: true,
  layerType: LayerType.mapLayer,
  style: 'pressure_cwk/contour',
};

const wmMockElevationLayer = new WMLayer(mockElevationLayer);
webmapUtils.registerWMLayer(wmMockElevationLayer, mockElevationLayer.id);

const wmMockAlternativeElevationLayer = new WMLayer(
  mockAlternativeElevationLayer,
);
webmapUtils.registerWMLayer(
  wmMockAlternativeElevationLayer,
  mockAlternativeElevationLayer.id,
);

const mockState1 = storeTestUtils.mockStateMapWithMultipleLayers(
  [
    mockRadarLayer,
    mockElevationLayer,
    mockAlternativeElevationLayer,
    mockPressureLayer,
    mockBaseLayer,
  ],
  mapId,
);

const mockState = {
  ...mockState1,
  services: {
    byId: {
      serviceId: {
        serviceUrl: mockRadarLayer.service,
        layers: [
          {
            name: 'RAD_NL25_PCP_CM',
            title: 'Precipitation Radar NL',
            leaf: true,
            path: [],
            keywords: [],
            styles: [
              {
                title: 'radar/nearest',
                name: 'radar/nearest',
                legendURL: 'url',
                abstract: 'No abstract available',
              },
              {
                title: 'precip-rainbow/nearest',
                name: 'precip-rainbow/nearest',
                legendURL: 'url',
                abstract: 'No abstract available',
              },
            ],
          },
          {
            name: 'ELEVATION_LAYER',
            title: 'Temperature (PL)',
            leaf: true,
            path: [],
            keywords: [],
            styles: [
              {
                title: 'Temperature (wow) shaded + contours',
                name: 'temp',
                legendURL: 'url',
                abstract: 'No abstract available',
              },
              {
                title: 'Automatic minimum and maximum',
                name: 'min-max',
                legendURL: 'url',
                abstract: 'No abstract available',
              },
            ],
          },
        ],
      },
    },
    allIds: ['serviceId'],
  },
};

const store = createMockStoreWithEggs(mockState);
