/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { render, fireEvent, screen } from '@testing-library/react';
import { createMockStoreWithEggs } from '@opengeoweb/shared';
import {
  layerActions,
  layerTypes,
  storeTestSettings,
  storeTestUtils,
} from '@opengeoweb/store';
import { defaultReduxLayerRadarColor } from '../../../../../utils/defaultTestSettings';
import EnableLayerConnect from './EnableLayerConnect';
import { CoreThemeStoreProvider } from '../../../../Providers/Providers';

describe('src/components/LayerManager/LayerContainerRow/LayerRow/EnableLayer/EnableLayerConnect', () => {
  it('should enable/disable layer', async () => {
    const mapId = 'mapid_1';
    const layer = defaultReduxLayerRadarColor;
    const mockState = storeTestUtils.mockStateMapWithLayer(layer, mapId);
    const store = createMockStoreWithEggs(mockState);
    const props = {
      layerId: layer.id,
      mapId: 'map-test',
      isEnabled: true,
    };

    render(
      <CoreThemeStoreProvider store={store}>
        <EnableLayerConnect {...props} />
      </CoreThemeStoreProvider>,
    );

    const enableButton = screen.getAllByRole('button')[0];

    fireEvent.click(enableButton);

    const expectedAction = [
      layerActions.layerChangeEnabled({
        layerId: props.layerId,
        enabled: !defaultReduxLayerRadarColor.enabled,
        mapId: props.mapId,
        origin: layerTypes.LayerActionOrigin.layerManager,
      }),
    ];

    expect(store.getActions()).toEqual(expectedAction);
  });

  it('should show layername on hover', async () => {
    const mapId = 'mapid_1';
    const layer = defaultReduxLayerRadarColor;
    const mockState = storeTestUtils.mockStateMapWithLayer(layer, mapId);
    const store = createMockStoreWithEggs(mockState);
    const props = {
      layerId: layer.id,
      mapId: 'map-test',
      layerName:
        storeTestSettings.defaultReduxServices.serviceid_1.layers![0].title,
    };

    render(
      <CoreThemeStoreProvider store={store}>
        <EnableLayerConnect {...props} />
      </CoreThemeStoreProvider>,
    );

    const enableButton = screen.getAllByRole('button')[1];

    fireEvent.mouseOver(enableButton);

    expect(
      await screen.findByText(
        storeTestSettings.defaultReduxServices.serviceid_1.layers![0].title,
      ),
    ).toBeTruthy();
  });

  it('should show custom tooltip and icon', async () => {
    const mapId = 'mapid_1';
    const layer = defaultReduxLayerRadarColor;
    const mockState = storeTestUtils.mockStateMapWithLayer(layer, mapId);
    const store = createMockStoreWithEggs(mockState);
    const testId = 'testIcon';
    const tooltip = 'testTooltip';
    const props = {
      layerId: layer.id,
      mapId: 'map-test',
      layerName:
        storeTestSettings.defaultReduxServices.serviceid_1.layers![0].title,
      tooltipTitle: tooltip,
      icon: <span data-testid={testId} />,
    };

    render(
      <CoreThemeStoreProvider store={store}>
        <EnableLayerConnect {...props} />
      </CoreThemeStoreProvider>,
    );

    const enableButton = screen.getAllByTestId(testId)[0];
    expect(enableButton).toBeTruthy();
    fireEvent.mouseOver(enableButton);
    expect(await screen.findByText(tooltip));
  });
});
