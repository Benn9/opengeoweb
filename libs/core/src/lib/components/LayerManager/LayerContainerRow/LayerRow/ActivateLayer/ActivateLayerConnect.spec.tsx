/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { render, screen } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import { produce } from 'immer';
import { createToolkitMockStoreWithEggs } from '@opengeoweb/shared';
import { layerReducer, storeTestUtils, webmapReducer } from '@opengeoweb/store';
import { defaultReduxLayerRadarKNMI } from '../../../../../utils/defaultTestSettings';
import { CoreThemeStoreProvider } from '../../../../Providers/Providers';
import ActivateLayerConnect from './ActivateLayerConnect';

describe('src/components/LayerManager/LayerContainerRow/LayerRow/ActivateLayer/ActivateLayerConnect', () => {
  const mapId = 'mapid_1';
  const layer = defaultReduxLayerRadarKNMI;
  const mockState = storeTestUtils.mockStateMapWithLayer(layer, mapId);

  it('should activate a layer', async () => {
    const mockState1 = produce(mockState, (draft) => {
      draft.webmap!.byId[mapId].autoTimeStepLayerId = undefined;
      draft.webmap!.byId[mapId].autoUpdateLayerId = undefined;
    });
    const user = userEvent.setup();
    const newMockState = {
      reducer: { webmap: webmapReducer, layers: layerReducer },
      preloadedState: { webmap: mockState1.webmap, layers: mockState1.layers },
    };

    const store = createToolkitMockStoreWithEggs(newMockState);

    render(
      <CoreThemeStoreProvider store={store}>
        <ActivateLayerConnect mapId={mapId} layerId={layer.id!} />
      </CoreThemeStoreProvider>,
    );

    await user.click(screen.getByRole('button', { name: /Auto none/ }));
    await user.click(screen.getByRole('option', { name: /Both/i }));
    await user.click(screen.getByRole('button', { name: /Auto both/i }));
    await user.click(screen.getByRole('option', { name: /Auto-timestep/i }));
    await user.click(screen.getByRole('button', { name: /Auto-timestep/i }));
    await user.click(screen.getByRole('option', { name: /Auto-update/i }));
    await user.click(screen.getByRole('button', { name: /Auto-update/i }));
    await user.click(screen.getByRole('option', { name: /None/i }));
    screen.getByRole('button', { name: /Auto none/i });
  });

  it('should not show for a layer without time dimension', async () => {
    const mockState1 = produce(mockState, (draft) => {
      draft.layers!.byId[layer.id!]!.dimensions = [];
    });
    const newMockState = {
      reducer: { webmap: webmapReducer, layers: layerReducer },
      preloadedState: { webmap: mockState1.webmap, layers: mockState1.layers },
    };
    const store = createToolkitMockStoreWithEggs(newMockState);
    const user = userEvent.setup();

    render(
      <CoreThemeStoreProvider store={store}>
        <ActivateLayerConnect mapId={mapId} layerId={layer.id!} />
      </CoreThemeStoreProvider>,
    );

    const noTimeDimension = screen.getByTestId('noTimeDimension');
    await user.hover(noTimeDimension);
    await screen.findByRole('tooltip', { name: 'Layer has no time dimension' });
  });
});
