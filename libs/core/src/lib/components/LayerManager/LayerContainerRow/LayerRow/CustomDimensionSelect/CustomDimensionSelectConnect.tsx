/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';

import { mapTypes } from '@opengeoweb/store';
import DimensionSelect from '../DimensionSelect/DimensionSelect';
import {
  DimensionSelectProps,
  useLayerDimensions,
} from '../DimensionSelect/DimensionSelectConnect';
import { LayerManagerCustomSettings } from '../../../LayerManagerUtils';

type Content = NonNullable<LayerManagerCustomSettings['content']>;
type Dimensions = NonNullable<Content['dimensions']>;
type DimensionsSettingsProps = Omit<Dimensions, 'nameMappings'>;

type CustomDimensionSelectProps = DimensionSelectProps &
  DimensionsSettingsProps;

const CustomDimensionSelectConnect: React.FC<CustomDimensionSelectProps> = ({
  layerId,
  mapId,
  dimensionsToShow,
  SingleValueComponent,
  ...props
}: CustomDimensionSelectProps) => {
  const { layerDimensions, isLayerEnabled, layerChangeDimension } =
    useLayerDimensions(layerId, mapId);

  const filteredDimensions = dimensionsToShow
    ? (dimensionsToShow
        .map(
          (
            dimensionName, // Use dimensionToShow order
          ) =>
            layerDimensions.find(
              (dimension) => dimension.name === dimensionName,
            ),
        )
        .filter(Boolean) as mapTypes.Dimension[])
    : layerDimensions;

  return (
    <DimensionSelect
      layerDimensions={filteredDimensions}
      SingleValueComponent={SingleValueComponent}
      onLayerChangeDimension={(
        dimensionName: string,
        dimensionValue: string,
        origin,
      ): void => {
        const dimension: mapTypes.Dimension = {
          name: dimensionName,
          currentValue: dimensionValue,
        };
        layerChangeDimension({
          dimension,
          origin,
        });
      }}
      isEnabled={isLayerEnabled}
      {...props}
    />
  );
};

export default CustomDimensionSelectConnect;
