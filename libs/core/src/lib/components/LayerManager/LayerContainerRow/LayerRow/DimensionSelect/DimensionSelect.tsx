/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { MenuItem, Grid } from '@mui/material';
import { TooltipSelect } from '@opengeoweb/shared';
import { mapTypes, layerTypes } from '@opengeoweb/store';
import { ElementType, FC, useState } from 'react';
import type {
  DimensionNameMappings,
  DimensionValueMappings,
} from '../../../LayerManagerUtils';

export interface SingleValueComponentProps extends mapTypes.Dimension {
  position: 'dimension' | 'value';
  nameMappings?: DimensionNameMappings;
  valueMappings?: DimensionValueMappings;
}

export interface DimensionSelectProps {
  layerDimensions?: mapTypes.Dimension[];
  onLayerChangeDimension: (
    dimensionName: string,
    dimensionValue: string,
    origin?: layerTypes.LayerActionOrigin.layerManager,
  ) => void;
  isEnabled?: boolean;
  icon?: ElementType;
  nameMappings?: DimensionNameMappings;
  valueMappings?: DimensionValueMappings;
  tooltipPrefix?: string;
  useLatestReferenceTime?: boolean;
  setUseLatestReferenceTime?: (useLatestReferenceTime: boolean) => void;
  SingleValueComponent?: React.FC<SingleValueComponentProps>;
  latestLabel?: string;
}

const DimensionSelect: FC<DimensionSelectProps> = ({
  onLayerChangeDimension,
  layerDimensions = [],
  isEnabled = false,
  icon,
  nameMappings,
  valueMappings,
  tooltipPrefix = 'Dimensions: ',
  setUseLatestReferenceTime,
  useLatestReferenceTime,
  SingleValueComponent,
  latestLabel,
}: DimensionSelectProps) => {
  const [selectedDimensionName, setSelectedDimensionName] = useState(
    layerDimensions?.[0]?.name!,
  );
  const selectedDimension = layerDimensions.find(
    (dimension) => dimension.name === selectedDimensionName,
  );

  const dimensionNames = layerDimensions.map((dimension) => dimension.name!);
  const selectedDimensionDoesntExist = !dimensionNames.includes(
    selectedDimensionName,
  );
  React.useEffect(() => {
    if (selectedDimensionDoesntExist) {
      setSelectedDimensionName(layerDimensions?.[0]?.name ?? '');
    }
  }, [layerDimensions, selectedDimensionDoesntExist]);

  if (selectedDimensionDoesntExist) {
    return null;
  }

  const collapseDimensionDropdown =
    SingleValueComponent && layerDimensions.length === 1;
  const collapseValueDropdown =
    SingleValueComponent && selectedDimension?.values?.length === 1;

  return (
    <Grid
      container
      direction="row"
      justifyContent="flex-start"
      alignItems="center"
    >
      <Grid item xs={12}>
        {collapseDimensionDropdown ? (
          <SingleValueComponent
            {...layerDimensions[0]}
            nameMappings={nameMappings}
            valueMappings={valueMappings}
            position="dimension"
          />
        ) : (
          <SelectDimension
            selectedDimensionName={selectedDimensionName}
            setSelectedDimensionName={setSelectedDimensionName}
            dimensionNames={dimensionNames}
            tooltipPrefix={tooltipPrefix}
            isEnabled={isEnabled}
            icon={icon}
            nameMappings={nameMappings}
          />
        )}

        {collapseValueDropdown ? (
          <SingleValueComponent
            {...layerDimensions[0]}
            nameMappings={nameMappings}
            valueMappings={valueMappings}
            position="value"
          />
        ) : (
          <SelectDimensionValue
            selectedDimension={selectedDimension}
            onLayerChangeDimension={onLayerChangeDimension}
            tooltipPrefix={tooltipPrefix}
            isEnabled={isEnabled}
            icon={icon}
            valueMappings={valueMappings}
            setUseLatestReferenceTime={setUseLatestReferenceTime}
            useLatestReferenceTime={useLatestReferenceTime}
            latestLabel={latestLabel}
          />
        )}
      </Grid>
    </Grid>
  );
};

export default DimensionSelect;

const SelectDimension: FC<{
  dimensionNames: string[];
  selectedDimensionName: string;
  setSelectedDimensionName: (dimension: string) => void;
  icon?: ElementType;
  nameMappings?: DimensionNameMappings;
  tooltipPrefix: string;
  isEnabled: boolean;
}> = ({
  selectedDimensionName,
  setSelectedDimensionName,
  icon,
  nameMappings,
  tooltipPrefix,
  isEnabled,
  dimensionNames,
}) => {
  return (
    <TooltipSelect
      disableUnderline
      tooltip={`${tooltipPrefix}${selectedDimensionName}`}
      isEnabled={isEnabled}
      value={selectedDimensionName}
      list={dimensionNames.map((name) => ({ value: name }))}
      currentIndex={dimensionNames.findIndex(
        (name) => name === selectedDimensionName,
      )}
      onChange={(event): void => {
        event.stopPropagation();
        setSelectedDimensionName(event.target.value as string);
      }}
      onChangeMouseWheel={(event): void => {
        setSelectedDimensionName(event.value);
      }}
      style={{
        maxWidth: '50%',
      }}
      IconComponent={icon}
    >
      <MenuItem disabled>Dimension</MenuItem>
      {dimensionNames.map((name) => (
        <MenuItem key={name} value={name}>
          {nameMappings?.[name] || name}
        </MenuItem>
      ))}
    </TooltipSelect>
  );
};

const SelectDimensionValue: FC<{
  selectedDimension?: mapTypes.Dimension;
  icon?: ElementType;
  tooltipPrefix: string;
  isEnabled: boolean;
  onLayerChangeDimension: (
    dimensionName: string,
    dimensionValue: string,
    origin?: layerTypes.LayerActionOrigin.layerManager,
  ) => void;
  valueMappings?: DimensionValueMappings;
  useLatestReferenceTime?: boolean;
  setUseLatestReferenceTime?: (useLatestReferenceTime: boolean) => void;
  latestLabel?: string;
}> = ({
  isEnabled,
  tooltipPrefix,
  selectedDimension,
  icon,
  onLayerChangeDimension,
  valueMappings,
  setUseLatestReferenceTime,
  useLatestReferenceTime,
  latestLabel = 'Latest',
}) => {
  const dimensionValuesRaw = selectedDimension?.values
    ?.split(',')
    .slice(0, 100)
    .reverse();

  if (!selectedDimension?.name || !dimensionValuesRaw?.length) {
    return null;
  }

  const isReferenceTimeDimensionAndUseLatestIsAvailable =
    selectedDimension.name === 'reference_time' &&
    useLatestReferenceTime !== undefined;

  const setDimensionValue = (dimensionValue: string): void => {
    if (isReferenceTimeDimensionAndUseLatestIsAvailable) {
      if (dimensionValue === latestLabel) {
        setUseLatestReferenceTime?.(true);
        onLayerChangeDimension(
          selectedDimension.name!,
          dimensionValuesRaw[0],
          layerTypes.LayerActionOrigin.layerManager,
        );
        return;
      }
      setUseLatestReferenceTime?.(false);
    }
    onLayerChangeDimension(
      selectedDimension.name!,
      dimensionValue,
      layerTypes.LayerActionOrigin.layerManager,
    );
  };

  const dimensionValues = isReferenceTimeDimensionAndUseLatestIsAvailable
    ? [latestLabel, ...dimensionValuesRaw]
    : dimensionValuesRaw.slice(0, 100).reverse();
  const isLatest =
    isReferenceTimeDimensionAndUseLatestIsAvailable &&
    useLatestReferenceTime &&
    selectedDimension.maxValue === selectedDimension.currentValue;

  return (
    <TooltipSelect
      disableUnderline
      tooltip={`${tooltipPrefix}${selectedDimension.name} ${selectedDimension.currentValue} ${selectedDimension.units}`}
      isEnabled={isEnabled}
      value={isLatest ? latestLabel : selectedDimension.currentValue}
      list={dimensionValues.map((dimensionValue) => ({
        value: dimensionValue,
      }))}
      currentIndex={
        isLatest
          ? 0
          : dimensionValues.findIndex((dimensionValue) => {
              return dimensionValue === selectedDimension.currentValue;
            })
      }
      onChange={(event): void => {
        event.stopPropagation();
        setDimensionValue(event.target.value as string);
      }}
      onChangeMouseWheel={(event): void => {
        setDimensionValue(event.value);
      }}
      style={{
        maxWidth: '50%',
      }}
      IconComponent={icon}
    >
      <MenuItem disabled>
        Dimension value ({dimensionValues.length} options)
      </MenuItem>
      {dimensionValues.map((dimensionValue) => (
        <MenuItem key={dimensionValue} value={dimensionValue}>
          {valueMappings?.[selectedDimension.name!]?.(dimensionValue) || (
            <span style={{ whiteSpace: 'nowrap' }}>
              {dimensionValue}
              {dimensionValue !== latestLabel && (
                <i> {selectedDimension.units}</i>
              )}
            </span>
          )}
        </MenuItem>
      ))}
    </TooltipSelect>
  );
};
