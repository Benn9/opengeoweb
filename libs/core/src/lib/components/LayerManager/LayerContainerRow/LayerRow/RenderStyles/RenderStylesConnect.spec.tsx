/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { render, fireEvent, screen } from '@testing-library/react';
import { createMockStoreWithEggs } from '@opengeoweb/shared';
import { layerActions, layerTypes, storeTestUtils } from '@opengeoweb/store';
import { defaultReduxLayerRadarKNMI } from '../../../../../utils/defaultTestSettings';
import RenderStylesConnect from './RenderStylesConnect';
import { CoreThemeStoreProvider } from '../../../../Providers/Providers';

describe('src/components/LayerManager/LayerContainerRow/LayerRow/RenderStyles/RenderStylesConnect', () => {
  it('should set styles', async () => {
    const mapId = 'mapid_1';
    const layer = defaultReduxLayerRadarKNMI;
    const mockState = storeTestUtils.mockStateMapWithLayer(layer, mapId);
    const store = createMockStoreWithEggs(mockState);
    const props = {
      mapId: 'test',
      layerId: layer.id!,
    };

    render(
      <CoreThemeStoreProvider store={store}>
        <RenderStylesConnect {...props} />
      </CoreThemeStoreProvider>,
    );

    const select = screen.getByTestId('selectStyle');

    fireEvent.mouseDown(select);
    const newName = {
      value: 'precip/nearest',
      title: 'precip/nearest',
    };
    const menuItem = await screen.findByText(newName.title);

    fireEvent.click(menuItem);

    const expectedAction = [
      layerActions.layerChangeStyle({
        layerId: layer.id!,
        style: newName.value,
        mapId: props.mapId,
        origin: layerTypes.LayerActionOrigin.layerManager,
      }),
    ];

    expect(store.getActions()).toEqual(expectedAction);
  });
});
