/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { useDispatch, useSelector } from 'react-redux';

import {
  CoreAppStore,
  layerActions,
  layerSelectors,
  layerTypes,
  mapTypes,
} from '@opengeoweb/store';
import DimensionSelect from './DimensionSelect';

export interface DimensionSelectProps {
  layerId: string;
  mapId: string;
}

interface LayerChangeDimensionParams {
  dimension: mapTypes.Dimension;
  origin: layerTypes.LayerActionOrigin | undefined;
}

export const useLayerDimensions = (
  layerId: string,
  mapId: string,
): {
  layerDimensions: mapTypes.Dimension[];
  isLayerEnabled: boolean;
  layerChangeDimension: ({
    dimension,
    origin,
  }: LayerChangeDimensionParams) => void;
} => {
  const dispatch = useDispatch();

  const layerDimensions = useSelector((store: CoreAppStore) =>
    layerSelectors.getLayerNonTimeDimensions(store, layerId),
  );

  const isLayerEnabled = useSelector((store: CoreAppStore) =>
    layerSelectors.getLayerEnabled(store, layerId),
  );

  const layerChangeDimension = React.useCallback(
    ({ dimension, origin }: LayerChangeDimensionParams) =>
      dispatch(
        layerActions.layerChangeDimension({
          layerId,
          dimension,
          mapId,
          origin,
        }),
      ),
    [layerId, dispatch, mapId],
  );

  return { layerDimensions, isLayerEnabled, layerChangeDimension };
};

const DimensionSelectConnect: React.FC<DimensionSelectProps> = ({
  layerId,
  mapId,
}: DimensionSelectProps) => {
  const { layerDimensions, isLayerEnabled, layerChangeDimension } =
    useLayerDimensions(layerId, mapId);

  const useLatestReferenceTime = useSelector((store: CoreAppStore) =>
    layerSelectors.getUseLatestReferenceTime(store, layerId),
  );

  const dispatch = useDispatch();
  const setUseLatestReferenceTime = (useLatestReferenceTime: boolean): void => {
    dispatch(
      layerActions.setUseLatestReferenceTime({
        id: layerId,
        useLatestReferenceTime,
      }),
    );
  };

  if (!layerDimensions.length || !layerId) {
    return null;
  }

  return (
    <DimensionSelect
      layerDimensions={layerDimensions}
      onLayerChangeDimension={(
        dimensionName: string,
        dimensionValue: string,
        origin,
      ): void => {
        const dimension: mapTypes.Dimension = {
          name: dimensionName,
          currentValue: dimensionValue,
        };
        layerChangeDimension({
          dimension,
          origin,
        });
      }}
      isEnabled={isLayerEnabled}
      useLatestReferenceTime={useLatestReferenceTime}
      setUseLatestReferenceTime={setUseLatestReferenceTime}
    />
  );
};

export default DimensionSelectConnect;
