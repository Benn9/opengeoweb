/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { render, screen } from '@testing-library/react';
import { layerWithSingleDimensionValue } from '../../../../../utils/defaultTestSettings';
import { CoreThemeProvider } from '../../../../Providers/Providers';
import DimensionSelect from './DimensionSelect';

describe('src/components/LayerManager/LayerContainerRow/LayerRow/DimensionSelect/DimensionSelect', () => {
  it('should show custom dropdown icon', async () => {
    const TestIcon: React.FC = () => <span data-testid="testIcon" />;
    const mockProps = {
      layerId: layerWithSingleDimensionValue.id!,
      mapId: 'map_1',
      onLayerChangeDimension: jest.fn(),
      layerDimensions: [
        {
          name: 'reference_time',
          units: 'ISO8601',
          currentValue: '2020-03-13T14:40:00Z',
          values: '2020-03-13T14:40:00Z',
        },
      ],
      icon: TestIcon,
    };

    render(
      <CoreThemeProvider>
        <DimensionSelect {...mockProps} />
      </CoreThemeProvider>,
    );

    expect(screen.getAllByTestId('testIcon').length).toEqual(2);
  });

  it('should show custom name for dimension', async () => {
    const mockProps = {
      layerId: layerWithSingleDimensionValue.id!,
      mapId: 'map_1',
      onLayerChangeDimension: jest.fn(),
      layerDimensions: [
        {
          name: 'reference_time',
          units: 'ISO8601',
          currentValue: '2020-03-13T14:40:00Z',
          values: '2020-03-13T14:40:00Z',
        },
      ],
      nameMappings: { reference_time: 'TEST_NAME' },
    };

    render(
      <CoreThemeProvider>
        <DimensionSelect {...mockProps} />
      </CoreThemeProvider>,
    );

    expect(screen.getByText('TEST_NAME')).toBeTruthy();
  });

  it('should show custom formatting for dimension value', async () => {
    const mockProps = {
      layerId: layerWithSingleDimensionValue.id!,
      mapId: 'map_1',
      onLayerChangeDimension: jest.fn(),
      layerDimensions: [
        {
          name: 'reference_time',
          units: 'ISO8601',
          currentValue: '2020-03-13T14:40:00Z',
          values: '2020-03-13T14:40:00Z',
        },
      ],
      valueMappings: {
        reference_time: (value: string): string => `${value}_TEST`,
      },
    };

    render(
      <CoreThemeProvider>
        <DimensionSelect {...mockProps} />
      </CoreThemeProvider>,
    );

    expect(screen.getByText('2020-03-13T14:40:00Z_TEST')).toBeTruthy();
  });
});
