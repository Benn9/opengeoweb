/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import {
  fireEvent,
  render,
  waitFor,
  waitForElementToBeRemoved,
  screen,
} from '@testing-library/react';
import { createMockStoreWithEggs } from '@opengeoweb/shared';
import userEvent from '@testing-library/user-event';
import { storeTestUtils } from '@opengeoweb/store';
import AddLayersButton from './AddLayersButton';
import { defaultReduxLayerRadarColor } from '../../../utils/defaultTestSettings';
import {
  CoreThemeProvider,
  CoreThemeStoreProvider,
} from '../../Providers/Providers';

describe('src/components/LayerManager/AddLayersButton/AddLayersButton', () => {
  const props = {
    mapId: 'mapId_1',
    tooltip: 'Add layers',
  };
  const mapId = 'mapid_1';
  const layer = defaultReduxLayerRadarColor;
  const mockState = storeTestUtils.mockStateMapWithLayer(layer, mapId);
  const store = createMockStoreWithEggs(mockState);
  const user = userEvent.setup();

  it('should render correct component', async () => {
    render(
      <CoreThemeProvider>
        <AddLayersButton />
      </CoreThemeProvider>,
    );
    expect(screen.getByTestId('addLayersButton')).toBeTruthy();

    const tooltip = screen.queryByRole('tooltip');
    expect(tooltip).toBeFalsy();
  });

  it('should show a title text "Add layers" in tooltip when hovering', async () => {
    render(
      <CoreThemeStoreProvider store={store}>
        <AddLayersButton {...props} />
      </CoreThemeStoreProvider>,
    );
    const addLayersButton = screen.getByTestId('addLayersButton');
    expect(addLayersButton).toBeTruthy();
    await user.hover(addLayersButton);
    // Wait until tooltip appears
    const tooltip = await screen.findByRole('tooltip');
    expect(tooltip.textContent).toContain('Add layers');

    await user.unhover(addLayersButton);
    await waitForElementToBeRemoved(() => screen.queryByRole('tooltip'), {
      timeout: 3000,
    });
  });

  it('should show a title text "Add layers" in tooltip on focus', async () => {
    render(
      <CoreThemeStoreProvider store={store}>
        <AddLayersButton {...props} />
      </CoreThemeStoreProvider>,
    );
    const addLayersButton = screen.getByTestId('addLayersButton');
    expect(addLayersButton).toBeTruthy();
    fireEvent.focus(addLayersButton);
    // Wait until tooltip appears
    const tooltip = await screen.findByRole('tooltip');
    expect(tooltip.textContent).toContain('Add layers');

    fireEvent.blur(addLayersButton);
    await waitFor(() => {
      expect(screen.queryByRole('tooltip')).toBeFalsy();
    });
  });

  it('should open addLayersPopup on click', async () => {
    render(
      <CoreThemeStoreProvider store={store}>
        <AddLayersButton {...props} />
      </CoreThemeStoreProvider>,
    );
    expect(screen.queryByTestId('addLayersPopup')).toBeFalsy();
    fireEvent.click(screen.queryByTestId('addLayersButton')!);
    await screen.findByTestId('addLayersPopup');
  });

  it('should have autofocus', async () => {
    render(
      <CoreThemeStoreProvider store={store}>
        <AddLayersButton {...props} shouldFocus />
      </CoreThemeStoreProvider>,
    );
    expect(screen.queryByTestId('addLayersPopup')).toBeFalsy();

    await waitFor(() =>
      expect(
        screen.getByTestId('addLayersButton')!.matches(':active'),
      ).toBeTruthy(),
    );

    expect(
      screen
        .getByTestId('addLayersButton')!
        .classList.contains('Mui-focusVisible'),
    ).toBeTruthy();
  });
});
