/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { MapControls, publicLayers } from '@opengeoweb/webmap-react';
import { useDefaultMapSettings } from '../../../storybookUtils/defaultStorySettings';
import LayerManagerConnect from '../LayerManagerConnect';
import { MapViewConnect } from '../../MapView';
import { CoreThemeStoreProvider } from '../../Providers/Providers';
import { store } from '../../../storybookUtils/store';
import LayerManagerMapButtonConnect from '../LayerManagerMapButtonConnect';

export default { title: 'components/LayerManager' };

const ProjectionSelectStoryComponent = (): React.ReactElement => {
  const mapId1 = 'mapid_1';

  useDefaultMapSettings({
    mapId: mapId1,
    layers: [{ ...publicLayers.radarLayer, id: `radar-${mapId1}` }],
    baseLayers: [
      { ...publicLayers.defaultLayers.baseLayerGrey, id: `baseGrey-${mapId1}` },
    ],
  });

  return (
    <div style={{ display: 'flex' }}>
      <LayerManagerConnect />
      <div style={{ width: '100%', height: '100vh', position: 'relative' }}>
        <MapControls>
          <LayerManagerMapButtonConnect mapId={mapId1} />
        </MapControls>
        <MapViewConnect mapId={mapId1} />
      </div>
    </div>
  );
};

export const ProjectionSelectInMap = (): React.ReactElement => (
  <CoreThemeStoreProvider store={store}>
    <ProjectionSelectStoryComponent />
  </CoreThemeStoreProvider>
);
