/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { render, fireEvent, screen } from '@testing-library/react';
import { createMockStoreWithEggs } from '@opengeoweb/shared';
import { uiActions, uiTypes } from '@opengeoweb/store';
import LayerManagerMapButtonConnect from './LayerManagerMapButtonConnect';
import { CoreThemeStoreProvider } from '../Providers/Providers';

describe('src/components/LayerManager/LayerManagerMapButtonConnect', () => {
  it('should dispatch action with passed in mapid when clicked', () => {
    const mockState = {
      ui: {
        dialogs: {
          layerManager: {
            type: uiTypes.DialogTypes.LayerManager,
            activeMapId: 'map1',
            isOpen: false,
          },
        },
      },
    };
    const store = createMockStoreWithEggs(mockState);
    const props = {
      mapId: 'mapId_123',
    };
    render(
      <CoreThemeStoreProvider store={store}>
        <LayerManagerMapButtonConnect {...props} />
      </CoreThemeStoreProvider>,
    );

    // button should be present
    expect(screen.getByTestId('layerManagerButton')).toBeTruthy();

    // close the legend dialog
    fireEvent.click(screen.getByTestId('layerManagerButton'));
    const expectedAction = [
      uiActions.setActiveMapIdForDialog({
        type: uiTypes.DialogTypes.LayerManager,
        mapId: props.mapId,
        setOpen: true,
        source: 'app',
      }),
    ];

    expect(store.getActions()).toEqual(expectedAction);
  });

  it('should dispatch action to close dialog when dialog is already opened for corresponding map', () => {
    const mockState = {
      ui: {
        dialogs: {
          layerManager: {
            type: uiTypes.DialogTypes.LayerManager,
            activeMapId: 'mapId_123',
            isOpen: true,
            source: 'app',
          },
        },
      },
    };
    const store = createMockStoreWithEggs(mockState);
    const props = {
      mapId: 'mapId_123',
    };
    render(
      <CoreThemeStoreProvider store={store}>
        <LayerManagerMapButtonConnect {...props} />
      </CoreThemeStoreProvider>,
    );

    // button should be present
    expect(screen.getByTestId('layerManagerButton')).toBeTruthy();

    // close the legend dialog

    fireEvent.click(screen.getByTestId('layerManagerButton'));
    const expectedAction = [
      uiActions.setActiveMapIdForDialog({
        type: uiTypes.DialogTypes.LayerManager,
        mapId: props.mapId,
        setOpen: false,
        source: 'app',
      }),
    ];

    expect(store.getActions()).toEqual(expectedAction);
  });

  it('should dispatch action with correct mapId when clicked with isMultiMap', () => {
    const mapId1 = 'mapId123';
    const mapId2 = 'mapId1234';
    const mockState = {
      ui: {
        dialogs: {
          layerSelect: {
            type: `${uiTypes.DialogTypes.LayerSelect}-${mapId2}`,
            activeMapId: 'map1',
            isOpen: false,
          },
        },
      },
    };
    const store = createMockStoreWithEggs(mockState);

    render(
      <>
        <CoreThemeStoreProvider store={store}>
          <LayerManagerMapButtonConnect mapId={mapId1} isMultiMap />
        </CoreThemeStoreProvider>
        <CoreThemeStoreProvider store={store}>
          <LayerManagerMapButtonConnect mapId={mapId2} isMultiMap />
        </CoreThemeStoreProvider>
      </>,
    );

    // button should be present
    for (const button of screen.getAllByTestId('layerManagerButton')) {
      expect(button).toBeTruthy();
      // open the legend dialog
      fireEvent.click(button);
    }

    const expectedAction = [
      uiActions.setActiveMapIdForDialog({
        type: `${uiTypes.DialogTypes.LayerManager}-${mapId1}`,
        mapId: mapId1,
        setOpen: true,
        source: 'app',
      }),
      uiActions.setActiveMapIdForDialog({
        type: `${uiTypes.DialogTypes.LayerManager}-${mapId2}`,
        mapId: mapId2,
        setOpen: true,
        source: 'app',
      }),
    ];

    expect(store.getActions()).toEqual(expectedAction);
  });
});
