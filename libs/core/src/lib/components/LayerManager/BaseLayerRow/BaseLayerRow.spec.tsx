/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { render, screen } from '@testing-library/react';
import { createMockStoreWithEggs } from '@opengeoweb/shared';
import { storeTestUtils } from '@opengeoweb/store';
import BaseLayerRow from './BaseLayerRow';
import { CoreThemeStoreProvider } from '../../Providers/Providers';

const { mockStateMapWithAnimationDelayWithoutLayers } = storeTestUtils;
describe('src/components/LayerManager/BaseLayerRow/BaseLayerRow', () => {
  const props = {
    mapId: 'mapId_1',
  };

  it('should render correct component', () => {
    const mockState = mockStateMapWithAnimationDelayWithoutLayers(props.mapId);
    const store = createMockStoreWithEggs(mockState);

    render(
      <CoreThemeStoreProvider store={store}>
        <BaseLayerRow {...props} />
      </CoreThemeStoreProvider>,
    );
    expect(screen.getByTestId('baseLayerRow')).toBeTruthy();
  });
  it('should render custom title text if given', () => {
    const mockState = mockStateMapWithAnimationDelayWithoutLayers(props.mapId);
    const store = createMockStoreWithEggs(mockState);

    const settings = {
      baseLayerRow: {
        title: 'Test title',
      },
    };
    render(
      <CoreThemeStoreProvider store={store}>
        <BaseLayerRow {...props} settings={settings} />
      </CoreThemeStoreProvider>,
    );
    expect(screen.getByText(settings.baseLayerRow.title)).toBeTruthy();
  });
  it('should render AddLayersButton ', () => {
    const mockState = mockStateMapWithAnimationDelayWithoutLayers(props.mapId);
    const store = createMockStoreWithEggs(mockState);

    render(
      <CoreThemeStoreProvider store={store}>
        <BaseLayerRow {...props} />
      </CoreThemeStoreProvider>,
    );
    expect(screen.getByTestId('addLayersButton')).toBeTruthy();
  });
  it('should not render AddLayersButton when disabled', () => {
    const mockState = mockStateMapWithAnimationDelayWithoutLayers(props.mapId);
    const store = createMockStoreWithEggs(mockState);
    const settings = {
      baseLayerRow: {
        addLayersButton: {
          isDisabled: true,
        },
      },
    };

    render(
      <CoreThemeStoreProvider store={store}>
        <BaseLayerRow {...props} settings={settings} />
      </CoreThemeStoreProvider>,
    );
    expect(screen.queryByTestId('addLayersButton')).toBeFalsy();
  });
});
