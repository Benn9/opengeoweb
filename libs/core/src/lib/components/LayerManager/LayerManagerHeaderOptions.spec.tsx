/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { fireEvent, render, screen } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import LayerManagerHeaderOptions, {
  sizeLarge,
  sizeMedium,
  sizeSmall,
} from './LayerManagerHeaderOptions';
import { CoreThemeProvider } from '../Providers/Providers';

describe('src/components/LayerManager/LayerManagerHeaderOptions', () => {
  const user = userEvent.setup();

  it('should render undocked button', async () => {
    const props = {
      isDockedLayerManager: false,
      shortcutsEnabled: true,
      onClickDockButton: jest.fn(),
      onChangeSize: jest.fn(),
    };
    render(
      <CoreThemeProvider>
        <LayerManagerHeaderOptions {...props} />
      </CoreThemeProvider>,
    );

    const dockButton = screen.queryByTestId('dockedBtn') as HTMLElement;
    await user.hover(dockButton);

    expect(await screen.findByText('Dock')).toBeTruthy();
    expect(screen.getByTestId('dockedLayerManager-uncollapse')).toBeTruthy();

    fireEvent.click(dockButton);
    expect(props.onClickDockButton).toHaveBeenCalled();
  });

  it('should render docked button', async () => {
    const props = {
      isDockedLayerManager: true,
      shortcutsEnabled: true,
      onClickDockButton: jest.fn(),
      onChangeSize: jest.fn(),
    };
    render(
      <CoreThemeProvider>
        <LayerManagerHeaderOptions {...props} />
      </CoreThemeProvider>,
    );

    const dockButton = screen.queryByTestId('dockedBtn') as HTMLElement;
    await user.hover(dockButton);

    expect(await screen.findByText('Undock')).toBeTruthy();
    expect(screen.getByTestId('dockedLayerManager-collapse')).toBeTruthy();

    fireEvent.click(dockButton);
    expect(props.onClickDockButton).toHaveBeenCalled();
  });

  it('should handle correct callback for different sizes', () => {
    const props = {
      isDockedLayerManager: true,
      shortcutsEnabled: true,
      onClickDockButton: jest.fn(),
      onChangeSize: jest.fn(),
    };
    render(
      <CoreThemeProvider>
        <LayerManagerHeaderOptions {...props} />
      </CoreThemeProvider>,
    );

    // small
    const smallSizeButton = screen.queryByTestId(
      'collapseSmall-btn',
    ) as HTMLElement;
    fireEvent.click(smallSizeButton);
    expect(props.onChangeSize).toHaveBeenCalledWith(sizeSmall);
    // medium
    const mediumSizeButton = screen.queryByTestId(
      'collapseMedium-btn',
    ) as HTMLElement;
    fireEvent.click(mediumSizeButton);
    expect(props.onChangeSize).toHaveBeenCalledWith(sizeMedium);
    // large
    const largeSizeButton = screen.queryByTestId(
      'collapseLarge-btn',
    ) as HTMLElement;
    fireEvent.click(largeSizeButton);
    expect(props.onChangeSize).toHaveBeenCalledWith(sizeLarge);
  });

  it('should handle correct callback for different sizes by keyboard controls', async () => {
    const props = {
      isDockedLayerManager: false,
      shortcutsEnabled: true,
      onClickDockButton: jest.fn(),
      onChangeSize: jest.fn(),
    };
    render(
      <CoreThemeProvider>
        <LayerManagerHeaderOptions {...props} />
      </CoreThemeProvider>,
    );

    // small
    await user.keyboard('{Control>}{Alt>}s');
    expect(props.onChangeSize).toHaveBeenCalledWith(sizeSmall);
    // medium
    await user.keyboard('{Control>}{Alt>}m');
    expect(props.onChangeSize).toHaveBeenCalledWith(sizeMedium);
    // large
    await user.keyboard('{Control>}{Alt>}l');
    expect(props.onChangeSize).toHaveBeenCalledWith(sizeLarge);
  });

  it('callback should not be called using keyboard shortcuts when they are disabled', async () => {
    const props = {
      isDockedLayerManager: false,
      shortcutsEnabled: false,
      onClickDockButton: jest.fn(),
      onChangeSize: jest.fn(),
    };
    render(
      <CoreThemeProvider>
        <LayerManagerHeaderOptions {...props} />
      </CoreThemeProvider>,
    );

    await user.keyboard('{Control>}{Alt>}s');
    expect(props.onChangeSize).not.toHaveBeenCalled();
  });

  it('should disable size and docking buttons', () => {
    const props = {
      isDockedLayerManager: false,
      shortcutsEnabled: false,
      onClickDockButton: jest.fn(),
      onChangeSize: jest.fn(),
    };

    const buttonSettings = {
      small: { isDisabled: true },
      medium: { isDisabled: true },
      large: { isDisabled: true },
      dock: { isDisabled: true },
    };

    render(
      <CoreThemeProvider>
        <LayerManagerHeaderOptions {...props} buttonSettings={buttonSettings} />
      </CoreThemeProvider>,
    );

    expect(screen.queryByTestId('collapseSmall-btn')).toBeFalsy();
    expect(screen.queryByTestId('collapseMedium-btn')).toBeFalsy();
    expect(screen.queryByTestId('collapseLarge-btn')).toBeFalsy();
    expect(screen.queryByTestId('dockedBtn')).toBeFalsy();
  });

  it('should show custom icons and divider', () => {
    const props = {
      isDockedLayerManager: false,
      shortcutsEnabled: false,
      onClickDockButton: jest.fn(),
      onChangeSize: jest.fn(),
    };

    const buttonSettings = {
      small: { icon: <span data-testid="smallTestIcon" /> },
      medium: { icon: <span data-testid="mediumTestIcon" /> },
      large: { icon: <span data-testid="largeTestIcon" /> },
      divider: { element: <span data-testid="testDivider" /> },
    };

    render(
      <CoreThemeProvider>
        <LayerManagerHeaderOptions {...props} buttonSettings={buttonSettings} />
      </CoreThemeProvider>,
    );

    expect(screen.getByTestId('smallTestIcon')).toBeTruthy();
    expect(screen.getByTestId('mediumTestIcon')).toBeTruthy();
    expect(screen.getByTestId('largeTestIcon')).toBeTruthy();
    expect(screen.getByTestId('testDivider')).toBeTruthy();
  });
});
