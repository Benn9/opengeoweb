/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */

import {
  act,
  fireEvent,
  render,
  waitFor,
  screen,
} from '@testing-library/react';
import { createMockStoreWithEggs } from '@opengeoweb/shared';
import { produce } from 'immer';
import userEvent from '@testing-library/user-event';
import React from 'react';

import {
  mapActions,
  genericActions,
  WebMapStateModuleState,
  uiTypes,
  mapEnums,
  syncConstants,
  storeTestUtils,
} from '@opengeoweb/store';
import { webmapUtils } from '@opengeoweb/webmap';
import {
  defaultReduxLayerRadarKNMI,
  WmdefaultReduxLayerRadarKNMI,
} from '../../utils/defaultTestSettings';
import { baseLayerGrey, overLayer } from '../../utils/testLayers';
import { CoreThemeStoreProvider } from '../Providers/Providers';
import MapViewConnect from './MapViewConnect';

describe('src/components/MapView/MapViewConnect', () => {
  it('should zoom and pan the map using keyboard', async () => {
    jest.useFakeTimers();

    const user = userEvent.setup({ advanceTimers: jest.advanceTimersByTime });

    const mapId = 'map-1';
    const layers = [defaultReduxLayerRadarKNMI, baseLayerGrey, overLayer];
    webmapUtils.registerWMLayer(
      WmdefaultReduxLayerRadarKNMI,
      defaultReduxLayerRadarKNMI.id!,
    );
    const mockState: WebMapStateModuleState & uiTypes.UIModuleState = {
      ...storeTestUtils.mockStateMapWithMultipleLayers(layers, mapId),
      ui: {
        order: [],
        dialogs: {},
        activeWindowId: mapId,
      },
    };
    const store = createMockStoreWithEggs(mockState);

    render(
      <CoreThemeStoreProvider store={store}>
        <MapViewConnect mapId={mapId} />
      </CoreThemeStoreProvider>,
    );

    await waitFor(() => {
      expect(screen.getByTestId('zoom-reset')).toBeTruthy();
    });
    expect(screen.getByTestId('zoom-in')).toBeTruthy();
    expect(screen.getByTestId('zoom-out')).toBeTruthy();

    const expectedActionA = mapActions.registerMap({ mapId });
    const expectedActionB = genericActions.syncGroupAddSource({
      id: mapId,
      type: [
        syncConstants.SYNCGROUPS_TYPE_SETTIME,
        syncConstants.SYNCGROUPS_TYPE_SETBBOX,
      ],
    });

    await waitFor(() => {
      expect(store.getActions()).toContainEqual(expectedActionA);
    });
    expect(store.getActions()).toContainEqual(expectedActionB);

    /* Now check the bbox action */
    fireEvent.click(screen.getByTestId('zoom-out'));

    await act(async () => jest.advanceTimersToNextTimer());

    const zoomOutButtonAction = genericActions.setBbox({
      bbox: {
        bottom: -25333333.333333332,
        left: -25333333.333333332,
        right: 25333333.333333332,
        top: 25333333.333333332,
      },
      sourceId: mapId,
      srs: 'EPSG:3857',
      mapId,
      origin: mapEnums.MapActionOrigin.map,
    });
    await waitFor(() =>
      expect(store.getActions()).toContainEqual(zoomOutButtonAction),
    );

    await user.keyboard('-');

    await act(async () => jest.advanceTimersToNextTimer());

    const zoomOutKeyboardAction = produce(zoomOutButtonAction, (draft) => {
      draft.payload.bbox = {
        bottom: -33777777.777777776,
        left: -33777777.777777776,
        right: 33777777.777777776,
        top: 33777777.777777776,
      };
    });

    await waitFor(() =>
      expect(store.getActions()).toContainEqual(zoomOutKeyboardAction),
    );

    store.clearActions();
    expect(store.getActions().length).toEqual(0);

    await user.keyboard('{ArrowLeft}');
    await act(async () => jest.advanceTimersToNextTimer());

    // If control key is pressed then ignore keypress
    await user.keyboard('{Control>}{ArrowLeft}');
    await act(async () => jest.advanceTimersToNextTimer());

    const arrowLeftKeyboardAction = produce(zoomOutKeyboardAction, (draft) => {
      draft.payload.bbox.left = -35128888.88888889;
      draft.payload.bbox.right = 32426666.666666664;
    });

    await waitFor(() =>
      // there should only be one action
      expect(store.getActions()).toEqual([arrowLeftKeyboardAction]),
    );

    jest.clearAllTimers();
    jest.useRealTimers();
  });
});
