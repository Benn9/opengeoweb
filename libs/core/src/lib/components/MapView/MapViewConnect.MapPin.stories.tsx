/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { Paper, Card, CardContent, Typography } from '@mui/material';
import { CustomToggleButton } from '@opengeoweb/shared';
import {
  CoreAppStore,
  mapSelectors,
  mapStoreActions,
  mapTypes,
} from '@opengeoweb/store';
import { MapLocation, MapPinLocationPayload } from '@opengeoweb/webmap-react';
import { store } from '../../storybookUtils/store';
import { MapViewConnect } from '.';
import { CoreThemeStoreProvider } from '../Providers/Providers';
import { useDefaultMapSettings } from '../../storybookUtils/defaultStorySettings';

export default {
  title: 'components/MapView/MapViewConnect',
  component: MapViewConnect,
};

interface SetMapPinProps {
  mapId: string;
  disablePin?: boolean;
}

const SetMapPin: React.FC<SetMapPinProps> = ({
  mapId,
  disablePin = false,
}: SetMapPinProps) => {
  const mapPinLocation: MapLocation | undefined = useSelector(
    (store: CoreAppStore) => mapSelectors.getPinLocation(store, mapId),
  );
  useDefaultMapSettings({ mapId, layers: [], baseLayers: [] });
  const dispatch = useDispatch();

  const mapPinChangeLocation = React.useCallback(
    (payload: MapPinLocationPayload): void => {
      dispatch(mapStoreActions.setMapPinLocation(payload));
    },
    [dispatch],
  );

  const toggleMapPinIsVisible = React.useCallback(
    (payload: mapTypes.ToggleMapPinIsVisiblePayload): void => {
      dispatch(mapStoreActions.toggleMapPinIsVisible(payload));
    },
    [dispatch],
  );

  const setMapPinDisabled = React.useCallback(
    (payload: mapTypes.DisableMapPinPayload): void => {
      dispatch(mapStoreActions.setDisableMapPin(payload));
    },
    [dispatch],
  );

  React.useEffect(() => {
    toggleMapPinIsVisible({ mapId, displayMapPin: true });
    mapPinChangeLocation({ mapId, mapPinLocation: { lat: 52.0, lon: 5.0 } });
    setMapPinDisabled({ mapId, disableMapPin: disablePin });
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <div>
      <div style={{ height: '100vh' }}>
        <MapViewConnect mapId={mapId} />
      </div>
      <div
        style={{
          position: 'absolute',
          left: '50px',
          top: '10px',
          zIndex: 10000,
        }}
      >
        <Paper>
          <Card>
            <CardContent>
              <Typography variant="subtitle1">
                Position of map cursor:
              </Typography>
              <Typography variant="body2">
                {`Lon: ${
                  mapPinLocation
                    ? mapPinLocation.lon.toFixed(2)
                    : 'click on map'
                }`}
                <br />
                {`Lat: ${
                  mapPinLocation
                    ? mapPinLocation.lat.toFixed(2)
                    : 'click on map'
                }`}
                <br />
              </Typography>
            </CardContent>
          </Card>
        </Paper>
      </div>
    </div>
  );
};

interface ToggleMapPinProps {
  mapId: string;
}

const ToggleMapPin: React.FC<ToggleMapPinProps> = ({
  mapId,
}: ToggleMapPinProps) => {
  useDefaultMapSettings({ mapId });
  const dispatch = useDispatch();
  const isMapPinVisible: boolean = useSelector(
    (store: CoreAppStore) => mapSelectors.getDisplayMapPin(store, mapId)!,
  );
  const toggleMapPinIsVisible = React.useCallback(
    (displayMapPin: boolean): void => {
      dispatch(mapStoreActions.toggleMapPinIsVisible({ mapId, displayMapPin }));
    },
    [dispatch, mapId],
  );

  React.useEffect(() => {
    dispatch(
      mapStoreActions.setMapPinLocation({
        mapId,
        mapPinLocation: { lat: 52.0, lon: 5.0 },
      }),
    );
    toggleMapPinIsVisible(true);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const onClick = (): void => {
    toggleMapPinIsVisible(!isMapPinVisible);
  };

  return (
    <div>
      <div style={{ height: '100vh' }}>
        <MapViewConnect mapId={mapId} />
      </div>
      <div
        style={{
          position: 'absolute',
          left: '50px',
          top: '10px',
          zIndex: 10000,
        }}
      >
        <Paper>
          <Card>
            <CardContent>
              <Typography variant="subtitle1">
                Press button to toggle map pin
              </Typography>
              <CustomToggleButton onClick={onClick}>
                Toggle map pin
              </CustomToggleButton>
            </CardContent>
          </Card>
        </Paper>
      </div>
    </div>
  );
};

export const SetMapPinAction = (): React.ReactElement => (
  <CoreThemeStoreProvider store={store}>
    <SetMapPin mapId="mapid_1" />
  </CoreThemeStoreProvider>
);

SetMapPinAction.storyName = 'Set Map Pin (takeSnapshot)';

export const SetMapPinActionDisabled = (): React.ReactElement => (
  <CoreThemeStoreProvider store={store}>
    <SetMapPin mapId="mapid_2" disablePin />
  </CoreThemeStoreProvider>
);

SetMapPinActionDisabled.storyName = 'Set Map Pin Disabled (takeSnapshot)';

export const ToggleMapPinAction = (): React.ReactElement => (
  <CoreThemeStoreProvider store={store}>
    <ToggleMapPin mapId="mapid_3" />
  </CoreThemeStoreProvider>
);
