/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { useDispatch, useSelector } from 'react-redux';

import {
  layerActions,
  mapActions,
  mapSelectors,
  genericActions,
  layerTypes,
  mapEnums,
  genericTypes,
  CoreAppStore,
  syncConstants,
  uiSelectors,
  syncGroupsTypes,
  drawtoolSelectors,
} from '@opengeoweb/store';
import { handleMomentISOString, webmapUtils } from '@opengeoweb/webmap';

import {
  MapPinLocationPayload,
  MapView,
  MapViewLayer,
  SetMapDimensionPayload,
  UpdateLayerInfoPayload,
} from '@opengeoweb/webmap-react';

import { useTouchZoomPan } from './useTouchZoomPan';
import { useKeyboardZoomAndPan } from './useKeyboardZoomAndPan';

export interface MapViewConnectProps {
  mapId: string;
  displayTimeInMap?: boolean;
  controls?: {
    zoomControls?: boolean;
  };
  showScaleBar?: boolean;
  children?: React.ReactNode;
  showLayerInfo?: boolean;
  passiveMap?: boolean;
}

export const ORIGIN_REACTMAPVIEWCONNECT_ONMAPCHANGEDIMENSION =
  'ORIGIN_REACTMAPVIEWCONNECT_ONMAPCHANGEDIMENSION';

export const ORIGIN_REACTMAPVIEWCONNECT_ONUPDATELAYERINFO =
  'ORIGIN_REACTMAPVIEWCONNECT_ONUPDATELAYERINFO';

/**
 * Connected component used to display the map and selected layers.
 * Includes options to disable the map controls and legend.
 *
 * Expects the following props:
 * @param {string} mapId mapId: string - Id of the map
 * @param {object} [controls.zoomControls] **optional** controls: object - toggle the map controls, zoomControls defaults to true
 * @param {boolean} [displayTimeInMap] **optional** displayTimeInMap: boolean, toggles the mapTime, defaults to true
 * @param {boolean} [showScaleBar] **optional** showScaleBar: boolean, toggles the scaleBar, defaults to true
 * @example
 * ```<MapViewConnect mapId={mapId} controls={{ zoomControls: false }} displayTimeInMap={false} showScaleBar={false}/>```
 */
const MapViewConnect: React.FC<MapViewConnectProps> = ({
  mapId,
  children,
  ...props
}: MapViewConnectProps) => {
  const mapDimensions = useSelector((store: CoreAppStore) =>
    mapSelectors.getMapDimensions(store, mapId),
  );
  const layers = useSelector((store: CoreAppStore) =>
    mapSelectors.getMapLayers(store, mapId),
  );
  const baseLayers = useSelector((store: CoreAppStore) =>
    mapSelectors.getMapBaseLayers(store, mapId),
  );
  const overLayers = useSelector((store: CoreAppStore) =>
    mapSelectors.getMapOverLayers(store, mapId),
  );
  const featureLayers = useSelector((store: CoreAppStore) =>
    mapSelectors.getMapFeatureLayers(store, mapId),
  );
  const bbox = useSelector((store: CoreAppStore) =>
    mapSelectors.getBbox(store, mapId),
  );
  const srs = useSelector((store: CoreAppStore) =>
    mapSelectors.getSrs(store, mapId),
  );
  const activeLayerId = useSelector((store: CoreAppStore) =>
    mapSelectors.getActiveLayerId(store, mapId),
  );
  const animationDelay = useSelector((store: CoreAppStore) =>
    mapSelectors.getMapAnimationDelay(store, mapId),
  );
  const mapPinLocation = useSelector((store: CoreAppStore) =>
    mapSelectors.getPinLocation(store, mapId),
  );
  const disableMapPin = useSelector((store: CoreAppStore) =>
    mapSelectors.getDisableMapPin(store, mapId),
  );

  const displayMapPin = useSelector((store: CoreAppStore) =>
    mapSelectors.getDisplayMapPin(store, mapId),
  );
  const timestep = useSelector((store: CoreAppStore) =>
    mapSelectors.getMapTimeStep(store, mapId),
  );

  // draw tool selectors
  const activeDrawToolId = useSelector((store: CoreAppStore) =>
    drawtoolSelectors.getActiveDrawToolId(store),
  );
  const shouldAllowMultipleDrawShapes = useSelector((store: CoreAppStore) =>
    drawtoolSelectors.getShouldAllowMultipleShapes(store, activeDrawToolId),
  );
  const geoJSONIntersectionLayerId = useSelector((store: CoreAppStore) =>
    drawtoolSelectors.getGeoJSONIntersectionLayerId(store, activeDrawToolId),
  );
  const geoJSONIntersectionBoundsLayerId = useSelector((store: CoreAppStore) =>
    drawtoolSelectors.getGeoJSONIntersectionBoundsLayerId(
      store,
      activeDrawToolId,
    ),
  );

  const dispatch = useDispatch();

  const mapChangeDimension = React.useCallback(
    (mapDimensionPayload: SetMapDimensionPayload): void => {
      if (mapDimensionPayload.dimension) {
        const wmjsMap = webmapUtils.getWMJSMapById(mapId);
        const dimension = wmjsMap.getDimension(
          mapDimensionPayload.dimension.name!,
        );
        if (
          dimension &&
          dimension.currentValue === mapDimensionPayload.dimension.currentValue
        ) {
          return;
        }
      }
      dispatch(mapActions.mapChangeDimension(mapDimensionPayload));
    },
    [dispatch, mapId],
  );

  const setTime = React.useCallback(
    (setTimePayload: genericTypes.SetTimePayload): void => {
      const wmjsMap = webmapUtils.getWMJSMapById(mapId);
      /* Check if the map not already has this value set, otherwise this component will re-render */
      if (
        wmjsMap &&
        wmjsMap.getDimension('time') &&
        wmjsMap.getDimension('time').currentValue === setTimePayload.value
      ) {
        return;
      }
      dispatch(genericActions.setTime(setTimePayload));
    },
    [dispatch, mapId],
  );

  const updateLayerInformation = React.useCallback(
    (payload: UpdateLayerInfoPayload): void => {
      dispatch(layerActions.onUpdateLayerInformation(payload));
    },
    [dispatch],
  );

  const registerMap = React.useCallback(
    (payload: { mapId: string }): void => {
      dispatch(mapActions.registerMap(payload));
    },
    [dispatch],
  );
  const unregisterMap = React.useCallback(
    (payload: { mapId: string }): void => {
      dispatch(mapActions.unregisterMap(payload));
    },
    [dispatch],
  );
  const genericSetBbox = React.useCallback(
    (payload: genericTypes.SetBboxPayload): void => {
      dispatch(genericActions.setBbox(payload));
    },
    [dispatch],
  );
  const syncGroupAddSource = React.useCallback(
    (payload: syncGroupsTypes.SyncGroupsAddSourcePayload): void => {
      dispatch(genericActions.syncGroupAddSource(payload));
    },
    [dispatch],
  );
  const syncGroupRemoveSource = React.useCallback(
    (payload: syncGroupsTypes.SyncGroupRemoveSourcePayload): void => {
      dispatch(genericActions.syncGroupRemoveSource(payload));
    },
    [dispatch],
  );
  const layerError = React.useCallback(
    (payload: layerTypes.ErrorLayerPayload): void => {
      dispatch(layerActions.layerError(payload));
    },
    [dispatch],
  );
  const mapPinChangeLocation = React.useCallback(
    (payload: MapPinLocationPayload): void => {
      dispatch(mapActions.setMapPinLocation(payload));
    },
    [dispatch],
  );
  const setSelectedFeature = React.useCallback(
    (payload: layerTypes.SetSelectedFeaturePayload): void => {
      dispatch(layerActions.setSelectedFeature(payload));
    },
    [dispatch],
  );

  const updateFeature = React.useCallback(
    (payload: layerTypes.UpdateFeaturePayload): void => {
      dispatch(layerActions.updateFeature(payload));
    },
    [dispatch],
  );

  const exitFeatureDrawMode = React.useCallback(
    (payload: layerTypes.ExitFeatureDrawModePayload): void => {
      dispatch(layerActions.exitFeatureDrawMode(payload));
    },
    [dispatch],
  );

  const activeWindowId = useSelector(uiSelectors.getActiveWindowId);

  const isActiveWindowId = (): boolean => {
    return activeWindowId === mapId;
  };

  useKeyboardZoomAndPan(isActiveWindowId(), mapId);
  useTouchZoomPan(isActiveWindowId(), mapId);

  React.useEffect(() => {
    registerMap({ mapId });
    syncGroupAddSource({
      id: mapId,
      type: [
        syncConstants.SYNCGROUPS_TYPE_SETTIME,
        syncConstants.SYNCGROUPS_TYPE_SETBBOX,
      ],
    });

    return (): void => {
      unregisterMap({ mapId });
      syncGroupRemoveSource({
        id: mapId,
      });
    };
  }, [
    mapId,
    registerMap,
    syncGroupAddSource,
    syncGroupRemoveSource,
    unregisterMap,
  ]);

  return (
    <div style={{ height: '100%' }}>
      <MapView
        {...props}
        mapId={mapId}
        srs={srs}
        bbox={bbox}
        mapPinLocation={displayMapPin ? mapPinLocation : undefined}
        dimensions={mapDimensions}
        activeLayerId={activeLayerId}
        animationDelay={animationDelay}
        timestep={timestep}
        displayMapPin={displayMapPin}
        disableMapPin={disableMapPin}
        onMapChangeDimension={(
          mapDimensionPayload: SetMapDimensionPayload,
        ): void => {
          if (
            mapDimensionPayload &&
            mapDimensionPayload.dimension &&
            mapDimensionPayload.dimension.name &&
            mapDimensionPayload.dimension.name === 'time'
          ) {
            setTime({
              sourceId: mapId,
              origin: `${mapDimensionPayload.origin}==> ${ORIGIN_REACTMAPVIEWCONNECT_ONMAPCHANGEDIMENSION}`,
              value: handleMomentISOString(
                mapDimensionPayload.dimension.currentValue,
              ),
            } as genericTypes.SetTimePayload);
          } else {
            mapChangeDimension(mapDimensionPayload);
          }
        }}
        onUpdateLayerInformation={updateLayerInformation}
        onMapPinChangeLocation={mapPinChangeLocation}
        onMapZoomEnd={(a): void => {
          genericSetBbox({
            bbox: a.bbox,
            srs: a.srs!,
            sourceId: mapId,
            origin: mapEnums.MapActionOrigin.map,
            mapId,
          });
        }}
      >
        {baseLayers.map((layer) => (
          <MapViewLayer
            id={`baselayer-${layer.id}`}
            key={layer.id}
            onLayerError={(_, error): void => {
              layerError({ layerId: layer.id!, error: Error(`${error}`) });
            }}
            {...layer}
          />
        ))}
        {layers.map((layer: layerTypes.Layer) => (
          <MapViewLayer
            id={`layer-${layer.id}`}
            key={layer.id}
            onLayerError={(_, error): void => {
              layerError({ layerId: layer.id!, error: Error(`${error}`) });
            }}
            {...layer}
          />
        ))}
        {featureLayers.map((layer: layerTypes.Layer) => (
          <MapViewLayer
            id={`featurelayer-${layer.id}`}
            key={layer.id}
            onLayerError={(_, error): void => {
              layerError({ layerId: layer.id!, error: Error(`${error}`) });
            }}
            onClickFeature={(event): void => {
              const isClickOutsideFeature = !event;
              if (isClickOutsideFeature && !layer.isInEditMode) {
                setSelectedFeature({
                  layerId: layer.id!,
                  selectedFeatureIndex: undefined,
                });
                return;
              }
              if (event?.isInEditMode === false) {
                setSelectedFeature({
                  layerId: layer.id!,
                  selectedFeatureIndex: event.featureIndex,
                });
              }
            }}
            updateGeojson={(
              geojson: GeoJSON.FeatureCollection,
              reason: string,
            ): void => {
              updateFeature({
                geojson,
                layerId: layer.id!,
                reason,
                shouldAllowMultipleShapes: shouldAllowMultipleDrawShapes,
                geoJSONIntersectionLayerId,
                geoJSONIntersectionBoundsLayerId,
              });
            }}
            exitDrawModeCallback={(reason: string): void =>
              exitFeatureDrawMode({
                reason,
                layerId: layer.id!,
                shouldAllowMultipleShapes: shouldAllowMultipleDrawShapes,
              })
            }
            {...layer}
          />
        ))}
        {overLayers.map((layer) => (
          <MapViewLayer
            id={`baselayer-${layer.id}`}
            key={layer.id}
            onLayerError={(_, error): void => {
              layerError({ layerId: layer.id!, error: Error(`${error}`) });
            }}
            {...layer}
          />
        ))}
        {children}
      </MapView>
    </div>
  );
};

export default MapViewConnect;
