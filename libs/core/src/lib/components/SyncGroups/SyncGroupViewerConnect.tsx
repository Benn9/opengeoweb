/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { useSelector, useDispatch } from 'react-redux';
import {
  uiTypes,
  syncGroupsSelectors,
  syncGroupsTypes,
  genericActions,
  useSetupDialog,
} from '@opengeoweb/store';
import { SyncGroupViewer } from './SyncGroupViewer';

export const SyncGroupViewerConnect: React.FC = () => {
  const dispatch = useDispatch();

  // TODO
  // Improve the selector to prevent re-renders
  // For example dusting anination syncgroups should not re-render
  const viewState = useSelector(syncGroupsSelectors.syncGroupGetViewState);

  const syncGroupAddGroup = React.useCallback(
    (payload: syncGroupsTypes.SyncGroupAddGroupPayload): void => {
      dispatch(genericActions.syncGroupAddGroup(payload));
    },
    [dispatch],
  );

  const syncGroupRemoveGroup = React.useCallback(
    (payload: syncGroupsTypes.SyncGroupRemoveGroupPayload): void => {
      dispatch(genericActions.syncGroupRemoveGroup(payload));
    },
    [dispatch],
  );

  const syncGroupAddTarget = React.useCallback(
    (payload: syncGroupsTypes.SyncGroupAddTargetPayload): void => {
      dispatch(genericActions.syncGroupAddTarget(payload));
    },
    [dispatch],
  );

  const syncGroupRemoveTarget = React.useCallback(
    (payload: syncGroupsTypes.SyncGroupRemoveTargetPayload): void => {
      dispatch(genericActions.syncGroupRemoveTarget(payload));
    },
    [dispatch],
  );

  const { dialogOrder, isDialogOpen, onCloseDialog, setDialogOrder } =
    useSetupDialog(uiTypes.DialogTypes.SyncGroups);

  return (
    <SyncGroupViewer
      onClose={onCloseDialog}
      isOpen={isDialogOpen}
      syncGroupViewState={viewState}
      syncGroupAddTarget={syncGroupAddTarget}
      syncGroupRemoveTarget={syncGroupRemoveTarget}
      syncGroupAddGroup={syncGroupAddGroup}
      syncGroupRemoveGroup={syncGroupRemoveGroup}
      order={dialogOrder}
      onMouseDown={setDialogOrder}
    />
  );
};
