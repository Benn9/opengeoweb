/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { render, screen } from '@testing-library/react';

import { lightTheme } from '@opengeoweb/theme';
import { CoreThemeProvider } from '../../Providers/Providers';
import TimeSliderLiteBackgroundSvg from './TimeSliderLiteBackgroundSvg';

describe('src/components/TimeSliderLite/TimeSliderLiteBackgroundSvg', () => {
  const defaultProps = {
    width: 1498,
    height: 44,
    startTime: 1682856000,
    endTime: 1683201600,
    locale: 'en',
  };

  it('should render background', () => {
    const numberOfDays =
      (defaultProps.endTime - defaultProps.startTime) / (60 * 60 * 24);
    const numberOfVisibleDays = numberOfDays + 2; // Renders one day padding each side
    const numberOfTicks = numberOfVisibleDays * 5; // Four ticks per day plus end tick
    const { baseElement } = render(
      <CoreThemeProvider>
        <TimeSliderLiteBackgroundSvg {...defaultProps} />
      </CoreThemeProvider>,
    );

    // eslint-disable-next-line testing-library/no-node-access
    expect(baseElement.querySelectorAll('rect').length).toBe(
      numberOfVisibleDays,
    );
    // eslint-disable-next-line testing-library/no-node-access
    expect(baseElement.querySelectorAll('line').length).toBe(numberOfTicks);
    // eslint-disable-next-line testing-library/no-node-access
    expect(baseElement.querySelectorAll('text').length).toBe(
      numberOfVisibleDays,
    );
  });

  it('should render observed and forecast background in alternating colors', () => {
    const currentTime = (defaultProps.startTime + defaultProps.endTime) / 2;
    const { baseElement } = render(
      <CoreThemeProvider>
        <TimeSliderLiteBackgroundSvg
          {...defaultProps}
          currentTime={currentTime}
        />
      </CoreThemeProvider>,
    );
    // eslint-disable-next-line testing-library/no-node-access
    const backgroundRectangles = baseElement.querySelectorAll('rect');
    expect(backgroundRectangles.length).toBe(7);

    const first = backgroundRectangles[0];
    const second = backgroundRectangles[1];
    const third = backgroundRectangles[2];
    const partial = backgroundRectangles[3];
    const thirdLast = backgroundRectangles[4];
    const secondLast = backgroundRectangles[5];
    const last = backgroundRectangles[6];

    const observedFill =
      lightTheme.palette.geowebColors.timeSliderLite.timelineBackgroundObserved
        .fill;
    const observedFillAlternative =
      lightTheme.palette.geowebColors.timeSliderLite
        .timelineBackgroundObservedAlternative.fill;
    const forecastFill =
      lightTheme.palette.geowebColors.timeSliderLite.timelineBackground.fill;
    const forecastFillAlternative =
      lightTheme.palette.geowebColors.timeSliderLite
        .timelineBackgroundAlternative.fill;

    expect(first.getAttribute('fill')).toBe(observedFillAlternative);
    expect(second.getAttribute('fill')).toBe(observedFill);

    // Middle should consist of forecast background and observed partial rectangle
    expect(third.getAttribute('fill')).toBe(forecastFill);
    expect(partial.getAttribute('fill')).toBe(observedFillAlternative);
    expect(Number(partial.getAttribute('width'))).toBeLessThan(
      Number(first.getAttribute('width')),
    );
    expect(Number(partial.getAttribute('x'))).toBe(
      Number(third.getAttribute('x')),
    );

    expect(thirdLast.getAttribute('fill')).toBe(forecastFillAlternative);
    expect(secondLast.getAttribute('fill')).toBe(forecastFill);
    expect(last.getAttribute('fill')).toBe(forecastFillAlternative);
  });

  it('should localize day labels', async () => {
    render(
      <CoreThemeProvider>
        <TimeSliderLiteBackgroundSvg {...defaultProps} locale="fi" />
      </CoreThemeProvider>,
    );

    // Expect Finnish labels
    expect(await screen.findByText('Su 30.4.')).toBeTruthy();
    expect(await screen.findByText('Ma 1.5.')).toBeTruthy();
    expect(await screen.findByText('Ti 2.5.')).toBeTruthy();
    expect(await screen.findByText('Ke 3.5.')).toBeTruthy();
    expect(await screen.findByText('To 4.5.')).toBeTruthy();
    expect(await screen.findByText('Pe 5.5.')).toBeTruthy();
  });
});
