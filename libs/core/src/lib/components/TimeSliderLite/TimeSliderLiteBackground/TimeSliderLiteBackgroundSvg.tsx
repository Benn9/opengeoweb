/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */

import { useTheme } from '@mui/material';
import React from 'react';
import {
  DAY_TO_MILLISECOND,
  MILLISECOND_TO_DAY,
  MILLISECOND_TO_SECOND,
  SECOND_TO_DAY,
  SECOND_TO_MILLISECOND,
  getMonthChanges,
  getSelectedTimePx,
  maxSubTicksPerDay,
  secondsSinceLocalMidnight,
  subTicksPerDay,
} from '../timeSliderLiteUtils';
import { TimeZone } from '../TimeSliderLiteOptionsMenu/TimeZoneSwitch/TimeZoneSwitch';

interface Props {
  width: number;
  height: number;
  startTime: number;
  endTime: number;
  currentTime?: number;
  timeStep?: [number, number];
  style?: React.CSSProperties;
  locale: string;
  timeZone?: TimeZone;
}

const TICK_HEIGHT = 6;
const HOUR_LABEL_OFFSET = 1.5;
const MONTH_LABEL_BREAKPOINT = 65;
const HOUR_LABEL_BREAKPOINT = 800;
const HOUR_LABEL_EVERY_THIRD_HOUR_BREAKPOINT = 1600;
export const TIME_SLIDER_LITE_PADDING_RIGHT = 1;

const TimeSliderLiteBackgroundSvg: React.FC<Props> = ({
  width,
  height,
  startTime,
  endTime,
  currentTime = Date.now() * MILLISECOND_TO_SECOND,
  timeStep,
  style,
  locale,
  timeZone = 'LT',
}) => {
  const {
    palette: {
      geowebColors: { timeSliderLite },
    },
  } = useTheme();

  if (!width) {
    return <svg style={style} height={height} />;
  }

  // Convert start and end time from Unix timestamp to JavaScript Date object
  const startDate = new Date(startTime * SECOND_TO_MILLISECOND);
  const endDate = new Date(endTime * SECOND_TO_MILLISECOND);
  const totalDays =
    (endDate.getTime() - startDate.getTime()) * MILLISECOND_TO_DAY;
  const utcOffset =
    timeZone === 'UTC'
      ? new Date().getTimezoneOffset() * 60 * SECOND_TO_DAY
      : 0;

  const tickSpacing = width / totalDays;
  if (
    tickSpacing === Infinity ||
    tickSpacing === -Infinity ||
    isNaN(tickSpacing)
  ) {
    return null;
  }

  const startTimeDayOffset =
    secondsSinceLocalMidnight(startTime) * SECOND_TO_DAY + utcOffset;
  const tickOffset = startTimeDayOffset * tickSpacing;
  const showMonthLabel = tickSpacing < MONTH_LABEL_BREAKPOINT;

  const maxTimeStep = timeStep ? Math.max(...timeStep) : null;
  const hideHourLabels = maxTimeStep && maxTimeStep > 60 * 60 * 3;
  const showHourLabel = !hideHourLabels && tickSpacing > HOUR_LABEL_BREAKPOINT;
  const limitHourLabelsToEveryThirdHour = maxTimeStep && maxTimeStep > 60 * 60;
  const showHourLabelEveryThirdHour =
    limitHourLabelsToEveryThirdHour ||
    tickSpacing < HOUR_LABEL_EVERY_THIRD_HOUR_BREAKPOINT;

  // Set up variables for date labels and background colors
  const dayLabels: string[] = [];
  const dayLabelDate = new Date(startTime * SECOND_TO_MILLISECOND);
  const lastDayLabelDate = new Date(endDate.getTime() + 2 * DAY_TO_MILLISECOND); // Append two more days
  while (dayLabelDate < lastDayLabelDate) {
    const dateString = dayLabelDate.toLocaleDateString(locale, {
      weekday: 'short',
      month: 'numeric',
      day: 'numeric',
    });
    const label = dateString.charAt(0).toUpperCase() + dateString.slice(1);

    dayLabels.push(label);
    dayLabelDate.setDate(dayLabelDate.getDate() + 1);
  }

  const monthChanges: number[] = getMonthChanges(startDate, endDate);
  const monthLabels: JSX.Element[] | null = showMonthLabel
    ? monthChanges.map((monthChange) => {
        const x = getSelectedTimePx(startTime, endTime, monthChange, width) + 4;
        const dateString = new Date(monthChange * 1000).toLocaleDateString(
          locale,
          {
            weekday: 'short',
            month: 'numeric',
            day: 'numeric',
          },
        );
        const label = dateString.charAt(0).toUpperCase() + dateString.slice(1);
        return (
          <text
            x={x}
            y={14}
            fill={timeSliderLite.timeline.fill}
            textAnchor="left"
            fontSize="12px"
            key={`${monthChange}-month-change-text`}
            style={{ userSelect: 'none' }}
          >
            {label}
          </text>
        );
      })
    : null;

  const currentDayIndex =
    (currentTime - startTime) * SECOND_TO_DAY + startTimeDayOffset;
  const backgroundRectangles: JSX.Element[] = dayLabels
    .map((label, index) => {
      const observedColor =
        index % 2 === 0
          ? timeSliderLite.timelineBackgroundObservedAlternative.fill
          : timeSliderLite.timelineBackgroundObserved.fill;
      const forecastColor =
        index % 2 === 0
          ? timeSliderLite.timelineBackground.fill
          : timeSliderLite.timelineBackgroundAlternative.fill;

      const defaultProps = {
        x: Math.floor(index * tickSpacing - tickOffset),
        y: 0,
        height,
      };

      const baseRectangle = (
        <rect
          {...defaultProps}
          fill={index + 1 < currentDayIndex ? observedColor : forecastColor}
          width={Math.ceil(tickSpacing)}
          key={`${label}-color`}
        />
      );

      const localDayOffset =
        secondsSinceLocalMidnight(currentTime) * SECOND_TO_DAY + utcOffset;
      const isCurrentDay = index === Math.floor(currentDayIndex);
      if (isCurrentDay) {
        const partialRectangle = (
          <rect
            {...defaultProps}
            width={tickSpacing * localDayOffset}
            fill={observedColor}
            key={`${label}-partial-color`}
          />
        );
        return [baseRectangle, partialRectangle];
      }

      return baseRectangle;
    })
    .flat();

  const dailyLabels: JSX.Element[] | null = !showMonthLabel
    ? dayLabels.map((label, index) => (
        <text
          x={index * tickSpacing + 8 - tickOffset}
          y={14}
          fill={timeSliderLite.timeline.fill}
          textAnchor="left"
          fontSize="12px"
          key={`${label}-text`}
          style={{ userSelect: 'none' }}
        >
          {dayLabels[index]}
        </text>
      ))
    : null;

  const hourLabelsPerDay = 24;
  const hourLabels: JSX.Element[] | null = showHourLabel
    ? dayLabels
        .map((label, index) =>
          Array.from({ length: hourLabelsPerDay }, (_, value) => {
            const defaultProps = {
              x:
                index * tickSpacing +
                value * (tickSpacing / hourLabelsPerDay) -
                tickOffset -
                HOUR_LABEL_OFFSET,
              y: height - 10,
              visibility:
                showHourLabelEveryThirdHour && value % 3 !== 0
                  ? 'hidden'
                  : 'visible',
              fill: timeSliderLite.timeline.fill,
              fontSize: '12px',
            };
            return (
              <g key={`${label}-${value}-hourLabel`}>
                <text
                  {...defaultProps}
                  textAnchor="end"
                  style={{ userSelect: 'none' }}
                >
                  {value}
                </text>
                <text
                  {...defaultProps}
                  textAnchor="start"
                  style={{ userSelect: 'none' }}
                >
                  :00
                </text>
                ,
              </g>
            );
          }),
        )
        .flat()
    : null;

  const tickMarks = dayLabels
    .map((label, index) => {
      const defaultProps = {
        y1: height,
        y2: Math.floor(height - TICK_HEIGHT),
        stroke: timeSliderLite.timeline.stroke,
      };

      const ticksPerDay = timeStep
        ? maxSubTicksPerDay(
            tickSpacing,
            index > currentDayIndex ? timeStep[1] : timeStep[0],
          )
        : subTicksPerDay(tickSpacing);

      const isCurrentDay = index === Math.floor(currentDayIndex);

      if (isCurrentDay && timeStep) {
        const observedTicksPerDay = maxSubTicksPerDay(tickSpacing, timeStep[0]);
        const forecastTicksPerDay = maxSubTicksPerDay(tickSpacing, timeStep[1]);
        const observedTickMarksFromStartOfDay = Array.from(
          {
            length:
              Math.floor(observedTicksPerDay * (currentDayIndex - index)) + 1,
          },
          (_, value) => (
            <line
              {...defaultProps}
              x1={
                index * tickSpacing +
                value * (tickSpacing / observedTicksPerDay) -
                tickOffset
              }
              x2={
                index * tickSpacing +
                value * (tickSpacing / observedTicksPerDay) -
                tickOffset
              }
              key={`${label}-${value}-observed-tick`}
            />
          ),
        );
        const forecastTickMarksFromEndOfDay = Array.from(
          {
            length:
              Math.floor(
                forecastTicksPerDay * (1 - (currentDayIndex - index)),
              ) + 1,
          },
          (_, value) => (
            <line
              {...defaultProps}
              x1={
                (index + 1) * tickSpacing -
                value * (tickSpacing / forecastTicksPerDay) -
                tickOffset
              }
              x2={
                (index + 1) * tickSpacing -
                value * (tickSpacing / forecastTicksPerDay) -
                tickOffset
              }
              key={`${label}-${value}-forecast-tick`}
            />
          ),
        );
        return [
          ...observedTickMarksFromStartOfDay,
          ...forecastTickMarksFromEndOfDay,
        ];
      }

      return Array.from({ length: ticksPerDay + 1 }, (_, value) => (
        <line
          {...defaultProps}
          x1={
            index * tickSpacing +
            value * (tickSpacing / ticksPerDay) -
            tickOffset
          }
          x2={
            index * tickSpacing +
            value * (tickSpacing / ticksPerDay) -
            tickOffset
          }
          key={`${label}-${value}-tick`}
        />
      ));
    })
    .flat();

  return (
    <svg
      style={style}
      width={width + TIME_SLIDER_LITE_PADDING_RIGHT}
      height={height}
    >
      {backgroundRectangles}
      {tickMarks}
      {monthLabels}
      {dailyLabels}
      {hourLabels}
    </svg>
  );
};

export default TimeSliderLiteBackgroundSvg;
