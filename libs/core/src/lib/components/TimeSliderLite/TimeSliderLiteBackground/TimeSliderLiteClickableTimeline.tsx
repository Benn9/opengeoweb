/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */

import { useTheme } from '@mui/material';
import React from 'react';
import {
  MILLISECOND_TO_SECOND,
  getNearestSelectableTime,
  getSelectedTimePx,
} from '../timeSliderLiteUtils';

interface IProps {
  currentTime?: number;
  endTime: number;
  opacity?: number;
  setSelectedTime: (time: number) => void;
  startTime: number;
  timeStep: [number, number];
  width: number;
}

const HOVER_NEEDLE_WIDTH = 1;
const DEFAULT_OPACITY = 0.5;

const TimeSliderClickableTimeline: React.FC<IProps> = ({
  currentTime = Date.now() * MILLISECOND_TO_SECOND,
  endTime,
  opacity = DEFAULT_OPACITY,
  setSelectedTime,
  startTime,
  timeStep,
  width,
}) => {
  const [isShown, setIsShown] = React.useState(false);
  const [mouseX, setMouseX] = React.useState(0);

  const {
    palette: {
      geowebColors: { timeSliderLite },
    },
  } = useTheme();

  const needleSuggestion = getNearestSelectableTime(
    timeStep,
    startTime,
    endTime,
    mouseX,
    width,
    currentTime,
  );

  const x = getSelectedTimePx(startTime, endTime, needleSuggestion, width);

  if (isNaN(x)) {
    return null;
  }

  return (
    <svg
      data-testid="TimeSliderLite-clickableTimeline"
      style={{
        position: 'absolute',
        width: '100%',
        height: '100%',
        cursor: 'pointer',
      }}
      onClick={(): void => setSelectedTime(needleSuggestion)}
      onMouseEnter={(): void => setIsShown(true)}
      onMouseLeave={(): void => setIsShown(false)}
      onMouseMove={(event): void => {
        isShown && setMouseX(event.nativeEvent.offsetX);
      }}
    >
      <line
        opacity={isShown ? opacity : 0}
        x1={x}
        y1="0"
        x2={x}
        y2="100%"
        stroke={timeSliderLite.timeline.stroke}
        strokeWidth={HOVER_NEEDLE_WIDTH}
        style={{
          transition: 'opacity 0.2s ease-in-out',
        }}
      />
    </svg>
  );
};

export default TimeSliderClickableTimeline;
