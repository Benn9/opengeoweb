/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */

import { mapActions, webmapReducer } from '@opengeoweb/store';
import { configureStore } from '@reduxjs/toolkit';
import * as React from 'react';
import { fireEvent, render, screen } from '@testing-library/react';
import { lightTheme } from '@opengeoweb/theme';
import TimeSliderLiteConnect from './TimeSliderLiteConnect';
import { CoreThemeStoreProvider } from '../Providers/Providers';
import {
  DAY_TO_SECOND,
  HOUR_TO_MINUTE,
  HOUR_TO_SECOND,
} from './timeSliderLiteUtils';

describe('src/components/TimeSliderLite/TimeSliderLiteConnect', () => {
  it('should render the component when the map has a layer with time dimension', () => {
    const mapId = 'mapid_1';
    const store = {
      addEggs: jest.fn(),
      ...configureStore({
        reducer: { webmap: webmapReducer },
      }),
    };
    store.dispatch(mapActions.registerMap({ mapId }));

    render(
      <CoreThemeStoreProvider theme={lightTheme} store={store}>
        <TimeSliderLiteConnect sourceId="timeslider-1" mapId={mapId} />
      </CoreThemeStoreProvider>,
    );

    expect(screen.getByTestId('timeSliderLite-mapid_1')).toBeTruthy();
    expect(screen.getByTestId('TimeSliderLite-MenuButton')).toBeTruthy();
    expect(screen.getByTestId('TimeSliderLite-PlayButton')).toBeTruthy();
    expect(
      screen.getByTestId('TimeSliderLite-StepButtonBackward'),
    ).toBeTruthy();
    expect(screen.getByTestId('TimeSliderLite-StepButtonForward')).toBeTruthy();
    expect(screen.getByTestId('TimeSliderLite-Background')).toBeTruthy();
    expect(screen.getByTestId('TimeSliderLite-HideButton')).toBeTruthy();
  });

  const expectedTooltipTimeStamp = (timeInSeconds: number): string =>
    new Date(timeInSeconds * 1000).toLocaleDateString('en', {
      weekday: 'short',
      month: 'numeric',
      day: 'numeric',
      hour: 'numeric',
      minute: 'numeric',
    });

  it('should have modifiable default time step', () => {
    const currentTime = '2020-03-13T13:00:00Z';
    const currentTimeSeconds = new Date(currentTime).getTime() / 1000;

    const defaultTimeStep = [HOUR_TO_MINUTE, HOUR_TO_MINUTE] as [
      number,
      number,
    ];

    const mapId = 'mapid_1';
    const store = {
      addEggs: jest.fn(),
      ...configureStore({
        reducer: { webmap: webmapReducer },
      }),
    };
    store.dispatch(mapActions.registerMap({ mapId }));
    store.dispatch(
      mapActions.mapChangeDimension({
        mapId,
        dimension: {
          name: 'time',
          currentValue: currentTime,
        },
        origin: 'map',
      }),
    );

    render(
      <CoreThemeStoreProvider store={store}>
        <TimeSliderLiteConnect
          sourceId="timeslider-2"
          mapId={mapId}
          currentTime={currentTimeSeconds}
          selectedTime={currentTimeSeconds}
          startTime={currentTimeSeconds - DAY_TO_SECOND}
          endTime={currentTimeSeconds + DAY_TO_SECOND}
          TimeSliderLiteOptionsMenuProps={{
            defaultTimeStep,
          }}
        />
      </CoreThemeStoreProvider>,
    );

    const tooltip = screen.getByRole('tooltip');
    expect(tooltip).toBeTruthy();
    expect(tooltip.innerHTML).toMatch(
      expectedTooltipTimeStamp(currentTimeSeconds),
    );

    // Should increment by defaultTimeStep
    fireEvent.click(screen.getByTestId('TimeSliderLite-StepButtonForward'));
    expect(tooltip.innerHTML).toMatch(
      expectedTooltipTimeStamp(currentTimeSeconds + HOUR_TO_SECOND),
    );
  });
});
