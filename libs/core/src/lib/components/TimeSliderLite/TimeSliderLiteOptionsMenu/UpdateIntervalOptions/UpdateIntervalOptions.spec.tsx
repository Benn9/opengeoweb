/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */

import { render, screen, within } from '@testing-library/react';
import React from 'react';
import userEvent from '@testing-library/user-event';
import UpdateIntervalOptions, {
  UpdateIntervalOptionsProps,
} from './UpdateIntervalOptions';

describe('TimeStepSelector', () => {
  const defaultIntervalOption = 1;
  const intervalOptions = [1, 2, 5, 10, 15, 30, 60];
  const defaultProps: UpdateIntervalOptionsProps = {
    useUpdateInterval: [10, jest.fn()],
  };

  const user = userEvent.setup();

  const getIntervalSelectListBox = async (): Promise<HTMLElement> => {
    const selectButton = within(
      screen.getByTestId('TimeSliderLite-updateIntervalSelect'),
    ).getByRole('button', { hidden: true });
    await user.click(selectButton);
    return within(screen.getByRole('presentation')).getByRole('listbox');
  };

  it('should render with selected update interval', () => {
    const expectedSelectLabel = '10 min';
    render(<UpdateIntervalOptions {...defaultProps} />);

    const select = screen.getByTestId('TimeSliderLite-updateIntervalSelect');
    expect(select).toBeInTheDocument();

    expect(screen.getByText(expectedSelectLabel)).toBeInTheDocument();
  });

  it('should render using dropdownButtonIcon', () => {
    const Icon: React.FC = () => <div>Test icon</div>;
    render(
      <UpdateIntervalOptions {...defaultProps} dropdownButtonIcon={Icon} />,
    );

    expect(screen.getByText('Test icon')).toBeInTheDocument();
  });

  test.each(intervalOptions)(
    'should render %s min as selectable option',
    async (intervalOption) => {
      const intervalLabel =
        intervalOption !== defaultIntervalOption
          ? `${intervalOption} min`
          : `${intervalOption} min (default)`;
      const initialUpdateInterval =
        intervalOption !== defaultIntervalOption
          ? defaultIntervalOption
          : intervalOptions[1];

      render(
        <UpdateIntervalOptions
          {...defaultProps}
          useUpdateInterval={[initialUpdateInterval as number, jest.fn()]}
        />,
      );

      const intervalOptionListBox = await getIntervalSelectListBox();

      await user.click(
        // eslint-disable-next-line testing-library/no-node-access
        within(intervalOptionListBox).getByText(intervalLabel).parentElement!,
      );
      expect(screen.getByText(intervalLabel)).toBeTruthy();
    },
  );

  it('should render custom options', async () => {
    const intervalOptionsWithout15min: number[] = [5, 10, 30, 60];

    render(
      <UpdateIntervalOptions
        {...defaultProps}
        updateIntervalOptions={intervalOptionsWithout15min}
      />,
    );

    const intervalOptionListBox = await getIntervalSelectListBox();

    expect(within(intervalOptionListBox).queryByText('15 min')).toBeNull();

    await user.click(
      // eslint-disable-next-line testing-library/no-node-access
      within(intervalOptionListBox).getByText('5 min (default)').parentElement!,
    );
    expect(screen.getByText('5 min (default)')).toBeTruthy();
  });

  it('should render custom prefix', async () => {
    const expectedOptionLabel = 'every 15 min';
    render(
      <UpdateIntervalOptions {...defaultProps} intervalOptionPrefix="every " />,
    );

    const intervalOptionListBox = await getIntervalSelectListBox();

    const button = within(intervalOptionListBox).getByText(expectedOptionLabel);
    await user.click(
      // eslint-disable-next-line testing-library/no-node-access
      button.parentElement!,
    );
    expect(screen.getByText(expectedOptionLabel)).toBeTruthy();
  });

  it('should render default custom postfix', async () => {
    const expectedOptionLabel = '1 min as default';
    render(
      <UpdateIntervalOptions
        {...defaultProps}
        defaultOptionPostfix=" min as default"
      />,
    );

    const intervalOptionListBox = await getIntervalSelectListBox();

    const button = within(intervalOptionListBox).getByText(expectedOptionLabel);
    await user.click(
      // eslint-disable-next-line testing-library/no-node-access
      button.parentElement!,
    );
    expect(screen.getByText(expectedOptionLabel)).toBeTruthy();
  });

  it('should render custom postfix', async () => {
    const expectedOptionLabel = '15 mins';
    render(
      <UpdateIntervalOptions {...defaultProps} intervalOptionPostfix=" mins" />,
    );

    const intervalOptionListBox = await getIntervalSelectListBox();

    const button = within(intervalOptionListBox).getByText(expectedOptionLabel);
    await user.click(
      // eslint-disable-next-line testing-library/no-node-access
      button.parentElement!,
    );
    expect(screen.getByText(expectedOptionLabel)).toBeTruthy();
  });

  it('should render disabled if readonly', () => {
    render(<UpdateIntervalOptions {...defaultProps} useUpdateInterval={[5]} />);

    const selectInputWithoutSetter = screen.getByTestId(
      'TimeSliderLite-updateIntervalSelect-input',
    );
    expect(selectInputWithoutSetter).toBeDisabled();
  });

  it('should render disabled if only one option', () => {
    render(
      <UpdateIntervalOptions
        {...defaultProps}
        updateIntervalOptions={[4]}
        useUpdateInterval={[4, jest.fn()]}
      />,
    );
    const selectInputWithoutOptions = screen.getByTestId(
      'TimeSliderLite-updateIntervalSelect-input',
    );
    expect(selectInputWithoutOptions).toBeDisabled();
  });
});
