/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */

import React from 'react';
import { render, fireEvent, screen } from '@testing-library/react';
import SubmenuWrapper from './SubmenuWrapper';

describe('SubmenuWrapper', () => {
  const defaultProps = {
    'data-testid': 'TimeSliderLite-submenuWrapper',
    closeButtonIcon: <div>Test close icon</div>,
    setSubmenu: jest.fn(),
    title: 'Test Title',
  };

  it('renders with correct title, close button and close button icon', () => {
    render(<SubmenuWrapper {...defaultProps}>Content</SubmenuWrapper>);
    const cardElement = screen.getByTestId('TimeSliderLite-submenuWrapper');
    const titleElement = screen.getByText('Test Title');
    const closeButtonElement = screen.getByTestId(
      'TimeSliderLite-submenuWrapper-closeButton',
    );
    const closeIcon = screen.getByText('Test close icon');

    expect(cardElement).toBeInTheDocument();
    expect(titleElement).toBeInTheDocument();
    expect(closeButtonElement).toBeInTheDocument();
    expect(closeIcon).toBeInTheDocument();
  });

  it('renders default close icon when closeButtonIcon prop is not provided', () => {
    render(
      <SubmenuWrapper {...defaultProps} closeButtonIcon={undefined}>
        Content
      </SubmenuWrapper>,
    );
    const closeButtonElement = screen.getByTestId(
      'TimeSliderLite-submenuWrapper-closeButton',
    );
    const defaultCloseButtonIcon = closeButtonElement.firstChild as HTMLElement;

    expect(defaultCloseButtonIcon.classList).toContain('MuiSvgIcon-root');
  });

  it('setSubmenu null when close button is clicked', () => {
    render(<SubmenuWrapper {...defaultProps}>Content</SubmenuWrapper>);
    const closeButtonElement = screen.getByTestId(
      'TimeSliderLite-submenuWrapper-closeButton',
    );

    fireEvent.click(closeButtonElement);
    expect(defaultProps.setSubmenu).toHaveBeenCalledWith(null);
  });
});
