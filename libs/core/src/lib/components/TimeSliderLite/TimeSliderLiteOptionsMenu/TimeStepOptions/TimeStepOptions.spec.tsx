/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */

import React from 'react';
import { render, fireEvent, screen } from '@testing-library/react';
import TimeStepSelector, { defaultTimeStepOptions } from './TimeStepSelector';

describe('TimeStepSelector', () => {
  const defaultOptions = Object.keys(defaultTimeStepOptions);
  const defaultProps = {
    defaultTimeStep: Number(defaultOptions[0]),
    setValue: jest.fn(),
  };

  it('should render with default props', () => {
    render(<TimeStepSelector {...defaultProps} />);
    const select = screen.getByTestId('TimeSliderLite-timeStepSelect-input');
    expect(select).toBeInTheDocument();
  });

  it('should render dropdown button icon', () => {
    const Icon: React.FC = () => <div>Test Icon</div>;
    render(<TimeStepSelector {...defaultProps} dropdownButtonIcon={Icon} />);

    const icon = screen.getByText('Test Icon');
    expect(icon).toBeInTheDocument();
  });

  it('should update the value when an option is selected', () => {
    const setValue = jest.fn();
    render(<TimeStepSelector {...defaultProps} setValue={setValue} />);
    const select = screen.getByTestId('TimeSliderLite-timeStepSelect-input');
    fireEvent.change(select, { target: { value: 15 } });
    expect(setValue).toHaveBeenCalledWith(15);
  });

  it('should not select value smaller than default time step', () => {
    const setValue = jest.fn();
    const defaultTimeStep = Number(defaultOptions[2]);
    render(
      <TimeStepSelector
        defaultTimeStep={defaultTimeStep}
        setValue={setValue}
      />,
    );
    const select = screen.getByTestId('TimeSliderLite-timeStepSelect-input');

    const invalidOption = Number(defaultOptions[1]);
    fireEvent.change(select, { target: { value: invalidOption } });
    expect(setValue).not.toHaveBeenCalledWith(invalidOption);

    const validOption = Number(defaultOptions[3]);
    fireEvent.change(select, { target: { value: validOption } });
    expect(setValue).toHaveBeenCalledWith(validOption);
  });
});
