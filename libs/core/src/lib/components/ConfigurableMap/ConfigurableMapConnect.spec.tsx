/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { render, waitFor, screen } from '@testing-library/react';
import { createMockStoreWithEggs } from '@opengeoweb/shared';

import {
  mapActions,
  uiActions,
  genericActions,
  uiTypes,
  mapTypes,
  mapConstants,
  defaultLayers,
  CoreAppStore,
  syncConstants,
  storeTestUtils,
  getSingularDrawtoolDrawLayerId,
  mapStoreActions,
} from '@opengeoweb/store';
import { ActionCreatorWithPayload } from '@reduxjs/toolkit';
import { publicLayers, emptyGeoJSON } from '@opengeoweb/webmap-react';
import { LayerType } from '@opengeoweb/webmap';
import {
  ConfigurableMapConnect,
  ConfigurableMapConnectProps,
  defaultBbox,
} from './ConfigurableMapConnect';
import { CoreThemeStoreProvider } from '../Providers/Providers';
import { mockStateMapWithDimensions } from '../../utils/testUtils';
import { multiDimensionLayer } from '../../utils/defaultTestSettings';

const { registerMap, setMapPreset } = mapActions;
const { syncGroupAddSource } = genericActions;
describe('components/ConfigurableMap/ConfigurableMapConnect', () => {
  const mockState: CoreAppStore = {
    syncronizationGroupStore: {
      groups: {
        byId: {},
        allIds: [],
      },
      sources: {
        byId: {},
        allIds: [],
      },
      viewState: {
        timeslider: {
          groups: [],
          sourcesById: [],
        },
        zoompane: {
          groups: [],
          sourcesById: [],
        },
        level: {
          groups: [],
          sourcesById: [],
        },
      },
    },
  };

  it('should render map with default props', () => {
    const store = createMockStoreWithEggs(mockState);
    const props: ConfigurableMapConnectProps = {
      layers: [],
      id: 'test-map-1',
    };
    render(
      <CoreThemeStoreProvider store={store}>
        <ConfigurableMapConnect {...props} />
      </CoreThemeStoreProvider>,
    );
    expect(screen.getByTestId('ConfigurableMap')).toBeTruthy();
    expect(screen.queryByTestId('mapTitle')).toBeFalsy();
    expect(screen.getByTestId('layerManagerButton')).toBeTruthy();
    expect(screen.getByTestId('open-Legend')).toBeTruthy();
    expect(screen.queryByTestId('map-time')).toBeFalsy();
    expect(screen.getByTestId('zoom-reset')).toBeTruthy();
    expect(screen.getByTestId('zoom-in')).toBeTruthy();
    expect(screen.getByTestId('zoom-out')).toBeTruthy();
    expect(screen.getByTestId('timeSlider')).toBeTruthy();

    const expectedActions = [
      registerMap({ mapId: props.id! }),
      syncGroupAddSource({
        id: props.id!,
        type: [
          syncConstants.SYNCGROUPS_TYPE_SETTIME,
          syncConstants.SYNCGROUPS_TYPE_SETBBOX,
        ],
      }),
      uiActions.registerDialog({
        mapId: 'test-map-1',
        setOpen: mapConstants.IS_LEGEND_OPEN_BY_DEFAULT,
        source: 'app',
        type: 'legend-test-map-1',
      }),
      uiActions.registerDialog({
        type: `${uiTypes.DialogTypes.Search}-test-map-1`,
        setOpen: false,
        source: 'app',
      }),
      uiActions.registerDialog({
        setOpen: false,
        source: 'app',
        type: `${uiTypes.DialogTypes.DockedLayerManager}-test-map-1`,
      }),
      setMapPreset({
        initialProps: {
          mapPreset: {
            layers: [
              {
                ...defaultLayers.baseLayerGrey,
                id: expect.stringContaining('layerid_'),
              },
              {
                ...defaultLayers.overLayer,
                id: expect.stringContaining('layerid_'),
              },
              ...props.layers,
            ],
            proj: {
              bbox: defaultBbox.bbox,
              srs: defaultBbox.srs,
            },
            dimensions: [],
            shouldAutoUpdate: false,
            shouldAnimate: false,
            shouldShowZoomControls: true,
            displayMapPin: false,
            showTimeSlider: true,
          },
        },
        mapId: props.id!,
      }),
    ];
    expect(store.getActions()).toEqual(expectedActions);
    expect(screen.queryByTestId('mapControls')?.getAttribute('style')).toEqual(
      'top: 8px;',
    );
  });

  it('should disable the timeSlider', () => {
    const store = createMockStoreWithEggs(mockState);
    const props: ConfigurableMapConnectProps = {
      layers: [],
      id: 'test-map-1',
      disableTimeSlider: true,
    };
    render(
      <CoreThemeStoreProvider store={store}>
        <ConfigurableMapConnect {...props} />
      </CoreThemeStoreProvider>,
    );
    expect(screen.getByTestId('ConfigurableMap')).toBeTruthy();
    expect(screen.queryByTestId('mapTitle')).toBeFalsy();
    expect(screen.getByTestId('layerManagerButton')).toBeTruthy();
    expect(screen.getByTestId('open-Legend')).toBeTruthy();
    expect(screen.queryByTestId('map-time')).toBeFalsy();
    expect(screen.queryByTestId('timeSlider')).toBeFalsy();
    expect(screen.getByTestId('zoom-reset')).toBeTruthy();
    expect(screen.getByTestId('zoom-in')).toBeTruthy();
    expect(screen.getByTestId('zoom-out')).toBeTruthy();

    const expectedActions = [
      registerMap({ mapId: props.id! }),
      syncGroupAddSource({
        id: props.id!,
        type: [
          syncConstants.SYNCGROUPS_TYPE_SETTIME,
          syncConstants.SYNCGROUPS_TYPE_SETBBOX,
        ],
      }),
      uiActions.registerDialog({
        mapId: 'test-map-1',
        setOpen: mapConstants.IS_LEGEND_OPEN_BY_DEFAULT,
        source: 'app',
        type: 'legend-test-map-1',
      }),
      uiActions.registerDialog({
        type: `${uiTypes.DialogTypes.Search}-test-map-1`,
        setOpen: false,
        source: 'app',
      }),
      uiActions.registerDialog({
        setOpen: false,
        source: 'app',
        type: `${uiTypes.DialogTypes.DockedLayerManager}-test-map-1`,
      }),
      setMapPreset({
        initialProps: {
          mapPreset: {
            layers: [
              {
                ...defaultLayers.baseLayerGrey,
                id: expect.stringContaining('layerid_'),
              },
              {
                ...defaultLayers.overLayer,
                id: expect.stringContaining('layerid_'),
              },
              ...props.layers,
            ],
            proj: {
              bbox: defaultBbox.bbox,
              srs: defaultBbox.srs,
            },
            dimensions: [],
            shouldAutoUpdate: false,
            shouldAnimate: false,
            shouldShowZoomControls: true,
            displayMapPin: false,
            showTimeSlider: true,
          },
        },
        mapId: props.id!,
      }),
    ];
    expect(store.getActions()).toEqual(expectedActions);
  });

  it('should render map with custom title', () => {
    const store = createMockStoreWithEggs(mockState);
    const props: ConfigurableMapConnectProps = {
      layers: [],
      id: 'test-map-0',
      title: 'Custom map title',
    };
    render(
      <CoreThemeStoreProvider store={store}>
        <ConfigurableMapConnect {...props} />
      </CoreThemeStoreProvider>,
    );
    expect(screen.getByTestId('ConfigurableMap')).toBeTruthy();
    expect(screen.queryByTestId('mapTitle')!.textContent).toEqual(props.title);
    expect(screen.queryByTestId('mapControls')?.getAttribute('style')).toEqual(
      'top: 24px;',
    );
  });

  it('should render map with custom bbox', () => {
    const store = createMockStoreWithEggs(mockState);
    const props: ConfigurableMapConnectProps = {
      layers: [],
      id: 'test-map-2',
      srs: 'EPSG:3575',
      bbox: {
        left: -13000000,
        bottom: -13000000,
        right: 13000000,
        top: 13000000,
      },
    };
    render(
      <CoreThemeStoreProvider store={store}>
        <ConfigurableMapConnect {...props} />
      </CoreThemeStoreProvider>,
    );
    expect(screen.getByTestId('ConfigurableMap')).toBeTruthy();

    const actionResult = store
      .getActions()
      .find(
        (
          action: ActionCreatorWithPayload<
            mapTypes.SetMapPresetPayload,
            string
          >,
        ) => action.type === setMapPreset.type,
      );

    expect(actionResult.payload.initialProps.mapPreset.proj.bbox).toEqual(
      props.bbox,
    );
    expect(actionResult.payload.initialProps.mapPreset.proj.srs).toEqual(
      props.srs,
    );
  });

  it('should set autoUpdate to false if not passed', async () => {
    const store = createMockStoreWithEggs(mockState);
    const props: ConfigurableMapConnectProps = {
      id: 'map1',
      layers: [],
    };

    render(
      <CoreThemeStoreProvider store={store}>
        <ConfigurableMapConnect {...props} />
      </CoreThemeStoreProvider>,
    );

    const actionResult = store
      .getActions()
      .find(
        (
          action: ActionCreatorWithPayload<
            mapTypes.SetMapPresetPayload,
            string
          >,
        ) => action.type === setMapPreset.type,
      );

    expect(
      actionResult.payload.initialProps.mapPreset.shouldAutoUpdate,
    ).toBeFalsy();
  });

  it('should set autoUpdate to true if passed as true', async () => {
    const store = createMockStoreWithEggs(mockState);
    const testShouldAutoUpdate = true;
    const props: ConfigurableMapConnectProps = {
      id: 'map1',
      layers: [],
      shouldAutoUpdate: testShouldAutoUpdate,
    };

    render(
      <CoreThemeStoreProvider store={store}>
        <ConfigurableMapConnect {...props} />
      </CoreThemeStoreProvider>,
    );

    const actionResult = store
      .getActions()
      .find(
        (
          action: ActionCreatorWithPayload<
            mapTypes.SetMapPresetPayload,
            string
          >,
        ) => action.type === setMapPreset.type,
      );

    expect(
      actionResult.payload.initialProps.mapPreset.shouldAutoUpdate,
    ).toEqual(testShouldAutoUpdate);
  });

  it('should include zoomControls if shouldShowZoomControls not passed', () => {
    const store = createMockStoreWithEggs(mockState);
    const props: ConfigurableMapConnectProps = {
      layers: [],
    };

    render(
      <CoreThemeStoreProvider store={store}>
        <ConfigurableMapConnect {...props} />
      </CoreThemeStoreProvider>,
    );

    expect(screen.getByTestId('ConfigurableMap')).toBeTruthy();
    expect(screen.getByTestId('zoom-reset')).toBeTruthy();
    expect(screen.getByTestId('zoom-in')).toBeTruthy();
    expect(screen.getByTestId('zoom-out')).toBeTruthy();
  });

  it('should not include zoomControls if shouldShowZoomControls is not visible in store', () => {
    const mapId = 'test';
    const newMockState = {
      ...mockState,
      webmap: {
        byId: {
          [mapId]: {
            shouldShowZoomControls: false,
            mapLayers: [],
            baseLayers: [],
            overLayers: [],
            dimensions: [],
          },
        },
        allIds: [mapId],
      },
    };
    const store = createMockStoreWithEggs(newMockState);
    const props: ConfigurableMapConnectProps = {
      layers: [],
      shouldShowZoomControls: false,
      id: mapId,
    };

    render(
      <CoreThemeStoreProvider store={store}>
        <ConfigurableMapConnect {...props} />
      </CoreThemeStoreProvider>,
    );

    expect(screen.getByTestId('ConfigurableMap')).toBeTruthy();
    expect(screen.queryByTestId('zoom-reset')).toBeFalsy();
    expect(screen.queryByTestId('zoom-in')).toBeFalsy();
    expect(screen.queryByTestId('zoom-out')).toBeFalsy();
  });

  it('should include timeSlider if showTimeSlider not passed', () => {
    const store = createMockStoreWithEggs(mockState);
    const props: ConfigurableMapConnectProps = {
      layers: [],
    };

    render(
      <CoreThemeStoreProvider store={store}>
        <ConfigurableMapConnect {...props} />
      </CoreThemeStoreProvider>,
    );

    expect(screen.getByTestId('ConfigurableMap')).toBeTruthy();
    expect(screen.getByTestId('timeSlider')).toBeTruthy();
  });

  it('should not include timeSlider is not visible', async () => {
    const mapId = 'test-1';
    const newMockState = {
      ...mockState,
      webmap: {
        byId: {
          [mapId]: {
            isTimeSliderVisible: false,
            mapLayers: [],
            overLayers: [],
            baseLayers: [],
            dimensions: [],
          },
        },
        allIds: [mapId],
      },
    };
    const store = createMockStoreWithEggs(newMockState);
    const props: ConfigurableMapConnectProps = {
      layers: [],
      showTimeSlider: false,
      id: mapId,
    };

    render(
      <CoreThemeStoreProvider store={store}>
        <ConfigurableMapConnect {...props} />
      </CoreThemeStoreProvider>,
    );
    expect(screen.getByTestId('ConfigurableMap')).toBeTruthy();
    await waitFor(() => {
      expect(screen.queryByTestId('timeSlider')).toBeFalsy();
    });
  });

  it('should render map with autoanimate enabled', () => {
    const end = `2021-12-20T14:00:00Z`;
    jest.spyOn(Date, 'now').mockReturnValue(new Date(end).valueOf());
    const store = createMockStoreWithEggs(mockState);
    const props: ConfigurableMapConnectProps = {
      layers: [],
      id: 'test-map-3',
      shouldAnimate: true,
    };
    render(
      <CoreThemeStoreProvider store={store}>
        <ConfigurableMapConnect {...props} />
      </CoreThemeStoreProvider>,
    );
    expect(screen.getByTestId('ConfigurableMap')).toBeTruthy();

    const actionResult = store
      .getActions()
      .find(
        (
          action: ActionCreatorWithPayload<
            mapTypes.SetMapPresetPayload,
            string
          >,
        ) => action.type === setMapPreset.type,
      );

    expect(actionResult.payload.initialProps.mapPreset.shouldAnimate).toEqual(
      props.shouldAnimate,
    );
  });

  it('should render map with autoanimate disabled when not given', () => {
    const end = `2021-12-20T14:00:00Z`;
    jest.spyOn(Date, 'now').mockReturnValue(new Date(end).valueOf());
    const store = createMockStoreWithEggs(mockState);
    const props: ConfigurableMapConnectProps = {
      layers: [],
      id: 'test-map-3',
    };
    render(
      <CoreThemeStoreProvider store={store}>
        <ConfigurableMapConnect {...props} />
      </CoreThemeStoreProvider>,
    );
    expect(screen.getByTestId('ConfigurableMap')).toBeTruthy();

    const actionResult = store
      .getActions()
      .find(
        (
          action: ActionCreatorWithPayload<
            mapTypes.SetMapPresetPayload,
            string
          >,
        ) => action.type === setMapPreset.type,
      );

    expect(
      actionResult.payload.initialProps.mapPreset.shouldAnimate,
    ).toBeFalsy();
  });

  it('should render map with autoanimate enabled and animationPayload if passed', () => {
    jest.spyOn(Date, 'now').mockReturnValue(new Date().setHours(14, 0, 0, 0));
    const store = createMockStoreWithEggs(mockState);
    const props: ConfigurableMapConnectProps = {
      layers: [],
      id: 'test-map-3',
      shouldAnimate: true,
      animationPayload: { interval: 15, duration: 2 * 60 },
    };
    render(
      <CoreThemeStoreProvider store={store}>
        <ConfigurableMapConnect {...props} />
      </CoreThemeStoreProvider>,
    );
    expect(screen.getByTestId('ConfigurableMap')).toBeTruthy();

    const actionResult = store
      .getActions()
      .find(
        (
          action: ActionCreatorWithPayload<
            mapTypes.SetMapPresetPayload,
            string
          >,
        ) => action.type === setMapPreset.type,
      );

    expect(
      actionResult.payload.initialProps.mapPreset.animationPayload,
    ).toEqual(props.animationPayload);
    expect(actionResult.payload.initialProps.mapPreset.shouldAnimate).toEqual(
      props.shouldAnimate,
    );
  });

  it('should render map with autoanimate enabled and pass speed if passed in payload', () => {
    const store = createMockStoreWithEggs(mockState);
    const props: ConfigurableMapConnectProps = {
      layers: [],
      id: 'test-map-3',
      shouldAnimate: true,
      animationPayload: { speed: 8 },
    };
    render(
      <CoreThemeStoreProvider store={store}>
        <ConfigurableMapConnect {...props} />
      </CoreThemeStoreProvider>,
    );
    expect(screen.getByTestId('ConfigurableMap')).toBeTruthy();

    const actionResult = store
      .getActions()
      .find(
        (
          action: ActionCreatorWithPayload<
            mapTypes.SetMapPresetPayload,
            string
          >,
        ) => action.type === setMapPreset.type,
      );

    expect(
      actionResult.payload.initialProps.mapPreset.animationPayload,
    ).toEqual(props.animationPayload);
    expect(actionResult.payload.initialProps.mapPreset.shouldAnimate).toEqual(
      props.shouldAnimate,
    );
  });

  it('should render map with custom layers', () => {
    const store = createMockStoreWithEggs(mockState);
    const autoLayerId = 'active';
    const props: ConfigurableMapConnectProps = {
      layers: [
        { ...publicLayers.radarLayer, id: autoLayerId },
        publicLayers.baseLayerWorldMap,
        defaultLayers.overLayer,
      ],
      id: 'test-map-4',
      autoTimeStepLayerId: autoLayerId,
      autoUpdateLayerId: autoLayerId,
    };
    render(
      <CoreThemeStoreProvider store={store}>
        <ConfigurableMapConnect {...props} />
      </CoreThemeStoreProvider>,
    );
    expect(screen.getByTestId('ConfigurableMap')).toBeTruthy();
    expect(screen.queryByTestId('mapTitle')).toBeFalsy();

    const expectedAction = {
      type: setMapPreset.type,
      payload: {
        mapId: props.id,
        initialProps: {
          mapPreset: {
            autoTimeStepLayerId: props.autoTimeStepLayerId,
            autoUpdateLayerId: props.autoUpdateLayerId,
            displayMapPin: false,
            layers: props.layers,
            proj: {
              bbox: defaultBbox.bbox,
              srs: defaultBbox.srs,
            },
            dimensions: [],
            shouldAnimate: false,
            shouldAutoUpdate: false,
            shouldShowZoomControls: true,
            showTimeSlider: true,
          } as mapTypes.MapPreset,
        },
      },
    };
    expect(store.getActions()).toContainEqual(expectedAction);
  });

  it('should render map with maptime visible', () => {
    const mapId = 'test-map-5';
    const layer = {
      ...publicLayers.radarLayer,
      dimensions: [{ name: 'time', currentValue: '2021-12-07T13:30:00Z' }],
    };
    const customMockState = storeTestUtils.mockStateMapWithLayer(layer, mapId);
    const store = createMockStoreWithEggs(customMockState);
    const props: ConfigurableMapConnectProps = {
      layers: [],
      id: mapId,
      displayTimeInMap: true,
    };
    render(
      <CoreThemeStoreProvider store={store}>
        <ConfigurableMapConnect {...props} />
      </CoreThemeStoreProvider>,
    );
    expect(screen.getByTestId('ConfigurableMap')).toBeTruthy();
    expect(screen.getByTestId('map-time')).toBeTruthy();
  });

  it('should render map with LayerManagerButton and legend button', () => {
    const store = createMockStoreWithEggs(mockState);
    const props: ConfigurableMapConnectProps = {
      layers: [],
      displayLayerManagerAndLegendButtonInMap: true,
    };
    render(
      <CoreThemeStoreProvider store={store}>
        <ConfigurableMapConnect {...props} />
      </CoreThemeStoreProvider>,
    );
    expect(screen.getByTestId('ConfigurableMap')).toBeTruthy();
    expect(screen.getByTestId('layerManagerButton')).toBeTruthy();
    expect(screen.getByTestId('open-Legend')).toBeTruthy();
  });

  it('should render map without LayerManagerButton or legend button', () => {
    const store = createMockStoreWithEggs(mockState);
    const props: ConfigurableMapConnectProps = {
      layers: [],
      displayLayerManagerAndLegendButtonInMap: false,
    };
    render(
      <CoreThemeStoreProvider store={store}>
        <ConfigurableMapConnect {...props} />
      </CoreThemeStoreProvider>,
    );
    expect(screen.getByTestId('ConfigurableMap')).toBeTruthy();
    expect(screen.queryByTestId('layerManagerButton')).toBeFalsy();
    expect(screen.queryByTestId('open-Legend')).toBeFalsy();
  });

  it('should render map with DimensionSelectButton', () => {
    const mapId = 'test-map-7';
    const customMockState = mockStateMapWithDimensions(
      multiDimensionLayer,
      mapId,
    );
    const newMockState = { ...mockState, ...customMockState };
    const store = createMockStoreWithEggs(newMockState);
    const props: ConfigurableMapConnectProps = {
      layers: [multiDimensionLayer],
      id: mapId,
      displayDimensionSelectButtonInMap: true,
    };
    render(
      <CoreThemeStoreProvider store={store}>
        <ConfigurableMapConnect {...props} />
      </CoreThemeStoreProvider>,
    );
    expect(screen.getByTestId('ConfigurableMap')).toBeTruthy();
    expect(screen.getByTestId('dimensionMapBtn-elevation')).toBeTruthy();
  });

  it('should render map without DimensionSelectButton', () => {
    jest.spyOn(console, 'warn').mockImplementation();
    const mapId = 'test-map-8';
    const customMockState = mockStateMapWithDimensions(
      multiDimensionLayer,
      mapId,
    );
    const newMockState = { ...mockState, ...customMockState };
    const store = createMockStoreWithEggs(newMockState);
    const props: ConfigurableMapConnectProps = {
      layers: [multiDimensionLayer],
      id: mapId,
      displayDimensionSelectButtonInMap: false,
    };
    render(
      <CoreThemeStoreProvider store={store}>
        <ConfigurableMapConnect {...props} />
      </CoreThemeStoreProvider>,
    );
    expect(screen.getByTestId('ConfigurableMap')).toBeTruthy();
    expect(screen.queryByTestId('dimensionMapBtn-elevation')).toBeFalsy();
  });

  it('should render map with toggleTimestepAuto using preset values', () => {
    const store = createMockStoreWithEggs(mockState);
    const props: ConfigurableMapConnectProps = {
      layers: [],
      id: 'radarView',
      displayDimensionSelectButtonInMap: false,
      toggleTimestepAuto: false,
    };

    render(
      <CoreThemeStoreProvider store={store}>
        <ConfigurableMapConnect {...props} />
      </CoreThemeStoreProvider>,
    );

    const actionResult = store
      .getActions()
      .find(
        (
          action: ActionCreatorWithPayload<
            mapTypes.SetMapPresetPayload,
            string
          >,
        ) => action.type === setMapPreset.type,
      );

    expect(
      actionResult.payload.initialProps.mapPreset.toggleTimestepAuto,
    ).toEqual(props.toggleTimestepAuto);
    expect(screen.queryByTestId('dimensionMapBtn-elevation')).toBeNull();
  });

  it('should render map with mapid in legend type', async () => {
    const props: ConfigurableMapConnectProps = {
      layers: [],
      id: 'radarView',
      displayDimensionSelectButtonInMap: false,
      toggleTimestepAuto: false,
      multiLegend: true,
    };

    const store = createMockStoreWithEggs(mockState);

    render(
      <CoreThemeStoreProvider store={store}>
        <ConfigurableMapConnect {...props} />
      </CoreThemeStoreProvider>,
    );
    const expectedAction = uiActions.registerDialog({
      mapId: props.id,
      setOpen: mapConstants.IS_LEGEND_OPEN_BY_DEFAULT,
      source: 'app',
      type: `legend-${props.id}`,
    });

    await waitFor(() => {
      expect(store.getActions()).toContainEqual(expectedAction);
    });
  });

  it('should pass setTimestep value using preset animation interval values', () => {
    const store = createMockStoreWithEggs(mockState);
    const props: ConfigurableMapConnectProps = {
      layers: [],
      displayDimensionSelectButtonInMap: false,
      id: 'radarView',
      animationPayload: {
        interval: 5,
      },
    };

    render(
      <CoreThemeStoreProvider store={store}>
        <ConfigurableMapConnect {...props} />
      </CoreThemeStoreProvider>,
    );

    const actionResult = store
      .getActions()
      .find(
        (
          action: ActionCreatorWithPayload<
            mapTypes.SetMapPresetPayload,
            string
          >,
        ) => action.type === setMapPreset.type,
      );

    expect(
      actionResult.payload.initialProps.mapPreset.animationPayload.interval,
    ).toEqual(props.animationPayload!.interval);
  });

  it('should show layermanager', () => {
    const store = createMockStoreWithEggs(mockState);
    const props: ConfigurableMapConnectProps = {
      layers: [],
      id: 'radarView',
      shouldShowLayerManager: true,
    };

    render(
      <CoreThemeStoreProvider store={store}>
        <ConfigurableMapConnect {...props} />
      </CoreThemeStoreProvider>,
    );

    const actionResult = store
      .getActions()
      .find(
        (
          action: ActionCreatorWithPayload<
            mapTypes.SetMapPresetPayload,
            string
          >,
        ) => action.type === uiActions.setActiveMapIdForDialog.type,
      ).payload;

    expect(actionResult.type).toEqual(uiTypes.DialogTypes.LayerManager);
    expect(actionResult.mapId).toEqual(props.id);
    expect(actionResult.setOpen).toEqual(props.shouldShowLayerManager);
    expect(actionResult.source).toEqual('app');
  });

  it('should show hide normal LayerManager and show dockedLayerManager', () => {
    const mapId = 'radarView';
    const store = createMockStoreWithEggs(mockState);
    const props: ConfigurableMapConnectProps = {
      layers: [],
      id: mapId,
      shouldShowLayerManager: true,
      shouldShowDockedLayerManager: true,
    };

    render(
      <CoreThemeStoreProvider store={store}>
        <ConfigurableMapConnect {...props} />
      </CoreThemeStoreProvider>,
    );

    const expectedAction1 = uiActions.setToggleOpenDialog({
      type: uiTypes.DialogTypes.LayerManager,
      mapId,
      setOpen: false,
    });

    const expectedAction2 = uiActions.setToggleOpenDialog({
      type: `${uiTypes.DialogTypes.DockedLayerManager}-${'radarView'}`,
      mapId,
      setOpen: true,
    });

    expect(store.getActions()).toContainEqual(expectedAction1);
    expect(store.getActions()).toContainEqual(expectedAction2);
  });

  it('should register the DockedLayerManager', () => {
    const store = createMockStoreWithEggs(mockState);
    const mapId = 'mapid_1';
    const props: ConfigurableMapConnectProps = {
      id: mapId,
      layers: [],
      displayLayerManagerAndLegendButtonInMap: false,
    };
    render(
      <CoreThemeStoreProvider store={store}>
        <ConfigurableMapConnect {...props} />
      </CoreThemeStoreProvider>,
    );
    const expectedAction = uiActions.registerDialog({
      setOpen: false,
      source: 'app',
      type: `${uiTypes.DialogTypes.DockedLayerManager}-${mapId}`,
    });

    expect(store.getActions()).toContainEqual(expectedAction);
  });

  it('should not show draw controls if not given', () => {
    const store = createMockStoreWithEggs(mockState);
    const mapId = 'mapid_1';
    const props: ConfigurableMapConnectProps = {
      id: mapId,
      layers: [],
    };
    render(
      <CoreThemeStoreProvider store={store}>
        <ConfigurableMapConnect {...props} />
      </CoreThemeStoreProvider>,
    );
    expect(screen.queryByTestId('drawingToolButton')).toBeFalsy();
  });

  it('should add a draw layer if shouldDisplayDrawControls is given', () => {
    const store = createMockStoreWithEggs(mockState);
    const mapId = 'mapid_1';
    const props: ConfigurableMapConnectProps = {
      id: mapId,
      layers: [],
      shouldDisplayDrawControls: true,
    };
    render(
      <CoreThemeStoreProvider store={store}>
        <ConfigurableMapConnect {...props} />
      </CoreThemeStoreProvider>,
    );

    expect(store.getActions()).toContainEqual(
      mapStoreActions.addLayer({
        mapId,
        layer: {
          geojson: emptyGeoJSON,
          layerType: LayerType.featureLayer,
        },
        layerId: getSingularDrawtoolDrawLayerId(mapId),
        origin: 'ConfigurableMapConnect',
      }),
    );
  });

  it('should render given mapControls', async () => {
    const store = createMockStoreWithEggs(mockState);
    const mapId = 'mapid_1';
    const props: ConfigurableMapConnectProps = {
      id: mapId,
      layers: [],
      mapControls: <p>my new button</p>,
    };
    render(
      <CoreThemeStoreProvider store={store}>
        <ConfigurableMapConnect {...props} />
      </CoreThemeStoreProvider>,
    );
    expect(await screen.findByText('my new button')).toBeTruthy();
  });
});
