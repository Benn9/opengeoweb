/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */

import React from 'react';
import { act, render, screen } from '@testing-library/react';
import { createStore } from '@redux-eggs/redux-toolkit';
import { getSagaExtension } from '@redux-eggs/saga-extension';
import { mapActions } from '@opengeoweb/store';
import userEvent from '@testing-library/user-event';
import { CoreThemeStoreProvider } from '../Providers/Providers';
import { SearchControlButtonConnect } from './SearchControlButtonConnect';
import { SearchControlConnect } from './SearchControlConnect';

describe('src/components/Search/SearchControlConnect', () => {
  it('should display search dialog', async () => {
    const store = createStore({
      extensions: [getSagaExtension()],
    });

    const mapId = 'mapId123';
    render(
      <CoreThemeStoreProvider store={store}>
        <SearchControlButtonConnect mapId={mapId} />
        <SearchControlConnect mapId={mapId} />
      </CoreThemeStoreProvider>,
    );

    act(() => {
      store.dispatch(mapActions.registerMap({ mapId }));
    });

    expect(screen.queryByRole('textbox')).toBeFalsy();

    await userEvent.click(screen.getByRole('button', { name: 'Map search' }));

    expect(screen.getByRole('textbox')).toBeTruthy();

    await userEvent.click(screen.getByRole('button', { name: 'Map search' }));

    expect(screen.queryByRole('textbox')).toBeFalsy();
  });
});
