/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import {
  LayerManagerConnect,
  LayerManagerHeaderOptions,
  LayerManagerDescriptionRow,
  LayerManagerLayerContainerRow,
  LayerManagerBaseLayerRow,
  LayerManagerMapButtonConnect,
  LayerManager,
  useFetchServices,
} from './components/LayerManager';

import {
  MultiDimensionSelectMapButtonsConnect,
  MultiMapDimensionSelectConnect,
} from './components/MultiMapDimensionSelect';

export * from './components/ComponentsLookUp';
export * from './components/ConfigurableMap';

export { SyncGroupViewerConnect } from './components/SyncGroups/SyncGroupViewerConnect';

export * from './components/MultiMapView/MultiMapViewConnect';
export * from './components/MultiMapView/HarmoniePresets';

export * from './components/MapView';

export { LegendConnect, LegendMapButtonConnect } from './components/Legend';

export { ZoomControlConnect } from './components/MapControls';

export {
  TimeSliderLite,
  TimeSliderLiteOptionsMenu,
  TimeSliderLiteConnect,
  TimeSliderLiteUtils,
} from './components/TimeSliderLite';

export {
  LayerManagerConnect,
  LayerManagerHeaderOptions,
  LayerManagerDescriptionRow,
  LayerManagerBaseLayerRow,
  LayerManagerLayerContainerRow,
  LayerManagerMapButtonConnect,
  LayerManager,
  useFetchServices,
};

export {
  MultiDimensionSelectMapButtonsConnect,
  MultiMapDimensionSelectConnect,
};

export * as testLayers from './utils/testLayers';
export * from './utils/jsonPresetFilter';
export * as defaultConfigurations from './utils/defaultConfigurations';
export * from './components/Providers/Providers';
export * from './components/RouterWrapper';

export * from './components/AppWrapper';
