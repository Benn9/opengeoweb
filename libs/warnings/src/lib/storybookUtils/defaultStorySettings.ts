/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { useDispatch } from 'react-redux';
import {
  layerTypes,
  mapActions,
  mapStoreActions,
  mapTypes,
  defaultLayers,
} from '@opengeoweb/store';

export const initialBbox = {
  srs: 'EPSG:3857',
  bbox: {
    left: -450651.2255879827,
    bottom: 6490531.093143953,
    right: 1428345.8183648037,
    top: 7438773.776232235,
  },
};

interface UseDefaultMapSettingsProps {
  mapId?: string;
  baseLayers?: layerTypes.Layer[];
  bbox?: mapTypes.Bbox;
  srs?: string;
}

export const useDefaultMapSettings = (
  props: UseDefaultMapSettingsProps = {},
): void => {
  const dispatch = useDispatch();

  React.useEffect(() => {
    const { mapId = 'test-map-1' } = props;
    const {
      baseLayers = [
        { ...defaultLayers.baseLayerGrey, id: `layer-grey-${mapId}` },
        { ...defaultLayers.overLayer, id: `overlayer-${mapId}` },
      ],
      bbox = initialBbox.bbox,
      srs = initialBbox.srs,
    } = props;

    dispatch(
      mapStoreActions.setBaseLayers({
        mapId,
        layers: baseLayers,
      }),
    );
    dispatch(
      mapActions.setBbox({
        bbox,
        srs,
        mapId,
      }),
    );

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);
};
