/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */

import { AxiosInstance } from 'axios';
import { createFakeApiInstance } from '@opengeoweb/api';
import { WarningsApi } from './api';
import {
  DrawingDetails,
  DrawingFromBE,
  DrawingListItem,
} from '../store/drawings/types';

import drawingList from './fakeApi/drawingsList.json';
import drawingsListFullContent from './fakeApi/drawingsListFullContent.json';

export const ERROR_NOT_FOUND = 'Request failed with status code 400';

export const extractDrawingDetailsFromJSON = (
  drawingId: string,
): DrawingFromBE => {
  return (drawingsListFullContent as DrawingFromBE[]).find(
    (element) => element.id === drawingId,
  )!;
};

const getApiRoutes = (axiosInstance: AxiosInstance): WarningsApi => ({
  // drawings
  getDrawings: (): Promise<{ data: DrawingListItem[] }> => {
    return axiosInstance.get('/drawings').then(() => {
      const data = drawingList as DrawingListItem[];
      return {
        data,
      };
    });
  },
  getDrawingDetails: (drawingId: string): Promise<{ data: DrawingFromBE }> => {
    return axiosInstance.get(`/drawings/${drawingId}`).then(() => {
      if (!extractDrawingDetailsFromJSON(drawingId)) {
        throw new Error(ERROR_NOT_FOUND);
      }
      return {
        data: extractDrawingDetailsFromJSON(drawingId),
      };
    });
  },
  updateDrawing: (drawingId: string, data: DrawingFromBE): Promise<void> => {
    // eslint-disable-next-line no-console
    console.log('api: save drawing', drawingId, data);
    return axiosInstance.post(`/drawings/${drawingId}/`, data);
  },
  saveDrawingAs: (data: DrawingDetails): Promise<string> => {
    // eslint-disable-next-line no-console
    console.log('api: save drawing as', data);
    return axiosInstance.post(`/drawings/`, data).then(() => 'new-drawing-id');
  },
  deleteDrawing: (drawingId: string): Promise<void> => {
    // eslint-disable-next-line no-console
    console.log('api: delete drawing', drawingId);
    drawingList.splice(
      drawingList.findIndex((drawing) => drawing.id === drawingId),
      1,
    );

    return axiosInstance.post(`/drawings/${drawingId}?delete=true`);
  },
});

// used in storybook
export const createApi = (): WarningsApi => {
  const instance = createFakeApiInstance();
  return getApiRoutes(instance);
};
