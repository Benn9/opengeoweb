/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */

import * as utils from '@opengeoweb/api';

import { createFakeApiInstance, CreateApiProps } from '@opengeoweb/api';
import { createApi, getIdFromUrl } from './api';
import { DrawingDetails, DrawingFromBE } from '../store/drawings/types';

describe('libs/utils/api', () => {
  describe('getIdFromUrl', () => {
    it('should an id from url', () => {
      expect(getIdFromUrl(null!)).toBeNull();
      expect(
        getIdFromUrl(
          'https://0.0.0.1:8081/drawings/bcf71f54-0da9-11ed-b5bd-0644f2de783d/',
        ),
      ).toEqual('bcf71f54-0da9-11ed-b5bd-0644f2de783d');
      expect(
        getIdFromUrl(
          'https://0.0.0.1:8081/bcf71f54-0da9-11ed-b5bd-0644f2de783d/',
        ),
      ).toEqual('bcf71f54-0da9-11ed-b5bd-0644f2de783d');
      expect(
        getIdFromUrl(
          'https://0.0.0.1:8081/drawings/acf71f54-xs90-11ed-b5bd-0644f2de783d/',
        ),
      ).toEqual('acf71f54-xs90-11ed-b5bd-0644f2de783d');
    });
  });

  const fakeAxiosInstance = createFakeApiInstance();
  const fakeApiParams: CreateApiProps = {
    config: {
      baseURL: 'fakeURL',
      appURL: 'fakeUrl',
      authTokenURL: 'anotherFakeUrl',
      authClientId: 'fakeauthClientId',
    },
    auth: {
      username: 'Michael Jackson',
      token: '1223344',
      refresh_token: '33455214',
    },
    onSetAuth: jest.fn(),
  };

  describe('getApiRoutes', () => {
    it('should call with the right params getDrawings', async () => {
      jest
        .spyOn(utils, 'createApiInstance')
        .mockReturnValueOnce(fakeAxiosInstance);
      const spy = jest.spyOn(fakeAxiosInstance, 'get');
      const api = createApi(fakeApiParams);

      await api.getDrawings();
      expect(spy).toHaveBeenCalledWith('/drawings');
    });

    it('should call with the right params getDrawingDetails', async () => {
      jest
        .spyOn(utils, 'createApiInstance')
        .mockReturnValueOnce(fakeAxiosInstance);
      const spy = jest.spyOn(fakeAxiosInstance, 'get');
      const api = createApi(fakeApiParams);

      await api.getDrawingDetails('test-id-1');
      expect(spy).toHaveBeenCalledWith('/drawings/test-id-1');
    });

    it('should call with the right params updateDrawing', async () => {
      jest
        .spyOn(utils, 'createApiInstance')
        .mockReturnValueOnce(fakeAxiosInstance);
      const spy = jest.spyOn(fakeAxiosInstance, 'post');
      const api = createApi(fakeApiParams);

      const drawingDetails: DrawingFromBE = {
        id: '345346456345',
        objectName: 'Object 88',
        lastUpdatedTime: '2022-05-26T12:34:27Z',
        scope: 'user',
        username: 'Michael',
        geoJSON: {
          type: 'FeatureCollection',
          features: [
            {
              type: 'Feature',
              properties: {
                fill: '#ff7800',
                'fill-opacity': 0.2,
                stroke: '#ff7800',
                'stroke-width': 2,
                'stroke-opacity': 1,
                selectionType: 'poly',
              },
              geometry: {
                type: 'Polygon',
                coordinates: [
                  [
                    [10.481821619038993, 58.535200764136974],
                    [8.640758193817074, 60.346399887508504],
                    [13.391889613744592, 67.5974266962731],
                    [16.658292464944765, 66.50937700061121],
                    [10.481821619038993, 58.535200764136974],
                  ],
                ],
              },
            },
          ],
        },
      };
      await api.updateDrawing(drawingDetails.id!, drawingDetails);
      expect(spy).toHaveBeenCalledWith(
        `/drawings/${drawingDetails.id}/`,
        drawingDetails,
      );
    });

    it('should call with the right params for saveDrawingAs', async () => {
      jest
        .spyOn(utils, 'createApiInstance')
        .mockReturnValueOnce(fakeAxiosInstance);
      const spy = jest.spyOn(fakeAxiosInstance, 'post');
      const api = createApi(fakeApiParams);

      const drawingDetails: DrawingDetails = {
        objectName: 'Object 88',
        scope: 'user',
        username: 'Michael',
        geoJSON: {
          type: 'FeatureCollection',
          features: [
            {
              type: 'Feature',
              properties: {
                fill: '#ff7800',
                'fill-opacity': 0.2,
                stroke: '#ff7800',
                'stroke-width': 2,
                'stroke-opacity': 1,
                selectionType: 'poly',
              },
              geometry: {
                type: 'Polygon',
                coordinates: [
                  [
                    [10.481821619038993, 58.535200764136974],
                    [8.640758193817074, 60.346399887508504],
                    [13.391889613744592, 67.5974266962731],
                    [16.658292464944765, 66.50937700061121],
                    [10.481821619038993, 58.535200764136974],
                  ],
                ],
              },
            },
          ],
        },
      };
      await api.saveDrawingAs(drawingDetails);
      expect(spy).toHaveBeenCalledWith(`/drawings/`, drawingDetails);
    });

    it('should call with the right params for deleteDrawing', async () => {
      jest
        .spyOn(utils, 'createApiInstance')
        .mockReturnValueOnce(fakeAxiosInstance);
      const spy = jest.spyOn(fakeAxiosInstance, 'post');
      const api = createApi(fakeApiParams);
      const drawingId = 'test-1';
      await api.deleteDrawing(drawingId);
      expect(spy).toHaveBeenCalledWith(`/drawings/${drawingId}?delete=true`);
    });
  });

  describe('createApi', () => {
    it('should return a WarningsApi object for authenticated instance', () => {
      const authApi = createApi({ auth: fakeApiParams.auth });
      expect(authApi).toBeDefined();
      expect(authApi.getDrawings).toBeDefined();
    });

    it('should return a WarningsApi object for non-authenticated instance', () => {
      const nonAuthApi = createApi({ auth: undefined });
      expect(nonAuthApi).toBeDefined();
      expect(nonAuthApi.getDrawings).toBeDefined();
    });
  });
});
