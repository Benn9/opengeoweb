/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */

import { createSlice, PayloadAction, Draft } from '@reduxjs/toolkit';
import { uiActions } from '@opengeoweb/store';
import { BaseDrawingListItem } from '../drawings/types';
import { publicWarningDialogType } from './utils';

export interface PublicWarningFormState {
  object?: BaseDrawingListItem;
}

export const initialState: PublicWarningFormState = {};

const slice = createSlice({
  initialState,
  name: 'publicWarningForm',
  reducers: {
    setFormValues: (
      draft: Draft<PublicWarningFormState>,
      action: PayloadAction<{
        object: BaseDrawingListItem;
      }>,
    ) => {
      draft.object = action.payload.object;
    },
  },
  extraReducers: (builder) => {
    builder.addCase(
      uiActions.setToggleOpenDialog,
      (draft: Draft<PublicWarningFormState>, action) => {
        const { type, setOpen } = action.payload;
        if (type === publicWarningDialogType && setOpen === false) {
          delete draft.object;
        }
      },
    );
  },
});

export const { reducer } = slice;
export const publicWarningActions = slice.actions;
