/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */

import { WarningModuleStore } from '../config';
import { getPublicWarningObject, getPublicWarningStore } from './selectors';
import { initialState } from './reducer';

describe('public warning selectors', () => {
  const initialTestState: WarningModuleStore = {
    publicWarningForm: {
      object: {
        id: '923723984872338768743',
        objectName: 'Drawing object 101',
        lastUpdatedTime: '2022-06-01T12:34:27Z',
      },
    },
  };

  it('should return draw store using getPublicWarningStore selector', () => {
    const publicWarningStore = getPublicWarningStore(initialTestState);
    expect(publicWarningStore).toEqual(initialTestState.publicWarningForm);
  });

  it('should return public warning object', () => {
    const publicWarningStore = getPublicWarningObject(initialTestState);
    expect(publicWarningStore).toEqual(
      initialTestState.publicWarningForm?.object,
    );
    expect(
      getPublicWarningObject({ publicWarningForm: initialState }),
    ).toBeUndefined();
  });
});
