/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */

import { WarningModuleStore } from '../config';
import { getDrawStore, selectDrawByInstanceId } from './selectors';

describe('draw selectors', () => {
  const initialState: WarningModuleStore = {
    drawings: {
      entities: {
        map1: {
          drawingInstanceId: 'map1',
          objectName: 'Object 1',
          id: 'drawing-id-1',
        },
        map2: { drawingInstanceId: 'map2', objectName: 'Object 2', id: '' },
      },
      ids: ['map1', 'map2'],
    },
  };

  it('should return draw store using getDrawStore selector', () => {
    const drawStore = getDrawStore(initialState);

    expect(drawStore).toEqual(initialState.drawings);
  });

  it('should select draw item by mapId using selectDrawByMapId selector', () => {
    const selectedDrawItem = selectDrawByInstanceId(initialState, 'map1');

    expect(selectedDrawItem).toEqual({
      drawingInstanceId: 'map1',
      objectName: 'Object 1',
      id: 'drawing-id-1',
    });
  });

  it('should return undefined for non-existent mapId using selectDrawByMapId selector', () => {
    const selectedDrawItem = selectDrawByInstanceId(
      initialState,
      'nonExistentMapId',
    );

    expect(selectedDrawItem).toBeUndefined();
  });

  it('should return undefined when drawings is not defined using selectDrawByMapId selector', () => {
    const selectedDrawItem = selectDrawByInstanceId({}, 'map1');

    expect(selectedDrawItem).toBeUndefined();
  });
});
