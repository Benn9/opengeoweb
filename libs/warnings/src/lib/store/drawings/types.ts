/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */

import { EntityState } from '@reduxjs/toolkit';

export interface DrawItem {
  id: string;
  objectName: string;
  drawingInstanceId: string;
}

export type DrawState = EntityState<DrawItem>;

export type DrawingScope = 'user' | 'global';

export interface BaseDrawingListItem {
  id: string;
  objectName: string;
  lastUpdatedTime: string;
}

export interface DrawingListItem extends BaseDrawingListItem {
  scope: DrawingScope;
  geoJSON: GeoJSON.FeatureCollection;
}

export interface DrawingFromBE extends DrawingListItem {
  username: string;
}

export interface DrawingDetails
  extends Omit<DrawingFromBE, 'id' | 'lastUpdatedTime'> {
  id?: string;
  lastUpdatedTime?: string;
}

// actions
export type SetDrawValuesPayload = DrawItem;

export type SubmitDrawValuesPayload = {
  id?: string;
  geoJSON: GeoJSON.FeatureCollection;
  objectName: string;
  lastUpdatedTime?: string;
  scope: DrawingScope;
};

export type PublicWarningsFormDataType = {
  phenomenon: string;
  validFrom: string;
  validUntil: string;
  leadtime: string;
  level: string;
  probability: number;
  descriptionOriginal: string;
  descriptionTranslation: string;
  objectId: string;
};
