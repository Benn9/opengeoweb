/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */
import { snackbarActions } from '@opengeoweb/snackbar';
import { uiActions } from '@opengeoweb/store';
import { SagaIterator } from 'redux-saga';
import { call, put, takeLatest } from 'redux-saga/effects';
import { getAxiosErrorMessage, isAxiosError } from '@opengeoweb/shared';
import { getWarningsApi } from '../../utils/api';
import { drawActions } from './reducer';
import { SubmitDrawValuesPayload } from './types';
import { drawingDialogType, objectDialogType } from './utils';

export const getSnackbarMessage = (objectName: string, id: string): string =>
  `Object ${objectName} has been ${id ? 'updated' : 'saved'}!`;

export function* submitDrawValuesSaga({
  payload,
}: {
  payload: SubmitDrawValuesPayload;
}): SagaIterator {
  const { id, ...formValues } = payload;

  try {
    yield put(
      uiActions.toggleIsLoadingDialog({
        isLoading: true,
        type: drawingDialogType,
      }),
    );
    const warningsApi = yield call(getWarningsApi);
    if (!id) {
      const newID = yield call(warningsApi.saveDrawingAs, formValues);
      yield put(
        drawActions.setDrawValues({
          drawingInstanceId: objectDialogType,
          id: newID,
          objectName: formValues.objectName,
        }),
      );
    } else {
      yield call(warningsApi.updateDrawing, id, formValues);
    }

    yield put(
      uiActions.setErrorDialog({
        type: drawingDialogType,
        error: '',
      }),
    );
    yield put(
      snackbarActions.openSnackbar({
        message: getSnackbarMessage(formValues.objectName, id!),
      }),
    );
    yield put(
      uiActions.toggleIsLoadingDialog({
        isLoading: false,
        type: drawingDialogType,
      }),
    );
  } catch (error) {
    yield put(
      uiActions.toggleIsLoadingDialog({
        isLoading: false,
        type: drawingDialogType,
      }),
    );

    const errorMessage = isAxiosError(error)
      ? getAxiosErrorMessage(error)
      : error.message;
    yield put(
      uiActions.setErrorDialog({
        type: drawingDialogType,
        error: errorMessage,
      }),
    );
  }
}

function* drawingSaga(): SagaIterator {
  yield takeLatest(drawActions.submitDrawValues, submitDrawValuesSaga);
}

export default drawingSaga;
