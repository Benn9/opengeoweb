/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */
import React from 'react';
import { lightTheme } from '@opengeoweb/theme';
import { createStore } from '@redux-eggs/redux-toolkit';
import { getSagaExtension } from '@redux-eggs/saga-extension';
import { WarningsThemeStoreProvider } from './components/Providers/Providers';
import { WarningsApi } from './utils/api';
import { createApi as createFakeApi } from './utils/fakeApi';
import { DrawingFromBE, DrawingListItem } from './store/drawings/types';
import { MapWithWarnings } from './storybookUtils/storyComponents';

const store = createStore({
  extensions: [getSagaExtension()],
});

export default {
  title: 'demo/errors',
};

const createFakeApiWithErrorOnSave = (): WarningsApi => {
  const currentFakeApi = createFakeApi();
  return {
    ...currentFakeApi,
    saveDrawingAs: (): Promise<string> => {
      return new Promise((_, reject) => {
        reject(
          new Error('Something went wrong while saving, please try again.'),
        );
      });
    },
    updateDrawing: (): Promise<void> => {
      return new Promise((_, reject) => {
        reject(
          new Error('Something went wrong while saving, please try again.'),
        );
      });
    },
  };
};

export const ErrorSaveDrawing = (): React.ReactElement => {
  return (
    <WarningsThemeStoreProvider
      createApi={createFakeApiWithErrorOnSave}
      theme={lightTheme}
      store={store}
    >
      <MapWithWarnings mapId="mapid_1" />
    </WarningsThemeStoreProvider>
  );
};

const createFakeApiWithErrorOnGetList = (): WarningsApi => {
  const currentFakeApi = createFakeApi();
  return {
    ...currentFakeApi,
    getDrawings: (): Promise<{ data: DrawingListItem[] }> => {
      return new Promise((_, reject) => {
        setTimeout(() => reject(new Error('Something went wrong')), 1000);
      });
    },
  };
};

export const ErrorGetList = (): React.ReactElement => {
  return (
    <WarningsThemeStoreProvider
      createApi={createFakeApiWithErrorOnGetList}
      theme={lightTheme}
      store={store}
    >
      <MapWithWarnings mapId="mapid_1" />
    </WarningsThemeStoreProvider>
  );
};

const createFakeApiWithErrorOnEdit = (): WarningsApi => {
  const currentFakeApi = createFakeApi();
  return {
    ...currentFakeApi,
    getDrawingDetails: (): Promise<{ data: DrawingFromBE }> => {
      return new Promise((_, reject) => {
        setTimeout(
          () => reject(new Error(`Error fetching drawing, please try again.`)),
          1000,
        );
      });
    },
  };
};

export const ErrorEditDrawing = (): React.ReactElement => {
  return (
    <WarningsThemeStoreProvider
      createApi={createFakeApiWithErrorOnEdit}
      theme={lightTheme}
      store={store}
    >
      <MapWithWarnings mapId="mapid_1" />
    </WarningsThemeStoreProvider>
  );
};

export const ErrorDeleteDrawing = (): React.ReactElement => {
  const createFakeApiWithError = (): WarningsApi => {
    const currentFakeApi = createFakeApi();
    return {
      ...currentFakeApi,
      deleteDrawing: (): Promise<void> => {
        return new Promise((_, reject) => {
          reject(new Error('Something went wrong'));
        });
      },
    };
  };
  return (
    <WarningsThemeStoreProvider
      createApi={createFakeApiWithError}
      theme={lightTheme}
      store={store}
    >
      <MapWithWarnings mapId="mapid_1" />
    </WarningsThemeStoreProvider>
  );
};
