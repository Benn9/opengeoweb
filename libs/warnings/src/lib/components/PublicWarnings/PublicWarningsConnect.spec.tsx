/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */
import React from 'react';

import {
  mapActions,
  uiActions,
  uiReducer,
  webmapReducer,
} from '@opengeoweb/store';
import { fireEvent, render, waitFor, screen } from '@testing-library/react';
import { configureStore } from '@reduxjs/toolkit';
import { Provider } from 'react-redux';
import { ThemeProvider } from '@opengeoweb/theme';
import { PublicWarningsConnect } from './PublicWarningsConnect';
import { DIALOG_TITLE } from './PublicWarnings';
import { publicWarningDialogType } from '../../store/publicWarningForm/utils';
import { reducer as publicWarningReducer } from '../../store/publicWarningForm/reducer';

describe('components/PublicWarningsConnect', () => {
  it('should render with default props', async () => {
    const props = {
      source: 'app' as const,
      startPosition: { top: 600, left: 800 },
    };
    const mapId = 'mapId1';
    const store = configureStore({
      reducer: {
        ui: uiReducer,
        webmap: webmapReducer,
        publicWarningForm: publicWarningReducer,
      },
    });
    store.dispatch(mapActions.registerMap({ mapId }));

    render(
      <Provider store={store}>
        <ThemeProvider>
          <PublicWarningsConnect {...props} />
        </ThemeProvider>
      </Provider>,
    );
    expect(screen.queryByText(DIALOG_TITLE)).toBeFalsy();

    expect(
      store.getState().ui.dialogs[publicWarningDialogType]?.isOpen,
    ).toBeFalsy();
    await waitFor(() => {
      // activate it
      store.dispatch(
        uiActions.setActiveMapIdForDialog({
          type: publicWarningDialogType,
          mapId,
          setOpen: true,
          source: props.source,
        }),
      );
    });
    expect(screen.getByText(DIALOG_TITLE)).toBeTruthy();

    const dialog = screen.getByRole('dialog');

    expect(getComputedStyle(dialog!).top).toEqual(
      `${props.startPosition.top}px`,
    );
    expect(getComputedStyle(dialog!).left).toEqual(
      `${props.startPosition.left}px`,
    );

    expect(
      store.getState().ui.dialogs[publicWarningDialogType]?.isOpen,
    ).toBeTruthy();

    // close it
    fireEvent.click(screen.getByRole('button', { name: /Close/i }));
    expect(
      store.getState().ui.dialogs[publicWarningDialogType]?.isOpen,
    ).toBeFalsy();
  });
});
