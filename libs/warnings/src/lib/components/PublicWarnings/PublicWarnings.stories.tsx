/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { Box } from '@mui/material';
import { darkTheme, lightTheme } from '@opengeoweb/theme';
import { PublicWarnings } from './PublicWarnings';
import { WarningsThemeProvider } from '../Providers/Providers';
import { LABEL_CURRENT, PublicWarningsForm } from '../PublicWarningsForm';

export default {
  title: 'components/PublicWarnings',
};

const props = {
  isOpen: true,
  onClose: (): void => {},
};

const formProps = {
  isDisabled: true,
  object: {
    id: '923723984872338768743',
    objectName: 'Drawing object 101',
    lastUpdatedTime: '2022-06-01T12:34:27Z',
  },
  publicWarningDetails: {
    phenomenon: 'coastalEvent',
    validFrom: '2022-06-01T15:00:00Z',
    validUntil: '2022-06-01T18:00:00Z',
    leadtime: LABEL_CURRENT,
    level: 'extreme',
    probability: 80,
    descriptionOriginal:
      'Some pretty intense coastal weather is coming our way',
    descriptionTranslation: 'And this would be the translation',
    objectId: '923723984872338768743',
  },
};

export const PublicWarningsLight = (): React.ReactElement => {
  return (
    <Box sx={{ width: '800px', height: '1000px' }}>
      <WarningsThemeProvider theme={lightTheme}>
        <PublicWarnings {...props} size={{ width: 320, height: 1000 }}>
          <PublicWarningsForm {...formProps} />
        </PublicWarnings>
      </WarningsThemeProvider>
    </Box>
  );
};

PublicWarningsLight.parameters = {
  zeplinLink: [
    {
      name: 'Light theme',
      link: 'https://app.zeplin.io/project/5ecf84a3c6ae1047a368f393/screen/651aa14b3c962721814061e4/version/651c3bb654916d54e604b10a',
    },
  ],
};

PublicWarningsLight.storyName = 'PublicWarnings light (takeSnapshot)';

export const PublicWarningsDark = (): React.ReactElement => {
  return (
    <Box sx={{ width: '800px', height: '1000px' }}>
      <WarningsThemeProvider theme={darkTheme}>
        <PublicWarnings {...props} size={{ width: 320, height: 1000 }}>
          <PublicWarningsForm {...formProps} />
        </PublicWarnings>
      </WarningsThemeProvider>
    </Box>
  );
};

PublicWarningsDark.parameters = {
  zeplinLink: [
    {
      name: 'Dark theme',
      link: 'https://app.zeplin.io/project/5ecf84a3c6ae1047a368f393/screen/651c3bc8617fc7550c8d796f/version/651c3bc8617fc7550c8d7970',
    },
  ],
};
PublicWarningsDark.storyName = 'PublicWarnings dark (takeSnapshot)';

export const PublicWarningsLightBig = (): React.ReactElement => {
  return (
    <WarningsThemeProvider theme={lightTheme}>
      <Box sx={{ width: '900px', height: '800px' }}>
        <PublicWarnings {...props} size={{ width: 800, height: 800 }}>
          <PublicWarningsForm {...formProps} />
        </PublicWarnings>
      </Box>
    </WarningsThemeProvider>
  );
};
PublicWarningsLightBig.storyName = 'PublicWarnings big light (takeSnapshot)';

export const PublicWarningsDarkBig = (): React.ReactElement => {
  return (
    <WarningsThemeProvider theme={darkTheme}>
      <Box sx={{ width: '900px', height: '800px' }}>
        <PublicWarnings {...props} size={{ width: 800, height: 800 }}>
          <PublicWarningsForm {...formProps} />
        </PublicWarnings>
      </Box>
    </WarningsThemeProvider>
  );
};
PublicWarningsDarkBig.storyName = 'PublicWarnings big dark (takeSnapshot)';
