/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */
import React from 'react';
import { createStore } from '@redux-eggs/redux-toolkit';
import { MapViewConnect } from '@opengeoweb/core';
import { AppHeader, AppLayout } from '@opengeoweb/shared';
import { PublicWarningsConnect } from './PublicWarningsConnect';
import { WarningsThemeStoreProvider } from '../Providers/Providers';
import { useDefaultMapSettings } from '../../storybookUtils/defaultStorySettings';
import { AppHamburgerMenu } from '../../storybookUtils/storyComponents';

export default {
  title: 'components/PublicWarningsConnect',
};

const MapWithPublicWarnings: React.FC<{ mapId: string }> = ({ mapId }) => {
  useDefaultMapSettings({
    mapId,
  });

  return (
    <AppLayout header={<AppHeader menu={<AppHamburgerMenu mapId={mapId} />} />}>
      <PublicWarningsConnect />
      <MapViewConnect mapId={mapId} />
    </AppLayout>
  );
};

export const PublicWarningsMap = (): React.ReactElement => {
  const store = createStore();
  return (
    <WarningsThemeStoreProvider store={store}>
      <MapWithPublicWarnings mapId="mapid_1" />
    </WarningsThemeStoreProvider>
  );
};
