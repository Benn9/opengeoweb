/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { fireEvent, render, screen } from '@testing-library/react';
import { DIALOG_TITLE, PublicWarnings } from './PublicWarnings';
import { WarningsThemeProvider } from '../Providers/Providers';

describe('components/PublicWarnings', () => {
  it('should render with default props', () => {
    const props = {
      isOpen: true,
      onClose: jest.fn(),
    };
    render(
      <WarningsThemeProvider>
        <PublicWarnings {...props}>some children</PublicWarnings>
      </WarningsThemeProvider>,
    );

    expect(screen.getByText(DIALOG_TITLE)).toBeTruthy();

    fireEvent.click(screen.getByRole('button', { name: /Close/i }));

    expect(props.onClose).toHaveBeenCalled();
    expect(screen.getByText('some children')).toBeTruthy();
  });

  it('should not render when isOpen is false', () => {
    const props = {
      isOpen: false,
      onClose: jest.fn(),
    };
    render(
      <WarningsThemeProvider>
        <PublicWarnings {...props}>some children</PublicWarnings>
      </WarningsThemeProvider>,
    );

    expect(screen.queryByText(DIALOG_TITLE)).toBeFalsy();
    expect(screen.queryByText('some children')).toBeFalsy();
  });
});
