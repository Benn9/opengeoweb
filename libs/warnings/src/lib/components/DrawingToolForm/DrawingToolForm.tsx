/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */
import {
  Box,
  Button,
  FormHelperText,
  Grid,
  MenuItem,
  SelectChangeEvent,
  Typography,
} from '@mui/material';
import {
  ReactHookFormHiddenInput,
  ReactHookFormProvider,
  ReactHookFormSelect,
  ReactHookFormTextField,
  defaultFormOptions,
  isValidGeoJsonCoordinates,
} from '@opengeoweb/form-fields';
import { AlertBanner, CustomIconButton, dateUtils } from '@opengeoweb/shared';
import * as React from 'react';
import { useFormContext } from 'react-hook-form';
import { clamp } from 'lodash';
import {
  getGeoJSONPropertyValue,
  DrawMode,
  getIcon,
  rewindGeometry,
} from '@opengeoweb/webmap-react';
import { FeatureCollection, GeoJsonProperties, Geometry } from 'geojson';
import { DATE_FORMAT } from '../../utils/constants';

export const opacityOptions = [100, 90, 80, 70, 60, 50, 40, 30, 20, 10, 0];
export const getObjectName = (objectName: string): string =>
  objectName || `${dateUtils.dateToString(new Date(), DATE_FORMAT, true)} UTC`;

export const SAVE_OBJECT_BUTTON = 'Save new object';
export const UPDATE_OBJECT_BUTTON = 'Update object';
export const INPUT_OBJECT = 'Object name';
export const INPUT_OPACITY = 'Opacity';
export const DRAW_EXIT_MODE_MESSAGE = 'press ESC to exit draw mode';

export const coordinatesEmptyMessage = 'An object drawing is required';

export interface DrawingToolFormValues {
  objectName: string;
  geoJSON: GeoJSON.FeatureCollection;
  id: string;
}

export interface DrawingToolFormComponentProps {
  onSubmitForm: (formValues: DrawingToolFormValues) => void;
  // eslint-disable-next-line react/no-unused-prop-types
  objectName?: string;
  isLoading?: boolean;
  error?: string;
  // eslint-disable-next-line react/no-unused-prop-types
  id?: string;
  drawModes: DrawMode[];
  onChangeDrawMode: (newMode: DrawMode) => void;
  activeDrawModeId: string;
  onDeactivateTool: () => void;
  isInEditMode: boolean;
  geoJSONProperties: GeoJSON.GeoJsonProperties;
  onChangeProperties: (properties: GeoJSON.GeoJsonProperties) => void;
  onChangeName: (name: string) => void;
  geoJSON: FeatureCollection<Geometry, GeoJsonProperties> | undefined;
}

const DrawingToolForm: React.FC<DrawingToolFormComponentProps> = ({
  onSubmitForm,
  isLoading,
  error,
  id = '',
  drawModes = [],
  onChangeDrawMode = (): void => {},
  activeDrawModeId = '',
  onDeactivateTool = (): void => {},
  isInEditMode = false,
  geoJSONProperties = {},
  onChangeProperties = (): void => {},
  onChangeName = (): void => {},
  geoJSON,
}: DrawingToolFormComponentProps) => {
  const {
    handleSubmit,
    setValue,
    formState: { errors, isDirty },
  } = useFormContext();

  React.useEffect(() => {
    if (activeDrawModeId) {
      setValue('geoJSON', geoJSON, {
        shouldDirty: true,
      });
    }
  }, [activeDrawModeId, geoJSON, setValue]);

  React.useEffect(() => {
    // Only set form value if not in edit mode
    if (!isInEditMode && !isLoading && geoJSON) {
      setValue('geoJSON', geoJSON, {
        shouldValidate: true,
      });
    }
  }, [isLoading, isInEditMode, geoJSON, setValue]);

  const onSubmit = (): void => {
    onDeactivateTool();
    handleSubmit((formValues) => {
      // remove unneeded properties
      const { opacity, geoJSON, ...allFormValues } = formValues;
      // rewind geometry
      const rewindedGeometry = rewindGeometry(geoJSON);
      onSubmitForm({
        ...allFormValues,
        id,
        geoJSON: rewindedGeometry,
      } as DrawingToolFormValues);
    })();
  };

  const hasErrors = Object.keys(errors).length > 0;

  const opacity = React.useMemo(
    () =>
      getGeoJSONPropertyValue(
        'fill-opacity',
        geoJSONProperties,
        drawModes.find((mode) => mode.value === 'POLYGON'),
      ) as number,
    [geoJSONProperties, drawModes],
  );

  const onChangeOpacity = (event: SelectChangeEvent): void => {
    const { value } = event.target;
    const newOpacity = parseInt(value, 10) / 100;
    const additionalStrokeOpacity = 0.8;
    onChangeProperties({
      'fill-opacity': newOpacity,
      'stroke-opacity': clamp(newOpacity + additionalStrokeOpacity, 0.01, 1),
    });
  };

  const onKeyDown = (event: React.KeyboardEvent<HTMLElement>): void => {
    if (event.key === 'Escape') {
      onDeactivateTool();
    }
  };

  const onBlurName = (event: React.FocusEvent<HTMLInputElement>): void => {
    onChangeName(event.target.value);
  };

  return (
    <>
      <Box
        sx={{
          height: 'calc( 100% - 72px )',
          overflow: 'auto',
        }}
      >
        {error && (
          <Box
            sx={{
              width: '100%',
            }}
          >
            <AlertBanner title={error} shouldClose />
          </Box>
        )}
        <Grid
          data-testid="drawingToolForm"
          container
          direction="column"
          spacing={1}
          padding={2}
        >
          <Grid item>
            <Typography variant="body2">Tools</Typography>
          </Grid>
          <Grid item container>
            <Grid item>
              {drawModes.map((mode) => (
                <CustomIconButton
                  key={mode.drawModeId}
                  variant="tool"
                  tooltipTitle={mode.title}
                  isSelected={
                    mode.isSelectable && activeDrawModeId === mode.drawModeId
                  }
                  onClick={(): void => onChangeDrawMode(mode)}
                  sx={{ marginRight: 1, marginBottom: 1 }}
                  data-testid={mode.drawModeId}
                  onKeyDown={onKeyDown}
                  disabled={isLoading}
                  size="medium"
                >
                  {getIcon(mode.drawModeId)}
                </CustomIconButton>
              ))}
              {isInEditMode && (
                <Typography
                  component="span"
                  sx={{
                    display: 'inline-block',
                    marginLeft: 2,
                    top: -1,
                    position: 'relative',
                    fontSize: 14,
                  }}
                >
                  {DRAW_EXIT_MODE_MESSAGE}
                </Typography>
              )}
              {!!errors.geoJSON && isDirty && (
                <FormHelperText error variant="filled" sx={{ marginLeft: 0 }}>
                  {errors.geoJSON.message as string}
                </FormHelperText>
              )}
              <ReactHookFormHiddenInput
                name="geoJSON"
                rules={{
                  validate: {
                    coordinatesNotEmpty: (value): boolean | string =>
                      isValidGeoJsonCoordinates(value!) ||
                      coordinatesEmptyMessage,
                  },
                }}
              />
            </Grid>
          </Grid>
          <Grid item container>
            <Grid item xs={12} sm={6}>
              <ReactHookFormSelect
                name="opacity"
                label={INPUT_OPACITY}
                rules={{}}
                disabled={isLoading}
                isReadOnly={false}
                data-testid="drawingToolForm-opacity"
                onChange={onChangeOpacity}
                autoFocus
                value={opacity * 100}
              >
                {opacityOptions.map((opacity) => (
                  <MenuItem value={opacity} key={opacity}>
                    {opacity} %
                  </MenuItem>
                ))}
              </ReactHookFormSelect>
            </Grid>
          </Grid>
          <Grid item>
            <ReactHookFormTextField
              name="objectName"
              label={INPUT_OBJECT}
              rules={{ required: true }}
              disabled={isLoading}
              isReadOnly={false}
              onBlur={onBlurName}
            />
            <ReactHookFormHiddenInput name="id" />
          </Grid>
        </Grid>
      </Box>
      <Box
        sx={{
          padding: '24px 10px 14px 10px',
          position: 'absolute',
          bottom: 0,
          width: '100%',
          backgroundColor: 'geowebColors.background.surfaceApp',
          boxShadow: '0 2px 4px 0 rgba(0, 0, 0, 0.5)',
          zIndex: 1,
        }}
      >
        <Button
          variant="primary"
          onClick={onSubmit}
          sx={{ width: '100%', textTransform: 'none', fontSize: '12px' }}
          data-testid="saveDrawingFormButton"
          disabled={isLoading || hasErrors}
        >
          {id !== '' ? UPDATE_OBJECT_BUTTON : SAVE_OBJECT_BUTTON}
        </Button>
      </Box>
    </>
  );
};

const Wrapper: React.FC<DrawingToolFormComponentProps> = (
  props: DrawingToolFormComponentProps,
) => {
  const { id = '', objectName = '', geoJSON = undefined } = props;
  return (
    <ReactHookFormProvider
      options={{
        ...defaultFormOptions,
        defaultValues: {
          id,
          objectName: getObjectName(objectName),
          geoJSON,
        },
      }}
    >
      <DrawingToolForm {...props} />
    </ReactHookFormProvider>
  );
};

export { Wrapper as DrawingToolForm };

export default Wrapper;
