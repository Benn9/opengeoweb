/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */
import * as React from 'react';
import { render, screen } from '@testing-library/react';
import { createStore } from '@redux-eggs/redux-toolkit';
import { WarningsThemeProvider, WarningsThemeStoreProvider } from './Providers';

describe('lib/Providers/Providers', () => {
  describe('WarningsThemeProvider', () => {
    it('should render', () => {
      render(<WarningsThemeProvider>testing</WarningsThemeProvider>);
      expect(screen.getByText('testing')).toBeTruthy();
    });
  });

  describe('WarningsThemeStoreProvider', () => {
    it('should render', () => {
      const store = createStore();
      render(
        <WarningsThemeStoreProvider store={store}>
          testing
        </WarningsThemeStoreProvider>,
      );
      expect(screen.getByText('testing')).toBeTruthy();
    });
  });
});
