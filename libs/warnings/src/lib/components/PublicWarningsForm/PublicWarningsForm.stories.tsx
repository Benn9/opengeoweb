/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */

import { Paper } from '@mui/material';
import * as React from 'react';
import { darkTheme } from '@opengeoweb/theme';
import { LABEL_CURRENT, PublicWarningsForm } from './PublicWarningsForm';
import { WarningsThemeProvider } from '../Providers/Providers';

export default {
  title: 'components/PublicWarningsForm',
};

const defaultProps = {
  isDisabled: true,
};

const props = {
  ...defaultProps,
  object: {
    id: '923723984872338768743',
    objectName: 'Drawing object 101',
    lastUpdatedTime: '2022-06-01T12:34:27Z',
  },
  publicWarningDetails: {
    phenomenon: 'coastalEvent',
    validFrom: '2022-06-01T15:00:00Z',
    validUntil: '2022-06-01T18:00:00Z',
    leadtime: LABEL_CURRENT,
    level: 'extreme',
    probability: 80,
    descriptionOriginal:
      'Some pretty intense coastal weather is coming our way',
    descriptionTranslation: 'And this would be the translation',
    objectId: '923723984872338768743',
  },
};

export const PublicWarningsFormDemo = (): React.ReactElement => {
  return (
    <WarningsThemeProvider>
      <Paper sx={{ maxWidth: 1200, height: 'calc(100vh)' }}>
        <PublicWarningsForm {...props} />
      </Paper>
    </WarningsThemeProvider>
  );
};
PublicWarningsFormDemo.storyName = 'PublicWarningsForm light';

export const PublicWarningsFormDemoDark = (): React.ReactElement => {
  return (
    <WarningsThemeProvider theme={darkTheme}>
      <Paper sx={{ maxWidth: 1200, height: 'calc(100vh)' }}>
        <PublicWarningsForm {...props} />
      </Paper>
    </WarningsThemeProvider>
  );
};
PublicWarningsFormDemoDark.storyName = 'PublicWarningsForm dark';

export const PublicWarningsEmptyFormDemo = (): React.ReactElement => {
  return (
    <WarningsThemeProvider>
      <Paper sx={{ maxWidth: 1200, height: 'calc(100vh)' }}>
        <PublicWarningsForm {...defaultProps} />
      </Paper>
    </WarningsThemeProvider>
  );
};
PublicWarningsEmptyFormDemo.storyName = 'PublicWarningsForm empty light';

export const PublicWarningsEmptyFormDemoDark = (): React.ReactElement => {
  return (
    <WarningsThemeProvider theme={darkTheme}>
      <Paper sx={{ maxWidth: 1200, height: 'calc(100vh)' }}>
        <PublicWarningsForm {...defaultProps} />
      </Paper>
    </WarningsThemeProvider>
  );
};
PublicWarningsEmptyFormDemoDark.storyName = 'PublicWarningsForm empty dark';
