/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */

import {
  Box,
  Button,
  FormControlLabel,
  Grid,
  ListItemIcon,
  MenuItem,
  Radio,
  Theme,
  styled,
} from '@mui/material';
import {
  ReactHookFormDateTime,
  ReactHookFormProvider,
  ReactHookFormRadioGroup,
  ReactHookFormSelect,
  defaultFormOptions,
} from '@opengeoweb/form-fields';
import {
  CoastalEvent,
  Fog,
  Lightning,
  Rain,
  Snow,
  TemperatureHigh,
  TemperatureLow,
  Wind,
  breakpoints,
} from '@opengeoweb/theme';
import * as React from 'react';
import { dateUtils } from '@opengeoweb/shared';
import { useFormContext } from 'react-hook-form';
import { DescriptionField } from './DescriptionField';
import { AreaField } from './AreaField';
import {
  BaseDrawingListItem,
  PublicWarningsFormDataType,
} from '../../store/drawings/types';
import { DATE_FORMAT_UTC } from '../../utils/constants';

export const getEmptyPublicWarning = (
  object?: BaseDrawingListItem,
): PublicWarningsFormDataType => {
  const currentTime = new Date();
  const validToTime = dateUtils.add(currentTime, { hours: 6 });

  return {
    phenomenon: '',
    validFrom: dateUtils.dateToString(currentTime, DATE_FORMAT_UTC, true),
    validUntil: dateUtils.dateToString(validToTime, DATE_FORMAT_UTC, true),
    leadtime: LABEL_CURRENT,
    level: '',
    probability: 30,
    descriptionOriginal: '',
    descriptionTranslation: '',
    objectId: object?.id || '',
  };
};

// tablet breakpoint - 2 padding * 2 for both sides
const containerQueryBreakpoint = `@container (min-width: ${
  breakpoints.tablet - 32
}px)`;

export const LABEL_PHENOMENON = 'Phenomenon';
export const LABEL_VALID_FROM = 'Valid from';
export const LABEL_VALID_UNTIL = 'Valid until';
export const LABEL_CURRENT = 'Current';
export const LABEL_EARLY = 'Early';
export const LABEL_LEVEL = 'Level';
export const LABEL_PROBABILITY = 'Probability';
export const BUTTON_CLEAR = 'Clear';
export const BUTTON_SAVE = 'Save';
export const BUTTON_PUBLISH = 'Publish';

const dateFormat = 'DD/MM/YYYY HH:mm';

const probablityOptions = [100, 90, 80, 70, 60, 50, 40, 30, 20, 10, 0];
const levelOptions = [
  {
    title: 'Moderate',
    key: 'moderate',
    color: '#FAE21E',
    textColor: '#051039',
  },
  { title: 'Severe', key: 'severe', color: '#E27000', textColor: '#051039' },
  { title: 'Extreme', key: 'extreme', color: '#D62A20', textColor: '#ffffff' },
];
export const warningPhenomena = [
  { title: 'Coastal event', key: 'coastalEvent', icon: <CoastalEvent /> },
  { title: 'Fog', key: 'fog', icon: <Fog /> },
  { title: 'High temperature', key: 'highTemp', icon: <TemperatureHigh /> },
  { title: 'Low temperature', key: 'lowTemp', icon: <TemperatureLow /> },
  { title: 'Rain', key: 'rain', icon: <Rain /> },
  { title: 'Snow/ice', key: 'snowIce', icon: <Snow /> },
  { title: 'Thunderstorm', key: 'thunderstorm', icon: <Lightning /> },
  { title: 'Wind', key: 'wind', icon: <Wind /> },
];

const ContainerWrapper = styled('div')(() => ({
  containerType: 'size',
  height: '100%',
}));

const ContainerFormWrapper = styled('div')(({ theme }) => ({
  padding: theme.spacing(2),
  height: 'calc(100% - 206px)',
  overflowY: 'auto',
  [containerQueryBreakpoint]: {
    height: 'calc(100% - 88px)',
  },
}));

const ContainerGrid = styled('div')(({ theme }) => ({
  backgroundColor: theme.palette.background.paper,
  '.MuiGrid-grid-sm-6, .MuiGrid-grid-sm-4': {
    width: '100%',
    minWidth: '100%',
  },
  [containerQueryBreakpoint]: {
    '.MuiGrid-grid-sm-6': {
      width: '50%',
      minWidth: '50%',
    },
    '.MuiGrid-grid-sm-4': {
      width: '33.33%',
      minWidth: '33.33%',
    },
  },
}));

interface FormProps {
  isDisabled?: boolean;
  object?: BaseDrawingListItem;
}

const Form: React.FC<FormProps> = ({ isDisabled = false, object }) => {
  const { watch } = useFormContext();

  const selectedLevel = watch('level');
  const levelDetails = levelOptions.find(
    (level) => level.key === selectedLevel,
  );

  return (
    <ContainerWrapper>
      <ContainerFormWrapper>
        <ContainerGrid>
          <Grid container rowSpacing={2}>
            <Grid item xs={12}>
              <ReactHookFormSelect
                name="phenomenon"
                label={LABEL_PHENOMENON}
                rules={{}}
              >
                {warningPhenomena.map((phenomenon) => {
                  return (
                    <MenuItem key={phenomenon.key} value={phenomenon.key}>
                      <Grid container justifyContent="space-between">
                        <Grid item>{phenomenon.title}</Grid>
                        <Grid item>
                          <ListItemIcon>{phenomenon.icon}</ListItemIcon>
                        </Grid>
                      </Grid>
                    </MenuItem>
                  );
                })}
              </ReactHookFormSelect>
            </Grid>

            <Grid item xs={12}>
              <Grid item xs={12}>
                <AreaField
                  isDisabled
                  onEditObject={(): void => {}}
                  onViewObject={(): void => {}}
                  onDeleteObject={(): void => {}}
                  onAddObject={(): void => {}}
                  id={object?.id}
                  objectName={object?.objectName}
                  lastUpdatedTime={object?.lastUpdatedTime}
                />
              </Grid>
            </Grid>

            <Grid item xs={12} container spacing={2}>
              <Grid item xs={12} sm={6}>
                <ReactHookFormDateTime
                  name="validFrom"
                  rules={{}}
                  label={LABEL_VALID_FROM}
                  format={dateFormat}
                />
              </Grid>
              <Grid item xs={12} sm={6}>
                <ReactHookFormDateTime
                  name="validUntil"
                  rules={{}}
                  label={LABEL_VALID_UNTIL}
                  format={dateFormat}
                />
              </Grid>
            </Grid>

            <ReactHookFormRadioGroup
              name="leadtime"
              rules={{}}
              sx={{ marginBottom: -1 }}
            >
              <Grid item xs={12} container spacing={2}>
                <Grid item xs={6}>
                  <FormControlLabel
                    sx={{ padding: 1, paddingBottom: 0 }}
                    control={<Radio style={{ pointerEvents: 'auto' }} />}
                    label={LABEL_CURRENT}
                    value={LABEL_CURRENT}
                  />
                </Grid>
                <Grid item xs={6}>
                  <FormControlLabel
                    sx={{ padding: 1, paddingBottom: 0 }}
                    control={<Radio style={{ pointerEvents: 'auto' }} />}
                    label={LABEL_EARLY}
                    value={LABEL_EARLY}
                  />
                </Grid>
              </Grid>
            </ReactHookFormRadioGroup>

            <Grid item xs={12} container spacing={2}>
              <Grid item xs={12} sm={6}>
                <ReactHookFormSelect
                  name="level"
                  label={LABEL_LEVEL}
                  rules={{}}
                  sx={{
                    '& .MuiSelect-select.MuiSelect-filled': {
                      color: levelDetails?.textColor || 'none',
                      backgroundColor: levelDetails?.color || 'none',
                      backgroundClip: 'content-box',
                    },
                  }}
                >
                  {levelOptions.map((level) => (
                    <MenuItem
                      value={level.key}
                      key={level.key}
                      sx={{
                        '&.MuiMenuItem-root ': {
                          color: level.textColor,
                          background: level.color,
                          backgroundClip: 'content-box',
                          padding: '12px 8px',
                          '&:hover': {
                            backgroundColor: `${level.color}`,
                            backgroundClip: 'content-box',
                            boxShadow: (theme: Theme): string =>
                              `inset -8px -12px ${theme.palette.geowebColors.cards.cardContainerMouseOver}, inset 8px 12px ${theme.palette.geowebColors.cards.cardContainerMouseOver}`,
                          },
                          '&.Mui-selected': {
                            backgroundColor: `${level.color}`,
                            '&:hover': {
                              boxShadow: (theme: Theme): string =>
                                `inset 4px 0px 0px 0px ${theme.palette.geowebColors.typographyAndIcons.iconLinkActive}, inset -8px -12px ${theme.palette.geowebColors.cards.cardContainerMouseOver}, inset 8px 12px ${theme.palette.geowebColors.cards.cardContainerMouseOver}`,
                            },
                          },
                        },
                      }}
                    >
                      &nbsp;{level.title}
                    </MenuItem>
                  ))}
                </ReactHookFormSelect>
              </Grid>
              <Grid item xs={12} sm={6}>
                <ReactHookFormSelect
                  name="probability"
                  label={LABEL_PROBABILITY}
                  rules={{}}
                >
                  {probablityOptions.map((probability) => (
                    <MenuItem value={probability} key={probability}>
                      {probability} %
                    </MenuItem>
                  ))}
                </ReactHookFormSelect>
              </Grid>
            </Grid>

            <Grid item xs={12} container spacing={2}>
              <Grid item xs={12}>
                <DescriptionField />
              </Grid>
            </Grid>
          </Grid>
        </ContainerGrid>
      </ContainerFormWrapper>
      <Box
        sx={{
          padding: '24px 10px 14px 10px',
          position: 'absolute',
          bottom: 0,
          width: '100%',
          backgroundColor: 'geowebColors.background.surfaceApp',
          boxShadow: '0 2px 4px 0 rgba(0, 0, 0, 0.5)',
          zIndex: 1,
        }}
      >
        <ContainerGrid>
          <Grid container spacing={2}>
            <Grid item xs={12} sm={4}>
              <Button
                variant="tertiary"
                sx={{
                  width: '100%',
                  marginBottom: 1,
                }}
                disabled={isDisabled}
              >
                {BUTTON_CLEAR}
              </Button>
            </Grid>
            <Grid item xs={12} sm={4}>
              <Button
                variant="tertiary"
                sx={{
                  width: '100%',
                  marginBottom: 1,
                }}
                disabled={isDisabled}
              >
                {BUTTON_SAVE}
              </Button>
            </Grid>
            <Grid item xs={12} sm={4}>
              <Button
                variant="primary"
                sx={{
                  width: '100%',
                }}
                disabled={isDisabled}
              >
                {BUTTON_PUBLISH}
              </Button>
            </Grid>
          </Grid>
        </ContainerGrid>
      </Box>
    </ContainerWrapper>
  );
};

interface PublicWarningsFormProps extends FormProps {
  publicWarningDetails?: PublicWarningsFormDataType;
}

export const PublicWarningsForm: React.FC<PublicWarningsFormProps> = (
  props,
) => {
  const { object, publicWarningDetails } = props;

  const publicWarningFormData =
    publicWarningDetails || getEmptyPublicWarning(object);

  return (
    <ReactHookFormProvider
      options={{
        ...defaultFormOptions,
        defaultValues: publicWarningFormData,
      }}
    >
      <Form {...props} />
    </ReactHookFormProvider>
  );
};
