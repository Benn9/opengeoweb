/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { configureStore } from '@reduxjs/toolkit';
import { render, screen } from '@testing-library/react';
import { Provider } from 'react-redux';
import { ThemeProvider } from '@opengeoweb/theme';
import { PublicWarningsFormConnect } from './PublicWarningsFormConnect';

import {
  publicWarningActions,
  reducer as publicWarningReducer,
} from '../../store/publicWarningForm/reducer';
import { NO_OBJECT, OBJECTS_MENU, formatDate } from './AreaField';

describe('components/PublicWarningsFormConnect', () => {
  it('should render with default props', () => {
    const store = configureStore({
      reducer: {
        publicWarningForm: publicWarningReducer,
      },
    });

    render(
      <Provider store={store}>
        <ThemeProvider>
          <PublicWarningsFormConnect />
        </ThemeProvider>
      </Provider>,
    );

    expect(store.getState().publicWarningForm.object).toBeUndefined();
    expect(screen.getByText(NO_OBJECT)).toBeTruthy();
    expect(screen.queryByRole('button', { name: OBJECTS_MENU })).toBeFalsy();
  });

  it('should render prefilled object values', () => {
    const store = configureStore({
      reducer: {
        publicWarningForm: publicWarningReducer,
      },
    });
    const testObject = {
      id: 'test',
      lastUpdatedTime: '2022-06-01T12:34:27Z',
      objectName: 'my test area',
    };

    store.dispatch(publicWarningActions.setFormValues({ object: testObject }));

    render(
      <Provider store={store}>
        <ThemeProvider>
          <PublicWarningsFormConnect />
        </ThemeProvider>
      </Provider>,
    );

    expect(store.getState().publicWarningForm.object).toEqual(testObject);
    expect(screen.getByText(testObject.objectName)).toBeTruthy();
    expect(
      screen.getByText(formatDate(testObject.lastUpdatedTime)),
    ).toBeTruthy();
    expect(screen.getByRole('button', { name: OBJECTS_MENU })).toBeTruthy();
  });
});
