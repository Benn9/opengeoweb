/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { render, screen } from '@testing-library/react';

import {
  BUTTON_CLEAR,
  BUTTON_PUBLISH,
  BUTTON_SAVE,
  LABEL_CURRENT,
  LABEL_EARLY,
  LABEL_LEVEL,
  LABEL_PHENOMENON,
  LABEL_PROBABILITY,
  LABEL_VALID_FROM,
  LABEL_VALID_UNTIL,
  PublicWarningsForm,
} from './PublicWarningsForm';
import { WarningsThemeProvider } from '../Providers/Providers';
import { OBJECTS_MENU, formatDate } from './AreaField';

describe('components/PublicWarningsForm/DescriptionField', () => {
  it('should render with default props', () => {
    render(
      <WarningsThemeProvider>
        <PublicWarningsForm />
      </WarningsThemeProvider>,
    );

    expect(screen.getByText(LABEL_PHENOMENON)).toBeTruthy();
    expect(screen.getByText(LABEL_VALID_FROM)).toBeTruthy();
    expect(screen.getByText(LABEL_VALID_UNTIL)).toBeTruthy();
    expect(screen.getByText(LABEL_CURRENT)).toBeTruthy();
    expect(screen.getByText(LABEL_EARLY)).toBeTruthy();
    expect(screen.getByText(LABEL_LEVEL)).toBeTruthy();
    expect(screen.getByText(LABEL_PROBABILITY)).toBeTruthy();
    expect(screen.getByText(BUTTON_CLEAR)).toBeTruthy();
    expect(screen.getByText(BUTTON_SAVE)).toBeTruthy();
    expect(screen.getByText(BUTTON_PUBLISH)).toBeTruthy();
  });

  it('should render as disabled', () => {
    render(
      <WarningsThemeProvider>
        <PublicWarningsForm isDisabled />
      </WarningsThemeProvider>,
    );

    expect(
      screen.getByText(BUTTON_CLEAR).getAttribute('disabled'),
    ).toBeDefined();
    expect(
      screen.getByText(BUTTON_SAVE).getAttribute('disabled'),
    ).toBeDefined();
    expect(
      screen.getByText(BUTTON_PUBLISH).getAttribute('disabled'),
    ).toBeDefined();
  });

  it('should render object', () => {
    const props = {
      object: {
        id: 'test',
        lastUpdatedTime: '2022-06-01T12:34:27Z',
        objectName: 'my test area',
      },
    };
    render(
      <WarningsThemeProvider>
        <PublicWarningsForm {...props} />
      </WarningsThemeProvider>,
    );

    expect(screen.getByText(props.object.objectName)).toBeTruthy();
    expect(
      screen.getByText(formatDate(props.object.lastUpdatedTime)),
    ).toBeTruthy();
    expect(screen.getByRole('button', { name: OBJECTS_MENU })).toBeTruthy();
  });

  it('should render with default form properties if none passed', () => {
    jest.useFakeTimers().setSystemTime(new Date('2021-12-20T14:00:00Z'));

    render(
      <WarningsThemeProvider>
        <PublicWarningsForm />
      </WarningsThemeProvider>,
    );

    expect(screen.getByText(LABEL_PHENOMENON)).toBeTruthy();
    expect(screen.getByText('No object selected')).toBeTruthy();
    // expect(screen.getByText('20/12/2021 14:00')).toBeTruthy();
    // date end
    expect(
      screen.getByRole('radio', { name: 'Current', checked: true }),
    ).toBeTruthy();
    expect(screen.getByText(LABEL_LEVEL)).toBeTruthy();
    expect(screen.getByText('30 %')).toBeTruthy();
  });

  it('should render with form properties if passed', () => {
    const props = {
      object: {
        id: '923723984872338768743',
        objectName: 'Drawing object 101',
        lastUpdatedTime: '2022-06-01T12:34:27Z',
      },
      publicWarningDetails: {
        phenomenon: 'coastalEvent',
        validFrom: '2022-06-01T15:00:00Z',
        validUntil: '2022-06-01T18:00:00Z',
        leadtime: LABEL_EARLY,
        level: 'extreme',
        probability: 80,
        descriptionOriginal:
          'Some pretty intense coastal weather is coming our way',
        descriptionTranslation: 'And this would be the translation',
        objectId: '923723984872338768743',
      },
    };

    render(
      <WarningsThemeProvider>
        <PublicWarningsForm {...props} />
      </WarningsThemeProvider>,
    );

    expect(screen.getByText('Coastal event')).toBeTruthy();
    expect(screen.getByText('Drawing object 101')).toBeTruthy();
    expect(
      screen
        .getByRole('textbox', {
          name: 'Valid from',
        })
        .getAttribute('value') || '',
    ).toEqual('01/06/2022 15:00');
    expect(
      screen
        .getByRole('textbox', {
          name: 'Valid until',
        })
        .getAttribute('value') || '',
    ).toEqual('01/06/2022 18:00');
    expect(
      screen.getByRole('radio', { name: 'Early', checked: true }),
    ).toBeTruthy();
    expect(screen.getByText('Extreme')).toBeTruthy();
    expect(screen.getByText('80 %')).toBeTruthy();
  });
});
