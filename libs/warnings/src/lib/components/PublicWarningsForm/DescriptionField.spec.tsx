/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */

import { ReactHookFormProvider } from '@opengeoweb/form-fields';
import React from 'react';
import { fireEvent, render, screen } from '@testing-library/react';
import { WarningsThemeProvider } from '../Providers/Providers';
import { DescriptionField } from './DescriptionField';

describe('components/PublicWarningsForm/DescriptionField', () => {
  it('should be able to switch tabs', () => {
    render(
      <WarningsThemeProvider>
        <ReactHookFormProvider>
          <DescriptionField />
        </ReactHookFormProvider>
      </WarningsThemeProvider>,
    );
    const tab1Test = 'testing tab 1 content';
    fireEvent.change(screen.getByRole('textbox'), {
      target: { value: tab1Test },
    });

    expect(screen.getByText(tab1Test)).toBeTruthy();

    // switch to tab 2
    fireEvent.click(
      screen.getByRole('tab', { name: /Description translation/i }),
    );
    expect(screen.queryByText(tab1Test)).toBeFalsy();

    const tab2Test = 'testing tab 2 content';
    fireEvent.change(screen.getByRole('textbox'), {
      target: { value: tab2Test },
    });
    expect(screen.getByText(tab2Test)).toBeTruthy();

    // switch to tab 1
    fireEvent.click(screen.getByRole('tab', { name: /Description original/i }));
    expect(screen.getByText(tab1Test)).toBeTruthy();
    expect(screen.queryByText(tab2Test)).toBeFalsy();

    // switch to tab 2
    fireEvent.click(
      screen.getByRole('tab', { name: /Description translation/i }),
    );
    expect(screen.getByText(tab2Test)).toBeTruthy();
    expect(screen.queryByText(tab1Test)).toBeFalsy();
  });
});
