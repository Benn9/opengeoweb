/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */

import { fireEvent, render, screen } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import React from 'react';
import { ObjectManager } from './ObjectManager';
import { WarningsThemeProvider } from '../Providers/Providers';
import { DrawingListItem } from '../../store/drawings/types';
import { BUTTON_PUBLIC_WARNING, BUTTON_SHARE } from './Objects';

describe('src/components/ObjectManager/ObjectManager', () => {
  it('should be able to click on a drawing in the list', async () => {
    const props = {
      onClose: jest.fn(),
      isOpen: true,
      drawingListItems: [
        {
          id: '923723984872338768743',
          objectName: 'Drawing object 101',
          lastUpdatedTime: '2022-06-01T12:34:27Z',
          scope: 'user',
        },
        {
          id: '934834893283',
          objectName: 'Object 89',
          lastUpdatedTime: '2022-05-26T12:34:27Z',
          scope: 'user',
        },
        {
          id: '345346456345',
          objectName: 'Object 88',
          lastUpdatedTime: '2022-05-26T12:34:27Z',
          scope: 'user',
        },
      ] as DrawingListItem[],
      onClickDrawing: jest.fn(),
      onDeleteDrawing: jest.fn(),
      onUnMount: jest.fn(),
    };

    render(
      <WarningsThemeProvider>
        <ObjectManager {...props} />
      </WarningsThemeProvider>,
    );

    expect(screen.queryByRole('progressbar')).toBeFalsy();

    fireEvent.click(screen.getByText(props.drawingListItems[0].objectName));

    expect(props.onClickDrawing).toHaveBeenCalledWith(
      props.drawingListItems[0],
    );

    fireEvent.click(screen.getByText(props.drawingListItems[1].objectName));
    expect(props.onClickDrawing).toHaveBeenCalledWith(
      props.drawingListItems[1],
    );

    // select the same
    fireEvent.click(screen.getByText(props.drawingListItems[1].objectName));
    expect(props.onClickDrawing).toHaveBeenCalledWith(
      props.drawingListItems[1],
    );
    // press delete
    fireEvent.click(
      screen.queryAllByRole('button', { name: /Object options/i })[0],
    );
    const deleteElement = await screen.findByText('Delete');
    fireEvent.click(deleteElement);

    expect(props.onDeleteDrawing).toHaveBeenCalledWith(
      props.drawingListItems[0].id,
    );
  });

  it('should drawing list item as active', () => {
    const props = {
      onClose: jest.fn(),
      isOpen: true,
      drawingListItems: [
        {
          id: '923723984872338768743',
          objectName: 'Drawing object 101',
          lastUpdatedTime: '2022-06-01T12:34:27Z',
          scope: 'user',
        },
        {
          id: '934834893283',
          objectName: 'Object 89',
          lastUpdatedTime: '2022-05-26T12:34:27Z',
          scope: 'user',
        },
        {
          id: '345346456345',
          objectName: 'Object 88',
          lastUpdatedTime: '2022-05-26T12:34:27Z',
          scope: 'user',
        },
      ] as DrawingListItem[],
      onClickDrawing: jest.fn(),
      onUnMount: jest.fn(),
      activeDrawingId: '923723984872338768743',
    };

    render(
      <WarningsThemeProvider>
        <ObjectManager {...props} />
      </WarningsThemeProvider>,
    );

    expect(screen.getAllByRole('button')[2].classList).toContain(
      'Mui-selected',
    );
  });

  it('should show error if passed', async () => {
    const props = {
      onClose: jest.fn(),
      isOpen: true,
      drawingListItems: [] as DrawingListItem[],
      onClickDrawing: jest.fn(),
      onUnMount: jest.fn(),
      activeDrawingId: '923723984872338768743',
      error: 'Woops, something went wrong!',
    };

    const user = userEvent.setup();

    render(
      <WarningsThemeProvider>
        <ObjectManager {...props} />
      </WarningsThemeProvider>,
    );

    expect(screen.getByText(props.error)).toBeTruthy();

    await user.click(screen.getByRole('button', { name: /CLOSE/ }));

    expect(screen.queryByText(props.error)).toBeFalsy();
  });

  it('should show loading bar if loading', async () => {
    const props = {
      onClose: jest.fn(),
      isOpen: true,
      drawingListItems: [] as DrawingListItem[],
      onClickDrawing: jest.fn(),
      onUnMount: jest.fn(),
      activeDrawingId: '923723984872338768743',
      isLoading: true,
    };

    render(
      <WarningsThemeProvider>
        <ObjectManager {...props} />
      </WarningsThemeProvider>,
    );

    expect(screen.getByRole('progressbar')).toBeTruthy();
  });

  it('should be able to share an object', async () => {
    const props = {
      onClose: jest.fn(),
      isOpen: true,
      drawingListItems: [
        {
          id: '923723984872338768743',
          objectName: 'Drawing object 101',
          lastUpdatedTime: '2022-06-01T12:34:27Z',
          scope: 'user',
        },
        {
          id: '934834893283',
          objectName: 'Object 89',
          lastUpdatedTime: '2022-05-26T12:34:27Z',
          scope: 'user',
        },
        {
          id: '345346456345',
          objectName: 'Object 88',
          lastUpdatedTime: '2022-05-26T12:34:27Z',
          scope: 'user',
        },
      ] as DrawingListItem[],
      onShareObject: jest.fn(),
      onUnMount: jest.fn(),
      activeDrawingId: '923723984872338768743',
      isLoading: true,
    };

    render(
      <WarningsThemeProvider>
        <ObjectManager {...props} />
      </WarningsThemeProvider>,
    );

    fireEvent.click(screen.queryAllByLabelText(BUTTON_SHARE)[1]);
    fireEvent.click(await screen.findByText(BUTTON_PUBLIC_WARNING));

    expect(props.onShareObject).toHaveBeenCalledWith(props.drawingListItems[1]);
  });
});
