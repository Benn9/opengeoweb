/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */

import {
  act,
  fireEvent,
  render,
  screen,
  waitFor,
} from '@testing-library/react';
import React from 'react';
import { Provider } from 'react-redux';
import createSagaMiddleware from 'redux-saga';
import { configureStore } from '@reduxjs/toolkit';
import userEvent from '@testing-library/user-event';
import {
  layerReducer,
  uiReducer,
  webmapReducer,
  mapActions,
  drawtoolReducer,
  layerActions,
} from '@opengeoweb/store';
import { ApiProvider } from '@opengeoweb/api';
import { dateUtils } from '@opengeoweb/shared';
import {
  snackbarReducer,
  snackbarSaga,
  SnackbarWrapperConnect,
} from '@opengeoweb/snackbar';
import {
  defaultPolygon,
  emptyGeoJSON,
  rewindGeometry,
} from '@opengeoweb/webmap-react';
import { DELETE_TITLE, ObjectManagerConnect } from './ObjectManagerConnect';
import { createApi as createFakeApi } from '../../utils/fakeApi';
import { API_NAME, WarningsApi } from '../../utils/api';
import { WarningsThemeProvider } from '../Providers/Providers';
import drawingList from '../../utils/fakeApi/drawingsList.json';
import { DrawingFromBE, DrawingListItem } from '../../store/drawings/types';
import { ObjectManagerMapButtonConnect } from './ObjectManagerMapButtonConnect';
import { reducer as drawingsReducer } from '../../store/drawings/reducer';
import { reducer as publicWarningReducer } from '../../store/publicWarningForm/reducer';

import {
  BUTTON_PUBLIC_WARNING,
  BUTTON_SHARE,
  NO_OBJECTS_FOUND,
} from './Objects';
import DrawingToolConnect from '../DrawingTool/DrawingToolConnect';
import DrawingToolMapButtonConnect from '../DrawingTool/DrawingToolMapButtonConnect';
import drawingSaga from '../../store/drawings/sagas';
import { PublicWarningsConnect } from '../PublicWarnings/PublicWarningsConnect';
import { publicWarningDialogType } from '../../store/publicWarningForm/utils';
import { DATE_FORMAT } from '../../utils/constants';
import { coordinatesEmptyMessage } from '../DrawingToolForm/DrawingToolForm';
import { testGeoJSON } from '../../storybookUtils/testUtils';
import { BUTTON_ADD_OBJECT } from './ObjectManager';

describe('src/components/ObjectManager/ObjectManagerConnect', () => {
  const createApi = (): WarningsApi => {
    return {
      ...createFakeApi(),
      getDrawings: (): Promise<{ data: DrawingListItem[] }> => {
        return new Promise((resolve) => {
          const data = drawingList;
          resolve({ data: data as DrawingListItem[] });
        });
      },
    };
  };
  const sagaMiddleware = createSagaMiddleware();

  const store = configureStore({
    reducer: {
      ui: uiReducer,
      layers: layerReducer,
      webmap: webmapReducer,
      drawings: drawingsReducer,
      drawingtools: drawtoolReducer,
      snackbar: snackbarReducer,
      publicWarningForm: publicWarningReducer,
    },
    middleware: (getDefaultMiddleware) =>
      getDefaultMiddleware().concat(sagaMiddleware),
  });

  sagaMiddleware.run(snackbarSaga);
  sagaMiddleware.run(drawingSaga);

  it('should show the correct drawings', async () => {
    const mapId = 'map123';
    store.dispatch(mapActions.registerMap({ mapId }));

    render(
      <Provider store={store}>
        <ApiProvider createApi={createApi} name={API_NAME}>
          <WarningsThemeProvider>
            <ObjectManagerConnect />
            <ObjectManagerMapButtonConnect mapId={mapId} />
          </WarningsThemeProvider>
        </ApiProvider>
      </Provider>,
    );

    const user = userEvent.setup();
    await user.click(
      screen.getByRole('button', {
        name: `Object Manager Button for ${mapId}`,
      }),
    );

    expect(await screen.findByText(`Object Manager for ${mapId}`)).toBeTruthy();

    expect(await screen.findByText(drawingList[0].objectName)).toBeTruthy();
    expect(
      await screen.findByText(
        `${dateUtils.dateToString(
          dateUtils.isoStringToDate(drawingList[0].lastUpdatedTime),
          DATE_FORMAT,
          true,
        )} UTC`,
      ),
    ).toBeTruthy();

    // Global scope drawing should have Global: in front
    expect(await screen.findByText(drawingList[2].objectName)).toBeTruthy();
    expect(
      await screen.findByText(
        `Global: ${dateUtils.dateToString(
          dateUtils.isoStringToDate(drawingList[2].lastUpdatedTime),
          DATE_FORMAT,
          true,
        )} UTC`,
      ),
    ).toBeTruthy();
  });

  it('should add a new geoJSON layer on click, update it and remove it again on click', async () => {
    const mapId = 'map124';
    store.dispatch(mapActions.registerMap({ mapId }));

    render(
      <Provider store={store}>
        <ApiProvider createApi={createApi} name={API_NAME}>
          <WarningsThemeProvider>
            <ObjectManagerConnect />
            <ObjectManagerMapButtonConnect mapId={mapId} />
          </WarningsThemeProvider>
        </ApiProvider>
      </Provider>,
    );

    const user = userEvent.setup();
    await user.click(
      screen.getByRole('button', {
        name: `Object Manager Button for ${mapId}`,
      }),
    );
    expect(await screen.findByText(`Object Manager for ${mapId}`)).toBeTruthy();
    expect(store.getState().layers.byId).toEqual({});
    expect(await screen.findByText(drawingList[0].objectName)).toBeTruthy();
    await user.click(screen.getByText(drawingList[0].objectName));
    expect(store.getState().layers.byId[`drawlayer-${mapId}`].geojson).toEqual(
      drawingList[0].geoJSON,
    );
    expect(store.getState().drawings.entities.objectManager!.id).toEqual(
      drawingList[0].id,
    );
    // click another object
    await user.click(screen.getByText(drawingList[1].objectName));
    expect(store.getState().layers.byId[`drawlayer-${mapId}`].geojson).toEqual(
      drawingList[1].geoJSON,
    );
    expect(store.getState().drawings.entities.objectManager!.id).toEqual(
      drawingList[1].id,
    );

    // click same object again
    await user.click(screen.getByText(drawingList[1].objectName));
    await waitFor(() =>
      expect(store.getState().drawings.entities.objectManager!.id).toEqual(''),
    );
    expect(
      store.getState().layers.byId[`drawlayer-${mapId}`].geojson!.features,
    ).toEqual([]);
  });

  it('should remove geoJSON layer when closing the dialog', async () => {
    const mapId = 'map125';
    store.dispatch(mapActions.registerMap({ mapId }));

    render(
      <Provider store={store}>
        <ApiProvider createApi={createApi} name={API_NAME}>
          <WarningsThemeProvider>
            <ObjectManagerConnect />
            <ObjectManagerMapButtonConnect mapId={mapId} />
          </WarningsThemeProvider>
        </ApiProvider>
      </Provider>,
    );

    const user = userEvent.setup();
    await user.click(
      screen.getByRole('button', {
        name: `Object Manager Button for ${mapId}`,
      }),
    );
    expect(await screen.findByText(`Object Manager for ${mapId}`)).toBeTruthy();
    expect(await screen.findByText(drawingList[0].objectName)).toBeTruthy();
    await user.click(screen.getByText(drawingList[0].objectName));
    expect(store.getState().layers.byId[`drawlayer-${mapId}`].geojson).toEqual(
      drawingList[0].geoJSON,
    );
    expect(store.getState().drawings.entities.objectManager!.id).toEqual(
      drawingList[0].id,
    );

    user.click(screen.getByRole('button', { name: /close/i }));
    await waitFor(() =>
      expect(store.getState().drawings.entities.objectManager!.id).toEqual(''),
    );
    expect(
      store.getState().layers.byId[`drawlayer-${mapId}`].geojson!.features,
    ).toEqual([]);
  });

  it('should show an error when Edit fails', async () => {
    const mapId = 'map126';
    store.dispatch(mapActions.registerMap({ mapId }));

    const createApiWithErrorOnEdit = (): WarningsApi => {
      return {
        ...createFakeApi(),
        getDrawingDetails: (): Promise<{ data: DrawingFromBE }> => {
          return new Promise((_, reject) => {
            reject(new Error(`Error fetching drawing, please try again.`));
          });
        },
      };
    };

    render(
      <Provider store={store}>
        <ApiProvider createApi={createApiWithErrorOnEdit} name={API_NAME}>
          <WarningsThemeProvider>
            <ObjectManagerConnect />
            <ObjectManagerMapButtonConnect mapId={mapId} />
          </WarningsThemeProvider>
        </ApiProvider>
      </Provider>,
    );

    const user = userEvent.setup();
    await user.click(
      screen.getByRole('button', {
        name: `Object Manager Button for ${mapId}`,
      }),
    );

    expect(await screen.findByText(`Object Manager for ${mapId}`)).toBeTruthy();
    expect(await screen.findByText(drawingList[0].objectName)).toBeTruthy();
    await user.click(screen.getAllByLabelText('Object options')[0]);
    await user.click(screen.getByText('Edit'));
    // Error should be shown
    expect(
      await screen.findByText('Error fetching drawing, please try again.'),
    ).toBeTruthy();
    // List items should still be visible
    expect(await screen.findByText(drawingList[0].objectName)).toBeTruthy();
    // Object options should be closed
    expect(screen.queryByText('Edit')).toBeFalsy();
  });

  it('should show an error when getting the list fails', async () => {
    const mapId = 'map127';
    store.dispatch(mapActions.registerMap({ mapId }));

    const createApiWithErrorOnList = (): WarningsApi => {
      return {
        ...createFakeApi(),
        getDrawings: (): Promise<{ data: DrawingListItem[] }> => {
          return new Promise((_, reject) => {
            reject(new Error(`Error fetching drawings.`));
          });
        },
      };
    };

    render(
      <Provider store={store}>
        <ApiProvider createApi={createApiWithErrorOnList} name={API_NAME}>
          <WarningsThemeProvider>
            <ObjectManagerConnect />
            <ObjectManagerMapButtonConnect mapId={mapId} />
          </WarningsThemeProvider>
        </ApiProvider>
      </Provider>,
    );

    const user = userEvent.setup();
    await user.click(
      screen.getByRole('button', {
        name: `Object Manager Button for ${mapId}`,
      }),
    );

    expect(await screen.findByText(`Object Manager for ${mapId}`)).toBeTruthy();
    // Error should be shown
    expect(await screen.findByText('Error fetching drawings.')).toBeTruthy();
    // No results message should be shown
    expect(await screen.findByText(NO_OBJECTS_FOUND)).toBeTruthy();
  });

  it('should be able to edit a polygon', async () => {
    const mapId = 'map128';
    // eslint-disable-next-line @typescript-eslint/ban-ts-comment
    // @ts-ignore
    store.addEggs = jest.fn();
    store.dispatch(mapActions.registerMap({ mapId }));

    const mockUpdateDrawing = jest
      .fn()
      .mockImplementation((): Promise<void> => {
        return new Promise((resolve) => {
          resolve();
        });
      });
    const createApi = (): WarningsApi => {
      return {
        ...createFakeApi(),
        updateDrawing: mockUpdateDrawing,
      };
    };

    render(
      <Provider store={store}>
        <ApiProvider createApi={createApi} name={API_NAME}>
          <SnackbarWrapperConnect>
            <WarningsThemeProvider>
              <ObjectManagerConnect />
              <ObjectManagerMapButtonConnect mapId={mapId} />
              <DrawingToolConnect showMapIdInTitle />
            </WarningsThemeProvider>
          </SnackbarWrapperConnect>
        </ApiProvider>
      </Provider>,
    );

    const user = userEvent.setup();
    await user.click(
      screen.getByRole('button', {
        name: `Object Manager Button for ${mapId}`,
      }),
    );
    expect(await screen.findByText(`Object Manager for ${mapId}`)).toBeTruthy();

    expect(await screen.findByText(drawingList[0].objectName)).toBeTruthy();
    await user.click(screen.getByText(drawingList[0].objectName));

    expect(store.getState().layers.byId[`drawlayer-${mapId}`].geojson).toEqual(
      drawingList[0].geoJSON,
    );

    await user.click(
      screen.queryAllByRole('button', { name: /Object options/i })[0],
    );

    await user.click(screen.getByText('Edit'));

    // object manager should be closed
    await waitFor(() =>
      expect(screen.queryByText(`Object Manager for ${mapId}`)).toBeFalsy(),
    );

    // draw dialog should be opened
    expect(await screen.findByText(`Drawing Toolbox ${mapId}`)).toBeTruthy();

    // geojson should be updated
    expect(store.getState().drawings.entities.objectManager!.id).toEqual(
      drawingList[0].id,
    );

    // dialog should have object data
    expect(
      screen
        .getByRole('textbox', { name: 'Object name' })
        .getAttribute('value'),
    ).toEqual(drawingList[0].objectName);

    expect(
      store.getState().drawingtools.entities.drawingTool?.geoJSONLayerId,
    ).toEqual(`drawlayer-${mapId}`);
    expect(
      store.getState().drawingtools.entities.drawingTool?.activeDrawModeId,
    ).toEqual('');
    expect(store.getState().layers.byId[`drawlayer-${mapId}`].geojson).toEqual(
      drawingList[0].geoJSON,
    );

    // click save button
    expect(screen.getByText('Update object').classList).not.toContain(
      'Mui-disabled',
    );
    await user.click(screen.getByText('Update object'));

    // api should be called
    await waitFor(() =>
      expect(mockUpdateDrawing).toHaveBeenCalledWith(drawingList[0].id, {
        objectName: drawingList[0].objectName,
        scope: 'user',
        geoJSON: rewindGeometry(
          drawingList[0].geoJSON as GeoJSON.FeatureCollection,
        ),
      }),
    );

    // snackbar should show
    await screen.findByText(
      `Object ${drawingList[0].objectName} has been updated!`,
    );
  });

  it('should be able to delete a polygon', async () => {
    const mapId = 'map128';
    // eslint-disable-next-line @typescript-eslint/ban-ts-comment
    // @ts-ignore
    store.addEggs = jest.fn();
    store.dispatch(mapActions.registerMap({ mapId }));

    const mockDeleteDrawing = jest.fn();
    const mockGetDrawings = jest
      .fn()
      .mockImplementation((): Promise<{ data: DrawingListItem[] }> => {
        return new Promise((resolve) => {
          const data = drawingList;
          resolve({ data: data as DrawingListItem[] });
        });
      });
    const createApi = (): WarningsApi => {
      return {
        ...createFakeApi(),
        deleteDrawing: mockDeleteDrawing,
        getDrawings: mockGetDrawings,
      };
    };

    render(
      <Provider store={store}>
        <ApiProvider createApi={createApi} name={API_NAME}>
          <SnackbarWrapperConnect>
            <WarningsThemeProvider>
              <ObjectManagerConnect />
              <ObjectManagerMapButtonConnect mapId={mapId} />
              <DrawingToolConnect showMapIdInTitle />
            </WarningsThemeProvider>
          </SnackbarWrapperConnect>
        </ApiProvider>
      </Provider>,
    );

    const user = userEvent.setup();
    await user.click(
      screen.getByRole('button', {
        name: `Object Manager Button for ${mapId}`,
      }),
    );
    expect(await screen.findByText(`Object Manager for ${mapId}`)).toBeTruthy();
    expect(mockGetDrawings).toHaveBeenCalledTimes(1);

    expect(await screen.findByText(drawingList[0].objectName)).toBeTruthy();
    await user.click(screen.getByText(drawingList[0].objectName));

    expect(store.getState().layers.byId[`drawlayer-${mapId}`].geojson).toEqual(
      drawingList[0].geoJSON,
    );

    await user.click(
      screen.queryAllByRole('button', { name: /Object options/i })[0],
    );

    await user.click(screen.getByText('Delete'));
    expect(screen.getByText(DELETE_TITLE)).toBeTruthy();

    await user.click(screen.getByRole('button', { name: /Delete/i }));

    // api should be called
    expect(mockDeleteDrawing).toBeCalledWith(drawingList[0].id);

    // geojson should be removed
    await waitFor(() =>
      expect(
        store.getState().layers.byId[`drawlayer-${mapId}`].geojson!.features,
      ).toEqual([]),
    );

    // snackbar should show
    await screen.findByText(`${drawingList[0].objectName} has been deleted`);

    // list should update
    expect(mockGetDrawings).toHaveBeenCalledTimes(2);
  });

  it('should close the drawing tool when opening the object manager and vice versa', async () => {
    const mapId = 'map129';
    store.dispatch(mapActions.registerMap({ mapId }));

    render(
      <Provider store={store}>
        <ApiProvider createApi={createApi} name={API_NAME}>
          <WarningsThemeProvider>
            <ObjectManagerConnect />
            <ObjectManagerMapButtonConnect mapId={mapId} />
            <DrawingToolConnect showMapIdInTitle />
            <DrawingToolMapButtonConnect mapId={mapId} />
          </WarningsThemeProvider>
        </ApiProvider>
      </Provider>,
    );

    const user = userEvent.setup();

    // open drawing tool
    await user.click(
      screen.getByRole('button', {
        name: `Drawing Toolbox`,
      }),
    );
    expect(await screen.findByText(`Drawing Toolbox ${mapId}`)).toBeTruthy();
    expect(screen.queryByText(`Object Manager for ${mapId}`)).toBeFalsy();

    // check that drawing tool is active
    const drawingButton = screen.getByRole('button', {
      name: 'Drawing Toolbox',
    });
    expect(drawingButton.classList).toContain('Mui-selected');

    // open object manager
    await user.click(
      screen.getByRole('button', {
        name: `Object Manager Button for ${mapId}`,
      }),
    );

    expect(await screen.findByText(`Object Manager for ${mapId}`)).toBeTruthy();
    expect(screen.queryByText(`Drawing Toolbox ${mapId}`)).toBeFalsy();
    expect(drawingButton.classList).not.toContain('Mui-selected');

    // select object
    expect(await screen.findByText(drawingList[0].objectName)).toBeTruthy();
    await user.click(screen.getByText(drawingList[0].objectName));
    expect(store.getState().layers.byId[`drawlayer-${mapId}`].geojson).toEqual(
      drawingList[0].geoJSON,
    );
    expect(store.getState().drawings.entities.objectManager!.id).toEqual(
      drawingList[0].id,
    );

    // open drawing tool again
    await user.click(
      screen.getByRole('button', {
        name: `Drawing Toolbox`,
      }),
    );
    expect(await screen.findByText(`Drawing Toolbox ${mapId}`)).toBeTruthy();
    expect(screen.queryByText(`Object Manager for ${mapId}`)).toBeFalsy();
    expect(drawingButton.classList).toContain('Mui-selected');

    expect(
      store.getState().layers.byId[`drawlayer-${mapId}`].geojson?.features,
    ).toEqual([]);
    expect(store.getState().drawings.entities.objectManager!.id).toEqual('');
  });

  it('should be able to share an object', async () => {
    const mapId = 'map128';
    // eslint-disable-next-line @typescript-eslint/ban-ts-comment
    // @ts-ignore
    store.addEggs = jest.fn();
    store.dispatch(mapActions.registerMap({ mapId }));

    const mockDeleteDrawing = jest.fn();
    const mockGetDrawings = jest
      .fn()
      .mockImplementation((): Promise<{ data: DrawingListItem[] }> => {
        return new Promise((resolve) => {
          const data = drawingList;
          resolve({ data: data as DrawingListItem[] });
        });
      });
    const createApi = (): WarningsApi => {
      return {
        ...createFakeApi(),
        deleteDrawing: mockDeleteDrawing,
        getDrawings: mockGetDrawings,
      };
    };

    render(
      <Provider store={store}>
        <ApiProvider createApi={createApi} name={API_NAME}>
          <SnackbarWrapperConnect>
            <WarningsThemeProvider>
              <ObjectManagerConnect />
              <ObjectManagerMapButtonConnect mapId={mapId} />
              <DrawingToolConnect showMapIdInTitle />
              <PublicWarningsConnect />
            </WarningsThemeProvider>
          </SnackbarWrapperConnect>
        </ApiProvider>
      </Provider>,
    );

    const user = userEvent.setup();
    await user.click(
      screen.getByRole('button', {
        name: `Object Manager Button for ${mapId}`,
      }),
    );
    expect(await screen.findByText(`Object Manager for ${mapId}`)).toBeTruthy();
    expect(mockGetDrawings).toBeCalledTimes(1);

    expect(
      store.getState().ui.dialogs[publicWarningDialogType]?.isOpen,
    ).toBeFalsy();
    expect(store.getState().publicWarningForm.object).toBeUndefined();

    await user.click(screen.queryAllByLabelText(BUTTON_SHARE)[1]);
    await user.click(await screen.findByText(BUTTON_PUBLIC_WARNING));

    expect(
      store.getState().ui.dialogs[publicWarningDialogType]?.isOpen,
    ).toBeTruthy();
    const expectedResult = {
      id: drawingList[1].id,
      lastUpdatedTime: drawingList[1].lastUpdatedTime,
      objectName: drawingList[1].objectName,
    };
    await waitFor(() => {
      expect(store.getState().publicWarningForm.object).toEqual(expectedResult);
    });

    expect(
      screen.getByRole('heading', { name: expectedResult.objectName }),
    ).toBeTruthy();

    user.click(screen.queryAllByRole('button', { name: /close/i })[1]);
    await waitFor(() => {
      expect(
        store.getState().ui.dialogs[publicWarningDialogType]?.isOpen,
      ).toBeFalsy();
    });
    expect(store.getState().publicWarningForm.object).toBeUndefined();
  });

  it('should be able to save a new object', async () => {
    const mapId = 'map130';
    // eslint-disable-next-line @typescript-eslint/ban-ts-comment
    // @ts-ignore
    store.addEggs = jest.fn();
    store.dispatch(mapActions.registerMap({ mapId }));

    const mockSaveDrawing = jest.fn().mockImplementation((): Promise<void> => {
      return new Promise((resolve) => {
        resolve();
      });
    });
    const createApi = (): WarningsApi => {
      return {
        ...createFakeApi(),
        saveDrawingAs: mockSaveDrawing,
      };
    };

    render(
      <Provider store={store}>
        <ApiProvider createApi={createApi} name={API_NAME}>
          <SnackbarWrapperConnect>
            <WarningsThemeProvider>
              <ObjectManagerConnect />
              <ObjectManagerMapButtonConnect mapId={mapId} />
              <DrawingToolConnect showMapIdInTitle />
              <DrawingToolMapButtonConnect mapId={mapId} />
            </WarningsThemeProvider>
          </SnackbarWrapperConnect>
        </ApiProvider>
      </Provider>,
    );

    const user = userEvent.setup();
    await user.click(
      screen.getByRole('button', {
        name: `Object Manager Button for ${mapId}`,
      }),
    );
    expect(await screen.findByText(`Object Manager for ${mapId}`)).toBeTruthy();

    expect(await screen.findByText(drawingList[0].objectName)).toBeTruthy();
    await user.click(screen.getByText(drawingList[0].objectName));

    expect(store.getState().layers.byId[`drawlayer-${mapId}`].geojson).toEqual(
      drawingList[0].geoJSON,
    );

    await user.click(
      screen.getByRole('button', {
        name: `Drawing Toolbox`,
      }),
    );

    // draw dialog should be opened
    expect(await screen.findByText(`Drawing Toolbox ${mapId}`)).toBeTruthy();
    // Object manager should be closed
    expect(screen.queryByText(`Object Manager for ${mapId}`)).toBeFalsy();

    // dialog should have default data
    expect(
      screen
        .getByRole('textbox', { name: 'Object name' })
        .getAttribute('value'),
    ).not.toEqual('');

    expect(
      screen.getAllByRole('textbox', { hidden: true })[0].getAttribute('value'),
    ).toEqual('20');

    // trigger drawn shape
    act(() =>
      store.dispatch(
        layerActions.updateFeature({
          layerId: `drawlayer-${mapId}`,
          geojson: testGeoJSON,
        }),
      ),
    );

    await waitFor(() =>
      expect(
        store.getState().layers.byId[`drawlayer-${mapId}`].geojson,
      ).toEqual(testGeoJSON),
    );

    // click save button
    expect(screen.getByText('Save new object').classList).not.toContain(
      'Mui-disabled',
    );
    await user.click(screen.getByText('Save new object'));

    // api should be called
    expect(mockSaveDrawing).toBeCalledWith({
      objectName: expect.any(String),
      scope: 'user',
      geoJSON: testGeoJSON,
    });

    // snackbar should show
    expect(
      (await screen.findByTestId('snackbarComponent')).textContent,
    ).toContain('has been saved!');
  });

  it('should handle draw actions', async () => {
    const mapId = 'map131';
    store.dispatch(mapActions.registerMap({ mapId }));

    render(
      <Provider store={store}>
        <ApiProvider createApi={createApi} name={API_NAME}>
          <WarningsThemeProvider>
            <ObjectManagerConnect />
            <ObjectManagerMapButtonConnect mapId={mapId} />
            <DrawingToolConnect showMapIdInTitle />
            <DrawingToolMapButtonConnect mapId={mapId} />
          </WarningsThemeProvider>
        </ApiProvider>
      </Provider>,
    );

    const user = userEvent.setup();
    await user.click(
      screen.getByRole('button', {
        name: `Object Manager Button for ${mapId}`,
      }),
    );
    expect(await screen.findByText(`Object Manager for ${mapId}`)).toBeTruthy();

    expect(await screen.findByText(drawingList[0].objectName)).toBeTruthy();
    await user.click(screen.getByText(drawingList[0].objectName));

    expect(store.getState().layers.byId[`drawlayer-${mapId}`].geojson).toEqual(
      drawingList[0].geoJSON,
    );

    // open drawing tool
    await user.click(screen.getByRole('button', { name: 'Drawing Toolbox' }));

    expect(
      store.getState().drawingtools.entities.drawingTool?.activeDrawModeId,
    ).toEqual('');

    const polygonButton = screen.getByRole('button', { name: /Polygon/i });
    expect(polygonButton.classList).not.toContain('Mui-selected');

    // activate polygon
    await user.click(polygonButton);
    expect(polygonButton!.classList).toContain('Mui-selected');

    expect(
      store.getState().drawingtools.entities.drawingTool?.activeDrawModeId,
    ).toEqual(defaultPolygon.drawModeId);

    // click delete and should disable active mode as it's not selectable
    const deleteButton = screen.getByRole('button', { name: /Delete/i });
    await user.click(deleteButton);

    expect(
      store.getState().drawingtools.entities.drawingTool?.activeDrawModeId,
    ).toEqual('');

    expect(await screen.findByText(coordinatesEmptyMessage)).toBeTruthy();

    // activate polygon
    await user.click(polygonButton);
    await waitFor(() =>
      expect(polygonButton.classList).toContain('Mui-selected'),
    );

    // trigger drawn shape
    act(() => {
      store.dispatch(
        layerActions.updateFeature({
          layerId: `drawlayer-${mapId}`,
          geojson: testGeoJSON,
        }),
      );
    });

    // exit draw mode
    fireEvent.keyDown(polygonButton, { key: 'Escape' });

    await waitFor(() => {
      expect(screen.queryByText(coordinatesEmptyMessage)).toBeFalsy();
    });

    const { layers, drawingtools } = store.getState();
    expect(layers.byId[`drawlayer-${mapId}`].isInEditMode).toBeFalsy();
    expect(drawingtools.entities.drawingTool?.activeDrawModeId).toEqual('');
  });

  it('should handle name and opacity changes', async () => {
    const mapId = 'map132';
    store.dispatch(mapActions.registerMap({ mapId }));

    render(
      <Provider store={store}>
        <ApiProvider createApi={createApi} name={API_NAME}>
          <WarningsThemeProvider>
            <ObjectManagerConnect />
            <ObjectManagerMapButtonConnect mapId={mapId} />
            <DrawingToolConnect showMapIdInTitle />
            <DrawingToolMapButtonConnect mapId={mapId} />
          </WarningsThemeProvider>
        </ApiProvider>
      </Provider>,
    );

    const user = userEvent.setup();
    await user.click(
      screen.getByRole('button', {
        name: `Object Manager Button for ${mapId}`,
      }),
    );
    expect(await screen.findByText(`Object Manager for ${mapId}`)).toBeTruthy();

    expect(await screen.findByText(drawingList[0].objectName)).toBeTruthy();
    await user.click(screen.getByText(drawingList[0].objectName));

    expect(store.getState().layers.byId[`drawlayer-${mapId}`].geojson).toEqual(
      drawingList[0].geoJSON,
    );

    // open drawing tool
    await user.click(screen.getByRole('button', { name: 'Drawing Toolbox' }));

    // change the name
    const nameInput = screen.getByRole('textbox', { name: /Object name/i });
    const newNameValue = 'testing new name';
    fireEvent.change(nameInput, { target: { value: newNameValue } });
    fireEvent.blur(nameInput);

    expect(
      store.getState().drawings.entities.objectManager!.objectName,
    ).toEqual(newNameValue);

    // trigger drawn shape
    act(() => {
      store.dispatch(
        layerActions.updateFeature({
          layerId: `drawlayer-${mapId}`,
          geojson: testGeoJSON,
        }),
      );
    });

    // change opacity
    fireEvent.mouseDown(screen.getByRole('button', { name: 'Opacity 20 %' }));

    const menuItem = await screen.findByText('50 %');
    await user.click(menuItem);

    await waitFor(() => {
      expect(
        store.getState().layers.byId[`drawlayer-${mapId}`].geojson?.features[0]!
          .properties!['fill-opacity'],
      ).toEqual(0.5);
    });
  });

  it('should deactivate draw mode and clear geoJSON when closing the drawing toolbox', async () => {
    const mapId = 'map133';
    store.dispatch(mapActions.registerMap({ mapId }));

    render(
      <Provider store={store}>
        <ApiProvider createApi={createApi} name={API_NAME}>
          <WarningsThemeProvider>
            <ObjectManagerConnect />
            <ObjectManagerMapButtonConnect mapId={mapId} />
            <DrawingToolConnect showMapIdInTitle />
            <DrawingToolMapButtonConnect mapId={mapId} />
          </WarningsThemeProvider>
        </ApiProvider>
      </Provider>,
    );

    const user = userEvent.setup();
    await user.click(
      screen.getByRole('button', {
        name: `Object Manager Button for ${mapId}`,
      }),
    );
    expect(await screen.findByText(`Object Manager for ${mapId}`)).toBeTruthy();

    expect(await screen.findByText(drawingList[0].objectName)).toBeTruthy();
    await user.click(screen.getByText(drawingList[0].objectName));

    expect(store.getState().layers.byId[`drawlayer-${mapId}`].geojson).toEqual(
      drawingList[0].geoJSON,
    );

    // open drawing tool
    await user.click(screen.getByRole('button', { name: 'Drawing Toolbox' }));

    expect(
      store.getState().drawingtools.entities.drawingTool?.activeDrawModeId,
    ).toEqual('');

    const polygonButton = screen.getByRole('button', { name: /Polygon/i });

    // activate polygon drawmode
    await user.click(polygonButton);

    expect(
      store.getState().drawingtools.entities.drawingTool?.activeDrawModeId,
    ).toEqual(defaultPolygon.drawModeId);

    // trigger drawing in progress
    act(() => {
      store.dispatch(
        layerActions.toggleFeatureMode({
          layerId: `drawlayer-${mapId}`,
          isInEditMode: true,
          drawMode: 'POLYGON',
        }),
      );
    });

    // close drawing toolbox by clicking the button
    await user.click(screen.getByRole('button', { name: 'Drawing Toolbox' }));

    const { layers, drawingtools } = store.getState();
    expect(layers.byId[`drawlayer-${mapId}`].isInEditMode).toBeFalsy();
    expect(layers.byId[`drawlayer-${mapId}`].drawMode).toEqual('');
    expect(drawingtools.entities.drawingTool?.activeDrawModeId).toEqual('');
  });

  it('should show an error on the drawing toolbox when saving fails and clear the error when closing the dialog', async () => {
    const mapId = 'map134';
    // eslint-disable-next-line @typescript-eslint/ban-ts-comment
    // @ts-ignore
    store.addEggs = jest.fn();
    store.dispatch(mapActions.registerMap({ mapId }));

    const mockUpdateDrawing = jest
      .fn()
      .mockImplementation((): Promise<void> => {
        return new Promise((_, reject) => {
          reject(new Error('Example error'));
        });
      });
    const createApi = (): WarningsApi => {
      return {
        ...createFakeApi(),
        updateDrawing: mockUpdateDrawing,
      };
    };

    render(
      <Provider store={store}>
        <ApiProvider createApi={createApi} name={API_NAME}>
          <SnackbarWrapperConnect>
            <WarningsThemeProvider>
              <ObjectManagerConnect />
              <ObjectManagerMapButtonConnect mapId={mapId} />
              <DrawingToolConnect showMapIdInTitle />
            </WarningsThemeProvider>
          </SnackbarWrapperConnect>
        </ApiProvider>
      </Provider>,
    );

    const user = userEvent.setup();
    await user.click(
      screen.getByRole('button', {
        name: `Object Manager Button for ${mapId}`,
      }),
    );
    expect(await screen.findByText(`Object Manager for ${mapId}`)).toBeTruthy();

    expect(await screen.findByText(drawingList[0].objectName)).toBeTruthy();
    await user.click(screen.getByText(drawingList[0].objectName));

    expect(store.getState().layers.byId[`drawlayer-${mapId}`].geojson).toEqual(
      drawingList[0].geoJSON,
    );

    await user.click(
      screen.queryAllByRole('button', { name: /Object options/i })[0],
    );

    await user.click(screen.getByText('Edit'));

    // object manager should be closed
    await waitFor(() =>
      expect(screen.queryByText(`Object Manager for ${mapId}`)).toBeFalsy(),
    );

    // draw dialog should be opened
    expect(await screen.findByText(`Drawing Toolbox ${mapId}`)).toBeTruthy();

    // click save button
    await user.click(screen.getByText('Update object'));

    // error should show
    await screen.findByText('Example error');
    expect(store.getState().ui.dialogs.drawingTool?.error).toEqual(
      'Example error',
    );

    // close dialog
    await user.click(screen.getByRole('button', { name: 'Close' }));

    expect(store.getState().ui.dialogs.drawingTool?.error).toEqual('');
  });

  it('should clear drawn objects when opening the object manager', async () => {
    const mapId = 'map135';
    store.dispatch(mapActions.registerMap({ mapId }));

    render(
      <Provider store={store}>
        <ApiProvider createApi={createApi} name={API_NAME}>
          <WarningsThemeProvider>
            <ObjectManagerConnect />
            <ObjectManagerMapButtonConnect mapId={mapId} />
            <DrawingToolConnect showMapIdInTitle />
            <DrawingToolMapButtonConnect mapId={mapId} />
          </WarningsThemeProvider>
        </ApiProvider>
      </Provider>,
    );

    const user = userEvent.setup();
    await user.click(
      screen.getByRole('button', {
        name: `Object Manager Button for ${mapId}`,
      }),
    );
    expect(await screen.findByText(`Object Manager for ${mapId}`)).toBeTruthy();

    expect(await screen.findByText(drawingList[0].objectName)).toBeTruthy();
    await user.click(screen.getByText(drawingList[0].objectName));

    expect(store.getState().layers.byId[`drawlayer-${mapId}`].geojson).toEqual(
      drawingList[0].geoJSON,
    );

    // open drawing tool
    await user.click(screen.getByRole('button', { name: 'Drawing Toolbox' }));

    const polygonButton = screen.getByRole('button', { name: /Polygon/i });

    // activate polygon
    await user.click(polygonButton);

    expect(
      store.getState().drawingtools.entities.drawingTool?.activeDrawModeId,
    ).toEqual(defaultPolygon.drawModeId);

    // trigger drawn shape
    act(() => {
      store.dispatch(
        layerActions.updateFeature({
          layerId: `drawlayer-${mapId}`,
          geojson: testGeoJSON,
        }),
      );
    });

    expect(store.getState().layers.byId[`drawlayer-${mapId}`].geojson).toEqual(
      testGeoJSON,
    );
    // open object manager
    await user.click(
      screen.getByRole('button', {
        name: `Object Manager Button for ${mapId}`,
      }),
    );
    expect(await screen.findByText(`Object Manager for ${mapId}`)).toBeTruthy();
    expect(store.getState().layers.byId[`drawlayer-${mapId}`].geojson).toEqual(
      emptyGeoJSON,
    );
  });

  it('should add a new object from the object manager', async () => {
    const mapId = 'map136';
    store.dispatch(mapActions.registerMap({ mapId }));

    render(
      <Provider store={store}>
        <ApiProvider createApi={createApi} name={API_NAME}>
          <WarningsThemeProvider>
            <ObjectManagerConnect />
            <ObjectManagerMapButtonConnect mapId={mapId} />
            <DrawingToolConnect showMapIdInTitle />
            <DrawingToolMapButtonConnect mapId={mapId} />
          </WarningsThemeProvider>
        </ApiProvider>
      </Provider>,
    );

    const user = userEvent.setup();
    await user.click(
      screen.getByRole('button', {
        name: `Object Manager Button for ${mapId}`,
      }),
    );
    expect(await screen.findByText(`Object Manager for ${mapId}`)).toBeTruthy();
    await user.click(
      screen.getByRole('button', {
        name: BUTTON_ADD_OBJECT,
      }),
    );
    // draw dialog should be opened
    expect(await screen.findByText(`Drawing Toolbox ${mapId}`)).toBeTruthy();
    expect(screen.getByText('Save new object')).toBeTruthy();
    // Object manager should be closed
    expect(screen.queryByText(`Object Manager for ${mapId}`)).toBeFalsy();
  });
});
