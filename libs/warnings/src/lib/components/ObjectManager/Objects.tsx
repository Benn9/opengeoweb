/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */

import { ToggleMenu, dateUtils, getHeight } from '@opengeoweb/shared';
import { VariableSizeList as List } from 'react-window';
import {
  Stack,
  ListItem,
  ListItemButton,
  Paper,
  Typography,
  Box,
} from '@mui/material';
import React, { FC, Fragment } from 'react';
import { Delete, Edit, ExitDomain, Share } from '@opengeoweb/theme';
import { DrawingListItem } from '../../store/drawings/types';
import { DATE_FORMAT, DATE_FORMAT_HEADER } from '../../utils/constants';

const DEFAULT_DIALOG_HEIGHT = 500;
const DEFAULT_ADD_BUTTON_HEIGHT = 80;
const DEFAULT_PADDING_TOP = 6;
const OBJECT_ITEM_SIZE = 50;
const HEADER_ITEM_SIZE = 23;
const MIN_HEIGHT_SCROLLABLE_LIST = 90; // Slightly less than 2 rows

const useGetRequiredHeights = (): {
  listRef: React.RefObject<HTMLDivElement>;
  listHeight: number;
} => {
  const listRef = React.useRef<HTMLDivElement>(null);

  // Used to store the height of the list dialog content
  const [dialogHeight, setDialogHeight] = React.useState<number>(
    DEFAULT_DIALOG_HEIGHT,
  );
  // Used to store the height of the "Add new object" button
  const [addButtonHeight, setAddButtonHeight] = React.useState<number>(
    DEFAULT_ADD_BUTTON_HEIGHT,
  );

  // Obtain the height of of the list dialog content
  React.useLayoutEffect(() => {
    const dialogElement = listRef.current?.parentElement as HTMLDivElement;
    dialogElement?.offsetHeight && setDialogHeight(dialogElement?.offsetHeight);

    const observer = new ResizeObserver(() => {
      const height = getHeight(dialogElement);
      height && setDialogHeight(height);
    });
    observer.observe(dialogElement);

    return (): void => {
      observer.disconnect();
    };
  }, [listRef]);

  // Obtain height of the "Add new object" button
  React.useEffect(() => {
    const buttonElement = listRef.current?.parentElement
      ?.lastChild as HTMLDivElement;

    const height = getHeight(buttonElement);
    height && setAddButtonHeight(height);
  }, []);

  const availableSpaceForList =
    dialogHeight - addButtonHeight - DEFAULT_PADDING_TOP;
  const listHeight =
    availableSpaceForList && availableSpaceForList < MIN_HEIGHT_SCROLLABLE_LIST
      ? MIN_HEIGHT_SCROLLABLE_LIST
      : availableSpaceForList;

  return {
    listRef,
    listHeight,
  };
};

export const NO_OBJECTS_FOUND = 'No objects found';

export const BUTTON_EDIT = 'Edit';
export const BUTTON_DELETE = 'Delete';
export const BUTTON_SHARE = 'Share';
export const BUTTON_AIRMET = 'AIRMET';
export const BUTTON_SIGMET = 'SIGMET';
export const BUTTON_PUBLIC_WARNING = 'PUBLIC WARNING';
export const MENU_SHARE = 'Share object to';

type ListInclHeadersType = { isObject: boolean; index: number }[];

export const getListInclHeaders = (
  objects: DrawingListItem[],
): { listInclHeaders: ListInclHeadersType; headerList: string[] } => {
  const listInclHeaders: ListInclHeadersType = [];
  const headerList: string[] = [];
  // Construct list incl day headers
  objects.forEach((object, index) => {
    const objectDate = dateUtils.isoStringToDate(object.lastUpdatedTime);
    const header = dateUtils.dateToString(objectDate, DATE_FORMAT_HEADER, true);
    const isNewDay = !headerList.find((item) => item === header);
    if (isNewDay) {
      const newLength = headerList.push(header);
      listInclHeaders.push({ isObject: false, index: newLength - 1 });
    }
    listInclHeaders.push({ isObject: true, index });
  });

  return { listInclHeaders, headerList };
};

export const Objects: FC<{
  objects: DrawingListItem[];
  onClickObject: (object: DrawingListItem) => void;
  onClickEdit: (objectId: string) => void;
  onClickDelete: (drawingId: string) => void;
  onClickShare: (object: DrawingListItem) => void;
  activeObject: string;
  isLoading?: boolean;
}> = ({
  objects,
  onClickObject,
  onClickEdit,
  onClickDelete,
  onClickShare,
  activeObject,
  isLoading = false,
}) => {
  // Used to store the height of the list dialog content
  const { listRef, listHeight } = useGetRequiredHeights();

  const { listInclHeaders, headerList } = getListInclHeaders(objects);

  const getItemSize = (index: number): number => {
    const item = listInclHeaders[index];
    if (item.isObject) {
      return OBJECT_ITEM_SIZE;
    }
    return HEADER_ITEM_SIZE;
  };

  return (
    <Box ref={listRef} sx={{ paddingTop: 0.75 }}>
      {!isLoading && objects.length === 0 ? (
        <Typography
          sx={{ paddingLeft: '16px', paddingTop: '8px' }}
          variant="body1"
        >
          {NO_OBJECTS_FOUND}
        </Typography>
      ) : (
        <List
          height={listHeight}
          itemCount={listInclHeaders.length}
          itemSize={getItemSize}
          width="100%"
        >
          {({ index, style }): JSX.Element => {
            const listItem = listInclHeaders[index];
            if (!listItem.isObject) {
              const header = headerList[listItem.index];
              return (
                <Typography
                  style={style}
                  key={listItem.index}
                  sx={{
                    paddingLeft: 2.5,
                    paddingBottom: 0.5,
                    paddingTop: 0.5,
                    opacity: 0.67,
                  }}
                  variant="caption"
                >
                  {header}
                </Typography>
              );
            }

            const object = objects[listItem.index];
            const objectDate = dateUtils.isoStringToDate(
              object.lastUpdatedTime,
            );

            return (
              <ListItem
                style={style}
                disablePadding
                key={object.id}
                sx={{ '& .MuiListItemSecondaryAction-root': { right: '8px' } }}
                secondaryAction={
                  <>
                    <ToggleMenu
                      buttonSx={{
                        marginRight: object.scope === 'user' ? 1 : 4,
                      }}
                      buttonIcon={<Share />}
                      menuTitle={MENU_SHARE}
                      tooltipTitle={BUTTON_SHARE}
                      menuPosition="bottom"
                      aria-label={BUTTON_SHARE}
                      menuItems={[
                        {
                          text: BUTTON_AIRMET,
                          action: (): void => {},
                          icon: <ExitDomain />,
                          isDisabled: true,
                        },
                        {
                          text: BUTTON_SIGMET,
                          action: (): void => {},
                          icon: <ExitDomain />,
                          isDisabled: true,
                        },
                        {
                          text: BUTTON_PUBLIC_WARNING,
                          action: (): void => {
                            onClickShare(object);
                          },
                          icon: <ExitDomain />,
                        },
                      ]}
                    />
                    {object.scope === 'user' && (
                      <ToggleMenu
                        menuTitle="Object options"
                        tooltipTitle="Object options"
                        menuPosition="bottom"
                        aria-label="Options"
                        menuItems={[
                          {
                            text: BUTTON_EDIT,
                            action: (): void => {
                              onClickEdit(object.id);
                            },
                            icon: <Edit />,
                          },
                          {
                            text: BUTTON_DELETE,
                            action: (): void => {
                              onClickDelete(object.id);
                            },
                            icon: <Delete />,
                          },
                        ]}
                      />
                    )}
                  </>
                }
              >
                <Paper
                  elevation={0}
                  sx={{
                    width: '100%',
                    margin: 0.25,
                    backgroundColor: 'geowebColors.background.surface',
                    border: 'solid 1px',
                    borderColor: 'geowebColors.cards.cardContainerBorder',
                  }}
                >
                  <ListItemButton
                    onClick={(): void => {
                      onClickObject(object);
                    }}
                    selected={activeObject === object.id}
                    sx={{ padding: 1, height: '44px' }}
                  >
                    <Stack>
                      <Typography variant="caption">
                        {object.objectName}
                      </Typography>
                      <Typography variant="caption" sx={{ opacity: 0.67 }}>
                        {object.scope === 'global' && 'Global: '}
                        {dateUtils.dateToString(
                          objectDate,
                          DATE_FORMAT,
                          true,
                        )}{' '}
                        UTC
                      </Typography>
                    </Stack>
                  </ListItemButton>
                </Paper>
              </ListItem>
            );
          }}
        </List>
      )}
    </Box>
  );
};
