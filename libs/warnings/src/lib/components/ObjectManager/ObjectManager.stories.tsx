/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */
import React from 'react';
import { Box } from '@mui/material';
import { darkTheme, lightTheme } from '@opengeoweb/theme';
import { ObjectManager } from './ObjectManager';
import { WarningsThemeProvider } from '../Providers/Providers';
import drawingListItems from '../../utils/fakeApi/drawingsList.json';
import { DrawingListItem } from '../../store/drawings/types';

export default {
  title: 'components/ObjectManager',
};

export const ObjectManagerLight = (): React.ReactElement => {
  return (
    <WarningsThemeProvider theme={lightTheme}>
      <Box sx={{ width: '500px', height: '800px' }}>
        <ObjectManager
          onClose={(): void => {}}
          isOpen
          drawingListItems={drawingListItems as DrawingListItem[]}
          activeDrawingId={drawingListItems[0].id}
        />
      </Box>
    </WarningsThemeProvider>
  );
};

ObjectManagerLight.parameters = {
  zeplinLink: [
    {
      name: 'Light theme',
      link: 'https://app.zeplin.io/project/5ecf84a3c6ae1047a368f393/screen/64956cfe39ec23476bc3b1c8',
    },
  ],
};

ObjectManagerLight.storyName = 'ObjectManagerLight (takeSnapshot)';

export const ObjectManagerDark = (): React.ReactElement => {
  return (
    <WarningsThemeProvider theme={darkTheme}>
      <Box sx={{ width: '500px', height: '800px' }}>
        <ObjectManager
          onClose={(): void => {}}
          isOpen
          drawingListItems={drawingListItems as DrawingListItem[]}
          activeDrawingId={drawingListItems[0].id}
        />
      </Box>
    </WarningsThemeProvider>
  );
};

ObjectManagerDark.parameters = {
  zeplinLink: [
    {
      name: 'Dark theme',
      link: 'https://app.zeplin.io/project/5ecf84a3c6ae1047a368f393/screen/64997ae799d4a822f85e26cc',
    },
  ],
};

ObjectManagerDark.storyName = 'ObjectManagerDark (takeSnapshot)';

export const ObjectManagerLightNoResults = (): React.ReactElement => {
  return (
    <WarningsThemeProvider theme={lightTheme}>
      <Box sx={{ width: '500px', height: '800px' }}>
        <ObjectManager
          onClose={(): void => {}}
          isOpen
          drawingListItems={[] as DrawingListItem[]}
        />
      </Box>
    </WarningsThemeProvider>
  );
};

ObjectManagerLightNoResults.storyName =
  'ObjectManagerLightNoResults (takeSnapshot)';

export const ObjectManagerDarkNoResults = (): React.ReactElement => {
  return (
    <WarningsThemeProvider theme={darkTheme}>
      <Box sx={{ width: '500px', height: '800px' }}>
        <ObjectManager
          onClose={(): void => {}}
          isOpen
          drawingListItems={[] as DrawingListItem[]}
        />
      </Box>
    </WarningsThemeProvider>
  );
};

ObjectManagerDarkNoResults.storyName =
  'ObjectManagerDarkNoResults (takeSnapshot)';

export const ObjectManagerLoading = (): React.ReactElement => {
  return (
    <WarningsThemeProvider theme={lightTheme}>
      <Box sx={{ width: '500px', height: '800px' }}>
        <ObjectManager
          onClose={(): void => {}}
          isOpen
          drawingListItems={drawingListItems as DrawingListItem[]}
          isLoading
        />
      </Box>
    </WarningsThemeProvider>
  );
};

export const ObjectManagerErrorLight = (): React.ReactElement => {
  return (
    <WarningsThemeProvider theme={lightTheme}>
      <Box sx={{ width: '500px', height: '800px' }}>
        <ObjectManager
          onClose={(): void => {}}
          isOpen
          drawingListItems={drawingListItems as DrawingListItem[]}
          error="Something went wrong"
        />
      </Box>
    </WarningsThemeProvider>
  );
};

export const ObjectManagerErrorDark = (): React.ReactElement => {
  return (
    <WarningsThemeProvider theme={darkTheme}>
      <Box sx={{ width: '500px', height: '800px' }}>
        <ObjectManager
          onClose={(): void => {}}
          isOpen
          drawingListItems={drawingListItems as DrawingListItem[]}
          error="Something went wrong"
        />
      </Box>
    </WarningsThemeProvider>
  );
};

ObjectManagerErrorLight.storyName = 'ObjectManagerErrorLight (takeSnapshot)';
ObjectManagerErrorDark.storyName = 'ObjectManagerErrorDark (takeSnapshot)';
