/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */

import {
  CoreAppStore,
  mapStoreActions,
  uiActions,
  uiTypes,
  drawtoolSelectors,
  layerActions,
  drawtoolActions,
  layerSelectors,
  getSingularDrawtoolDrawLayerId,
  uiSelectors,
} from '@opengeoweb/store';
import { emptyGeoJSON } from '@opengeoweb/webmap-react';
import { useDispatch, useSelector } from 'react-redux';
import { drawActions } from '../../store/drawings/reducer';

import {
  drawingDialogType,
  objectDialogType,
} from '../../store/drawings/utils';

export const useCloseDrawDialog = ({
  mapId,
  source,
}: {
  mapId: string;
  source?: uiTypes.Source;
}): { closeDrawDialog: () => void } => {
  const dispatch = useDispatch();
  const drawLayerId = getSingularDrawtoolDrawLayerId(mapId);

  const drawDialogActiveDrawModeId = useSelector((store: CoreAppStore) =>
    drawtoolSelectors.getActiveDrawModeId(store, drawingDialogType),
  );

  const drawDialogGeoJSON = useSelector((store: CoreAppStore) =>
    layerSelectors.getFeatureLayerGeoJSON(store, drawLayerId),
  );
  const dialogError = useSelector((store: CoreAppStore) =>
    uiSelectors.getDialogError(store, drawingDialogType),
  );

  const closeDrawDialog = (): void => {
    if (drawDialogActiveDrawModeId) {
      // disable the edit layer
      dispatch(
        layerActions.toggleFeatureMode({
          layerId: drawLayerId,
          isInEditMode: false,
          drawMode: '',
        }),
      );
      // reset active draw tool
      dispatch(
        drawtoolActions.changeDrawToolMode({
          drawToolId: drawingDialogType,
          drawModeId: drawDialogActiveDrawModeId,
        }),
      );
    }
    // close draw dialog
    dispatch(
      uiActions.setActiveMapIdForDialog({
        type: drawingDialogType,
        mapId,
        setOpen: false,
        source,
      }),
    );

    // reset all drawings fields
    dispatch(
      drawActions.setDrawValues({
        drawingInstanceId: objectDialogType,
        objectName: '',
        id: '',
      }),
    );

    // reset error
    if (dialogError) {
      dispatch(
        uiActions.setErrorDialog({
          type: drawingDialogType,
          error: '',
        }),
      );
    }

    // clear existing geoJSON
    if (drawDialogGeoJSON) {
      dispatch(
        mapStoreActions.layerChangeGeojson({
          layerId: drawLayerId,
          geojson: emptyGeoJSON,
        }),
      );
    }
  };

  return { closeDrawDialog };
};

export const useCloseObjectDialog = ({
  mapId,
  source,
}: {
  mapId: string;
  source?: uiTypes.Source;
}): { closeObjectDialog: (shouldReset?: boolean) => void } => {
  const dispatch = useDispatch();
  const drawLayerId = getSingularDrawtoolDrawLayerId(mapId);

  const objectGeoJSONShape = useSelector((store: CoreAppStore) =>
    layerSelectors.getFeatureLayerGeoJSON(store, drawLayerId),
  );

  const closeObjectDialog = (shouldReset = true): void => {
    // reset active object
    if (shouldReset) {
      dispatch(
        drawActions.setDrawValues({
          drawingInstanceId: objectDialogType,
          objectName: '',
          id: '',
        }),
      );
    }

    // close object dialog
    dispatch(
      uiActions.setActiveMapIdForDialog({
        type: objectDialogType,
        mapId,
        setOpen: false,
        source,
      }),
    );

    if (objectGeoJSONShape && shouldReset) {
      dispatch(
        mapStoreActions.layerChangeGeojson({
          layerId: drawLayerId,
          geojson: emptyGeoJSON,
        }),
      );
    }
  };

  return { closeObjectDialog };
};

const useDialogOptions = ({
  dialogType,
}: {
  dialogType: uiTypes.DialogTypes;
}): {
  isDialogOpen: boolean;
  dialogMapId: string;
  dialogLayerId: string;
  geoJSON: GeoJSON.FeatureCollection | undefined;
} => {
  const isDialogOpen = useSelector((store: CoreAppStore) =>
    uiSelectors.getisDialogOpen(store, dialogType),
  );
  const dialogMapId = useSelector((store: CoreAppStore) =>
    uiSelectors.getDialogMapId(store, dialogType),
  );
  const dialogLayerId = getSingularDrawtoolDrawLayerId(dialogMapId);
  const geoJSON = useSelector((store: CoreAppStore) =>
    layerSelectors.getFeatureLayerGeoJSON(store, dialogLayerId),
  );

  return { isDialogOpen, dialogMapId, geoJSON, dialogLayerId };
};

export const useObjectDrawDialogAction = ({
  mapId,
  source,
}: {
  mapId: string;
  source?: uiTypes.Source;
}): {
  openDrawDialog: (geoJSON?: GeoJSON.FeatureCollection) => void;
  closeDrawDialog: () => void;
  openObjectDialog: () => void;
  closeObjectDialog: (shouldReset?: boolean) => void;
} => {
  const dispatch = useDispatch();
  const currentDrawLayerId = getSingularDrawtoolDrawLayerId(mapId);
  // draw selectors
  const {
    isDialogOpen: isDrawDialogOpen,
    geoJSON: drawDialogGeoJSON,
    dialogLayerId: drawDialogLayerId,
  } = useDialogOptions({ dialogType: drawingDialogType });

  const drawDialogActiveDrawModeId = useSelector((store: CoreAppStore) =>
    drawtoolSelectors.getActiveDrawModeId(store, drawingDialogType),
  );

  // object selectors
  const {
    isDialogOpen: isObjectDialogOpen,
    geoJSON: objectGeoJSONShape,
    dialogLayerId: objectDialogLayerId,
  } = useDialogOptions({ dialogType: objectDialogType });

  const clearGeoJSON = (layerId: string): void => {
    dispatch(
      mapStoreActions.layerChangeGeojson({
        layerId,
        geojson: emptyGeoJSON,
      }),
    );
  };

  const openObjectDialog = (): void => {
    if (isDrawDialogOpen) {
      if (drawDialogGeoJSON) {
        clearGeoJSON(drawDialogLayerId);
      }
      closeDrawDialog();
    }

    // open object dialog
    dispatch(
      uiActions.setActiveMapIdForDialog({
        type: objectDialogType,
        mapId,
        setOpen: true,
        source,
      }),
    );

    // move shape to screen
    if (isObjectDialogOpen && objectGeoJSONShape) {
      // update new shape
      dispatch(
        mapStoreActions.layerChangeGeojson({
          layerId: currentDrawLayerId,
          geojson: objectGeoJSONShape,
        }),
      );
      // remove current shape
      clearGeoJSON(objectDialogLayerId);
    }
  };

  const openDrawDialog = (newGeoJSON: GeoJSON.FeatureCollection): void => {
    if (isObjectDialogOpen) {
      if (objectGeoJSONShape) {
        clearGeoJSON(objectDialogLayerId);
      }
      const shouldResetShape = newGeoJSON === undefined;
      closeObjectDialog(shouldResetShape);
    }

    // open draw dialog
    dispatch(
      uiActions.setActiveMapIdForDialog({
        type: drawingDialogType,
        mapId,
        setOpen: true,
        source,
      }),
    );

    // activate draw tool
    dispatch(
      drawtoolActions.updateGeoJSONLayerId({
        drawToolId: drawingDialogType,
        geoJSONLayerId: currentDrawLayerId,
      }),
    );
    // move shape to screen
    if (isDrawDialogOpen && drawDialogGeoJSON) {
      // update new shape
      dispatch(
        mapStoreActions.layerChangeGeojson({
          layerId: currentDrawLayerId,
          geojson: drawDialogGeoJSON,
        }),
      );
      // remove current shape
      clearGeoJSON(drawDialogLayerId);
    }

    if (drawDialogActiveDrawModeId) {
      // activate current
      dispatch(
        layerActions.toggleFeatureMode({
          layerId: currentDrawLayerId,
          isInEditMode: true,
          drawMode: drawDialogActiveDrawModeId,
        }),
      );

      // deactivate previous
      dispatch(
        layerActions.toggleFeatureMode({
          layerId: drawDialogLayerId,
          isInEditMode: false,
          drawMode: '',
        }),
      );
    }

    if (newGeoJSON) {
      dispatch(
        mapStoreActions.layerChangeGeojson({
          layerId: currentDrawLayerId,
          geojson: newGeoJSON,
        }),
      );
    }
  };

  const { closeDrawDialog } = useCloseDrawDialog({
    mapId,
    source,
  });

  const { closeObjectDialog } = useCloseObjectDialog({ mapId, source });

  return {
    closeDrawDialog,
    closeObjectDialog,
    openObjectDialog,
    openDrawDialog,
  };
};
