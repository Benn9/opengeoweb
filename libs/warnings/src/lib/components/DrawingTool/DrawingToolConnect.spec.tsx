/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { createStore } from '@redux-eggs/redux-toolkit';
import { getSagaExtension } from '@redux-eggs/saga-extension';
import {
  render,
  fireEvent,
  screen,
  act,
  waitFor,
} from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import { uiTypes, mapActions } from '@opengeoweb/store';
import { currentlySupportedDrawModes } from '@opengeoweb/webmap-react';
import DrawingToolConnect, { getDialogType } from './DrawingToolConnect';
import { WarningsThemeStoreProvider } from '../Providers/Providers';
import DrawingToolMapButtonConnect from './DrawingToolMapButtonConnect';

describe('src/components/MapDraw/DrawingTool/DrawingToolConnect', () => {
  const store = createStore({
    extensions: [getSagaExtension()],
  });

  it('should register the drawing dialog and drawtools when mounting and unregister when unmounting', async () => {
    const mapId = 'map123';
    store.dispatch(mapActions.registerMap({ mapId }));

    const { unmount } = render(
      <WarningsThemeStoreProvider store={store}>
        <DrawingToolConnect />
      </WarningsThemeStoreProvider>,
    );

    await waitFor(() =>
      expect(store.getState().ui.dialogs.drawingTool?.type).toEqual(
        uiTypes.DialogTypes.DrawingTool,
      ),
    );
    expect(
      store.getState().drawingtools.entities.drawingTool?.drawModes,
    ).toEqual(currentlySupportedDrawModes);

    unmount();

    await waitFor(() =>
      expect(store.getState().ui?.dialogs.drawingTool).toBeFalsy(),
    );
    expect(store.getState().drawingtools?.entities.drawingTool).toBeFalsy();
  });

  it('should handle focus, blur and close', async () => {
    const mapId = 'map124';

    render(
      <WarningsThemeStoreProvider store={store}>
        <DrawingToolConnect />
        <DrawingToolMapButtonConnect mapId={mapId} />
      </WarningsThemeStoreProvider>,
    );
    act(() => {
      store.dispatch(mapActions.registerMap({ mapId }));
    });

    const user = userEvent.setup();

    // open drawing tool
    await user.click(
      screen.getByRole('button', {
        name: `Drawing Toolbox`,
      }),
    );
    expect(await screen.findByTestId('drawingToolWindow')).toBeTruthy();

    fireEvent.focus(screen.getByTestId('drawingToolWindow'));
    expect(store.getState().ui.dialogs.drawingTool?.focused).toEqual(true);

    fireEvent.blur(screen.getByTestId('drawingToolWindow'));
    expect(store.getState().ui.dialogs.drawingTool?.focused).toEqual(false);

    await user.click(
      screen.getByRole('button', {
        name: `Close`,
      }),
    );
    expect(screen.queryByTestId('drawingToolWindow')).toBeFalsy();
  });

  it('should register 2x isMultiMap dialogs but with one instance', async () => {
    const mapId = 'map125';
    const mapId2 = 'map126';
    store.dispatch(mapActions.registerMap({ mapId }));
    store.dispatch(mapActions.registerMap({ mapId: mapId2 }));

    render(
      <WarningsThemeStoreProvider store={store}>
        <DrawingToolConnect mapId={mapId} isMultiMap showMapIdInTitle />
        <DrawingToolConnect mapId={mapId2} isMultiMap showMapIdInTitle />
      </WarningsThemeStoreProvider>,
    );

    expect(store.getState().ui.dialogs[`drawingTool-${mapId}`]?.type).toEqual(
      `${uiTypes.DialogTypes.DrawingTool}-${mapId}`,
    );
    expect(store.getState().ui.dialogs[`drawingTool-${mapId2}`]?.type).toEqual(
      `${uiTypes.DialogTypes.DrawingTool}-${mapId2}`,
    );

    expect(store.getState().drawingtools.entities.drawingTool).toBeTruthy();
  });

  describe('getDialogType', () => {
    const mapId = 'map123';
    it('should return the correct dialog type', async () => {
      // multimap
      expect(getDialogType(mapId, true)).toEqual(
        `${uiTypes.DialogTypes.DrawingTool}-${mapId}`,
      );
      // default
      expect(getDialogType(mapId, false)).toEqual(
        uiTypes.DialogTypes.DrawingTool,
      );
    });
  });
});
