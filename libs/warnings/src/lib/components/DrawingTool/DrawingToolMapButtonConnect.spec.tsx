/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import {
  render,
  fireEvent,
  screen,
  waitFor,
  act,
} from '@testing-library/react';
import { mapActions } from '@opengeoweb/store';

import { createStore } from '@redux-eggs/redux-toolkit';
import { getSagaExtension } from '@redux-eggs/saga-extension';
import DrawingToolMapButtonConnect from './DrawingToolMapButtonConnect';
import { WarningsThemeStoreProvider } from '../Providers/Providers';

import DrawingToolConnect from './DrawingToolConnect';

describe('src/components/DrawingTool/DrawingToolMapButtonConnect', () => {
  const store = createStore({
    extensions: [getSagaExtension()],
  });
  it('should open and close drawing tool dialog for given mapId', async () => {
    const mapId = 'mapId_123';
    const mapId2 = 'mapId_124';

    render(
      <WarningsThemeStoreProvider store={store}>
        <DrawingToolConnect />
        <DrawingToolMapButtonConnect mapId={mapId} />
        <DrawingToolMapButtonConnect mapId={mapId2} />
      </WarningsThemeStoreProvider>,
    );

    act(() => {
      store.dispatch(mapActions.registerMap({ mapId }));
      store.dispatch(mapActions.registerMap({ mapId: mapId2 }));
    });

    // buttons should be present
    expect(
      screen.getAllByRole('button', { name: 'Drawing Toolbox' }).length,
    ).toEqual(2);

    // open the drawing tool dialog for the first map
    fireEvent.click(
      screen.getAllByRole('button', { name: 'Drawing Toolbox' })[0],
    );
    await waitFor(() =>
      expect(store.getState().ui.dialogs.drawingTool.activeMapId).toEqual(
        mapId,
      ),
    );
    expect(store.getState().ui.dialogs.drawingTool.isOpen).toEqual(true);

    // open the drawing tool dialog for the second map
    fireEvent.click(
      screen.getAllByRole('button', { name: 'Drawing Toolbox' })[1],
    );
    await waitFor(() =>
      expect(store.getState().ui.dialogs.drawingTool.activeMapId).toEqual(
        mapId2,
      ),
    );
    expect(store.getState().ui.dialogs.drawingTool.isOpen).toEqual(true);

    // close the drawing tool dialog
    fireEvent.click(
      screen.getAllByRole('button', { name: 'Drawing Toolbox' })[1],
    );

    await waitFor(() =>
      expect(store.getState().ui.dialogs.drawingTool.isOpen).toEqual(false),
    );
  });
});
