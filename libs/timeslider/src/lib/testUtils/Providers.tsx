/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */

import React from 'react';
import { Provider } from 'react-redux';
import { Theme } from '@mui/material';
import {
  lightTheme,
  ThemeProviderProps,
  ThemeWrapper,
} from '@opengeoweb/theme';
import { Store } from '@reduxjs/toolkit';
import { withEggs } from '@opengeoweb/shared';
import { coreModuleConfig } from '@opengeoweb/store';

interface BaseProviderProps {
  children?: React.ReactNode;
  theme?: Theme;
}

export const ThemeProvider: React.FC<BaseProviderProps> = ({
  children,
  theme = lightTheme,
}) => <ThemeWrapper theme={theme}>{children}</ThemeWrapper>;

const ThemeWrapperWithModules: React.FC<ThemeProviderProps> = withEggs(
  coreModuleConfig,
)(({ theme, children }) => {
  return <ThemeProvider theme={theme}>{children} </ThemeProvider>;
});

interface ThemeStoreProviderProps extends BaseProviderProps {
  store: Store;
}

export const ThemeStoreProvider: React.FC<ThemeStoreProviderProps> = ({
  children,
  theme = lightTheme,
  store,
}) => (
  <Provider store={store}>
    <ThemeWrapperWithModules theme={theme}>
      {children as React.ReactElement}
    </ThemeWrapperWithModules>
  </Provider>
);
