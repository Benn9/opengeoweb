/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { render, fireEvent, screen } from '@testing-library/react';
import moment from 'moment';
import { TimeSliderLegend, TimeSliderLegendProps } from './TimeSliderLegend';
import { ThemeProvider } from '../../testUtils/Providers';

describe('src/lib/components/TimeSlider/TimeSliderLegend/TimeSliderLegend', () => {
  const date = moment.utc('20200101', 'YYYYMMDD');
  const props: TimeSliderLegendProps = {
    mapId: 'map_1',
    timeStep: 5,
    centerTime: moment.utc(date).startOf('day').hours(12).unix(),
    currentTime: moment.utc(date).startOf('day').hours(12).unix(),
    secondsPerPx: 5,
    unfilteredSelectedTime: moment.utc(date).startOf('day').hours(12).unix(),
    timeSliderWidth: 100,
    setUnfilteredSelectedTime: jest.fn(),
    onZoom: jest.fn(),
    onSetNewDate: jest.fn(),
    onSetCenterTime: jest.fn(),
  };

  jest.useFakeTimers();

  it('should render the component with canvas', () => {
    render(
      <ThemeProvider>
        <TimeSliderLegend {...props} />
      </ThemeProvider>,
    );
    expect(screen.getByRole('presentation', { name: 'canvas' })).toBeTruthy();
  });

  it('should follow selected time when hovering mouse on the legend while not changing center time', () => {
    const props = {
      mapId: 'map_1',
      scale: 5,
      timeStep: 5,
      centerTime: moment.utc(date).startOf('day').hours(12).unix(),
      currentTime: moment.utc(date).startOf('day').hours(12).unix(),
      selectedTime: moment.utc(date).startOf('day').hours(12).unix(),
      dataStartTime: moment.utc(date).startOf('day').unix(),
      dataEndTime: moment.utc(date).endOf('day').unix(),
      isTimeSliderHoverOn: true,
      secondsPerPx: 30,
      unfilteredSelectedTime: moment.utc(date).startOf('day').hours(12).unix(),
      timeSliderWidth: 100,
      setUnfilteredSelectedTime: jest.fn(),
      onZoom: jest.fn(),
      onSetNewDate: jest.fn(),
      onSetCenterTime: jest.fn(),
    };

    const { baseElement } = render(
      <ThemeProvider>
        <TimeSliderLegend {...props} />
      </ThemeProvider>,
    );
    expect(baseElement).toBeTruthy();

    const canvas = screen.getByRole('presentation', { name: 'canvas' });
    expect(canvas).toBeTruthy();

    canvas.focus();
    fireEvent.mouseMove(canvas);

    // When hover is on, time instant should not get selected by just moving mouse on legend
    expect(props.onSetNewDate).not.toHaveBeenCalled();

    // Center time should stay intact
    expect(props.onSetCenterTime).not.toHaveBeenCalled();
  });

  it('should not do any time selections at all when just moving mouse on the legend with no hover on', () => {
    const props = {
      mapId: 'map_1',
      scale: 5,
      timeStep: 5,
      centerTime: moment.utc(date).startOf('day').hours(12).unix(),
      currentTime: moment.utc(date).startOf('day').hours(12).unix(),
      selectedTime: moment.utc(date).startOf('day').hours(12).unix(),
      dataStartTime: moment.utc(date).startOf('day').unix(),
      dataEndTime: moment.utc(date).endOf('day').unix(),
      isTimeSliderHoverOn: false,
      secondsPerPx: 30,
      unfilteredSelectedTime: moment.utc(date).startOf('day').hours(12).unix(),
      timeSliderWidth: 100,
      setUnfilteredSelectedTime: jest.fn(),
      onZoom: jest.fn(),
      onSetNewDate: jest.fn(),
      onSetCenterTime: jest.fn(),
    };

    const { baseElement } = render(
      <ThemeProvider>
        <TimeSliderLegend {...props} />
      </ThemeProvider>,
    );

    expect(baseElement).toBeTruthy();

    const canvas = screen.getByRole('presentation', { name: 'canvas' })
      .firstChild as HTMLElement;
    expect(canvas).toBeTruthy();

    canvas.focus();
    fireEvent.mouseMove(canvas);

    expect(props.onSetNewDate).not.toHaveBeenCalled();
    expect(props.onSetCenterTime).not.toHaveBeenCalled();
  });

  it('should move legend only (changing center time, not selected time) when mouse is dragged far away from needle', async () => {
    const props = {
      mapId: 'map_1',
      scale: 5,
      timeStep: 5,
      centerTime: moment.utc(date).startOf('day').hours(12).unix(),
      currentTime: moment.utc(date).startOf('day').hours(12).unix(),
      selectedTime: moment.utc(date).startOf('day').hours(12).unix(),
      dataStartTime: moment.utc(date).startOf('day').unix(),
      dataEndTime: moment.utc(date).endOf('day').unix(),
      isTimeSliderHoverOn: false,
      secondsPerPx: 30,
      unfilteredSelectedTime: moment.utc(date).startOf('day').hours(12).unix(),
      timeSliderWidth: 100,
      setUnfilteredSelectedTime: jest.fn(),
      onZoom: jest.fn(),
      onSetNewDate: jest.fn(),
      onSetCenterTime: jest.fn(),
    };

    const { baseElement } = render(
      <ThemeProvider>
        <TimeSliderLegend {...props} />
      </ThemeProvider>,
    );
    expect(baseElement).toBeTruthy();

    const canvas = screen.getByRole('presentation', { name: 'canvas' })
      .firstChild as HTMLCanvasElement;
    expect(canvas).toBeTruthy();
    canvas.focus();

    fireEvent.mouseDown(canvas, {
      x: 0,
      y: 0,
    });

    fireEvent.mouseMove(canvas, {
      buttons: 1, // Dragging ~ mouse down while mouse moving
      x: 0,
      y: 0,
      width: canvas.width,
    });

    expect(props.onSetCenterTime).toHaveBeenCalled();
    expect(props.onSetNewDate).not.toHaveBeenCalled();
  });

  it('should move current time when mouse scrolled on legend', () => {
    const extendProps = {
      ...props,
      selectedTime: moment.utc(date).startOf('day').hours(12).unix(),
      dataStartTime: moment.utc(date).startOf('day').hours(11).unix(),
      dataEndTime: moment.utc(date).startOf('day').hours(13).unix(),
      isTimeSliderHoverOn: true,
    };

    const { baseElement } = render(
      <ThemeProvider>
        <TimeSliderLegend {...extendProps} />
      </ThemeProvider>,
    );
    expect(baseElement).toBeTruthy();
    const canvas = screen.getByRole('presentation', { name: 'canvas' })
      .firstChild as HTMLElement;

    // deltaY values equivalent to one movement of a mouse scroll wheel
    // should move one timestep forward or backward.
    const deltaY1 = 100;
    const deltaY2 = -100;

    fireEvent.wheel(canvas, { deltaY: deltaY1 });
    expect(props.onSetNewDate).not.toBeCalled();

    jest.runOnlyPendingTimers();
    expect(props.onSetNewDate).toHaveBeenCalledTimes(1);
    expect(props.onSetNewDate).toHaveBeenCalledWith('2020-01-01T11:55:00.000Z');

    fireEvent.wheel(canvas, { deltaY: deltaY2 });
    expect(props.onSetNewDate).toHaveBeenCalledTimes(1);

    jest.runOnlyPendingTimers();
    expect(props.onSetNewDate).toHaveBeenCalledTimes(2);
    expect(props.onSetNewDate).toHaveBeenCalledWith('2020-01-01T12:05:00.000Z');
  });

  it('should not move current time when mouse scrolled on legend when selectedTime is dataEndTime', () => {
    const extendProps = {
      ...props,
      selectedTime: moment.utc(date).startOf('day').hours(12).unix(),
      dataStartTime: moment.utc(date).startOf('day').hours(11).unix(),
      dataEndTime: moment.utc(date).startOf('day').hours(12).unix(),
      unfilteredSelectedTime: moment.utc(date).startOf('day').hours(12).unix(),
    };

    const { baseElement } = render(
      <ThemeProvider>
        <TimeSliderLegend {...extendProps} />
      </ThemeProvider>,
    );
    expect(baseElement).toBeTruthy();
    const canvas = screen.getByRole('presentation', { name: 'canvas' })
      .firstChild as HTMLElement;

    // deltaY values equivalent to one movement of a mouse scroll wheel
    // should move one timestep forward or backward.
    const deltaY1 = -100;
    const deltaY2 = 100;

    fireEvent.wheel(canvas, { deltaY: deltaY1 });
    expect(props.onSetNewDate).not.toBeCalled();

    jest.runOnlyPendingTimers();
    expect(props.onSetNewDate).toHaveBeenCalledTimes(1);
    expect(props.onSetNewDate).toHaveBeenCalledWith('2020-01-01T12:00:00.000Z');

    fireEvent.wheel(canvas, { deltaY: deltaY2 });
    expect(props.onSetNewDate).toHaveBeenCalledTimes(1);

    jest.runOnlyPendingTimers();
    expect(props.onSetNewDate).toHaveBeenCalledTimes(2);
    expect(props.onSetNewDate).toHaveBeenCalledWith('2020-01-01T11:55:00.000Z');
  });
});
