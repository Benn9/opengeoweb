/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */

import {
  isInsideAnimationArea,
  isLeftAnimationIconArea,
  isRightAnimationIconArea,
  isSelectedTimeIconArea,
} from './timeSliderLegendUtils';

describe('src/lib/components/TimeSlider/TimeSliderLegend/TimeSliderLegendUtils', () => {
  const x = 50;
  const width = 24;

  it('should return true if chosen value is inside the needle area', () => {
    expect(isSelectedTimeIconArea(x, x - 5, width)).toBeTruthy();
    expect(isSelectedTimeIconArea(x, x + width, width)).toBeFalsy();
  });
  it('should return true if chosen value is inside the left or right animation area', () => {
    expect(isRightAnimationIconArea(x, x, width)).toBeTruthy();
    expect(isLeftAnimationIconArea(x, x, width)).toBeTruthy();

    expect(isRightAnimationIconArea(x, x + width, width)).toBeTruthy();
    expect(isLeftAnimationIconArea(x, x - width, width)).toBeTruthy();

    expect(isRightAnimationIconArea(x, x - width, width)).toBeFalsy();
    expect(isLeftAnimationIconArea(x, x + width, width)).toBeFalsy();
  });

  it('should check if x is inside left and right animation area', () => {
    expect(isInsideAnimationArea(x, x - 10, x + 10)).toBeTruthy();
    expect(isInsideAnimationArea(x, x - 10, x - 5)).toBeFalsy();
    expect(isInsideAnimationArea(x, x + 5, x + 10)).toBeFalsy();
  });
});
