/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { lightTheme, darkTheme, ThemeWrapper } from '@opengeoweb/theme';
import { Box } from '@mui/material';
import { add, getUnixTime, sub } from 'date-fns';
import { TimeSliderLegend, TimeSliderLegendProps } from './TimeSliderLegend';

export default { title: 'components/TimeSlider/TimeSliderLegend' };

const date = `2020-10-30T`;
const animationStartTime1700 = `${date}17:00:00.000Z`;
const centerTime1800 = getUnixTime(new Date(`${date}18:00:00.000Z`));

const t1900iso = `${date}19:00:00.000Z`;
const t1900Date = new Date(t1900iso);
const t1900 = getUnixTime(t1900Date);

const commonStoryProps: TimeSliderLegendProps = {
  mapId: 'map_1',
  secondsPerPx: 0,
  animationStartTime: animationStartTime1700,
  centerTime: centerTime1800,
  animationEndTime: t1900iso,
  unfilteredSelectedTime: t1900,
  timeSliderWidth: 100,
};

const TimeSliderLegendDisplay = (): React.ReactElement => {
  const dayAfter1900 = getUnixTime(add(t1900Date, { days: 1 }));
  const dayBefore1900 = getUnixTime(sub(t1900Date, { days: 1 }));
  return (
    <div>
      <Box m={2}>
        <TimeSliderLegend
          {...commonStoryProps}
          secondsPerPx={3}
          currentTime={t1900}
        />
      </Box>
      <Box m={2}>
        <TimeSliderLegend
          {...commonStoryProps}
          secondsPerPx={6}
          currentTime={dayAfter1900}
        />
      </Box>
      <Box m={2}>
        <TimeSliderLegend
          {...commonStoryProps}
          secondsPerPx={12}
          currentTime={dayBefore1900}
        />
      </Box>
      <Box m={2}>
        <TimeSliderLegend
          {...commonStoryProps}
          secondsPerPx={24}
          currentTime={dayBefore1900}
        />
      </Box>
      <Box m={2}>
        <TimeSliderLegend
          {...commonStoryProps}
          secondsPerPx={48}
          currentTime={dayBefore1900}
        />
      </Box>
      <Box m={2}>
        <TimeSliderLegend
          {...commonStoryProps}
          secondsPerPx={96}
          currentTime={dayBefore1900}
        />
      </Box>
      <Box m={2}>
        <TimeSliderLegend
          {...commonStoryProps}
          secondsPerPx={336}
          currentTime={dayBefore1900}
        />
      </Box>
      <Box m={2}>
        <TimeSliderLegend
          {...commonStoryProps}
          secondsPerPx={672}
          currentTime={dayBefore1900}
        />
      </Box>
      <Box m={2}>
        <TimeSliderLegend
          {...commonStoryProps}
          secondsPerPx={1440}
          currentTime={dayBefore1900}
        />
      </Box>
      <Box m={2}>
        <TimeSliderLegend
          {...commonStoryProps}
          secondsPerPx={4320}
          currentTime={dayBefore1900}
        />
      </Box>
      <Box m={2}>
        <TimeSliderLegend
          {...commonStoryProps}
          secondsPerPx={17520}
          currentTime={dayBefore1900}
        />
      </Box>
    </div>
  );
};

export const TimeSliderLegendDemoLight = (): React.ReactElement => (
  <ThemeWrapper theme={lightTheme}>
    <TimeSliderLegendDisplay />
  </ThemeWrapper>
);
TimeSliderLegendDemoLight.storyName =
  'Time Slider Legend Light Theme (takeSnapshot)';
TimeSliderLegendDemoLight.parameters = {
  zeplinLink: [
    {
      name: 'Light theme',
      link: 'https://app.zeplin.io/project/5ecf84a3c6ae1047a368f393/screen/609aac358e8408bddafecdb8/version/636a1d6dbf31ce15fc6c4f2b',
    },
  ],
};

export const TimeSliderLegendDemoDark = (): React.ReactElement => (
  <ThemeWrapper theme={darkTheme}>
    <TimeSliderLegendDisplay />
  </ThemeWrapper>
);
TimeSliderLegendDemoDark.storyName =
  'Time Slider Legend Dark Theme (takeSnapshot)';
TimeSliderLegendDemoDark.parameters = {
  zeplinLink: [
    {
      name: 'Dark theme',
      link: 'https://app.zeplin.io/project/5ecf84a3c6ae1047a368f393/screen/605b5a7151c9824390c1f83c/version/636a1d8d49272e1618c51e0b',
    },
  ],
};
