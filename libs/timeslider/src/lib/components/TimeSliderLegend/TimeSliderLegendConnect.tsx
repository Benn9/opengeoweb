/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { useDispatch, useSelector } from 'react-redux';

import {
  mapActions,
  mapSelectors,
  genericActions,
  CoreAppStore,
  mapEnums,
} from '@opengeoweb/store';

import { getUnixTime } from 'date-fns';
import { handleMomentISOString } from '@opengeoweb/webmap';
import { TimeSliderLegend } from './TimeSliderLegend';
import {
  secondsPerPxFromCanvasWidth,
  useTurnOffAutoUpdateIfItsOn,
} from '../TimeSlider/timeSliderUtils';

interface TimeSliderLegendConnectProps {
  sourceId?: string;
  mapId: string;
}

export const TimeSliderLegendConnect: React.FC<
  TimeSliderLegendConnectProps
> = ({ sourceId, mapId }) => {
  const centerTime = useSelector((store: CoreAppStore) =>
    mapSelectors.getMapTimeSliderCenterTime(store, mapId),
  );
  const timeSliderWidth = useSelector((store: CoreAppStore) =>
    mapSelectors.getMapTimeSliderWidth(store, mapId),
  );
  const secondsPerPx = useSelector((store: CoreAppStore) =>
    mapSelectors.getMapTimeSliderSecondsPerPx(store, mapId),
  );
  const isAnimating = useSelector((store: CoreAppStore) =>
    mapSelectors.isAnimating(store, mapId),
  );
  const timeStep = useSelector((store: CoreAppStore) =>
    mapSelectors.getMapTimeStep(store, mapId),
  );
  const isTimeSliderHoverOn = useSelector((store: CoreAppStore) =>
    mapSelectors.isTimeSliderHoverOn(store, mapId),
  );

  const animationStartTime = useSelector((store: CoreAppStore) =>
    mapSelectors.getAnimationStartTime(store, mapId),
  );
  const animationEndTime = useSelector((store: CoreAppStore) =>
    mapSelectors.getAnimationEndTime(store, mapId),
  );

  const unfilteredSelectedTime = useSelector((store: CoreAppStore) =>
    mapSelectors.getTimeSliderUnfilteredSelectedTime(store, mapId),
  );

  const selectedTime = useSelector((store: CoreAppStore) =>
    mapSelectors.getSelectedTime(store, mapId),
  );
  const currentTime = getUnixTime(Date.now());

  const [dataStartTime, dataEndTime] = useSelector((store: CoreAppStore) =>
    mapSelectors.getDataLimitsFromLayers(store, mapId),
  );
  const dispatch = useDispatch();
  const turnOffAutoUpdateIfItsOn = useTurnOffAutoUpdateIfItsOn(mapId);

  return (
    <TimeSliderLegend
      mapId={mapId}
      centerTime={centerTime!}
      timeSliderWidth={timeSliderWidth!}
      secondsPerPx={secondsPerPx!}
      selectedTime={selectedTime}
      currentTime={currentTime}
      isTimeSliderHoverOn={isTimeSliderHoverOn}
      dataStartTime={dataStartTime}
      dataEndTime={dataEndTime}
      animationStartTime={animationStartTime}
      animationEndTime={animationEndTime}
      timeStep={timeStep}
      unfilteredSelectedTime={unfilteredSelectedTime || selectedTime}
      setUnfilteredSelectedTime={(
        timeSliderUnfilteredSelectedTime: number,
      ): void => {
        dispatch(
          mapActions.setTimeSliderUnfilteredSelectedTime({
            timeSliderUnfilteredSelectedTime,
            mapId,
          }),
        );
      }}
      onSetNewDate={(newDate): void => {
        turnOffAutoUpdateIfItsOn();
        if (isAnimating) {
          dispatch(mapActions.mapStopAnimation({ mapId }));
        }
        dispatch(
          genericActions.setTime({
            sourceId: sourceId!,
            origin: 'TimeSliderConnect, 139',
            value: handleMomentISOString(newDate),
          }),
        );
      }}
      onSetCenterTime={(newTime: number): void => {
        dispatch(
          mapActions.setTimeSliderCenterTime({
            mapId,
            timeSliderCenterTime: newTime,
          }),
        );
      }}
      onZoom={(newSecondsPerPx, newCenterTime): void => {
        dispatch(
          mapActions.setTimeSliderSecondsPerPx({
            mapId,
            timeSliderSecondsPerPx: newSecondsPerPx,
          }),
        );
        dispatch(
          mapActions.setTimeSliderCenterTime({
            mapId,
            timeSliderCenterTime: newCenterTime,
          }),
        );
      }}
      onSetAnimationStartTime={(animationStartTime: string): void => {
        dispatch(
          mapActions.setAnimationStartTime({
            mapId,
            animationStartTime,
            origin: mapEnums.MapActionOrigin.map,
          }),
        );
      }}
      onSetAnimationEndTime={(animationEndTime: string): void => {
        dispatch(
          mapActions.setAnimationEndTime({
            mapId,
            animationEndTime,
            origin: mapEnums.MapActionOrigin.map,
          }),
        );
      }}
      updateCanvasWidth={(storeWidth: number, newWidth: number): void => {
        if (storeWidth !== newWidth) {
          dispatch(
            mapActions.setTimeSliderWidth({
              mapId,
              timeSliderWidth: newWidth,
            }),
          );
          const spanInSeconds = secondsPerPx! * storeWidth;

          const newSecondsPerPx = secondsPerPxFromCanvasWidth(
            newWidth,
            spanInSeconds,
          );
          if (newSecondsPerPx !== undefined) {
            dispatch(
              mapActions.setTimeSliderSecondsPerPx({
                mapId,
                timeSliderSecondsPerPx: newSecondsPerPx,
              }),
            );
          }
        }
      }}
    />
  );
};
