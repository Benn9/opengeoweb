/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */

import { Theme } from '@mui/material';
import moment from 'moment';
import { mapEnums, mapUtils } from '@opengeoweb/store';
import { timestampToPixelEdges } from '../../TimeSlider/timeSliderUtils';

type ScaleToUnitSecondsType = Partial<{ [key in mapEnums.Scale]: number }>;

const scaleToUnitSeconds: ScaleToUnitSecondsType = {
  [mapEnums.Scale.Minutes5]: 5 * 60,
  [mapEnums.Scale.Hour]: 60 * 60,
  [mapEnums.Scale.Hours3]: 3 * 60 * 60,
  [mapEnums.Scale.Hours6]: 6 * 60 * 60,
  [mapEnums.Scale.Day]: 24 * 60 * 60,
  [mapEnums.Scale.Week]: 7 * 24 * 60 * 60,
};

const TICK_MARK_HEIGHT_PRIMARY = 40;
const TICK_MARK_HEIGHT_SECONDARY = 10;
const TICK_MARK_HEIGHT_TERTIARY = 5;

const getStepPxLineHeight = (
  scale: mapEnums.Scale,
  timestep: number,
): number => {
  const scaleUnitSeconds = scaleToUnitSeconds[scale];
  const rem = timestep % scaleUnitSeconds!;
  switch (scale) {
    case mapEnums.Scale.Hour: {
      if (rem === 0) {
        return TICK_MARK_HEIGHT_PRIMARY;
      }
      // 15 min subdivisions
      if (rem % (15 * 60) === 0) {
        return TICK_MARK_HEIGHT_SECONDARY;
      }
      return TICK_MARK_HEIGHT_TERTIARY;
    }
    case mapEnums.Scale.Hours3: {
      if (rem === 0) {
        return TICK_MARK_HEIGHT_PRIMARY;
      }
      return TICK_MARK_HEIGHT_TERTIARY;
    }
    case mapEnums.Scale.Hours6: {
      if (rem === 0) {
        return TICK_MARK_HEIGHT_PRIMARY;
      }
      // 3 hour subdivisions
      if (rem % (3 * 60 * 60) === 0) {
        return TICK_MARK_HEIGHT_SECONDARY;
      }
      return TICK_MARK_HEIGHT_TERTIARY;
    }
    case mapEnums.Scale.Day: {
      if (rem === 0) {
        return TICK_MARK_HEIGHT_PRIMARY;
      }
      // 6 hour subdivisions
      if (rem % (6 * 60 * 60) === 0) {
        return TICK_MARK_HEIGHT_SECONDARY;
      }
      return TICK_MARK_HEIGHT_TERTIARY;
    }
    case mapEnums.Scale.Week: {
      const weekday = moment.unix(timestep).utc().isoWeekday();
      // momentjs isoWeekdays range from 1(Monday) to 7(Sunday)
      if (weekday === 1) {
        return TICK_MARK_HEIGHT_PRIMARY;
      }
      return TICK_MARK_HEIGHT_TERTIARY;
    }
    case mapEnums.Scale.Year: {
      const month = moment.unix(timestep).utc().month();
      // momentjs months are zero indexed
      if (month === 0) {
        return TICK_MARK_HEIGHT_PRIMARY;
      }
      if (month % 3 === 0) {
        return TICK_MARK_HEIGHT_SECONDARY;
      }
      return TICK_MARK_HEIGHT_TERTIARY;
    }
    case mapEnums.Scale.Minutes5: {
      // add primary line on day change
      if (timestep % (60 * 60 * 24) === 0) {
        return TICK_MARK_HEIGHT_PRIMARY;
      }
      return TICK_MARK_HEIGHT_TERTIARY;
    }
    default:
      // mapEnums.Scale.Month
      return TICK_MARK_HEIGHT_TERTIARY;
  }
};

const drawStepPxLine = (
  ctx: CanvasRenderingContext2D,
  theme: Theme,
  stepPx: number,
  height: number,
  fundamentalScale: mapEnums.Scale,
  timestep: number,
): void => {
  const ctx2 = ctx;
  ctx2.strokeStyle =
    theme.palette.geowebColors.timeSlider.timelineTimeScale.fill!;
  ctx2.lineWidth = 1;
  const stepPxLineHeight = getStepPxLineHeight(fundamentalScale, timestep);

  ctx2.beginPath();
  ctx2.moveTo(stepPx, height - stepPxLineHeight);
  ctx2.lineTo(stepPx, height);
  ctx2.stroke();
};

const drawTimeText = (
  ctx: CanvasRenderingContext2D,
  theme: Theme,
  stepPx: number,
  height: number,
  timeText: string,
  scale: mapEnums.Scale,
): void => {
  const ctx2 = ctx;
  const { fontSize, fontFamily, color } =
    theme.palette.geowebColors.timeSlider.timelineText;
  ctx2.fillStyle = color!;
  // time text has smaller font than other timeline text
  ctx2.font = `${fontSize}px ${fontFamily}`;
  ctx2.textAlign = 'left';
  if (
    scale === mapEnums.Scale.Year ||
    scale === mapEnums.Scale.Month ||
    scale === mapEnums.Scale.Week ||
    scale === mapEnums.Scale.Day
  ) {
    ctx2.fillText(timeText, stepPx, height - 12);
  } else {
    ctx2.fillText(timeText, stepPx - 15, height - 12);
  }
};

const drawYearScaleText = (
  ctx: CanvasRenderingContext2D,
  theme: Theme,
  newDate: string,
  stepPx: number,
  height: number,
): void => {
  const ctx2 = ctx;
  const { fontSize, fontFamily, color } =
    theme.palette.geowebColors.timeSlider.timelineText;
  ctx2.fillStyle = color!;
  ctx2.font = `${fontSize}px ${fontFamily}`;
  ctx2.textAlign = 'left';
  ctx2.fillText(newDate, stepPx + 2, height - 10);
};

const drawLeftSideDateText = (
  ctx: CanvasRenderingContext2D,
  theme: Theme,
  newDate: string,
  height: number,
): void => {
  const ctx2 = ctx;
  const { fontSize, fontFamily, color } =
    theme.palette.geowebColors.timeSlider.timelineText;
  ctx2.fillStyle = color!;
  ctx2.font = `${fontSize}px ${fontFamily}`;
  ctx2.textAlign = 'left';
  ctx2.fillText(newDate, 2, height - 30);
};

const getTimeFormat = (scale: number): string => {
  switch (scale) {
    case mapEnums.Scale.Year:
      return 'Y';
    case mapEnums.Scale.Month:
      return 'MMM';
    case mapEnums.Scale.Week:
      return 'DD MMM';
    case mapEnums.Scale.Day:
      return 'ddd DD';
    default:
      return 'HH:mm';
  }
};

const drawDateChangeLine = (
  ctx: CanvasRenderingContext2D,
  theme: Theme,
  stepPx: number,
  height: number,
): void => {
  const ctx2 = ctx;
  ctx2.strokeStyle =
    theme.palette.geowebColors.timeSlider.timelineTimeScale.fill!;
  ctx2.lineWidth = 1;
  ctx2.beginPath();
  ctx2.moveTo(stepPx, height - 40);
  ctx2.lineTo(stepPx, height);
  ctx2.stroke();
};

const drawDashedMonthChangeLine = (
  ctx: CanvasRenderingContext2D,
  theme: Theme,
  stepPx: number,
  height: number,
): void => {
  const ctx2 = ctx;
  ctx2.strokeStyle =
    theme.palette.geowebColors.timeSlider.timelineMonthChangeDash.rgba!;
  ctx2.lineWidth = 3;
  ctx2.setLineDash([5, 5]);
  ctx2.beginPath();
  ctx2.moveTo(stepPx, 0);
  ctx2.lineTo(stepPx, height);
  ctx2.stroke();
  ctx2.setLineDash([]);
};

export const roundByUnitOfTime = (
  timeUnix: number,
  scaleType: moment.unitOfTime.StartOf,
  roundUp = false,
  nightTime = false,
): number => {
  if (scaleType === 'day' && nightTime) {
    if (roundUp) {
      return moment
        .unix(timeUnix)
        .utc()
        .add(1, 'day')
        .hours(21)
        .minutes(30)
        .seconds(0)
        .unix();
    }
    return moment
      .unix(timeUnix)
      .utc()
      .subtract(1, 'day')
      .hours(6)
      .minutes(30)
      .seconds(0)
      .unix();
  }
  const unitOfTimeStart = moment.unix(timeUnix).utc().startOf(scaleType);
  if (roundUp) {
    return unitOfTimeStart
      .add(1, scaleType as moment.unitOfTime.DurationConstructor)
      .unix();
  }
  return unitOfTimeStart.unix();
};

const roundToNearestMultiple = (
  timeUnix: number,
  scale: mapEnums.Scale,
  roundUp = false,
): number => {
  const stepSeconds = scaleToUnitSeconds[scale];
  const roundFn = roundUp ? Math.ceil : Math.floor;
  return roundFn(timeUnix / stepSeconds!) * stepSeconds!;
};

/**
 * In this script starting and ending times of the time slider are rounded.
 * Here it is defined what is a time unit to a start of which a rounding is
 * done for each scale.
 * @returns an object where there are rounding units of each scale
 */

const scaleToRoundableUnit = {
  [mapEnums.Scale.Hour]: 'hour',
  [mapEnums.Scale.Day]: 'day',
  [mapEnums.Scale.Week]: 'isoWeek',
  [mapEnums.Scale.Month]: 'isoWeek',
  [mapEnums.Scale.Year]: 'year',
};

export const getRoundedStartAndEnd = (
  visibleTimeStart: number,
  visibleTimeEnd: number,
  scale: mapEnums.Scale,
): [number, number] => {
  if (
    scale === mapEnums.Scale.Minutes5 ||
    scale === mapEnums.Scale.Hours3 ||
    scale === mapEnums.Scale.Hours6
  ) {
    return [
      roundToNearestMultiple(visibleTimeStart, scale, false),
      roundToNearestMultiple(visibleTimeEnd, scale, true),
    ];
  }
  const momentUnit = scaleToRoundableUnit[
    scale
  ] as moment.unitOfTime.DurationConstructor;
  return [
    roundByUnitOfTime(visibleTimeStart, momentUnit, false),
    roundByUnitOfTime(visibleTimeEnd, momentUnit, true),
  ];
};

export const getCustomRoundedStartAndEnd = (
  visibleTimeStart: number,
  visibleTimeEnd: number,
  unit: string,
  nightTime = false,
): [number, number] => {
  return [
    roundByUnitOfTime(
      visibleTimeStart,
      unit as moment.unitOfTime.DurationConstructor,
      false,
      nightTime,
    ),
    roundByUnitOfTime(
      visibleTimeEnd,
      unit as moment.unitOfTime.DurationConstructor,
      true,
      nightTime,
    ),
  ];
};

const incrementTimestep = (
  timeAtTimeStep: moment.Moment,
  fundamentalScale: mapEnums.Scale,
): moment.Moment => {
  switch (fundamentalScale) {
    case mapEnums.Scale.Minutes5:
      return timeAtTimeStep.add(1, 'minute');
    case mapEnums.Scale.Hour:
      return timeAtTimeStep.add(5, 'minutes');
    case mapEnums.Scale.Hours3:
    case mapEnums.Scale.Hours6:
      return timeAtTimeStep.add(1, 'hour');
    case mapEnums.Scale.Day:
      return timeAtTimeStep.add(3, 'hours');
    case mapEnums.Scale.Week:
      return timeAtTimeStep.add(1, 'day');
    case mapEnums.Scale.Month:
      return timeAtTimeStep.add(1, 'week');
    case mapEnums.Scale.Year:
      return timeAtTimeStep.add(1, 'month');
    default:
      return timeAtTimeStep.add(1, 'month');
  }
};

const getTimesteps = (
  roundedStart: number,
  roundedEnd: number,
  fundamentalScaleType: mapEnums.Scale,
): Array<number> => {
  const timesteps: number[] = [];
  let timestep = roundedStart;
  while (timestep <= roundedEnd) {
    timesteps.push(timestep);
    timestep = incrementTimestep(
      moment.unix(timestep).utc(),
      fundamentalScaleType,
    ).unix();
  }
  return timesteps;
};

export const getCustomTimesteps = (
  roundedStart: number,
  roundedEnd: number,
  increment: string,
  nightTime = false,
): number[] => {
  const momentIncrement = increment as moment.unitOfTime.DurationConstructor;
  const timesteps: number[] = [];
  let timestep = roundedStart;
  if (momentIncrement === 'day' && nightTime) {
    while (timestep <= roundedEnd) {
      timesteps.push(timestep);
      timesteps.push(moment.unix(timestep).utc().hours(21).minutes(30).unix());
      timestep = moment.unix(timestep).utc().add(1, 'day').unix();
    }
  } else {
    while (timestep <= roundedEnd) {
      timesteps.push(timestep);
      timestep = moment.unix(timestep).utc().add(1, momentIncrement).unix();
    }
  }
  return timesteps;
};

const getLeftSideDateText = (
  timestamp: number,
  fundamentalScale: mapEnums.Scale,
): string => {
  const timeMoment = moment.unix(timestamp).utc();
  switch (fundamentalScale) {
    case mapEnums.Scale.Minutes5:
    case mapEnums.Scale.Hour:
    case mapEnums.Scale.Hours3:
    case mapEnums.Scale.Hours6:
      return timeMoment.format('Y MMM ddd DD');
    case mapEnums.Scale.Day:
    case mapEnums.Scale.Week:
      return timeMoment.format('Y MMM');
    case mapEnums.Scale.Month:
      return timeMoment.format('Y');
    case mapEnums.Scale.Year:
      return timeMoment.format('Y');
    default:
      return '';
  }
};

// For a data scale an appropriate fundamental scale is chosen
// depending on secondsPerPx of data. When a time range of
// data is for example many months. the fundamental scale mapEnums.Scale.Year
// is chosen here and the legend of the time slider is drawn in the
// same as if otiginally a year scale would have been chose.
//
// If a  time range of data is a few weeks, mapEnums.Scale.Month is chosen. So, all
// the time range of data can be shown in the window.

const getFundamentalScale = (secondsPerPx: number): mapEnums.Scale => {
  if (secondsPerPx >= mapUtils.scaleToSecondsPerPx[mapEnums.Scale.Month]) {
    return mapEnums.Scale.Year;
  }
  if (secondsPerPx >= mapUtils.scaleToSecondsPerPx[mapEnums.Scale.Week]) {
    return mapEnums.Scale.Month;
  }
  if (secondsPerPx >= mapUtils.scaleToSecondsPerPx[mapEnums.Scale.Day]) {
    return mapEnums.Scale.Week;
  }
  if (secondsPerPx >= mapUtils.scaleToSecondsPerPx[mapEnums.Scale.Hours6]) {
    return mapEnums.Scale.Day;
  }
  if (secondsPerPx >= mapUtils.scaleToSecondsPerPx[mapEnums.Scale.Hours3]) {
    return mapEnums.Scale.Hours6;
  }
  if (secondsPerPx >= mapUtils.scaleToSecondsPerPx[mapEnums.Scale.Hour]) {
    return mapEnums.Scale.Hours3;
  }
  if (secondsPerPx >= mapUtils.scaleToSecondsPerPx[mapEnums.Scale.Minutes5]) {
    return mapEnums.Scale.Hour;
  }
  return mapEnums.Scale.Minutes5;
};

export const drawTimeScale = (
  context: CanvasRenderingContext2D,
  theme: Theme,
  visibleTimeStart: number,
  visibleTimeEnd: number,
  canvasWidth: number,
  height: number,
  secondsPerPx: number,
): void => {
  const fundamentalScale = getFundamentalScale(secondsPerPx);

  const [roundedStart, roundedEnd] = getRoundedStartAndEnd(
    visibleTimeStart,
    visibleTimeEnd,
    fundamentalScale,
  );

  // draw regular tickmarks
  const ctx = context;
  getTimesteps(roundedStart, roundedEnd, fundamentalScale).forEach(
    (timestep) => {
      const stepPx = timestampToPixelEdges(
        timestep,
        visibleTimeStart,
        visibleTimeEnd,
        canvasWidth,
      );
      // Move 1 pixel line by 0.5 pixels to prevent it from being shown as blurry: https://usefulangle.com/post/17/html5-canvas-drawing-1px-crisp-straight-lines
      drawStepPxLine(
        ctx,
        theme,
        Math.floor(stepPx) + 0.5,
        height,
        fundamentalScale,
        timestep,
      );

      // Draw the time step time as text
      const timeFormat = getTimeFormat(fundamentalScale);
      const timeText = moment
        .unix(timestep)
        .utc()
        .format(timeFormat)
        .toString();
      if (
        ([
          mapEnums.Scale.Minutes5,
          mapEnums.Scale.Hour,
          mapEnums.Scale.Hours3,
          mapEnums.Scale.Hours6,
          mapEnums.Scale.Day,
        ].includes(fundamentalScale) &&
          timestep % scaleToUnitSeconds[fundamentalScale]! === 0) ||
        (fundamentalScale === mapEnums.Scale.Week &&
          moment.unix(timestep).isoWeekday() === 1)
      ) {
        drawTimeText(ctx, theme, stepPx, height, timeText, fundamentalScale);
      }

      if (
        fundamentalScale === mapEnums.Scale.Year &&
        moment.unix(timestep).month() === 0
      ) {
        drawYearScaleText(ctx, theme, timeText, stepPx, height);
      }
    },
  );

  // draw month overlay lines
  if (
    fundamentalScale === mapEnums.Scale.Week ||
    fundamentalScale === mapEnums.Scale.Month
  ) {
    const [roundedStart, roundedEnd] = getCustomRoundedStartAndEnd(
      visibleTimeStart,
      visibleTimeEnd,
      'month',
    );
    const monthTimesteps = getCustomTimesteps(
      roundedStart,
      roundedEnd,
      'month',
    );
    monthTimesteps.forEach((timestep) => {
      const stepPx = timestampToPixelEdges(
        timestep,
        visibleTimeStart,
        visibleTimeEnd,
        canvasWidth,
      );
      if (fundamentalScale === mapEnums.Scale.Month) {
        const momentTime = moment.unix(timestep).utc();
        // Move 1 pixel line by 0.5 pixels to prevent it from being shown as blurry: https://usefulangle.com/post/17/html5-canvas-drawing-1px-crisp-straight-lines
        drawDateChangeLine(ctx, theme, Math.floor(stepPx) + 0.5, height);
        drawTimeText(
          ctx,
          theme,
          stepPx,
          height,
          momentTime.format('MMM'),
          fundamentalScale,
        );
        return;
      }
      // adding 0.5 to make line not so blurred
      drawDashedMonthChangeLine(ctx, theme, Math.floor(stepPx) + 0.5, height);
    });
  }
  // draw date change texts
  if (fundamentalScale === mapEnums.Scale.Year) {
    return;
  }

  const dateText = getLeftSideDateText(visibleTimeStart, fundamentalScale);

  drawLeftSideDateText(ctx, theme, dateText, height);
};
