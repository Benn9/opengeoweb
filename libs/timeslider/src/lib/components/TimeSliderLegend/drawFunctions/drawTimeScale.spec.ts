/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */

import moment from 'moment';
import { mapEnums } from '@opengeoweb/store';

import {
  getCustomTimesteps,
  getRoundedStartAndEnd,
  roundByUnitOfTime,
} from './drawTimeScale';

describe('src/components/TimeSlider/TimeSliderLegendRenderFunctions', () => {
  describe('getRoundedStartAndEnd', () => {
    it('should round the times up/down to nearest 5 minutes', () => {
      const startTimeVisible = moment.utc('2021-05-05 12:27:27').unix();
      const endTimeVisible = moment.utc('2021-05-05 12:51:27').unix();
      const [roundedStart, roundedEnd] = getRoundedStartAndEnd(
        startTimeVisible,
        endTimeVisible,
        mapEnums.Scale.Minutes5,
      );
      expect(roundedStart).toEqual(moment.utc('2021-05-05 12:25:00').unix());
      expect(roundedEnd).toEqual(moment.utc('2021-05-05 12:55:00').unix());
    });

    it('should round the times up/down to nearest 3 hours', () => {
      const startTimeVisible = moment.utc('2021-05-05 13:27:27').unix();
      const endTimeVisible = moment.utc('2021-05-05 14:27:27').unix();
      const [roundedStart, roundedEnd] = getRoundedStartAndEnd(
        startTimeVisible,
        endTimeVisible,
        mapEnums.Scale.Hours3,
      );
      expect(roundedStart).toEqual(moment.utc('2021-05-05 12:00:00').unix());
      expect(roundedEnd).toEqual(moment.utc('2021-05-05 15:00:00').unix());
    });

    it('should round the times up/down to nearest 6 hours', () => {
      const startTimeVisible = moment.utc('2021-05-05 17:27:27').unix();
      const endTimeVisible = moment.utc('2021-05-05 20:27:27').unix();
      const [roundedStart, roundedEnd] = getRoundedStartAndEnd(
        startTimeVisible,
        endTimeVisible,
        mapEnums.Scale.Hours6,
      );
      expect(roundedStart).toEqual(moment.utc('2021-05-05 12:00:00').unix());
      expect(roundedEnd).toEqual(moment.utc('2021-05-06 00:00:00').unix());
    });

    it('should not round if the times are already correct multiples', () => {
      const startTimeVisible = moment.utc('2021-05-05 12:00:00').unix();
      const endTimeVisible = moment.utc('2021-05-05 15:00:00').unix();
      const [roundedStart, roundedEnd] = getRoundedStartAndEnd(
        startTimeVisible,
        endTimeVisible,
        mapEnums.Scale.Hours3,
      );
      expect(roundedStart).toEqual(moment.utc('2021-05-05 12:00:00').unix());
      expect(roundedEnd).toEqual(moment.utc('2021-05-05 15:00:00').unix());
    });

    it('should round the to up/down by one hour', () => {
      const startTimeVisible = moment.utc('2021-05-05 12:15:00').unix();
      const endTimeVisible = moment.utc('2021-05-05 12:45:00').unix();
      const [roundedStart, roundedEnd] = getRoundedStartAndEnd(
        startTimeVisible,
        endTimeVisible,
        mapEnums.Scale.Hour,
      );
      expect(roundedStart).toEqual(moment.utc('2021-05-05 12:00:00').unix());
      expect(roundedEnd).toEqual(moment.utc('2021-05-05 13:00:00').unix());
    });

    it('should round up/down by one day', () => {
      const startTimeVisible = moment.utc('2021-05-05 12:00:00').unix();
      const endTimeVisible = moment.utc('2021-05-05 13:00:00').unix();
      const [roundedStart, roundedEnd] = getRoundedStartAndEnd(
        startTimeVisible,
        endTimeVisible,
        mapEnums.Scale.Day,
      );
      expect(roundedStart).toEqual(moment.utc('2021-05-05 00:00:00').unix());
      expect(roundedEnd).toEqual(moment.utc('2021-05-06 00:00:00').unix());
    });

    it('should round up/down by one week', () => {
      const startTimeVisible = moment.utc('2021-05-05 12:00:00').unix();
      const endTimeVisible = moment.utc('2021-05-08 12:00:00').unix();
      const [roundedStart, roundedEnd] = getRoundedStartAndEnd(
        startTimeVisible,
        endTimeVisible,
        mapEnums.Scale.Week,
      );
      expect(roundedStart).toEqual(moment.utc('2021-05-03 00:00:00').unix());
      expect(roundedEnd).toEqual(moment.utc('2021-05-10 00:00:00').unix());
    });

    it('should round up/down by one year', () => {
      const startTimeVisible = moment.utc('2021-05-05 12:00:00').unix();
      const endTimeVisible = moment.utc('2021-10-05 12:00:00').unix();
      const [roundedStart, roundedEnd] = getRoundedStartAndEnd(
        startTimeVisible,
        endTimeVisible,
        mapEnums.Scale.Year,
      );
      expect(roundedStart).toEqual(moment.utc('2021-01-01 00:00:00').unix());
      expect(roundedEnd).toEqual(moment.utc('2022-01-01 00:00:00').unix());
    });
  });
  describe('roundByUnitOfTime', () => {
    it('should round week down', () => {
      const momentTime = moment.utc('2022-11-15T12:00:00Z');
      const rounded = roundByUnitOfTime(momentTime.unix(), 'isoWeek', false);

      expect(rounded).toEqual(momentTime.clone().startOf('isoWeek').unix());
    });
    it('should round week up', () => {
      const momentTime = moment.utc('2022-11-15T12:00:00Z');
      const rounded = roundByUnitOfTime(momentTime.unix(), 'isoWeek', true);

      expect(rounded).toEqual(
        momentTime.clone().startOf('isoWeek').add(1, 'week').unix(),
      );
    });
    it('should round day down to morning time one day before', () => {
      const momentTime = moment.utc('2022-11-15T12:00:00Z');
      const rounded = roundByUnitOfTime(momentTime.unix(), 'day', false, true);

      expect(rounded).toEqual(
        momentTime.clone().subtract(1, 'day').hours(6).minutes(30).unix(),
      );
    });
    it('should round day up to night time one day after', () => {
      const momentTime = moment.utc('2022-11-15T12:00:00Z');
      const rounded = roundByUnitOfTime(momentTime.unix(), 'day', true, true);

      expect(rounded).toEqual(
        momentTime.clone().add(1, 'day').hours(21).minutes(30).unix(),
      );
    });
  });
  describe('getCustomTimesteps', () => {
    it('should work for week', () => {
      const momentTime = moment.utc('2022-11-15T12:00:00Z');
      const timesteps = getCustomTimesteps(
        momentTime.unix(),
        momentTime.clone().add(2, 'week').unix(),
        'week',
      );

      expect(timesteps).toEqual([
        momentTime.unix(),
        momentTime.clone().add(1, 'week').unix(),
        momentTime.clone().add(2, 'week').unix(),
      ]);
    });
    it('should work for day with nighttime', () => {
      const momentTime = moment.utc('2022-11-15T06:30:00Z');
      const timesteps = getCustomTimesteps(
        momentTime.unix(),
        momentTime.clone().add(1, 'day').hours(21).minutes(30).unix(),
        'day',
        true,
      );

      expect(timesteps).toEqual([
        momentTime.unix(),
        momentTime.clone().hours(21).minutes(30).unix(),
        momentTime.clone().add(1, 'day').unix(),
        momentTime.clone().add(1, 'day').hours(21).minutes(30).unix(),
      ]);
    });
  });
});
