/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */

import moment from 'moment';
import { mapEnums } from '@opengeoweb/store';
import { getColorChangeTimesteps } from './drawBackground';

describe('src/components/TimeSlider/drawFunctions/drawBackground', () => {
  describe('getColorChangeTimesteps', () => {
    it('should work for week', () => {
      const momentTime = moment.utc('2022-11-15T12:00:00Z');
      const colorChangeTimesteps = getColorChangeTimesteps(
        mapEnums.Scale.Week,
        momentTime.clone().unix(),
        momentTime.clone().add(1, 'week').unix(),
      );
      const startWeek = momentTime.clone().startOf('isoWeek');
      expect(colorChangeTimesteps).toEqual([
        startWeek.unix(),
        startWeek.clone().add(1, 'week').unix(),
        startWeek.clone().add(2, 'week').unix(),
      ]);
    });
    it('should work for one hour', () => {
      const momentTime = moment.utc('2022-11-15T12:00:00Z');
      const colorChangeTimesteps = getColorChangeTimesteps(
        mapEnums.Scale.Hour,
        momentTime.clone().unix(),
        momentTime.clone().unix(),
      );
      const morning = momentTime.clone().hour(6).minute(30);
      const evening = momentTime.clone().hour(21).minute(30);
      expect(colorChangeTimesteps).toEqual([
        morning.clone().subtract(1, 'day').unix(),
        evening.clone().subtract(1, 'day').unix(),
        morning.unix(),
        evening.unix(),
        morning.clone().add(1, 'day').unix(),
        evening.clone().add(1, 'day').unix(),
      ]);
    });
    it('should work for hours', () => {
      const momentTime = moment.utc('2022-11-15T12:00:00Z');
      const day = 'day';
      const colorChangeTimesteps = getColorChangeTimesteps(
        mapEnums.Scale.Hour,
        momentTime.clone().unix(),
        momentTime.clone().add(1, day).unix(),
      );
      const evening = momentTime.clone().hour(21).minute(30);
      const morning = momentTime.clone().hour(6).minute(30);
      expect(colorChangeTimesteps).toEqual([
        morning.clone().subtract(1, 'day').unix(),
        evening.clone().subtract(1, 'day').unix(),
        morning.unix(),
        evening.unix(),
        morning.clone().add(1, 'day').unix(),
        evening.clone().add(1, 'day').unix(),
        morning.clone().add(2, 'day').unix(),
        evening.clone().add(2, 'day').unix(),
      ]);
    });
  });
});
