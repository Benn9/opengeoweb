/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */

import React from 'react';
import { fireEvent, render, screen } from '@testing-library/react';
import { createMockStoreWithEggs } from '@opengeoweb/shared';
import { produce } from 'immer';
import {
  mapActions,
  mapConstants,
  mapEnums,
  mapTypes,
  storeTestUtils,
} from '@opengeoweb/store';
import { webmapTestSettings } from '@opengeoweb/webmap';
import { TimeSliderClockConnect } from './TimeSliderClockConnect';
import { ThemeStoreProvider } from '../../testUtils/Providers';

describe('components/TimeSlider/TimeSliderClock/TimeSliderClockConnect', () => {
  const mapId = 'mapid_1';
  it('renders correctly', () => {
    const mockState = {
      webmap: {
        allIds: [],
        byId: {
          [mapId]: {
            dimensions: [
              {
                name: 'time',
                units: 'ISO8601',
                currentValue: '2020-03-13T13:30:00Z',
              },
            ],
            isTimeSliderVisible: true,
          } as mapTypes.WebMap,
        },
      } as mapTypes.WebMapState,
    };
    const store = createMockStoreWithEggs(mockState);
    render(
      <ThemeStoreProvider store={store}>
        <TimeSliderClockConnect mapId={mapId} />
      </ThemeStoreProvider>,
    );
    screen.getByText('Fri 13 Mar 13:30 UTC');

    expect(
      screen.queryByRole('button', { name: /animation options/i }),
    ).not.toBeInTheDocument();
  });

  it('handles button clicks', () => {
    const mockState = produce(
      storeTestUtils.mockStateMapWithLayer(
        webmapTestSettings.defaultReduxLayerRadarKNMI,
        mapId,
      ),
      (draft) => {
        draft.webmap!.byId[mapId].isTimeSliderVisible = false;
      },
    );
    const store = createMockStoreWithEggs(mockState);
    render(
      <ThemeStoreProvider store={store}>
        <TimeSliderClockConnect mapId={mapId} />
      </ThemeStoreProvider>,
    );

    fireEvent.click(screen.getByRole('button', { name: /animation options/i }));

    fireEvent.click(screen.getByTestId('play-svg-path'));
    expect(store.getActions()).toContainEqual(
      mapActions.mapStartAnimation({
        mapId,
        end: expect.any(String),
        interval: mapConstants.defaultTimeStep,
        origin: mapEnums.MapActionOrigin.map,
        start: expect.any(String),
      }),
    );
  });
});
