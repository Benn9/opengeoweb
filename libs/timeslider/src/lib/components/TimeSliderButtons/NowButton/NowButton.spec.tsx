/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import {
  render,
  screen,
  waitForElementToBeRemoved,
} from '@testing-library/react';
import userEvent from '@testing-library/user-event';

import { NowButton, NOW_BUTTON_TITLE } from './NowButton';
import { ThemeProvider } from '../../../testUtils/Providers';

describe('src/components/TimeSlider/NowButton', () => {
  const user = userEvent.setup();

  it('should show and hide tooltip', async () => {
    const props = {
      onSetNow: jest.fn(),
      disabled: false,
    };

    render(
      <ThemeProvider>
        <NowButton {...props} />
      </ThemeProvider>,
    );
    const button = screen.getByRole('button', { name: 'now' });
    expect(button).toBeTruthy();
    expect(screen.queryByRole('tooltip')).toBeFalsy();
    expect(screen.queryByText(NOW_BUTTON_TITLE)).toBeFalsy();

    await user.hover(button);
    expect(await screen.findByRole('tooltip')).toBeTruthy();
    expect(screen.getByText(NOW_BUTTON_TITLE)).toBeTruthy();

    await user.unhover(button);
    await waitForElementToBeRemoved(() => screen.queryByRole('tooltip'), {
      timeout: 3000,
    });
    expect(screen.queryByText(NOW_BUTTON_TITLE)).toBeFalsy();
  });
});
