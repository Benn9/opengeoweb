/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */

import { useDispatch, useSelector } from 'react-redux';
import React from 'react';

import {
  CoreAppStore,
  genericActions,
  mapActions,
  mapEnums,
  mapSelectors,
} from '@opengeoweb/store';
import { handleMomentISOString } from '@opengeoweb/webmap';
import { BackwardForwardStepButton } from './BackwardForwardStepButton';
import {
  setPreviousTimeStep,
  setNextTimeStep,
} from '../../TimeSlider/changeTimeFunctions';
import { useTurnOffAutoUpdateIfItsOn } from '../../TimeSlider/timeSliderUtils';

export interface BackwardForwardStepButtonConnectProps {
  mapId: string;
  isForwardStep?: boolean;
}

export const BackwardForwardStepButtonConnect = ({
  isForwardStep,
  mapId,
}: BackwardForwardStepButtonConnectProps): JSX.Element => {
  const timeStep = useSelector((store: CoreAppStore) =>
    mapSelectors.getMapTimeStep(store, mapId),
  );

  const dispatch = useDispatch();

  const setTime = (newDate: string): void => {
    dispatch(
      genericActions.setTime({
        origin: '',
        sourceId: mapId,
        value: handleMomentISOString(newDate),
      }),
    );
  };

  const stopAnimation = (): void => {
    dispatch(
      mapActions.mapStopAnimation({
        mapId,
        origin: mapEnums.MapActionOrigin.map,
      }),
    );
  };

  const isAnimating = useSelector((store: CoreAppStore) =>
    mapSelectors.isAnimating(store, mapId),
  );

  const selectedTime = useSelector((store: CoreAppStore) =>
    mapSelectors.getSelectedTime(store, mapId),
  );

  const [dataStartTime, dataEndTime] = useSelector((store: CoreAppStore) =>
    mapSelectors.getDataLimitsFromLayers(store, mapId),
  );
  const turnOffAutoUpdateIfItsOn = useTurnOffAutoUpdateIfItsOn(mapId);
  const onClickBF = (): void => {
    turnOffAutoUpdateIfItsOn();
    if (isAnimating) {
      stopAnimation();
    }
    isForwardStep
      ? setNextTimeStep(timeStep!, selectedTime, dataEndTime, setTime)
      : setPreviousTimeStep(timeStep!, selectedTime, dataStartTime, setTime);
  };
  return (
    <BackwardForwardStepButton
      isDisabled={false}
      isForwardStep={isForwardStep}
      onClickBFButton={(): void => {
        onClickBF();
      }}
    />
  );
};
