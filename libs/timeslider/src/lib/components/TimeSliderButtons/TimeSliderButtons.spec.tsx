/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { render, screen } from '@testing-library/react';

import { TimeSliderButtons } from './TimeSliderButtons';
import { ThemeProvider } from '../../testUtils/Providers';

describe('src/components/TimeSlider/TimeSliderButtons', () => {
  it('render correct buttons depending on mediaquery', () => {
    const props = {};

    // Matches to false, so smaller buttonlayout is rendered
    window.matchMedia = jest.fn().mockImplementation(() => {
      return {
        matches: false,
        addListener: jest.fn(),
        removeListener: jest.fn(),
      };
    });
    render(
      <ThemeProvider>
        <TimeSliderButtons {...props} />
      </ThemeProvider>,
    );

    expect(screen.getAllByRole('button').length === 4).toBeTruthy();
    expect(screen.getByTestId('optionsMenuButton')).toBeTruthy();
    expect(screen.getByTestId('playButton')).toBeTruthy();
    expect(screen.getByTestId('step-forward-svg-path')).toBeTruthy();
    expect(screen.getByTestId('step-backward-svg-path')).toBeTruthy();
  });
});
