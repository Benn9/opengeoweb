/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { Menu, MenuItem, Box, Typography } from '@mui/material';
import { CustomIconButton } from '@opengeoweb/shared';
import { useState } from 'react';
import { throttle } from 'lodash';
import { minutesToDescribedDuration } from '../../TimeSlider/timeSliderUtils';

const THROTTLE_WAIT_TIME = 300; // [ms]
const THROTTLE_OPTIONS = {
  trailing: false, // Invoke function when called, but not more than once every <THROTTLE_WAIT_TIME>.
};
export interface Mark {
  label?: string;
  value: number;
  text: string;
}

export interface TimeSliderMenuProps {
  marks: Mark[];
  value: number;
  handleMenuItemClick: (mark: Mark) => void;
  title: string;
  icon: JSX.Element;
  isOpenByDefault?: boolean;
  allOptionsDisabled?: boolean;
  isAutoDisabled?: boolean;
  handleAutoClick?: () => void;
  isAutoSelected?: boolean;
  onChangeMouseWheel?: (element: Mark) => void;
  displayVariableDuration?: boolean;
}

const buttonText = (
  value: number,
  displayVariableDuration: boolean | undefined,
  currentMark: Mark,
): string => {
  if (displayVariableDuration) {
    return minutesToDescribedDuration(value);
  }
  if (currentMark) {
    return currentMark.text;
  }
  return 'Auto';
};

export const TimeSliderMenu = ({
  marks,
  value,
  isOpenByDefault,
  handleMenuItemClick,
  allOptionsDisabled,
  isAutoDisabled,
  handleAutoClick,
  isAutoSelected,
  title,
  icon,
  onChangeMouseWheel = (): void => {},
  displayVariableDuration,
}: TimeSliderMenuProps): JSX.Element => {
  const currentMarkIndex = marks.findIndex((mark) => mark.value === value);
  const currentMark = marks[currentMarkIndex];

  const [anchorRef, setAnchorRef] = React.useState<null | HTMLElement>(null);
  const [open, setOpen] = useState(false);

  // eslint-disable-next-line react-hooks/exhaustive-deps
  const throttledScroll = React.useCallback(
    throttle(
      (newValue) => {
        onChangeMouseWheel(newValue);
      },
      THROTTLE_WAIT_TIME,
      THROTTLE_OPTIONS,
    ),
    [],
  );

  React.useEffect(() => {
    if (anchorRef && isOpenByDefault) {
      setOpen(true);
    }
  }, [anchorRef, isOpenByDefault]);

  const onWheel = React.useCallback(
    (event: React.WheelEvent): void => {
      const direction = event.deltaY < 0 ? 1 : -1;
      const newValue =
        marks[
          Math.min(Math.max(currentMarkIndex - direction, 0), marks.length - 1)
        ];
      throttledScroll(newValue);
    },
    [currentMarkIndex, marks, throttledScroll],
  );

  return (
    <Box
      onKeyDown={(event): void => {
        event.stopPropagation();
      }}
    >
      <CustomIconButton
        variant="tool"
        tooltipTitle={title}
        onClick={(): void => {
          setOpen(true);
        }}
        ref={setAnchorRef}
        isSelected
        onWheel={onWheel}
        sx={{ padding: '11px!important', width: '100%!important' }}
      >
        {icon}
        <Typography fontSize={12} fontWeight={400} textTransform="none">
          {buttonText(value, displayVariableDuration, currentMark)}
        </Typography>
      </CustomIconButton>
      <Menu
        anchorEl={anchorRef}
        open={open}
        onClose={(): void => setOpen(false)}
        BackdropProps={{ sx: { backgroundColor: 'transparent' } }}
        anchorOrigin={{
          vertical: 'top',
          horizontal: 'center',
        }}
        transformOrigin={{
          vertical: 'bottom',
          horizontal: 'center',
        }}
      >
        <MenuItem
          disabled
          sx={{
            fontSize: '12px',
            opacity: 0.67,
            padding: '0px 12px',
            '&.MuiMenuItem-root': {
              minHeight: '30px!important',
            },
          }}
        >
          {title}
        </MenuItem>

        {marks.map((mark) => {
          return (
            <MenuItem
              onClick={(): void => {
                handleMenuItemClick(mark);
                setOpen(false);
              }}
              key={mark.value}
              selected={currentMark?.value === mark.value && !isAutoSelected}
              disabled={allOptionsDisabled}
            >
              {mark.label || mark.text}
            </MenuItem>
          );
        })}
        {handleAutoClick && (
          <MenuItem
            onClick={(): void => {
              handleAutoClick();
              setOpen(false);
            }}
            selected={isAutoSelected}
            disabled={allOptionsDisabled || isAutoDisabled}
          >
            Auto
          </MenuItem>
        )}
        {displayVariableDuration && (
          <MenuItem
            disabled
            sx={{
              fontSize: '12px',
              opacity: 0.67,
              padding: '0px 12px',
              '&.MuiMenuItem-root': {
                minHeight: '30px!important',
              },
            }}
          >
            length
          </MenuItem>
        )}
        {displayVariableDuration && (
          <CustomIconButton
            variant="tool"
            data-testid="menu-animation-length-button"
            sx={{ padding: '11px!important', width: '100%!important' }}
          >
            <Typography
              fontSize={14}
              fontWeight={500}
              textTransform="none"
              lineHeight={2}
            >
              {minutesToDescribedDuration(value)}
            </Typography>
          </CustomIconButton>
        )}
      </Menu>
    </Box>
  );
};
