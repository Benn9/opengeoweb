/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import {
  fireEvent,
  render,
  screen,
  waitForElementToBeRemoved,
} from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import { range } from 'lodash';

import { Mark, TimeSliderMenu, TimeSliderMenuProps } from './TimeSliderMenu';
import { ThemeProvider } from '../../../testUtils/Providers';

describe('src/components/TimeSlider/TimeSliderButtons/TimeSliderMenu', () => {
  const marks: Mark[] = range(1, 10).map((value) => ({
    text: `t${value}`,
    value,
    label: `label${value}`,
  }));
  const title = 'Test Menu';
  const props: TimeSliderMenuProps = {
    handleMenuItemClick: jest.fn(),
    icon: null!,
    marks,
    title,
    value: 4,
    isAutoSelected: true,
    isAutoDisabled: false,
    handleAutoClick: jest.fn(),
    onChangeMouseWheel: jest.fn(),
  };
  const user = userEvent.setup();

  it('should work correctly', async () => {
    render(
      <ThemeProvider>
        <TimeSliderMenu {...props} />
      </ThemeProvider>,
    );

    // Should render button with text
    const button = screen.getByRole('button', { name: title });
    expect(button).toHaveTextContent('t4');

    // Should show and hide tooltip
    expect(screen.queryByRole('tooltip')).not.toBeInTheDocument();
    expect(screen.queryByText(title)).not.toBeInTheDocument();

    await user.hover(button);
    expect(await screen.findByRole('tooltip')).toBeInTheDocument();
    expect(screen.getByText(title)).toBeInTheDocument();

    await user.unhover(button);
    await waitForElementToBeRemoved(() => screen.queryByRole('tooltip'));
    expect(screen.queryByText(title)).not.toBeInTheDocument();

    // Should toggle a menu
    expect(screen.queryByRole('menu')).not.toBeInTheDocument();

    await user.click(button);
    expect(screen.getByRole('menu')).toBeInTheDocument();

    await user.keyboard('{Escape}');
    expect(screen.queryByRole('menu')).not.toBeInTheDocument();

    // Should render menu with marks and correct mark selected
    await user.click(button);
    expect(screen.getAllByRole('menuitem')).toHaveLength(11);

    expect(screen.getByRole('menuitem', { name: /auto/i })).toHaveClass(
      'Mui-selected',
    );

    // Menu items should be enabled
    expect(
      screen.getByRole('menuitem', { name: /label2/i }),
    ).not.toHaveAttribute('aria-disabled');

    // handleMenuItemClick is called correctly
    await user.click(screen.getByRole('menuitem', { name: /label6/i }));
    expect(props.handleMenuItemClick).toHaveBeenCalledTimes(1);
    expect(props.handleMenuItemClick).toHaveBeenCalledWith({
      label: 'label6',
      text: 't6',
      value: 6,
    });

    await user.click(button);

    await user.click(screen.getByRole('menuitem', { name: /label7/i }));
    expect(props.handleMenuItemClick).toHaveBeenCalledTimes(2);
    expect(props.handleMenuItemClick).toHaveBeenLastCalledWith({
      label: 'label7',
      text: 't7',
      value: 7,
    });

    // handleAutoClick is called correctly and auto is selected
    await user.click(button);

    expect(screen.getByRole('menuitem', { name: /auto/i })).toHaveClass(
      'Mui-selected',
    );

    await user.click(screen.getByRole('menuitem', { name: /auto/i }));
    expect(props.handleAutoClick).toHaveBeenCalledTimes(1);
  });

  it('should render a menu as disabled and no auto option if passed in as props', async () => {
    const props2: TimeSliderMenuProps = {
      ...props,
      handleAutoClick: undefined,
      allOptionsDisabled: true,
    };
    render(
      <ThemeProvider>
        <TimeSliderMenu {...props2} />
      </ThemeProvider>,
    );

    const button = screen.getByRole('button', { name: title });

    // No auto button
    expect(
      screen.queryByRole('menuitem', { name: /auto/i }),
    ).not.toBeInTheDocument();

    // Menu items are disabled
    await user.click(button);
    expect(screen.getByRole('menu')).toBeInTheDocument();
    expect(screen.getByRole('menuitem', { name: /label7/i })).toHaveAttribute(
      'aria-disabled',
    );
  });

  it('should render a menu with auto disabled', async () => {
    const props2: TimeSliderMenuProps = {
      ...props,
      isAutoDisabled: true,
    };
    render(
      <ThemeProvider>
        <TimeSliderMenu {...props2} />
      </ThemeProvider>,
    );

    const button = screen.getByRole('button', { name: title });

    // Auto is disabled when there is no timestep from the layer
    await user.click(button);
    expect(screen.getByRole('menu')).toBeInTheDocument();
    expect(screen.getByRole('menuitem', { name: /auto/i })).toBeInTheDocument();
    expect(screen.getByRole('menuitem', { name: /auto/i })).toHaveAttribute(
      'aria-disabled',
    );
  });

  it('should call onChangeMouseWheel on wheel scroll', async () => {
    jest.useFakeTimers();

    render(
      <ThemeProvider>
        <TimeSliderMenu {...props} />
      </ThemeProvider>,
    );

    const button = screen.getByRole('button', { name: title });

    fireEvent.wheel(button, { deltaY: 1 });
    expect(props.onChangeMouseWheel).toHaveBeenCalledTimes(1);
    expect(props.onChangeMouseWheel).toHaveBeenCalledWith({
      label: 'label5',
      text: 't5',
      value: 5,
    });

    // wait for throttle time
    jest.runOnlyPendingTimers();

    fireEvent.wheel(button, { deltaY: -1 });
    expect(props.onChangeMouseWheel).toHaveBeenCalledTimes(2);
    expect(props.onChangeMouseWheel).toHaveBeenCalledWith({
      label: 'label3',
      text: 't3',
      value: 3,
    });

    jest.useRealTimers();
  });

  it('should render a button at the bottom of the menu', async () => {
    const props2: TimeSliderMenuProps = {
      ...props,
      displayVariableDuration: true,
    };
    render(
      <ThemeProvider>
        <TimeSliderMenu {...props2} />
      </ThemeProvider>,
    );

    const button = screen.getByRole('button', { name: title });
    await user.click(button);
    expect(
      screen.getByRole('menuitem', { name: /length/i }),
    ).toBeInTheDocument();
    const testButton = screen.getByTestId('menu-animation-length-button');
    expect(testButton).toBeInTheDocument();
  });
});
