/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */

import { fireEvent, render, screen } from '@testing-library/react';
import React from 'react';

import { HideSliderButton } from './HideSliderButton';
import { ThemeProvider } from '../../../testUtils/Providers';

describe('src/components/TimeSlider/HideSliderButton', () => {
  it('should call the toggle function when clicked', () => {
    const props = {
      open: true,
      toggleTimeSliderIsVisible: jest.fn(),
    };
    render(
      <ThemeProvider>
        <HideSliderButton {...props} />
      </ThemeProvider>,
    );
    fireEvent.click(screen.queryByTestId('hideTimeSliderButton')!);
    expect(props.toggleTimeSliderIsVisible).toHaveBeenCalledTimes(1);
  });
});
