/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */

import React, { FC } from 'react';
import { AutoUpdateButtonConnect } from '../AutoUpdateButton/AutoUpdateButtonConnect';
import { OptionsMenu } from './OptionsMenu';
import { SpeedButtonConnect } from '../SpeedButton/SpeedButtonConnect';
import { TimeStepButtonConnect } from '../TimeStepButton/TimeStepButtonConnect';
import { NowButtonConnect } from '../NowButton/NowButtonConnect';
import { AnimationLengthButtonConnect } from '../AnimationLengthButton/AnimationLengthButtonConnect';
import { TimeSpanButtonConnect } from '../TimeSpanButton/TimeSpanButtonConnect';

export const OptionsMenuConnect: FC<{
  sourceId: string;
  mapId: string;
}> = ({ sourceId, mapId }) => (
  <OptionsMenu
    nowBtn={<NowButtonConnect mapId={mapId} sourceId={sourceId} />}
    animationLengthBtn={<AnimationLengthButtonConnect mapId={mapId} />}
    autoUpdateBtn={<AutoUpdateButtonConnect mapId={mapId} />}
    speedBtn={<SpeedButtonConnect mapId={mapId} />}
    timeStepBtn={<TimeStepButtonConnect mapId={mapId} />}
    timeSpanBtn={<TimeSpanButtonConnect mapId={mapId} />}
  />
);
