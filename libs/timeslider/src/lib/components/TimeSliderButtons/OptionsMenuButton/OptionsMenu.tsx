/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */

import React, { FC } from 'react';
import { Grid } from '@mui/material';
import { mapConstants } from '@opengeoweb/store';
import { AutoUpdateButton } from '../AutoUpdateButton/AutoUpdateButton';
import { SpeedButton } from '../SpeedButton/SpeedButton';
import { TimeStepButton } from '../TimeStepButton/TimeStepButton';
import { TimeSpanButton } from '../TimeSpanButton/TimeSpanButton';
import { NowButton } from '../NowButton/NowButton';
import { AnimationLengthButton } from '../AnimationLengthButton/AnimationLengthButton';

export const OptionsMenu: FC<{
  nowBtn?: React.ReactChild;
  timeSpanBtn?: React.ReactChild;
  timeStepBtn?: React.ReactChild;
  speedBtn?: React.ReactChild;
  animationLengthBtn?: React.ReactChild;
  autoUpdateBtn?: React.ReactChild;
}> = ({
  autoUpdateBtn,
  nowBtn,
  speedBtn,
  timeSpanBtn,
  timeStepBtn,
  animationLengthBtn,
}) => {
  return (
    <>
      <Grid item xs="auto">
        {nowBtn || <NowButton />}
      </Grid>
      <Grid item xs="auto">
        {timeSpanBtn || (
          <TimeSpanButton
            centerTime={0}
            onChangeTimeSliderSpan={(): void => {}}
            secondsPerPx={0}
            selectedTime={0}
            timeSliderSpan={0}
            timeSliderWidth={0}
            isTimeSpanAuto={false}
          />
        )}
      </Grid>
      <Grid item xs="auto">
        {animationLengthBtn || (
          <AnimationLengthButton onChangeAnimationLength={(): void => {}} />
        )}
      </Grid>
      <Grid item xs="auto">
        {timeStepBtn || (
          <TimeStepButton
            onChangeTimeStep={(): void => {}}
            isTimestepAuto={false}
          />
        )}
      </Grid>
      <Grid item xs="auto">
        {speedBtn || (
          <SpeedButton
            animationDelay={mapConstants.defaultAnimationDelayAtStart}
            setMapAnimationDelay={(): void => {}}
          />
        )}
      </Grid>
      <Grid item xs="auto">
        {autoUpdateBtn || <AutoUpdateButton />}
      </Grid>
    </>
  );
};
