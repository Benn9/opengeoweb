/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';

import { AutoUpdateButtonConnect } from '../AutoUpdateButton/AutoUpdateButtonConnect';
import { OptionsMenuButton } from './OptionsMenuButton';
import { SpeedButtonConnect } from '../SpeedButton/SpeedButtonConnect';
import { TimeStepButtonConnect } from '../TimeStepButton/TimeStepButtonConnect';
import { NowButtonConnect } from '../NowButton/NowButtonConnect';
import { AnimationLengthButtonConnect } from '../AnimationLengthButton/AnimationLengthButtonConnect';
import { TimeSpanButtonConnect } from '../TimeSpanButton/TimeSpanButtonConnect';

interface OptionsMenuButtonConnectProps {
  sourceId: string;
  mapId: string;
}

/**
 * OptionsMenuButtonConnect component with components connected to the store displaying the options menu button
 *
 * Expects the following props:
 * @param {string} mapId mapId: string - Id of the map
 * @example
 * ``` <OptionsMenuButtonConnect mapId={mapId} />```
 */

export const OptionsMenuButtonConnect: React.FC<
  OptionsMenuButtonConnectProps
> = ({ sourceId, mapId }: OptionsMenuButtonConnectProps) => {
  return (
    <OptionsMenuButton
      nowBtn={<NowButtonConnect mapId={mapId} sourceId={sourceId} />}
      animationLengthBtn={<AnimationLengthButtonConnect mapId={mapId} />}
      autoUpdateBtn={<AutoUpdateButtonConnect mapId={mapId} />}
      speedBtn={<SpeedButtonConnect mapId={mapId} />}
      timeStepBtn={<TimeStepButtonConnect mapId={mapId} />}
      timeSpanBtn={<TimeSpanButtonConnect mapId={mapId} />}
    />
  );
};
