/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import { connect, ConnectedProps } from 'react-redux';
import React from 'react';

import {
  CoreAppStore,
  mapActions,
  mapConstants,
  mapEnums,
  mapSelectors,
} from '@opengeoweb/store';
import { PlayButton } from './PlayButton';

export interface PlayButtonConnectProps {
  mapId: string;
  isAnimating?: boolean;
  animationStartTime?: string;
  animationEndTime?: string;
  isDisabled?: boolean;
  mapStartAnimation?: typeof mapActions.mapStartAnimation;
  mapStopAnimation?: typeof mapActions.mapStopAnimation;
  timeStep?: number;
  linkedMapAnimationInfo?: { isAnimating: boolean; id: string };
}

const connectRedux = connect(
  (store: CoreAppStore, props: PlayButtonConnectProps) => ({
    animationStartTime: mapSelectors.getAnimationStartTime(store, props.mapId),
    animationEndTime: mapSelectors.getAnimationEndTime(store, props.mapId),
    timeStep: mapSelectors.getMapTimeStep(store, props.mapId),
    linkedMapAnimationInfo: mapSelectors.linkedMapAnimationInfo(
      store,
      props.mapId,
    ),
  }),
  {
    mapStartAnimation: mapActions.mapStartAnimation,
    mapStopAnimation: mapActions.mapStopAnimation,
  },
);

type Props = PlayButtonConnectProps & ConnectedProps<typeof connectRedux>;

export const PlayButtonConnect = connectRedux(
  ({
    mapId,
    animationStartTime,
    animationEndTime,
    isDisabled,
    timeStep,
    linkedMapAnimationInfo,
    mapStartAnimation,
    mapStopAnimation,
  }: Props) => {
    const animationInterval = timeStep || mapConstants.defaultTimeStep;

    const onTogglePlay = (): void => {
      if (linkedMapAnimationInfo.isAnimating) {
        mapStopAnimation({
          mapId: linkedMapAnimationInfo.id,
          origin: mapEnums.MapActionOrigin.map,
        });
      } else {
        mapStartAnimation({
          mapId,
          start: animationStartTime,
          end: animationEndTime,
          interval: animationInterval,
          origin: mapEnums.MapActionOrigin.map,
        });
      }
    };

    return (
      <PlayButton
        isAnimating={linkedMapAnimationInfo.isAnimating}
        isDisabled={isDisabled}
        onTogglePlayButton={(): void => {
          onTogglePlay();
        }}
      />
    );
  },
);
