/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import {
  fireEvent,
  render,
  screen,
  waitForElementToBeRemoved,
} from '@testing-library/react';
import moment from 'moment';
import userEvent from '@testing-library/user-event';

import { webmapTestSettings } from '@opengeoweb/webmap';
import { TimeSpanButton } from './TimeSpanButton';
import { ThemeProvider } from '../../../testUtils/Providers';

describe('src/components/TimeSlider/TimeSliderButtons/TimeSpanButton', () => {
  const props = {
    timeSliderSpan: 2 * 24 * 3600,
    timeSliderWidth: 100,
    centerTime: moment.utc('2021-01-01 12:00:00').unix(),
    secondsPerPx: 60,
    selectedTime: moment.utc('2021-01-01 12:15:00').unix(),
    onChangeTimeSliderSpan: jest.fn(),
    layers: [webmapTestSettings.multiDimensionLayer3],
    isTimeSpanAuto: false,
  };
  const user = userEvent.setup();

  it('should at start in tests render the component with a span text', () => {
    render(
      <ThemeProvider>
        <TimeSpanButton {...props} />
      </ThemeProvider>,
    );

    expect(
      screen.getByRole('button', { name: /time span/i }),
    ).toHaveTextContent('48h');
  });

  it('should show and hide tooltip', async () => {
    render(
      <ThemeProvider>
        <TimeSpanButton {...props} />
      </ThemeProvider>,
    );

    const button = screen.getByRole('button', { name: /time span/i });
    expect(button).toBeInTheDocument();

    expect(screen.queryByRole('tooltip')).not.toBeInTheDocument();
    expect(screen.queryByText(/Time span/)).not.toBeInTheDocument();

    await user.hover(button);
    expect(await screen.findByRole('tooltip')).toBeInTheDocument();
    expect(screen.getByText(/Time span/)).toBeInTheDocument();

    await user.unhover(button);
    await waitForElementToBeRemoved(() => screen.queryByRole('tooltip'));
    expect(screen.queryByText(/Time span/)).not.toBeInTheDocument();
  });

  it('should toggle a menu on click', async () => {
    render(
      <ThemeProvider>
        <TimeSpanButton {...props} />
      </ThemeProvider>,
    );

    expect(screen.queryByRole('menu')).not.toBeInTheDocument();

    const button = screen.getByRole('button', { name: /time span/i });
    await user.click(button);
    expect(screen.getByRole('menu')).toBeInTheDocument();

    await user.keyboard('{Escape}');
    expect(screen.queryByRole('menu')).not.toBeInTheDocument();
  });

  it('should render the button correctly with 13 marks and 2 days as active', async () => {
    render(
      <ThemeProvider>
        <TimeSpanButton {...props} />
      </ThemeProvider>,
    );

    await user.click(screen.getByRole('button', { name: /time span/i }));

    expect(screen.getAllByRole('menuitem')).toHaveLength(13);

    expect(screen.getByRole('menuitem', { name: /48 h/i })).toHaveClass(
      'Mui-selected',
    );
  });

  it('handleChange is called to increase/decrease scale', async () => {
    render(
      <ThemeProvider>
        <TimeSpanButton {...props} />
      </ThemeProvider>,
    );
    await user.click(screen.getByRole('button', { name: /time span/i }));

    await user.click(screen.getByRole('menuitem', { name: /6 h/i }));
    expect(props.onChangeTimeSliderSpan).toHaveBeenCalledTimes(1);

    expect(props.onChangeTimeSliderSpan).toHaveBeenLastCalledWith(
      6 * 3600,
      1609500060,
      216,
    );
  });

  it('span button can be correctly called with other span value than 5', async () => {
    const props2 = {
      mapId: 'map_id',
      timeSliderSpan: 2 * 7 * 24 * 3600,
      timeSliderWidth: 100,
      centerTime: moment.utc('2021-01-01 12:00:00').unix(),
      secondsPerPx: 60,
      selectedTime: moment.utc('2021-01-01 12:15:00').unix(),
      onChangeTimeSliderSpan: jest.fn(),
      isTimeSpanAuto: false,
    };
    render(
      <ThemeProvider>
        <TimeSpanButton {...props2} />
      </ThemeProvider>,
    );
    await user.click(screen.getByRole('button', { name: /time span/i }));

    expect(screen.getByRole('menuitem', { name: /2 weeks/i })).toHaveClass(
      'Mui-selected',
    );
  });

  it('TimeSpanButton value can be scrolled with mouse', async () => {
    jest.useFakeTimers();

    render(
      <ThemeProvider>
        <TimeSpanButton {...props} />
      </ThemeProvider>,
    );

    const button = screen.getByRole('button', { name: /time span/i });

    fireEvent.wheel(button, { deltaY: 1 });
    expect(props.onChangeTimeSliderSpan).toHaveBeenCalledTimes(1);
    expect(props.onChangeTimeSliderSpan).toHaveBeenCalledWith(
      24 * 3600,
      1609490340,
      864,
    );

    // wait for throttle time
    jest.runOnlyPendingTimers();

    fireEvent.wheel(button, { deltaY: -1 });
    expect(props.onChangeTimeSliderSpan).toHaveBeenCalledTimes(2);
    expect(props.onChangeTimeSliderSpan).toHaveBeenCalledWith(
      7 * 24 * 3600,
      1609412580,
      6048,
    );

    jest.useRealTimers();
  });
});
