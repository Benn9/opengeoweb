/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { useDispatch, useSelector } from 'react-redux';

import {
  CoreAppStore,
  layerSelectors,
  mapActions,
  mapEnums,
  mapSelectors,
  mapUtils,
} from '@opengeoweb/store';
import { TimeSpanButton } from './TimeSpanButton';

export interface TimeSpanButtonConnectProps {
  mapId: string;
}

export const TimeSpanButtonConnect = ({
  mapId,
}: TimeSpanButtonConnectProps): JSX.Element => {
  const timeSliderSpan = useSelector((store: CoreAppStore) =>
    mapSelectors.getMapTimeSliderSpan(store, mapId),
  );
  const isAnimating = useSelector((store: CoreAppStore) =>
    mapSelectors.isAnimating(store, mapId),
  );
  const centerTime = useSelector((store: CoreAppStore) =>
    mapSelectors.getMapTimeSliderCenterTime(store, mapId),
  );
  const isTimeSpanAuto = useSelector((store: CoreAppStore) =>
    mapSelectors.isTimeSpanAuto(store, mapId),
  );
  const timeSliderWidth = useSelector((store: CoreAppStore) =>
    mapSelectors.getMapTimeSliderWidth(store, mapId),
  );
  const secondsPerPx = useSelector((store: CoreAppStore) =>
    mapSelectors.getMapTimeSliderSecondsPerPx(store, mapId),
  );

  const autoTimeStepLayerId = useSelector((store: CoreAppStore) =>
    mapSelectors.getAutoTimeStepLayerId(store, mapId),
  );
  const activeLayerTimeDimension = useSelector((store: CoreAppStore) =>
    layerSelectors.getLayerTimeDimension(store, autoTimeStepLayerId),
  );
  const timeStepFromLayer = mapUtils.getActiveLayerTimeStep(
    activeLayerTimeDimension!,
  );

  const dispatch = useDispatch();

  const onSetTimeSliderSpan = (
    newSpan: number,
    newCenterTime: number,
    newSecondsPerPx: number,
  ): void => {
    dispatch(
      mapActions.setTimeSliderSecondsPerPx({
        mapId,
        timeSliderSecondsPerPx: newSecondsPerPx,
      }),
    );
    dispatch(
      mapActions.setTimeSliderSpan({
        mapId,
        timeSliderSpan: newSpan!,
      }),
    );
    dispatch(
      mapActions.setTimeSliderCenterTime({
        mapId,
        timeSliderCenterTime: newCenterTime,
      }),
    );
  };

  const onToggleTimeSpanAuto = React.useCallback((): void => {
    dispatch(
      mapActions.toggleTimeSpanAuto({
        mapId,
        timeSpanAuto: !isTimeSpanAuto,
        origin: mapEnums.MapActionOrigin.map,
      }),
    );
  }, [dispatch, isTimeSpanAuto, mapId]);
  const selectedTime = useSelector((store: CoreAppStore) =>
    mapSelectors.getSelectedTime(store, mapId),
  );
  return (
    <TimeSpanButton
      timeSliderSpan={timeSliderSpan!}
      onChangeTimeSliderSpan={onSetTimeSliderSpan}
      disabled={isAnimating}
      secondsPerPx={secondsPerPx!}
      centerTime={centerTime!}
      selectedTime={selectedTime}
      timeSliderWidth={timeSliderWidth!}
      onToggleTimeSpanAuto={onToggleTimeSpanAuto}
      isTimeSpanAuto={
        Boolean(isTimeSpanAuto) && Boolean(activeLayerTimeDimension)
      }
      timeStepFromLayer={timeStepFromLayer!}
    />
  );
};
