/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */
import { dateUtils } from '@opengeoweb/shared';
import React from 'react';

import {
  CoreAppStore,
  mapActions,
  mapEnums,
  mapSelectors,
  mapTypes,
  mapUtils,
} from '@opengeoweb/store';
import { CanvasComponent } from '@opengeoweb/webmap-react';
import { useDispatch, useSelector } from 'react-redux';

export interface TimeBounds {
  startTime: number;
  endTime: number;
}

/**
 * Returns time bounds from the given dimension. If no time dimension given, current time is returned as default
 */
export const getTimeBounds = (dimensions: mapTypes.Dimension[]): TimeBounds => {
  const defaultStartEnd = dateUtils.unix(
    dateUtils.set(dateUtils.utc(), { seconds: 0, milliseconds: 0 }),
  );
  const timeDimension = dimensions?.find((dim) => dim.name === 'time');

  const startTime =
    timeDimension && timeDimension.minValue
      ? dateUtils.unix(dateUtils.utc(timeDimension.minValue))
      : defaultStartEnd;
  const endTime =
    timeDimension && timeDimension.maxValue
      ? dateUtils.unix(dateUtils.utc(timeDimension.maxValue))
      : defaultStartEnd;

  return {
    startTime,
    endTime,
  };
};

export const scalingCoefficient = (
  end: number,
  start: number,
  width: number,
): number => width / (end - start);

export const timestampToPixelEdges = (
  timestamp: number,
  start: number,
  end: number,
  width: number,
): number => (timestamp - start) * scalingCoefficient(end, start, width);

export const timestampToPixel = (
  timestamp: number,
  centerTime: number,
  widthPx: number,
  secondsPerPx: number,
): number => widthPx / 2 - (centerTime - timestamp) / secondsPerPx;

export const pixelToTimestamp = (
  timePx: number,
  centerTime: number,
  widthPx: number,
  secondsPerPx: number,
): number => centerTime - (widthPx / 2 - timePx) * secondsPerPx;

/**
 * This function creates and returns an array where there is secondsPerPx
 * number of the data set added for a data scale (secondsPerPx)
 * to secondsPerPxToScale.
 * @param secondsPerPx
 * @returns an array containing secondsPerPx numbers.
 * The secondsPerPx number for a data scale is included
 * contrary to secondsPerPxToScale.
 */

export const secondsPerPxValues = (secondsPerPx: number): number[] => {
  const sortedSecondsPerPx = [...mapUtils.secondsPerPxToScale].map(
    (value: [number, mapEnums.Scale]) => Number(value[0]),
  );
  const full = [...sortedSecondsPerPx, secondsPerPx];
  return full;
};

export const secondsPerPxFromCanvasWidth = (
  canvasWidth: number,
  spanInSeconds: number,
): number | undefined => {
  if (canvasWidth <= 0 || spanInSeconds <= 0) {
    return undefined;
  }
  return spanInSeconds / canvasWidth;
};

export const getNewCenterOfFixedPointZoom = (
  fixedTimePoint: number,
  oldSecondsPerPx: number,
  newSecondsPerPx: number,
  oldCenterTime: number,
): number => {
  const centerToFixedPointPx =
    (fixedTimePoint - oldCenterTime) / oldSecondsPerPx;
  return fixedTimePoint - centerToFixedPointPx * newSecondsPerPx;
};

/**
 * Move the time slider such that a given time point (timePoint)
 * is at a given pixel width (timePointPx).
 * @returns a new center time for the resulting position
 */
export const moveRelativeToTimePoint = (
  timePoint: number,
  timePointPx: number,
  secondsPerPx: number,
  canvasWidth: number,
): number => timePoint - secondsPerPx * (timePointPx - canvasWidth / 2);

/** This reusable custom hook tells whether given pointer event targets current canvas node.
 * Example:
 *
 * const [isAllowedCanvasNodePointed, nodeRef] = useCanvasTarget('mousedown', false);
 *  ...
 * return isAllowedCanvasNodePointed ? (<CanvasComponent ref={nodeRef}>Component with ref</CanvasComponent>) : (<CanvasComponent>Component with NO ref</CanvasComponent>);
 *  ...
 */

export const useCanvasTarget = (
  eventType: string,
): [boolean, React.RefObject<CanvasComponent>] => {
  const nodeRef = React.useRef<CanvasComponent>(null);
  const [isTargetNode, setTargetNode] = React.useState(false);

  // Check if pointer event targets current node element
  const pointerEventListener = React.useCallback(
    (event: PointerEvent) => {
      setTargetNode(
        nodeRef.current!.canvas
          ? nodeRef.current!.canvas.isEqualNode(event.target as Node)
          : false,
      );
    },
    [nodeRef],
  );

  React.useEffect(() => {
    document.addEventListener(eventType, pointerEventListener);
    return (): void =>
      document.removeEventListener(eventType, pointerEventListener);
  }, [eventType, pointerEventListener]); // Only add/remove event listener when listener really changes, that is, NOT necessarily between every re-render.

  return [isTargetNode, nodeRef];
};

export const timeBoxGeom = {
  smallWidth: 126,
  largeWidth: 167,
  height: 24,
  cornerRadius: 5,
  lineWidth: 1,
  iconWidth: 25,
};

export const AUTO_MOVE_AREA_PADDING = 1;
export const getAutoMoveAreaWidth = (scale: mapEnums.Scale): number =>
  (scale === mapEnums.Scale.Year
    ? timeBoxGeom.largeWidth + timeBoxGeom.iconWidth
    : timeBoxGeom.smallWidth + timeBoxGeom.iconWidth) /
    2 +
  AUTO_MOVE_AREA_PADDING;

export enum TimeInMinutes {
  YEAR = 525600,
  MONTH = 43800,
  DAY = 1440,
  HOUR = 60,
  MINUTE = 1,
}

/**
 *
 * @param minutes
 * @returns the two highest values in the format yr(s) mo(s) d h m
 */
export const minutesToDescribedDuration = (minutes: number): string => {
  const units = [
    { label: 'yr', value: TimeInMinutes.YEAR, plural: 's' },
    { label: 'mo', value: TimeInMinutes.MONTH, plural: 's' },
    { label: 'd', value: TimeInMinutes.DAY },
    { label: 'h', value: TimeInMinutes.HOUR },
    { label: 'm', value: TimeInMinutes.MINUTE },
  ];
  const durations = units.reduce(
    (units, duration) => {
      const { min, time, count } = units;
      const { label, value, plural = '' } = duration;
      const unit = Math.floor(min / value);

      if (unit > 0 && count < 2) {
        return {
          time: `${time}${unit}${label}${unit > 1 ? plural : ''} `,
          min: min - unit * value,
          count: count + 1,
        };
      }

      return { min, time, count };
    },
    {
      min: minutes,
      time: '',
      count: 0,
    },
  );

  return durations.time.trim();
};

// For a data scale an appropriate fundamental scale is chosen
// depending on secondsPerPx of data. When a time range of
// data is for example many months. the fundamental scale mapEnums.Scale.Year
// is chosen here and the legend of the time slider is drawn in the
// same as if otiginally a year scale would have been chose.
//
// If a  time range of data is a few weeks, mapEnums.Scale.Month is chosen. So, all
// the time range of data can be shown in the window.

export const getFundamentalScale = (secondsPerPx: number): mapEnums.Scale => {
  if (secondsPerPx >= mapUtils.scaleToSecondsPerPx[mapEnums.Scale.Month]) {
    return mapEnums.Scale.Year;
  }
  if (secondsPerPx >= mapUtils.scaleToSecondsPerPx[mapEnums.Scale.Week]) {
    return mapEnums.Scale.Month;
  }
  if (secondsPerPx >= mapUtils.scaleToSecondsPerPx[mapEnums.Scale.Day]) {
    return mapEnums.Scale.Week;
  }
  if (secondsPerPx >= mapUtils.scaleToSecondsPerPx[mapEnums.Scale.Hours6]) {
    return mapEnums.Scale.Day;
  }
  if (secondsPerPx >= mapUtils.scaleToSecondsPerPx[mapEnums.Scale.Hours3]) {
    return mapEnums.Scale.Hours6;
  }
  if (secondsPerPx >= mapUtils.scaleToSecondsPerPx[mapEnums.Scale.Hour]) {
    return mapEnums.Scale.Hours3;
  }
  if (secondsPerPx >= mapUtils.scaleToSecondsPerPx[mapEnums.Scale.Minutes5]) {
    return mapEnums.Scale.Hour;
  }
  return mapEnums.Scale.Minutes5;
};

export const useTurnOffAutoUpdateIfItsOn = (mapId: string): (() => void) => {
  const isAutoUpdating = useSelector((store: CoreAppStore) =>
    mapSelectors.isAutoUpdating(store, mapId),
  );
  const dispatch = useDispatch();
  const turnOffAutoUpdateIfItsOn = (): void => {
    if (isAutoUpdating) {
      dispatch(mapActions.toggleAutoUpdate({ mapId, shouldAutoUpdate: false }));
    }
  };
  return turnOffAutoUpdateIfItsOn;
};
