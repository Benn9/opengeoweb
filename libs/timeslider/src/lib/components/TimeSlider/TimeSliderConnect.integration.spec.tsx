/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */

import { act, render, screen } from '@testing-library/react';
import React from 'react';
import { createStore } from '@redux-eggs/redux-toolkit';
import { getSagaExtension } from '@redux-eggs/saga-extension';
import { layerActions, mapActions, uiActions } from '@opengeoweb/store';
import userEvent from '@testing-library/user-event';
import { TimeSliderConnect } from './TimeSliderConnect';
import { ThemeStoreProvider } from '../../testUtils/Providers';

describe('src/components/TimeSlider/TimeSliderConnect', () => {
  it('should turn off auto update when changing time manually', async () => {
    const mapId = 'mapId';

    const store = createStore({
      extensions: [getSagaExtension()],
    });

    render(
      <ThemeStoreProvider store={store}>
        <TimeSliderConnect mapId={mapId} sourceId={mapId} />
      </ThemeStoreProvider>,
    );

    // setup store
    act(() => {
      store.dispatch(mapActions.registerMap({ mapId }));
      const date = `2020-01-15T`;
      const dateTime = `${date}12:00:00Z`;
      store.dispatch(
        layerActions.addLayer({
          mapId,
          layerId: 'layerId',
          layer: {
            dimensions: [
              {
                name: 'time',
                minValue: dateTime,
                currentValue: dateTime,
                maxValue: `${date}13:00:00Z`,
              },
            ],
          },
          origin: 'origin',
        }),
      );
      store.dispatch(uiActions.setActiveWindowId({ activeWindowId: mapId }));
    });

    const user = userEvent.setup();

    const turnAutoUpdateOn = async (): Promise<void> => {
      await user.click(
        screen.getByRole('button', { name: /Auto update off/i }),
      );
      expect(
        screen.getByRole('button', { name: /Auto update on/i }),
      ).toBeInTheDocument();
    };

    const expectAutoUpdateToBeOff = (): void => {
      expect(
        screen.getByRole('button', { name: /Auto update off/i }),
      ).toBeInTheDocument();
    };

    // open animation menu and turn auto update on
    await user.click(
      screen.getByRole('button', { name: /animation options/i }),
    );
    await turnAutoUpdateOn();

    // click backward turns auto update off
    await user.click(screen.getByRole('button', { name: /backward/i }));
    expectAutoUpdateToBeOff();

    // click forward turns auto update off
    await turnAutoUpdateOn();
    await user.click(screen.getByRole('button', { name: /forward/i }));
    expectAutoUpdateToBeOff();

    // click now button turns auto update off
    await turnAutoUpdateOn();
    await user.click(screen.getByRole('button', { name: /now/i }));
    expectAutoUpdateToBeOff();

    // move time with keyboard turns auto update off
    await turnAutoUpdateOn();
    await user.keyboard('{Control>}{ArrowLeft}{/Control}');
    expectAutoUpdateToBeOff();

    await turnAutoUpdateOn();
    await user.keyboard('{Control>}{ArrowRight}{/Control}');
    expectAutoUpdateToBeOff();
  });
});
