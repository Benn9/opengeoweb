/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { dateUtils } from '@opengeoweb/shared';

import {
  mapSelectors,
  mapActions,
  uiSelectors,
  layerSelectors,
  genericActions,
  mapEnums,
  mapUtils,
  CoreAppStore,
  mapConstants,
} from '@opengeoweb/store';
import { handleMomentISOString } from '@opengeoweb/webmap';
import { TimeSlider } from './TimeSlider';
import {
  TimeBounds,
  getNewCenterOfFixedPointZoom,
  getTimeBounds,
  secondsPerPxFromCanvasWidth,
  useTurnOffAutoUpdateIfItsOn,
} from './timeSliderUtils';
import { TimeSliderButtonsConnect } from '../TimeSliderButtons';
import { TimeSliderLegendConnect } from '../TimeSliderLegend';
import { TimeSliderCurrentTimeBoxConnect } from '../TimeSliderCurrentTimeBox';

export const useUpdateTimestep = (mapId: string): void => {
  const autoTimeStepLayerId = useSelector((store: CoreAppStore) =>
    mapSelectors.getAutoTimeStepLayerId(store, mapId),
  );
  const isTimeStepAuto = useSelector((store: CoreAppStore) =>
    mapSelectors.isTimestepAuto(store, mapId),
  );
  const timeDimension = useSelector((store: CoreAppStore) =>
    layerSelectors.getLayerTimeDimension(store, autoTimeStepLayerId),
  );
  const dispatch = useDispatch();
  React.useEffect(() => {
    if (isTimeStepAuto) {
      const timeStep = mapUtils.getActiveLayerTimeStep(timeDimension);
      if (timeStep !== undefined) {
        dispatch(mapActions.setTimeStep({ mapId, timeStep }));
      }
    }
  }, [autoTimeStepLayerId, dispatch, isTimeStepAuto, mapId, timeDimension]);
};

export const useUpdateTimeSpan = (
  mapId: string,
  onSetTimeSliderSpan: (
    newSpan: number,
    newCenterTime: number,
    newSecondsPerPx: number,
  ) => void,
): void => {
  const autoTimeStepLayerId = useSelector((store: CoreAppStore) =>
    mapSelectors.getAutoTimeStepLayerId(store, mapId),
  );
  const isTimeSpanAuto = useSelector((store: CoreAppStore) =>
    mapSelectors.isTimeSpanAuto(store, mapId),
  );
  const activeLayerTimeDimension = useSelector((store: CoreAppStore) =>
    layerSelectors.getLayerTimeDimension(store, autoTimeStepLayerId),
  );
  const timeSliderWidth = useSelector((store: CoreAppStore) =>
    mapSelectors.getMapTimeSliderWidth(store, mapId),
  );
  const centerTime = useSelector((store: CoreAppStore) =>
    mapSelectors.getMapTimeSliderCenterTime(store, mapId),
  );
  const secondsPerPx = useSelector((store: CoreAppStore) =>
    mapSelectors.getMapTimeSliderSecondsPerPx(store, mapId),
  );
  const currentTimeSpan = useSelector((store: CoreAppStore) =>
    mapSelectors.getMapTimeSliderSpan(store, mapId),
  );

  const selectedTime = useSelector((store: CoreAppStore) =>
    mapSelectors.getSelectedTime(store, mapId),
  );
  const updateTimeSliderSpan = (
    spanInSeconds: number,
    newCenterTime: number,
    newSecondsPerPx: number,
  ): void => {
    if (
      spanInSeconds !== undefined &&
      newCenterTime !== undefined &&
      newSecondsPerPx !== undefined
    ) {
      onSetTimeSliderSpan(spanInSeconds, newCenterTime, newSecondsPerPx);
    }
  };

  React.useEffect(() => {
    if (isTimeSpanAuto && autoTimeStepLayerId) {
      const { startTime, endTime }: TimeBounds = getTimeBounds([
        activeLayerTimeDimension!,
      ]);
      const spanInSeconds = endTime! - startTime!;

      const newSecondsPerPx = secondsPerPxFromCanvasWidth(
        timeSliderWidth!,
        spanInSeconds,
      );

      const newCenterTime = (startTime! + endTime!) / 2;

      updateTimeSliderSpan(spanInSeconds, newCenterTime, newSecondsPerPx!);
    } else {
      const spanInSeconds = currentTimeSpan || mapConstants.defaultTimeSpan;

      const newSecondsPerPx = secondsPerPxFromCanvasWidth(
        timeSliderWidth!,
        spanInSeconds,
      );

      const newCenterTime = getNewCenterOfFixedPointZoom(
        selectedTime,
        secondsPerPx!,
        newSecondsPerPx!,
        centerTime!,
      );

      updateTimeSliderSpan(spanInSeconds, newCenterTime, newSecondsPerPx!);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [autoTimeStepLayerId, isTimeSpanAuto, mapId]);
};

interface TimeSliderConnectProps {
  sourceId: string;
  mapId: string;
  isAlwaysVisible?: boolean;
}

export const TimeSliderConnect: React.FC<TimeSliderConnectProps> = ({
  sourceId,
  mapId,
  isAlwaysVisible = false,
}: TimeSliderConnectProps) => {
  const isTimeSliderHoverOn = useSelector((store: CoreAppStore) =>
    mapSelectors.isTimeSliderHoverOn(store, mapId),
  );

  const activeWindowId = useSelector((store: CoreAppStore) =>
    uiSelectors.getActiveWindowId(store),
  );
  const isTimeSliderVisible = useSelector((store: CoreAppStore) =>
    mapSelectors.isTimeSliderVisible(store, mapId),
  );

  // don't use redux state if isAlwaysVisible
  const isVisible = isAlwaysVisible || isTimeSliderVisible;
  const mapIsActive = mapId === activeWindowId;

  const dispatch = useDispatch();

  // TODO: move keyboard logic to TimeSlider.tsx
  React.useEffect(() => {
    const handleKeyDown = (event: KeyboardEvent): void => {
      if (event.ctrlKey && event.altKey && event.key === 'h') {
        dispatch(
          mapActions.toggleTimeSliderHover({
            mapId,
            isTimeSliderHoverOn: !isTimeSliderHoverOn,
          }),
        );
      }
    };
    document.addEventListener('keydown', handleKeyDown);
    return (): void => {
      document.removeEventListener('keydown', handleKeyDown);
    };
  }, [mapId, isTimeSliderHoverOn, dispatch]);

  const onToggleTimeSliderVisibility = React.useCallback(
    (isTimeSliderVisible: boolean): void => {
      dispatch(
        mapActions.toggleTimeSliderIsVisible({
          mapId,
          isTimeSliderVisible,
          origin: mapEnums.MapActionOrigin.map,
        }),
      );
    },
    [dispatch, mapId],
  );

  const onSetTimeSliderSpan = React.useCallback(
    (newSpan: number, newCenterTime: number, newSecondsPerPx: number): void => {
      dispatch(
        mapActions.setTimeSliderSecondsPerPx({
          mapId,
          timeSliderSecondsPerPx: newSecondsPerPx,
        }),
      );
      dispatch(
        mapActions.setTimeSliderSpan({
          mapId,
          timeSliderSpan: newSpan!,
        }),
      );
      dispatch(
        mapActions.setTimeSliderCenterTime({
          mapId,
          timeSliderCenterTime: newCenterTime,
        }),
      );
    },
    [dispatch, mapId],
  );

  const timeStep = useSelector((store: CoreAppStore) =>
    mapSelectors.getMapTimeStep(store, mapId),
  );

  const [dataStartTime, dataEndTime] = useSelector((store: CoreAppStore) =>
    mapSelectors.getDataLimitsFromLayers(store, mapId),
  );
  const isAnimating = useSelector((store: CoreAppStore) =>
    mapSelectors.isAnimating(store, mapId),
  );

  const selectedTime = useSelector((store: CoreAppStore) =>
    mapSelectors.getSelectedTime(store, mapId),
  );
  useUpdateTimestep(mapId);
  useUpdateTimeSpan(mapId, onSetTimeSliderSpan);

  const turnOffAutoUpdateIfItsOn = useTurnOffAutoUpdateIfItsOn(mapId);

  return (
    <TimeSlider
      currentTime={dateUtils.unix(dateUtils.utc())}
      timeStep={timeStep}
      dataStartTime={dataStartTime}
      dataEndTime={dataEndTime}
      onSetNewDate={(newDate: string): void => {
        turnOffAutoUpdateIfItsOn();
        if (isAnimating) {
          dispatch(mapActions.mapStopAnimation({ mapId }));
        }
        dispatch(
          genericActions.setTime({
            sourceId,
            value: handleMomentISOString(newDate),
            origin: '',
          }),
        );
      }}
      onSetCenterTime={(newTime: number): void => {
        dispatch(
          mapActions.setTimeSliderCenterTime({
            mapId,
            timeSliderCenterTime: newTime,
          }),
        );
      }}
      selectedTime={selectedTime}
      mapId={mapId}
      buttons={<TimeSliderButtonsConnect mapId={mapId} sourceId={sourceId} />}
      timeBox={
        <TimeSliderCurrentTimeBoxConnect mapId={mapId} sourceId={sourceId} />
      }
      legend={<TimeSliderLegendConnect mapId={mapId} sourceId={sourceId} />}
      mapIsActive={mapIsActive}
      onToggleTimeSlider={onToggleTimeSliderVisibility}
      isVisible={isVisible}
    />
  );
};
