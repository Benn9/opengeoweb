/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { connect, useSelector } from 'react-redux';
import {
  genericActions,
  mapActions,
  mapSelectors,
  CoreAppStore,
} from '@opengeoweb/store';

import { handleMomentISOString } from '@opengeoweb/webmap';
import { TimeSliderCurrentTimeBox } from './TimeSliderCurrentTimeBox';

interface TimeSliderCurrentTimeBoxConnectComponentProps {
  mapId: string;
  sourceId?: string;
  timeStep?: number;
  centerTime?: number;
  secondsPerPx?: number;
  span?: number;
  isAnimating?: boolean;
  isAutoUpdating?: boolean;
  unfilteredSelectedTime?: number;
  setTimeSliderUnfilteredSelectedTime?: typeof mapActions.setTimeSliderUnfilteredSelectedTime;
  stopMapAnimation?: typeof mapActions.mapStopAnimation;
  setTime?: typeof genericActions.setTime;
  setCenterTime?: typeof mapActions.setTimeSliderCenterTime;
}

const connectRedux = connect(
  (
    store: CoreAppStore,
    props: TimeSliderCurrentTimeBoxConnectComponentProps,
  ) => ({
    centerTime: mapSelectors.getMapTimeSliderCenterTime(store, props.mapId),
    secondsPerPx: mapSelectors.getMapTimeSliderSecondsPerPx(store, props.mapId),
    span: mapSelectors.getMapTimeSliderSpan(store, props.mapId),
    timeStep: mapSelectors.getMapTimeStep(store, props.mapId),
    isAnimating: mapSelectors.isAnimating(store, props.mapId),
    isAutoUpdating: mapSelectors.isAutoUpdating(store, props.mapId),
    unfilteredSelectedTime: mapSelectors.getTimeSliderUnfilteredSelectedTime(
      store,
      props.mapId,
    ),
  }),
  {
    setTimeSliderUnfilteredSelectedTime:
      mapActions.setTimeSliderUnfilteredSelectedTime,
    stopMapAnimation: mapActions.mapStopAnimation,
    setTime: genericActions.setTime,
    setCenterTime: mapActions.setTimeSliderCenterTime,
  },
);

const TimeSliderCurrentTimeBoxConnectComponent: React.FC<
  TimeSliderCurrentTimeBoxConnectComponentProps
> = ({
  mapId,
  sourceId,
  centerTime,
  secondsPerPx,
  span,
  timeStep,
  isAnimating,
  isAutoUpdating,
  stopMapAnimation,
  unfilteredSelectedTime,
  setTimeSliderUnfilteredSelectedTime,
  setTime,
  setCenterTime,
}: TimeSliderCurrentTimeBoxConnectComponentProps) => {
  const selectedTime = useSelector((store: CoreAppStore) =>
    mapSelectors.getSelectedTime(store, mapId),
  );
  const [dataStartTime, dataEndTime] = useSelector((store: CoreAppStore) =>
    mapSelectors.getDataLimitsFromLayers(store, mapId),
  );
  return (
    <TimeSliderCurrentTimeBox
      centerTime={centerTime!}
      secondsPerPx={secondsPerPx!}
      selectedTime={selectedTime}
      span={span}
      dataStartTime={dataStartTime}
      dataEndTime={dataEndTime}
      timeStep={timeStep}
      isAutoUpdating={isAutoUpdating}
      unfilteredSelectedTime={unfilteredSelectedTime || selectedTime}
      setUnfilteredSelectedTime={(
        timeSliderUnfilteredSelectedTime: number,
      ): void => {
        if (setTimeSliderUnfilteredSelectedTime) {
          setTimeSliderUnfilteredSelectedTime({
            timeSliderUnfilteredSelectedTime,
            mapId,
          });
        }
      }}
      onSetNewDate={(newDate: string): void => {
        if (isAnimating) {
          stopMapAnimation!({ mapId });
        }
        setTime!({
          sourceId: sourceId!,
          origin: 'TimeSliderConnect, 139',
          value: handleMomentISOString(newDate),
        });
      }}
      onSetCenterTime={(newTime: number): void => {
        setCenterTime!({ mapId, timeSliderCenterTime: newTime });
      }}
    />
  );
};

export const TimeSliderCurrentTimeBoxConnect = connectRedux(
  TimeSliderCurrentTimeBoxConnectComponent,
);
