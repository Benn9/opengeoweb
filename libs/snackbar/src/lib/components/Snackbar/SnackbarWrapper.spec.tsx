/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { fireEvent, render, screen } from '@testing-library/react';

import { SnackbarWrapper } from './SnackbarWrapper';

describe('src/lib/components/Snackbar', () => {
  it('should render children even if snackbar is closed', () => {
    const defaultProps = {
      message: 'Snackbar message',
      id: 'snackbar1',
      isOpen: false,
      onCloseSnackbar: jest.fn(),
    };

    render(
      <SnackbarWrapper {...defaultProps}>
        <div>I am a child</div>
      </SnackbarWrapper>,
    );

    expect(screen.getByText('I am a child')).toBeTruthy();
    expect(screen.queryByTestId('snackbarComponent')).toBeFalsy();
  });

  it('should call onCloseSnackbar when clicked on the close button', () => {
    const defaultProps = {
      message: 'Snackbar message',
      id: 'snackbar1',
      isOpen: true,
      onCloseSnackbar: jest.fn(),
    };

    render(
      <SnackbarWrapper {...defaultProps}>
        <div>I am a child</div>
      </SnackbarWrapper>,
    );

    expect(screen.getByTestId('snackbarComponent')).toBeTruthy();

    fireEvent.click(screen.getByTestId('snackbarCloseButton'));
    expect(defaultProps.onCloseSnackbar).toHaveBeenCalled();
  });

  it('should not call onCloseSnackbar when clicked anywhere on the page', () => {
    const defaultProps = {
      message: 'Snackbar message',
      id: 'snackbar1',
      isOpen: true,
      onCloseSnackbar: jest.fn(),
    };

    render(
      <SnackbarWrapper {...defaultProps}>
        <div data-testid="childdiv">I am a child</div>
      </SnackbarWrapper>,
    );

    expect(screen.getByTestId('snackbarComponent')).toBeTruthy();

    fireEvent.click(screen.getByTestId('childdiv'));
    expect(screen.getByTestId('snackbarComponent')).toBeTruthy();
    expect(defaultProps.onCloseSnackbar).not.toHaveBeenCalled();
  });
});
