/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */
import axios, {
  AxiosInstance,
  InternalAxiosRequestConfig,
  AxiosResponse,
} from 'axios';
import { CreateApiProps, Credentials, GeoWebJWT } from './types';

const DEFAULT_TIMEOUT = 15000;

export const KEEP_ALIVE_IN_SECONDS = 60; // Number of seconds in between intervals to check with the token request if connection is stil intact

export const KEEP_ALIVE_POLLER_IN_SECONDS = 10; // Number of milliseconds to check if connection is restored or to check if KEEP_ALIVE_IN_SECONDS has passed

export const REFRESH_TOKEN_WHEN_PCT_EXPIRED = 75; // Refresh token when 75% expired. Set to (10 / 3600) * 100 = 0.2777778%  to test with 10 second interval.

const DEFAULT_TOKEN_EXPIRES_IN = 3600; // Number of seconds a token expires by default

export const MILLISECOND_TO_SECOND = 1 / 1000;

/**
 * Creates a Credentials object based on the axios response from the token service.
 *
 * It will calculate the expires_at attribute based on the expires_in property found in the JWT.
 *
 * @param tokenResponse Response of the tokenservice as axios response object.
 * @returns Credentials object.
 */
export const makeCredentialsFromTokenResponse = (
  tokenResponse: AxiosResponse,
): Credentials => {
  const token: GeoWebJWT = tokenResponse.data.body || tokenResponse.data;
  // eslint-disable-next-line @typescript-eslint/naming-convention
  const { access_token, refresh_token, id_token, expires_in } = token;
  if (!access_token || !id_token) {
    throw new Error('Login failed');
  }
  const userInfoString = atob(
    // convert base64url specific characters before parsing as base64
    id_token.split('.')[1].replace(/-/g, '+').replace(/_/g, '/'),
  );
  const userInfo = JSON.parse(userInfoString);

  const tokenExpirationInSeconds: number =
    expires_in && expires_in > 0 ? expires_in : DEFAULT_TOKEN_EXPIRES_IN; // If not set or zero in the token, assume valid for 1 hour.
  const pctTokenExpirationInSeconds =
    tokenExpirationInSeconds * (REFRESH_TOKEN_WHEN_PCT_EXPIRED / 100);
  const epochTimeTokenExpirationInSeconds =
    pctTokenExpirationInSeconds + getCurrentTimeInSeconds();

  const isAuthCognito = userInfo.iss.includes('cognito');
  const newAuth = {
    username: isAuthCognito ? userInfo['cognito:username'] : userInfo.email,
    token: access_token,
    refresh_token: refresh_token || '',
    expires_at: epochTimeTokenExpirationInSeconds,
    keep_session_alive_at: getCurrentTimeInSeconds() + KEEP_ALIVE_IN_SECONDS,
    has_connection_issue: false,
  };
  return newAuth;
};

export const refreshAccessToken = ({
  auth,
  config: { authTokenURL, authClientId, appURL } = {},
  timeout = DEFAULT_TIMEOUT,
}: CreateApiProps): Promise<AxiosResponse> => {
  // Refresh token request with a new axios instance
  // without request interceptor
  const tokenAxiosInstance = axios.create({
    headers: {},
    timeout,
  });
  const refreshPayload = {
    refresh_token: auth!.refresh_token,
    redirect_uri: `${appURL}/code`,
    grant_type: 'refresh_token',
    client_id: authClientId!,
  };
  /* Send data in the "application/x-www-form-urlencoded" format.
    If only JSON is supported, use Axios' default content type ("application/x-www-form-urlencoded"). */
  const useDefaultContentType = authTokenURL!.includes('amazonaws.com');
  const data = useDefaultContentType
    ? refreshPayload
    : new URLSearchParams(refreshPayload);

  const axiosConfig = {
    headers: {
      'Content-Type': useDefaultContentType
        ? 'application/json'
        : 'application/x-www-form-urlencoded',
    },
  };
  return tokenAxiosInstance.post(authTokenURL!, data, axiosConfig);
};

export const refreshAccessTokenAndSetAuthContext = async ({
  auth,
  onSetAuth,
  config,
  timeout = DEFAULT_TIMEOUT,
}: CreateApiProps): Promise<void> => {
  try {
    const refreshedToken = await refreshAccessToken({
      auth,
      config,
      timeout,
    });
    const newAuth = makeCredentialsFromTokenResponse(refreshedToken);

    // Cognito does not send a new refresh token, but gitlab does. Set it here into the auth context.
    if (!newAuth.refresh_token || newAuth.refresh_token.length === 0) {
      newAuth.refresh_token = auth!.refresh_token;
    }

    onSetAuth!(newAuth);
  } catch (e) {
    onSetAuth!({ ...auth!, has_connection_issue: true });
  }
};

export const createApiInstance = ({
  auth,
  onSetAuth,
  config: { baseURL, authTokenURL, authClientId, appURL } = {},
  timeout = DEFAULT_TIMEOUT,
}: CreateApiProps): AxiosInstance => {
  const axiosInstance = axios.create({
    baseURL,
    headers: {},
    timeout,
  });
  // Request interceptor for API calls done BEFORE the request is made.
  axiosInstance.interceptors.request.use(
    async (
      axiosConfig: InternalAxiosRequestConfig,
    ): Promise<InternalAxiosRequestConfig<unknown>> => {
      const timeInSecondsLeftBeforeExpiration =
        auth && auth.expires_at
          ? auth.expires_at - getCurrentTimeInSeconds()
          : 0; // If expires_at is not set, don't do anything. (set timeInSecondsLeftBeforeExpiration = 0 will skip refresh)

      if (timeInSecondsLeftBeforeExpiration < 0) {
        await refreshAccessTokenAndSetAuthContext({
          auth,
          onSetAuth,
          config: { baseURL, authTokenURL, authClientId, appURL },
          timeout,
        });
      }

      const newConfig = {
        ...axiosConfig,
        headers: {
          'Content-Type': 'application/json',
          Authorization: `Bearer ${auth!.token}`,
          Accept: 'application/json',
          ...axiosConfig.headers, // use header settings from config parameters
        },
      };
      return newConfig as InternalAxiosRequestConfig;
    },
    (error) => {
      Promise.reject(error);
    },
  );

  // Response interceptor for API calls done AFTER the request is made.
  axiosInstance.interceptors.response.use(
    (response) => response,
    async (error) => {
      const originalRequest = error.config;
      if (
        error.response &&
        error.response.status &&
        (error.response.status === 401 || error.response.status === 403) &&
        !originalRequest.inRetry
      ) {
        originalRequest.inRetry = true;
        await refreshAccessTokenAndSetAuthContext({
          auth,
          onSetAuth,
          config: { baseURL, authTokenURL, authClientId, appURL },
          timeout,
        });
        return axiosInstance(originalRequest);
      }
      return Promise.reject(error);
    },
  );

  return axiosInstance;
};

export const createNonAuthApiInstance = ({
  config: { baseURL } = {},
  timeout = DEFAULT_TIMEOUT,
}: CreateApiProps): AxiosInstance => {
  const axiosInstance = axios.create({
    baseURL,
    headers: {},
    timeout,
  });

  // Request interceptor for API calls
  axiosInstance.interceptors.request.use(
    async (
      config: InternalAxiosRequestConfig,
    ): Promise<InternalAxiosRequestConfig<unknown>> => {
      const newConfig = {
        ...config,
        headers: {
          'Content-Type': 'application/json',
          Accept: 'application/json',
          ...config.headers, // use header settings from config parameters
        },
      };
      return newConfig as InternalAxiosRequestConfig;
    },
    (error) => {
      Promise.reject(error);
    },
  );

  return axiosInstance;
};

export const fakeApiRequest = (signal?: AbortController): Promise<void> =>
  new Promise((resolve, reject) => {
    const timer = setTimeout(() => {
      resolve();
    }, 300);
    signal?.signal.addEventListener('abort', () => {
      clearTimeout(timer);
      reject(new Error('canceled'));
    });
  });

export const createFakeApiInstance = (): AxiosInstance =>
  ({
    // eslint-disable-next-line no-unused-vars, @typescript-eslint/no-unused-vars,
    get: (_url: string, _params?: unknown): Promise<void> => fakeApiRequest(),
    // eslint-disable-next-line no-unused-vars, @typescript-eslint/no-unused-vars,
    put: (_url, _params?): Promise<void> => fakeApiRequest(),
    // eslint-disable-next-line no-unused-vars, @typescript-eslint/no-unused-vars,
    post: (_url, _params?): Promise<void> => fakeApiRequest(),
    // eslint-disable-next-line no-unused-vars, @typescript-eslint/no-unused-vars
    delete: (_url, _params?): Promise<void> => fakeApiRequest(),
    // eslint-disable-next-line no-unused-vars, @typescript-eslint/no-unused-vars, @typescript-eslint/explicit-module-boundary-types
    patch: (_url, _params?, _signal?): Promise<void> =>
      fakeApiRequest(_signal as AbortController),
  } as AxiosInstance);

export const getCurrentTimeInSeconds = (): number =>
  Date.now() * MILLISECOND_TO_SECOND;
