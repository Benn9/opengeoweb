/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */

import { render } from '@testing-library/react';
import React from 'react';
import { ApiProvider, getApi } from './ApiContext';

describe('src/lib/components/ApiContext', () => {
  describe('getApi', () => {
    it('should register an api if name is given, and unregister on unmount', () => {
      const createDummyApi = jest.fn();
      const props = {
        createApi: (): unknown => createDummyApi,
        name: 'test-api',
      };
      expect(getApi(props.name)).toBeUndefined();

      const { unmount } = render(
        <ApiProvider {...props}>hello text</ApiProvider>,
      );
      expect(getApi(props.name)).toEqual(createDummyApi);

      unmount();
      // TODO: enable again https://gitlab.com/opengeoweb/opengeoweb/-/issues/3406
      // expect(getApi(props.name)).toBeUndefined();
    });
  });
});
