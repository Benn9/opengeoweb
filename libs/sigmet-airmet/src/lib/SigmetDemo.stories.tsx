/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */
import * as React from 'react';
import { useDispatch } from 'react-redux';
import { Box } from '@mui/material';
import {
  calculateDialogSizeAndPosition,
  ToolContainerDraggable,
} from '@opengeoweb/shared';
import {
  LayerManagerConnect,
  MultiMapDimensionSelectConnect,
} from '@opengeoweb/core';
import { uiActions } from '@opengeoweb/store';
import { LayerSelectConnect } from '@opengeoweb/layer-select';
import MetInfoWrapper from './components/MetInfoWrapper/MetInfoWrapper';
import {
  StoryWrapperFakeApi,
  StoryWrapperFakeApiWithErrors,
} from './utils/testUtils';
import { sigmetConfig } from './utils/config';
import { ProductConfig } from './types';
import { DownloadSigmetAirmetConfigWrapper } from './components/MetInfoWrapper';

export default { title: 'Demo Sigmet' };

interface AirmetDialogDemoComponentProps {
  productConfig?: ProductConfig;
}

const SigmetDialogDemoComponent = ({
  productConfig = sigmetConfig,
}: AirmetDialogDemoComponentProps): React.ReactElement => {
  const dispatch = useDispatch();
  const { width, height, top, left } = calculateDialogSizeAndPosition();

  React.useEffect(() => {
    dispatch(
      uiActions.setToggleOpenDialog({
        setOpen: false,
        type: 'legend',
      }),
    );
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <div style={{ height: '100vh' }}>
      <ToolContainerDraggable
        onClose={(): void => {}}
        title="SIGMET list"
        startPosition={{ top, left }}
        startSize={{ width, height }}
        minWidth={400}
        minHeight={300}
      >
        <Box
          sx={{
            padding: 3,
          }}
        >
          <MetInfoWrapper productType="sigmet" productConfig={productConfig} />
        </Box>
      </ToolContainerDraggable>
      <LayerManagerConnect bounds="parent" showMapIdInTitle />
      <LayerSelectConnect />
      <MultiMapDimensionSelectConnect />
    </div>
  );
};

interface DownloadConfigDemoProps {
  fakeConfig?: {
    GW_FEATURE_MODULE_SIGMET_CONFIGURATION?: string;
  };
}

const DownloadConfigDemoComponent: React.FC<DownloadConfigDemoProps> = ({
  fakeConfig,
}): React.ReactElement => {
  const dispatch = useDispatch();
  const { width, height, top, left } = calculateDialogSizeAndPosition();

  React.useEffect(() => {
    dispatch(
      uiActions.setToggleOpenDialog({
        setOpen: false,
        type: 'legend',
      }),
    );
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <div style={{ height: '100vh' }}>
      <ToolContainerDraggable
        onClose={(): void => {}}
        title="SIGMET list"
        startPosition={{ top, left }}
        startSize={{ width, height }}
        minWidth={400}
        minHeight={300}
      >
        <Box
          sx={{
            padding: 3,
          }}
        >
          <DownloadSigmetAirmetConfigWrapper
            productConfigKey={
              fakeConfig!.GW_FEATURE_MODULE_SIGMET_CONFIGURATION!
            }
            productType="sigmet"
          />
        </Box>
      </ToolContainerDraggable>
      <LayerManagerConnect bounds="parent" showMapIdInTitle />
      <LayerSelectConnect />
      <MultiMapDimensionSelectConnect />
    </div>
  );
};

export const SigmetDialogDemo = (): React.ReactElement => {
  return (
    <StoryWrapperFakeApi>
      <SigmetDialogDemoComponent />
    </StoryWrapperFakeApi>
  );
};

export const SigmetDialogDemoDark = (): React.ReactElement => {
  return (
    <StoryWrapperFakeApi isDarkTheme>
      <SigmetDialogDemoComponent />
    </StoryWrapperFakeApi>
  );
};

export const SigmetDialogDemoErrorApi = (): React.ReactElement => {
  return (
    <StoryWrapperFakeApiWithErrors isDarkTheme>
      <SigmetDialogDemoComponent productConfig={sigmetConfig} />
    </StoryWrapperFakeApiWithErrors>
  );
};

export const SigmetDialogDemoErrorConfig = (): React.ReactElement => {
  const productConfigWithErrors = {
    location_indicator_mwo: 10,
    fir_areas: {
      TEST: {
        location_indicator_atsu: 0,
      },
    },
  } as unknown as ProductConfig;
  return (
    <StoryWrapperFakeApi isDarkTheme>
      <SigmetDialogDemoComponent productConfig={productConfigWithErrors} />
    </StoryWrapperFakeApi>
  );
};

export const SigmetConfigIsLoading = (): React.ReactElement => {
  const fakeConfig = {
    GW_FEATURE_MODULE_SIGMET_CONFIGURATION: '/sigmetConfiguration',
  };
  return (
    <StoryWrapperFakeApi>
      <DownloadConfigDemoComponent fakeConfig={fakeConfig} />
    </StoryWrapperFakeApi>
  );
};

export const SigmetConfigIsLoadingWithError = (): React.ReactElement => {
  const fakeConfig = {};
  return (
    <StoryWrapperFakeApiWithErrors>
      <DownloadConfigDemoComponent fakeConfig={fakeConfig} />
    </StoryWrapperFakeApiWithErrors>
  );
};

SigmetDialogDemo.parameters = {
  zeplinLink: [
    {
      name: 'Light theme',
      link: 'https://app.zeplin.io/project/5ecf84a3c6ae1047a368f393/screen/61a4934891039316017011cd/version/6213a41da08de61393dadfca',
    },
    {
      name: 'Dark theme',
      link: 'https://app.zeplin.io/project/5ecf84a3c6ae1047a368f393/screen/61a4935476031e13fb0f3ab6/version/6213a4443c444d10d9e17536',
    },
  ],
};
