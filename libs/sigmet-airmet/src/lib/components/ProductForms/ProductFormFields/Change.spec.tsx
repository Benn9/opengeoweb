/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */
import React from 'react';
import {
  fireEvent,
  render,
  waitFor,
  screen,
  within,
} from '@testing-library/react';
import { ReactHookFormProvider } from '@opengeoweb/form-fields';
import Change from './Change';

describe('components/ProductForms/ProductFormFields/Change', () => {
  it('should be possible to select a change', async () => {
    render(
      <ReactHookFormProvider>
        <Change isDisabled={false} isReadOnly={false} />
      </ReactHookFormProvider>,
    );
    const field: HTMLInputElement = within(
      screen.getByTestId('change-WKN'),
    ).getByRole('radio');

    await waitFor(() => {
      expect(field.checked).toBeFalsy();
    });

    fireEvent.click(field);
    await waitFor(() => {
      expect(field.checked).toBeTruthy();
    });
  });
  it('should show change as disabled', async () => {
    render(
      <ReactHookFormProvider>
        <Change isDisabled isReadOnly={false} />
      </ReactHookFormProvider>,
    );

    expect(
      (
        within(screen.getByTestId('change-WKN')).getByRole(
          'radio',
        ) as HTMLInputElement
      ).disabled,
    ).toBeTruthy();
    expect(
      (
        within(screen.getByTestId('change-NC')).getByRole(
          'radio',
        ) as HTMLInputElement
      ).disabled,
    ).toBeTruthy();
    expect(
      (
        within(screen.getByTestId('change-INTSF')).getByRole(
          'radio',
        ) as HTMLInputElement
      ).disabled,
    ).toBeTruthy();
  });
  it('should show change as readonly', () => {
    render(
      <ReactHookFormProvider
        options={{
          defaultValues: {
            change: 'WKN',
          },
        }}
      >
        <Change isDisabled isReadOnly />
      </ReactHookFormProvider>,
    );

    const wkn: HTMLInputElement = within(
      screen.getByTestId('change-WKN'),
    ).getByRole('radio');
    expect(wkn.disabled).toBeTruthy();
    expect(wkn.checked).toBeTruthy();

    const nc: HTMLInputElement = within(
      screen.getByTestId('change-NC'),
    ).getByRole('radio', { hidden: true });
    expect(nc.disabled).toBeTruthy();
    expect(nc.checked).toBeFalsy();

    const intsf: HTMLInputElement = within(
      screen.getByTestId('change-INTSF'),
    ).getByRole('radio', { hidden: true });
    expect(intsf.disabled).toBeTruthy();
    expect(intsf.checked).toBeFalsy();
  });
});
