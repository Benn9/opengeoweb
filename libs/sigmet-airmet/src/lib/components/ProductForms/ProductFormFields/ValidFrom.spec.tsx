/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */
import React from 'react';
import { fireEvent, render, waitFor, screen } from '@testing-library/react';
import { dateUtils } from '@opengeoweb/shared';

import {
  ReactHookFormProvider,
  defaultFormOptions,
} from '@opengeoweb/form-fields';

import ValidFrom, {
  getDefaultValidFromValue,
  getValidFromXHoursBeforeMessage,
  VALID_FROM_ERROR_BEFORE_CURRENT,
} from './ValidFrom';
import { airmetConfig, sigmetConfig } from '../../../utils/config';

afterEach(() => {
  jest.clearAllMocks();
});

describe('components/ProductForms/ProductFormFields/ValidFrom', () => {
  const delayTime = sigmetConfig.valid_from_delay_minutes;

  it('should show current time +Xmins if no date passed in', async () => {
    const timeToShow = dateUtils.add(dateUtils.utc(), { minutes: delayTime });

    render(
      <ReactHookFormProvider>
        <ValidFrom
          productType="sigmet"
          productConfig={sigmetConfig}
          isDisabled={false}
        />
      </ReactHookFormProvider>,
    );

    const field = screen.getByRole('textbox');
    await waitFor(() => {
      expect(field!.getAttribute('value')!).toEqual(
        dateUtils.dateToString(timeToShow, 'yyyy/MM/dd HH:mm'),
      );
    });

    expect(screen.getByText('Select date and time')).toBeTruthy();
  });

  it('should show passed in date', async () => {
    render(
      <ReactHookFormProvider
        options={{
          ...defaultFormOptions,
          defaultValues: {
            validDateStart: '2020-11-17T13:03Z',
          },
        }}
      >
        <ValidFrom
          productType="sigmet"
          productConfig={sigmetConfig}
          isDisabled={false}
        />
      </ReactHookFormProvider>,
    );

    const field = screen.getByRole('textbox');

    await waitFor(() => {
      expect(field!.getAttribute('value')!).toEqual('2020/11/17 13:03');
    });
  });

  it('should be possible to set a date', async () => {
    const time = dateUtils.add(dateUtils.utc(), { minutes: delayTime });
    render(
      <ReactHookFormProvider>
        <ValidFrom
          productType="sigmet"
          productConfig={sigmetConfig}
          isDisabled={false}
        />
      </ReactHookFormProvider>,
    );

    const field = screen.getByRole('textbox');

    await waitFor(() => {
      expect(field!.getAttribute('value')!).toEqual(
        dateUtils.dateToString(time, 'yyyy/MM/dd HH:mm'),
      );
    });

    fireEvent.change(field!, { target: { value: '2020/11/17 13:03' } });

    await waitFor(() => {
      expect(field!.getAttribute('value')!).toEqual('2020/11/17 13:03');
    });
  });

  it('should show correct error', async () => {
    render(
      <ReactHookFormProvider
        options={{
          ...defaultFormOptions,
          defaultValues: {
            validDateStart: '2020-11-17T13:03Z',
          },
        }}
      >
        <ValidFrom
          productType="sigmet"
          productConfig={sigmetConfig}
          isDisabled={false}
        />
      </ReactHookFormProvider>,
    );

    const field = screen.getByRole('textbox');

    await waitFor(() => {
      expect(field!.getAttribute('value')!).toEqual('2020/11/17 13:03');
    });

    // in past
    fireEvent.change(field!, { target: { value: '2000/11/17 13:03' } });

    await waitFor(() => {
      expect(screen.getByText(VALID_FROM_ERROR_BEFORE_CURRENT)).toBeTruthy();
    });

    // before validity start
    fireEvent.change(field!, { target: { value: '2100/11/17 13:03' } });
    await waitFor(() => {
      expect(screen.getByText(getValidFromXHoursBeforeMessage(4))).toBeTruthy();
    });
  });

  it('should show the correct disabled input', async () => {
    render(
      <ReactHookFormProvider
        options={{
          ...defaultFormOptions,
          defaultValues: {
            validDateStart: '2000-11-17T13:03+00:00',
          },
        }}
      >
        <ValidFrom
          productType="sigmet"
          productConfig={sigmetConfig}
          isDisabled
        />
      </ReactHookFormProvider>,
    );

    const field = screen.getByRole('textbox');

    await waitFor(() => {
      expect(field).toBeTruthy();
    });
    expect(field.getAttribute('disabled')).toBeDefined();
  });

  it('should show the correct readonly input', () => {
    render(
      <ReactHookFormProvider>
        <ValidFrom
          productType="sigmet"
          productConfig={sigmetConfig}
          isReadOnly
          isDisabled
        />
      </ReactHookFormProvider>,
    );

    expect(screen.getByText('Date and time')).toBeTruthy();
  });

  describe('getDefaultValidFromValue', () => {
    const now = new Date('2023-02-03T14:00:00Z');
    jest.spyOn(dateUtils, 'utc').mockReturnValue(now);

    it('should add the default delay', () => {
      expect(getDefaultValidFromValue(sigmetConfig)).toBe(
        dateUtils.dateToString(
          dateUtils.add(now, {
            minutes: sigmetConfig.valid_from_delay_minutes,
          }),
        ),
      );
      expect(getDefaultValidFromValue(airmetConfig)).toBe(
        dateUtils.dateToString(
          dateUtils.add(now, {
            minutes: airmetConfig.valid_from_delay_minutes,
          }),
        ),
      );
    });
  });
});
