/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */
import React from 'react';
import { useFormContext } from 'react-hook-form';
import { dateUtils } from '@opengeoweb/shared';

import {
  ReactHookFormDateTime,
  isAfter,
  isValidDate,
  isXHoursAfter,
  useDraftFormHelpers,
} from '@opengeoweb/form-fields';

import {
  getDefaultValidityMinutesFromConfig,
  getFieldLabel,
  getMaxHoursOfValidity,
  getValidFromDelayTimeMinutesFromConfig,
  triggerValidations,
} from '../utils';
import { ConfigurableFormFieldProps, ProductConfig } from '../../../types';
import ProductFormFieldLayout from './ProductFormFieldLayout';
import { styles } from '../ProductForm.styles';

export const getDefaultValidUntilValue = (
  productConfig: ProductConfig,
): string => {
  const defaultValidityMinutes =
    getDefaultValidityMinutesFromConfig(productConfig);
  const validFromDelayTimeMinutes =
    getValidFromDelayTimeMinutesFromConfig(productConfig);
  return dateUtils.dateToString(
    dateUtils.add(dateUtils.utc(), {
      minutes: defaultValidityMinutes + validFromDelayTimeMinutes,
    }),
  );
};

const ValidUntil: React.FC<ConfigurableFormFieldProps> = ({
  productConfig,
  isDisabled,
  isReadOnly,
  onChange = (): void => {},
}: ConfigurableFormFieldProps) => {
  const { getValues, trigger } = useFormContext();
  const { isRequired } = useDraftFormHelpers();

  const label = getFieldLabel('Date and time', isReadOnly!);

  return (
    <ProductFormFieldLayout title="Valid until">
      <ReactHookFormDateTime
        disablePast
        name="validDateEnd"
        label={label}
        defaultNullValue={getDefaultValidUntilValue(productConfig)}
        rules={{
          validate: {
            isRequired,
            isValidDate,
            // Validity end has to be after validity start
            isAfter: (value): boolean | string =>
              isAfter(value, getValues('validDateStart')) ||
              'Valid until time has to be after Valid from time',
            // Validity end can be no more than 4 hours after validity start (6 for VA/TC SIGMET)
            isXHoursAfter: (value): boolean | string =>
              isXHoursAfter(
                value,
                getValues('validDateStart'),
                getMaxHoursOfValidity(
                  getValues('phenomenon'),
                  getValues('locationIndicatorATSR'),
                  productConfig,
                ),
              ) ||
              `Valid until time can be no more than ${getMaxHoursOfValidity(
                getValues('phenomenon'),
                getValues('locationIndicatorATSR'),
                productConfig,
              )} hours after Valid from time`,
          },
        }}
        disabled={isDisabled}
        isReadOnly={isReadOnly}
        onChange={(): void => {
          triggerValidations(
            ['validDateStart', 'observationOrForecastTime'],
            getValues,
            trigger,
          );
          onChange();
        }}
        sx={styles.helperText}
      />
    </ProductFormFieldLayout>
  );
};

export default ValidUntil;
