/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */
import React from 'react';
import { fireEvent, render, waitFor, screen } from '@testing-library/react';
import { ReactHookFormProvider } from '@opengeoweb/form-fields';

import Phenomenon from './Phenomenon';
import { SigmetPhenomena } from '../../../types';

describe('components/ProductForms/ProductFormFields/Phenomenon', () => {
  it('should be possible to select a sigmet phenomenon', async () => {
    render(
      <ReactHookFormProvider
        options={{
          defaultValues: {
            phenomenon: 'EMBD_TS',
          },
        }}
      >
        <Phenomenon productType="sigmet" isDisabled={false} />
      </ReactHookFormProvider>,
    );
    fireEvent.mouseDown(screen.getByRole('button'));

    const menuItem = await screen.findByText('Radioactive cloud');
    fireEvent.click(menuItem);

    await waitFor(() => {
      expect(screen.getByLabelText('Select phenomenon').textContent).toEqual(
        'Radioactive cloud',
      );
    });
  });

  it('should be possible to select an airmet phenomenon', async () => {
    render(
      <ReactHookFormProvider
        options={{
          defaultValues: {
            phenomenon: 'BKN_CLD',
          },
        }}
      >
        <Phenomenon productType="airmet" isDisabled={false} />
      </ReactHookFormProvider>,
    );
    fireEvent.mouseDown(screen.getByRole('button'));

    const menuItem = await screen.findByText('Surface wind');
    fireEvent.click(menuItem);

    await waitFor(() => {
      expect(screen.getByLabelText('Select phenomenon').textContent).toEqual(
        'Surface wind',
      );
    });
  });

  it('should show the phenomenon as disabled', () => {
    render(
      <ReactHookFormProvider
        options={{
          defaultValues: {
            phenomenon: 'EMBD_TS',
          },
        }}
      >
        <Phenomenon productType="sigmet" isDisabled />
      </ReactHookFormProvider>,
    );
    const phenomenon = screen.getByLabelText('Select phenomenon');
    expect(phenomenon.textContent).toEqual(SigmetPhenomena['EMBD_TS']);
    expect(phenomenon.getAttribute('aria-disabled') === 'true').toBeTruthy();
  });

  it('should show the phenomenon as readOnly', async () => {
    render(
      <ReactHookFormProvider
        options={{
          defaultValues: {
            phenomenon: 'EMBD_TS',
          },
        }}
      >
        <Phenomenon productType="sigmet" isDisabled isReadOnly />
      </ReactHookFormProvider>,
    );
    const phenomenon = screen.getByLabelText('Phenomenon');
    expect(phenomenon.textContent).toEqual(SigmetPhenomena['EMBD_TS']);
    expect(phenomenon.getAttribute('aria-disabled') === 'true').toBeTruthy();
  });
});
