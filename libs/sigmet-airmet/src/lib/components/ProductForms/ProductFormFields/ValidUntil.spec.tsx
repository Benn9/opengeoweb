/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */
import React from 'react';
import { fireEvent, render, waitFor, screen } from '@testing-library/react';
import { dateUtils } from '@opengeoweb/shared';

import {
  ReactHookFormProvider,
  defaultFormOptions,
} from '@opengeoweb/form-fields';

import ValidUntil from './ValidUntil';
import ValidFrom from './ValidFrom';
import { sigmetConfig, airmetConfig } from '../../../utils/config';

describe('components/ProductForms/ProductFormFields/ValidUntil', () => {
  const delayTime = sigmetConfig.valid_from_delay_minutes;
  it('should show current time +3hour and 30mins if no date passed for SIGMET', async () => {
    const now = new Date('2023-02-03T14:00:00Z');
    jest.spyOn(dateUtils, 'utc').mockReturnValue(now);

    const timeToShow = dateUtils.add(now, {
      hours: 3,
      minutes: delayTime,
    });
    render(
      <ReactHookFormProvider>
        <ValidUntil productConfig={sigmetConfig} isDisabled={false} />
      </ReactHookFormProvider>,
    );
    const field = screen.getByRole('textbox');
    await waitFor(() => {
      expect(field.getAttribute('value')!).toEqual(
        dateUtils.dateToString(timeToShow, 'yyyy/MM/dd HH:mm'),
      );
    });
    expect(screen.getByText('Select date and time')).toBeTruthy();
  });
  it('should show current time +1hour and 30mins if no date passed for AIRMET', async () => {
    const timeToShow = dateUtils.add(dateUtils.utc(), {
      hours: 1,
      minutes: delayTime,
    });
    render(
      <ReactHookFormProvider>
        <ValidUntil productConfig={airmetConfig} isDisabled={false} />
      </ReactHookFormProvider>,
    );
    const field = screen.getByRole('textbox');
    await waitFor(() => {
      expect(field.getAttribute('value')!).toEqual(
        dateUtils.dateToString(timeToShow, 'yyyy/MM/dd HH:mm'),
      );
    });
  });
  it('should show passed in date', async () => {
    render(
      <ReactHookFormProvider
        options={{
          ...defaultFormOptions,
          defaultValues: {
            validDateEnd: '2020-11-17T15:03+00:00',
          },
        }}
      >
        <ValidUntil productConfig={sigmetConfig} isDisabled={false} />
      </ReactHookFormProvider>,
    );
    const field = screen.getByRole('textbox');
    await waitFor(() => {
      expect(field.getAttribute('value')!).toEqual('2020/11/17 15:03');
    });
  });

  it('should be possible to set a date', async () => {
    const timeToShow = dateUtils.add(dateUtils.utc(), {
      hours: 3,
      minutes: delayTime,
    });

    render(
      <ReactHookFormProvider>
        <ValidUntil productConfig={sigmetConfig} isDisabled={false} />
      </ReactHookFormProvider>,
    );
    const field = screen.getByRole('textbox');
    await waitFor(() => {
      expect(field.getAttribute('value')!).toEqual(
        dateUtils.dateToString(timeToShow, 'yyyy/MM/dd HH:mm'),
      );
    });
    fireEvent.change(field, { target: { value: '2020/11/17 13:03' } });
    await waitFor(() => {
      expect(field.getAttribute('value')!).toEqual('2020/11/17 13:03');
    });
  });
  it('should show correct error', async () => {
    render(
      <ReactHookFormProvider
        options={{
          ...defaultFormOptions,
          defaultValues: {
            validDateEnd: '2020-11-17T15:03+00:00',
            validDateStart: '2020-11-17T13:03+00:00',
          },
        }}
      >
        <ValidFrom productConfig={sigmetConfig} isDisabled={false} />
        <ValidUntil productConfig={sigmetConfig} isDisabled={false} />
      </ReactHookFormProvider>,
    );
    const validUntil = screen.getAllByRole('textbox')[1];
    await waitFor(() => {
      expect(validUntil.getAttribute('value')!).toEqual('2020/11/17 15:03');
    });

    // after validDateStart
    fireEvent.change(validUntil, {
      target: {
        value: '2000/11/17 15:03',
      },
    });

    await waitFor(() => {
      expect(
        screen.getByText('Valid until time has to be after Valid from time'),
      ).toBeTruthy();
    });

    // before valid time
    fireEvent.change(validUntil, { target: { value: '2300/11/17 15:03' } });
    await waitFor(() => {
      expect(
        screen.getByText(
          'Valid until time can be no more than 4 hours after Valid from time',
        ),
      ).toBeTruthy();
    });
  });
  it('should show the correct disabled input', async () => {
    render(
      <ReactHookFormProvider
        options={{
          ...defaultFormOptions,
          defaultValues: {
            validDateEnd: '2000-11-17T13:03+00:00',
          },
        }}
      >
        <ValidUntil productConfig={sigmetConfig} isDisabled />
      </ReactHookFormProvider>,
    );
    const field = screen.getByRole('textbox');
    await waitFor(() => {
      expect(field).toBeTruthy();
    });
    expect(field.getAttribute('disabled')).toBeDefined();
  });

  it('should show the correct readonly labels', async () => {
    render(
      <ReactHookFormProvider>
        <ValidUntil isReadOnly productConfig={sigmetConfig} isDisabled />
      </ReactHookFormProvider>,
    );
    expect(screen.getByText('Date and time')).toBeTruthy();
  });
});
