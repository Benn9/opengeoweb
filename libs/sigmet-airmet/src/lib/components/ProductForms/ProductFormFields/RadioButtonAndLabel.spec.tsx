/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */
import { render, screen } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import React from 'react';
import { RadioButtonAndLabel } from './RadioButtonAndLabel';

describe('components/ProductForms/ProductFormFields/RadioButtonAndLabel', () => {
  const user = userEvent.setup();

  it('should render successfully', () => {
    render(
      <RadioButtonAndLabel
        value="NC"
        label="No change"
        disabled={false}
        data-testid="change-NC"
      />,
    );
    expect(screen.getByRole('radio')).toBeTruthy();
  });

  it('should have pointer events: none on the label', () => {
    render(
      <RadioButtonAndLabel
        value="NC"
        label="No change"
        disabled={false}
        data-testid="change-NC"
      />,
    );

    const label = screen.getByTestId('change-NC');
    expect(label).toBeTruthy();

    expect(screen.getByRole('radio')).toBeTruthy();
    expect(
      (screen.queryByRole('radio') as HTMLInputElement).checked,
    ).toBeFalsy();
    expect(label.style.pointerEvents).toEqual('none');
  });

  it('should select radio button when clicking on the input', async () => {
    render(
      <RadioButtonAndLabel
        value="NC"
        label="No change"
        disabled={false}
        data-testid="change-NC"
      />,
    );

    expect(screen.getByRole('radio')).toBeTruthy();
    expect(
      (screen.queryByRole('radio') as HTMLInputElement).checked,
    ).toBeFalsy();

    await user.click(screen.getByRole('radio'));

    expect(
      (screen.getByRole('radio') as HTMLInputElement).checked,
    ).toBeTruthy();
  });
});
