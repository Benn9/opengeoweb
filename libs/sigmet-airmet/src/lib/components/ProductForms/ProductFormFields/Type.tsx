/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */
import React from 'react';
import { MenuItem } from '@mui/material';
import { useFormContext } from 'react-hook-form';
import { ReactHookFormSelect } from '@opengeoweb/form-fields';

import ProductFormFieldLayout from './ProductFormFieldLayout';
import { FormFieldProps, SigmetType } from '../../../types';
import { getFieldLabel, triggerValidations } from '../utils';

const Type: React.FC<FormFieldProps> = ({
  isReadOnly,
  isDisabled,
  onChange = (): void => {},
}: FormFieldProps) => {
  const { getValues, trigger } = useFormContext();
  const label = getFieldLabel('Type', isReadOnly!);

  return (
    <ProductFormFieldLayout title="Type">
      <ReactHookFormSelect
        name="type"
        label={label}
        disabled={isDisabled}
        isReadOnly={isReadOnly}
        size="small"
        data-testid="type"
        onChange={(): Promise<boolean> | void => {
          triggerValidations(
            ['validDateStart', 'validDateEnd'],
            getValues,
            trigger,
          );
          onChange();
        }}
        autoFocus
      >
        {Object.keys(SigmetType).map((key) => (
          <MenuItem value={key} key={key}>
            {SigmetType[key as keyof typeof SigmetType]}
          </MenuItem>
        ))}
      </ReactHookFormSelect>
    </ProductFormFieldLayout>
  );
};

export default Type;
