/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */
import React from 'react';
import {
  render,
  fireEvent,
  waitFor,
  screen,
  within,
} from '@testing-library/react';
import {
  ReactHookFormProvider,
  defaultFormOptions,
} from '@opengeoweb/form-fields';
import { dateUtils } from '@opengeoweb/shared';

import AirmetForm from './AirmetForm';
import { TestWrapper } from '../../../utils/testUtils';
import {
  fakeCancelAirmet,
  fakeDraftAirmet,
  fakePublishedAirmet,
  fakeAirmetList,
} from '../../../utils/mockdata/fakeAirmetList';
import { Airmet, AirmetPhenomena } from '../../../types';
import { exitDrawModeMessage } from '../ProductFormFields/StartGeometry';

describe('components/ProductForms/AirmetForm/AirmetForm', () => {
  it('should render successfully', async () => {
    render(
      <TestWrapper>
        <ReactHookFormProvider
          options={{
            ...defaultFormOptions,
            defaultValues: { ...fakeDraftAirmet },
          }}
        >
          <AirmetForm mode="edit" initialAirmet={fakeDraftAirmet} />
        </ReactHookFormProvider>
      </TestWrapper>,
    );

    expect(screen.getByTestId('phenomenon').textContent).toContain(
      AirmetPhenomena[
        fakeDraftAirmet.phenomenon as unknown as keyof typeof AirmetPhenomena
      ],
    );
  });
  it('should show no issue date for a new airmet', async () => {
    render(
      <TestWrapper>
        <ReactHookFormProvider
          options={{
            ...defaultFormOptions,
            defaultValues: {},
          }}
        >
          <AirmetForm mode="new" />
        </ReactHookFormProvider>
      </TestWrapper>,
    );

    expect(screen.getByText('(Not published)')).toBeTruthy();
  });

  it('should show no issue date for a draft airmet', async () => {
    render(
      <TestWrapper>
        <ReactHookFormProvider
          options={{
            ...defaultFormOptions,
            defaultValues: { ...fakeDraftAirmet },
          }}
        >
          <AirmetForm mode="edit" initialAirmet={fakeDraftAirmet} />
        </ReactHookFormProvider>
      </TestWrapper>,
    );
    expect(screen.getByText('(Not published)')).toBeTruthy();
  });

  it('should show issue date for a published airmet', async () => {
    render(
      <TestWrapper>
        <ReactHookFormProvider
          options={{
            ...defaultFormOptions,
            defaultValues: { ...fakePublishedAirmet },
          }}
        >
          <AirmetForm mode="edit" initialAirmet={fakePublishedAirmet} />
        </ReactHookFormProvider>
      </TestWrapper>,
    );

    expect(
      screen.getByText(
        `${dateUtils.dateToString(
          dateUtils.utc(fakePublishedAirmet.issueDate),
          'yyyy/MM/dd HH:mm',
        )} UTC`,
      ),
    ).toBeTruthy();
  });

  it('should show issue date for a cancel airmet', async () => {
    render(
      <TestWrapper>
        <ReactHookFormProvider
          options={{
            ...defaultFormOptions,
            defaultValues: { ...fakeCancelAirmet },
          }}
        >
          <AirmetForm
            mode="edit"
            isCancelAirmet
            initialCancelAirmet={fakeCancelAirmet}
          />
        </ReactHookFormProvider>
      </TestWrapper>,
    );

    expect(
      screen.getByText(
        `${dateUtils.dateToString(
          dateUtils.utc(fakeCancelAirmet.issueDate),
          'yyyy/MM/dd HH:mm',
        )} UTC`,
      ),
    ).toBeTruthy();
  });

  it('should show a helper text when entering draw mode', async () => {
    render(
      <TestWrapper>
        <ReactHookFormProvider options={defaultFormOptions}>
          <AirmetForm mode="new" />
        </ReactHookFormProvider>
      </TestWrapper>,
    );

    const startGeometryContainer = screen.getByTestId('startGeometry');

    const drawBtn = within(screen.getByTestId('drawtools-polygon')).getByRole(
      'radio',
    );
    fireEvent.click(drawBtn!);

    await waitFor(() =>
      expect(startGeometryContainer.textContent).toContain(exitDrawModeMessage),
    );
  });

  it('should disable form when entering draw mode', async () => {
    render(
      <TestWrapper>
        <ReactHookFormProvider options={defaultFormOptions}>
          <AirmetForm mode="new" />
        </ReactHookFormProvider>
      </TestWrapper>,
    );

    const drawBtn = within(screen.getByTestId('drawtools-polygon')).getByRole(
      'radio',
    );
    fireEvent.click(drawBtn);

    const inputWrapper = screen.getByTestId('isObservationOrForecast-OBS');

    await waitFor(() =>
      expect(
        within(inputWrapper).getByRole('radio').hasAttribute('disabled'),
      ).toBeTruthy(),
    );

    // enable form again by switching mode
    const deleteBtn = within(screen.getByTestId('drawtools-delete')).getByRole(
      'radio',
    );
    fireEvent.click(deleteBtn);

    await waitFor(() =>
      expect(
        within(inputWrapper).queryByRole('radio')!.hasAttribute('disabled'),
      ).toBeFalsy(),
    );
  });

  it('should show the visibility value and cause and not show Levels field in case of SFC_VIS phenomena', async () => {
    render(
      <TestWrapper>
        <ReactHookFormProvider
          options={{
            ...defaultFormOptions,
            defaultValues: { ...fakeAirmetList[1].airmet },
          }}
        >
          <AirmetForm
            mode="edit"
            initialAirmet={fakeAirmetList[1].airmet as Airmet}
          />
        </ReactHookFormProvider>
      </TestWrapper>,
    );

    await screen.findByText('Select visibility');
    await screen.findByText('Select cause');

    await waitFor(() => expect(screen.queryByTestId('levels-AT')).toBeFalsy());
  });
  it('should show the wind speed, unit and direction and not show Levels field in case of SFC_WIND phenomena', async () => {
    render(
      <TestWrapper>
        <ReactHookFormProvider
          options={{
            ...defaultFormOptions,
            defaultValues: { ...fakeAirmetList[5].airmet },
          }}
        >
          <AirmetForm
            mode="edit"
            initialAirmet={fakeAirmetList[5].airmet as Airmet}
          />
        </ReactHookFormProvider>
      </TestWrapper>,
    );

    await screen.findByText('Set wind direction');
    await screen.findByTestId('surfaceWind-windUnit');
    await screen.findByText('Set speed');

    await waitFor(() => expect(screen.queryByTestId('levels-AT')).toBeFalsy());
  });
  it('should show the cloud Levels fields and not show Levels fields in case of BKN_CLD phenomena', () => {
    render(
      <TestWrapper>
        <ReactHookFormProvider
          options={{
            ...defaultFormOptions,
            defaultValues: { ...fakeAirmetList[7].airmet },
          }}
        >
          <AirmetForm
            mode="edit"
            initialAirmet={fakeAirmetList[7].airmet as Airmet}
          />
        </ReactHookFormProvider>
      </TestWrapper>,
    );

    expect(screen.getByTestId('cloudLevels-Above')).toBeTruthy();
    expect(screen.getByTestId('cloudLevel-unit')).toBeTruthy();
    expect(screen.getByTestId('cloudLevel-value')).toBeTruthy();
    expect(screen.getByTestId('cloudLevels-SFC')).toBeTruthy();
    expect(screen.getByTestId('cloudLowerLevel-unit')).toBeTruthy();
    expect(screen.getByTestId('cloudLowerLevel-value')).toBeTruthy();
    expect(screen.queryByTestId('levels-AT')).toBeFalsy();
  });
  it('should show the cloud Levels fields and not show Levels fields in case of OVC_CLD phenomena', () => {
    render(
      <TestWrapper>
        <ReactHookFormProvider
          options={{
            ...defaultFormOptions,
            defaultValues: { ...fakeAirmetList[4].airmet },
          }}
        >
          <AirmetForm
            mode="edit"
            initialAirmet={fakeAirmetList[4].airmet as Airmet}
          />
        </ReactHookFormProvider>
      </TestWrapper>,
    );

    expect(screen.getByTestId('cloudLevels-Above')).toBeTruthy();
    expect(screen.getByTestId('cloudLevel-unit')).toBeTruthy();
    expect(screen.getByTestId('cloudLevel-value')).toBeTruthy();
    expect(screen.getByTestId('cloudLevels-SFC')).toBeTruthy();
    expect(screen.queryByTestId('cloudLowerLevel-unit')).toBeFalsy();
    expect(screen.queryByTestId('cloudLowerLevel-value')).toBeFalsy();
    expect(screen.queryByTestId('levels-AT')).toBeFalsy();
  });

  it('should be able to start drawing without provided geometries', async () => {
    const fakeDraftAirmetWithoutGeometries = {
      ...fakeDraftAirmet,
      startGeometry: null,
      startGeometryIntersect: null,
    };

    expect(fakeDraftAirmetWithoutGeometries.startGeometry).toBeNull();

    render(
      <TestWrapper>
        <ReactHookFormProvider
          options={{
            ...defaultFormOptions,
            defaultValues: { ...fakeDraftAirmetWithoutGeometries },
          }}
        >
          <AirmetForm
            mode="edit"
            initialAirmet={
              fakeDraftAirmetWithoutGeometries as unknown as Airmet
            }
          />
        </ReactHookFormProvider>
      </TestWrapper>,
    );

    const drawBtn = within(screen.getByTestId('drawtools-polygon')).getByRole(
      'radio',
    );
    fireEvent.click(drawBtn!);

    expect(screen.getByTestId('phenomenon').textContent).toContain(
      AirmetPhenomena[
        fakeDraftAirmetWithoutGeometries.phenomenon as unknown as keyof typeof AirmetPhenomena
      ],
    );
  });
});
