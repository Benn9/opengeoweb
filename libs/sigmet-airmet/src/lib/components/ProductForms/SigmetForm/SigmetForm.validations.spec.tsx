/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */
import React from 'react';
import {
  render,
  fireEvent,
  waitFor,
  screen,
  within,
} from '@testing-library/react';
import {
  ReactHookFormProvider,
  defaultFormOptions,
} from '@opengeoweb/form-fields';
import { dateUtils } from '@opengeoweb/shared';

import SigmetForm from './SigmetForm';
import { fakeSigmetList } from '../../../utils/mockdata/fakeSigmetList';
import { noTAC } from '../ProductFormTac';
import { TestWrapper } from '../../../utils/testUtils';
import { LevelUnits, Sigmet } from '../../../types';
import { ForecastTimeValidation } from '../ProductFormFields/ObservationForecastTime';
import { coordinatesEmptyMessage } from '../ProductFormFields/StartGeometry';
import { coordinatesEmptyMessage as coordinatesEmptyMessageEndGeometry } from '../ProductFormFields/Progress';
import { getFir } from '../utils';
import { sigmetConfig } from '../../../utils/config';

describe('components/SigmetForm/SigmetForm - validations', () => {
  it('should show an error message when the start position drawing is removed', async () => {
    render(
      <TestWrapper>
        <ReactHookFormProvider
          options={{
            ...defaultFormOptions,
            defaultValues: { ...fakeSigmetList[0].sigmet },
          }}
        >
          <SigmetForm
            mode="edit"
            initialSigmet={fakeSigmetList[0].sigmet as Sigmet}
          />
        </ReactHookFormProvider>
      </TestWrapper>,
    );
    await waitFor(() => expect(screen.queryByText(noTAC)).toBeFalsy());

    const deleteButtonStartGeometry = within(
      within(screen.getByTestId('startGeometry')).getByTestId(
        'drawtools-delete',
      ),
    ).getByRole('radio');

    fireEvent.click(deleteButtonStartGeometry!);
    await screen.findByText(coordinatesEmptyMessage);
  });

  it('should remove the error message for start position when selecting fir', async () => {
    render(
      <TestWrapper>
        <ReactHookFormProvider
          options={{
            ...defaultFormOptions,
            defaultValues: { ...fakeSigmetList[0].sigmet },
          }}
        >
          <SigmetForm
            mode="edit"
            initialSigmet={fakeSigmetList[0].sigmet as Sigmet}
          />
        </ReactHookFormProvider>
      </TestWrapper>,
    );
    await waitFor(() => expect(screen.queryByText(noTAC)).toBeFalsy());

    const deleteButtonStartGeometry = within(
      within(screen.getByTestId('startGeometry')).getByTestId(
        'drawtools-delete',
      ),
    ).getByRole('radio');
    fireEvent.click(deleteButtonStartGeometry!);

    await screen.findByText(coordinatesEmptyMessage);

    const firButtonStartGeometry = within(
      within(screen.getByTestId('startGeometry')).getByTestId('drawtools-fir'),
    ).getByRole('radio');
    fireEvent.click(firButtonStartGeometry!);

    await waitFor(() =>
      expect(screen.queryByText(coordinatesEmptyMessage)).toBeFalsy(),
    );
  });

  it('should show an error message when progress is End position and the end position drawing is removed', async () => {
    render(
      <TestWrapper>
        <ReactHookFormProvider
          options={{
            ...defaultFormOptions,
            defaultValues: { ...fakeSigmetList[0].sigmet },
          }}
        >
          <SigmetForm
            mode="edit"
            initialSigmet={fakeSigmetList[0].sigmet as Sigmet}
          />
        </ReactHookFormProvider>
      </TestWrapper>,
    );

    await waitFor(() => expect(screen.queryByText(noTAC)).toBeFalsy());

    expect(
      (
        screen.getByTestId('movementType-FORECAST_POSITION')
          .firstChild as HTMLElement
      ).classList,
    ).toContain('Mui-checked');

    const deleteButtonEndGeometry = within(
      within(screen.getByTestId('endGeometry')).getByTestId('drawtools-delete'),
    ).getByRole('radio');

    fireEvent.click(deleteButtonEndGeometry!);

    await screen.findByText(coordinatesEmptyMessageEndGeometry);
  });

  it('should show an error message for begin and end position after selecting a FIR location', async () => {
    render(
      <TestWrapper>
        <ReactHookFormProvider
          options={{
            ...defaultFormOptions,
            defaultValues: { ...fakeSigmetList[0].sigmet },
          }}
        >
          <SigmetForm
            mode="edit"
            initialSigmet={fakeSigmetList[0].sigmet as Sigmet}
          />
        </ReactHookFormProvider>
      </TestWrapper>,
    );

    await waitFor(() => expect(screen.queryByText(noTAC)).toBeFalsy());

    expect(screen.queryByText(coordinatesEmptyMessage)).toBeNull();
    expect(screen.queryByText(coordinatesEmptyMessageEndGeometry)).toBeNull();

    // change fir location
    fireEvent.mouseDown(await screen.findByLabelText('Select FIR'));
    const menuItem = await screen.findAllByText('BRUSSEL FIR');
    fireEvent.click(menuItem[0]);

    await screen.findByText(coordinatesEmptyMessage);
    await screen.findByText(coordinatesEmptyMessageEndGeometry);
  });

  it('should remove the error message for end position when selecting fir', async () => {
    render(
      <TestWrapper>
        <ReactHookFormProvider
          options={{
            ...defaultFormOptions,
            defaultValues: { ...fakeSigmetList[0].sigmet },
          }}
        >
          <SigmetForm
            mode="edit"
            initialSigmet={fakeSigmetList[0].sigmet as Sigmet}
          />
        </ReactHookFormProvider>
      </TestWrapper>,
    );

    await waitFor(() => expect(screen.queryByText(noTAC)).toBeFalsy());

    const deleteButtonEndGeometry = within(
      within(screen.getByTestId('endGeometry')).getByTestId('drawtools-delete'),
    ).getByRole('radio');
    fireEvent.click(deleteButtonEndGeometry!);

    await screen.findByText(coordinatesEmptyMessageEndGeometry);

    const firButtonEndGeometry = within(
      within(screen.getByTestId('endGeometry')).getByTestId('drawtools-fir'),
    ).getByRole('radio');
    fireEvent.click(firButtonEndGeometry!);

    await waitFor(() =>
      expect(
        screen.queryByText(coordinatesEmptyMessageEndGeometry),
      ).toBeFalsy(),
    );
  });

  it('should show an error message when choosing forecast and forecast time is before valid from time', async () => {
    render(
      <TestWrapper>
        <ReactHookFormProvider
          options={{
            ...defaultFormOptions,
            defaultValues: { ...fakeSigmetList[0].sigmet },
          }}
        >
          <SigmetForm
            mode="edit"
            initialSigmet={fakeSigmetList[0].sigmet as Sigmet}
          />
        </ReactHookFormProvider>
      </TestWrapper>,
    );
    await waitFor(() => expect(screen.queryByText(noTAC)).toBeFalsy());

    const fcstField = screen.getByTestId('isObservationOrForecast-FCST');
    fireEvent.click(fcstField);

    await screen.findByText(ForecastTimeValidation);

    expect(
      within(screen.getByTestId('obs-fcst-time'))
        .getByRole('textbox')
        .getAttribute('aria-invalid') === 'true',
    ).toBeTruthy();
  });

  it('should show an error message when changing validFrom to be after validUntil', async () => {
    render(
      <TestWrapper>
        <ReactHookFormProvider
          options={{
            ...defaultFormOptions,
            defaultValues: { ...fakeSigmetList[0].sigmet },
          }}
        >
          <SigmetForm
            mode="edit"
            initialSigmet={fakeSigmetList[0].sigmet as Sigmet}
          />
        </ReactHookFormProvider>
      </TestWrapper>,
    );
    await waitFor(() => expect(screen.queryByText(noTAC)).toBeFalsy());

    // change validFrom
    const time = dateUtils.add(dateUtils.utc(), {
      hours: 3,
    });

    const validFrom = within(screen.getByTestId('valid-from')).getByRole(
      'textbox',
    );
    fireEvent.change(validFrom, {
      target: { value: dateUtils.dateToString(time, 'yyyy/MM/dd HH:mm') },
    });
    await waitFor(() => {
      expect(validFrom.getAttribute('value')!).toEqual(
        dateUtils.dateToString(time, 'yyyy/MM/dd HH:mm'),
      );
    });

    // check error message
    await waitFor(() => {
      expect(
        screen.getByText('Valid until time has to be after Valid from time'),
      ).toBeTruthy();
    });
  });

  it('should show an error message for startdate, enddate, movementSpeed, upperLevel and lowerLevel after changing FIR location', async () => {
    const testFirArea = getFir(sigmetConfig);
    const mockConfigMultipleFIR = {
      location_indicator_mwo: 'EHDB',
      fir_areas: {
        EHAA: {
          fir_name: 'AMSTERDAM FIR',
          fir_location: testFirArea,
          location_indicator_atsr: 'EHAA',
          location_indicator_atsu: 'EHAA',
          area_preset: 'NL_FIR',
          max_hours_of_validity: 4,
          hours_before_validity: 4,
          level_min: {
            FT: 100,
            FL: 50,
          },
          level_max: {
            FT: 4900,
            FL: 650,
          },
          level_rounding_FL: 100,
          level_rounding_FT: 5,
          level_rounding_M: 1,
          movement_min: {
            KT: 5,
            KMH: 10,
          },
          movement_max: {
            KT: 150,
            KMH: 99,
          },
          movement_rounding_kt: 5,
          movement_rounding_kmh: 10,
          units: [
            {
              unit_type: 'level_unit',
              allowed_units: ['FT', 'M'],
            },
            {
              unit_type: 'movement_unit',
              allowed_units: ['KT'],
            },
          ],
        },
        EBBU: {
          fir_name: 'OTHER FIR',
          fir_location: testFirArea,
          location_indicator_atsr: 'EBBU',
          location_indicator_atsu: 'EBBU',
          area_preset: 'OTHER_FIR',
          max_hours_of_validity: 2,
          hours_before_validity: 1,
          level_min: {
            FT: 200,
            FL: 50,
          },
          level_max: {
            FT: 4000,
            FL: 650,
          },
          level_rounding_FL: 100,
          level_rounding_FT: 5,
          level_rounding_M: 1,
          movement_min: {
            KT: 15,
            KMH: 10,
          },
          movement_max: {
            KT: 150,
            KMH: 99,
          },
          movement_rounding_kt: 5,
          movement_rounding_kmh: 10,
          units: [
            {
              unit_type: 'level_unit',
              allowed_units: ['FT', 'FL'],
            },
            {
              unit_type: 'movement_unit',
              allowed_units: ['KT'],
            },
          ],
        },
      },
      valid_from_delay_minutes: 45,
      default_validity_minutes: 90,
      active_firs: ['EHAA', 'EBBU'],
    };
    const fakeSigmet = {
      ...fakeSigmetList[0].sigmet,
      validDateStart: dateUtils.dateToString(
        dateUtils.add(dateUtils.utc(), { hours: 2 }),
      ),
      validDateEnd: dateUtils.dateToString(
        dateUtils.add(dateUtils.utc(), { hours: 5 }),
      ),
      movementType: 'MOVEMENT',
      movementSpeed: 10,
      levelInfoMode: 'BETW',
      level: {
        value: 4500,
        unit: 'FT' as LevelUnits,
      },
      lowerLevel: {
        value: 150,
        unit: 'FT' as LevelUnits,
      },
    } as Sigmet;

    render(
      <TestWrapper>
        <ReactHookFormProvider
          options={{
            ...defaultFormOptions,
            defaultValues: { ...fakeSigmet },
          }}
        >
          <SigmetForm
            mode="edit"
            initialSigmet={fakeSigmet}
            productConfig={mockConfigMultipleFIR}
          />
        </ReactHookFormProvider>
      </TestWrapper>,
    );

    await waitFor(() => expect(screen.queryByText(noTAC)).toBeFalsy());

    // change fir location
    fireEvent.mouseDown(await screen.findByLabelText('Select FIR'));
    const menuItem = await screen.findAllByText('OTHER FIR');
    fireEvent.click(menuItem[0]);

    await waitFor(() => {
      expect(
        screen
          .queryAllByRole('textbox')
          .filter((field) => field.getAttribute('aria-invalid') === 'true'),
      ).not.toHaveLength(0);
    });

    expect(
      screen.getByText(
        'Valid from time can be no more than 1 hours after current time',
      ),
    ).toBeTruthy();
    expect(
      screen.getByText(
        'Valid until time can be no more than 2 hours after Valid from time',
      ),
    ).toBeTruthy();
    expect(screen.getByText('The minimum level in kt is 15')).toBeTruthy();
    expect(screen.getByText('The minimum level in ft is 200')).toBeTruthy();
    expect(screen.getByText('The maximum level in ft is 4000')).toBeTruthy();

    // change fir location back
    fireEvent.mouseDown(await screen.findByLabelText('Select FIR'));
    const menuItem1 = await screen.findByText('AMSTERDAM FIR');
    fireEvent.click(menuItem1);

    await waitFor(() => {
      expect(
        screen.queryByText(
          'Valid from time can be no more than 1 hours after current time',
        ),
      ).toBeFalsy();
    });
    expect(
      screen.queryByText(
        'Valid until time can be no more than 2 hours after Valid from time',
      ),
    ).toBeFalsy();
    expect(screen.queryByText('The minimum level in kt is 15')).toBeFalsy();
    expect(screen.queryByText('The minimum level in ft is 200')).toBeFalsy();
    expect(screen.queryByText('The maximum level in ft is 4000')).toBeFalsy();
  });
});
