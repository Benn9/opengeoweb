/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */
import {
  defaultFormOptions,
  ReactHookFormProvider,
} from '@opengeoweb/form-fields';
import * as React from 'react';
import { useFormContext } from 'react-hook-form';
import {
  CancelSigmet,
  LevelUnits,
  Sigmet,
  SigmetPhenomena,
} from '../../../types';
import { sigmetConfig } from '../../../utils/config';
import {
  fakeSigmetList,
  fakeVolcanicCancelSigmetWithMoveTo,
} from '../../../utils/mockdata/fakeSigmetList';

import {
  SnapshotStoryWrapper,
  StoryWrapperFakeApi,
} from '../../../utils/testUtils';
import {
  getValidFromXHoursBeforeMessage,
  VALID_FROM_ERROR_BEFORE_CURRENT,
} from '../ProductFormFields/ValidFrom';
import { getFir } from '../utils';
import SigmetForm from './SigmetForm';

export default {
  title: 'components/SigmetForm',
};

const zeplinThemeLinkLight = [
  {
    name: 'Light theme',
    link: 'https://app.zeplin.io/project/5ecf84a3c6ae1047a368f393/screen/61a4934891039316017011cd/version/63453b21d90f056999965f74',
  },
];

const zeplinThemeLinkDark = [
  {
    name: 'Dark theme',
    link: 'https://app.zeplin.io/project/5ecf84a3c6ae1047a368f393/screen/61a4935476031e13fb0f3ab6/version/63453b3bb0834e6a236fa210',
  },
];

interface DemoComponentProps {
  isDarkTheme?: boolean;
}

// New sigmet
const NewSigmet = ({
  isDarkTheme = false,
}: DemoComponentProps): React.ReactElement => {
  return (
    <StoryWrapperFakeApi isDarkTheme={isDarkTheme}>
      <SnapshotStoryWrapper>
        <ReactHookFormProvider
          options={{
            ...defaultFormOptions,
            defaultValues: {
              validDateStart: '2022-01-01T12:00Z',
              validDateEnd: '2022-01-01T13:00Z',
            },
          }}
        >
          <SigmetForm mode="new" showMap={false} />
        </ReactHookFormProvider>
      </SnapshotStoryWrapper>
    </StoryWrapperFakeApi>
  );
};

export const NewSigmetLightTheme = (): React.ReactElement => <NewSigmet />;
NewSigmetLightTheme.storyName = 'New Sigmet light theme (takeSnapshot)';
NewSigmetLightTheme.parameters = { zeplinLink: zeplinThemeLinkLight };

export const NewSigmetDarkTheme = (): React.ReactElement => (
  <NewSigmet isDarkTheme />
);
NewSigmetDarkTheme.storyName = 'New Sigmet dark theme (takeSnapshot)';
NewSigmetDarkTheme.parameters = { zeplinLink: zeplinThemeLinkDark };

// Error Sigmet
const DemoForm = (): React.ReactElement => {
  const { setError } = useFormContext();

  React.useEffect(() => {
    setTimeout(() => {
      setError('phenomenon', { message: 'this field is required' });
      setError('validDateStart', {
        message: VALID_FROM_ERROR_BEFORE_CURRENT,
      });
      setError('validDateEnd', {
        message: getValidFromXHoursBeforeMessage(4),
      });
    }, 1);
  }, [setError]);

  return <SigmetForm mode="new" showMap={false} />;
};

const ErrorSigmet = ({
  isDarkTheme = false,
}: DemoComponentProps): React.ReactElement => {
  return (
    <StoryWrapperFakeApi isDarkTheme={isDarkTheme}>
      <SnapshotStoryWrapper>
        <ReactHookFormProvider
          options={{
            ...defaultFormOptions,
            defaultValues: {
              validDateStart: '2022-01-01T13:00Z',
              validDateEnd: '2022-01-01T12:00Z',
            },
          }}
        >
          <DemoForm />
        </ReactHookFormProvider>
      </SnapshotStoryWrapper>
    </StoryWrapperFakeApi>
  );
};
export const ErrorSigmetLightTheme = (): React.ReactElement => <ErrorSigmet />;
ErrorSigmetLightTheme.storyName = 'Error Sigmet light theme (takeSnapshot)';
ErrorSigmetLightTheme.parameters = { zeplinLink: zeplinThemeLinkLight };

export const ErrorSigmetDarkTheme = (): React.ReactElement => (
  <ErrorSigmet isDarkTheme />
);
ErrorSigmetDarkTheme.storyName = 'Error Sigmet dark theme (takeSnapshot)';
ErrorSigmetDarkTheme.parameters = { zeplinLink: zeplinThemeLinkDark };

// Edit Sigmet
const VASigmet = {
  uuid: 'someuniqueidprescibedbyBE3',
  phenomenon: 'VA_CLD' as SigmetPhenomena,
  sequence: 'A02V',
  issueDate: '2022-01-02T12:35Z',
  validDateStart: '2022-01-02T11:35Z',
  validDateEnd: '2022-01-02T12:35Z',
  firName: 'AMSTERDAM FIR',
  locationIndicatorATSU: 'EHAA',
  locationIndicatorATSR: 'EHAA',
  locationIndicatorMWO: 'EHDB',
  isObservationOrForecast: 'OBS',
  observationOrForecastTime: '2022-01-02T12:35Z',
  movementType: 'NO_VA_EXP',
  change: 'WKN',
  type: 'NORMAL',
  status: 'PUBLISHED',
  levelInfoMode: 'BETW',
  vaSigmetVolcanoName: 'EYJAFJALLAJOKULL',
  vaSigmetVolcanoCoordinates: { latitude: 63.62, longitude: -19.61 },
  level: {
    value: 1500,
    unit: 'FT' as LevelUnits,
  },
  lowerLevel: {
    value: 1000,
    unit: 'FT' as LevelUnits,
  },
  firGeometry: getFir(sigmetConfig),
  startGeometry: {
    type: 'FeatureCollection',
    features: [
      {
        type: 'Feature',
        properties: {
          selectionType: 'poly',
        },
        geometry: {
          type: 'Polygon',
          coordinates: [
            [
              [3.7513346922388284, 52.93209131750574],
              [4.890169687103436, 52.26807619123409],
              [3.250247294498403, 51.960365357854286],
              [3.7513346922388284, 52.93209131750574],
            ],
          ],
        },
      },
    ],
  },
  startGeometryIntersect: {
    type: 'FeatureCollection',
    features: [
      {
        type: 'Feature',
        properties: {
          selectionType: 'poly',
        },
        geometry: {
          type: 'Polygon',
          coordinates: [
            [
              [3.7513346922388284, 52.93209131750574],
              [4.890169687103436, 52.26807619123409],
              [3.250247294498403, 51.960365357854286],
              [3.7513346922388284, 52.93209131750574],
            ],
          ],
        },
      },
    ],
  },
};

const EditSigmet = ({
  isDarkTheme,
}: DemoComponentProps): React.ReactElement => {
  return (
    <StoryWrapperFakeApi isDarkTheme={isDarkTheme}>
      <SnapshotStoryWrapper>
        <ReactHookFormProvider
          options={{
            ...defaultFormOptions,
            defaultValues: {
              ...VASigmet,
            },
          }}
        >
          <SigmetForm
            mode="edit"
            showMap={false}
            initialSigmet={VASigmet as Sigmet}
          />
        </ReactHookFormProvider>
      </SnapshotStoryWrapper>
    </StoryWrapperFakeApi>
  );
};

export const EditSigmetLightTheme = (): React.ReactElement => <EditSigmet />;
EditSigmetLightTheme.storyName = 'Edit Sigmet light theme (takeSnapshot)';
EditSigmetLightTheme.parameters = { zeplinLink: zeplinThemeLinkLight };

export const EditSigmetDarkTheme = (): React.ReactElement => (
  <EditSigmet isDarkTheme />
);
EditSigmetDarkTheme.storyName = 'Edit Sigmet dark theme (takeSnapshot)';
EditSigmetDarkTheme.parameters = { zeplinLink: zeplinThemeLinkDark };

// View sigmet
const ViewSigmet = ({
  isDarkTheme,
}: DemoComponentProps): React.ReactElement => {
  return (
    <StoryWrapperFakeApi isDarkTheme={isDarkTheme}>
      <SnapshotStoryWrapper>
        <ReactHookFormProvider
          options={{
            ...defaultFormOptions,
            defaultValues: {
              ...fakeSigmetList[7].sigmet,
            },
          }}
        >
          <SigmetForm
            mode="view"
            showMap={false}
            initialSigmet={fakeSigmetList[7].sigmet as Sigmet}
          />
        </ReactHookFormProvider>
      </SnapshotStoryWrapper>
    </StoryWrapperFakeApi>
  );
};

export const ViewSigmetLightTheme = (): React.ReactElement => <ViewSigmet />;
ViewSigmetLightTheme.storyName = 'View Sigmet light theme (takeSnapshot)';
ViewSigmetLightTheme.parameters = { zeplinLink: zeplinThemeLinkLight };

export const ViewSigmetDarkTheme = (): React.ReactElement => (
  <ViewSigmet isDarkTheme />
);
ViewSigmetDarkTheme.storyName = 'View Sigmet dark theme (takeSnapshot)';
ViewSigmetDarkTheme.parameters = { zeplinLink: zeplinThemeLinkLight };

// Cancel sigmet
const CancelVASigmet = ({
  isDarkTheme,
}: DemoComponentProps): React.ReactElement => {
  return (
    <StoryWrapperFakeApi isDarkTheme={isDarkTheme}>
      <SnapshotStoryWrapper>
        <ReactHookFormProvider
          options={{
            ...defaultFormOptions,
            defaultValues: {
              ...fakeVolcanicCancelSigmetWithMoveTo.sigmet,
            },
          }}
        >
          <SigmetForm
            mode="view"
            showMap={false}
            isCancelSigmet={true}
            initialCancelSigmet={
              fakeVolcanicCancelSigmetWithMoveTo.sigmet as CancelSigmet
            }
          />
        </ReactHookFormProvider>
      </SnapshotStoryWrapper>
    </StoryWrapperFakeApi>
  );
};

export const CancelVASigmetLightTheme = (): React.ReactElement => (
  <CancelVASigmet />
);
CancelVASigmetLightTheme.storyName =
  'Cancel volcanic ashes Sigmet light theme (takeSnapshot)';
CancelVASigmetLightTheme.parameters = { zeplinLink: zeplinThemeLinkLight };

export const CancelVASigmetDarkTheme = (): React.ReactElement => (
  <CancelVASigmet isDarkTheme />
);
CancelVASigmetDarkTheme.storyName =
  'Cancel volcanic ashes Sigmet dark theme (takeSnapshot)';
CancelVASigmetDarkTheme.parameters = { zeplinLink: zeplinThemeLinkDark };
