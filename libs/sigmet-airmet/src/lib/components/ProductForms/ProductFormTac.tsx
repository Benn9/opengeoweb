/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */
import { debounce } from 'lodash';
import { Box, Typography } from '@mui/material';
import React from 'react';
import { useIsMounted } from '@opengeoweb/shared';
import {
  CancelSigmet,
  Change,
  isInstanceOfCancelSigmetOrAirmet,
  isInstanceOfSigmetOrAirmet,
  SigmetMovementType,
  ObservationOrForcast,
  SigmetPhenomena,
  Sigmet,
  Airmet,
  CancelAirmet,
  AirmetPhenomena,
  AirmetMovementType,
} from '../../types';
import { prepareFormValues } from './utils';

type AviationProduct = Sigmet | CancelSigmet | Airmet | CancelAirmet;

export const noTAC = 'Missing data: no TAC can be generated';

export const isLevelFieldsComplete = (product: Sigmet | Airmet): boolean => {
  switch (product.phenomenon) {
    // For certain AIRMETs no levels are provided
    case 'SFC_VIS' as AirmetPhenomena:
    case 'SFC_WIND' as AirmetPhenomena:
      return true;
    // For certain AIRMETs we have cloudlevels
    case 'OVC_CLD' as AirmetPhenomena:
    case 'BKN_CLD' as AirmetPhenomena: {
      if (!product.cloudLevelInfoMode) {
        return false;
      }
      const cloudLevelMode = product.cloudLevelInfoMode;
      const levelNeeded =
        cloudLevelMode === 'BETW_SFC_ABV' || cloudLevelMode === 'BETW_SFC';
      const levelAndLowerLevelNeeded =
        cloudLevelMode === 'BETW' || cloudLevelMode === 'BETW_ABV';
      if (
        (levelAndLowerLevelNeeded &&
          (!product.cloudLevel?.value || !product.cloudLowerLevel?.value)) ||
        (levelNeeded && !product.cloudLevel?.value)
      ) {
        return false;
      }

      return true;
    }
    // Other AIRMETs and all SIGMETs
    default: {
      if (!product.levelInfoMode) {
        return false;
      }
      const levelMode = product.levelInfoMode;
      const levelNeeded =
        levelMode === 'AT' ||
        levelMode === 'ABV' ||
        levelMode === 'TOPS' ||
        levelMode === 'TOPS_ABV' ||
        levelMode === 'BETW_SFC';
      const levelAndLowerLevelNeeded = levelMode === 'BETW';
      if (
        (levelNeeded && !product.level?.value) ||
        (levelAndLowerLevelNeeded &&
          (!product.level?.value || !product.lowerLevel?.value))
      ) {
        return false;
      }
      return true;
    }
  }
};

/* Changes in intensity should always be included in sigmet except for
 * volcanic ash cloud and radioactive cloud where it should be omitted
 * if specified in the config
 */
export const isChangeComplete = (
  product: Sigmet | Airmet,
  omitChange?: boolean,
): boolean => {
  const omitChangeForVaAndRdoact =
    (omitChange && product.phenomenon === ('VA_CLD' as SigmetPhenomena)) ||
    (omitChange && product.phenomenon === ('RDOACT_CLD' as SigmetPhenomena));
  const isNotEmpty =
    !omitChangeForVaAndRdoact &&
    product.change !== undefined &&
    product.change !== null &&
    product.change !== ('' as Change);
  return omitChangeForVaAndRdoact || isNotEmpty;
};

export const isSurfaceVisComplete = (product: Sigmet | Airmet): boolean => {
  // For Surface Visibility the cause is required
  if (
    product.phenomenon === ('SFC_VIS' as AirmetPhenomena) &&
    !product.visibilityCause
  ) {
    return false;
  }
  return true;
};

// Ensure we have enough data to retrieve a TAC
export const shouldRetrieveTAC = (
  product: AviationProduct,
  omitChange?: boolean,
): boolean => {
  if (!product) {
    return false;
  }
  // always retrieve for a cancel product - this is always complete
  if (product && isInstanceOfCancelSigmetOrAirmet(product)) {
    return true;
  }
  if (
    product &&
    isInstanceOfSigmetOrAirmet(product) &&
    product.phenomenon &&
    product.phenomenon !== ('' as SigmetPhenomena | AirmetPhenomena) &&
    product.isObservationOrForecast &&
    product.isObservationOrForecast !== ('' as ObservationOrForcast) &&
    product.validDateStart &&
    product.validDateStart !== null &&
    product.validDateEnd &&
    product.validDateEnd !== null &&
    isLevelFieldsComplete(product) &&
    product.startGeometry &&
    product.startGeometry !== null &&
    product.startGeometryIntersect &&
    product.startGeometryIntersect !== null &&
    product.movementType &&
    product.movementType !== ('' as SigmetMovementType | AirmetMovementType) &&
    isChangeComplete(product, omitChange) &&
    isSurfaceVisComplete(product)
  ) {
    return true;
  }
  return false;
};

export const useTAC = (
  initialProduct: AviationProduct,
  getTAC: (product: AviationProduct) => Promise<{ data: string }>,
  hasErrors = (): boolean => false,
  omitChange?: boolean,
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
): [string, any] => {
  const [TAC, setTAC] = React.useState(noTAC);
  const { isMounted } = useIsMounted();

  const retrieveTAC = async (product: AviationProduct): Promise<void> => {
    try {
      const result = await getTAC(prepareFormValues(product));
      if (isMounted.current) {
        setTAC(result.data);
      }
    } catch (error) {
      if (isMounted.current) {
        setTAC(noTAC);
      }
    }
  };

  React.useEffect(() => {
    if (shouldRetrieveTAC(initialProduct, omitChange) && !hasErrors()) {
      retrieveTAC(initialProduct);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const update = (
    formValues: AviationProduct,
    hasFormErrors: () => boolean,
  ): void => {
    if (shouldRetrieveTAC(formValues, omitChange) && !hasFormErrors) {
      retrieveTAC(formValues);
    } else if (isMounted.current) {
      setTAC(noTAC);
    }
  };

  const debouncedSearch = React.useRef(
    debounce(async (formFn, hasFormErrors) => {
      update(formFn(), hasFormErrors());
    }, 1000),
  ).current;

  const delayedQuery = (formFn: () => AviationProduct): void => {
    debouncedSearch.cancel();
    debouncedSearch(formFn, hasErrors);
  };

  return [TAC, delayedQuery];
};

interface ProductFormTACProps {
  tac: string;
}
const ProductFormTAC: React.FC<ProductFormTACProps> = ({
  tac,
}: ProductFormTACProps) => {
  const tacLines = tac.split('\n');
  const hasTac = tacLines.length && tacLines.toString() !== noTAC;

  return (
    <Typography
      variant="body2"
      component="div"
      sx={{
        paddingTop: '16px',
        whiteSpace: 'break-spaces',
      }}
      data-testid="productform-tac-message"
    >
      {tacLines.map((line) => (
        <Box
          component="p"
          key={line}
          sx={{
            margin: 0,
            fontSize: hasTac ? '1.125rem' : '1rem',
            fontWeight: 'normal',
            fontStretch: 'normal',
            fontStyle: 'normal',
            lineHeight: 1.56,
            letterSpacing: '0.5px',
          }}
          data-testid="productform-tac-message-line"
        >
          {line}
        </Box>
      ))}
    </Typography>
  );
};

export default ProductFormTAC;
