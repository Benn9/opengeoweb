/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */
import * as React from 'react';
import { Box, Button } from '@mui/material';
import { FieldValues, UseFormGetValues, UseFormTrigger } from 'react-hook-form';
import { ToggleMenu, useIsMounted, dateUtils } from '@opengeoweb/shared';
import { layerTypes, mapTypes } from '@opengeoweb/store';
import { DrawFIRLand } from '@opengeoweb/theme';
import { LayerType } from '@opengeoweb/webmap';
import {
  DRAWMODE,
  DrawMode,
  addFeatureProperties,
  defaultBox,
  defaultDelete,
  defaultPoint,
  defaultPolygon,
  getIcon,
} from '@opengeoweb/webmap-react';
import {
  LevelUnits,
  ATSRLocation,
  ICAOLocation,
  ProductStatus,
  CancelSigmet,
  Sigmet,
  ProductStatusDescription,
  SigmetPhenomena,
  isInstanceOfCancelSigmet,
  ProductCanbe,
  SigmetFromBackend,
  SigmetFromFrontend,
  ProductActions,
  Airmet,
  CancelAirmet,
  ProductConfig,
  Firareas,
  FIRConfigSigmet,
  AirmetFromBackend,
  AirmetFromFrontend,
  ProductType,
  isInstanceOfCancelSigmetOrAirmet,
  AirmetPhenomena,
  AviationProduct,
  FIRLocationGeoJson,
  AirmetConfig,
  FIRConfigAirmet,
  StartOrEndDrawing,
  CloudLevelMinMaxType,
  SurfaceWindMinMaxType,
  MovementMinMaxType,
  LevelMinMaxType,
} from '../../types';
import {
  featurePropsEnd,
  featurePropsStart,
} from '../MapViewGeoJson/constants';

export const getHoursBeforeValidity = (
  phenomenon: string,
  fir: string,
  config: ProductConfig,
): number => {
  if (!fir) {
    return 4;
  }
  const {
    /* eslint-disable @typescript-eslint/naming-convention */
    hours_before_validity,
    va_hours_before_validity,
    tc_hours_before_validity,
  } = config.fir_areas[fir] as FIRConfigSigmet;

  switch (phenomenon) {
    // Tropical Cyclone
    case 'TC':
      return tc_hours_before_validity;
    // Volcanic ash cloud
    case 'VA_CLD':
      return va_hours_before_validity;
    default:
      return hours_before_validity;
  }
};

export const getMaxHoursOfValidity = (
  phenomenon: string,
  fir: string,
  config: ProductConfig,
): number => {
  if (!fir) {
    return 4;
  }
  const {
    /* eslint-disable @typescript-eslint/naming-convention */
    max_hours_of_validity,
    va_max_hours_of_validity,
    tc_max_hours_of_validity,
  } = config.fir_areas[fir] as FIRConfigSigmet;

  switch (phenomenon) {
    // Tropical Cyclone
    case 'TC':
      return tc_max_hours_of_validity;
    // Volcanic ash cloud
    case 'VA_CLD':
      return va_max_hours_of_validity;
    default:
      return max_hours_of_validity;
  }
};

export const getValidFromDelayTimeMinutesFromConfig = (
  config: ProductConfig,
): number => {
  return config.valid_from_delay_minutes;
};

export const getDefaultValidityMinutesFromConfig = (
  config: ProductConfig,
): number => {
  return config.default_validity_minutes;
};

export const getOmitChangeFromConfig = (config: ProductConfig): boolean => {
  return config.omit_change_va_and_rdoact_cloud === true;
};

export const getFirArea = (
  selectedFIR: string,
  productConfig: ProductConfig,
): string => {
  // If no FIR selected or FIR doesn't exist in list, select first FIR
  return selectedFIR && productConfig.fir_areas[selectedFIR] !== undefined
    ? selectedFIR
    : productConfig.active_firs[0];
};

export const getActiveFIRArea = (
  selectedFIR: string,
  productConfig: ProductConfig,
): FIRConfigSigmet | FIRConfigAirmet => {
  const FIR = getFirArea(selectedFIR, productConfig);
  return productConfig.fir_areas[FIR];
};

export const DEFAULT_MINIMUM_SURFACE_LEVEL = 0;

export const getMinCloudLevelValue = (
  unit: string,
  selectedFIR: string,
  productConfig: AirmetConfig,
): number => {
  // Use first FIR in list if no FIR selected
  const { cloud_level_min } = getActiveFIRArea(
    selectedFIR,
    productConfig,
  ) as FIRConfigAirmet;

  // Return min value for selected unit - if not present in list, return 0 as we cannot go below the surface
  return cloud_level_min !== undefined &&
    cloud_level_min![unit as keyof CloudLevelMinMaxType]
    ? cloud_level_min![unit as keyof CloudLevelMinMaxType]!
    : DEFAULT_MINIMUM_SURFACE_LEVEL;
};

export const getMinCloudLowerLevelValue = (
  unit: string,
  selectedFIR: string,
  productConfig: AirmetConfig,
): number => {
  // Use first FIR in list if no FIR selected
  const { cloud_lower_level_min } = getActiveFIRArea(
    selectedFIR,
    productConfig,
  ) as FIRConfigAirmet;

  // Return min value for selected unit - if not present in list, return 0 as we cannot go below the surface
  return cloud_lower_level_min !== undefined &&
    cloud_lower_level_min![unit as keyof CloudLevelMinMaxType]
    ? cloud_lower_level_min![unit as keyof CloudLevelMinMaxType]!
    : DEFAULT_MINIMUM_SURFACE_LEVEL;
};

export const getMaxCloudLevelValue = (
  unit: string,
  selectedFIR: string,
  productConfig: AirmetConfig,
): number | null => {
  // Use first FIR in list if no FIR selected
  const { cloud_level_max } = getActiveFIRArea(
    selectedFIR,
    productConfig,
  ) as FIRConfigAirmet;

  // Return max value for selected unit - if not present in list, return null
  return cloud_level_max !== undefined &&
    cloud_level_max![unit as keyof CloudLevelMinMaxType]
    ? cloud_level_max![unit as keyof CloudLevelMinMaxType]!
    : null;
};

export const getMaxCloudLowerLevelValue = (
  unit: string,
  selectedFIR: string,
  productConfig: AirmetConfig,
): number | null => {
  // Use first FIR in list if no FIR selected
  const { cloud_lower_level_max } = getActiveFIRArea(
    selectedFIR,
    productConfig,
  ) as FIRConfigAirmet;

  // Return max value for selected unit - if not present in list, return null
  return cloud_lower_level_max !== undefined &&
    cloud_lower_level_max![unit as keyof CloudLevelMinMaxType]
    ? cloud_lower_level_max![unit as keyof CloudLevelMinMaxType]!
    : null;
};

// Retrieve max value from config - if not set, there is no max value for this unit so we return null
export const getMaxLevelValue = (
  unit: string,
  selectedFIR: string,
  productConfig: ProductConfig,
): number | null => {
  // Use first FIR in list if no FIR selected
  const { level_max } = getActiveFIRArea(selectedFIR, productConfig);
  // Return max value for selected unit - if not present in list, return null
  return level_max !== undefined && level_max![unit as keyof LevelMinMaxType]
    ? level_max![unit as keyof LevelMinMaxType]!
    : null;
};

// Retrieve min value from config - if not set, return zero
export const getMinLevelValue = (
  unit: string,
  selectedFIR: string,
  productConfig: ProductConfig,
): number => {
  // Use first FIR in list if no FIR selected
  const { level_min } = getActiveFIRArea(selectedFIR, productConfig);

  // Return min value for selected unit - if not present in list, return 0 as we cannot go below the surface
  return level_min !== undefined &&
    level_min![unit as keyof CloudLevelMinMaxType]
    ? level_min![unit as keyof CloudLevelMinMaxType]!
    : DEFAULT_MINIMUM_SURFACE_LEVEL;
};

export const getMaxWindSpeedValue = (
  unit: string,
  selectedFIR: string,
  productConfig: AirmetConfig,
): number | null => {
  // Use first FIR in list if no FIR selected
  const { wind_speed_max } = getActiveFIRArea(
    selectedFIR,
    productConfig,
  ) as FIRConfigAirmet;

  // Return min value for selected unit - if not present in list, return null
  return wind_speed_max !== undefined &&
    wind_speed_max![unit as keyof SurfaceWindMinMaxType]
    ? wind_speed_max![unit as keyof SurfaceWindMinMaxType]!
    : null;
};

export const DEFAULT_MIN_WIND_SPEED = 0;
export const getMinWindSpeedValue = (
  unit: string,
  selectedFIR: string,
  productConfig: AirmetConfig,
): number => {
  // Use first FIR in list if no FIR selected
  const { wind_speed_min } = getActiveFIRArea(
    selectedFIR,
    productConfig,
  ) as FIRConfigAirmet;

  // Return min value for selected unit - if not present in list, return 0 as we have negative wind speed
  return wind_speed_min !== undefined &&
    wind_speed_min![unit as keyof SurfaceWindMinMaxType]
    ? wind_speed_min![unit as keyof SurfaceWindMinMaxType]!
    : DEFAULT_MIN_WIND_SPEED;
};

export const getMaxVisibilityValue = (
  selectedFIR: string,
  productConfig: AirmetConfig,
): number | null => {
  // Use first FIR in list if no FIR selected
  const { visibility_max } = getActiveFIRArea(
    selectedFIR,
    productConfig,
  ) as FIRConfigAirmet;

  // Return max value for selected unit - if not present in list, return null
  return visibility_max || null;
};

export const DEFAULT_MIN_VISIBILITY = 0;
export const getMinVisibilityValue = (
  selectedFIR: string,
  productConfig: AirmetConfig,
): number => {
  // Use first FIR in list if no FIR selected
  const { visibility_min } = getActiveFIRArea(
    selectedFIR,
    productConfig,
  ) as FIRConfigAirmet;

  // Return min value for selected unit - if not present in list, return 0 as we have negative wind speed
  return visibility_min || DEFAULT_MIN_VISIBILITY;
};

export const getAllFirAreas = (productConfig: ProductConfig): Firareas => {
  return productConfig.fir_areas;
};

export const getFirOptions = (
  productConfig: ProductConfig,
): { ATSR: ATSRLocation; FIR: ICAOLocation }[] => {
  const firAreas = getAllFirAreas(productConfig);
  const optionsFIR = Object.keys(firAreas).map((fir) => ({
    ATSR: firAreas[fir].location_indicator_atsr,
    FIR: firAreas[fir].fir_name,
  }));
  return optionsFIR;
};

export const getLevelInFeet = (level: number, unit: string): number => {
  if (LevelUnits[unit as keyof typeof LevelUnits] === 'm') {
    return level / 0.3048;
  }
  if (LevelUnits[unit as keyof typeof LevelUnits] === 'FL') {
    return level * 100;
  }
  return level;
};

export const isLevelLower = (
  ownValue: number | string,
  ownUnit: LevelUnits,
  otherValue: number,
  otherUnit: LevelUnits,
): boolean => {
  if (typeof ownValue === 'string' && !ownValue.length) {
    return true;
  }
  if (ownUnit === otherUnit && typeof ownValue === 'number') {
    return ownValue < otherValue;
  }

  // not same units - convert both to feet and compare
  const ownValueFeet = getLevelInFeet(ownValue as number, ownUnit);
  const otherValueFeet = getLevelInFeet(otherValue, otherUnit);

  return ownValueFeet < otherValueFeet;
};

export const getDialogtitle = (
  product: AviationProduct,
  productType: ProductType,
): string => {
  const productName = productType.toUpperCase();

  if (product === null) {
    return `New ${productName}`;
  }
  if (!isInstanceOfCancelSigmetOrAirmet(product)) {
    const phenomenon =
      productType === 'sigmet'
        ? SigmetPhenomena[
            (product as Sigmet)
              .phenomenon as unknown as keyof typeof SigmetPhenomena
          ]
        : AirmetPhenomena[
            (product as Airmet)
              .phenomenon as unknown as keyof typeof AirmetPhenomena
          ];
    return `${productName} ${phenomenon} - ${
      product.status === 'DRAFT'
        ? 'saved as draft'
        : ProductStatusDescription[product.status]
    }`;
  }
  if (isInstanceOfCancelSigmet(product)) {
    return `${productName} Cancels ${product.cancelsSigmetSequenceId}`;
  }
  return `${productName} Cancels ${product.cancelsAirmetSequenceId}`;
};

export const getProductFormMode = (
  canBeOptions: ProductCanbe[],
  productListItem: SigmetFromBackend | AirmetFromBackend,
): string => {
  if (productListItem === null) {
    return 'new';
  }

  return canBeOptions.includes('PUBLISHED') ? 'edit' : 'view';
};

const buttonStyle = {
  minWidth: 96,
};

export const getProductFormLifecycleButtons: React.FC = (
  canBe: string[],
  onButtonPress: (newStatus: ProductStatus | 'DISCARDED') => void,
) => {
  const hasDiscardButton = canBe.includes('DISCARDED');
  const onDiscard = (): void => onButtonPress('DISCARDED');

  const hasSaveButton = canBe.includes('DRAFTED');
  const onSave = (): void => onButtonPress('DRAFT');

  const hasCancelButton = canBe.includes('CANCELLED');
  const onCancel = (): void => onButtonPress('CANCELLED');

  const hasPublishButton = canBe.includes('PUBLISHED');
  const onPublish = (): void => onButtonPress('PUBLISHED');

  const mobileMenuItems = [
    ...(hasDiscardButton ? [{ text: 'Discard', action: onDiscard }] : []),
    ...(hasSaveButton ? [{ text: 'Save', action: onSave }] : []),
    ...(hasCancelButton ? [{ text: 'Cancel', action: onCancel }] : []),
    ...(hasPublishButton ? [{ text: 'Publish', action: onPublish }] : []),
  ];

  return (
    <Box
      sx={{
        display: 'flex',
        justifyContent: 'flex-end',
        'button:not(:first-of-type)': { marginLeft: 1 },
      }}
    >
      <Box display={{ xs: 'none', lg: 'block' }}>
        {hasDiscardButton && (
          <Button
            variant="flat"
            onClick={onDiscard}
            sx={buttonStyle}
            data-testid="productform-dialog-discard"
          >
            DISCARD
          </Button>
        )}
        {hasSaveButton && (
          <Button
            variant="tertiary"
            onClick={onSave}
            sx={buttonStyle}
            data-testid="productform-dialog-draft"
          >
            Save
          </Button>
        )}
        {hasCancelButton && (
          <Button
            variant="tertiary"
            onClick={onCancel}
            sx={buttonStyle}
            data-testid="productform-dialog-cancel"
          >
            Cancel
          </Button>
        )}
        {hasPublishButton && (
          <Button
            variant="primary"
            onClick={onPublish}
            sx={buttonStyle}
            data-testid="productform-dialog-publish"
          >
            Publish
          </Button>
        )}
      </Box>
      <Box display={{ lg: 'none' }}>
        {mobileMenuItems.length > 0 && (
          <ToggleMenu menuItems={mobileMenuItems} />
        )}
      </Box>
    </Box>
  );
};

export const hasValue = (value: number | string): boolean =>
  value !== null && value.toString() !== 'NaN' && value !== '';

export const prepareFormValues = <T extends AviationProduct>(product: T): T =>
  Object.keys(product).reduce((list, key) => {
    const value = product[
      key as keyof AviationProduct
    ] as unknown as GeoJSON.FeatureCollection;
    // remove hidden form values
    if (key === 'IS_DRAFT') {
      return list;
    }

    // remove geometry if empty coordinates
    if (
      key === 'endGeometry' ||
      key === 'endGeometryIntersect' ||
      key === 'startGeometry' ||
      key === 'startGeometryIntersect'
    ) {
      if (
        value?.features.length > 0 &&
        value?.features[0].geometry.type === 'Polygon' &&
        value?.features[0].geometry.coordinates[0].length
      ) {
        return { ...list, [key]: value };
      }
      if (
        value?.features.length > 0 &&
        value?.features[0].geometry.type === 'Point' &&
        value?.features[0].geometry.coordinates.length
      ) {
        return { ...list, [key]: value };
      }

      return list;
    }

    // check if nested object has value
    if (typeof value === 'object' && value !== null && value !== undefined) {
      const hasNaNValue = Object.keys(value).reduce(
        (total, valKey: keyof GeoJSON.FeatureCollection) =>
          !hasValue(String(value[valKey])) || total,
        false,
      );

      return {
        ...list,
        ...(!hasNaNValue && {
          [key]: value,
        }),
      };
    }

    return {
      ...list,
      ...(hasValue(value) && {
        [key]: value,
      }),
    };
  }, {} as T);

export const createSigmetPostParameter = (
  sigmetToPost: Sigmet | CancelSigmet,
  newStatus: ProductStatus,
  previousUuid: string,
): SigmetFromFrontend => {
  const sigmetIncludingUuid =
    previousUuid !== null
      ? { ...sigmetToPost, uuid: previousUuid }
      : sigmetToPost;

  // Do not send empty numbered fields to the BE to prevent errors
  const sigmet = prepareFormValues(sigmetIncludingUuid);

  return {
    changeStatusTo: newStatus,
    sigmet,
  };
};

export const createAirmetPostParameter = (
  airmetToPost: Airmet | CancelAirmet,
  newStatus: ProductStatus,
  previousUuid: string,
): AirmetFromFrontend => {
  const airmetIncludingUuid =
    previousUuid !== null
      ? { ...airmetToPost, uuid: previousUuid }
      : airmetToPost;

  // Do not send empty numbered fields to the BE to prevent errors
  const airmet = prepareFormValues(airmetIncludingUuid);

  return {
    changeStatusTo: newStatus,
    airmet,
  };
};

export const useDelayedFormValues = (
  getValues: () => void,
  register: () => void,
  delay = 1000,
): [unknown, () => void] => {
  const { isMounted } = useIsMounted();
  const formValueTimer = React.useRef<ReturnType<typeof setTimeout> | null>(
    null,
  );
  const [formValues, setFormValues] = React.useState(getValues());

  React.useEffect(() => {
    if (!isMounted.current) {
      return;
    }
    setFormValues(getValues());
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [register, getValues]);

  const updateFormValueTimer = (): void => {
    if (!isMounted.current) {
      return;
    }
    setFormValues(getValues());
  };

  const onChangeForm = (): void => {
    clearTimeout(formValueTimer.current!);
    formValueTimer.current = setTimeout(updateFormValueTimer, delay);
  };

  return [formValues, onChangeForm];
};

export const getConfirmationDialogTitle = (
  action: ProductActions,
  productType: ProductType,
): string => {
  const product = productType.toUpperCase();
  if (action === 'CLOSED') {
    return `Close ${product}`;
  }
  if (action === 'DISCARDED') {
    return `Discard ${product}`;
  }
  if (action === 'PUBLISHED') {
    return 'Publish';
  }
  return `Cancel ${product}`;
};

export const getConfirmationDialogContent = (
  action: ProductActions,
  productType: ProductType,
): string => {
  const product = productType.toUpperCase();
  if (action === 'DISCARDED') {
    return `Are you sure you would like to discard this ${product}? Its properties will be lost`;
  }
  if (action === 'CLOSED') {
    return 'Do you want to save any changes made?';
  }
  if (action === 'PUBLISHED') {
    return `Are you sure you want to publish this ${product}?`;
  }
  return `Are you sure you want to cancel this ${product}?`;
};

export const getConfirmationDialogButtonLabel = (
  action: ProductActions,
  productType: ProductType,
): string => {
  const product = productType.toUpperCase();
  switch (action) {
    case 'DISCARDED':
      return `Discard ${product}`;
    case 'PUBLISHED':
      return 'Publish';
    case 'CANCELLED':
      return 'Cancel';
    default:
      return 'Save and close';
  }
};

export const getConfirmationDialogCancelLabel = (
  action: ProductActions,
): string => {
  switch (action) {
    case 'CLOSED':
      return 'Discard and close';
    default:
      return undefined!;
  }
};

export const getProductIssueDate = (
  initialProduct: Sigmet | Airmet,
  initialCancelProduct: CancelSigmet | CancelAirmet,
): string => {
  if (
    initialProduct !== null &&
    (initialProduct.status === 'PUBLISHED' ||
      initialProduct.status === 'EXPIRED') &&
    initialProduct.issueDate
  ) {
    return `${dateUtils.dateToString(
      dateUtils.utc(initialProduct.issueDate),
      'yyyy/MM/dd HH:mm',
    )} UTC`;
  }

  if (initialCancelProduct !== null && initialCancelProduct.issueDate) {
    return `${dateUtils.dateToString(
      dateUtils.utc(initialCancelProduct.issueDate),
      'yyyy/MM/dd HH:mm',
    )} UTC`;
  }
  return '(Not published)';
};

// Provide a cross section of the units from the config and the units as allowed by the type definition
// Units from the config which are not in the type definition should be ignored
export const extractUnitsToShow = (
  configUnits: string[],
  unitType: Record<string, string>,
): Record<string, string> => {
  return configUnits.reduce((list, unit) => {
    if (unitType[unit]) {
      return {
        ...list,
        [unit]: unitType[unit],
      };
    }
    return list;
  }, {});
};

export const getAllowedUnits = (
  selectedFIR: string,
  productConfig: ProductConfig,
  config_type: string,
  unitType: Record<string, string>,
): Record<string, string> => {
  // For selected FIR, find the corresponding unit_type. If not specified in the config, return as defined in type
  if (selectedFIR !== undefined && productConfig.fir_areas[selectedFIR]) {
    const firAllowedUnits = productConfig.fir_areas[selectedFIR].units;
    const allowedMovementUnits = firAllowedUnits.find(
      (unitConfig) => unitConfig.unit_type === config_type,
    );

    return allowedMovementUnits
      ? extractUnitsToShow(allowedMovementUnits.allowed_units, unitType)
      : unitType;
  }

  // If no FIR selected or FIR not in options - return all units as defined in the type
  return unitType;
};

/**
 * Function to translate the geojson data to a GeoJSON.FeatureCollection
 */
export const getFir = (
  productConfig: ProductConfig,
  firName?: string,
): FIRLocationGeoJson => {
  const currentFIRName = firName || productConfig.active_firs[0];
  return productConfig.fir_areas[currentFIRName].fir_location;
};

/**
 * Function to check if the given geojson object has 'fir' selectionType property
 */

export const isFir = (geojson: GeoJSON.FeatureCollection): boolean => {
  return (
    geojson &&
    geojson.features &&
    geojson.features.length > 0 &&
    geojson.features[0]!.properties!['selectionType'] === 'fir'
  );
};

export const getFieldLabel = (
  title: string,
  isDisabled: boolean,
  action = 'Select',
): string => (isDisabled ? title : `${action} ${title.toLowerCase()}`);

export const getBaseLayers = (
  productConfig: ProductConfig,
): layerTypes.Layer[] =>
  productConfig.mapPreset?.layers?.filter(
    (layer) =>
      layer.layerType === LayerType.baseLayer ||
      layer.layerType === LayerType.overLayer,
  ) || [];

export const getMaxMovementSpeedValue = (
  unit: string,
  selectedFIR: string,
  productConfig: ProductConfig,
): number | null => {
  const { movement_max } = getActiveFIRArea(selectedFIR, productConfig);

  // Return max value for selected unit - if not present in list, return null
  return movement_max !== undefined &&
    movement_max![unit as keyof MovementMinMaxType]
    ? movement_max![unit as keyof MovementMinMaxType]!
    : null;
};

export const DEFAULT_MIN_MOVEMENT_SPEED = 0;

export const getMinMovementSpeedValue = (
  unit: string,
  selectedFIR: string,
  productConfig: ProductConfig,
): number => {
  const { movement_min } = getActiveFIRArea(selectedFIR, productConfig);

  // Return min value for selected unit - if not present in list, return DEFAULT_MIN_MOVEMENT_SPEED (zero, no negative speeds)
  return movement_min !== undefined &&
    movement_min![unit as keyof MovementMinMaxType]
    ? movement_min![unit as keyof MovementMinMaxType]!
    : DEFAULT_MIN_MOVEMENT_SPEED;
};

/** Trigger form validations for given list of field names if field has a value */
export const triggerValidations = (
  fieldNames: string[],
  getValues: UseFormGetValues<FieldValues>,
  trigger: UseFormTrigger<FieldValues>,
): void => {
  fieldNames.forEach((field) => {
    const value = getValues(field);
    if (value || value === 0) {
      trigger(field);
    }
  });
};

export const getProjection = (
  productConfig: ProductConfig,
): mapTypes.MapPreset['proj'] => productConfig.mapPreset?.proj || undefined;

export const getShapeWithProperties = (
  shape: GeoJSON.Feature,
  properties: GeoJSON.GeoJsonProperties,
): GeoJSON.Feature => ({
  ...shape,
  properties: {
    ...shape.properties,
    ...properties,
  },
});

export const firID = 'drawtools-fir';

export const getDrawTools = (
  type: StartOrEndDrawing,
  firShape: GeoJSON.FeatureCollection,
): DrawMode[] => {
  const defaultStyleProperties =
    type === StartOrEndDrawing.start ? featurePropsStart : featurePropsEnd;

  return [
    {
      ...defaultPoint,
      title: `Drop a point as ${type} position`,
      shape: getShapeWithProperties(defaultPoint.shape, defaultStyleProperties),
    },
    {
      ...defaultBox,
      title: `Draw a box as ${type} position`,
      shape: getShapeWithProperties(defaultBox.shape, defaultStyleProperties),
    },
    {
      ...defaultPolygon,
      title: `Draw a shape as ${type} position`,
      shape: getShapeWithProperties(
        defaultPolygon.shape,
        defaultStyleProperties,
      ),
    },
    {
      drawModeId: firID,
      title: `Set FIR as ${type} position`,
      isSelectable: false,
      value: DRAWMODE.POLYGON,
      shape: addFeatureProperties(firShape, defaultStyleProperties),
    },
    { ...defaultDelete, title: 'Remove geometry' },
  ];
};

export const updateDrawToolsWithFir = (
  drawTools: DrawMode[],
  newFirShape: GeoJSON.FeatureCollection,
): DrawMode[] => {
  return drawTools.map((mode) => {
    if (mode.drawModeId === firID) {
      return {
        ...mode,
        shape: newFirShape,
      };
    }
    return mode;
  });
};

export const getToolIcon = (id: string): React.ReactElement => {
  const defaultIcon = getIcon(id);
  if (defaultIcon) {
    return defaultIcon;
  }

  if (id === firID) {
    return <DrawFIRLand />;
  }
  return <DrawFIRLand />;
};
