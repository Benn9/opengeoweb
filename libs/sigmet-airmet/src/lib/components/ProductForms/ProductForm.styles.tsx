/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */

export const styles = {
  label: {
    fontSize: '0.875rem',
    marginTop: 2,
    textAlign: 'right',
  },
  body: {
    paddingTop: '17px',
  },
  containerItem: {
    ' .MuiTypography-subtitle1': {
      marginTop: 1,
    },
    paddingTop: '8px!important',
    '+ .MuiGrid-item': {
      paddingTop: 0,
    },
    '.MuiButtonBase-root.MuiRadio-root': {
      padding: '6px',
      marginLeft: '3px',
    },
    '.MuiFormControl-root.MuiTextField-root': {
      marginBottom: 0,
    },
    '.MuiGrid-root': {
      alignItems: 'flex-start',
    },
  },
  volcanicField: {
    marginBottom: 2,
  },
  surfaceVisibility: {
    marginBottom: 2,
  },
  surfaceWind: {
    marginBottom: 2,
  },
  levelField: {
    marginBottom: 2,
  },
  movement: {
    marginBottom: 2,
  },
  latitude: {
    width: '97%',
    '.MuiFormControl-root.MuiTextField-root': {
      margin: 0,
    },
  },
  unit: {
    width: '95%',
  },
  quitDrawModeMessage: { paddingBottom: '10px', opacity: '1' },
  drawSection: {
    '+ .MuiGrid-item': {
      paddingTop: 5,
    },
    ' .MuiTypography-subtitle1': {
      marginTop: 1,
    },
  },
  helperText: {
    ' .MuiFormHelperText-root': {
      top: 'auto',
      position: 'relative',
      lineHeight: '1.33em',
      '&.Mui-error': {
        top: 'auto',
        position: 'relative',
      },
    },
    '.MuiFormLabel-root.MuiInputLabel-root': {
      top: -4,
      lineHeight: '1.33em',
    },
    '.MuiSvgIcon-root': {
      width: '0.8em',
      height: '0.8em',
    },
    '.MuiButtonBase-root.MuiIconButton-root': {
      padding: '2px',
      marginLeft: '-4px',
    },
  },
};
