/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */
import {
  fireEvent,
  render,
  renderHook,
  act,
  screen,
  waitFor,
} from '@testing-library/react';
import { dateUtils } from '@opengeoweb/shared';
import { DrawFIRLand } from '@opengeoweb/theme';
import React from 'react';
import { LayerType } from '@opengeoweb/webmap';
import {
  DRAWMODE,
  addFeatureProperties,
  defaultBox,
  defaultDelete,
  defaultPoint,
  defaultPolygon,
} from '@opengeoweb/webmap-react';
import {
  LevelUnits,
  isInstanceOfCancelSigmet,
  isInstanceOfSigmet,
  SigmetPhenomena,
  ProductCanbe,
  ProductStatusDescription,
  Sigmet,
  CancelSigmet,
  AirmetPhenomena,
  isInstanceOfAirmet,
  isInstanceOfCancelAirmet,
  MovementUnit,
  FIRLocationGeoJson,
  SigmetConfig,
  ProductConfig,
  Firareas,
  AirmetConfig,
  StartOrEndDrawing,
  Airmet,
} from '../../types';
import {
  isLevelLower,
  getDialogtitle,
  getProductFormMode,
  getProductFormLifecycleButtons,
  createSigmetPostParameter,
  useDelayedFormValues,
  getConfirmationDialogTitle,
  getConfirmationDialogContent,
  getConfirmationDialogButtonLabel,
  getConfirmationDialogCancelLabel,
  getHoursBeforeValidity,
  getMaxHoursOfValidity,
  getValidFromDelayTimeMinutesFromConfig,
  getDefaultValidityMinutesFromConfig,
  getMaxLevelValue,
  getLevelInFeet,
  getProductIssueDate,
  createAirmetPostParameter,
  getMinLevelValue,
  getMaxWindSpeedValue,
  getMinWindSpeedValue,
  getAllowedUnits,
  prepareFormValues,
  hasValue,
  getFieldLabel,
  getAllFirAreas,
  getFirOptions,
  getFir,
  isFir,
  extractUnitsToShow,
  getBaseLayers,
  getMaxMovementSpeedValue,
  getMinMovementSpeedValue,
  DEFAULT_MIN_MOVEMENT_SPEED,
  getFirArea,
  getMinCloudLevelValue,
  getMinCloudLowerLevelValue,
  getMaxCloudLevelValue,
  getMaxCloudLowerLevelValue,
  DEFAULT_MINIMUM_SURFACE_LEVEL,
  getActiveFIRArea,
  DEFAULT_MIN_WIND_SPEED,
  DEFAULT_MIN_VISIBILITY,
  getMinVisibilityValue,
  getMaxVisibilityValue,
  triggerValidations,
  getProjection,
  getShapeWithProperties,
  getDrawTools,
  firID,
  updateDrawToolsWithFir,
  getOmitChangeFromConfig,
  getToolIcon,
} from './utils';
import {
  fakeNewSigmet,
  fakeSigmetList,
} from '../../utils/mockdata/fakeSigmetList';
import {
  fakeAirmetList,
  fakeNewAirmet,
} from '../../utils/mockdata/fakeAirmetList';
import { sigmetConfig } from '../../utils/config';
import {
  featurePropsEnd,
  featurePropsStart,
} from '../MapViewGeoJson/constants';

const testFirArea = getFir(sigmetConfig);

describe('components/SigmetForm/utils', () => {
  const config: SigmetConfig = {
    location_indicator_mwo: 'EHDB',
    active_firs: ['EHAA'],
    fir_areas: {
      EHAA: {
        fir_name: 'AMSTERDAM FIR',
        fir_location: testFirArea,
        location_indicator_atsr: 'EHAA',
        location_indicator_atsu: 'EHAA',
        hours_before_validity: 5,
        tc_hours_before_validity: 12,
        va_hours_before_validity: 20,
        max_hours_of_validity: 5,
        va_max_hours_of_validity: 12,
        tc_max_hours_of_validity: 20,
        adjacent_firs: ['EKDK', 'EGPX'],
        level_min: {
          FT: 100,
          FL: 50,
        },
        level_max: {
          FT: 4900,
          FL: 650,
        },
        level_rounding_FL: 100,
        level_rounding_FT: 5,
        level_rounding_M: 1,
        movement_min: {
          KT: 5,
          KMH: 10,
        },
        movement_max: {
          KT: 150,
          KMH: 99,
        },
        movement_rounding_kt: 5,
        movement_rounding_kmh: 10,
        units: [
          {
            unit_type: 'level_unit',
            allowed_units: ['FT', 'FL'],
          },
          {
            unit_type: 'movement_unit',
            allowed_units: ['KT'],
          },
        ],
        area_preset: 'NL_FIR',
      },
    },
    valid_from_delay_minutes: 60,
    default_validity_minutes: 180,
    omit_change_va_and_rdoact_cloud: true,
  };

  const configMultipleFIR: SigmetConfig = {
    location_indicator_mwo: 'EHDB',
    fir_areas: {
      EHAA: {
        fir_name: 'AMSTERDAM FIR',
        fir_location: testFirArea,
        location_indicator_atsr: 'EHAA',
        location_indicator_atsu: 'EHAA',
        area_preset: 'NL_FIR',
        max_hours_of_validity: 4,
        hours_before_validity: 4,
        tc_max_hours_of_validity: 6,
        tc_hours_before_validity: 12,
        va_max_hours_of_validity: 6,
        va_hours_before_validity: 12,
        adjacent_firs: ['EKDK', 'EDWW', 'EDGG', 'EBBU', 'EGTT', 'EGPX'],
        level_min: {
          FT: 100,
          FL: 50,
        },
        level_max: {
          FT: 4900,
          FL: 650,
        },
        level_rounding_FL: 100,
        level_rounding_FT: 5,
        level_rounding_M: 1,
        movement_min: {
          KT: 5,
          KMH: 10,
        },
        movement_max: {
          KT: 150,
          KMH: 99,
        },
        movement_rounding_kt: 5,
        movement_rounding_kmh: 10,
        units: [
          {
            unit_type: 'level_unit',
            allowed_units: ['FT', 'M'],
          },
          {
            unit_type: 'movement_unit',
            allowed_units: ['KMH'],
          },
        ],
      },
      EBBU: {
        fir_name: 'Some OTHER FIR',
        fir_location: testFirArea,
        location_indicator_atsr: 'EBBU',
        location_indicator_atsu: 'EBBU',
        area_preset: 'OTHER_FIR',
        max_hours_of_validity: 6,
        hours_before_validity: 7,
        tc_max_hours_of_validity: 12,
        tc_hours_before_validity: 13,
        va_max_hours_of_validity: 2,
        va_hours_before_validity: 1,
        adjacent_firs: ['HIP', 'KNEE', 'ALPHA', 'BRAVO'],
        level_min: {
          FT: 100,
          FL: 50,
        },
        level_max: {
          FT: 4900,
          FL: 650,
        },
        level_rounding_FL: 100,
        level_rounding_FT: 5,
        level_rounding_M: 1,
        movement_min: {
          KT: 5,
          KMH: 10,
        },
        movement_max: {
          KT: 150,
          KMH: 99,
        },
        movement_rounding_kt: 5,
        movement_rounding_kmh: 10,
        units: [
          {
            unit_type: 'level_unit',
            allowed_units: ['FT', 'FL'],
          },
          {
            unit_type: 'movement_unit',
            allowed_units: ['KT'],
          },
        ],
      },
      EDDD: {
        fir_name: 'Some OTHER FIR with no allowed movement units',
        fir_location: testFirArea,
        location_indicator_atsr: 'EDDD',
        location_indicator_atsu: 'EDDD',
        area_preset: 'OTHER_FIR_NO_UNITS',
        max_hours_of_validity: 6,
        hours_before_validity: 7,
        tc_max_hours_of_validity: 12,
        tc_hours_before_validity: 13,
        va_max_hours_of_validity: 2,
        va_hours_before_validity: 1,
        adjacent_firs: ['HIP', 'KNEE', 'ALPHA', 'BRAVO', 'CHARLIE'],
        units: [
          {
            unit_type: 'level_unit',
            allowed_units: ['FT', 'FL', 'RANDOMUNIT'],
          },
        ],
      },
    },
    valid_from_delay_minutes: 45,
    default_validity_minutes: 90,
    active_firs: ['EHAA', 'EBBU', 'EDDD'],
  };

  const airmetConfig: AirmetConfig = {
    location_indicator_mwo: 'EHDB',
    active_firs: ['EHAA'],
    fir_areas: {
      EHAA: {
        fir_name: 'AMSTERDAM FIR',
        fir_location: testFirArea,
        location_indicator_atsr: 'EHAA',
        location_indicator_atsu: 'EHAA',
        hours_before_validity: 5,
        max_hours_of_validity: 5,
        cloud_lower_level_min: {
          FT: 100,
        },
        cloud_lower_level_max: {
          FT: 900,
        },
        cloud_level_min: {
          FT: 100,
        },
        cloud_level_max: {
          FT: 9900,
        },
        wind_speed_min: {
          KT: 31,
        },
        wind_speed_max: {
          KT: 199,
        },
        movement_rounding_kt: 5,
        movement_rounding_kmh: 10,
        visibility_max: 4900,
        visibility_min: 0,
        level_min: {
          FT: 100,
          FL: 50,
        },
        level_max: {
          FT: 4900,
          FL: 650,
        },
        level_rounding_FL: 100,
        level_rounding_FT: 5,
        level_rounding_M: 1,
        units: [
          {
            unit_type: 'level_unit',
            allowed_units: ['FT', 'FL'],
          },
          {
            unit_type: 'movement_unit',
            allowed_units: ['KT'],
          },
        ],
      },
    },
    valid_from_delay_minutes: 60,
    default_validity_minutes: 180,
  };

  const airmetConfigMultipleFIR: AirmetConfig = {
    location_indicator_mwo: 'EHDB',
    fir_areas: {
      EHAA: {
        fir_name: 'AMSTERDAM FIR',
        fir_location: testFirArea,
        location_indicator_atsr: 'EHAA',
        location_indicator_atsu: 'EHAA',
        max_hours_of_validity: 4,
        hours_before_validity: 4,
        cloud_lower_level_min: {
          FT: 100,
        },
        cloud_lower_level_max: {
          FT: 900,
        },
        cloud_level_min: {
          FT: 100,
        },
        cloud_level_max: {
          FT: 9900,
        },
        wind_speed_min: {
          KT: 31,
        },
        wind_speed_max: {
          KT: 199,
        },
        visibility_max: 4900,
        visibility_min: 0,
        level_min: {
          FT: 100,
          FL: 50,
        },
        level_max: {
          FT: 4900,
          FL: 650,
        },
        level_rounding_FL: 100,
        level_rounding_FT: 5,
        level_rounding_M: 1,
        movement_rounding_kt: 5,
        movement_rounding_kmh: 10,
        units: [
          {
            unit_type: 'level_unit',
            allowed_units: ['FT', 'M'],
          },
          {
            unit_type: 'movement_unit',
            allowed_units: ['KMH'],
          },
        ],
      },
      EBBU: {
        fir_name: 'Some OTHER FIR',
        fir_location: testFirArea,
        location_indicator_atsr: 'EBBU',
        location_indicator_atsu: 'EBBU',
        max_hours_of_validity: 6,
        hours_before_validity: 7,
        cloud_lower_level_min: {
          FT: 100,
        },
        cloud_lower_level_max: {
          FT: 900,
        },
        cloud_level_min: {
          FT: 100,
        },
        cloud_level_max: {
          FT: 9900,
        },
        wind_speed_min: {
          KT: 31,
        },
        wind_speed_max: {
          KT: 199,
        },
        visibility_max: 4900,
        visibility_min: 0,
        level_min: {
          FT: 100,
          FL: 50,
        },
        level_max: {
          FT: 4900,
          FL: 650,
        },
        level_rounding_FL: 100,
        level_rounding_FT: 5,
        level_rounding_M: 1,
        movement_rounding_kt: 5,
        movement_rounding_kmh: 10,
        units: [
          {
            unit_type: 'level_unit',
            allowed_units: ['FT', 'FL'],
          },
          {
            unit_type: 'movement_unit',
            allowed_units: ['KT'],
          },
        ],
      },
      EDDD: {
        fir_name: 'Some OTHER FIR with no allowed movement units',
        fir_location: testFirArea,
        location_indicator_atsr: 'EDDD',
        location_indicator_atsu: 'EDDD',
        max_hours_of_validity: 6,
        hours_before_validity: 7,
        units: [
          {
            unit_type: 'level_unit',
            allowed_units: ['FT', 'FL', 'RANDOMUNIT'],
          },
        ],
      },
    },
    valid_from_delay_minutes: 45,
    default_validity_minutes: 90,
    active_firs: ['EHAA', 'EBBU', 'EDDD'],
  };

  describe('getHoursBeforeValidity', () => {
    it('should return default hours before validity from the config', () => {
      expect(getHoursBeforeValidity('HVY_DS', 'EHAA', config)).toBe(
        config.fir_areas.EHAA.hours_before_validity,
      );
    });
    it('should return vc hours before validity from the config', () => {
      expect(getHoursBeforeValidity('VA_CLD', 'EHAA', config)).toBe(
        config.fir_areas.EHAA.va_hours_before_validity,
      );
    });
    it('should return tc hours before validity from the config', () => {
      expect(getHoursBeforeValidity('TC', 'EHAA', config)).toBe(
        config.fir_areas.EHAA.tc_hours_before_validity,
      );
    });
    it('should return default 4 hours when there is no fir', () => {
      expect(getHoursBeforeValidity('HVY_DS', undefined!, config)).toBe(4);
    });
    it('should return default hours from the correct FIR when it has multiple FIRs', () => {
      expect(getHoursBeforeValidity('HVY_DS', 'EBBU', configMultipleFIR)).toBe(
        configMultipleFIR.fir_areas.EBBU.hours_before_validity,
      );
    });
  });

  describe('getMaxHoursOfValidity', () => {
    it('should return default max hours of validity from the config', () => {
      expect(getMaxHoursOfValidity('HVY_DS', 'EHAA', config)).toBe(
        config.fir_areas.EHAA.max_hours_of_validity,
      );
    });
    it('should return vc max hours of validity from the config', () => {
      expect(getMaxHoursOfValidity('VA_CLD', 'EHAA', config)).toBe(
        config.fir_areas.EHAA.va_max_hours_of_validity,
      );
    });
    it('should return tc max hours of validity from the config', () => {
      expect(getMaxHoursOfValidity('TC', 'EHAA', config)).toBe(
        config.fir_areas.EHAA.tc_max_hours_of_validity,
      );
    });
    it('should return default 4 hours if there is no fir', () => {
      expect(getMaxHoursOfValidity('TC', undefined!, config)).toBe(4);
    });
    it('should return default hours from the correct FIR when it has multiple FIRs', () => {
      expect(getMaxHoursOfValidity('HVY_DS', 'EBBU', configMultipleFIR)).toBe(
        configMultipleFIR.fir_areas.EBBU.max_hours_of_validity,
      );
    });
  });

  describe('getValidFromDelayTimeMinutesFromConfig', () => {
    it('should return the valid_from_delay_minutes key from the config', () => {
      expect(getValidFromDelayTimeMinutesFromConfig(config)).toBe(
        config.valid_from_delay_minutes,
      );
      expect(getValidFromDelayTimeMinutesFromConfig(configMultipleFIR)).toBe(
        configMultipleFIR.valid_from_delay_minutes,
      );
    });
  });

  describe('getDefaultValidityMinutesFromConfig', () => {
    it('should return the default_validity_minutes key from the config', () => {
      expect(getDefaultValidityMinutesFromConfig(config)).toBe(
        config.default_validity_minutes,
      );
      expect(getDefaultValidityMinutesFromConfig(configMultipleFIR)).toBe(
        configMultipleFIR.default_validity_minutes,
      );
    });
  });

  describe('getOmitChangeFromConfig', () => {
    it('should return the omit_change_va_and_rdoact_cloud key from the config', () => {
      expect(getOmitChangeFromConfig(config)).toBe(
        config.omit_change_va_and_rdoact_cloud,
      );
    });
  });

  describe('getFirArea', () => {
    it('should return the selected FIR  if it exists and is passed otherwise return first in list', () => {
      expect(getFirArea('EHAA', config)).toBe('EHAA');
      expect(getFirArea('EBBU', configMultipleFIR)).toBe('EBBU');
      expect(getFirArea('EDDD', configMultipleFIR)).toBe('EDDD');
      expect(getFirArea('RANDOMFIR', configMultipleFIR)).toBe('EHAA');
      expect(getFirArea(undefined!, configMultipleFIR)).toBe('EHAA');
    });
  });

  describe('getActiveFIRArea', () => {
    it('should return active FIR area if exists, otherwise return the first', () => {
      expect(getActiveFIRArea(undefined!, configMultipleFIR)).toEqual(
        configMultipleFIR.fir_areas[configMultipleFIR.active_firs[0]],
      );
      expect(
        getActiveFIRArea(configMultipleFIR.active_firs[0], configMultipleFIR),
      ).toEqual(configMultipleFIR.fir_areas[configMultipleFIR.active_firs[0]]);
      expect(
        getActiveFIRArea(configMultipleFIR.active_firs[1], configMultipleFIR),
      ).toEqual(configMultipleFIR.fir_areas[configMultipleFIR.active_firs[1]]);

      expect(getActiveFIRArea('EHAA', configMultipleFIR)).toEqual(
        configMultipleFIR.fir_areas[configMultipleFIR.active_firs[0]],
      );

      expect(getActiveFIRArea('EBBU', configMultipleFIR)).toEqual(
        configMultipleFIR.fir_areas[configMultipleFIR.active_firs[1]],
      );
    });
  });

  describe('getMinCloudLevelValue', () => {
    it('should return the max of FIR for passed unit', () => {
      expect(
        getMinCloudLevelValue('FT' as LevelUnits, 'EHAA', airmetConfig),
      ).toBe(airmetConfig.fir_areas.EHAA.cloud_level_min!.FT);
      expect(
        getMinCloudLevelValue(
          'FT' as LevelUnits,
          'EBBU',
          airmetConfigMultipleFIR,
        ),
      ).toBe(airmetConfigMultipleFIR.fir_areas.EBBU.cloud_level_min!.FT);
    });
    it('should return the max of first FIR for passed unit if FIR doesnt exist or isnt passed', () => {
      expect(
        getMinCloudLevelValue(
          'FT' as LevelUnits,
          'EHFG',
          airmetConfigMultipleFIR,
        ),
      ).toBe(airmetConfigMultipleFIR.fir_areas.EHAA.cloud_level_min!.FT);
      expect(
        getMinCloudLevelValue(
          'FT' as LevelUnits,
          undefined!,
          airmetConfigMultipleFIR,
        ),
      ).toBe(airmetConfigMultipleFIR.fir_areas.EHAA.cloud_level_min!.FT);
    });
    it('should return null if unit not present or if level_max not set in config', () => {
      expect(
        getMinCloudLevelValue(
          'FAKEUNIT' as LevelUnits,
          'EHFG',
          airmetConfigMultipleFIR,
        ),
      ).toBe(DEFAULT_MINIMUM_SURFACE_LEVEL);
      // cloud_level_min not set in config
      expect(
        getMinCloudLevelValue(
          'M' as LevelUnits,
          'EDDD',
          airmetConfigMultipleFIR,
        ),
      ).toBe(DEFAULT_MINIMUM_SURFACE_LEVEL);
    });
  });

  describe('getMinCloudLowerLevelValue', () => {
    it('should return the max of FIR for passed unit', () => {
      expect(
        getMinCloudLowerLevelValue('FT' as LevelUnits, 'EHAA', airmetConfig),
      ).toBe(airmetConfig.fir_areas.EHAA.cloud_lower_level_min!.FT);
      expect(
        getMinCloudLowerLevelValue(
          'FT' as LevelUnits,
          'EBBU',
          airmetConfigMultipleFIR,
        ),
      ).toBe(airmetConfigMultipleFIR.fir_areas.EBBU.cloud_lower_level_min!.FT);
    });
    it('should return the max of first FIR for passed unit if FIR doesnt exist or isnt passed', () => {
      expect(
        getMinCloudLowerLevelValue(
          'FT' as LevelUnits,
          'EHFG',
          airmetConfigMultipleFIR,
        ),
      ).toBe(airmetConfigMultipleFIR.fir_areas.EHAA.cloud_lower_level_min!.FT);
      expect(
        getMinCloudLowerLevelValue(
          'FT' as LevelUnits,
          undefined!,
          airmetConfigMultipleFIR,
        ),
      ).toBe(airmetConfigMultipleFIR.fir_areas.EHAA.cloud_lower_level_min!.FT);
    });
    it('should return null if unit not present or if level_max not set in config', () => {
      expect(
        getMinCloudLowerLevelValue(
          'FAKEUNIT' as LevelUnits,
          'EHFG',
          airmetConfigMultipleFIR,
        ),
      ).toBe(DEFAULT_MINIMUM_SURFACE_LEVEL);
      // cloud_lower_level_min not set in config
      expect(
        getMinCloudLowerLevelValue(
          'M' as LevelUnits,
          'EDDD',
          airmetConfigMultipleFIR,
        ),
      ).toBe(DEFAULT_MINIMUM_SURFACE_LEVEL);
    });
  });

  describe('getMaxCloudLevelValue', () => {
    it('should return the max of FIR for passed unit', () => {
      expect(
        getMaxCloudLevelValue('FT' as LevelUnits, 'EHAA', airmetConfig),
      ).toBe(airmetConfig.fir_areas.EHAA.cloud_level_max!.FT);
      expect(
        getMaxCloudLevelValue(
          'FT' as LevelUnits,
          'EBBU',
          airmetConfigMultipleFIR,
        ),
      ).toBe(airmetConfigMultipleFIR.fir_areas.EBBU.cloud_level_max!.FT);
    });
    it('should return the max of first FIR for passed unit if FIR doesnt exist or isnt passed', () => {
      expect(
        getMaxCloudLevelValue(
          'FT' as LevelUnits,
          'EHFG',
          airmetConfigMultipleFIR,
        ),
      ).toBe(airmetConfigMultipleFIR.fir_areas.EHAA.cloud_level_max!.FT);
      expect(
        getMaxCloudLevelValue(
          'FT' as LevelUnits,
          undefined!,
          airmetConfigMultipleFIR,
        ),
      ).toBe(airmetConfigMultipleFIR.fir_areas.EHAA.cloud_level_max!.FT);
    });
    it('should return null if unit not present or if level_max not set in config', () => {
      expect(
        getMaxCloudLevelValue(
          'FAKEUNIT' as LevelUnits,
          'EHFG',
          airmetConfigMultipleFIR,
        ),
      ).toBe(null);
      // cloud_level_max not set in config
      expect(
        getMaxCloudLevelValue(
          'M' as LevelUnits,
          'EDDD',
          airmetConfigMultipleFIR,
        ),
      ).toBe(null);
    });
  });

  describe('getMaxCloudLowerLevelValue', () => {
    it('should return the max of FIR for passed unit', () => {
      expect(
        getMaxCloudLowerLevelValue('FT' as LevelUnits, 'EHAA', airmetConfig),
      ).toBe(airmetConfig.fir_areas.EHAA.cloud_lower_level_max!.FT);
      expect(
        getMaxCloudLowerLevelValue(
          'FT' as LevelUnits,
          'EBBU',
          airmetConfigMultipleFIR,
        ),
      ).toBe(airmetConfigMultipleFIR.fir_areas.EBBU.cloud_lower_level_max!.FT);
    });
    it('should return the max of first FIR for passed unit if FIR doesnt exist or isnt passed', () => {
      expect(
        getMaxCloudLowerLevelValue(
          'FT' as LevelUnits,
          'EHFG',
          airmetConfigMultipleFIR,
        ),
      ).toBe(airmetConfigMultipleFIR.fir_areas.EHAA.cloud_lower_level_max!.FT);
      expect(
        getMaxCloudLowerLevelValue(
          'FT' as LevelUnits,
          undefined!,
          airmetConfigMultipleFIR,
        ),
      ).toBe(airmetConfigMultipleFIR.fir_areas.EHAA.cloud_lower_level_max!.FT);
    });
    it('should return null if unit not present or if level_max not set in config', () => {
      expect(
        getMaxCloudLowerLevelValue(
          'FAKEUNIT' as LevelUnits,
          'EHFG',
          airmetConfigMultipleFIR,
        ),
      ).toBe(null);
      // cloud_lower_level_max not set in config
      expect(
        getMaxCloudLowerLevelValue(
          'M' as LevelUnits,
          'EDDD',
          airmetConfigMultipleFIR,
        ),
      ).toBe(null);
    });
  });

  describe('getMaxLevelValue', () => {
    it('should return the max of FIR for passed unit', () => {
      expect(getMaxLevelValue('FL' as LevelUnits, 'EHAA', config)).toBe(
        config.fir_areas.EHAA.level_max!.FL,
      );
      expect(
        getMaxLevelValue('FL' as LevelUnits, 'EBBU', configMultipleFIR),
      ).toBe(configMultipleFIR.fir_areas.EBBU.level_max!.FL);
    });
    it('should return the max of first FIR for passed unit if FIR doesnt exist or isnt passed', () => {
      expect(
        getMaxLevelValue('FL' as LevelUnits, 'EHFG', configMultipleFIR),
      ).toBe(configMultipleFIR.fir_areas.EHAA.level_max!.FL);
      expect(
        getMaxLevelValue('FL' as LevelUnits, undefined!, configMultipleFIR),
      ).toBe(configMultipleFIR.fir_areas.EHAA.level_max!.FL);
    });
    it('should return null if unit not present or if level_max not set in config', () => {
      expect(
        getMaxLevelValue('FAKEUNIT' as LevelUnits, 'EHFG', configMultipleFIR),
      ).toBe(null);
      // level_max not set in config
      expect(
        getMaxLevelValue('M' as LevelUnits, 'EDDD', configMultipleFIR),
      ).toBe(null);
    });
  });

  describe('getMinLevelValue', () => {
    it('should return the min of FIR for passed unit', () => {
      expect(getMinLevelValue('FL' as LevelUnits, 'EHAA', config)).toBe(
        config.fir_areas.EHAA.level_min!.FL,
      );
      expect(
        getMinLevelValue('FL' as LevelUnits, 'EBBU', configMultipleFIR),
      ).toBe(configMultipleFIR.fir_areas.EBBU.level_min!.FL);
    });
    it('should return the min of first FIR for passed unit if FIR doesnt exist or isnt passed', () => {
      expect(
        getMinLevelValue('FL' as LevelUnits, 'EHFG', configMultipleFIR),
      ).toBe(configMultipleFIR.fir_areas.EHAA.level_min!.FL);
      expect(
        getMinLevelValue('FL' as LevelUnits, undefined!, configMultipleFIR),
      ).toBe(configMultipleFIR.fir_areas.EHAA.level_min!.FL);
    });
    it('should return zero if unit not present or if level_min not set', () => {
      expect(
        getMinLevelValue('FAKEUNIT' as LevelUnits, 'EHFG', configMultipleFIR),
      ).toBe(DEFAULT_MINIMUM_SURFACE_LEVEL);
      // level_min not set in config
      expect(
        getMinLevelValue('M' as LevelUnits, 'EDDD', configMultipleFIR),
      ).toBe(DEFAULT_MINIMUM_SURFACE_LEVEL);
    });
  });

  describe('getMaxWindSpeedValue', () => {
    it('should return the min of FIR for passed unit', () => {
      expect(getMaxWindSpeedValue('KT', 'EHAA', airmetConfig)).toBe(
        airmetConfig.fir_areas.EHAA.wind_speed_max!.KT,
      );
      expect(getMaxWindSpeedValue('KT', 'EBBU', airmetConfigMultipleFIR)).toBe(
        airmetConfigMultipleFIR.fir_areas.EBBU.wind_speed_max!.KT,
      );
    });
    it('should return the min of first FIR for passed unit if FIR doesnt exist or isnt passed', () => {
      expect(getMaxWindSpeedValue('KT', 'EHFG', airmetConfigMultipleFIR)).toBe(
        airmetConfigMultipleFIR.fir_areas.EHAA.wind_speed_max!.KT,
      );
      expect(
        getMaxWindSpeedValue('KT', undefined!, airmetConfigMultipleFIR),
      ).toBe(airmetConfigMultipleFIR.fir_areas.EHAA.wind_speed_max!.KT);
    });
    it('should return null if unit not present or if wind_speed_max not set', () => {
      expect(
        getMaxWindSpeedValue(
          'FAKEUNIT' as LevelUnits,
          'EHFG',
          airmetConfigMultipleFIR,
        ),
      ).toBe(null);
      // wind_speed_max not set in config
      expect(
        getMaxWindSpeedValue(
          'M' as LevelUnits,
          'EDDD',
          airmetConfigMultipleFIR,
        ),
      ).toBe(null);
    });
  });

  describe('getMinWindSpeedValue', () => {
    it('should return the min of FIR for passed unit', () => {
      expect(getMinWindSpeedValue('KT', 'EHAA', airmetConfig)).toBe(
        airmetConfig.fir_areas.EHAA.wind_speed_min!.KT,
      );
      expect(getMinWindSpeedValue('KT', 'EBBU', airmetConfigMultipleFIR)).toBe(
        airmetConfigMultipleFIR.fir_areas.EBBU.wind_speed_min!.KT,
      );
    });
    it('should return the min of first FIR for passed unit if FIR doesnt exist or isnt passed', () => {
      expect(getMinWindSpeedValue('KT', 'EHFG', airmetConfigMultipleFIR)).toBe(
        airmetConfigMultipleFIR.fir_areas.EHAA.wind_speed_min!.KT,
      );
      expect(
        getMinWindSpeedValue('KT', undefined!, airmetConfigMultipleFIR),
      ).toBe(airmetConfigMultipleFIR.fir_areas.EHAA.wind_speed_min!.KT);
    });
    it('should return zero if unit not present or if wind_speed_min not set', () => {
      expect(
        getMinWindSpeedValue(
          'FAKEUNIT' as LevelUnits,
          'EHFG',
          airmetConfigMultipleFIR,
        ),
      ).toBe(DEFAULT_MIN_WIND_SPEED);
      // wind_speed_min not set in config
      expect(
        getMinWindSpeedValue(
          'M' as LevelUnits,
          'EDDD',
          airmetConfigMultipleFIR,
        ),
      ).toBe(DEFAULT_MIN_WIND_SPEED);
    });
  });

  describe('getMinVisibilityValue', () => {
    it('should return the min of FIR for passed unit', () => {
      expect(getMinVisibilityValue('EHAA', airmetConfig)).toBe(
        airmetConfig.fir_areas.EHAA.visibility_min,
      );
      expect(getMinVisibilityValue('EBBU', airmetConfigMultipleFIR)).toBe(
        airmetConfigMultipleFIR.fir_areas.EBBU.visibility_min,
      );
    });
    it('should return the min of first FIR for passed unit if FIR doesnt exist or isnt passed', () => {
      expect(getMinVisibilityValue('EHFG', airmetConfigMultipleFIR)).toBe(
        airmetConfigMultipleFIR.fir_areas.EHAA.visibility_min,
      );
      expect(getMinVisibilityValue(undefined!, airmetConfigMultipleFIR)).toBe(
        airmetConfigMultipleFIR.fir_areas.EHAA.visibility_min,
      );
    });
    it('should return zero if visibility_min not set', () => {
      expect(getMinVisibilityValue('EHFG', airmetConfigMultipleFIR)).toBe(
        DEFAULT_MIN_VISIBILITY,
      );
      // visibility_min not set in config
      expect(getMinVisibilityValue('EDDD', airmetConfigMultipleFIR)).toBe(
        DEFAULT_MIN_VISIBILITY,
      );
    });
  });

  describe('getMaxVisibilityValue', () => {
    it('should return the min of FIR for passed unit', () => {
      expect(getMaxVisibilityValue('EHAA', airmetConfig)).toBe(
        airmetConfig.fir_areas.EHAA.visibility_max,
      );
      expect(getMaxVisibilityValue('EBBU', airmetConfigMultipleFIR)).toBe(
        airmetConfigMultipleFIR.fir_areas.EBBU.visibility_max,
      );
    });
    it('should return the min of first FIR for passed unit if FIR doesnt exist or isnt passed', () => {
      expect(getMaxVisibilityValue('EHFG', airmetConfigMultipleFIR)).toBe(
        airmetConfigMultipleFIR.fir_areas.EHAA.visibility_max,
      );
      expect(getMaxVisibilityValue(undefined!, airmetConfigMultipleFIR)).toBe(
        airmetConfigMultipleFIR.fir_areas.EHAA.visibility_max,
      );
    });
    it('should return null if visibility_max not set', () => {
      // visibility_max not set in config
      expect(getMaxVisibilityValue('EDDD', airmetConfigMultipleFIR)).toBe(null);
    });
  });

  describe('getAllFirAreas', () => {
    it('should return fir_areas from config', () => {
      // airmet config
      const testFirAreas: Firareas = {
        EHAA: {
          fir_name: 'AMSTERDAM FIR',
          location_indicator_atsr: 'EHAA',
          location_indicator_atsu: 'EHAA',
          max_hours_of_validity: 4,
          hours_before_validity: 4,
          cloud_lower_level_min: {
            FT: 100,
          },
          cloud_lower_level_max: {
            FT: 900,
          },
          cloud_level_min: {
            FT: 100,
          },
          cloud_level_max: {
            FT: 9900,
          },
          wind_speed_min: {
            KT: 31,
          },
          wind_speed_max: {
            KT: 199,
          },
          visibility_max: 4900,
          visibility_min: 0,
          level_min: {
            FT: 100,
            FL: 50,
          },
          level_max: {
            FT: 4900,
            FL: 100,
          },
          level_rounding_FL: 100,
          level_rounding_FT: 5,
          level_rounding_M: 1,
          movement_min: {
            KT: 5,
            KMH: 10,
          },
          movement_max: {
            KT: 150,
            KMH: 99,
          },
          movement_rounding_kt: 5,
          movement_rounding_kmh: 10,
          units: [
            {
              unit_type: 'level_unit',
              allowed_units: ['FT', 'FL'],
            },
          ],
          fir_location: testFirArea,
        },
      };
      expect(
        getAllFirAreas({
          location_indicator_mwo: 'EHDB',
          fir_areas: testFirAreas,
          active_firs: ['EHAA'],
          valid_from_delay_minutes: 60,
          default_validity_minutes: 90,
        }),
      ).toEqual(testFirAreas);

      // airmet config
      const testFirAreas2: Firareas = {
        EHAA: {
          fir_name: 'AMSTERDAM FIR',
          fir_location: testFirArea,
          location_indicator_atsr: 'EHAA',
          location_indicator_atsu: 'EHAA',
          max_hours_of_validity: 4,
          hours_before_validity: 4,
          cloud_lower_level_min: {
            FT: 100,
          },
          cloud_lower_level_max: {
            FT: 900,
          },

          cloud_level_min: {
            FT: 100,
          },
          cloud_level_max: {
            FT: 9900,
          },
          wind_speed_min: {
            KT: 31,
          },
          wind_speed_max: {
            KT: 199,
          },
          visibility_max: 4900,
          visibility_min: 0,
          level_min: {
            FT: 100,
            FL: 50,
          },
          level_max: {
            FT: 4900,
            FL: 650,
          },
          level_rounding_FL: 100,
          level_rounding_FT: 5,
          level_rounding_M: 1,
          movement_min: {
            KT: 5,
            KMH: 10,
          },
          movement_max: {
            KT: 150,
            KMH: 99,
          },
          movement_rounding_kt: 5,
          movement_rounding_kmh: 10,
          units: [
            {
              unit_type: 'level_unit',
              allowed_units: ['FT', 'FL'],
            },
          ],
        },
        EBBU: {
          fir_name: 'Some OTHER FIR',
          fir_location: testFirArea,
          location_indicator_atsr: 'EBBU',
          location_indicator_atsu: 'EBBU',
          area_preset: 'OTHER_FIR',
          max_hours_of_validity: 6,
          hours_before_validity: 7,
          cloud_lower_level_min: {
            FT: 100,
          },
          cloud_lower_level_max: {
            FT: 900,
          },

          cloud_level_min: {
            FT: 100,
          },
          cloud_level_max: {
            FT: 9900,
          },
          wind_speed_min: {
            KT: 31,
          },
          wind_speed_max: {
            KT: 199,
          },
          visibility_max: 4900,
          visibility_min: 0,
          level_min: {
            FT: 100,
            FL: 50,
          },
          level_max: {
            FT: 4900,
            FL: 650,
          },
          level_rounding_FL: 100,
          level_rounding_FT: 5,
          level_rounding_M: 1,
          movement_min: {
            KT: 5,
            KMH: 10,
          },
          movement_max: {
            KT: 150,
            KMH: 99,
          },
          movement_rounding_kt: 5,
          movement_rounding_kmh: 10,
          units: [
            {
              unit_type: 'level_unit',
              allowed_units: ['FT', 'FL'],
            },
            {
              unit_type: 'movement_unit',
              allowed_units: ['KT'],
            },
          ],
        },
      };
      expect(
        getAllFirAreas({
          location_indicator_mwo: 'EHDB',
          fir_areas: testFirAreas2,
          active_firs: ['EHAA', 'EBBU'],
          valid_from_delay_minutes: 60,
          default_validity_minutes: 90,
        }),
      ).toEqual(testFirAreas2);
    });
  });

  describe('getFirOptions', () => {
    it('should return getFirOptions', () => {
      const testFirAreas: Firareas = {
        EHAA: {
          fir_name: 'AMSTERDAM FIR',
          fir_location: testFirArea,
          location_indicator_atsr: 'EHAA',
          location_indicator_atsu: 'EHAA',
          max_hours_of_validity: 4,
          hours_before_validity: 4,
          cloud_lower_level_min: {
            FT: 100,
          },
          cloud_lower_level_max: {
            FT: 900,
          },
          cloud_level_min: {
            FT: 100,
          },
          cloud_level_max: {
            FT: 9900,
          },
          wind_speed_min: {
            KT: 31,
          },
          wind_speed_max: {
            KT: 199,
          },
          visibility_max: 4900,
          visibility_min: 0,
          level_min: {
            FT: 100,
            FL: 50,
          },
          level_max: {
            FT: 4900,
            FL: 100,
          },
          level_rounding_FL: 100,
          level_rounding_FT: 5,
          level_rounding_M: 1,
          movement_min: {
            KT: 5,
            KMH: 10,
          },
          movement_max: {
            KT: 150,
            KMH: 99,
          },
          movement_rounding_kt: 5,
          movement_rounding_kmh: 10,
          units: [
            {
              unit_type: 'level_unit',
              allowed_units: ['FT', 'FL'],
            },
          ],
        },
      };
      expect(
        getFirOptions({
          location_indicator_mwo: 'EHDB',
          fir_areas: testFirAreas,
          active_firs: ['EHAA'],
          valid_from_delay_minutes: 30,
          default_validity_minutes: 90,
        }),
      ).toEqual([
        {
          ATSR: testFirAreas.EHAA.location_indicator_atsr,
          FIR: testFirAreas.EHAA.fir_name,
        },
      ]);
      // airmet config
      const testFirAreas2: Firareas = {
        EHAA: {
          fir_name: 'AMSTERDAM FIR',
          fir_location: testFirArea,
          location_indicator_atsr: 'EHAA',
          location_indicator_atsu: 'EHAA',
          max_hours_of_validity: 4,
          hours_before_validity: 4,
          cloud_lower_level_min: {
            FT: 100,
          },
          cloud_lower_level_max: {
            FT: 900,
          },

          cloud_level_min: {
            FT: 100,
          },
          cloud_level_max: {
            FT: 9900,
          },
          wind_speed_min: {
            KT: 31,
          },
          wind_speed_max: {
            KT: 199,
          },
          visibility_max: 4900,
          visibility_min: 0,
          level_min: {
            FT: 100,
            FL: 50,
          },
          level_max: {
            FT: 4900,
            FL: 650,
          },
          level_rounding_FL: 100,
          level_rounding_FT: 5,
          level_rounding_M: 1,
          movement_min: {
            KT: 5,
            KMH: 10,
          },
          movement_max: {
            KT: 150,
            KMH: 99,
          },
          movement_rounding_kt: 5,
          movement_rounding_kmh: 10,
          units: [
            {
              unit_type: 'level_unit',
              allowed_units: ['FT', 'FL'],
            },
          ],
        },
        EBBU: {
          fir_name: 'Some OTHER FIR',
          fir_location: testFirArea,
          location_indicator_atsr: 'EBBU',
          location_indicator_atsu: 'EBBU',
          area_preset: 'OTHER_FIR',
          max_hours_of_validity: 6,
          hours_before_validity: 7,
          cloud_lower_level_min: {
            FT: 100,
          },
          cloud_lower_level_max: {
            FT: 900,
          },

          cloud_level_min: {
            FT: 100,
          },
          cloud_level_max: {
            FT: 9900,
          },
          wind_speed_min: {
            KT: 31,
          },
          wind_speed_max: {
            KT: 199,
          },
          visibility_max: 4900,
          visibility_min: 0,
          level_min: {
            FT: 100,
            FL: 50,
          },
          level_max: {
            FT: 4900,
            FL: 650,
          },
          level_rounding_FL: 100,
          level_rounding_FT: 5,
          level_rounding_M: 1,
          movement_min: {
            KT: 5,
            KMH: 10,
          },
          movement_max: {
            KT: 150,
            KMH: 99,
          },
          movement_rounding_kt: 5,
          movement_rounding_kmh: 10,
          adjacent_firs: ['HIP', 'KNEE', 'ALPHA', 'BRAVO'],
          units: [
            {
              unit_type: 'level_unit',
              allowed_units: ['FT', 'FL'],
            },
            {
              unit_type: 'movement_unit',
              allowed_units: ['KT'],
            },
          ],
        },
      };
      expect(
        getFirOptions({
          location_indicator_mwo: 'EHDB',
          fir_areas: testFirAreas2,
          active_firs: ['EHAA', 'EBBU'],
          valid_from_delay_minutes: 30,
          default_validity_minutes: 90,
        }),
      ).toEqual([
        {
          ATSR: testFirAreas2.EHAA.location_indicator_atsr,
          FIR: testFirAreas2.EHAA.fir_name,
        },
        {
          ATSR: testFirAreas2.EBBU.location_indicator_atsr,
          FIR: testFirAreas2.EBBU.fir_name,
        },
      ]);
    });
  });

  describe('getLevelInFeet', () => {
    it('should convert meters to feet correctly', () => {
      expect(getLevelInFeet(15.24, 'M' as LevelUnits)).toBe(50);
    });
    it('should convert flight level to feet correctly', () => {
      expect(getLevelInFeet(5, 'FL' as LevelUnits)).toBe(500);
    });
    it('should return same value if passed in value is already in feet', () => {
      expect(getLevelInFeet(100, 'FT' as LevelUnits)).toBe(100);
    });
  });

  describe('isLevelLower', () => {
    it('should return true when first parameter is lower than second parameter and both units are the same', () => {
      expect(
        isLevelLower(1, 'M' as LevelUnits, 2, 'M' as LevelUnits),
      ).toBeTruthy();

      expect(
        isLevelLower(1, 'FT' as LevelUnits, 2, 'FT' as LevelUnits),
      ).toBeTruthy();
      expect(
        isLevelLower(1, 'FL' as LevelUnits, 2, 'FL' as LevelUnits),
      ).toBeTruthy();
    });

    it('should return false when first parameter is higher than or equal to second parameter and both units are the same', () => {
      expect(
        isLevelLower(2, 'M' as LevelUnits, 1, 'M' as LevelUnits),
      ).toBeFalsy();
      expect(
        isLevelLower(2, 'FT' as LevelUnits, 1, 'FT' as LevelUnits),
      ).toBeFalsy();
      expect(
        isLevelLower(2, 'FL' as LevelUnits, 1, 'FL' as LevelUnits),
      ).toBeFalsy();
      expect(
        isLevelLower(1, 'M' as LevelUnits, 1, 'M' as LevelUnits),
      ).toBeFalsy();
      expect(
        isLevelLower(2, 'FT' as LevelUnits, 2, 'FT' as LevelUnits),
      ).toBeFalsy();
      expect(
        isLevelLower(2, 'FL' as LevelUnits, 2, 'FL' as LevelUnits),
      ).toBeFalsy();
    });

    it('should do a coorect conversion to determine if lower when units differ', () => {
      // meters and ft
      expect(
        isLevelLower(10, 'M' as LevelUnits, 33, 'FT' as LevelUnits),
      ).toBeTruthy();
      expect(
        isLevelLower(10, 'M' as LevelUnits, 32, 'FT' as LevelUnits),
      ).toBeFalsy();

      // m and FL
      expect(
        isLevelLower(30, 'M' as LevelUnits, 1, 'FL' as LevelUnits),
      ).toBeTruthy();
      expect(
        isLevelLower(31, 'M' as LevelUnits, 1, 'FL' as LevelUnits),
      ).toBeFalsy();

      // ft and FL
      expect(
        isLevelLower(1, 'FL' as LevelUnits, 101, 'FT' as LevelUnits),
      ).toBeTruthy();
      expect(
        isLevelLower(1, 'FL' as LevelUnits, 99, 'FT' as LevelUnits),
      ).toBeFalsy();
    });
  });

  describe('getProductFormMode', () => {
    it('should return new if passed canbe of [DRAFTED, DISCARDED, PUBLISHED] and sigmet = null (passed when new)', () => {
      const canBe = ['DRAFTED', 'DISCARDED', 'PUBLISHED'] as ProductCanbe[];
      expect(getProductFormMode(canBe, null!)).toBe('new');
    });
    it('should return edit if passed canbe of [DRAFTED, DISCARDED, PUBLISHED] (passed when draft)', () => {
      const canBe = ['DRAFTED', 'DISCARDED', 'PUBLISHED'] as ProductCanbe[];
      expect(getProductFormMode(canBe, fakeSigmetList[0])).toBe('edit');
    });
    it('should return view if passed canbe of [CANCELLED] (passed when published sigmet)', () => {
      const canBe = ['CANCELLED'] as ProductCanbe[];
      expect(getProductFormMode(canBe, fakeSigmetList[2])).toBe('view');
    });
    it('should return view if passed canbe of [] (passed when cancel/cancelled or expired sigmet)', () => {
      const canBe = [] as ProductCanbe[];
      expect(getProductFormMode(canBe, fakeSigmetList[6])).toBe('view');
    });
  });

  describe('getDialogtitle', () => {
    it('should return --New SIGMET-- or --New AIRMET-- if null is passed', () => {
      expect(getDialogtitle(null!, 'sigmet')).toBe('New SIGMET');
      expect(getDialogtitle(null!, 'airmet')).toBe('New AIRMET');
    });

    it('should return --SIGMET {phenomenon} - saved as draft-- if sigmet with status draft is passed', () => {
      const { sigmet } = fakeSigmetList[0];
      expect(getDialogtitle(sigmet, 'sigmet')).toBe(
        `SIGMET ${
          isInstanceOfSigmet(sigmet) &&
          SigmetPhenomena[
            sigmet.phenomenon as unknown as keyof typeof SigmetPhenomena
          ]
        } - saved as draft`,
      );
    });
    it('should return --AIRMET {phenomenon} - saved as draft-- if airmet with status draft is passed', () => {
      const { airmet } = fakeAirmetList[0];
      expect(getDialogtitle(airmet, 'airmet')).toBe(
        `AIRMET ${
          isInstanceOfAirmet(airmet) &&
          AirmetPhenomena[
            airmet.phenomenon as unknown as keyof typeof AirmetPhenomena
          ]
        } - saved as draft`,
      );
    });

    it('should return --SIGMET {phenomenon} - {status description}-- if sigmet with status published/expired/cancelled is passed', () => {
      // for published sigmet
      const { sigmet } = fakeSigmetList[2];
      expect(getDialogtitle(sigmet, 'sigmet')).toBe(
        `SIGMET ${
          isInstanceOfSigmet(sigmet) &&
          SigmetPhenomena[
            sigmet.phenomenon as unknown as keyof typeof SigmetPhenomena
          ]
        } - ${ProductStatusDescription[sigmet.status]}`,
      );

      // for expired sigmet
      const { sigmet: sigmet2 } = fakeSigmetList[6];
      expect(getDialogtitle(sigmet2, 'sigmet')).toBe(
        `SIGMET ${
          isInstanceOfSigmet(sigmet2) &&
          SigmetPhenomena[
            sigmet2.phenomenon as unknown as keyof typeof SigmetPhenomena
          ]
        } - ${ProductStatusDescription[sigmet2.status]}`,
      );

      // For cancelled sigmet
      const { sigmet: sigmet3 } = fakeSigmetList[5];
      expect(getDialogtitle(sigmet3, 'sigmet')).toBe(
        `SIGMET ${
          isInstanceOfSigmet(sigmet3) &&
          SigmetPhenomena[
            sigmet3.phenomenon as unknown as keyof typeof SigmetPhenomena
          ]
        } - ${ProductStatusDescription[sigmet3.status]}`,
      );
    });

    it('should return --Cancels SIGMET {seqnum}-- if cancel sigmet is passed', () => {
      const { sigmet } = fakeSigmetList[3];
      expect(getDialogtitle(sigmet, 'sigmet')).toBe(
        `SIGMET Cancels ${
          isInstanceOfCancelSigmet(sigmet) && sigmet.cancelsSigmetSequenceId
        }`,
      );
    });

    it('should return --Cancels AIRMET {seqnum}-- if cancel airmet is passed', () => {
      const { airmet } = fakeAirmetList[3];
      expect(getDialogtitle(airmet, 'airmet')).toBe(
        `AIRMET Cancels ${
          isInstanceOfCancelAirmet(airmet) && airmet.cancelsAirmetSequenceId
        }`,
      );
    });
  });

  describe('getProductFormLifecycleButtons', () => {
    it('should trigger discard', () => {
      const props = {
        canBe: ['DISCARDED'],
        onButtonPress: jest.fn(),
      };

      render(
        getProductFormLifecycleButtons(
          props.canBe,
          props.onButtonPress,
        ) as React.JSX.Element,
      );

      fireEvent.click(screen.getByTestId('productform-dialog-discard'));
      expect(props.onButtonPress).toHaveBeenCalledWith(props.canBe[0]);
    });

    it('should trigger saving as draft', () => {
      const props = {
        canBe: ['DRAFTED'],
        onButtonPress: jest.fn(),
      };
      render(
        getProductFormLifecycleButtons(
          props.canBe,
          props.onButtonPress,
        ) as React.JSX.Element,
      );

      fireEvent.click(screen.getByTestId('productform-dialog-draft'));
      expect(props.onButtonPress).toHaveBeenCalledWith('DRAFT');
    });

    it('should trigger cancel', () => {
      const props = {
        canBe: ['CANCELLED'],
        onButtonPress: jest.fn(),
      };
      render(
        getProductFormLifecycleButtons(
          props.canBe,
          props.onButtonPress,
        ) as React.JSX.Element,
      );

      fireEvent.click(screen.getByTestId('productform-dialog-cancel'));
      expect(props.onButtonPress).toHaveBeenCalledWith(props.canBe[0]);
    });

    it('should trigger publish', () => {
      const props = {
        canBe: ['PUBLISHED'],
        onButtonPress: jest.fn(),
      };
      render(
        getProductFormLifecycleButtons(
          props.canBe,
          props.onButtonPress,
        ) as React.JSX.Element,
      );

      fireEvent.click(screen.getByTestId('productform-dialog-publish'));
      expect(props.onButtonPress).toHaveBeenCalledWith(props.canBe[0]);
    });

    it('should render mobile list if canbe includes DISCARDED', () => {
      render(
        getProductFormLifecycleButtons(
          ['DISCARDED'],
          jest.fn(),
        ) as React.JSX.Element,
      );
      expect(screen.getByTestId('toggleMenuButton')).toBeTruthy();
    });

    it('should render mobile list if canbe includes DRAFTED', () => {
      render(
        getProductFormLifecycleButtons(
          ['DRAFTED'],
          jest.fn(),
        ) as React.JSX.Element,
      );
      expect(screen.getByTestId('toggleMenuButton')).toBeTruthy();
    });

    it('should render mobile list if canbe includes DRAFT', () => {
      render(
        getProductFormLifecycleButtons(
          ['PUBLISHED'],
          jest.fn(),
        ) as React.JSX.Element,
      );
      expect(screen.getByTestId('toggleMenuButton')).toBeTruthy();
    });

    it('should render mobile list if canbe includes CANCELLED', () => {
      render(
        getProductFormLifecycleButtons(
          ['CANCELLED'],
          jest.fn(),
        ) as React.JSX.Element,
      );
      expect(screen.getByTestId('toggleMenuButton')).toBeTruthy();
    });

    it('should not render mobile list if no items', () => {
      render(
        getProductFormLifecycleButtons(
          ['CANCEL'],
          jest.fn(),
        ) as React.JSX.Element,
      );
      expect(screen.queryByTestId('toggleMenuButton')).toBeFalsy();
    });
  });

  describe('hasValue', () => {
    it('should return true when not null or NaN or empty string', () => {
      expect(hasValue(NaN)).toBeFalsy();
      expect(hasValue(null!)).toBeFalsy();
      expect(hasValue(1)).toBeTruthy();
      expect(hasValue(0)).toBeTruthy();
      expect(hasValue(1.02)).toBeTruthy();
      expect(hasValue('')).toBeFalsy();
      expect(hasValue('1')).toBeTruthy();
    });
  });

  describe('prepareFormValues', () => {
    it('should remove empty numeric field values', () => {
      const { level, ...fakeSigmet } = fakeSigmetList[0].sigmet as Sigmet;
      const product = {
        fakeSigmet,
        vaSigmetVolcanoCoordinates: { latitude: NaN, longitude: NaN },
        level: {
          unit: LevelUnits.FL,
          value: NaN,
        },
      } as unknown as Sigmet;

      expect(prepareFormValues(product)).toEqual({
        fakeSigmet,
      });
    });

    it('should remove IS_DRAFT', () => {
      const { level, ...fakeSigmet } = fakeSigmetList[0].sigmet as Sigmet;
      const product = {
        fakeSigmet,
        vaSigmetVolcanoCoordinates: { latitude: NaN, longitude: NaN },
        level: {
          unit: LevelUnits.FL,
          value: NaN,
        },
        IS_DRAFT: true,
      } as unknown as Sigmet;

      // eslint-disable-next-line @typescript-eslint/no-explicit-any
      expect(prepareFormValues(product as any).IS_DRAFT).toBeUndefined();
    });

    it('should remove all values for empty coordinates', () => {
      const testProduct = {
        endGeometry: {
          type: 'FeatureCollection',
          features: [
            {
              type: 'Feature',
              properties: {
                stroke: '#6e1e91',
                'stroke-width': 1.5,
                'stroke-opacity': 1,
                fill: '#6e1e91',
                'fill-opacity': 0.25,
                selectionType: 'poly',
              },
              geometry: {
                type: 'Polygon',
                coordinates: [[]],
              },
            },
          ],
        },
        endGeometryIntersect: {
          type: 'FeatureCollection',
          features: [
            {
              type: 'Feature',
              properties: {
                stroke: '#6e1e91',
                'stroke-width': 1.5,
                'stroke-opacity': 1,
                fill: '#6e1e91',
                'fill-opacity': 0.5,
                selectionType: 'poly',
              },
              geometry: {
                type: 'Polygon',
                coordinates: [[]],
              },
            },
          ],
        },
      };

      const result = prepareFormValues(
        testProduct as unknown as Sigmet,
      ) as Sigmet;
      expect(result.endGeometry).toBeUndefined();
      expect(result.endGeometryIntersect).toBeUndefined();

      const testProduct2 = {
        endGeometry: {
          type: 'FeatureCollection',
          features: [
            {
              type: 'Feature',
              properties: {
                stroke: '#6e1e91',
                'stroke-width': 1.5,
                'stroke-opacity': 1,
                fill: '#6e1e91',
                'fill-opacity': 0.25,
                selectionType: 'poly',
              },
              geometry: {
                type: 'Polygon',
                coordinates: [
                  [5, 55],
                  [4.331914, 55.332644],
                  [3.368817, 55.764314],
                  [2.761908, 54.379261],
                ],
              },
            },
          ],
        },
        endGeometryIntersect: {
          type: 'FeatureCollection',
          features: [
            {
              type: 'Feature',
              properties: {
                stroke: '#6e1e91',
                'stroke-width': 1.5,
                'stroke-opacity': 1,
                fill: '#6e1e91',
                'fill-opacity': 0.5,
                selectionType: 'poly',
              },
              geometry: {
                type: 'Polygon',
                coordinates: [
                  [5, 55],
                  [4.331914, 55.332644],
                  [3.368817, 55.764314],
                  [2.761908, 54.379261],
                ],
              },
            },
          ],
        },
      };

      const result2 = prepareFormValues(
        testProduct2 as unknown as Sigmet,
      ) as Sigmet;
      expect(result2.endGeometry).toEqual(result2.endGeometry);
      expect(result2.endGeometryIntersect).toEqual(
        result2.endGeometryIntersect,
      );
    });

    it('should remove all values for empty coordinates of Point', () => {
      const testProduct = {
        endGeometry: {
          type: 'FeatureCollection',
          features: [
            {
              type: 'Feature',
              geometry: {
                type: 'Point',
                coordinates: [],
              },
            },
          ],
        },
        startGeometry: {
          type: 'FeatureCollection',
          features: [
            {
              type: 'Feature',
              geometry: {
                type: 'Point',
                coordinates: [5, 52],
              },
            },
          ],
        },
      };

      const result = prepareFormValues(
        testProduct as unknown as Sigmet,
      ) as Sigmet;
      expect(result.endGeometry).toBeUndefined();
      expect(result.startGeometry).toBeDefined();
    });

    it('should remove empty string values', () => {
      const { isObservationOrForecast, movementType, change, ...fakeSigmet } =
        fakeSigmetList[0].sigmet as Sigmet;
      const product = {
        fakeSigmet,
        isObservationOrForecast: '',
        movementType: '',
        change: '',
      } as unknown as Sigmet;

      expect(prepareFormValues(product)).toEqual({
        fakeSigmet,
      });
    });

    it('should remove all values for empty feature collections', () => {
      const testProduct = {
        endGeometry: {
          type: 'FeatureCollection',
          features: [],
        },
        endGeometryIntersect: {
          type: 'FeatureCollection',
          features: [],
        },
      };

      const result = prepareFormValues(
        testProduct as unknown as Sigmet,
      ) as Sigmet;
      expect(result.endGeometry).toBeUndefined();
      expect(result.endGeometryIntersect).toBeUndefined();
    });
  });

  describe('createSigmetPostParameter', () => {
    it('should not have a uuid when posting a new sigmet', () => {
      expect(fakeNewSigmet.uuid).toBeFalsy();
      expect(createSigmetPostParameter(fakeNewSigmet, 'DRAFT', null!)).toEqual({
        changeStatusTo: 'DRAFT',
        sigmet: fakeNewSigmet,
      });
    });
    it('should have a uuid when posting an existing sigmet', () => {
      const updatedSigmet = {
        ...fakeSigmetList[0].sigmet,
        uuid: null!,
        change: 'NC' as Sigmet['change'],
      };
      expect(
        createSigmetPostParameter(
          updatedSigmet,
          'DRAFT',
          fakeSigmetList[0].sigmet.uuid!,
        ),
      ).toEqual({
        changeStatusTo: 'DRAFT',
        sigmet: { ...updatedSigmet, uuid: fakeSigmetList[0].sigmet.uuid },
      });
    });

    it('should not have post volcanic coordinate fields if non passed in or if empty', () => {
      // not passed
      expect(
        createSigmetPostParameter(fakeSigmetList[0].sigmet, 'DRAFT', null!),
      ).toMatchObject({
        changeStatusTo: 'DRAFT',
        sigmet: {
          ...fakeSigmetList[0].sigmet,
          uuid: fakeSigmetList[0].sigmet.uuid,
        },
      });

      // empty
      const updatedSigmet = {
        ...fakeSigmetList[0].sigmet,
        vaSigmetVolcanoCoordinates: { latitude: NaN, longitude: NaN },
      };
      expect(
        createSigmetPostParameter(
          // eslint-disable-next-line @typescript-eslint/ban-ts-comment
          // @ts-ignore
          updatedSigmet,
          'DRAFT',
          fakeSigmetList[0].sigmet.uuid!,
        ),
      ).toEqual({
        changeStatusTo: 'DRAFT',
        sigmet: fakeSigmetList[0].sigmet,
      });
    });

    it('should post volcanic coordinate fields if non-empty', () => {
      expect(
        createSigmetPostParameter(fakeSigmetList[1].sigmet, 'DRAFT', null!),
      ).toMatchObject({
        changeStatusTo: 'DRAFT',
        sigmet: fakeSigmetList[1].sigmet,
      });
    });
  });

  describe('createAirmetPostParameter', () => {
    it('should not have a uuid when posting a new airmet', () => {
      expect(fakeNewAirmet.uuid).toBeFalsy();
      expect(createAirmetPostParameter(fakeNewAirmet, 'DRAFT', null!)).toEqual({
        changeStatusTo: 'DRAFT',
        airmet: fakeNewAirmet,
      });
    });
    it('should have a uuid when posting an existing airmet', () => {
      const updatedAirmet = {
        ...fakeAirmetList[0].airmet,
        uuid: null!,
        change: 'NC' as Airmet['change'],
      };
      expect(
        createAirmetPostParameter(
          updatedAirmet,
          'DRAFT',
          fakeAirmetList[0].airmet.uuid!,
        ),
      ).toEqual({
        changeStatusTo: 'DRAFT',
        airmet: { ...updatedAirmet, uuid: fakeAirmetList[0].airmet.uuid },
      });
    });
  });

  describe('triggerValidations', () => {
    it('should trigger validation on each given field if it has a value', async () => {
      const fieldNames = ['field1', 'field2'];
      const trigger = jest.fn();
      const getValues = jest.fn().mockReturnValue('hasValue');
      triggerValidations(fieldNames, getValues, trigger);
      await waitFor(() => {
        expect(getValues).toHaveBeenCalledTimes(fieldNames.length);
      });
      expect(trigger).toHaveBeenCalledTimes(fieldNames.length);
      expect(trigger).toHaveBeenCalledWith(fieldNames[0]);
      expect(trigger).toHaveBeenCalledWith(fieldNames[1]);
    });
    it('should not trigger validation on a field without a value', async () => {
      const fieldNames = ['field1', 'field2', 'field3'];
      const trigger = jest.fn();
      const getValues = jest
        .fn()
        .mockReturnValueOnce('') // field1
        .mockReturnValueOnce(0) // field2
        .mockReturnValue('value3'); // field3
      triggerValidations(fieldNames, getValues, trigger);
      await waitFor(() => {
        expect(getValues).toHaveBeenCalledTimes(fieldNames.length);
      });
      expect(trigger).toHaveBeenCalledTimes(2);
      expect(trigger).not.toHaveBeenCalledWith(fieldNames[0]);
      expect(trigger).toHaveBeenCalledWith(fieldNames[1]);
      expect(trigger).toHaveBeenCalledWith(fieldNames[2]);
    });
  });

  describe('useDelayedFormValues', () => {
    beforeEach(() => {
      jest.useFakeTimers();
      jest.spyOn(global, 'clearTimeout');
      jest.spyOn(global, 'setTimeout');
    });
    afterEach(() => {
      jest.clearAllTimers();
      jest.useRealTimers();
    });
    it('should set intial values', () => {
      const expectedData = fakeSigmetList[0].sigmet;
      const { getValues, register, delay } = {
        getValues: (): unknown => expectedData,
        register: jest.fn(),
        delay: 500,
      };

      const { result } = renderHook(() =>
        useDelayedFormValues(getValues, register, delay),
      );

      expect(result.current[0]).toEqual(expectedData);

      result.current[1]();
      expect(setTimeout).toHaveBeenLastCalledWith(expect.any(Function), 500);
    });
    it('should return callable function to trigger update form values and update the form values after timeout', async () => {
      const expectedData = fakeSigmetList[0].sigmet;
      const expectedData2 = fakeSigmetList[1].sigmet;
      const getExpectedData = jest
        .fn()
        .mockImplementationOnce(() => expectedData)
        .mockImplementationOnce(() => expectedData)
        .mockImplementationOnce(() => expectedData2);
      const { getValues, register, delay } = {
        getValues: (): unknown => getExpectedData,
        register: jest.fn(),
        delay: 500,
      };
      const { result, rerender } = renderHook(() =>
        useDelayedFormValues(getValues, register, delay),
      );
      expect(result.current[0]).toEqual(expectedData);

      result.current[1]();
      expect(clearTimeout).toHaveBeenCalled();
      expect(setTimeout).toHaveBeenLastCalledWith(expect.any(Function), 500);

      act(() => {
        jest.runOnlyPendingTimers();
      });

      rerender();
      expect(result.current[0]).toEqual(expectedData2);
    });
    it('should update formvalues if getValues is changed', async () => {
      const expectedData = fakeSigmetList[0].sigmet;
      const { getValues, register, delay } = {
        getValues: (): unknown => expectedData,
        register: jest.fn(),
        delay: 500,
      };

      const { result, rerender } = renderHook(
        ({ getValues }) => useDelayedFormValues(getValues, register, delay),
        {
          initialProps: { getValues },
        },
      );

      expect(result.current[0]).toEqual(expectedData);

      const expectedData2 = fakeSigmetList[1].sigmet;
      const getValues2 = (): unknown => expectedData2;
      rerender({ getValues: getValues2 });
      expect(result.current[0]).toEqual(expectedData2);
    });
  });

  describe('getConfirmationDialogTitle', () => {
    it('Should return Save properties when action = CLOSED', () => {
      expect(getConfirmationDialogTitle('CLOSED', 'sigmet')).toEqual(
        'Close SIGMET',
      );
      expect(getConfirmationDialogTitle('CLOSED', 'airmet')).toEqual(
        'Close AIRMET',
      );
    });
    it('Should return Save properties when action = DISCARDED', () => {
      expect(getConfirmationDialogTitle('DISCARDED', 'sigmet')).toEqual(
        'Discard SIGMET',
      );
      expect(getConfirmationDialogTitle('DISCARDED', 'airmet')).toEqual(
        'Discard AIRMET',
      );
    });
    it('Should return Save properties when action = PUBLISHED', () => {
      expect(getConfirmationDialogTitle('PUBLISHED', 'sigmet')).toEqual(
        'Publish',
      );
      expect(getConfirmationDialogTitle('PUBLISHED', 'airmet')).toEqual(
        'Publish',
      );
    });
    it('Should return Save properties when action = CANCELLED', () => {
      expect(getConfirmationDialogTitle('CANCELLED', 'sigmet')).toEqual(
        'Cancel SIGMET',
      );
      expect(getConfirmationDialogTitle('CANCELLED', 'airmet')).toEqual(
        'Cancel AIRMET',
      );
    });
  });

  describe('getConfirmationDialogContent', () => {
    it('Should return correct content when action = CLOSED', () => {
      expect(getConfirmationDialogContent('CLOSED', 'sigmet')).toEqual(
        'Do you want to save any changes made?',
      );
      expect(getConfirmationDialogContent('CLOSED', 'airmet')).toEqual(
        'Do you want to save any changes made?',
      );
    });
    it('Should return correct content when action = DISCARDED', () => {
      expect(getConfirmationDialogContent('DISCARDED', 'sigmet')).toEqual(
        'Are you sure you would like to discard this SIGMET? Its properties will be lost',
      );
      expect(getConfirmationDialogContent('DISCARDED', 'airmet')).toEqual(
        'Are you sure you would like to discard this AIRMET? Its properties will be lost',
      );
    });
    it('Should return correct content when action = PUBLISHED', () => {
      expect(getConfirmationDialogContent('PUBLISHED', 'sigmet')).toEqual(
        'Are you sure you want to publish this SIGMET?',
      );
      expect(getConfirmationDialogContent('PUBLISHED', 'airmet')).toEqual(
        'Are you sure you want to publish this AIRMET?',
      );
    });
    it('Should return correct content when action = CANCELLED', () => {
      expect(getConfirmationDialogContent('CANCELLED', 'airmet')).toEqual(
        'Are you sure you want to cancel this AIRMET?',
      );
      expect(getConfirmationDialogContent('CANCELLED', 'sigmet')).toEqual(
        'Are you sure you want to cancel this SIGMET?',
      );
    });
  });
  describe('getConfirmationDialogButtonLabel', () => {
    it('Should return correct button label when action = DISCARDED', () => {
      expect(getConfirmationDialogButtonLabel('DISCARDED', 'sigmet')).toEqual(
        'Discard SIGMET',
      );
      expect(getConfirmationDialogButtonLabel('DISCARDED', 'airmet')).toEqual(
        'Discard AIRMET',
      );
    });
    it('Should return correct button label when action = PUBLISHED', () => {
      expect(getConfirmationDialogButtonLabel('PUBLISHED', 'sigmet')).toEqual(
        'Publish',
      );
      expect(getConfirmationDialogButtonLabel('PUBLISHED', 'airmet')).toEqual(
        'Publish',
      );
    });
    it('Should return correct button label when action = CANCELLED', () => {
      expect(getConfirmationDialogButtonLabel('CANCELLED', 'sigmet')).toEqual(
        'Cancel',
      );
      expect(getConfirmationDialogButtonLabel('CANCELLED', 'airmet')).toEqual(
        'Cancel',
      );
    });
    it('Should return correct button label when other action', () => {
      expect(getConfirmationDialogButtonLabel('DRAFT', 'sigmet')).toEqual(
        'Save and close',
      );
      expect(getConfirmationDialogButtonLabel('DRAFT', 'airmet')).toEqual(
        'Save and close',
      );
    });
  });

  describe('getConfirmationDialogCancelLabel', () => {
    it('Should return correct button label when action = ClOSED', () => {
      expect(getConfirmationDialogCancelLabel('CLOSED')).toEqual(
        'Discard and close',
      );
    });

    it('Should return undefined', () => {
      expect(getConfirmationDialogCancelLabel('DRAFT')).toBeUndefined();
    });
  });

  describe('getProductIssueDate', () => {
    it('should return no issue date if not published yet', () => {
      expect(getProductIssueDate(fakeNewSigmet, null!)).toEqual(
        '(Not published)',
      );
      expect(
        getProductIssueDate(fakeSigmetList[0].sigmet as Sigmet, null!),
      ).toEqual('(Not published)');
    });
    it('should return issue date if published and expired', () => {
      expect(
        getProductIssueDate(fakeSigmetList[1].sigmet as Sigmet, null!),
      ).toEqual(
        `${dateUtils.dateToString(
          dateUtils.utc(fakeSigmetList[1].sigmet.issueDate),
          'yyyy/MM/dd HH:mm',
        )} UTC`,
      );
      expect(
        getProductIssueDate(fakeSigmetList[4].sigmet as Sigmet, null!),
      ).toEqual(
        `${dateUtils.dateToString(
          dateUtils.utc(fakeSigmetList[1].sigmet.issueDate),
          'yyyy/MM/dd HH:mm',
        )} UTC`,
      );
    });
    it('should return issue date if cancel sigmet', () => {
      expect(
        getProductIssueDate(null!, fakeSigmetList[3].sigmet as CancelSigmet),
      ).toEqual(
        `${dateUtils.dateToString(
          dateUtils.utc(fakeSigmetList[3].sigmet.issueDate),
          'yyyy/MM/dd HH:mm',
        )} UTC`,
      );
    });
  });

  describe('getAllowedUnits', () => {
    it('should return all units as defined in the passed type if no fir selected or unit type for selected fir not in config or if fir not present in options', () => {
      // no fir selected
      expect(
        getAllowedUnits(undefined!, config, 'movement_unit', MovementUnit),
      ).toBe(MovementUnit);
      // fir not present in options
      expect(
        getAllowedUnits('EGTT', config, 'movement_unit', MovementUnit),
      ).toBe(MovementUnit);
    });
    it('should return all units as defined in the passed type if unit type not provided for that FIR', () => {
      expect(
        getAllowedUnits(
          'EDDD',
          configMultipleFIR,
          'movement_unit',
          MovementUnit,
        ),
      ).toBe(MovementUnit);
    });
    it('should return units that are a cross section of the provided type and the allowed units', () => {
      expect(
        getAllowedUnits('EHAA', config, 'movement_unit', MovementUnit),
      ).toStrictEqual({ KT: 'kt' });
      // Units not available in the defined UnitType should be ignored - should ignore RANDOMUNIT as this is not defined in types
      expect(
        getAllowedUnits('EDDD', configMultipleFIR, 'level_unit', LevelUnits),
      ).toStrictEqual({ FT: 'ft', FL: 'FL' });
    });
  });

  describe('extractUnitsToShow', () => {
    it('should create a cross section of the provided units and the unit type ignoring any config units not in the type definition', () => {
      expect(extractUnitsToShow(['FT', 'FL'], LevelUnits)).toEqual({
        FT: 'ft',
        FL: 'FL',
      });
      expect(
        extractUnitsToShow(['FT', 'FL', 'ToBeIgnoredUnit'], LevelUnits),
      ).toEqual({
        FT: 'ft',
        FL: 'FL',
      });
      expect(extractUnitsToShow(['KT'], MovementUnit)).toEqual({ KT: 'kt' });
      expect(extractUnitsToShow(['FT', 'FL', 'M'], LevelUnits)).toEqual({
        FT: 'ft',
        FL: 'FL',
        M: 'm',
      });
    });
  });

  describe('getFir', () => {
    const testConfig = {
      location_indicator_mwo: 'EHDB',
      fir_areas: {
        EHAA: {
          fir_name: 'AMSTERDAM FIR',
          fir_location: {
            type: 'FeatureCollection',
            features: [
              {
                type: 'Feature',
                geometry: {
                  type: 'Polygon',
                  coordinates: [
                    [
                      [5.0, 55.0],
                      [4.331914, 55.332644],
                      [3.368817, 55.764314],
                      [2.761908, 54.379261],
                      [3.15576, 52.913554],
                      [2.000002, 51.500002],
                      [3.370001, 51.369722],
                      [3.370527, 51.36867],
                      [3.362223, 51.320002],
                      [3.36389, 51.313608],
                      [3.373613, 51.309999],
                      [3.952501, 51.214441],
                      [4.397501, 51.452776],
                      [5.078611, 51.391665],
                      [5.848333, 51.139444],
                      [5.651667, 50.824717],
                      [6.011797, 50.757273],
                      [5.934168, 51.036386],
                      [6.222223, 51.361666],
                      [5.94639, 51.811663],
                      [6.405001, 51.830828],
                      [7.053095, 52.237764],
                      [7.031389, 52.268885],
                      [7.063612, 52.346109],
                      [7.065557, 52.385828],
                      [7.133055, 52.888887],
                      [7.14218, 52.898244],
                      [7.191667, 53.3],
                      [6.5, 53.666667],
                      [6.500002, 55.000002],
                      [5.0, 55.0],
                    ],
                  ],
                },
                properties: {
                  selectionType: 'fir',
                },
              },
            ],
          },
          location_indicator_atsr: 'EHAA',
          location_indicator_atsu: 'EHAA',
          max_hours_of_validity: 4,
          hours_before_validity: 4,
          cloud_lower_level_min: {
            FT: 100,
          },
          cloud_lower_level_max: {
            FT: 900,
          },
          cloud_level_min: {
            FT: 100,
          },
          cloud_level_max: {
            FT: 9900,
          },
          wind_speed_min: {
            KT: 31,
          },
          wind_speed_max: {
            KT: 199,
          },
          visibility_max: 4900,
          visibility_min: 0,
          units: [
            {
              unit_type: 'level_unit',
              allowed_units: ['FT', 'FL'],
            },
            {
              unit_type: 'movement_unit',
              allowed_units: ['KT'],
            },
            {
              unit_type: 'surfacewind_unit',
              allowed_units: ['KT'],
            },
            {
              unit_type: 'cloud_level_unit',
              allowed_units: ['FT'],
            },
          ],
        },
        TEST: {
          fir_name: 'AMSTERDAM FIR',
          fir_location: {
            type: 'FeatureCollection',
            features: [
              {
                type: 'Feature',
                geometry: {
                  type: 'Polygon',
                  coordinates: [
                    [
                      [5.0, 55.0],
                      [4.331914, 55.332644],
                      [3.368817, 55.764314],
                      [2.761908, 54.379261],
                      [3.15576, 52.913554],
                      [2.000002, 51.500002],
                      [3.370001, 51.369722],
                      [3.370527, 51.36867],
                      [3.362223, 51.320002],
                      [3.36389, 51.313608],
                      [3.373613, 51.309999],
                      [7.053095, 52.237764],
                      [7.031389, 52.268885],
                      [7.063612, 52.346109],
                      [7.065557, 52.385828],
                      [7.133055, 52.888887],
                      [7.14218, 52.898244],
                      [7.191667, 53.3],
                      [6.5, 53.666667],
                      [6.500002, 55.000002],
                      [5.0, 55.0],
                    ],
                  ],
                },
                properties: {
                  selectionType: 'fir',
                },
              },
            ],
          },
          location_indicator_atsr: 'EHAA',
          location_indicator_atsu: 'EHAA',
          max_hours_of_validity: 4,
          hours_before_validity: 4,
          cloud_lower_level_min: {
            FT: 100,
          },
          cloud_lower_level_max: {
            FT: 900,
          },
          cloud_level_min: {
            FT: 100,
          },
          cloud_level_max: {
            FT: 9900,
          },
          wind_speed_min: {
            KT: 31,
          },
          wind_speed_max: {
            KT: 199,
          },
          visibility_max: 4900,
          visibility_min: 0,
          units: [
            {
              unit_type: 'level_unit',
              allowed_units: ['FT', 'FL'],
            },
            {
              unit_type: 'movement_unit',
              allowed_units: ['KT'],
            },
            {
              unit_type: 'surfacewind_unit',
              allowed_units: ['KT'],
            },
            {
              unit_type: 'cloud_level_unit',
              allowed_units: ['FT'],
            },
          ],
        },
      },
      active_firs: ['EHAA', 'TEST'],
    } as unknown as SigmetConfig;

    it('should return default fir from config', () => {
      expect(getFir(testConfig)).toEqual(
        testConfig.fir_areas[testConfig.active_firs[0]].fir_location,
      );
    });

    it('should return fir from config', () => {
      expect(getFir(testConfig, 'TEST')).toEqual(
        testConfig.fir_areas.TEST.fir_location,
      );
    });
  });

  describe('isFir', () => {
    it('should return true when geojson has selectionType "fir"', () => {
      const testFir = {
        type: 'FeatureCollection',
        features: [
          {
            type: 'Feature',
            geometry: {
              type: 'Polygon',
              coordinates: [
                [
                  [5.0, 55.0],
                  [4.331914, 55.332644],
                  [3.368817, 55.764314],
                  [2.761908, 54.379261],
                  [3.15576, 52.913554],
                  [2.000002, 51.500002],
                  [3.370001, 51.369722],
                  [3.370527, 51.36867],
                  [3.362223, 51.320002],
                  [3.36389, 51.313608],
                  [3.373613, 51.309999],
                  [3.952501, 51.214441],
                  [4.397501, 51.452776],
                  [5.078611, 51.391665],
                  [5.848333, 51.139444],
                  [5.651667, 50.824717],
                  [6.011797, 50.757273],
                  [5.934168, 51.036386],
                  [6.222223, 51.361666],
                  [5.94639, 51.811663],
                  [6.405001, 51.830828],
                  [7.053095, 52.237764],
                  [7.031389, 52.268885],
                  [7.063612, 52.346109],
                  [7.065557, 52.385828],
                  [7.133055, 52.888887],
                  [7.14218, 52.898244],
                  [7.191667, 53.3],
                  [6.5, 53.666667],
                  [6.500002, 55.000002],
                  [5.0, 55.0],
                ],
              ],
            },
            properties: {
              selectionType: 'fir',
            },
          },
        ],
      } as FIRLocationGeoJson;
      expect(isFir(testFir)).toBeTruthy();
    });
    it('should return false when geojson is invalid', () => {
      expect(
        isFir({
          type: 'FeatureCollection',
          features: [
            {
              type: 'Feature',
              geometry: {
                type: 'Polygon',
                coordinates: [
                  [
                    [5.0, 55.0],
                    [4.331914, 55.332644],
                    [3.368817, 55.764314],
                    [2.761908, 54.379261],
                    [3.15576, 52.913554],
                    [2.000002, 51.500002],
                    [3.370001, 51.369722],
                    [3.370527, 51.36867],
                    [3.362223, 51.320002],
                    [3.36389, 51.313608],
                    [3.373613, 51.309999],
                    [3.952501, 51.214441],
                    [4.397501, 51.452776],
                    [5.078611, 51.391665],
                    [5.848333, 51.139444],
                    [5.651667, 50.824717],
                    [6.011797, 50.757273],
                    [5.934168, 51.036386],
                    [6.222223, 51.361666],
                    [5.94639, 51.811663],
                    [6.405001, 51.830828],
                    [7.053095, 52.237764],
                    [7.031389, 52.268885],
                    [7.063612, 52.346109],
                    [7.065557, 52.385828],
                    [7.133055, 52.888887],
                    [7.14218, 52.898244],
                    [7.191667, 53.3],
                    [6.5, 53.666667],
                    [6.500002, 55.000002],
                    [5.0, 55.0],
                  ],
                ],
              },
              properties: {
                selectionType: 10,
              },
            },
          ],
        } as unknown as FIRLocationGeoJson),
      ).toBeFalsy();

      expect(
        isFir({
          type: 'FeatureCollection',
          features: [
            {
              type: 'Feature',
              geometry: {
                type: 'Polygon',
                coordinates: [
                  [
                    [5.0, 55.0],
                    [4.331914, 55.332644],
                    [3.368817, 55.764314],
                    [2.761908, 54.379261],
                    [3.15576, 52.913554],
                    [2.000002, 51.500002],
                    [3.370001, 51.369722],
                    [3.370527, 51.36867],
                    [3.362223, 51.320002],
                    [3.36389, 51.313608],
                    [3.373613, 51.309999],
                    [3.952501, 51.214441],
                    [4.397501, 51.452776],
                    [5.078611, 51.391665],
                    [5.848333, 51.139444],
                    [5.651667, 50.824717],
                    [6.011797, 50.757273],
                    [5.934168, 51.036386],
                    [6.222223, 51.361666],
                    [5.94639, 51.811663],
                    [6.405001, 51.830828],
                    [7.053095, 52.237764],
                    [7.031389, 52.268885],
                    [7.063612, 52.346109],
                    [7.065557, 52.385828],
                    [7.133055, 52.888887],
                    [7.14218, 52.898244],
                    [7.191667, 53.3],
                    [6.5, 53.666667],
                    [6.500002, 55.000002],
                    [5.0, 55.0],
                  ],
                ],
              },
              properties: {},
            },
          ],
        } as unknown as FIRLocationGeoJson),
      ).toBeFalsy();

      expect(
        isFir({
          type: 'FeatureCollection',
          features: [],
        }),
      ).toBeFalsy();
    });
  });

  describe('getFieldLabel', () => {
    it('should get field label', () => {
      expect(getFieldLabel('Test', false)).toEqual('Select test');
      expect(getFieldLabel('Test', true)).toEqual('Test');
      expect(getFieldLabel('Test', false, 'Set')).toEqual('Set test');
      expect(getFieldLabel('Test', true, 'Set')).toEqual('Test');
    });
  });

  describe('getBaseLayers', () => {
    it('should all layers defined in the config', () => {
      expect(getBaseLayers({} as ProductConfig)).toEqual([]);
      expect(
        getBaseLayers({
          mapPreset: {
            layers: [],
          },
        } as unknown as ProductConfig),
      ).toEqual([]);

      const testLayers = [
        {
          drawModeId: 'baseLayer-airmet',
          name: 'WorldMap_Light_Grey_Canvas',
          type: 'twms',
          layerType: LayerType.baseLayer,
        },
        {
          drawModeId: 'northseastations-airmet',
          name: 'northseastations',
          layerType: LayerType.overLayer,
          service:
            'https://adaguc-server-geoweb.geoweb.knmi.cloud/adagucserverdataset=OVL',
          format: 'image/png',
          enabled: true,
        },
      ];
      expect(
        getBaseLayers({
          mapPreset: {
            layers: testLayers,
          },
        } as unknown as ProductConfig),
      ).toEqual(testLayers);
    });

    it('should filter out non baseLayers', () => {
      expect(
        getBaseLayers({
          mapPreset: {
            layers: [
              {
                drawModeId: 'mapLayer',
                name: 'mapLayer',
                layerType: LayerType.mapLayer,
                service:
                  'https://adaguc-server-geoweb.geoweb.knmi.cloud/adagucserverdataset=OVL',
                format: 'image/png',
                enabled: true,
              },
              {
                drawModeId: 'mapLayer2',
                name: 'mapLayer',
                layerType: LayerType.mapLayer,
                service:
                  'https://adaguc-server-geoweb.geoweb.knmi.cloud/adagucserverdataset=OVL',
                format: 'image/png',
                enabled: true,
              },
            ],
          },
        } as unknown as ProductConfig),
      ).toEqual([]);

      const testLayers = [
        {
          drawModeId: 'baseLayer-airmet',
          name: 'WorldMap_Light_Grey_Canvas',
          type: 'twms',
          layerType: LayerType.baseLayer,
        },
        {
          drawModeId: 'northseastations-airmet',
          name: 'northseastations',
          layerType: LayerType.overLayer,
          service:
            'https://adaguc-server-geoweb.geoweb.knmi.cloud/adagucserverdataset=OVL',
          format: 'image/png',
          enabled: true,
        },
        {
          drawModeId: 'mapLayer',
          name: 'mapLayer',
          layerType: LayerType.mapLayer,
          service:
            'https://adaguc-server-geoweb.geoweb.knmi.cloud/adagucserverdataset=OVL',
          format: 'image/png',
          enabled: true,
        },
      ];
      expect(
        getBaseLayers({
          mapPreset: {
            layers: testLayers,
          },
        } as unknown as ProductConfig),
      ).toEqual([testLayers[0], testLayers[1]]);
    });
  });

  describe('getMaxMovementSpeedValue', () => {
    it('should return the max of FIR for passed unit', () => {
      expect(getMaxMovementSpeedValue('KT', 'EHAA', config)).toBe(
        config.fir_areas.EHAA.movement_max!.KT,
      );
      expect(getMaxMovementSpeedValue('KT', 'EBBU', configMultipleFIR)).toBe(
        configMultipleFIR.fir_areas.EBBU.movement_max!.KT,
      );
    });
    it('should return the max of first FIR for passed unit if FIR doesnt exist or isnt passed', () => {
      expect(getMaxMovementSpeedValue('KT', 'EHFG', configMultipleFIR)).toBe(
        configMultipleFIR.fir_areas.EHAA.movement_max!.KT,
      );
      expect(
        getMaxMovementSpeedValue('KT', undefined!, configMultipleFIR),
      ).toBe(configMultipleFIR.fir_areas.EHAA.movement_max!.KT);
    });
    it('should return null if unit not present or if movement_max not set in config', () => {
      expect(
        getMaxMovementSpeedValue('FAKEUNIT', 'EHFG', configMultipleFIR),
      ).toBe(null);
      // level_max not set in config
      expect(
        getMaxMovementSpeedValue(
          'KMH' as LevelUnits,
          'EDDD',
          configMultipleFIR,
        ),
      ).toBe(null);
    });
  });

  describe('getMinMovementSpeedValue', () => {
    it('should return the max of FIR for passed unit', () => {
      expect(getMinMovementSpeedValue('KT', 'EHAA', config)).toBe(
        config.fir_areas.EHAA.movement_min!.KT,
      );
      expect(getMinMovementSpeedValue('KT', 'EBBU', configMultipleFIR)).toBe(
        configMultipleFIR.fir_areas.EBBU.movement_min!.KT,
      );
    });
    it('should return the max of first FIR for passed unit if FIR doesnt exist or isnt passed', () => {
      expect(getMinMovementSpeedValue('KT', 'EHFG', configMultipleFIR)).toBe(
        configMultipleFIR.fir_areas.EHAA.movement_min!.KT,
      );
      expect(
        getMinMovementSpeedValue('KT', undefined!, configMultipleFIR),
      ).toBe(configMultipleFIR.fir_areas.EHAA.movement_min!.KT);
    });
    it('should return null if unit not present or if movement_min not set in config', () => {
      expect(
        getMinMovementSpeedValue('FAKEUNIT', 'EHFG', configMultipleFIR),
      ).toBe(DEFAULT_MIN_MOVEMENT_SPEED);
      // level_max not set in config
      expect(
        getMinMovementSpeedValue(
          'KMH' as LevelUnits,
          'EDDD',
          configMultipleFIR,
        ),
      ).toBe(DEFAULT_MIN_MOVEMENT_SPEED);
    });
  });

  describe('getProjection', () => {
    it('should return the projection defined in the config', () => {
      expect(getProjection({} as ProductConfig)).toEqual(undefined);
      expect(
        getProjection({
          mapPreset: {
            proj: {},
          },
        } as unknown as ProductConfig),
      ).toEqual({});

      const testProjection = {
        bbox: {
          left: 1,
          right: 2,
          top: 3,
          bottom: 4,
        },
        srs: 'EPSG:3857',
      };
      expect(
        getProjection({
          mapPreset: {
            proj: testProjection,
          },
        } as unknown as ProductConfig),
      ).toEqual(testProjection);
    });
  });

  describe('getShapeWithProperties', () => {
    it('should return the geojson feature with properties', () => {
      const shape: GeoJSON.Feature = {
        type: 'Feature',
        geometry: {
          type: 'Polygon',
          coordinates: [
            [
              [5.0, 55.0],
              [4.331914, 55.332644],
              [3.368817, 55.764314],
              [2.761908, 54.379261],
              [3.15576, 52.913554],
              [2.000002, 51.500002],
              [3.370001, 51.369722],
              [3.370527, 51.36867],
              [3.362223, 51.320002],
              [3.36389, 51.313608],
              [3.373613, 51.309999],
              [3.952501, 51.214441],
              [4.397501, 51.452776],
              [5.078611, 51.391665],
              [5.848333, 51.139444],
              [5.651667, 50.824717],
              [6.011797, 50.757273],
              [5.934168, 51.036386],
              [6.222223, 51.361666],
              [5.94639, 51.811663],
              [6.405001, 51.830828],
              [7.053095, 52.237764],
              [7.031389, 52.268885],
              [7.063612, 52.346109],
              [7.065557, 52.385828],
              [7.133055, 52.888887],
              [7.14218, 52.898244],
              [7.191667, 53.3],
              [6.5, 53.666667],
              [6.500002, 55.000002],
              [5.0, 55.0],
            ],
          ],
        },
        properties: {
          selectionType: 'test',
        },
      };

      expect(getShapeWithProperties(shape, featurePropsStart)).toEqual({
        ...shape,
        properties: {
          ...shape.properties,
          ...featurePropsStart,
        },
      });

      expect(getShapeWithProperties(shape, {})).toEqual(shape);
    });
  });

  describe('getDrawTools', () => {
    const shape: GeoJSON.FeatureCollection = {
      type: 'FeatureCollection',
      features: [
        {
          type: 'Feature',
          geometry: {
            type: 'Polygon',
            coordinates: [
              [
                [5.0, 55.0],
                [4.331914, 55.332644],
                [3.368817, 55.764314],
                [2.761908, 54.379261],
                [3.15576, 52.913554],
                [2.000002, 51.500002],
                [3.370001, 51.369722],
                [3.370527, 51.36867],
                [3.362223, 51.320002],
                [3.36389, 51.313608],
                [3.373613, 51.309999],
                [3.952501, 51.214441],
                [4.397501, 51.452776],
                [5.078611, 51.391665],
                [5.848333, 51.139444],
                [5.651667, 50.824717],
                [6.011797, 50.757273],
                [5.934168, 51.036386],
                [6.222223, 51.361666],
                [5.94639, 51.811663],
                [6.405001, 51.830828],
                [7.053095, 52.237764],
                [7.031389, 52.268885],
                [7.063612, 52.346109],
                [7.065557, 52.385828],
                [7.133055, 52.888887],
                [7.14218, 52.898244],
                [7.191667, 53.3],
                [6.5, 53.666667],
                [6.500002, 55.000002],
                [5.0, 55.0],
              ],
            ],
          },
          properties: {
            selectionType: 'test',
          },
        },
      ],
    };
    it('should return correct draw tools for start drawing', () => {
      expect(getDrawTools(StartOrEndDrawing.start, shape)).toEqual([
        {
          ...defaultPoint,
          title: `Drop a point as start position`,
          drawModeId: 'drawtools-point',
          shape: getShapeWithProperties(defaultPoint.shape, featurePropsStart),
        },
        {
          ...defaultBox,
          drawModeId: 'drawtools-box',
          title: `Draw a box as start position`,
          shape: getShapeWithProperties(defaultBox.shape, featurePropsStart),
        },
        {
          ...defaultPolygon,
          title: `Draw a shape as start position`,
          drawModeId: 'drawtools-polygon',
          shape: getShapeWithProperties(
            defaultPolygon.shape,
            featurePropsStart,
          ),
        },
        {
          drawModeId: firID,
          title: `Set FIR as start position`,
          isSelectable: false,
          value: DRAWMODE.POLYGON,
          shape: addFeatureProperties(shape, featurePropsStart),
        },
        {
          ...defaultDelete,
          title: 'Remove geometry',
          drawModeId: 'drawtools-delete',
        },
      ]);
    });

    it('should return correct draw tools for end drawing', () => {
      expect(getDrawTools(StartOrEndDrawing.end, shape)).toEqual([
        {
          ...defaultPoint,
          title: `Drop a point as end position`,
          drawModeId: 'drawtools-point',
          shape: getShapeWithProperties(defaultPoint.shape, featurePropsEnd),
        },
        {
          ...defaultBox,
          drawModeId: 'drawtools-box',
          title: `Draw a box as end position`,
          shape: getShapeWithProperties(defaultBox.shape, featurePropsEnd),
        },
        {
          ...defaultPolygon,
          title: `Draw a shape as end position`,
          drawModeId: 'drawtools-polygon',
          shape: getShapeWithProperties(defaultPolygon.shape, featurePropsEnd),
        },
        {
          drawModeId: firID,
          title: `Set FIR as end position`,
          isSelectable: false,
          value: DRAWMODE.POLYGON,
          shape: addFeatureProperties(shape, featurePropsEnd),
        },
        {
          ...defaultDelete,
          title: 'Remove geometry',
          drawModeId: 'drawtools-delete',
        },
      ]);
    });
  });

  describe('updateDrawToolsWithFir', () => {
    it('should update fir shape in tool', () => {
      const shape: GeoJSON.FeatureCollection = {
        type: 'FeatureCollection',
        features: [
          {
            type: 'Feature',
            geometry: {
              type: 'Polygon',
              coordinates: [
                [
                  [5.0, 55.0],
                  [4.331914, 55.332644],
                  [3.368817, 55.764314],
                  [2.761908, 54.379261],
                  [3.15576, 52.913554],
                  [2.000002, 51.500002],
                  [3.370001, 51.369722],
                  [3.370527, 51.36867],
                  [3.362223, 51.320002],
                  [3.36389, 51.313608],
                  [3.373613, 51.309999],
                  [3.952501, 51.214441],
                  [4.397501, 51.452776],
                  [5.078611, 51.391665],
                  [5.848333, 51.139444],
                  [5.651667, 50.824717],
                  [6.011797, 50.757273],
                  [5.934168, 51.036386],
                  [6.222223, 51.361666],
                  [5.94639, 51.811663],
                  [6.405001, 51.830828],
                  [7.053095, 52.237764],
                  [7.031389, 52.268885],
                  [7.063612, 52.346109],
                  [7.065557, 52.385828],
                  [7.133055, 52.888887],
                  [7.14218, 52.898244],
                  [7.191667, 53.3],
                  [6.5, 53.666667],
                  [6.500002, 55.000002],
                  [5.0, 55.0],
                ],
              ],
            },
            properties: {
              selectionType: 'test',
            },
          },
        ],
      };
      const result = updateDrawToolsWithFir(
        [
          defaultPoint,
          defaultBox,
          {
            drawModeId: firID,
            title: `Set FIR as end position`,
            isSelectable: false,
            value: DRAWMODE.POLYGON,
            shape: addFeatureProperties(shape, featurePropsEnd),
          },
        ],
        shape,
      );
      expect(result[2].shape).toEqual(shape);

      const result2 = updateDrawToolsWithFir(
        [
          defaultPoint,
          {
            drawModeId: firID,
            title: `Set FIR as end position`,
            isSelectable: false,
            value: DRAWMODE.POLYGON,
            shape: addFeatureProperties(shape, featurePropsEnd),
          },
          defaultBox,
        ],
        shape,
      );
      expect(result2[1].shape).toEqual(shape);

      const result3 = updateDrawToolsWithFir(
        [defaultPoint, defaultPolygon, defaultBox],
        shape,
      );
      expect(result3).toEqual([defaultPoint, defaultPolygon, defaultBox]);

      expect(updateDrawToolsWithFir([], shape)).toEqual([]);
    });
  });

  describe('getToolIcon', () => {
    it('should return fir icon', () => {
      expect(getToolIcon(firID)).toEqual(<DrawFIRLand />);
      expect(getToolIcon('unkown')).toEqual(<DrawFIRLand />);
    });
  });
});
