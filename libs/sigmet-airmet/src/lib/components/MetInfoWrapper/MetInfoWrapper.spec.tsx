/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */
import React from 'react';
import {
  act,
  fireEvent,
  render,
  screen,
  waitFor,
  within,
} from '@testing-library/react';
import { ApiProvider } from '@opengeoweb/api';
import { Provider } from 'react-redux';
import { lightTheme } from '@opengeoweb/theme';

import userEvent from '@testing-library/user-event';
import MetInfoWrapper from './MetInfoWrapper';
import { noTAC } from '../ProductForms/ProductFormTac';
import { TestWrapper, ThemeWrapperWithModules } from '../../utils/testUtils';
import {
  fakeAirmetTAC,
  fakeSigmetTAC,
  createApi as createFakeApi,
} from '../../utils/fakeApi';
import { fakeAirmetList } from '../../utils/mockdata/fakeAirmetList';
import { fakeSigmetList } from '../../utils/mockdata/fakeSigmetList';
import {
  Airmet,
  AirmetPhenomena,
  Sigmet,
  SigmetConfig,
  SigmetPhenomena,
} from '../../types';
import { airmetConfig, sigmetConfig } from '../../utils/config';
import { SigmetAirmetApi } from '../../utils/api';
import { store as defaultStore } from '../../utils/store';

describe('components/MetInfoWrapper/MetInfoWrapper', () => {
  beforeEach(() => {
    jest.useFakeTimers();
  });
  afterEach(() => {
    jest.clearAllTimers();
    jest.useRealTimers();
  });

  it('should not clear the form when the module is rerendered due to api changes caused by for example a token refresh', async () => {
    const mockFunction = jest.fn();
    const mockGetSigmetList = jest.fn(() => {
      return new Promise((resolve) => {
        resolve({ data: fakeSigmetList });
      });
    });
    const mockGetTac = jest.fn(() => {
      return new Promise((resolve) => {
        resolve({
          data: fakeSigmetTAC,
        });
      });
    });
    const Wrapper = (): React.ReactElement => {
      const auth1 = {
        username: 'test',
        token: '2094023',
        refresh_token: 'di98i3k',
      };
      const [auth, onSetAuth] = React.useState(auth1);

      setTimeout(() => {
        onSetAuth({
          username: 'test',
          token: '23aserf',
          refresh_token: '324kjkj',
        });
        mockFunction();
      }, 600);

      return (
        <ApiProvider
          auth={auth}
          onSetAuth={onSetAuth}
          createApi={(): SigmetAirmetApi => {
            return {
              getSigmetList: mockGetSigmetList,
              getSigmetTAC: mockGetTac,
            } as unknown as SigmetAirmetApi;
          }}
        >
          <Provider store={defaultStore}>
            <ThemeWrapperWithModules theme={lightTheme}>
              <MetInfoWrapper
                productType="sigmet"
                productConfig={sigmetConfig}
              />
            </ThemeWrapperWithModules>
          </Provider>
        </ApiProvider>
      );
    };

    render(<Wrapper />);

    // Wait until list has loaded
    await waitFor(() => {
      expect(screen.getAllByTestId('issueTime').length).toEqual(
        fakeSigmetList.length,
      );
    });
    expect(mockGetSigmetList).toHaveBeenCalledTimes(1);
    expect(mockGetTac).toHaveBeenCalledTimes(fakeSigmetList.length - 1);

    expect(screen.queryByTestId('productform-dialog')).toBeFalsy();
    fireEvent.click(screen.getByText('Create a new SIGMET'));
    await waitFor(() => {
      expect(screen.getByTestId('productform-dialog')).toBeTruthy();
    });
    expect(screen.getByTestId('dialogTitle').textContent).toEqual('New SIGMET');

    const obsField = screen.getByTestId('isObservationOrForecast-OBS');
    fireEvent.click(obsField);
    await waitFor(() =>
      expect(
        (
          screen.getByTestId('isObservationOrForecast-OBS')
            .firstChild as HTMLElement
        ).classList,
      ).toContain('Mui-checked'),
    );

    await waitFor(() => expect(mockFunction).toHaveBeenCalledTimes(0));

    await act(async () => jest.advanceTimersByTime(600));

    await waitFor(() => expect(mockFunction).toHaveBeenCalledTimes(1));
    // form values should not get cleared
    expect(
      (
        screen.getByTestId('isObservationOrForecast-OBS')
          .firstChild as HTMLElement
      ).classList,
    ).toContain('Mui-checked');
  });

  it('should load the correct list based on the productType passed in (airmet)', async () => {
    render(
      <TestWrapper>
        <MetInfoWrapper productType="airmet" productConfig={airmetConfig} />
      </TestWrapper>,
    );

    // Wait until airmetlist has loaded
    await screen.findAllByText('Issue time');

    expect(await screen.findByText('Create a new AIRMET')).toBeTruthy();
  });

  it('should load the correct list based on the productType passed in (sigmet)', async () => {
    render(
      <TestWrapper>
        <MetInfoWrapper productType="sigmet" productConfig={sigmetConfig} />
      </TestWrapper>,
    );

    // Wait until sigmetlist has loaded
    await screen.findAllByText('Issue time');

    expect(await screen.findByText('Create a new SIGMET')).toBeTruthy();
  });

  it('should open the dialog for a new sigmet when creating a new sigmet', async () => {
    render(
      <TestWrapper>
        <MetInfoWrapper productType="sigmet" productConfig={sigmetConfig} />
      </TestWrapper>,
    );

    // Wait until sigmetlist has loaded
    await screen.findAllByText('Issue time');

    expect(screen.queryByTestId('productform-dialog')).toBeFalsy();
    fireEvent.click(await screen.findByText('Create a new SIGMET'));
    await waitFor(() => {
      expect(screen.getByTestId('productform-dialog')).toBeTruthy();
    });
    expect(screen.getByTestId('dialogTitle').textContent).toEqual('New SIGMET');
  });

  it('should open the dialog for a new airmet when creating a new airmet, save it and open a new empty airmet', async () => {
    const mockGetAirmetList = jest.fn(() => {
      return new Promise((resolve) => {
        resolve({ data: fakeAirmetList });
      });
    });

    const mockGetTac = jest.fn(() => {
      return new Promise((resolve) => {
        resolve({
          data: fakeAirmetTAC,
        });
      });
    });

    const mockPostAirmet = jest.fn(() => {
      return new Promise<void>((resolve) => {
        setTimeout(() => {
          resolve();
        }, 1000);
      });
    });

    render(
      <TestWrapper
        createApi={(): // eslint-disable-next-line @typescript-eslint/no-explicit-any
        any => {
          return {
            // dummy calls
            getAirmetList: mockGetAirmetList,
            postAirmet: mockPostAirmet,
            getAirmetTAC: mockGetTac,
          };
        }}
      >
        <MetInfoWrapper productType="airmet" productConfig={airmetConfig} />
      </TestWrapper>,
    );
    const user = userEvent.setup({
      advanceTimers: jest.advanceTimersByTime,
    });

    // Wait until airmetlist has loaded
    await waitFor(() => {
      expect(mockGetAirmetList).toHaveBeenCalledTimes(1);
    });
    expect(screen.getAllByTestId('issueTime').length).toEqual(
      fakeAirmetList.length,
    );
    expect(mockGetTac).toHaveBeenCalledTimes(fakeAirmetList.length);

    expect(screen.queryByTestId('productform-dialog')).toBeFalsy();
    await user.click(await screen.findByText('Create a new AIRMET'));
    await waitFor(() => {
      expect(screen.getByTestId('productform-dialog')).toBeTruthy();
    });
    expect(screen.getByTestId('dialogTitle').textContent).toEqual('New AIRMET');
    expect(
      within(screen.getByTestId('phenomenon')).getByRole('button').textContent,
    ).toEqual('​');

    // change phenomenon
    await user.click(
      within(screen.getByTestId('phenomenon')).getByRole('button'),
    );

    const menuItem = await screen.findByText('Surface wind');
    await user.click(menuItem);
    await waitFor(() =>
      expect(screen.getByTestId('phenomenon').textContent).toEqual(
        'Surface wind',
      ),
    );

    // save as draft
    await user.click(screen.getByTestId('productform-dialog-draft'));
    expect(screen.queryByTestId('loader')!.style.opacity).toEqual('0');

    // post action should be called
    await waitFor(() => {
      expect(mockPostAirmet).toHaveBeenCalledTimes(1);
    });
    expect(mockPostAirmet).toHaveBeenCalledWith({
      changeStatusTo: 'DRAFT',
      airmet: expect.any(Object),
    });
    await act(async () => {
      jest.advanceTimersByTime(1000);
    });
    await waitFor(() => {
      // list should be updated
      expect(mockGetAirmetList).toHaveBeenCalledTimes(2);
    });

    await waitFor(() =>
      // form should be closed
      expect(screen.queryByTestId('productform-dialog')).toBeFalsy(),
    );

    // Open again the new dialog and verify the values are empty
    await user.click(screen.getByTestId('productListCreateButton'));
    await waitFor(() => {
      expect(screen.getByTestId('productform-dialog')).toBeTruthy();
    });
    expect(screen.getByTestId('dialogTitle').textContent).toEqual('New AIRMET');
    expect(screen.getByTestId('phenomenon').textContent).toEqual('​');
  });

  it('should open the dialog for the correct airmet when clicking a row', async () => {
    render(
      <TestWrapper>
        <MetInfoWrapper productType="airmet" productConfig={airmetConfig} />
      </TestWrapper>,
    );

    // Wait until airmetlist has loaded
    await waitFor(() =>
      expect(screen.getAllByTestId('issueTime').length).toBeGreaterThan(0),
    );

    expect(screen.queryByTestId('productform-dialog')).toBeFalsy();
    await screen.findAllByTestId('productListItem');

    fireEvent.click(screen.getAllByTestId('productListItem')[0]);
    await waitFor(() => {
      expect(screen.queryByText(noTAC)).toBeFalsy();
    });
    expect(screen.getByTestId('productform-dialog')).toBeTruthy();
    expect(screen.getByTestId('dialogTitle').textContent).toEqual(
      'AIRMET Isolated thunderstorm(s) with hail - saved as draft',
    );
  });

  it('should open the dialog for the correct sigmet when clicking a second time on the same row', async () => {
    render(
      <TestWrapper>
        <MetInfoWrapper productType="sigmet" productConfig={sigmetConfig} />
      </TestWrapper>,
    );

    // Wait until sigmetlist has loaded
    await screen.findAllByText('Issue time');

    expect(screen.queryByTestId('productform-dialog')).toBeFalsy();

    await waitFor(() =>
      expect(screen.getAllByTestId('productListItem').length).toEqual(
        fakeSigmetList.length,
      ),
    );
    // click on the first row
    fireEvent.click(screen.getAllByTestId('productListItem')[0]);
    await waitFor(() => expect(screen.queryByText(noTAC)).toBeFalsy());

    await waitFor(() => {
      expect(screen.getByTestId('phenomenon').textContent).toEqual(
        SigmetPhenomena[
          (fakeSigmetList[0].sigmet as Sigmet)
            .phenomenon as unknown as keyof typeof SigmetPhenomena
        ],
      );
    });
    expect(screen.getByTestId('productform-dialog')).toBeTruthy();
    expect(screen.getByTestId('dialogTitle').textContent).toEqual(
      'SIGMET Obscured thunderstorm(s) - saved as draft',
    );

    // close the dialog
    fireEvent.click(screen.getByTestId('contentdialog-close'));
    await waitFor(() =>
      expect(screen.queryByTestId('productform-dialog')).toBeFalsy(),
    );
    // click again on the same row
    fireEvent.click(screen.getAllByTestId('productListItem')[0]);
    await waitFor(() => expect(screen.queryByText(noTAC)).toBeFalsy());
    await waitFor(() => {
      expect(screen.getByTestId('productform-dialog')).toBeTruthy();
    });
    expect(screen.getByTestId('dialogTitle').textContent).toEqual(
      'SIGMET Obscured thunderstorm(s) - saved as draft',
    );
    expect(screen.getByTestId('phenomenon').textContent).toEqual(
      SigmetPhenomena[
        (fakeSigmetList[0].sigmet as Sigmet)
          .phenomenon as unknown as keyof typeof SigmetPhenomena
      ],
    );
    // close it again and check confirmation dialog does not show
    fireEvent.click(screen.getByTestId('contentdialog-close'));
    await waitFor(() =>
      expect(screen.queryByTestId('productform-dialog')).toBeFalsy(),
    );
  });

  it('should open the dialog for the correct airmet when clicking a second time on the same row', async () => {
    render(
      <TestWrapper>
        <MetInfoWrapper productType="airmet" productConfig={airmetConfig} />
      </TestWrapper>,
    );

    // Wait until sigmetlist has loaded
    await screen.findAllByText('Issue time');

    expect(screen.queryByTestId('productform-dialog')).toBeFalsy();

    await screen.findAllByTestId('productListItem');
    // click on the first row
    fireEvent.click(screen.getAllByTestId('productListItem')[0]);
    await waitFor(() => expect(screen.queryByText(noTAC)).toBeFalsy());

    await waitFor(() => {
      expect(screen.getByTestId('phenomenon').textContent).toEqual(
        AirmetPhenomena[
          (fakeAirmetList[0].airmet as Airmet)
            .phenomenon as unknown as keyof typeof AirmetPhenomena
        ],
      );
    });
    expect(screen.getByTestId('productform-dialog')).toBeTruthy();
    expect(screen.getByTestId('dialogTitle').textContent).toEqual(
      'AIRMET Isolated thunderstorm(s) with hail - saved as draft',
    );
    // close the dialog
    fireEvent.click(screen.getByTestId('contentdialog-close'));
    await waitFor(() =>
      expect(screen.queryByTestId('productform-dialog')).toBeFalsy(),
    );
    // click again on the same row
    fireEvent.click(screen.getAllByTestId('productListItem')[0]);
    await waitFor(() => expect(screen.queryByText(noTAC)).toBeFalsy());

    await waitFor(() => {
      expect(screen.getByTestId('productform-dialog')).toBeTruthy();
    });
    expect(screen.getByTestId('dialogTitle').textContent).toEqual(
      'AIRMET Isolated thunderstorm(s) with hail - saved as draft',
    );
    expect(screen.getByTestId('phenomenon').textContent).toEqual(
      AirmetPhenomena[
        (fakeAirmetList[0].airmet as Airmet)
          .phenomenon as unknown as keyof typeof AirmetPhenomena
      ],
    );
  });

  it('should open the dialog for an airmet, closing it without making any changes should not lead to a refetch of the list', async () => {
    const mockGetAirmetList = jest.fn(() => {
      return new Promise((resolve) => {
        resolve({ data: fakeAirmetList });
      });
    });
    const mockGetTac = jest.fn(() => {
      return new Promise((resolve) => {
        resolve({
          data: fakeAirmetTAC,
        });
      });
    });
    const mockPostAirmet = jest.fn(() => {
      return new Promise<void>((resolve) => {
        resolve();
      });
    });
    render(
      <TestWrapper
        createApi={(): // eslint-disable-next-line @typescript-eslint/no-explicit-any
        any => {
          return {
            // dummy calls
            getAirmetList: mockGetAirmetList,
            postAirmet: mockPostAirmet,
            getAirmetTAC: mockGetTac,
          };
        }}
      >
        <MetInfoWrapper productType="airmet" productConfig={airmetConfig} />
      </TestWrapper>,
    );

    // Wait until airmetlist has loaded
    await waitFor(() => {
      expect(screen.getAllByTestId('issueTime').length).toEqual(
        fakeAirmetList.length,
      );
    });
    expect(mockGetAirmetList).toHaveBeenCalledTimes(1);
    expect(mockGetTac).toHaveBeenCalledTimes(fakeAirmetList.length);

    expect(screen.queryByTestId('productform-dialog')).toBeFalsy();

    await screen.findAllByTestId('productListItem');
    // click on the first row
    fireEvent.click(screen.getAllByTestId('productListItem')[0]);
    await waitFor(() => expect(screen.queryByText(noTAC)).toBeFalsy());

    await waitFor(() => {
      expect(screen.getByTestId('phenomenon').textContent).toEqual(
        AirmetPhenomena[
          (fakeAirmetList[0].airmet as Airmet)
            .phenomenon as unknown as keyof typeof AirmetPhenomena
        ],
      );
    });
    expect(screen.getByTestId('productform-dialog')).toBeTruthy();
    expect(screen.getByTestId('dialogTitle').textContent).toEqual(
      'AIRMET Isolated thunderstorm(s) with hail - saved as draft',
    );
    // close modal
    fireEvent.click(screen.getByTestId('contentdialog-close'));

    await waitFor(() => {
      expect(screen.queryByTestId('productform-dialog')).toBeFalsy();
    });
    // there should be no spinner
    expect(screen.queryAllByTestId('loader')).toHaveLength(0);
    // list should not be updated an additional time
    expect(mockGetAirmetList).toHaveBeenCalledTimes(1);
  });

  describe('ensure polling works correctly', () => {
    it('should refetch the list periodically if there is an error', async () => {
      const fakeApi: SigmetAirmetApi = {
        ...createFakeApi(),
        getSigmetList: jest.fn(() => Promise.reject(new Error('error'))),
      } as unknown as SigmetAirmetApi;

      const createTestApi: () => SigmetAirmetApi = () => fakeApi;

      render(
        <TestWrapper createApi={createTestApi}>
          <MetInfoWrapper productType="sigmet" productConfig={sigmetConfig} />
        </TestWrapper>,
      );

      await waitFor(() => {
        expect(screen.getByTestId('productList-alert')).toBeTruthy();
      });
      act(() => jest.runOnlyPendingTimers());

      // Test new data has been fetched
      await waitFor(() => expect(fakeApi.getSigmetList).toHaveBeenCalled());

      expect(fakeApi.getSigmetList).toHaveBeenCalledTimes(2);
    });

    it('should not call periodically if list still loading', async () => {
      const fakeApi: SigmetAirmetApi = {
        ...createFakeApi(),
        getSigmetList: jest.fn(),
      } as unknown as SigmetAirmetApi;

      const createTestApi: () => SigmetAirmetApi = () => fakeApi;

      render(
        <TestWrapper createApi={createTestApi}>
          <MetInfoWrapper productType="sigmet" productConfig={sigmetConfig} />
        </TestWrapper>,
      );

      // Test new data has not been fetched
      act(() => {
        expect(fakeApi.getSigmetList).toHaveBeenCalledTimes(1);
      });
      act(() => {
        jest.runOnlyPendingTimers();
      });
      await waitFor(() => {
        expect(screen.getByTestId('productlist-loadingbar')).toBeTruthy();
      });
      act(() => {
        expect(fakeApi.getSigmetList).toHaveBeenCalledTimes(1);
      });
      await waitFor(() =>
        expect(screen.queryByTestId('productlist-loadingbar')).toBeFalsy(),
      );
    });
    it('should refetch the list periodically if there is a list loaded', async () => {
      const fakeApi: SigmetAirmetApi = {
        ...createFakeApi(),
        getSigmetList: jest.fn(() => Promise.resolve({ data: fakeSigmetList })),
      } as unknown as SigmetAirmetApi;

      const createTestApi: () => SigmetAirmetApi = () => fakeApi;

      render(
        <TestWrapper createApi={createTestApi}>
          <MetInfoWrapper productType="sigmet" productConfig={sigmetConfig} />
        </TestWrapper>,
      );

      // Test new data has not been fetched
      act(() => {
        expect(fakeApi.getSigmetList).toHaveBeenCalledTimes(1);
      });

      await waitFor(() =>
        expect(screen.queryByTestId('productlist-loadingbar')).toBeFalsy(),
      );

      act(() => {
        jest.runOnlyPendingTimers();
      });

      await waitFor(() => {
        expect(fakeApi.getSigmetList).toHaveBeenCalledTimes(2);
      });
    });

    it('should not (re)fetch the list periodically if there is an error in the config', async () => {
      const fakeApi: SigmetAirmetApi = {
        ...createFakeApi(),
        getSigmetList: jest.fn(() => Promise.resolve({ data: fakeSigmetList })),
      } as unknown as SigmetAirmetApi;

      const createTestApi: () => SigmetAirmetApi = () => fakeApi;

      render(
        <TestWrapper createApi={createTestApi}>
          <MetInfoWrapper
            productType="sigmet"
            productConfig={{} as SigmetConfig}
          />
        </TestWrapper>,
      );

      // Test new data has not been fetched
      act(() => {
        expect(fakeApi.getSigmetList).toHaveBeenCalledTimes(0);
      });

      await waitFor(() =>
        expect(screen.queryByTestId('productlist-loadingbar')).toBeFalsy(),
      );

      act(() => {
        jest.runOnlyPendingTimers();
      });

      act(() => {
        expect(fakeApi.getSigmetList).toHaveBeenCalledTimes(0);
      });
    });
  });
});
