/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */
import React from 'react';
import { act, renderHook, waitFor } from '@testing-library/react';
import { AirmetConfig, SigmetConfig } from '../../types';
import {
  airmetConfig,
  sigmetConfig,
  validateAirmet,
  validateSigmet,
} from '../../utils/config';
import { TestWrapper, TestWrapperProps } from '../../utils/testUtils';
import {
  ERROR_BACKEND,
  ERROR_CONFIG,
  useProductApi,
  validateConfig,
} from './utils';
import { createApi as createFakeApi } from '../../utils/fakeApi';
import { SigmetAirmetApi } from '../../utils/api';
import {
  AJVErrorObject,
  createReadableErrorMessage,
} from '../../utils/config/config';

describe('components/MetInfoWrapper/utils', () => {
  describe('validateConfig', () => {
    it('should validate return true for validated aviation products', () => {
      expect(validateConfig('sigmet', sigmetConfig)).toBeTruthy();
      expect(validateConfig('airmet', airmetConfig)).toBeTruthy();
    });

    it('should validate return error for unvalidated airmet', () => {
      const invalidAirmetConfig = {} as unknown as AirmetConfig;
      const expectedResult = validateAirmet(invalidAirmetConfig);
      expect(validateConfig('airmet', invalidAirmetConfig)).toEqual({
        name: ERROR_CONFIG,
        message: createReadableErrorMessage(expectedResult as AJVErrorObject[]),
      });

      const invalidAirmetConfig2 = {
        location_indicator_mwo: 'EHDB',
        fir_areas: {
          EHAA: {
            fir_name: 'AMSTERDAM FIR',
            location_indicator_atsr: 'EHAA',
            location_indicator_atsu: 'EHAA',
            max_hours_of_validity: 4,
            hours_before_validity: 4,
            level_min: {
              FT: 100,
              FL: 50,
            },
            level_max: {
              FT: 4900,
              FL: 100,
            },
            level_rounding_FL: 100,
            level_rounding_FT: 5,
            level_rounding_M: 1,
            movement_min: {
              KT: 5,
              KMH: 10,
            },
            movement_max: {
              KT: 150,
              KMH: 99,
            },
            movement_rounding_kt: 5,
            movement_rounding_kmh: 10,
          },
        },
        active_firs: ['EHAA'],
      } as unknown as AirmetConfig;
      const expectedResult2 = validateAirmet(invalidAirmetConfig2);
      expect(validateConfig('airmet', invalidAirmetConfig2)).toEqual({
        name: ERROR_CONFIG,
        message: createReadableErrorMessage(
          expectedResult2 as AJVErrorObject[],
        ),
      });
    });

    it('should validate return error for unvalidated sigmet', () => {
      const invalidSigmetConfig = {} as unknown as SigmetConfig;
      const expectedResult = validateSigmet(invalidSigmetConfig);
      expect(validateConfig('sigmet', invalidSigmetConfig)).toEqual({
        name: ERROR_CONFIG,
        message: createReadableErrorMessage(expectedResult as AJVErrorObject[]),
      });

      const invalidSigmetConfig2 = {
        location_indicator_mwo: 'EHDB',
        fir_areas: {
          EHAA: {
            fir_name: 'AMSTERDAM FIR',
            location_indicator_atsr: 'EHAA',
            location_indicator_atsu: 'EHAA',
            max_hours_of_validity: 4,
            hours_before_validity: 4,
            level_min: {
              FT: 100,
              FL: 50,
            },
            level_max: {
              FT: 4900,
              FL: 650,
            },
            level_rounding_FL: 100,
            level_rounding_FT: 5,
            level_rounding_M: 1,
            movement_min: {
              KT: 5,
              KMH: 10,
            },
            movement_max: {
              KT: 150,
              KMH: 99,
            },
            movement_rounding_kt: 5,
            movement_rounding_kmh: 10,
          },
        },
        active_firs: ['EHAA'],
      } as unknown as SigmetConfig;
      const expectedResult2 = validateSigmet(invalidSigmetConfig2);
      expect(validateConfig('sigmet', invalidSigmetConfig2)).toEqual({
        name: ERROR_CONFIG,
        message: createReadableErrorMessage(
          expectedResult2 as AJVErrorObject[],
        ),
      });
    });
  });

  describe('useProductApi', () => {
    it('fetch sigmet list if valid config', async () => {
      const fakeApi: SigmetAirmetApi = {
        ...createFakeApi(),
        getSigmetList: jest.fn(() => ({ data: sigmetConfig })),
      } as unknown as SigmetAirmetApi;

      const createTestApi: () => SigmetAirmetApi = () => fakeApi;
      const Wrapper = ({ children }: TestWrapperProps): React.ReactElement => (
        <TestWrapper createApi={createTestApi}>{children}</TestWrapper>
      );

      const { result } = renderHook(
        () => useProductApi('sigmet', sigmetConfig),
        {
          wrapper: Wrapper,
        },
      );

      expect(result.current.isLoading).toBeTruthy();
      await waitFor(() => {
        expect(fakeApi.getSigmetList).toHaveBeenCalledTimes(1);
      });
      expect(result.current.result).toEqual(sigmetConfig);
      expect(result.current.error).toBeUndefined();
      expect(result.current.isLoading).toBeFalsy();

      // refetch new data
      act(() => {
        result.current.fetchNewData();
      });
      expect(result.current.isLoading).toBeTruthy();
      expect(fakeApi.getSigmetList).toHaveBeenCalledTimes(2);

      await waitFor(() => expect(result.current.isLoading).toBeFalsy());
    });

    it('should return error on sigmet if fetching fails', async () => {
      const fakeApi: SigmetAirmetApi = {
        ...createFakeApi(),
        getSigmetList: jest.fn(() =>
          Promise.reject(new Error('error fetching')),
        ),
      };

      const createTestApi: () => SigmetAirmetApi = () => fakeApi;
      const Wrapper = ({ children }: TestWrapperProps): React.ReactElement => (
        <TestWrapper createApi={createTestApi}>{children}</TestWrapper>
      );

      const { result } = renderHook(
        () => useProductApi('sigmet', sigmetConfig),
        {
          wrapper: Wrapper,
        },
      );
      expect(result.current.isLoading).toBeTruthy();

      await waitFor(() =>
        expect(result.current.error).toEqual({
          name: ERROR_BACKEND,
        }),
      );
      expect(result.current.isLoading).toBeFalsy();

      // refetch new data
      act(() => {
        result.current.fetchNewData();
      });
      expect(result.current.isLoading).toBeTruthy();
      expect(fakeApi.getSigmetList).toHaveBeenCalledTimes(2);

      await waitFor(() => expect(result.current.isLoading).toBeFalsy());
    });

    it('should not start loading sigmet if config is not valid', async () => {
      const fakeApi: SigmetAirmetApi = {
        ...createFakeApi(),
        getSigmetList: jest.fn(),
      };
      const configWithErrors = {} as unknown as SigmetConfig;

      const createTestApi: () => SigmetAirmetApi = () => fakeApi;
      const Wrapper = ({ children }: TestWrapperProps): React.ReactElement => (
        <TestWrapper createApi={createTestApi}>{children}</TestWrapper>
      );

      const { result } = renderHook(
        () => useProductApi('sigmet', configWithErrors),
        {
          wrapper: Wrapper,
        },
      );
      expect(result.current.isLoading).toBeFalsy();
      expect(fakeApi.getSigmetList).toHaveBeenCalledTimes(0);
      await waitFor(() =>
        expect(result.current.error).toEqual({
          name: ERROR_CONFIG,
          message: createReadableErrorMessage(
            validateSigmet(configWithErrors) as AJVErrorObject[],
          ),
        }),
      );
      expect(result.current.isLoading).toBeFalsy();

      // should not be able to refetch when error on config
      act(() => {
        result.current.fetchNewData();
      });
      expect(result.current.isLoading).toBeFalsy();
      expect(fakeApi.getSigmetList).toHaveBeenCalledTimes(0);
    });

    it('should fetch airmet list if valid config', async () => {
      const fakeApi: SigmetAirmetApi = {
        ...createFakeApi(),
        getAirmetList: jest.fn(() => ({ data: airmetConfig })),
      } as unknown as SigmetAirmetApi;

      const createTestApi: () => SigmetAirmetApi = () => fakeApi;
      const Wrapper = ({ children }: TestWrapperProps): React.ReactElement => (
        <TestWrapper createApi={createTestApi}>{children}</TestWrapper>
      );

      const { result } = renderHook(
        () => useProductApi('airmet', airmetConfig),
        {
          wrapper: Wrapper,
        },
      );

      expect(result.current.isLoading).toBeTruthy();
      await waitFor(() => {
        expect(fakeApi.getAirmetList).toHaveBeenCalledTimes(1);
      });
      expect(result.current.result).toEqual(airmetConfig);
      expect(result.current.error).toBeUndefined();
      expect(result.current.isLoading).toBeFalsy();

      // refetch new data
      act(() => {
        result.current.fetchNewData();
      });
      expect(result.current.isLoading).toBeTruthy();
      expect(fakeApi.getAirmetList).toHaveBeenCalledTimes(2);

      await waitFor(() => expect(result.current.isLoading).toBeFalsy());
    });

    it('should return error on airmet if fetching fails', async () => {
      const fakeApi: SigmetAirmetApi = {
        ...createFakeApi(),
        getAirmetList: jest.fn(() =>
          Promise.reject(new Error('error fetching')),
        ),
      };

      const createTestApi: () => SigmetAirmetApi = () => fakeApi;
      const Wrapper = ({ children }: TestWrapperProps): React.ReactElement => (
        <TestWrapper createApi={createTestApi}>{children}</TestWrapper>
      );

      const { result } = renderHook(
        () => useProductApi('airmet', airmetConfig),
        {
          wrapper: Wrapper,
        },
      );
      expect(result.current.isLoading).toBeTruthy();

      await waitFor(() =>
        expect(result.current.error).toEqual({
          name: ERROR_BACKEND,
        }),
      );
      expect(result.current.isLoading).toBeFalsy();

      // refetch new data
      act(() => {
        result.current.fetchNewData();
      });
      expect(result.current.isLoading).toBeTruthy();
      expect(fakeApi.getAirmetList).toHaveBeenCalledTimes(2);

      await waitFor(() => expect(result.current.isLoading).toBeFalsy());
    });

    it('should not start loading airmet if config is not valid', async () => {
      const fakeApi: SigmetAirmetApi = {
        ...createFakeApi(),
        getAirmetList: jest.fn(),
      };
      const configWithErrors = {} as unknown as AirmetConfig;

      const createTestApi: () => SigmetAirmetApi = () => fakeApi;
      const Wrapper = ({ children }: TestWrapperProps): React.ReactElement => (
        <TestWrapper createApi={createTestApi}>{children}</TestWrapper>
      );

      const { result } = renderHook(
        () => useProductApi('airmet', configWithErrors),
        {
          wrapper: Wrapper,
        },
      );
      expect(result.current.isLoading).toBeFalsy();
      expect(fakeApi.getAirmetList).toHaveBeenCalledTimes(0);
      await waitFor(() =>
        expect(result.current.error).toEqual({
          name: ERROR_CONFIG,
          message: createReadableErrorMessage(
            validateAirmet(configWithErrors) as AJVErrorObject[],
          ),
        }),
      );
      expect(result.current.isLoading).toBeFalsy();

      // should not be able to refetch when error on config
      act(() => {
        result.current.fetchNewData();
      });
      expect(result.current.isLoading).toBeFalsy();
      expect(fakeApi.getAirmetList).toHaveBeenCalledTimes(0);
    });
  });
});
