/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */
import React from 'react';
import {
  Button,
  Grid,
  LinearProgress,
  List,
  ListItemButton,
} from '@mui/material';

import { AlertBanner } from '@opengeoweb/shared';
import ProductListRow from './ProductListRow';
import { AirmetFromBackend, ProductType, SigmetFromBackend } from '../../types';
import { ERROR_CONFIG } from '../MetInfoWrapper/utils';

interface ProductListProps {
  productType: ProductType;
  productList: SigmetFromBackend[] | AirmetFromBackend[];
  isLoading: boolean;
  error: Error;
  onClickProductRow: (product: SigmetFromBackend | AirmetFromBackend) => void;
  onClickNewProduct: () => void;
}

const ProductList: React.FC<ProductListProps> = ({
  productType,
  productList,
  isLoading,
  error,
  onClickProductRow,
  onClickNewProduct,
}: ProductListProps) => {
  const shouldHideCreateNew: boolean = error && error.name === ERROR_CONFIG;

  return (
    <Grid container direction="column" alignItems="stretch">
      <Grid container item justifyContent="flex-end">
        {!shouldHideCreateNew && (
          <Button
            variant="primary"
            data-testid="productListCreateButton"
            onClick={onClickNewProduct}
            sx={{ textTransform: 'initial' }}
          >
            Create a new {productType.toUpperCase()}
          </Button>
        )}
      </Grid>
      <Grid
        item
        sx={{
          width: '100%',
          minHeight: '600px',
          ' .MuiAlert-message': { whiteSpace: 'pre-wrap' },
        }}
      >
        {isLoading && (
          <LinearProgress
            data-testid="productlist-loadingbar"
            color="secondary"
            sx={{ marginTop: '28px', marginBottom: '-32px' }}
          />
        )}
        {error && (
          <AlertBanner
            severity="error"
            title={
              error.name ||
              'An error has occurred while retrieving the list, please try again'
            }
            info={error.message ? error.message : ''}
            dataTestId="productList-alert"
          />
        )}
        {productList && (
          <List sx={{ paddingTop: 4, containerType: 'inline-size' }}>
            {(productList as Array<SigmetFromBackend | AirmetFromBackend>).map(
              (productListItem, index) => {
                const product =
                  productType === 'sigmet'
                    ? (productListItem as SigmetFromBackend)[productType]
                    : (productListItem as AirmetFromBackend)[productType];
                return (
                  <ListItemButton
                    data-testid="productListItem"
                    onClick={(): void => {
                      onClickProductRow(productListItem);
                    }}
                    key={
                      product.sequence && product.issueDate
                        ? `${product.sequence}-${product.issueDate}`
                        : `draft-${index}`
                    }
                    sx={{ padding: 0.25 }}
                  >
                    <ProductListRow
                      product={product}
                      productType={productType}
                    />
                  </ListItemButton>
                );
              },
            )}
          </List>
        )}
      </Grid>
    </Grid>
  );
};

export default ProductList;
