/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */

import { AirmetConfig, SigmetConfig } from '../../types';

// eslint-disable-next-line @typescript-eslint/ban-ts-comment
// @ts-ignore
import sigmetValidator from './sigmetSchemaValidator';
// eslint-disable-next-line @typescript-eslint/ban-ts-comment
// @ts-ignore
import airmetValidator from './airmetSchemaValidator';

interface ErrorObject<
  Keyword extends string = string,
  Params = Record<string, unknown>,
  Schema = unknown,
> {
  keyword: Keyword;
  instancePath: string;
  schemaPath: string;
  params: Params;
  propertyName?: string;
  message?: string;
  schema?: Schema;
  data?: unknown;
}
export type AJVErrorObject = ErrorObject<
  string,
  Record<string, unknown>,
  unknown
>;

interface SchemaValidator {
  errors: [];
}

export const parseErrors = (errors: AJVErrorObject[]): AJVErrorObject[] =>
  errors.reduce<AJVErrorObject[]>((list, error) => {
    // filter out merge errors, as they are not very helpful
    if (error.keyword === '$merge') {
      return list;
    }

    return list.concat(error);
  }, []);

export const createReadableErrorMessage = (errors: AJVErrorObject[]): string =>
  errors
    .reduce<string[]>((list, error) => {
      const instPath = error.instancePath ? `${error.instancePath} ` : '';
      return list.concat(`${instPath}${error.message}`);
    }, [])
    .join('\n');

export const validateSigmet = (
  sigmetConfig: SigmetConfig,
): boolean | AJVErrorObject[] => {
  return (
    sigmetValidator(sigmetConfig) ||
    parseErrors((sigmetValidator as unknown as SchemaValidator).errors!)
  );
};

export const validateAirmet = (
  airmetConfig: AirmetConfig,
): boolean | AJVErrorObject[] => {
  return (
    airmetValidator(airmetConfig) ||
    parseErrors((airmetValidator as unknown as SchemaValidator).errors!)
  );
};
