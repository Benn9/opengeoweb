/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import moment from 'moment';
import { produce } from 'immer';
import { LayerType } from '@opengeoweb/webmap';
import { mapConstants, mapSelectors } from '.';
import {
  createMap,
  dateFormat,
  getAnimationDuration,
  getSpeedFactor,
} from './utils';
import { MapPreset, WebMap } from '../types';
import {
  defaultAnimationDelayAtStart,
  defaultSecondsPerPx,
  defaultTimeStep,
} from './constants';

const mapId = 'map-1';
const mockMap: WebMap = createMap({ id: mapId });
const mockStoreDefaultMap = {
  webmap: {
    byId: {
      [mapId]: mockMap,
    },
    allIds: [mapId],
  },
};
const layerId1 = 'layer-1';
const layerId2 = 'layer-2';
const layerId3 = 'layer-3';
const layerId4 = 'layer-4';

const testGeoJSON = {
  type: 'FeatureCollection',
  features: [
    {
      type: 'Feature',
      geometry: {
        type: 'Polygon',
        coordinates: [
          [
            [5.0, 55.0],
            [4.331914, 55.332644],
            [3.368817, 55.764314],
            [2.761908, 54.379261],
            [3.15576, 52.913554],
            [2.000002, 51.500002],
            [3.370001, 51.369722],
            [3.370527, 51.36867],
            [3.362223, 51.320002],
            [3.36389, 51.313608],
            [3.373613, 51.309999],
            [3.952501, 51.214441],
            [4.397501, 51.452776],
            [5.078611, 51.391665],
            [5.848333, 51.139444],
            [5.651667, 50.824717],
            [6.011797, 50.757273],
            [5.934168, 51.036386],
            [6.222223, 51.361666],
            [5.94639, 51.811663],
            [6.405001, 51.830828],
            [7.053095, 52.237764],
            [7.031389, 52.268885],
            [7.063612, 52.346109],
            [7.065557, 52.385828],
            [7.133055, 52.888887],
            [7.14218, 52.898244],
            [7.191667, 53.3],
            [6.5, 53.666667],
            [6.500002, 55.000002],
            [5.0, 55.0],
          ],
        ],
      },
      properties: {
        selectionType: 'fir',
      },
    },
  ],
} as GeoJSON.FeatureCollection;

const mockStoreDefaultWithPinLocation = {
  webmap: {
    byId: {
      [mapId]: {
        ...mockMap,
        mapPinLocation: { lat: 52.0, lon: 5.0 },
        disableMapPin: true,
        displayMapPin: true,
      },
    },
    allIds: [mapId],
  },
};
const mockStoreMapWithLayers = {
  webmap: {
    byId: {
      [mapId]: {
        ...mockMap,
        mapLayers: [layerId1],
        baseLayers: [layerId2],
        overLayers: [layerId3],
        featureLayers: [layerId4],
        autoTimeStepLayerId: layerId1,
        autoUpdateLayerId: layerId1,
      } as WebMap,
    },
    allIds: [mapId],
  },
  layers: {
    byId: {
      [layerId1]: {
        mapId,
        service: 'https://geoservices.knmi.nl/adagucserver?dataset=RADAR&',
        name: 'RAD_NL25_PCP_CM',
        format: 'image/png',
        style: 'knmiradar/nearest',
        id: layerId1,
        opacity: 1,
        enabled: true,
        layerType: LayerType.mapLayer,
      },
      [layerId2]: {
        mapId,
        name: 'arcGisSat',
        title: 'arcGisSat',
        type: 'twms',
        id: layerId2,
        opacity: 1,
        enabled: true,
        layerType: LayerType.baseLayer,
      },
      [layerId3]: {
        mapId,
        name: 'someOverLayer',
        title: 'someOverLayer',
        type: 'twms',
        id: layerId3,
        opacity: 1,
        enabled: true,
        layerType: LayerType.overLayer,
      },
      [layerId4]: {
        mapId,
        id: layerId4,
        layerType: LayerType.featureLayer,
        geojson: testGeoJSON,
      },
    },
    allIds: [layerId1, layerId2, layerId3, layerId4],
    availableBaseLayers: { allIds: [], byId: {} },
  },
};

describe('store/mapStore/map/selectors', () => {
  beforeEach(() => {
    const now = `${moment.utc().format('YYYY-MM-DD')}T14:00:00Z`;
    jest.spyOn(Date, 'now').mockReturnValue(new Date(now).valueOf());
  });

  describe('getMapById', () => {
    it('should return the map when it exists', () => {
      expect(mapSelectors.getMapById(mockStoreDefaultMap, mapId)).toEqual(
        mockStoreDefaultMap.webmap.byId[mapId],
      );
    });
    it('should return undefined when the map does not exist', () => {
      expect(mapSelectors.getMapById(mockStoreDefaultMap, 'fake-id')).toEqual(
        undefined,
      );
    });
    it('should return undefined when the store does not exist', () => {
      expect(mapSelectors.getMapById(null!, 'fake-id')).toEqual(undefined);
    });
  });

  describe('getAllMapIds', () => {
    it('should return all existing map ids in store', () => {
      expect(mapSelectors.getAllMapIds(mockStoreDefaultMap)).toEqual(
        mockStoreDefaultMap.webmap.allIds,
      );
    });
    it('should return empty array when no maps exist', () => {
      expect(
        mapSelectors.getAllMapIds({
          webmap: {
            byId: {},
            allIds: [],
          },
        }),
      ).toEqual([]);
    });
    it('should return empty array when the store does not exist', () => {
      expect(mapSelectors.getAllMapIds(null!)).toEqual([]);
    });
  });

  describe('getFirstMap', () => {
    it('should return the map when it exists', () => {
      expect(mapSelectors.getFirstMap(mockStoreDefaultMap)).toEqual(
        mockStoreDefaultMap.webmap.byId[
          mockStoreDefaultMap.webmap
            .allIds[0] as keyof typeof mockStoreDefaultMap.webmap.byId
        ],
      );
    });
    it('should return null when no map exists', () => {
      expect(
        mapSelectors.getFirstMap({
          webmap: {
            byId: {},
            allIds: ['mapId1'],
          },
        }),
      ).toEqual(null);
    });

    it('should return null when  map exists', () => {
      expect(
        mapSelectors.getFirstMap({
          webmap: {
            byId: {},
            allIds: [],
          },
        }),
      ).toEqual(null);
    });
    it('should return null when the store does not exist', () => {
      expect(mapSelectors.getFirstMap(null!)).toEqual(null);
    });
  });

  describe('getFirstMap', () => {
    it('should return the map id when it exists', () => {
      expect(mapSelectors.getFirstMapId(mockStoreDefaultMap)).toEqual(
        mockStoreDefaultMap.webmap.allIds[0],
      );
    });
    it('should return empty string when no map exists', () => {
      expect(
        mapSelectors.getFirstMapId({
          webmap: {
            byId: {},
            allIds: [],
          },
        }),
      ).toEqual('');
    });
    it('should return null when the store does not exist', () => {
      expect(mapSelectors.getFirstMap(null!)).toEqual(null);
    });
  });

  describe('getIsMapPresent', () => {
    it('should return true when it exists', () => {
      expect(mapSelectors.getIsMapPresent(mockStoreDefaultMap, mapId)).toEqual(
        true,
      );
    });
    it('should return false when the map does not exist', () => {
      expect(
        mapSelectors.getIsMapPresent(mockStoreDefaultMap, 'fake-id'),
      ).toEqual(false);
    });
    it('should return null when the store does not exist', () => {
      expect(mapSelectors.getIsMapPresent(null!, 'fake-id')).toEqual(false);
    });
  });

  describe('getLayerIds', () => {
    it('should return all layer ids for the map', () => {
      expect(mapSelectors.getLayerIds(mockStoreMapWithLayers, mapId)).toEqual(
        mockStoreMapWithLayers.webmap.byId[mapId].mapLayers,
      );
    });
    it('should return an empty list if the map has no layers', () => {
      expect(mapSelectors.getLayerIds(mockStoreDefaultMap, mapId)).toHaveLength(
        0,
      );
    });
    it('should return an empty list if the map does not exist', () => {
      expect(
        mapSelectors.getLayerIds(mockStoreDefaultMap, 'fake-id'),
      ).toHaveLength(0);
    });
  });

  describe('getLayers', () => {
    it('should return the list of layer objects for the map', () => {
      expect(mapSelectors.getMapLayers(mockStoreMapWithLayers, mapId)).toEqual([
        {
          mapId,
          service: 'https://geoservices.knmi.nl/adagucserver?dataset=RADAR&',
          name: 'RAD_NL25_PCP_CM',
          format: 'image/png',
          style: 'knmiradar/nearest',
          id: layerId1,
          opacity: 1,
          enabled: true,
          layerType: LayerType.mapLayer,
        },
      ]);
    });
    it('should return an empty list when the map has no layers', () => {
      expect(
        mapSelectors.getMapLayers(mockStoreDefaultMap, mapId),
      ).toHaveLength(0);
    });
  });

  describe('getMapBaseLayersIds', () => {
    it('should return a list of baselayer ids for the map', () => {
      const result = mapSelectors.getMapBaseLayersIds(
        mockStoreMapWithLayers,
        mapId,
      );
      expect(result).toHaveLength(1);
      expect(result).toEqual(
        mockStoreMapWithLayers.webmap.byId[mapId].baseLayers,
      );
    });
    it('should return an empty list when store does not exist', () => {
      const result = mapSelectors.getMapBaseLayersIds(null!, mapId);
      expect(result).toHaveLength(0);
    });
  });

  describe('getMapBaseLayers', () => {
    it('should return a list of baseLayer objects for the map', () => {
      expect(
        mapSelectors.getMapBaseLayers(mockStoreMapWithLayers, mapId),
      ).toEqual([
        {
          mapId,
          name: 'arcGisSat',
          title: 'arcGisSat',
          type: 'twms',
          id: layerId2,
          layerType: LayerType.baseLayer,
          opacity: 1,
          enabled: true,
        },
      ]);
    });
    it('should return an empty list when the map has no baselayers', () => {
      expect(
        mapSelectors.getMapBaseLayers(mockStoreDefaultMap, mapId),
      ).toHaveLength(0);
    });
  });

  describe('getMapOverLayersIds', () => {
    it('should return a list of overlayer ids for the map', () => {
      const result = mapSelectors.getMapOverLayersIds(
        mockStoreMapWithLayers,
        mapId,
      );
      expect(result).toHaveLength(1);
      expect(result).toEqual(
        mockStoreMapWithLayers.webmap.byId[mapId].overLayers,
      );
    });
    it('should return an empty list when store does not exist', () => {
      const result = mapSelectors.getMapOverLayersIds(null!, mapId);
      expect(result).toHaveLength(0);
    });
  });

  describe('getMapFeatureLayers', () => {
    it('should return a list of baseLayer objects for the map', () => {
      expect(
        mapSelectors.getMapFeatureLayers(mockStoreMapWithLayers, mapId),
      ).toEqual([
        {
          mapId,
          id: layerId4,
          layerType: LayerType.featureLayer,
          geojson: testGeoJSON,
        },
      ]);
    });
    it('should return an empty list when the map has no baselayers', () => {
      expect(
        mapSelectors.getMapFeatureLayers(mockStoreDefaultMap, mapId),
      ).toHaveLength(0);
    });
  });

  describe('getMapOverLayers', () => {
    it('should return a list of overLayer objects for the map', () => {
      expect(
        mapSelectors.getMapOverLayers(mockStoreMapWithLayers, mapId),
      ).toEqual([
        {
          mapId,
          name: 'someOverLayer',
          title: 'someOverLayer',
          type: 'twms',
          id: layerId3,
          opacity: 1,
          enabled: true,
          layerType: LayerType.overLayer,
        },
      ]);
    });
    it('should return an empty list when the map has no overLayers', () => {
      expect(
        mapSelectors.getMapOverLayers(mockStoreDefaultMap, mapId),
      ).toHaveLength(0);
    });
  });

  describe('getMapDimensions', () => {
    const mockStoreMapWithDimensions = {
      webmap: {
        byId: {
          [mapId]: {
            ...mockMap,
            dimensions: [
              {
                name: 'time',
                units: 'ISO8601',
                currentValue: '2020-03-13T13:30:00Z',
              },
            ],
            isAnimating: true,
          },
        },
        allIds: [mapId],
      },
    };

    it('should return the dimensions for the map', () => {
      expect(
        mapSelectors.getMapDimensions(mockStoreMapWithDimensions, mapId),
      ).toEqual(mockStoreMapWithDimensions.webmap.byId[mapId].dimensions);
    });
    it('should return an empty list when store does not exist', () => {
      expect(mapSelectors.getMapDimensions(null!, mapId)).toHaveLength(0);
    });
  });

  describe('getMapDimension', () => {
    const mockStoreMapWithDimensions = {
      webmap: {
        byId: {
          [mapId]: {
            ...mockMap,
            dimensions: [
              {
                name: 'time',
                units: 'ISO8601',
                currentValue: '2020-03-13T13:30:00Z',
              },
            ],
            isAnimating: true,
          },
        },
        allIds: [mapId],
      },
    };

    it('should return the requested dimension for the map', () => {
      expect(
        mapSelectors.getMapDimension(mockStoreMapWithDimensions, mapId, 'time'),
      ).toEqual(mockStoreMapWithDimensions.webmap.byId[mapId].dimensions[0]);
    });
    it('should return undefined when store does not exist', () => {
      expect(
        mapSelectors.getMapDimension(null!, mapId, 'dimname-does-not-exist'),
      ).toEqual(undefined);
    });
    it('should return undefined when dimension does not exist for the map', () => {
      expect(
        mapSelectors.getMapDimension(
          mockStoreMapWithDimensions,
          mapId,
          'elevation',
        ),
      ).toEqual(undefined);
    });
  });

  describe('getSrs', () => {
    it('should return the Srs for the map', () => {
      expect(mapSelectors.getSrs(mockStoreDefaultMap, mapId)).toEqual(
        mockStoreDefaultMap.webmap.byId[mapId].srs,
      );
    });
    it('should return an empty string when store does not exist', () => {
      expect(mapSelectors.getSrs(null!, mapId)).toEqual('');
    });
  });

  describe('getBbox', () => {
    it('should return the Bbox for the map', () => {
      expect(mapSelectors.getBbox(mockStoreDefaultMap, mapId)).toEqual(
        mockStoreDefaultMap.webmap.byId[mapId].bbox,
      );
    });
    it('should return an empty object when store does not exist', () => {
      expect(mapSelectors.getBbox(null!, mapId)).toMatchObject({});
    });
  });

  describe('isAnimating', () => {
    it('should return isAnimating for the map', () => {
      expect(mapSelectors.isAnimating(mockStoreDefaultMap, mapId)).toEqual(
        mockStoreDefaultMap.webmap.byId[mapId].isAnimating,
      );
    });
    it('should return false when store does not exist', () => {
      expect(mapSelectors.isAnimating(null!, mapId)).toEqual(false);
    });
  });

  describe('getAnimationStartTime', () => {
    it('should return map animation start time', () => {
      const mockStoreMapAnimationTimes = {
        webmap: {
          byId: {
            [mapId]: {
              ...mockMap,
              animationStartTime: moment.utc().subtract(6, 'h').toISOString(),
              animationEndTime: moment.utc().subtract(10, 'm').toISOString(),
            },
          },
          allIds: [mapId],
        },
      };
      expect(
        mapSelectors.getAnimationStartTime(mockStoreMapAnimationTimes, mapId),
      ).toBe(mockStoreMapAnimationTimes.webmap.byId[mapId].animationStartTime);
    });
    it('should return utc time - 6 hours when map does not exist', () => {
      expect(mapSelectors.getAnimationStartTime(null!, mapId)).toBe(
        moment.utc().subtract(6, 'h').format(dateFormat),
      );
    });
  });

  describe('getAnimationEndTime', () => {
    it('should return map animation end time', () => {
      const mockStoreMapAnimationTimes = {
        webmap: {
          byId: {
            [mapId]: {
              ...mockMap,
              animationStartTime: moment.utc().subtract(6, 'h').toISOString(),
              animationEndTime: moment.utc().subtract(10, 'm').toISOString(),
            },
          },
          allIds: [mapId],
        },
      };
      expect(
        mapSelectors.getAnimationEndTime(mockStoreMapAnimationTimes, mapId),
      ).toBe(mockStoreMapAnimationTimes.webmap.byId[mapId].animationEndTime);
    });
    it('should return utc time - 10 minutes when map does not exist', () => {
      expect(mapSelectors.getAnimationEndTime(null!, mapId)).toBe(
        moment.utc().subtract(10, 'm').format(dateFormat),
      );
    });
  });

  describe('isAutoUpdating', () => {
    const mockStoreMapActiveLayer = {
      webmap: {
        byId: {
          [mapId]: { ...mockMap, isAutoUpdating: true },
        },
        allIds: [mapId],
      },
    };

    it('should return map is auto updating', () => {
      expect(
        mapSelectors.isAutoUpdating(mockStoreMapActiveLayer, mapId),
      ).toEqual(mockStoreMapActiveLayer.webmap.byId[mapId].isAutoUpdating);
    });
    it('should return an false when map does not exist', () => {
      expect(mapSelectors.isAutoUpdating(null!, mapId)).toBeFalsy();
    });
  });

  describe('getActiveLayerId', () => {
    const mockStoreMapActiveLayer = {
      webmap: {
        byId: {
          [mapId]: {
            ...mockMap,
            autoUpdateLayerId: layerId2,
            autoTimeStepLayerId: layerId2,
          } as WebMap,
        },
        allIds: [mapId],
      },
    };

    it('should return the active layer id for the map', () => {
      expect(
        mapSelectors.getActiveLayerId(mockStoreMapActiveLayer, mapId),
      ).toEqual(layerId2);
    });
    it('should return undefined when store does not exist', () => {
      expect(mapSelectors.getActiveLayerId(undefined, mapId)).toEqual(
        undefined,
      );
    });
  });

  describe('getAutoUpdateLayerId', () => {
    const mockStore = {
      webmap: {
        byId: {
          [mapId]: {
            ...mockMap,
            autoUpdateLayerId: layerId2,
          } as WebMap,
        },
        allIds: [mapId],
      },
    };

    it('should return the auto update layer id for the map', () => {
      expect(mapSelectors.getAutoUpdateLayerId(mockStore, mapId)).toEqual(
        layerId2,
      );
    });
    it('should return undefined when store does not exist', () => {
      expect(mapSelectors.getAutoUpdateLayerId(undefined, mapId)).toEqual(
        undefined,
      );
    });
  });

  describe('getAutoTimeStepLayerId', () => {
    const mockStore = {
      webmap: {
        byId: {
          [mapId]: {
            ...mockMap,
            autoTimeStepLayerId: layerId2,
          } as WebMap,
        },
        allIds: [mapId],
      },
    };

    it('should return the active layer id for the map', () => {
      expect(mapSelectors.getAutoTimeStepLayerId(mockStore, mapId)).toEqual(
        layerId2,
      );
    });
    it('should return an undefined when store does not exist', () => {
      expect(mapSelectors.getAutoTimeStepLayerId(undefined, mapId)).toEqual(
        undefined,
      );
    });
  });

  describe('getMaptimeSliderSpan', () => {
    const mockStoreMapTimeSliderSpan = {
      webmap: {
        byId: {
          [mapId]: { ...mockMap, timeSliderSpan: 24 * 3600 },
        },
        allIds: [mapId],
      },
    };

    it('should return map time step', () => {
      expect(
        mapSelectors.getMapTimeSliderSpan(mockStoreMapTimeSliderSpan, mapId),
      ).toEqual(mockStoreMapTimeSliderSpan.webmap.byId[mapId].timeSliderSpan);
    });
    it('should return time span when map does not exist', () => {
      expect(mapSelectors.getMapTimeSliderSpan(null!, mapId)).toEqual(
        mapConstants.defaultTimeSpan,
      );
    });
  });

  describe('getMaptimeStep', () => {
    const mockStoreMapTimeStep = {
      webmap: {
        byId: {
          [mapId]: { ...mockMap, timeStep: 10 },
        },
        allIds: [mapId],
      },
    };

    it('should return map time step', () => {
      expect(mapSelectors.getMapTimeStep(mockStoreMapTimeStep, mapId)).toEqual(
        mockStoreMapTimeStep.webmap.byId[mapId].timeStep,
      );
    });
    it('should return defaultTimeStep when map does not exist', () => {
      expect(mapSelectors.getMapTimeStep(null!, mapId)).toEqual(
        defaultTimeStep,
      );
    });
  });

  describe('getMapTimeStepWithoutDefault', () => {
    const mockStoreMapTimeStep = {
      webmap: {
        byId: {
          [mapId]: { ...mockMap, timeStep: 10 },
        },
        allIds: [mapId],
      },
    };

    it('should return map time step', () => {
      expect(
        mapSelectors.getMapTimeStepWithoutDefault(mockStoreMapTimeStep, mapId),
      ).toEqual(mockStoreMapTimeStep.webmap.byId[mapId].timeStep);
    });
    it('should return undefined when map does not exist', () => {
      expect(mapSelectors.getMapTimeStepWithoutDefault(null!, mapId)).toEqual(
        undefined,
      );
    });
  });

  describe('getMapTimeSliderCenterTime', () => {
    const time = moment().unix();
    const storeWithCurrentTime = {
      webmap: {
        byId: {
          [mapId]: {
            ...mockMap,
            timeSliderCenterTime: time,
          },
        },
        allIds: [mapId],
      },
    };
    it('should return the current map time slider center time', () => {
      expect(
        mapSelectors.getMapTimeSliderCenterTime(storeWithCurrentTime, mapId),
      ).toEqual(time);
    });

    it('should return the current time when store does not exist', () => {
      const now = `${moment.utc().format('YYYY-MM-DD')}T14:00:00Z`;
      jest.spyOn(Date, 'now').mockReturnValue(new Date(now).valueOf());
      expect(mapSelectors.getMapTimeSliderCenterTime(null!, mapId)).toEqual(
        moment(now).unix(),
      );
    });
  });

  describe('getTimeSliderUnfilteredSelectedTime', () => {
    const time = moment().unix();
    const storeWithCurrentTime = {
      webmap: {
        byId: {
          [mapId]: {
            ...mockMap,
            timeSliderUnfilteredSelectedTime: time,
          },
        },
        allIds: [mapId],
      },
    };
    it('should return the current map time slider center time', () => {
      expect(
        mapSelectors.getTimeSliderUnfilteredSelectedTime(
          storeWithCurrentTime,
          mapId,
        ),
      ).toEqual(time);
    });

    it('should return undefined if store does not exist', () => {
      expect(
        mapSelectors.getTimeSliderUnfilteredSelectedTime(null!, mapId),
      ).toBeUndefined();
    });
  });

  describe('getMapTimeSliderSecondsPerPx', () => {
    it('should return the map time slider secondsPerPx value', () => {
      expect(
        mapSelectors.getMapTimeSliderSecondsPerPx(mockStoreDefaultMap, mapId),
      ).toEqual(mockStoreDefaultMap.webmap.byId[mapId].timeSliderSecondsPerPx);
    });

    it('should return 80 when map does not exist', () => {
      expect(mapSelectors.getMapTimeSliderSecondsPerPx(null!, mapId)).toEqual(
        80,
      );
    });
  });

  describe('getMapTimeSliderSecondsPerPx', () => {
    it('should return the map time slider secondsPerPx value', () => {
      expect(
        mapSelectors.getMapTimeSliderSecondsPerPx(mockStoreDefaultMap, mapId),
      ).toEqual(mockStoreDefaultMap.webmap.byId[mapId].timeSliderSecondsPerPx);
    });

    it('should return defaultSecondsPerPx when map does not exist', () => {
      expect(mapSelectors.getMapTimeSliderSecondsPerPx(null!, mapId)).toEqual(
        defaultSecondsPerPx,
      );
    });
  });

  describe('isTimestepAuto', () => {
    const mockStoreMapActiveLayer = {
      webmap: {
        byId: {
          [mapId]: { ...mockMap, isTimestepAuto: true },
        },
        allIds: [mapId],
      },
    };
    it('should return map is timestep auto mode', () => {
      expect(
        mapSelectors.isTimestepAuto(mockStoreMapActiveLayer, mapId),
      ).toEqual(mockStoreMapActiveLayer.webmap.byId[mapId].isTimestepAuto);
    });
    it('should return an false when map does not exist', () => {
      expect(mapSelectors.isTimestepAuto(null!, mapId)).toBeFalsy();
    });
  });

  describe('use time slider hover', () => {
    const mockStoreMapActiveLayer = {
      webmap: {
        byId: {
          [mapId]: { ...mockMap, isTimeSliderHoverOn: true },
        },
        allIds: [mapId],
      },
    };

    it('should return is map using time slider hover', () => {
      expect(
        mapSelectors.isTimeSliderHoverOn(mockStoreMapActiveLayer, mapId),
      ).toEqual(mockStoreMapActiveLayer.webmap.byId[mapId].isTimeSliderHoverOn);
    });
    it('should return an false when map does not exist', () => {
      expect(mapSelectors.isTimeSliderHoverOn(null!, mapId)).toBeFalsy();
    });
  });

  describe('isZoomControlsVisible', () => {
    const mockStore = {
      webmap: {
        byId: {
          [mapId]: { ...mockMap, shouldShowZoomControls: true },
        },
        allIds: [mapId],
      },
    };

    it('should return if zoom controls on map are visible', () => {
      expect(mapSelectors.isZoomControlsVisible(mockStore, mapId)).toEqual(
        mockStore.webmap.byId[mapId].shouldShowZoomControls,
      );
    });
    it('should return an true when map does not exist', () => {
      expect(mapSelectors.isZoomControlsVisible(null!, mapId)).toBeTruthy();
    });
  });

  describe('isTimeSliderVisible', () => {
    const mockStoreMapActiveLayer = {
      webmap: {
        byId: {
          [mapId]: { ...mockMap, isTimeSliderVisible: false },
        },
        allIds: [mapId],
      },
    };

    it('should return if timeslider on map is visible', () => {
      expect(
        mapSelectors.isTimeSliderVisible(mockStoreMapActiveLayer, mapId),
      ).toEqual(mockStoreMapActiveLayer.webmap.byId[mapId].isTimeSliderVisible);
    });
    it('should return an true when map does not exist', () => {
      expect(mapSelectors.isTimeSliderVisible(null!, mapId)).toBeTruthy();
    });
  });

  describe('getIsLayerActiveLayer', () => {
    it('should return true when layer is active layer', () => {
      expect(
        mapSelectors.getIsLayerActiveLayer(
          mockStoreMapWithLayers,
          mapId,
          layerId1,
        ),
      ).toBeTruthy();
      expect(
        mapSelectors.getIsLayerActiveLayer(
          mockStoreMapWithLayers,
          mapId,
          layerId2,
        ),
      ).toBeFalsy();
      expect(
        mapSelectors.getIsLayerActiveLayer(
          mockStoreMapWithLayers,
          mapId,
          layerId3,
        ),
      ).toBeFalsy();
    });
    it('should return false when not providing mapId or layerId', () => {
      expect(
        mapSelectors.getIsLayerActiveLayer(null!, null!, null!),
      ).toBeFalsy();
    });
  });

  describe('getMapIdFromLayerId', () => {
    it('should return mapId for given layerId', () => {
      expect(
        mapSelectors.getMapIdFromLayerId(mockStoreMapWithLayers, layerId1),
      ).toBe(mapId);
      expect(
        mapSelectors.getMapIdFromLayerId(
          mockStoreMapWithLayers,
          'non-existing-layer-id',
        ),
      ).toBe(null);
    });
  });

  describe('getLayerIdByLayerName', () => {
    it('should return layerId for given layerName', () => {
      expect(
        mapSelectors.getLayerIdByLayerName(
          mockStoreMapWithLayers,
          mapId,
          'RAD_NL25_PCP_CM',
        ),
      ).toBe(layerId1);
      expect(
        mapSelectors.getLayerIdByLayerName(
          mockStoreMapWithLayers,
          mapId,
          'non-existing-layer-name',
        ),
      ).toBe(null);
      expect(
        mapSelectors.getLayerIdByLayerName(
          mockStoreMapWithLayers,
          'non-existing-mapid',
          'RAD_NL25_PCP_CM',
        ),
      ).toBe(null);
    });
  });

  describe('getLayerIndexByLayerId', () => {
    it('should return layeIndex for given layerId', () => {
      expect(
        mapSelectors.getLayerIndexByLayerId(
          mockStoreMapWithLayers,
          mapId,
          layerId1,
        ),
      ).toBe(0);
      expect(
        mapSelectors.getLayerIndexByLayerId(
          mockStoreMapWithLayers,
          mapId,
          'non-existing-layer-id',
        ),
      ).toBe(-1);

      expect(
        mapSelectors.getLayerIndexByLayerId(
          mockStoreMapWithLayers,
          'non-existing-mapid',
          'non-existing-layer-id',
        ),
      ).toBe(-1);
    });
  });

  describe('getLayerByLayerIndex', () => {
    it('should return the Layer in the map for given layerIndex', () => {
      const firstLayer = mapSelectors.getMapLayers(
        mockStoreMapWithLayers,
        mapId,
      )[0];
      expect(
        mapSelectors.getLayerByLayerIndex(mockStoreMapWithLayers, mapId, 0),
      ).toEqual(firstLayer);
      expect(
        mapSelectors.getLayerByLayerIndex(mockStoreMapWithLayers, mapId, 10),
      ).toEqual(null);
      expect(
        mapSelectors.getLayerByLayerIndex(
          mockStoreMapWithLayers,
          'non-existing-mapid',
          10,
        ),
      ).toEqual(null);
    });
  });

  describe('getAllUniqueDimensions', () => {
    const mapId2 = 'map-2';
    const mockMap2 = createMap({ id: mapId2 });
    const mockStoreMapWithDimensions = {
      webmap: {
        byId: {
          [mapId]: {
            ...mockMap,
            dimensions: [
              {
                name: 'time',
                currentValue: '2021-11-11T12:50:00Z',
              },
              {
                name: 'reference_time',
                currentValue: '2021-11-11T09:00:00Z',
              },
              {
                name: 'elevation',
                currentValue: '850',
              },
            ],
          },
          [mapId2]: {
            ...mockMap2,
            dimensions: [
              {
                name: 'time',
                currentValue: '2021-11-11T12:50:00Z',
              },
              {
                name: 'reference_time',
                currentValue: '2021-11-11T09:00:00Z',
              },
              {
                name: 'ensemble_member',
                currentValue: '0',
              },
            ],
          },
        },
        allIds: [mapId, mapId2],
      },
    };
    it('should return all unique dimension names across all maps or an empty array if no store', () => {
      expect(
        mapSelectors.getAllUniqueDimensions(mockStoreMapWithDimensions),
      ).toEqual(['time', 'reference_time', 'elevation', 'ensemble_member']);
      expect(mapSelectors.getAllUniqueDimensions(null!)).toEqual([]);
    });
  });

  describe('getPinLocation', () => {
    it('should return pin location for map', () => {
      expect(
        mapSelectors.getPinLocation(mockStoreDefaultWithPinLocation, mapId),
      ).toEqual({ lat: 52.0, lon: 5.0 });
    });

    it('no store', () => {
      expect(mapSelectors.getPinLocation(null!, mapId)).toBeUndefined();
    });

    it('map pin not set', () => {
      const storeWithoutMapPin = produce(mockStoreDefaultMap, (draftState) => {
        // eslint-disable-next-line no-param-reassign
        draftState.webmap.byId[mapId].mapPinLocation = undefined;
      });
      expect(mapSelectors.getPinLocation(storeWithoutMapPin, mapId)).toEqual(
        undefined,
      );
    });
  });

  describe('getDisableMapPin', () => {
    it('no store', () => {
      expect(mapSelectors.getDisableMapPin(null!, mapId)).toEqual(false);
    });

    it('map pin not set', () => {
      expect(mapSelectors.getDisableMapPin(mockStoreDefaultMap, mapId)).toEqual(
        false,
      );
    });

    it('disabled map pin set to true', () => {
      expect(
        mapSelectors.getDisableMapPin(mockStoreDefaultWithPinLocation, mapId),
      ).toEqual(true);
    });
  });

  describe('getDisplayMapPin', () => {
    it('no store', () => {
      expect(mapSelectors.getDisplayMapPin(null!, mapId)).toBeFalsy();
    });

    it('map pin not set', () => {
      expect(
        mapSelectors.getDisplayMapPin(mockStoreDefaultMap, mapId),
      ).toBeFalsy();
    });

    it('disabled map pin set to true', () => {
      expect(
        mapSelectors.getDisplayMapPin(mockStoreDefaultWithPinLocation, mapId),
      ).toEqual(true);
    });
  });

  describe('getLegendId', () => {
    it('no store', () => {
      expect(mapSelectors.getLegendId(null!, mapId)).toBeUndefined();
    });

    it('legend has been set', () => {
      const legendId = 'test-legend-1';
      const mockStoreWithActivePreset = {
        webmap: {
          byId: {
            [mapId]: {
              ...mockMap,
              legendId,
            },
          },
          allIds: [mapId],
        },
      };

      expect(
        mapSelectors.getLegendId(mockStoreWithActivePreset, mapId),
      ).toEqual(legendId);
    });

    it('legend has not been set', () => {
      expect(
        mapSelectors.getLegendId(mockStoreDefaultMap, mapId),
      ).toBeUndefined();
    });
  });

  describe('getMapPreset', () => {
    it('no store', () => {
      expect(mapSelectors.getMapPreset(null!, mapId)).toEqual({
        layers: [],
        proj: {
          bbox: {},
          srs: '',
        },
        shouldAnimate: false,
        shouldAutoUpdate: false,
        showTimeSlider: true,
        displayMapPin: false,
        shouldShowZoomControls: true,
        animationPayload: {
          interval: defaultTimeStep,
          speed: getSpeedFactor(defaultAnimationDelayAtStart),
          duration: getAnimationDuration(
            mapSelectors.getAnimationEndTime(null!, mapId),
            mapSelectors.getAnimationStartTime(null!, mapId),
          ),
        },
        toggleTimestepAuto: false,
      });
    });

    it('mapPreset has been set', () => {
      const expectedLayersResult = [
        mockStoreMapWithLayers.layers.byId[layerId2],
        mockStoreMapWithLayers.layers.byId[layerId3],
        mockStoreMapWithLayers.layers.byId[layerId1],
      ].map(({ mapId, ...layer }) => layer);
      const webMap = mockStoreMapWithLayers.webmap.byId[mapId];
      expect(mapSelectors.getMapPreset(mockStoreMapWithLayers, mapId)).toEqual({
        layers: expectedLayersResult,
        proj: {
          bbox: webMap.bbox,
          srs: webMap.srs,
        },
        activeLayerId: webMap.autoTimeStepLayerId,
        autoTimeStepLayerId: webMap.autoTimeStepLayerId,
        autoUpdateLayerId: webMap.autoUpdateLayerId,
        shouldAnimate: false,
        shouldAutoUpdate: false,
        showTimeSlider: true,
        displayMapPin: false,
        shouldShowZoomControls: true,
        animationPayload: {
          interval: defaultTimeStep,
          speed: getSpeedFactor(defaultAnimationDelayAtStart),
          duration: getAnimationDuration(
            webMap.animationEndTime,
            webMap.animationStartTime,
          ),
        },
        toggleTimestepAuto: true,
      } as MapPreset);
    });

    it('mapPreset has been set with custom vars', () => {
      const legendId = 'legend-1';
      const mockStoreWithAnimateAutoUpdate = {
        webmap: {
          byId: {
            [mapId]: {
              ...mockMap,
              isAnimating: true,
              isAutoUpdating: true,
              displayMapPin: true,
              shouldShowZoomControls: false,
              timeStep: 1234,
              animationDelay: 250,
              isTimestepAuto: false,
              legendId,
            },
          },
          allIds: [mapId],
        },
        ui: {
          dialogs: {
            [legendId]: {
              isOpen: false,
              type: legendId,
              activeMapId: mapId,
            },
          },
          order: [legendId],
          activeWindowId: 'test1',
        },
      };
      const result = mapSelectors.getMapPreset(
        mockStoreWithAnimateAutoUpdate,
        mapId,
      );
      expect(result.shouldAnimate).toEqual(
        mockStoreWithAnimateAutoUpdate.webmap.byId[mapId].isAnimating,
      );
      expect(result.shouldAutoUpdate).toEqual(
        mockStoreWithAnimateAutoUpdate.webmap.byId[mapId].isAutoUpdating,
      );
      expect(result.displayMapPin).toBeTruthy();
      expect(result.shouldShowZoomControls).toEqual(
        mockStoreWithAnimateAutoUpdate.webmap.byId[mapId]
          .shouldShowZoomControls,
      );
      expect(result.animationPayload!.interval).toEqual(
        mockStoreWithAnimateAutoUpdate.webmap.byId[mapId].timeStep,
      );
      expect(result.animationPayload!.speed).toEqual(
        getSpeedFactor(
          mockStoreWithAnimateAutoUpdate.webmap.byId[mapId].animationDelay,
        ),
      );
      expect(result.toggleTimestepAuto).toEqual(
        mockStoreWithAnimateAutoUpdate.webmap.byId[mapId].isTimestepAuto,
      );
      expect(result.shouldShowLegend).toBeFalsy();
    });

    it('should remove time and reference time dimension from layers but leave other dimensions', () => {
      const timeDims = [
        {
          name: 'time',
          units: 'ISO8601',
          currentValue: '2022-07-28T06:00:00Z',
          minValue: '2022-07-26T06:00:00Z',
          maxValue: '2022-07-28T06:00:00Z',
          synced: false,
        },
        {
          name: 'reference_time',
          units: 'ISO8601',
          currentValue: '2022-07-26T06:00:00Z',
          minValue: '2022-07-19T00:00:00Z',
          maxValue: '2022-07-26T06:00:00Z',
          timeInterval: null!,
          synced: false,
        },
      ];
      const layer1 = {
        mapId,
        service: 'https://geoservices.knmi.nl/adagucserver?dataset=RADAR&',
        name: 'RAD_NL25_PCP_CM',
        format: 'image/png',
        style: 'knmiradar/nearest',
        id: layerId1,
        opacity: 1,
        enabled: true,
        layerType: LayerType.mapLayer,
        dimensions: [
          {
            name: 'elevation',
            units: 'hPa',
            currentValue: '200',
            minValue: '200',
            maxValue: '1000',
            timeInterval: null!,
            synced: false,
          },
        ],
      };
      const mockStoreMapWithLayersWithDim = {
        webmap: {
          byId: {
            [mapId]: {
              ...mockMap,
              mapLayers: [layerId1],
              baseLayers: [layerId2],
              overLayers: [layerId3],
            } as WebMap,
          },
          allIds: [mapId],
        },
        layers: {
          byId: {
            [layerId1]: {
              ...layer1,
              dimensions: [...layer1.dimensions, ...timeDims],
            },
            [layerId2]: {
              mapId,
              name: 'arcGisSat',
              title: 'arcGisSat',
              type: 'twms',
              id: layerId2,
              opacity: 1,
              enabled: true,
              layerType: LayerType.baseLayer,
            },
            [layerId3]: {
              mapId,
              name: 'someOverLayer',
              title: 'someOverLayer',
              type: 'twms',
              id: layerId3,
              opacity: 1,
              enabled: true,
              layerType: LayerType.overLayer,
            },
          },
          allIds: [layerId1, layerId2, layerId3],
          availableBaseLayers: { allIds: [], byId: {} },
        },
      };
      const expectedLayersResult = [
        mockStoreMapWithLayersWithDim.layers.byId[layerId2],
        mockStoreMapWithLayersWithDim.layers.byId[layerId3],
        layer1,
      ].map(({ mapId, ...layer }) => layer);
      expect(
        mapSelectors.getMapPreset(mockStoreMapWithLayersWithDim, mapId),
      ).toEqual({
        layers: expectedLayersResult,
        proj: {
          bbox: mockStoreMapWithLayersWithDim.webmap.byId[mapId].bbox,
          srs: mockStoreMapWithLayersWithDim.webmap.byId[mapId].srs,
        },
        shouldAnimate: false,
        shouldAutoUpdate: false,
        showTimeSlider: true,
        displayMapPin: false,
        shouldShowZoomControls: true,
        animationPayload: {
          interval: defaultTimeStep,
          speed: getSpeedFactor(defaultAnimationDelayAtStart),
          duration: getAnimationDuration(
            mockMap.animationEndTime,
            mockMap.animationStartTime,
          ),
        },
        toggleTimestepAuto: true,
      });
    });
    it('mapPreset has not been set', () => {
      expect(mapSelectors.getMapPreset(mockStoreDefaultMap, mapId)).toEqual({
        layers: [],
        proj: {
          bbox: mockStoreDefaultMap.webmap.byId[mapId].bbox,
          srs: mockStoreDefaultMap.webmap.byId[mapId].srs,
        },
        shouldAnimate: false,
        shouldAutoUpdate: false,
        showTimeSlider: true,
        displayMapPin: false,
        shouldShowZoomControls: true,
        animationPayload: {
          interval: defaultTimeStep,
          speed: getSpeedFactor(defaultAnimationDelayAtStart),
          duration: getAnimationDuration(
            mockStoreDefaultMap.webmap.byId[mapId].animationEndTime,
            mockStoreDefaultMap.webmap.byId[mapId].animationStartTime,
          ),
        },
        toggleTimestepAuto: true,
      });
    });

    it('should not return map id of layers', () => {
      const layer1 = {
        mapId,
        service: 'https://geoservices.knmi.nl/adagucserver?dataset=RADAR&',
        name: 'RAD_NL25_PCP_CM',
        format: 'image/png',
        style: 'knmiradar/nearest',
        id: layerId1,
        opacity: 1,
        enabled: true,
        layerType: LayerType.mapLayer,
        dimensions: [
          {
            name: 'elevation',
            units: 'hPa',
            currentValue: '200',
            minValue: '200',
            maxValue: '1000',
            timeInterval: null!,
            synced: false,
          },
        ],
      };
      const mockStoreWithMapIdOnLayer = {
        webmap: {
          byId: {
            [mapId]: {
              ...mockMap,
              mapLayers: [layerId1],
              baseLayers: [layerId2],
              overLayers: [layerId3],
              activeLayerId: layerId1,
            },
          },
          allIds: [mapId],
        },
        layers: {
          byId: {
            [layerId1]: {
              ...layer1,
            },
            [layerId2]: {
              mapId,
              name: 'arcGisSat',
              title: 'arcGisSat',
              type: 'twms',
              id: layerId2,
              opacity: 1,
              enabled: true,
              layerType: LayerType.baseLayer,
            },
            [layerId3]: {
              mapId,
              name: 'someOverLayer',
              title: 'someOverLayer',
              type: 'twms',
              id: layerId3,
              opacity: 1,
              enabled: true,
              layerType: LayerType.overLayer,
            },
          },
          allIds: [layerId1, layerId2, layerId3],
          availableBaseLayers: { allIds: [], byId: {} },
        },
      };
      const result = mapSelectors.getMapPreset(
        mockStoreWithMapIdOnLayer,
        mapId,
      );

      expect(result.layers);

      result.layers!.forEach((layer) => expect(layer.mapId).toBeUndefined());
    });
  });

  // For getMapLayerIdsEnabled and getIsEnabledLayersForMapDimension
  const layerId5 = 'layerId5';
  const otherMapLayer = 'otherMapLayer';
  const otherMapLayer2 = 'otherMapLayer2';
  const otherMap = 'otherMap';
  const mockMapWithLayersStore = {
    webmap: {
      byId: {
        [mapId]: {
          ...mockMap,
          mapLayers: [layerId1, layerId5],
          baseLayers: [layerId2],
          overLayers: [layerId3],
          activeLayerId: layerId1,
        },
        [otherMap]: {
          ...mockMap,
          mapLayers: [otherMapLayer, otherMapLayer2],
          activeLayerId: otherMapLayer,
          mapId: otherMap,
        },
      },
      allIds: [mapId, otherMap],
    },
    layers: {
      byId: {
        [layerId1]: {
          mapId,
          service: 'https://geoservices.knmi.nl/adagucserver?dataset=RADAR&',
          name: 'RAD_NL25_PCP_CM',
          format: 'image/png',
          style: 'knmiradar/nearest',
          id: layerId1,
          opacity: 1,
          enabled: true,
          layerType: LayerType.mapLayer,
          dimensions: [
            { name: 'time', units: 'ISO8601', currentValue: 'someTimeVal' },
            { name: 'elevation', units: 'm', currentValue: '100' },
          ],
        },
        [layerId2]: {
          mapId,
          name: 'arcGisSat',
          title: 'arcGisSat',
          type: 'twms',
          id: layerId2,
          opacity: 1,
          enabled: true,
          layerType: LayerType.baseLayer,
        },
        [layerId3]: {
          mapId,
          name: 'someOverLayer',
          title: 'someOverLayer',
          type: 'twms',
          id: layerId3,
          opacity: 1,
          enabled: true,
          layerType: LayerType.overLayer,
        },
        [otherMapLayer]: {
          mapId: otherMap,
          name: 'someLayerOnOtherMap',
          title: 'someLayerOnOtherMap',
          type: 'twms',
          id: otherMapLayer,
          opacity: 1,
          enabled: false,
          layerType: LayerType.overLayer,
          dimensions: [
            { name: 'time', units: 'ISO8601', currentValue: 'someTimeVal' },
            { name: 'elevation', units: 'm', currentValue: '100' },
          ],
        },
        [otherMapLayer2]: {
          mapId: otherMap,
          name: 'someLayerOnOtherMap',
          title: 'someLayerOnOtherMap',
          type: 'twms',
          id: otherMapLayer2,
          opacity: 1,
          enabled: true,
          layerType: LayerType.overLayer,
          dimensions: [
            { name: 'time', units: 'ISO8601', currentValue: 'someTimeVal' },
          ],
        },
        [layerId5]: {
          mapId,
          service: 'https://geoservices.knmi.nl/adagucserver?dataset=RADAR&',
          name: 'RAD_NL25_PCP_CM',
          format: 'image/png',
          style: 'knmiradar/nearest',
          id: layerId1,
          opacity: 1,
          enabled: false,
          layerType: LayerType.mapLayer,
          dimensions: [{ name: 'elevation', units: 'm', currentValue: '100' }],
        },
      },
      allIds: [
        layerId1,
        layerId2,
        layerId3,
        otherMapLayer,
        otherMapLayer2,
        layerId5,
      ],
      availableBaseLayers: { allIds: [], byId: {} },
    },
  };
  describe('getMapLayerIdsEnabled', () => {
    it('should return empty array if no store', () => {
      expect(mapSelectors.getMapLayerIdsEnabled(null!, mapId)).toEqual([]);
    });

    it('should return array with layerIds for all enabled layers for that map', () => {
      expect(
        mapSelectors.getMapLayerIdsEnabled(mockMapWithLayersStore, mapId),
      ).toEqual([layerId1]);
      expect(
        mapSelectors.getMapLayerIdsEnabled(mockMapWithLayersStore, 'otherMap'),
      ).toEqual([otherMapLayer2]);
    });
  });

  describe('getIsEnabledLayersForMapDimension', () => {
    it('should return false if no store', () => {
      expect(
        mapSelectors.getIsEnabledLayersForMapDimension(null!, mapId, 'time'),
      ).toBeFalsy();
    });

    it('should false if all layers are disabled for that dimension or if none of the enabled layers have the dimension', () => {
      expect(
        mapSelectors.getIsEnabledLayersForMapDimension(
          mockMapWithLayersStore,
          otherMap,
          'elevation',
        ),
      ).toBeFalsy();
      expect(
        mapSelectors.getIsEnabledLayersForMapDimension(
          mockMapWithLayersStore,
          otherMap,
          'randomDim',
        ),
      ).toBeFalsy();
    });

    it('should return true if one of the enabled layers for that map has that dimension', () => {
      expect(
        mapSelectors.getIsEnabledLayersForMapDimension(
          mockMapWithLayersStore,
          mapId,
          'elevation',
        ),
      ).toBeTruthy();
      expect(
        mapSelectors.getIsEnabledLayersForMapDimension(
          mockMapWithLayersStore,
          mapId,
          'time',
        ),
      ).toBeTruthy();
      expect(
        mapSelectors.getIsEnabledLayersForMapDimension(
          mockMapWithLayersStore,
          otherMap,
          'time',
        ),
      ).toBeTruthy();
    });
  });
});
