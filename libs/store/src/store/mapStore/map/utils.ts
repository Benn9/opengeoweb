/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */
import moment from 'moment';
import { TimeInterval, webmapUtils } from '@opengeoweb/webmap';
import { differenceInMinutes } from 'date-fns';
import { Bbox, Dimension } from './types';
import { Scale } from './enums';
import type { WebMapState, WebMap, SpeedFactorType } from '../types';
import type { Layer } from '../layers/types';
import {
  defaultSecondsPerPx,
  defaultAnimationDelayAtStart,
  defaultDelay,
  defaultTimeStep,
  defaultTimeSpan,
} from './constants';

export const dateFormat = 'YYYY-MM-DDTHH:mm:ss[Z]';

export interface CreateMapProps {
  id: string;
  isAnimating?: boolean;
  animationStartTime?: string;
  animationEndTime?: string;
  isAutoUpdating?: boolean;
  isEndTimeOverriding?: boolean;
  srs?: string;
  bbox?: Bbox;
  mapLayers?: string[];
  baseLayers?: string[];
  overLayers?: string[];
  featureLayers?: string[];
  dimensions?: Dimension[];
  autoTimeStepLayerId?: string;
  autoUpdateLayerId?: string;
  timeSliderSpan?: number;
  timeStep?: number;
  animationDelay?: number;
  timeSliderWidth?: number;
  timeSliderCenterTime?: number;
  timeSliderSecondsPerPx?: number;
  isTimestepAuto?: boolean;
  isTimeSpanAuto?: boolean;
  isTimeSliderHoverOn?: boolean;
  isTimeSliderVisible?: boolean;
  displayMapPin?: boolean;
  disableMapPin?: boolean;
  shouldShowZoomControls?: boolean;
}

export const createMap = ({
  id,
  isAnimating = false,
  animationStartTime = moment
    .utc(moment().unix() * 1000)
    .subtract(5, 'h')
    .format(dateFormat),
  animationEndTime = moment
    .utc(moment().unix() * 1000)
    .subtract(10, 'm')
    .format(dateFormat),
  isAutoUpdating = false,
  isEndTimeOverriding = false,
  bbox = {
    left: -19000000,
    bottom: -19000000,
    right: 19000000,
    top: 19000000,
  },
  srs = 'EPSG:3857',
  baseLayers = [],
  overLayers = [],
  mapLayers = [],
  featureLayers = [],
  dimensions = [],
  autoUpdateLayerId,
  autoTimeStepLayerId,
  timeSliderSpan = defaultTimeSpan,
  timeStep = defaultTimeStep,
  animationDelay = defaultAnimationDelayAtStart,
  timeSliderWidth = 440,
  timeSliderCenterTime = moment.utc().unix(),
  timeSliderSecondsPerPx = defaultSecondsPerPx,
  isTimestepAuto = true,
  isTimeSpanAuto = false,
  isTimeSliderHoverOn = false,
  isTimeSliderVisible = true,
  displayMapPin = false,
  disableMapPin = false,
  shouldShowZoomControls = true,
}: CreateMapProps): WebMap => ({
  id,
  isAnimating,
  animationStartTime,
  animationEndTime,
  isAutoUpdating,
  isEndTimeOverriding,
  srs,
  bbox,
  baseLayers,
  overLayers,
  mapLayers,
  featureLayers,
  dimensions,
  autoUpdateLayerId,
  autoTimeStepLayerId,
  timeSliderSpan,
  timeStep,
  animationDelay,
  timeSliderWidth,
  timeSliderCenterTime,
  timeSliderSecondsPerPx,
  isTimestepAuto,
  isTimeSpanAuto,
  isTimeSliderHoverOn,
  isTimeSliderVisible,
  displayMapPin,
  disableMapPin,
  shouldShowZoomControls,
});

export const checkValidLayersPayload = (
  layers: Layer[],
  mapId: string,
): boolean => {
  /* Check for duplicate ids */
  const layerIds: { [key: string]: boolean } = {};
  for (let i = 0; i < layers.length; i += 1) {
    if (layers[i].id) {
      /* Check if layer is already added to a different map */
      if (layers[i].mapId && mapId && layers[i].mapId !== mapId) {
        return false;
      }
      /* Check duplicate */
      if (!layerIds[layers[i].id!]) {
        layerIds[layers[i].id!] = true;
      } else {
        return false;
      }
    }
  }
  return true;
};

/**
 * This will get the map from the map draftstate.
 * If the mapId is not found, it registers one and returns it.
 * @param mapId The mapID
 * @param draft Draft map state
 */
export const getDraftMapById = (mapId: string, draft: WebMapState): WebMap => {
  const map = draft.byId[mapId];
  if (map) {
    return map;
  }
  if (!draft.allIds.includes(mapId)) {
    draft.byId[mapId] = createMap({ id: mapId } as WebMap);
    draft.allIds.push(mapId);
  }
  return draft.byId[mapId];
};

/**
 * Sets the map dimension in the state.
 * It will add dimensions to the map if they are missing.
 * If will update the existing dimensions if overwriteCurrentValue is set to true
 * @param draft The map draft state
 * @param mapId The mapId to update the dimensions for
 * @param dimensionFromAction  The dimension from the action
 * @param overwriteCurrentValue True to overwrite existing value. False to add a new dimension if one is not there yet.
 */
export const produceDraftStateSetWebMapDimension = (
  draft: WebMapState,
  mapId: string,
  dimensionFromAction: Dimension,
  overwriteCurrentValue: boolean,
): void => {
  const map = getDraftMapById(mapId, draft);
  if (dimensionFromAction) {
    if (!map.dimensions) {
      map.dimensions = [];
    }
    const { dimensions } = map;
    const mapDim = dimensions.find(
      (dim) => dim.name === dimensionFromAction.name,
    );
    if (mapDim) {
      if (overwriteCurrentValue) {
        mapDim.currentValue = dimensionFromAction.currentValue;
      }
    } else {
      dimensions.push({
        name: dimensionFromAction.name,
        currentValue: dimensionFromAction.currentValue,
      });
    }
  }
};

/**
 * Find the mapId belonging to a layerId
 * @param draft The WebMapState containing the state of all maps.
 * @param layerId The layer Id to find in the maps
 */
export const findMapIdFromLayerId = (
  draft?: WebMapState,
  layerId?: string,
): string => {
  if (!draft || !layerId) {
    return null!;
  }
  for (let i = 0; i < draft.allIds.length; i += 1) {
    const mapId = draft.allIds[i];
    const layerIds = draft.byId[mapId].mapLayers;
    for (let l = 0; l < layerIds.length; l += 1) {
      const layerIdFromMap = layerIds[l];
      if (layerIdFromMap === layerId) {
        return mapId;
      }
    }
  }
  return null!;
};

/*
    When a layer dimension is changed, it can affect the map dimension if the layer dimension is linked with the map.
    We need to find out from the layerId to which map it is coupled, and then adjust the map dimension
  */

export const produceDraftStateSetMapDimensionFromLayerChangeDimension = (
  draft: WebMapState,
  layerId: string,
  dimension: Dimension,
): void => {
  const wmjsDimension = webmapUtils.getWMJSDimensionForLayerAndDimension(
    layerId,
    dimension.name!,
  );
  if (!wmjsDimension) {
    return;
  }
  /* If the layer dimension is not linked with the map, we should not update the map dimension */
  if (!wmjsDimension.linked) {
    return;
  }
  const mapId = findMapIdFromLayerId(draft, layerId);
  if (!mapId) {
    return;
  }

  produceDraftStateSetWebMapDimension(draft, mapId, dimension, true);
};

/**
 * Returns array with new order of swapped elements
 * @param array Array with ids
 * @param oldIndex Old index of element in array
 * @param newIndex New index of element in array
 */
export function moveArrayElements(
  array: string[],
  oldIndex: number,
  newIndex: number,
): string[] {
  const newArray = [...array];
  const indexNew = newIndex >= newArray.length ? newArray.length - 1 : newIndex;
  newArray.splice(indexNew, 0, newArray.splice(oldIndex, 1)[0]);
  return newArray;
}

export const getTimeStepFromDataInterval = (
  timeInterval: TimeInterval,
): number => {
  switch (timeInterval.isRegularInterval) {
    case false:
      switch (timeInterval.year) {
        case 0: // month
          return 30 * 24 * 60;
        default:
          return timeInterval.year * 365 * 24 * 60;
      }
    case true:
      if (timeInterval.day !== 0) {
        return timeInterval.day * 24 * 60;
      }
      if (timeInterval.hour !== 0) {
        return timeInterval.hour * 60;
      }
      if (timeInterval.minute !== 0) {
        return timeInterval.minute;
      }
      if (timeInterval.second !== 0) {
        return timeInterval.second / 60.0;
      }
      return defaultTimeStep;
    default:
      return defaultTimeStep;
  }
};

export const getActiveLayerTimeStep = (
  timeDimension: Dimension | undefined,
): number | undefined =>
  timeDimension?.timeInterval
    ? getTimeStepFromDataInterval(timeDimension.timeInterval)
    : undefined;

export const getSpeedFactor = (speedDelay: number): SpeedFactorType => {
  return (defaultDelay / speedDelay) as SpeedFactorType;
};

export const getAnimationDuration = (
  animationEndTime: string | undefined,
  animationStartTime: string | undefined,
): number =>
  animationEndTime && animationStartTime
    ? differenceInMinutes(
        new Date(animationEndTime),
        new Date(animationStartTime),
      )
    : 0;

/**
 * Returns speed delay for given speedFactor. For options, see defined above in "speedFactors"
 */

export const getSpeedDelay = (speedFactor: SpeedFactorType): number => {
  return defaultDelay / speedFactor;
};

export const roundWithTimeStep = (
  unixTime: number,
  timeStep: number,
  type?: string,
): number => {
  const adjustedTimeStep = timeStep * 60;
  if (!type || type === 'round') {
    return Math.round(unixTime / adjustedTimeStep) * adjustedTimeStep;
  }
  if (type === 'floor') {
    return Math.floor(unixTime / adjustedTimeStep) * adjustedTimeStep;
  }
  if (type === 'ceil') {
    return Math.ceil(unixTime / adjustedTimeStep) * adjustedTimeStep;
  }
  return undefined!;
};
/**
 * In this Map collection all fundamental scales
 * (Scale.Minutes5 ... Scale.Year) are mapped with their corresponding
 * secondsPerPx numbers.
 * @returns a Map including information explained above
 */

export const secondsPerPxToScale: Map<number, Scale> = new Map([
  [2.5, Scale.Minutes5],
  [30, Scale.Hour],
  [3 * 30, Scale.Hours3],
  [6 * 30, Scale.Hours6],
  [24 * 30, Scale.Day],
  [7 * 24 * 30, Scale.Week],
  [30 * 24 * 30, Scale.Month],
  [365 * 24 * 30, Scale.Year],
]);

/**
 * Creates a reverse mapping of scales to their corresponding secondsPerPx
 * values to make it easier in some parts GeoWeb to fetch a secondsPerpx for
 * a certain fundamental scale (Scale.Minutes5 ... Scale.year)
 * @returns an Object, where names are fundamental scale values (0 - 7) and
 * values are their corresponding secondsPerPx numbers.
 */

export const scaleToSecondsPerPx: {
  [x in Scale]: number;
} = Object.fromEntries(
  Array.from(secondsPerPxToScale.entries()).map(([k, v]) => [v, k]),
) as { [x in Scale]: number };
