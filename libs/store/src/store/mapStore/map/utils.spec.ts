/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */

import { DateInterval } from '@opengeoweb/webmap';
import {
  getActiveLayerTimeStep,
  getAnimationDuration,
  getSpeedDelay,
  getSpeedFactor,
  getTimeStepFromDataInterval,
  moveArrayElements,
} from './utils';
import { defaultDelay } from './constants';

describe('store/mapStore/map/utils', () => {
  describe('moveArrayElements', () => {
    it('should move array elements', () => {
      const testArray = ['test-1', 'test-2', 'test-3'];
      expect(moveArrayElements(testArray, 0, 1)).toEqual([
        'test-2',
        'test-1',
        'test-3',
      ]);
      expect(moveArrayElements(testArray, 0, 2)).toEqual([
        'test-2',
        'test-3',
        'test-1',
      ]);
      expect(moveArrayElements(testArray, 1, 0)).toEqual([
        'test-2',
        'test-1',
        'test-3',
      ]);
      expect(moveArrayElements(testArray, 1, 2)).toEqual([
        'test-1',
        'test-3',
        'test-2',
      ]);
      expect(moveArrayElements(testArray, 2, 0)).toEqual([
        'test-3',
        'test-1',
        'test-2',
      ]);
      expect(moveArrayElements(testArray, 2, 1)).toEqual([
        'test-1',
        'test-3',
        'test-2',
      ]);
    });
  });

  const ti1 = new DateInterval('1', '0', '0', '0', '0', '0');
  const ti2 = new DateInterval('0', '1', '0', '0', '0', '0');
  const ti3 = new DateInterval('0', '0', '1', '0', '0', '0');
  const ti4 = new DateInterval('0', '0', '0', '1', '0', '0');
  const ti5 = new DateInterval('0', '0', '0', '0', '1', '0');
  const ti6 = new DateInterval('0', '0', '0', '0', '0', '1');
  describe.each([
    [ti1, 365 * 24 * 60],
    [ti2, 30 * 24 * 60],
    [ti3, 24 * 60],
    [ti4, 60],
    [ti5, 1],
    [ti6, 1 / 60],
  ])('getTimeStepFromDataInterval', (interval, expected) => {
    it(`should return ${expected} for ${JSON.stringify(interval)}`, () => {
      expect(getTimeStepFromDataInterval(interval)).toBeCloseTo(expected);
    });
  });

  describe('getActiveLayerTimeStep', () => {
    it('should return truthy value for time dimension with time interval', () => {
      expect(
        getActiveLayerTimeStep({ currentValue: '', timeInterval: ti1 }),
      ).toBeTruthy();
    });
    it('should return falsy value for time dimension without time interval', () => {
      expect(getActiveLayerTimeStep({ currentValue: '' })).toBeFalsy();
    });
  });

  describe('getSpeedFactor', () => {
    it('should return correct speedFactor for the passed delay', () => {
      expect(getSpeedFactor(62.5)).toEqual(16);
      expect(getSpeedFactor(125)).toEqual(8);
      expect(getSpeedFactor(250)).toEqual(4);
      expect(getSpeedFactor(500)).toEqual(2);
      expect(getSpeedFactor(1000)).toEqual(1);
      expect(getSpeedFactor(2000)).toEqual(0.5);
      expect(getSpeedFactor(5000)).toEqual(0.2);
      expect(getSpeedFactor(10000)).toEqual(0.1);
    });
  });

  describe('getSpeedDelay', () => {
    it('should return correct delay for the passed speedFactor', () => {
      expect(getSpeedDelay(0.1)).toEqual(defaultDelay / 0.1);
      expect(getSpeedDelay(0.2)).toEqual(defaultDelay / 0.2);
      expect(getSpeedDelay(0.5)).toEqual(defaultDelay / 0.5);
      expect(getSpeedDelay(1)).toEqual(defaultDelay / 1);
      expect(getSpeedDelay(2)).toEqual(defaultDelay / 2);
      expect(getSpeedDelay(4)).toEqual(defaultDelay / 4);
      expect(getSpeedDelay(8)).toEqual(defaultDelay / 8);
      expect(getSpeedDelay(16)).toEqual(defaultDelay / 16);
    });
  });

  describe('getAnimationDuration', () => {
    it('should return correct duration for passed start and end times', () => {
      expect(
        getAnimationDuration('2023-01-01T00:30:00Z', '2023-01-01T00:00:00Z'),
      ).toEqual(30);
      expect(
        getAnimationDuration('2023-01-01T00:30:00Z', '2023-01-01T00:00:01Z'),
      ).toEqual(29);
      expect(getAnimationDuration(undefined, undefined)).toEqual(0);
    });
  });
});
