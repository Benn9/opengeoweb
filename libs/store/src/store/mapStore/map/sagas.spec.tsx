/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import { takeLatest, takeEvery, put, select } from 'redux-saga/effects';
import moment from 'moment';
import { act, render } from '@testing-library/react';
import { Dimension, LayerType, WMJSMap, webmapUtils } from '@opengeoweb/webmap';
import { Store } from '@reduxjs/toolkit';
import { add, sub } from 'date-fns';
import React from 'react';
import produce from 'immer';
import { mapActions, defaultLayers } from '.';
import { layerActions, layerSelectors } from '../layers';
import {
  stopAnimationSaga,
  startAnimationSaga,
  rootSaga,
  generateTimeList,
  deleteLayerSaga,
  setLayerDimensionsSaga,
  toggleAutoUpdateSaga,
  updateAnimation,
  setMapPresetSaga,
  handleBaseLayersSaga,
  unregisterMapSaga,
} from './sagas';
import { SetMapPresetPayload, TimeListType, WebMap } from './types';
import { Layer } from '../layers/types';
import { defaultAnimationDelayAtStart, defaultTimeStep } from './constants';
import { uiActions } from '../../ui';
import { getSpeedDelay } from './utils';
import {
  CoreAppStore,
  ThemeStoreProvider,
  createStore,
  genericActions,
} from '../..';

describe('store/mapStore/map/sagas', () => {
  it('should catch rootSaga actions and fire corresponding sagas', () => {
    const generator = rootSaga();
    expect(generator.next().value).toEqual(
      takeLatest(mapActions.mapStopAnimation.type, stopAnimationSaga),
    );
    expect(generator.next().value).toEqual(
      takeLatest(mapActions.mapStartAnimation.type, startAnimationSaga),
    );
    expect(generator.next().value).toEqual(
      takeLatest(layerActions.layerDelete.type, deleteLayerSaga),
    );
    expect(generator.next().value).toEqual(
      takeLatest(
        layerActions.onUpdateLayerInformation.type,
        setLayerDimensionsSaga,
      ),
    );
    expect(generator.next().value).toEqual(
      takeLatest(mapActions.toggleAutoUpdate.type, toggleAutoUpdateSaga),
    );
    expect(generator.next().value).toEqual(
      takeEvery(mapActions.setMapPreset.type, setMapPresetSaga),
    );

    expect(generator.next().value).toEqual(
      takeEvery(mapActions.unregisterMap.type, unregisterMapSaga),
    );
    expect(generator.next().done).toBeTruthy();
  });

  // new Date().toISOString() returns a iso8601 string with milliseconds: `2000-01-23T10:00:00.000Z`
  // but in geoweb we save iso8601 without milliseconds in redux: `2000-01-23T10:00:00Z`
  const removeMilliseconds = (iso8601: string): string =>
    iso8601.replace(/\.[0-9]{3}/, '');

  const date = `2000-01-23T`;
  // setup data for integration tests
  const mockNowTimeIso = `${date}10:00:00Z`;
  const mockNowTime = new Date(mockNowTimeIso);

  // layer current time and layer max time is set to be now time
  // we want to test that the layer current time is updated
  // to the new max time when new max time arrives
  const initialLayerCurrentTime = mockNowTimeIso;
  const initialLayerMaxTime = mockNowTimeIso;

  jest.useFakeTimers();
  jest.setSystemTime(mockNowTime);

  afterAll(() => {
    jest.useRealTimers();
  });

  const initialTimeDimension: Dimension = {
    name: 'time',
    units: 'ISO8601',
    currentValue: initialLayerCurrentTime,
    maxValue: initialLayerMaxTime,
    minValue: sub(mockNowTime, { days: 1 }).toISOString(),
    validSyncSelection: true,
  };

  const mapId = 'mapId';
  const layerId = 'layerId';

  const layer: Layer = {
    id: layerId,
    service: 'https://testservice',
    name: 'layerName',
    title: 'layerTitle',
    enabled: true,
    layerType: LayerType.mapLayer,
    dimensions: [initialTimeDimension],
  };
  const layerInRedux = {
    ...layer,
    acceptanceTimeInMinutes: 60,
    mapId,
    opacity: 1,
    status: 'default',
    values: undefined,
    useLatestReferenceTime: true,
  };
  const timeSyncGroupId = 'timeslider';
  const origin = 'origin';

  const setupStore = (
    isMapSyncedWithLayer = true,
    layerToStore = layer,
  ): Store<CoreAppStore> => {
    const store = createStore();

    render(<ThemeStoreProvider store={store}>hi</ThemeStoreProvider>);

    act(() => {
      store.dispatch(mapActions.registerMap({ mapId }));

      if (isMapSyncedWithLayer) {
        // setup sync group so map time changes when layer time changes
        store.dispatch(
          genericActions.syncGroupAddGroup({
            groupId: timeSyncGroupId,
            title: 'Timeslider',
            type: 'SYNCGROUPS_TYPE_SETTIME',
          }),
        );
        store.dispatch(
          genericActions.syncGroupAddTarget({
            groupId: timeSyncGroupId,
            targetId: mapId,
          }),
        );
      }

      store.dispatch(
        layerActions.addLayer({
          layerId: layerToStore.id!,
          mapId,
          layer: layerToStore,
          origin,
        }),
      );

      // set auto update to true so that new max time moves animation start and end time
      store.dispatch(
        mapActions.toggleAutoUpdate({ mapId, shouldAutoUpdate: true }),
      );
    });
    return store;
  };

  const mapValuesThatDontChangeDuringTesting = {
    id: mapId,
    featureLayers: [],
    disableMapPin: false,
    isEndTimeOverriding: false,
    isTimeSliderHoverOn: false,
    isTimeSpanAuto: false,
    srs: 'EPSG:3857',
    isTimeSliderVisible: true,
    timeSliderCenterTime: expect.any(Number),
    timeSliderSecondsPerPx: 80,
    timeSliderSpan: 86400,
    timeSliderWidth: 440,
    bbox: expect.any(Object),
    shouldShowZoomControls: true,
    displayMapPin: false,
    animationDelay: defaultAnimationDelayAtStart,
    isTimestepAuto: true,
  };

  // animation start time is by default 04:50 or 5 hours behind animation end time
  const defaultTimeBetweenStartAndEnd = {
    hours: 4,
    minutes: 50,
  };
  const animationStartTimeDefault = sub(
    new Date(initialLayerMaxTime),
    defaultTimeBetweenStartAndEnd,
  );
  const mapAfterSetupStore: WebMap = {
    ...mapValuesThatDontChangeDuringTesting,

    mapLayers: [layerId],
    baseLayers: [],
    overLayers: [],

    autoTimeStepLayerId: layerId,
    autoUpdateLayerId: layerId,

    dimensions: [{ name: 'time', currentValue: initialLayerMaxTime }],

    timeStep: 60,

    isAutoUpdating: true,

    isAnimating: false,

    animationStartTime: removeMilliseconds(
      animationStartTimeDefault.toISOString(),
    ),

    animationEndTime: initialLayerMaxTime,
  };

  // mock wm map to make testing easier
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  const animationDrawCall = jest.fn() as any;
  const mockWmMap = {
    getListener: () => ({
      suspendEvents: (): void => {},
      resumeEvents: (): void => {},
    }),
    stopAnimating: () => {},
    draw: animationDrawCall,
    initialAnimationStep: 0,
    isAnimating: true,
  } as WMJSMap;
  const start = mockNowTimeIso;
  const end = add(mockNowTime, { minutes: 15 }).toISOString();
  const minutesBetweenEachAnimationStep = 1;
  describe('startAnimationSaga', () => {
    beforeEach(() => {
      animationDrawCall.mockReset();
      jest.spyOn(webmapUtils, 'getWMJSMapById').mockReturnValue(mockWmMap);
    });
    it('should start animation with timelist and stop animation', () => {
      const timeList = [
        { name: 'time', value: `${date}08:45:00.000Z` },
        { name: 'time', value: `${date}08:50:00.000Z` },
        { name: 'time', value: `${date}08:55:00.000Z` },
      ] as TimeListType[];

      const store = setupStore();
      act(() => {
        store.dispatch(
          mapActions.mapStartAnimation({
            mapId,
            timeList,
          }),
        );
      });
      expect(animationDrawCall).toBeCalledWith(timeList);
      expect(store.getState().webmap!.byId[mapId].isAnimating).toEqual(true);

      // test that stop animation works
      act(() => {
        store.dispatch(
          mapActions.mapStopAnimation({
            mapId,
          }),
        );
      });
      expect(store.getState().webmap!.byId[mapId].isAnimating).toEqual(false);
    });

    it('should start animation and create timeList', () => {
      const store = setupStore();
      act(() => {
        store.dispatch(
          mapActions.mapStartAnimation({
            mapId,
            start,
            end,
            interval: minutesBetweenEachAnimationStep,
          }),
        );
      });

      expect(animationDrawCall).toHaveBeenCalledWith(
        generateTimeList(
          moment.utc(start),
          moment.utc(end),
          minutesBetweenEachAnimationStep,
        ),
      );
    });

    it('should start animation from initial time', () => {
      const initialTime = add(new Date(start), { minutes: 9 }).toISOString();

      const store = setupStore();
      act(() => {
        store.dispatch(
          mapActions.mapStartAnimation({
            mapId,
            start,
            initialTime,
            end,
            interval: minutesBetweenEachAnimationStep,
          }),
        );
      });

      expect(animationDrawCall).toHaveBeenCalledWith(
        generateTimeList(
          moment.utc(start),
          moment.utc(end),
          minutesBetweenEachAnimationStep,
        ),
      );
      expect(mockWmMap.initialAnimationStep).toEqual(9);
    });
  });

  describe('updateAnimation', () => {
    const mapId = 'map-test';
    const maxValue = '2021-01-01T12:00:00Z';
    const animationStart = moment.utc('2021-01-01T10:00:00Z');
    const animationEnd = moment.utc('2021-01-01T11:00:00Z');

    it('should set animation end to max value', () => {
      const generator = updateAnimation(mapId, maxValue);
      generator.next();
      generator.next(animationStart);

      const setAnimationendAction = generator.next(animationEnd).value;
      expect(
        setAnimationendAction.payload.action.payload.animationEndTime,
      ).toEqual(maxValue);
    });

    it('should keep animation length fixed', () => {
      const generator = updateAnimation(mapId, maxValue);
      generator.next();
      generator.next(animationStart);
      generator.next(animationEnd);

      const length = animationEnd.unix() - animationStart.unix();
      const setAnimationStartAction = generator.next().value;
      const newAnimationStart =
        setAnimationStartAction.payload.action.payload.animationStartTime;
      expect(
        moment.utc(maxValue).unix() - moment.utc(newAnimationStart).unix(),
      ).toEqual(length);
    });

    it('should restart the animation', () => {
      const generator = updateAnimation(mapId, maxValue);
      generator.next();
      generator.next(animationStart);
      generator.next(animationEnd);
      generator.next();
      generator.next();
      generator.next(true);
      expect(generator.next(20).value.payload.action.type).toEqual(
        put(mapActions.mapStartAnimation({ mapId })).payload.action.type,
      );
    });
  });
  describe('deleteLayerSaga', () => {
    it('should stop animation after deleting the last layer', () => {
      const store = setupStore();

      // start animation
      act(() => {
        store.dispatch(
          mapActions.mapStartAnimation({
            mapId,
            start,
            end,
            interval: minutesBetweenEachAnimationStep,
          }),
        );
      });
      expect(store.getState().webmap!.byId[mapId].isAnimating).toEqual(true);

      // deleting layer should stop animation
      act(() => {
        store.dispatch(
          layerActions.layerDelete({
            mapId,
            layerId,
            layerIndex: 0,
          }),
        );
      });

      expect(store.getState().webmap!.byId[mapId].isAnimating).toEqual(false);
      expect(store.getState().layers!.allIds).toEqual([]);
    });

    it('should not stop animation after deleting when there are still layers', () => {
      const store = setupStore();

      // add second layer and start animation
      const layer2id = 'layer2id';
      act(() => {
        const layer2 = {
          ...layer,
          name: 'layer2name',
          id: layer2id,
        };
        store.dispatch(
          layerActions.addLayer({
            layerId: layer2id,
            mapId,
            layer: layer2,
            origin,
          }),
        );

        store.dispatch(
          mapActions.mapStartAnimation({
            mapId,
            start,
            end,
            interval: minutesBetweenEachAnimationStep,
          }),
        );
      });

      // deleting layer should not stop animation
      act(() => {
        store.dispatch(
          layerActions.layerDelete({
            mapId,
            layerId,
            layerIndex: 0,
          }),
        );
      });

      expect(store.getState().webmap!.byId[mapId].isAnimating).toEqual(true);
      expect(store.getState().layers!.allIds).toEqual([layer2id]);
    });
  });

  describe('toggleAutoUpdateSaga', () => {
    it('should update active layer to latest timedimension', () => {
      const layerWithCurrentTimeBeforeMaxTime = produce(layer, (draft) => {
        draft.dimensions![0].currentValue = sub(new Date(initialLayerMaxTime), {
          hours: 1,
        }).toISOString();
      });
      const store = setupStore(true, layerWithCurrentTimeBeforeMaxTime);
      expect(store.getState().webmap!.byId[mapId]).toEqual(mapAfterSetupStore);
      expect(store.getState().layers!.byId[layerId]).toEqual(layerInRedux);
    });

    it('should not dispatch setTime or toggleAutoUpdates if no synced maps', () => {
      const store = setupStore(false);
      expect(store.getState().webmap!.byId[mapId]).toEqual({
        ...mapAfterSetupStore,
        dimensions: [],
      });
    });
  });

  describe('setLayerDimensionsSaga', () => {
    const newMaxValueHoursDifference = 3;
    const newMaxValue = removeMilliseconds(
      add(new Date(initialTimeDimension.maxValue!), {
        hours: newMaxValueHoursDifference,
      }).toISOString(),
    );
    const timeDimensionWithNewMaxValue = {
      ...initialTimeDimension,
      maxValue: newMaxValue,
    };

    it('should update the active layer and animation to new time dimension if there is new data and autoupdating is true', () => {
      const store = setupStore();

      // action with same time as before shouldn't change state
      act(() => {
        store.dispatch(
          layerActions.onUpdateLayerInformation({
            origin,
            layerDimensions: {
              layerId,
              dimensions: [initialTimeDimension],
              origin,
            },
          }),
        );
      });
      expect(store.getState().webmap!.byId[mapId]).toEqual(mapAfterSetupStore);
      expect(store.getState().layers!.byId[layerId]).toEqual(layerInRedux);

      // update layer with new max time should change map and layer time
      act(() => {
        store.dispatch(
          layerActions.onUpdateLayerInformation({
            origin,
            layerDimensions: {
              layerId,
              dimensions: [timeDimensionWithNewMaxValue],
              origin,
            },
          }),
        );
      });

      expect(store.getState().webmap!.byId[mapId]).toEqual({
        ...mapAfterSetupStore,
        dimensions: [
          {
            name: 'time',
            currentValue: timeDimensionWithNewMaxValue.maxValue!,
          },
        ],

        animationEndTime: timeDimensionWithNewMaxValue.maxValue!,

        animationStartTime: removeMilliseconds(
          add(animationStartTimeDefault, {
            hours: newMaxValueHoursDifference,
          }).toISOString(),
        ),
      });
      expect(store.getState().layers!.byId[layerId]).toEqual({
        ...layerInRedux,
        dimensions: [
          {
            ...initialTimeDimension,
            currentValue: timeDimensionWithNewMaxValue.maxValue!,
            maxValue: timeDimensionWithNewMaxValue.maxValue!,
            values: undefined,
          },
        ],
      });
    });

    it('should restart the animation if animation was running and there is new time dimension data', () => {
      const store = setupStore();

      // turn animation on
      act(() => {
        store.dispatch(mapActions.mapStartAnimation({ mapId }));
      });
      expect(store.getState().webmap!.byId[mapId]).toEqual({
        ...mapAfterSetupStore,
        isAnimating: true,
      });

      // new max value changes animation start and end time
      // and updates max value of layer
      act(() => {
        store.dispatch(
          layerActions.onUpdateLayerInformation({
            origin,
            layerDimensions: {
              layerId,
              dimensions: [timeDimensionWithNewMaxValue],
              origin,
            },
          }),
        );
      });

      expect(store.getState().webmap!.byId[mapId]).toEqual({
        ...mapAfterSetupStore,
        isAnimating: true,
        dimensions: [{ name: 'time', currentValue: newMaxValue }],
        animationEndTime: newMaxValue,

        animationStartTime: removeMilliseconds(
          sub(
            new Date(newMaxValue),
            defaultTimeBetweenStartAndEnd,
          ).toISOString(),
        ),
      });

      expect(
        store.getState().layers!.byId[layerId].dimensions![0].maxValue,
      ).toEqual(newMaxValue);
    });

    it('should start the animation if animation is set in redux store but not running yet on the map even if already have the latest dimension', () => {
      jest.spyOn(webmapUtils, 'getWMJSMapById').mockReturnValue({
        ...mockWmMap,
        isAnimating: false,
      } as WMJSMap);

      const store = setupStore();

      // start animation but wmMap has isAnimating false
      act(() => {
        store.dispatch(mapActions.mapStartAnimation({ mapId }));
      });

      animationDrawCall.mockClear();

      // dispatch same time as is in state
      // this will start animation in wm map
      act(() => {
        store.dispatch(
          layerActions.onUpdateLayerInformation({
            origin,
            layerDimensions: {
              layerId,
              dimensions: [initialTimeDimension],
              origin,
            },
          }),
        );
      });
      expect(animationDrawCall).toBeCalledTimes(1);
    });

    it('should only update the active layer', () => {
      const store = setupStore();

      // add second layer which will not get updated
      const layer2id = 'layer2id';
      act(() => {
        const layer2 = {
          ...layer,
          name: 'layer2name',
          id: layer2id,
        };
        store.dispatch(
          layerActions.addLayer({
            layerId: layer2id!,
            mapId,
            layer: layer2,
            origin,
          }),
        );
      });

      // new max value will only be set for first layer
      act(() => {
        store.dispatch(
          layerActions.onUpdateLayerInformation({
            origin,
            layerDimensions: {
              layerId,
              dimensions: [timeDimensionWithNewMaxValue],
              origin,
            },
          }),
        );
      });

      expect(
        store.getState().layers!.byId[layerId].dimensions![0].maxValue,
      ).toEqual(timeDimensionWithNewMaxValue.maxValue);
      expect(
        store.getState().layers!.byId[layer2id].dimensions![0].maxValue,
      ).toEqual(initialTimeDimension.maxValue);
    });

    it('should not update the active layer and animation to new time dimension if there is new data but autoupdating is false', () => {
      const store = setupStore();

      // turn auto update off so new max value will not change map time
      act(() => {
        store.dispatch(
          mapActions.toggleAutoUpdate({ mapId, shouldAutoUpdate: false }),
        );
      });

      // dispatching time dimension with new max value
      act(() => {
        store.dispatch(
          layerActions.onUpdateLayerInformation({
            origin,
            layerDimensions: {
              layerId,
              dimensions: [timeDimensionWithNewMaxValue],
              origin,
            },
          }),
        );
      });
      // should not change map animation start and end
      expect(store.getState().webmap!.byId[mapId]).toEqual({
        ...mapAfterSetupStore,
        isAutoUpdating: false,
      });
    });

    it('should auto update the active layer for multiple maps if there is new data', () => {
      const mapId2 = 'mapId2';
      const store = setupStore();

      // add a second map and add it to the time sync group
      act(() => {
        store.dispatch(mapActions.registerMap({ mapId: mapId2 }));

        store.dispatch(
          genericActions.syncGroupAddTarget({
            groupId: timeSyncGroupId,
            targetId: mapId2,
          }),
        );
      });

      // new time should be set to second map also
      act(() => {
        store.dispatch(
          layerActions.onUpdateLayerInformation({
            origin,
            layerDimensions: {
              layerId,
              dimensions: [timeDimensionWithNewMaxValue],
              origin,
            },
          }),
        );
      });

      // both maps should have new time
      const maps = store.getState().webmap!.byId;
      expect(maps[mapId].dimensions![0].currentValue).toEqual(
        timeDimensionWithNewMaxValue.maxValue,
      );
      expect(maps[mapId2].dimensions![0].currentValue).toEqual(
        timeDimensionWithNewMaxValue.maxValue,
      );
    });
  });

  describe('handleBaseLayersSaga', () => {
    it('should set baseLayers', () => {
      const mapId = 'test-1';
      const baseLayers = [defaultLayers.baseLayerGrey];
      const generator = handleBaseLayersSaga(mapId, baseLayers);

      expect(generator.next().value).toEqual(
        select(layerSelectors.getAvailableBaseLayersForMap, mapId),
      );

      expect(generator.next([]).value).toEqual(
        put(
          layerActions.setBaseLayers({
            mapId,
            layers: [
              { ...baseLayers[0], id: expect.stringContaining('layerid_') },
            ],
          }),
        ),
      );

      expect(generator.next().done).toBeTruthy();
    });

    it('should set baseLayers including overLayers', () => {
      const mapId = 'test-1';
      const baseLayers = [
        defaultLayers.baseLayerGrey,
        { id: 'overlayer', layerType: 'overLayer' as LayerType },
        { id: 'overlayer-2', layerType: 'overLayer' as LayerType },
      ];
      const generator = handleBaseLayersSaga(mapId, baseLayers);

      expect(generator.next().value).toEqual(
        select(layerSelectors.getAvailableBaseLayersForMap, mapId),
      );

      expect(generator.next([]).value).toEqual(
        put(
          layerActions.setBaseLayers({
            mapId,
            layers: baseLayers.map((layer) => ({
              ...layer,
              id: expect.anything(),
            })),
          }),
        ),
      );

      expect(generator.next().done).toBeTruthy();
    });

    it('should set a baseLayer id if the name exist in the available baselayer list', () => {
      const mapId = 'test-1';
      const testExistingId = 'test-id-2';
      const baseLayers = [
        { ...defaultLayers.baseLayerGrey, name: 'OpenStreetMap number 2' },
      ];
      const generator = handleBaseLayersSaga(mapId, baseLayers);

      expect(generator.next().value).toEqual(
        select(layerSelectors.getAvailableBaseLayersForMap, mapId),
      );

      expect(
        generator.next([
          {
            id: 'test-id-1',
            name: 'OpenStreetMap',
          },
          {
            id: testExistingId,
            name: 'OpenStreetMap number 2',
          },
        ]).value,
      ).toEqual(
        put(
          layerActions.setBaseLayers({
            mapId,
            layers: [{ ...baseLayers[0], id: testExistingId }],
          }),
        ),
      );

      expect(generator.next().done).toBeTruthy();
    });

    it('should add a baseLayer to the available baselayer list if it does not exist in the existing list', () => {
      const mapId = 'test-1';

      const baseLayers = [
        {
          ...defaultLayers.baseLayerGrey,
          name: 'New one that does not exist',
        },
      ];
      const generator = handleBaseLayersSaga(mapId, baseLayers);

      expect(generator.next().value).toEqual(
        select(layerSelectors.getAvailableBaseLayersForMap, mapId),
      );

      expect(
        generator.next([
          {
            id: 'test-id-1',
            name: 'OpenStreetMap',
          },
          {
            id: 'test-id-2',
            name: 'OpenStreetMap number 2',
          },
        ]).value,
      ).toEqual(
        put(
          layerActions.addAvailableBaseLayers({
            layers: [
              {
                ...baseLayers[0],
                mapId,
                id: expect.stringContaining('layerid'),
              },
            ],
          }),
        ),
      );

      expect(generator.next().value).toEqual(
        put(
          layerActions.setBaseLayers({
            mapId,
            layers: [
              { ...baseLayers[0], id: expect.stringContaining('layerid') },
            ],
          }),
        ),
      );

      expect(generator.next().done).toBeTruthy();
    });
  });

  describe('setMapPresetSaga', () => {
    const legendId = 'legend';

    // mock generating of layer ids
    // so that we can test that they are mapped correctly to the layers
    const generatedLayerId0 = 'layer0';
    const generatedLayerId1 = 'layer1';
    const generatedLayerId2 = 'layer2';
    const generatedLayerId3 = 'layer3';
    const generatedLayerId4 = 'layer4';

    beforeEach(() => {
      jest.resetAllMocks();

      jest
        .spyOn(webmapUtils, 'generateLayerId')
        .mockReturnValueOnce(generatedLayerId0)
        .mockReturnValueOnce(generatedLayerId1)
        .mockReturnValueOnce(generatedLayerId2)
        .mockReturnValueOnce(generatedLayerId3)
        .mockReturnValueOnce(generatedLayerId4);
    });

    const setupStoreAndSetMapPreset = (
      mapPreset: SetMapPresetPayload,
    ): CoreAppStore => {
      const store = createStore();
      render(<ThemeStoreProvider store={store}>hi</ThemeStoreProvider>);

      act(() => {
        store.dispatch(mapActions.registerMap({ mapId }));
        store.dispatch(uiActions.registerDialog({ mapId, type: legendId }));

        store.dispatch(mapActions.setMapPreset(mapPreset));
      });

      return store.getState();
    };

    const defaultAnimationEndTime = sub(mockNowTime, { minutes: 10 });

    const mapAfterSetupIfMapPresetIsEmpty: WebMap = {
      ...mapValuesThatDontChangeDuringTesting,

      mapLayers: [],
      baseLayers: [generatedLayerId2],
      overLayers: [generatedLayerId1],

      autoTimeStepLayerId: undefined,
      autoUpdateLayerId: undefined,

      dimensions: [],

      timeStep: undefined,

      legendId,

      isAutoUpdating: false,

      isAnimating: false,

      animationStartTime: removeMilliseconds(
        sub(mockNowTime, { hours: 5 }).toISOString(),
      ),

      animationEndTime: removeMilliseconds(
        defaultAnimationEndTime.toISOString(),
      ),
      dockedLayerManagerSize: '',
    };

    it('should use default values if no value is provided in mapPreset', () => {
      const mapPreset: SetMapPresetPayload = {
        mapId,
        initialProps: {
          mapPreset: {},
        },
      };

      const state = setupStoreAndSetMapPreset(mapPreset);

      expect(state.webmap!.byId[mapId]).toEqual(
        mapAfterSetupIfMapPresetIsEmpty,
      );

      expect(state.ui!.dialogs[legendId]!.isOpen).toEqual(false);
    });
    it('should add given layers and use default base and overlayer', async () => {
      const layer0name = 'layer0name';
      const layer1name = 'layer1name';
      const mapPreset = {
        mapId,
        initialProps: {
          mapPreset: {
            layers: [
              {
                // the layer id will be replaced with another id.
                // this is to prevent adding two layers that both have the same id.
                id: 'layer0idBefore',
                layerType: LayerType.mapLayer,
                name: layer0name,
              },
              {
                id: 'layer1idBefore',
                layerType: LayerType.mapLayer,
                name: layer1name,
              },
            ],
          },
        },
      };

      const state = setupStoreAndSetMapPreset(mapPreset);

      expect(state.webmap!.byId[mapId]).toEqual({
        ...mapAfterSetupIfMapPresetIsEmpty,

        // if auto timestep or auto update id is not provided in mapPreset
        // it defaults to using the first layer in the array
        autoTimeStepLayerId: generatedLayerId0,
        autoUpdateLayerId: generatedLayerId0,
        mapLayers: [generatedLayerId0, generatedLayerId1],
        overLayers: [generatedLayerId3],
        baseLayers: [generatedLayerId4],
      });

      expect(state.layers!.allIds).toHaveLength(4);
      const layers = state.layers!.byId;
      // check that new layer ids are mapped to correct layer names
      expect(layers[generatedLayerId0].name).toEqual(layer0name);
      expect(layers[generatedLayerId1].name).toEqual(layer1name);
    });

    it('should set auto timestep and auto update layer id when provided in mapPreset', () => {
      const layer1idBefore = 'layer1idBefore';
      const layer1name = 'layer1name';
      const layer0name = 'layer0name';
      const mapPreset: SetMapPresetPayload = {
        mapId,
        initialProps: {
          mapPreset: {
            autoTimeStepLayerId: layer1idBefore,
            autoUpdateLayerId: layer1idBefore,
            layers: [
              {
                id: 'layer0idBefore',
                layerType: LayerType.mapLayer,
                name: layer0name,
              },
              {
                id: layer1idBefore,
                layerType: LayerType.mapLayer,
                name: layer1name,
              },
            ],
          },
        },
      };

      const state = setupStoreAndSetMapPreset(mapPreset);

      expect(state.webmap!.byId[mapId]).toEqual({
        ...mapAfterSetupIfMapPresetIsEmpty,

        autoTimeStepLayerId: generatedLayerId0,
        autoUpdateLayerId: generatedLayerId0,
        mapLayers: [generatedLayerId1, generatedLayerId0],
        overLayers: [generatedLayerId3],
        baseLayers: [generatedLayerId4],
      });

      expect(state.layers!.allIds).toHaveLength(4);
      // check that new layer ids are mapped to correct layer names
      expect(state.layers!.byId[generatedLayerId0].name).toEqual(layer1name);
      expect(state.layers!.byId[generatedLayerId1].name).toEqual(layer0name);
    });
    it('should set baselayer.', () => {
      const baseLayerName = 'baseLayerName';
      const mapPreset = {
        mapId,
        initialProps: {
          mapPreset: {
            layers: [
              {
                id: 'baseLayerIdBefore',
                name: baseLayerName,
                type: 'twms',
                layerType: LayerType.baseLayer,
              },
            ],
          },
        },
      };

      const state = setupStoreAndSetMapPreset(mapPreset);

      expect(state.webmap!.byId[mapId]).toEqual({
        ...mapAfterSetupIfMapPresetIsEmpty,
        baseLayers: [generatedLayerId2],
      });

      expect(state.layers!.allIds).toHaveLength(2);
      // check that new layer id is mapped to correct layer name
      expect(state.layers!.byId[generatedLayerId2].name).toEqual(baseLayerName);
    });
    it('should set baselayer and overlayer.', () => {
      const baseLayerName = 'baseLayerName';
      const overLayerName = 'overLayerName';
      const mapPreset = {
        mapId,
        initialProps: {
          mapPreset: {
            layers: [
              {
                id: 'baseLayerIdBefore',
                name: baseLayerName,
                type: 'twms',
                layerType: LayerType.baseLayer,
              },
              {
                id: 'overLayerIdBefore',
                name: overLayerName,
                type: 'twms',
                layerType: LayerType.overLayer,
              },
            ],
          },
        },
      };
      const state = setupStoreAndSetMapPreset(mapPreset);

      expect(state.webmap!.byId[mapId]).toEqual({
        ...mapAfterSetupIfMapPresetIsEmpty,
        overLayers: [generatedLayerId1],
        baseLayers: [generatedLayerId2],
      });

      expect(state.layers!.allIds).toHaveLength(2);
      // check that new layer ids are mapped to correct layer names
      expect(state.layers!.byId[generatedLayerId1].name).toEqual(overLayerName);
      expect(state.layers!.byId[generatedLayerId2].name).toEqual(baseLayerName);
    });
    it('should save properties from the mapPreset in the store', () => {
      const duration = 99999;
      const interval = 88888;
      const animationDelay = 16;
      const bbox = {
        left: -7529663.50832266,
        bottom: 308359.5390525013,
        right: 7493930.85787452,
        top: 11742807.68245839,
      };
      const dockedLayerManagerSize = 'sizeSmall';
      const shouldAutoUpdate = true;
      const showTimeSlider = true;
      const shouldShowZoomControls = false;
      const displayMapPin = true;
      const shouldAnimate = true;
      const shouldShowLegend = true;
      const mapPreset = {
        mapId,
        initialProps: {
          mapPreset: {
            shouldAutoUpdate,
            showTimeSlider,
            proj: {
              bbox,
              srs: 'EPSG:3857',
            },
            shouldShowZoomControls,
            displayMapPin,
            shouldAnimate,
            animationPayload: {
              duration,
              interval,
              speed: animationDelay,
            },
            shouldShowLegend,
            dockedLayerManagerSize,
          },
        },
      } as const;

      const state = setupStoreAndSetMapPreset(mapPreset);

      expect(state.webmap!.byId[mapId]).toEqual({
        ...mapAfterSetupIfMapPresetIsEmpty,
        isAutoUpdating: shouldAutoUpdate,
        isTimeSliderVisible: showTimeSlider,
        bbox,
        shouldShowZoomControls,
        displayMapPin,
        isAnimating: shouldAnimate,
        animationStartTime: removeMilliseconds(
          sub(mockNowTime, { minutes: duration }).toISOString(),
        ),
        animationEndTime: mockNowTimeIso,
        timeStep: interval,
        isTimestepAuto: false,
        animationDelay: getSpeedDelay(animationDelay),
        dockedLayerManagerSize,
      });

      expect(state.ui!.dialogs[legendId]!.isOpen).toEqual(shouldShowLegend);
    });

    it('should set animationEndTime from valid endTime of mapPreset', () => {
      const two = 2;
      const duration = 1000;
      const shouldAnimate = false;
      const mapPreset = {
        mapId,
        initialProps: {
          mapPreset: {
            shouldAnimate,
            animationPayload: {
              duration,
              endTime: `NOW+PT${two}H00M`,
            },
          },
        },
      } as const;

      const state = setupStoreAndSetMapPreset(mapPreset);

      const animationEndTime = add(mockNowTime, { hours: two });
      expect(state.webmap!.byId[mapId]).toEqual({
        ...mapAfterSetupIfMapPresetIsEmpty,
        isAnimating: shouldAnimate,

        animationStartTime: sub(animationEndTime, {
          minutes: duration,
        }).toISOString(),

        animationEndTime: animationEndTime.toISOString(),
      });
    });
    it('should not set animationEndTime from invalid endTime of mapPreset', () => {
      const duration = 120;
      const mapPreset = {
        mapId,
        initialProps: {
          mapPreset: {
            shouldAnimate: false,
            animationPayload: {
              duration,
              endTime: '00:00', // this is not a valid time format
            },
          },
        },
      } as const;

      const state = setupStoreAndSetMapPreset(mapPreset);

      expect(state.webmap!.byId[mapId]).toEqual({
        ...mapAfterSetupIfMapPresetIsEmpty,
        isAnimating: false,

        animationStartTime: sub(defaultAnimationEndTime, {
          minutes: duration,
        }).toISOString(),

        animationEndTime: removeMilliseconds(
          defaultAnimationEndTime.toISOString(),
        ),
      });
    });
    it('should startAnimation with valid endTime of mapPreset if shouldEndtimeOverride is true', async () => {
      const duration = 120;
      const mapPreset = {
        mapId,
        initialProps: {
          mapPreset: {
            shouldAnimate: true,
            shouldAutoUpdate: true, // this will not be saved in the store because shouldEndtimeOverride is true
            animationPayload: {
              duration,
              endTime: 'NOW+PT0H00M',
              shouldEndtimeOverride: true,
            },
          },
        },
      } as const;

      const state = setupStoreAndSetMapPreset(mapPreset);

      expect(state.webmap!.byId[mapId]).toEqual({
        ...mapAfterSetupIfMapPresetIsEmpty,

        isTimestepAuto: false,
        isAnimating: true,
        isAutoUpdating: false, // auto update is off because shouldEndtimeOverride is true
        isEndTimeOverriding: true,

        animationStartTime: sub(mockNowTime, {
          minutes: duration,
        }).toISOString(),

        animationEndTime: mockNowTime.toISOString(),

        timeStep: defaultTimeStep,
      });
    });
    it('should not start animation with endTime of mapPreset if shouldEndtimeOverride is false', () => {
      const duration = 1000;
      const shouldAnimate = true;

      const mapPreset = {
        mapId,
        initialProps: {
          mapPreset: {
            shouldAnimate,
            shouldAutoUpdate: true,
            animationPayload: {
              duration,
              endTime: 'NOW+PT2H00M', // this value is ignored because shouldEndtimeOverride is false
              shouldEndtimeOverride: false,
            },
          },
        },
      } as const;

      const state = setupStoreAndSetMapPreset(mapPreset);

      expect(state.webmap!.byId[mapId]).toEqual({
        ...mapAfterSetupIfMapPresetIsEmpty,

        isTimestepAuto: false,
        timeStep: defaultTimeStep,
        isAnimating: shouldAnimate,
        isAutoUpdating: false,

        animationStartTime: removeMilliseconds(
          sub(mockNowTime, {
            minutes: duration,
          }).toISOString(),
        ),
        animationEndTime: mockNowTimeIso,
      });
    });
    it('should fire toggleTimestepAuto', () => {
      const mapPreset = {
        mapId,
        initialProps: {
          mapPreset: {
            toggleTimestepAuto: false,
          },
        },
      };

      const state = setupStoreAndSetMapPreset(mapPreset);

      expect(state.webmap!.byId[mapId]).toEqual({
        ...mapAfterSetupIfMapPresetIsEmpty,
        isTimestepAuto: false,
      });
    });
  });

  describe('unregisterMapSaga', () => {
    it('should delete layers for unregistered map', () => {
      const store = setupStore();

      // add second layer to map
      act(() => {
        const layer2id = 'layer2id';
        const layer2 = {
          ...layer,
          name: 'layer2name',
          id: layer2id,
        };
        store.dispatch(
          layerActions.addLayer({
            layerId: layer2id,
            mapId,
            layer: layer2,
            origin,
          }),
        );
      });

      act(() => {
        store.dispatch(
          mapActions.unregisterMap({
            mapId,
          }),
        );
      });

      expect(store.getState().layers!.byId).toEqual({});
    });
  });
});
