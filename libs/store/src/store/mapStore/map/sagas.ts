/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import {
  takeLatest,
  select,
  put,
  call,
  takeEvery,
  all,
  delay,
} from 'redux-saga/effects';
import { SagaIterator } from 'redux-saga';
import moment, { Moment } from 'moment';
import { AnimationStep, webmapUtils } from '@opengeoweb/webmap';
import { defaultLayers } from '@opengeoweb/webmap-react';
import { mapActions, mapSelectors } from '.';

import * as layerSelectors from '../layers/selectors';
import { layerActions } from '../layers';

import { Layer, LayerActionOrigin, ReduxLayer } from '../layers/types';
import { defaultTimeStep, IS_LEGEND_OPEN_BY_DEFAULT } from './constants';
import {
  replaceLayerIdsToEnsureUniqueLayerIdsInStore,
  LayersAndAutoLayerIds,
} from './replaceLayerIdsToEnsureUniqueLayerIdsInStore';
import { filterLayers } from './filterLayers';
import { uiActions } from '../../ui/reducer';
import { getSyncedMapIdsForTimeslider } from '../../generic/synchronizationGroups/selectors';
import { setTime } from '../../generic/actions';
import { TimeListType, ToggleAutoUpdatePayload } from '../types';
import { dateFormat, getSpeedDelay, roundWithTimeStep } from './utils';

// Expects start, end time as moment object and interval in minutes
export const generateTimeList = (
  start: Moment,
  end: Moment,
  interval: number,
): { name: string; value: string }[] => {
  const timeList: { name: string; value: string }[] = [];
  const unixStart = moment(start).utc().unix();
  const unixEnd = moment(end).utc().unix();
  const intervalSeconds = interval * 60;
  for (let j = unixStart; j <= unixEnd; j += intervalSeconds) {
    timeList.push({ name: 'time', value: moment.unix(j).toISOString() });
  }
  return timeList;
};

export const getAnimationEndTime = (animationEndTime: string): string => {
  const calculateFromNow = animationEndTime.split(/[-+]/)[0] === 'NOW';
  const isAddingTime = animationEndTime.indexOf('+') !== -1;
  const timeInMinutes = moment
    .duration(animationEndTime.split(/[-+]/)[1])
    .asMinutes();
  const startingTime = calculateFromNow
    ? moment().utc()
    : moment().utc().startOf('day');
  const newAnimationEndTime = isAddingTime
    ? moment(startingTime).add(timeInMinutes, 'minutes').toISOString()
    : moment(startingTime).subtract(timeInMinutes, 'minutes').toISOString();
  return newAnimationEndTime;
};

export const isAnimationEndTimeValid = (animationEndTime: string): boolean => {
  const hasValidPrefix =
    animationEndTime.split(/[-+]/)[0] === 'NOW' ||
    animationEndTime.split(/[-+]/)[0] === 'TODAY';
  const hasValidDate = moment.isDuration(
    moment.duration(animationEndTime.split(/[-+]/)[1]),
  );
  return hasValidPrefix && hasValidDate;
};

const validInitialAnimationStep = (
  initialAnimationStep: number,
  draw: string | AnimationStep[] | undefined,
): boolean =>
  !!initialAnimationStep &&
  !!draw?.length &&
  initialAnimationStep >= 0 &&
  draw.length > initialAnimationStep;

export function updateMapDraw(
  mapId: string,
  draw: string | AnimationStep[] | undefined,
  initialAnimationStep?: number,
): void {
  const webMap = webmapUtils.getWMJSMapById(mapId);
  if (webMap) {
    webMap.getListener().suspendEvents();
    webMap.stopAnimating();
    if (
      initialAnimationStep &&
      validInitialAnimationStep(initialAnimationStep, draw)
    ) {
      webMap.initialAnimationStep = initialAnimationStep;
    } else {
      webMap.initialAnimationStep = 0;
    }
    webMap.draw(draw);
    webMap.getListener().resumeEvents();
  }
}

export function* startAnimationSaga({
  payload,
}: ReturnType<typeof mapActions.mapStartAnimation>): Generator {
  const { mapId, start, initialTime, end, interval, timeList } = payload;

  const roundedStart = roundWithTimeStep(
    Number(start && moment.utc(start).unix()),
    interval!,
    'ceil',
  );
  const roundedEnd = roundWithTimeStep(
    Number(end && moment.utc(end).unix()),
    interval!,
    'floor',
  );

  const animationList =
    timeList ||
    generateTimeList(
      moment.utc(roundedStart * 1000),
      moment.utc(roundedEnd * 1000),
      interval!,
    );
  yield animationList;

  const initialAnimationStep =
    initialTime && animationList
      ? (animationList as TimeListType[]).findIndex(
          (time) => new Date(time.value) >= new Date(initialTime),
        )
      : undefined;

  yield updateMapDraw(
    mapId,
    animationList as string | AnimationStep[] | undefined,
    initialAnimationStep,
  );
}

export function* stopAnimationSaga({
  payload,
}: ReturnType<typeof mapActions.mapStopAnimation>): Generator {
  const { mapId } = payload;
  yield updateMapDraw(mapId, 'opengeoweb-core-map-reducer');
}

export function* deleteLayerSaga({
  payload,
}: ReturnType<typeof layerActions.layerDelete>): SagaIterator {
  const { mapId } = payload;
  const layers = yield select(mapSelectors.getMapLayers, mapId);
  if (!layers.length) {
    yield put(mapActions.mapStopAnimation({ mapId }));
  }
}

export function* updateAnimation(
  mapId: string,
  maxValue: string,
): SagaIterator {
  const animationStart = yield select(
    mapSelectors.getAnimationStartTime,
    mapId,
  );
  const animationStartUnix = moment.utc(animationStart).unix();
  const animationEnd = yield select(mapSelectors.getAnimationEndTime, mapId);
  const animationEndUnix = moment.utc(animationEnd).unix();
  const t = moment.utc(maxValue).unix();
  const deltaT = t - animationEndUnix;

  const start = moment
    .unix(animationStartUnix + deltaT)
    .utc()
    .format(dateFormat);
  const end = moment
    .unix(animationEndUnix + deltaT)
    .utc()
    .format(dateFormat);

  yield put(
    mapActions.setAnimationEndTime({
      mapId,
      animationEndTime: end,
    }),
  );
  yield put(
    mapActions.setAnimationStartTime({
      mapId,
      animationStartTime: start,
    }),
  );

  const isAnimating = yield select(mapSelectors.isAnimating, mapId);
  if (isAnimating) {
    // restart animation if animation was running
    const timeStep = yield select(mapSelectors.getMapTimeStep, mapId);
    yield put(
      mapActions.mapStartAnimation({
        mapId,
        start,
        end,
        interval: timeStep,
      }),
    );
  }
}

export function* setLayerDimensionsSaga({
  payload,
}: ReturnType<typeof layerActions.onUpdateLayerInformation>): SagaIterator {
  try {
    const { layerDimensions } = payload;
    if (!layerDimensions) {
      return;
    }
    const { dimensions, layerId } = layerDimensions;
    const layer = yield select(layerSelectors.getLayerById, layerId);
    if (!layer) {
      return;
    }

    const newTimeDimension = dimensions.find(
      (dimension) => dimension.name === 'time',
    );
    const { mapId } = layer;
    const autoUpdateLayerId = yield select(
      mapSelectors.getAutoUpdateLayerId,
      mapId,
    );
    const shouldAutoUpdate = yield select(mapSelectors.isAutoUpdating, mapId);
    const prevTimeDimension = yield select(
      layerSelectors.getLayerTimeDimension,
      layerId,
    );

    const isAnimating = yield select(mapSelectors.isAnimating, mapId);
    const webmapInstance = webmapUtils.getWMJSMapById(mapId);

    const isAutoUpdateLayer = layerId === autoUpdateLayerId;

    const incomingMaxTime = newTimeDimension?.maxValue;
    const isIncomingMaxTimeLaterThanCurrentLayerTime =
      incomingMaxTime &&
      prevTimeDimension?.currentValue &&
      prevTimeDimension.currentValue !== incomingMaxTime;

    if (
      isAutoUpdateLayer && // only update the active layer
      shouldAutoUpdate &&
      isIncomingMaxTimeLaterThanCurrentLayerTime
    ) {
      /*
        To prevent weird animation time jumping while updating,
        this should only update the layer dimension if it is not animating,
        otherwise the animation will jump between max and then back to its own animation time.
        When animating, the animation logic makes sure that the layer is updated to the latest value
      */
      if (!isAnimating) {
        yield put(
          layerActions.layerChangeDimension({
            layerId,
            origin: LayerActionOrigin.setLayerDimensionSaga,
            dimension: {
              name: 'time',
              currentValue: incomingMaxTime,
            },
          }),
        );
      }

      // Each time a layer updates, set the new time for all synced timesliders
      const syncedMapIds: string[] = yield select(getSyncedMapIdsForTimeslider);
      if (syncedMapIds) {
        // Change time value for all other timesliders
        yield all(
          syncedMapIds.map((syncedMapId) =>
            put(
              setTime({
                origin: 'mapStore saga',
                sourceId: syncedMapId,
                value: incomingMaxTime,
              }),
            ),
          ),
        );
      }

      yield call(updateAnimation, mapId, incomingMaxTime);

      // If there is a discrepancy between the redux store and the webmap isAnimating
      // it means the animation hasn't started yet - start it now we have the time dimension
    } else if (isAnimating && !webmapInstance.isAnimating && incomingMaxTime) {
      yield call(updateAnimation, mapId, incomingMaxTime);
    }
  } catch (error) {
    // eslint-disable-next-line no-console
    console.warn(error);
  }
}

export function* toggleAutoUpdateSaga({
  payload,
}: ReturnType<typeof mapActions.toggleAutoUpdate>): SagaIterator {
  const { shouldAutoUpdate, mapId } = payload;
  if (!shouldAutoUpdate) {
    return;
  }
  try {
    // go to end of active layer
    const autoUpdateLayerId = yield select(
      mapSelectors.getAutoUpdateLayerId,
      mapId,
    );
    const timeDimension = yield select(
      layerSelectors.getLayerTimeDimension,
      autoUpdateLayerId,
    );
    if (timeDimension?.maxValue) {
      yield put(
        layerActions.layerChangeDimension({
          layerId: autoUpdateLayerId,
          origin: LayerActionOrigin.toggleAutoUpdateSaga,
          dimension: {
            name: 'time',
            currentValue: timeDimension.maxValue,
          },
        }),
      );

      // Change time value for all other timesliders that are synced by syncgroups
      const syncedMapIds: string[] = yield select(getSyncedMapIdsForTimeslider);
      if (syncedMapIds.length > 0) {
        yield all(
          syncedMapIds.map((syncedMapId) =>
            put(
              setTime({
                origin: 'mapStore saga',
                sourceId: syncedMapId,
                value: timeDimension.maxValue,
              }),
            ),
          ),
        );

        // When timesliders are synced by syncgrous autoupdate is toggled on, toggle autoupdate off for other timesliders
        const payloads: ToggleAutoUpdatePayload[] = syncedMapIds.reduce<
          ToggleAutoUpdatePayload[]
        >((syncedMapIdList, syncedMapId) => {
          if (syncedMapId !== mapId) {
            return syncedMapIdList.concat({
              mapId: syncedMapId,
              shouldAutoUpdate: false,
            });
          }
          return syncedMapIdList;
        }, []);

        if (payloads.length > 0) {
          yield all(
            payloads.map((payload) =>
              put(mapActions.toggleAutoUpdate(payload)),
            ),
          );
        }
      }
      yield call(updateAnimation, mapId, timeDimension.maxValue);
    }
  } catch (error) {
    // eslint-disable-next-line no-console
    console.warn(error);
  }
}

export function* handleBaseLayersSaga(
  mapId: string,
  baseLayers: Layer[],
): SagaIterator {
  const baseLayer = baseLayers.find((layer) => layer.layerType === 'baseLayer');
  const currentAvailableBaseLayers = yield select(
    layerSelectors.getAvailableBaseLayersForMap,
    mapId,
  );

  // find a availableBaseLayer with the same name, and use that id for the active baselayer
  const activeAvailableBaseLayer = currentAvailableBaseLayers.find(
    (availableBaseLayer: ReduxLayer) =>
      availableBaseLayer.name === baseLayer!.name,
  );

  const activeBaseLayerId = activeAvailableBaseLayer
    ? activeAvailableBaseLayer.id
    : webmapUtils.generateLayerId();

  // if the baseLayer can't be found in a visible available baseLayer list, add it
  if (!activeAvailableBaseLayer && currentAvailableBaseLayers.length) {
    yield put(
      layerActions.addAvailableBaseLayers({
        // TODO: remove type casting in https://gitlab.com/opengeoweb/opengeoweb/-/issues/1884
        layers: [{ ...baseLayer, mapId, id: activeBaseLayerId }] as Layer[],
      }),
    );
  }

  const baseLayersWithActiveId = baseLayers.map((layer, index) =>
    index === 0
      ? {
          ...layer,
          id: activeBaseLayerId || layer.id,
        }
      : layer,
  );

  yield put(
    layerActions.setBaseLayers({
      mapId,
      // TODO: remove type casting in https://gitlab.com/opengeoweb/opengeoweb/-/issues/1884
      layers: baseLayersWithActiveId as Layer[],
    }),
  );
}

export function* setMapPresetSaga({
  payload,
}: ReturnType<typeof mapActions.setMapPreset>): SagaIterator {
  try {
    const { mapId, initialProps } = payload;
    const { mapPreset } = initialProps;

    if (mapPreset) {
      const {
        layers,
        activeLayerId,
        autoTimeStepLayerId,
        autoUpdateLayerId,
        proj,
        shouldAutoUpdate,
        shouldAnimate,
        animationPayload,
        toggleTimestepAuto,
        showTimeSlider,
        displayMapPin,
        shouldShowZoomControls,
        shouldShowLegend,
        dockedLayerManagerSize,
      } = mapPreset;

      const { mapLayers, baseLayers, overLayers } = filterLayers(layers);
      if (layers) {
        //  make sure all layers have a unique id before going forward
        let autoTimeStepLayerIdNew = autoTimeStepLayerId;
        let autoUpdateLayerIdNew = autoUpdateLayerId;
        const onlyActiveLayerIdIsSet =
          !autoTimeStepLayerId && !autoUpdateLayerId && activeLayerId;
        if (onlyActiveLayerIdIsSet) {
          autoTimeStepLayerIdNew = activeLayerId;
          autoUpdateLayerIdNew = activeLayerId;
        }
        const newLayerIds: LayersAndAutoLayerIds = yield call(
          replaceLayerIdsToEnsureUniqueLayerIdsInStore,
          {
            layers: mapLayers,
            autoTimeStepLayerId: autoTimeStepLayerIdNew,
            autoUpdateLayerId: autoUpdateLayerIdNew,
          },
        );

        //  set layers
        yield put(
          layerActions.setLayers({
            mapId,
            layers: newLayerIds.layers,
          }),
        );

        //  set active layer if given otherwise to first layer
        const firstLayerId = newLayerIds.layers[0]?.id;
        yield put(
          mapActions.setAutoLayerId({
            mapId,
            autoTimeStepLayerId:
              newLayerIds.autoTimeStepLayerId ?? firstLayerId,
            autoUpdateLayerId: newLayerIds.autoUpdateLayerId ?? firstLayerId,
          }),
        );
      }

      // sets (default) baseLayers
      const baseLayersWithDefaultLayer = baseLayers.length
        ? baseLayers
        : [defaultLayers.baseLayerGrey];
      // sets (default) overLayers
      const overLayersWithDefaultLayer = overLayers!.length
        ? overLayers
        : [defaultLayers.overLayer];

      const allBaseLayers = [
        ...baseLayersWithDefaultLayer,
        ...overLayersWithDefaultLayer!,
      ].map((layer) => ({
        ...layer,
        id: webmapUtils.generateLayerId(),
      })) as Layer[];

      yield call(handleBaseLayersSaga, mapId, allBaseLayers);

      if (proj) {
        //  set bbox
        yield put(
          mapActions.setBbox({
            mapId,
            bbox: proj?.bbox,
            srs: proj?.srs,
          }),
        );
      }

      const animationLength = animationPayload && animationPayload.duration;
      const animationEndTime =
        animationPayload &&
        animationPayload.endTime &&
        isAnimationEndTimeValid(animationPayload.endTime) &&
        animationPayload.endTime;
      const shouldEndtimeOverride = animationPayload
        ? animationPayload.shouldEndtimeOverride
        : false;

      if (shouldEndtimeOverride) {
        //  auto update
        yield put(
          mapActions.setEndTimeOverriding({
            mapId,
            shouldEndtimeOverride,
          }),
        );
      }

      if (shouldAutoUpdate !== undefined && !animationEndTime) {
        //  auto update
        yield put(
          mapActions.toggleAutoUpdate({
            mapId,
            shouldAutoUpdate,
          }),
        );
      }

      if (showTimeSlider !== undefined) {
        // toggle timeslider
        yield put(
          mapActions.toggleTimeSliderIsVisible({
            mapId,
            isTimeSliderVisible: showTimeSlider,
          }),
        );
      }

      if (shouldShowZoomControls !== undefined) {
        // toggle zoom controls
        yield put(
          mapActions.toggleZoomControls({ mapId, shouldShowZoomControls }),
        );
      }

      if (displayMapPin !== undefined) {
        //  display map pin
        yield put(
          mapActions.toggleMapPinIsVisible({
            mapId,
            displayMapPin,
          }),
        );
      }

      // sets timestep by interval of animationPayload
      const interval = animationPayload && animationPayload.interval;
      if (interval) {
        yield put(mapActions.setTimeStep({ mapId, timeStep: interval }));
      }

      // sets animationEndTime by endTime of animationPayload
      if (animationEndTime) {
        yield put(
          mapActions.setAnimationEndTime({
            mapId,
            animationEndTime: getAnimationEndTime(animationEndTime),
          }),
        );
        if (!animationLength) {
          const animationEnd = yield select(
            mapSelectors.getAnimationEndTime,
            mapId,
          );
          yield put(
            mapActions.setAnimationStartTime({
              mapId,
              animationStartTime: moment(animationEnd)
                .subtract(5 * 60, 'minutes') // set to default of 5 hours
                .toISOString(),
            }),
          );
        }
      }

      // sets animationStartTime by duration of animationPayload
      if (animationLength) {
        const animationEnd = yield select(
          mapSelectors.getAnimationEndTime,
          mapId,
        );
        yield put(
          mapActions.setAnimationStartTime({
            mapId,
            animationStartTime: moment(animationEnd)
              .subtract(animationLength, 'minutes')
              .toISOString(),
          }),
        );
      }

      // sets animationDelay by speed of animationPayload
      if (animationPayload && animationPayload.speed) {
        yield put(
          mapActions.setAnimationDelay({
            mapId,
            animationDelay: getSpeedDelay(animationPayload.speed),
          }),
        );
      }

      //  turn animation on
      if (shouldAnimate === true) {
        const duration =
          animationPayload && animationPayload.duration
            ? animationPayload.duration
            : 5 * 60; //  set to default of 5 hours
        const animationEnd =
          shouldEndtimeOverride && animationEndTime
            ? yield select(mapSelectors.getAnimationEndTime, mapId)
            : moment.utc().format(dateFormat);
        const animationStart =
          shouldEndtimeOverride && animationLength
            ? yield select(mapSelectors.getAnimationStartTime, mapId)
            : moment.utc().subtract(duration, 'minutes').format(dateFormat);
        yield put(
          mapActions.mapStartAnimation({
            mapId,
            start: animationStart,
            end: animationEnd,
            interval: interval || defaultTimeStep,
          }),
        );

        //  If animation interval set, set the timestep auto property to false
        if (interval) {
          yield put(
            mapActions.toggleTimestepAuto({
              mapId,
              timestepAuto: false,
            }),
          );
        }
      } else if (toggleTimestepAuto !== undefined) {
        //  Set timestep auto based on preset if animation is off
        yield put(
          mapActions.toggleTimestepAuto({
            mapId,
            timestepAuto: toggleTimestepAuto,
          }),
        );
      }

      // show legend
      const shouldOpenLegend =
        shouldShowLegend !== undefined
          ? shouldShowLegend
          : IS_LEGEND_OPEN_BY_DEFAULT;
      const legendId = yield select(mapSelectors.getLegendId, mapId);
      if (legendId) {
        yield put(
          uiActions.setToggleOpenDialog({
            type: legendId,
            setOpen: shouldOpenLegend,
          }),
        );
      }

      if (dockedLayerManagerSize) {
        yield put(
          mapActions.setDockedLayerManagerSize({
            mapId,
            dockedLayerManagerSize,
          }),
        );
      }

      while (
        animationEndTime &&
        (shouldEndtimeOverride || !(shouldAutoUpdate || shouldAnimate))
      ) {
        const fiveMinuteDelayForAnimation = 1000 * 60 * 5;
        yield delay(fiveMinuteDelayForAnimation);
        const animationEnd = yield select(
          mapSelectors.getAnimationEndTime,
          mapId,
        );
        yield put(
          mapActions.setAnimationEndTime({
            mapId,
            animationEndTime: moment(animationEnd)
              .add(5, 'minutes')
              .toISOString(),
          }),
        );
        const animationStart = yield select(
          mapSelectors.getAnimationStartTime,
          mapId,
        );
        yield put(
          mapActions.setAnimationStartTime({
            mapId,
            animationStartTime: moment(animationStart)
              .add(5, 'minutes')
              .toISOString(),
          }),
        );
      }
    }
  } catch (error) {
    console.error(error);
  }
}

export function* unregisterMapSaga({
  payload,
}: ReturnType<typeof mapActions.unregisterMap>): Generator {
  const { mapId } = payload;
  const layerList = yield select(layerSelectors.getLayersByMapId, mapId);
  yield all(
    (layerList as ReduxLayer[]).map((layer) =>
      put(
        layerActions.layerDelete({
          mapId,
          layerId: layer.id as string,
          layerIndex: 0,
          origin: LayerActionOrigin.unregisterMapSaga,
        }),
      ),
    ),
  );
}

export function* rootSaga(): SagaIterator {
  // resets WMJSMap state
  yield takeLatest(mapActions.mapStopAnimation.type, stopAnimationSaga);
  yield takeLatest(mapActions.mapStartAnimation.type, startAnimationSaga);
  yield takeLatest(layerActions.layerDelete.type, deleteLayerSaga);
  yield takeLatest(
    layerActions.onUpdateLayerInformation.type,
    setLayerDimensionsSaga,
  );
  yield takeLatest(mapActions.toggleAutoUpdate.type, toggleAutoUpdateSaga);
  yield takeEvery(mapActions.setMapPreset.type, setMapPresetSaga);
  yield takeEvery(mapActions.unregisterMap.type, unregisterMapSaga);
}

export default rootSaga;
