/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */

import { LayerType } from '@opengeoweb/webmap';
import {
  WmMultiDimensionServices,
  defaultReduxServices,
} from './storeTestSettings';
import type { CoreAppStore } from '../types';
import { createMap } from './map/utils';
import { createLayer } from './layers/reducer';
import { layerTypes } from './layers';
import { mapConstants, mapTypes } from './map';
import { WebMapStateModuleState } from './types';

export const createLayersState = (
  layerId: string,
  props = {},
): layerTypes.LayerState => ({
  byId: {
    [layerId]: createLayer({
      id: layerId,
      ...props,
    }),
  },
  allIds: [layerId],
  availableBaseLayers: { allIds: [], byId: {} },
});

export const createMultipleLayersState = (
  layers: layerTypes.Layer[],
  mapId: string,
): layerTypes.LayerState => {
  const mockState: layerTypes.LayerState = {
    byId: {},
    allIds: [],
    availableBaseLayers: { allIds: [], byId: {} },
  };

  for (let i = 0; i < layers.length; i += 1) {
    const layerState = createLayer({ ...layers[i], id: layers[i].id, mapId });
    mockState.byId[layers[i].id!] = { ...layerState };
    mockState.allIds.push(layers[i].id!);
    if (layers[i].layerType === LayerType.baseLayer) {
      mockState.availableBaseLayers.byId[layers[i].id!] = { ...layerState };
      mockState.availableBaseLayers.allIds.push(layers[i].id!);
    }
  }
  return mockState;
};

export const createWebmapState = (
  ...mapIds: string[]
): mapTypes.WebMapState => {
  const mockState: mapTypes.WebMapState = {
    byId: {},
    allIds: [],
  };

  for (let i = 0; i < mapIds.length; i += 1) {
    const mapState = createMap({ id: mapIds[i] });
    mockState.byId[mapIds[i]] = { ...mapState };
    mockState.allIds.push(mapIds[i]);
  }
  return mockState;
};

export const createMapDimensionsState = (
  dimensions?: mapTypes.Dimension[],
  ...mapIds: string[]
): mapTypes.WebMapState => {
  const mockState: mapTypes.WebMapState = {
    byId: {},
    allIds: [],
  };

  for (let i = 0; i < mapIds.length; i += 1) {
    const mapState = createMap({ id: mapIds[i] });
    mockState.byId[mapIds[i]] = { ...mapState };
    mockState.byId[mapIds[i]].dimensions = dimensions;
    mockState.allIds.push(mapIds[i]);
  }
  return mockState;
};
export const webmapStateWithAddedLayer = (
  layerType: LayerType,
  layerId: string,
  mapId: string,
  dimensions?: mapTypes.Dimension[],
): mapTypes.WebMapState => {
  const webmap = createWebmapState(mapId);

  // Add the dimensions to the map state
  if (dimensions && dimensions.length) {
    dimensions.forEach((dimension) => {
      webmap.byId[mapId].dimensions!.push(dimension);
    });
  }

  // Add the layer to the map state
  switch (layerType) {
    case LayerType.mapLayer:
      webmap.byId[mapId].mapLayers.push(layerId);
      break;
    case LayerType.baseLayer:
      webmap.byId[mapId].baseLayers.push(layerId);
      break;
    case LayerType.overLayer:
      webmap.byId[mapId].overLayers.push(layerId);
      break;
    default:
      break;
  }

  // first layer is active layer
  webmap.byId[mapId].autoUpdateLayerId = layerId;
  webmap.byId[mapId].autoTimeStepLayerId = layerId;

  return webmap;
};

export const mockStateMapWithLayer = (
  layer: layerTypes.Layer,
  mapId: string,
): CoreAppStore => ({
  webmap: webmapStateWithAddedLayer(
    layer.layerType!,
    layer.id!,
    mapId,
    layer.dimensions,
  ),
  services: { byId: defaultReduxServices, allIds: ['serviceid_1'] },
  layers: createLayersState(layer.id!, {
    name: layer.name,
    layerType: layer.layerType,
    mapId,
    service: layer.service,
    dimensions: layer.dimensions,
  }),
  syncronizationGroupStore: {
    groups: {
      byId: {},
      allIds: [],
    },
    sources: {
      byId: {},
      allIds: [],
    },
    viewState: {
      timeslider: {
        groups: [{ id: 'dummyId1', selected: ['mapId1', 'mapId2'] }],
        sourcesById: [],
      },
      zoompane: {
        groups: [],
        sourcesById: [],
      },
      level: {
        groups: [],
        sourcesById: [],
      },
    },
  },
});
const webmapStateWithMultipleLayers = (
  layers: layerTypes.Layer[],
  mapId: string,
): mapTypes.WebMapState => {
  const webmap = createWebmapState(mapId);

  // Add the layers to the map state
  layers.forEach((layer) => {
    switch (layer.layerType) {
      case LayerType.mapLayer:
        webmap.byId[mapId].mapLayers.push(layer.id!);
        break;
      case LayerType.baseLayer:
        webmap.byId[mapId].baseLayers.push(layer.id!);
        break;
      case LayerType.overLayer:
        webmap.byId[mapId].overLayers.push(layer.id!);
        break;
      case LayerType.featureLayer:
        webmap.byId[mapId].featureLayers.push(layer.id!);
        break;
      default:
        break;
    }
  });

  // first layer is active layer
  if (layers.length) {
    webmap.byId[mapId].autoTimeStepLayerId = layers[0].id;
    webmap.byId[mapId].autoUpdateLayerId = layers[0].id;
  }

  return webmap;
};

export const mockStateMapWithMultipleLayers = (
  layers: layerTypes.Layer[],
  mapId: string,
): CoreAppStore => ({
  webmap: webmapStateWithMultipleLayers(layers, mapId),
  services: { byId: defaultReduxServices, allIds: ['serviceid_1'] },
  layers: createMultipleLayersState(layers, mapId),
  syncronizationGroupStore: {
    groups: {
      byId: {},
      allIds: [],
    },
    sources: {
      byId: {},
      allIds: [],
    },
    viewState: {
      timeslider: {
        groups: [{ id: 'dummyId1', selected: ['mapId1', 'mapId2'] }],
        sourcesById: [],
      },
      zoompane: {
        groups: [],
        sourcesById: [],
      },
      level: {
        groups: [],
        sourcesById: [],
      },
    },
  },
});

export const testGeoJSON: GeoJSON.FeatureCollection = {
  type: 'FeatureCollection',
  features: [
    {
      type: 'Feature',
      geometry: {
        type: 'Polygon',
        coordinates: [
          [
            [5.0, 55.0],
            [4.331914, 55.332644],
            [3.368817, 55.764314],
            [2.761908, 54.379261],
            [3.15576, 52.913554],
            [2.000002, 51.500002],
            [3.370001, 51.369722],
            [3.370527, 51.36867],
            [3.362223, 51.320002],
            [3.36389, 51.313608],
            [3.373613, 51.309999],
            [3.952501, 51.214441],
            [4.397501, 51.452776],
            [5.078611, 51.391665],
            [5.848333, 51.139444],
            [5.651667, 50.824717],
            [6.011797, 50.757273],
            [5.934168, 51.036386],
            [6.222223, 51.361666],
            [5.94639, 51.811663],
            [6.405001, 51.830828],
            [7.053095, 52.237764],
            [7.031389, 52.268885],
            [7.063612, 52.346109],
            [7.065557, 52.385828],
            [7.133055, 52.888887],
            [7.14218, 52.898244],
            [7.191667, 53.3],
            [6.5, 53.666667],
            [6.500002, 55.000002],
            [5.0, 55.0],
          ],
        ],
      },
      properties: {
        fill: '#ff7800',
        'fill-opacity': 0.2,
        stroke: '#ff7800',
        'stroke-width': 2,
        'stroke-opacity': 1,
      },
    },
  ],
};

const webmapStateWithAnimationDelay = (
  mapId: string,
  animationDelay: number,
): mapTypes.WebMapState => {
  const webmap = createWebmapState(mapId);

  // Add the dimensions to the map state
  webmap.byId[mapId].animationDelay = animationDelay;
  return webmap;
};

export const mockStateMapWithAnimationDelayWithoutLayers = (
  mapId: string,
): WebMapStateModuleState => ({
  webmap: webmapStateWithAnimationDelay(
    mapId,
    mapConstants.defaultAnimationDelayAtStart,
  ),
});

const webmapStateWithTimeSliderSpan = (
  mapId: string,
  timeSliderSpan: number,
): mapTypes.WebMapState => {
  const webmap = createWebmapState(mapId);

  // Add the span to the map state
  webmap.byId[mapId].timeSliderSpan = timeSliderSpan;
  return webmap;
};
export const mockStateMapWithTimeSliderSpanWithoutLayers = (
  mapId: string,
  timeSliderSpan: number,
): WebMapStateModuleState => ({
  webmap: webmapStateWithTimeSliderSpan(mapId, timeSliderSpan),
});

export const mockStateMapWithDimensions = (
  layer: layerTypes.Layer,
  mapId: string,
): WebMapStateModuleState => ({
  webmap: webmapStateWithAddedLayer(
    layer.layerType!,
    layer.id!,
    mapId,
    layer.dimensions,
  ),
  services: {
    byId: WmMultiDimensionServices,
    allIds: ['serviceid_1', 'serviceid_2'],
  },
  layers: createLayersState(layer.id!, {
    name: layer.name,
    layerType: layer.layerType,
    mapId,
    service: layer.service,
    dimensions: layer.dimensions,
    enabled: layer?.enabled !== undefined ? layer.enabled : true,
  }),
});

const webmapStateWithTimeStep = (
  mapId: string,
  timeStep: number,
): mapTypes.WebMapState => {
  const webmap = createWebmapState(mapId);

  // Add the timeStep to the map state
  webmap.byId[mapId].timeStep = timeStep;
  return webmap;
};

export const mockStateMapWithTimeStepWithoutLayers = (
  mapId: string,
  timeStep: number,
): WebMapStateModuleState => ({
  webmap: webmapStateWithTimeStep(mapId, timeStep),
});
