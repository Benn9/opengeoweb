/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */

import React from 'react';
import { Provider } from 'react-redux';
import {
  lightTheme,
  ThemeProviderProps,
  ThemeWrapper,
} from '@opengeoweb/theme';
import { Store } from '@reduxjs/toolkit';
import { withEggs } from '@opengeoweb/shared';
import { createStore as createEggStore } from '@redux-eggs/redux-toolkit';
import { getSagaExtension } from '@redux-eggs/saga-extension';
import { coreModuleConfig } from '../coreModuleConfig';

export const createStore = (): Store =>
  createEggStore({
    extensions: [getSagaExtension({})],
  });

const ThemeWrapperWithModules: React.FC<ThemeProviderProps> = withEggs(
  coreModuleConfig,
)(({ theme, children }: ThemeProviderProps) => (
  <ThemeWrapper theme={theme}>{children} </ThemeWrapper>
));

interface ThemeStoreProviderProps extends ThemeProviderProps {
  store: Store;
}

/**
 * A Provider component which provides the GeoWeb theme and store for the core.
 * Note: Should only be used with core components, as the provided store is only for core.
 * @param children
 * @returns
 */
export const ThemeStoreProvider: React.FC<ThemeStoreProviderProps> = ({
  children,
  theme = lightTheme,
  store,
}) => (
  <Provider store={store}>
    <ThemeWrapperWithModules theme={theme}>
      {children as React.ReactElement}
    </ThemeWrapperWithModules>
  </Provider>
);
