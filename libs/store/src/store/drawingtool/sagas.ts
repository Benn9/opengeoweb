/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */

import { put, select, takeLatest } from 'redux-saga/effects';
import { SagaIterator } from 'redux-saga';
import { LayerType } from '@opengeoweb/webmap';
import {
  emptyGeoJSON,
  getFeatureCollection,
  getGeoJson,
  moveFeature,
} from '@opengeoweb/webmap-react';
import { drawtoolActions } from './reducer';
import * as drawtoolSelectors from './selectors';
import { layerActions, layerSelectors, mapStoreActions } from '../mapStore';

const registerOrigin = 'drawings saga:registerDrawToolSaga';

export function* registerDrawToolSaga({
  payload,
}: ReturnType<typeof drawtoolActions.registerDrawTool>): SagaIterator {
  const {
    mapId,
    geoJSONLayerId,
    geoJSONIntersectionLayerId,
    geoJSONIntersectionBoundsLayerId,
    defaultGeoJSON = emptyGeoJSON,
    defaultGeoJSONIntersection = emptyGeoJSON,
    defaultGeoJSONIntersectionBounds,
  } = payload;

  // create for every drawTool a draw layer
  if (geoJSONLayerId && mapId) {
    yield put(
      mapStoreActions.addLayer({
        mapId,
        layer: {
          // empty geoJSON
          geojson: defaultGeoJSON,
          layerType: LayerType.featureLayer,
        },
        layerId: geoJSONLayerId,
        origin: registerOrigin,
      }),
    );
  }
  // create intersection layer
  if (geoJSONIntersectionLayerId && mapId) {
    yield put(
      mapStoreActions.addLayer({
        mapId,
        layer: {
          geojson: defaultGeoJSONIntersection,
          layerType: LayerType.featureLayer,
        },
        layerId: geoJSONIntersectionLayerId,
        origin: registerOrigin,
      }),
    );
  }
  // create intersection bounds layer
  if (geoJSONIntersectionBoundsLayerId && mapId) {
    yield put(
      mapStoreActions.addLayer({
        mapId,
        layer: {
          geojson: defaultGeoJSONIntersectionBounds,
          layerType: LayerType.featureLayer,
        },
        layerId: geoJSONIntersectionBoundsLayerId,
        origin: registerOrigin,
      }),
    );
  }
}

export function* changeDrawToolSaga({
  payload,
}: ReturnType<typeof drawtoolActions.changeDrawToolMode>): SagaIterator {
  try {
    const { drawModeId, drawToolId, shouldUpdateShape = true } = payload;
    const drawingTool = yield select(
      drawtoolSelectors.selectDrawToolById,
      drawToolId,
    );

    const {
      shouldAllowMultipleShapes = false,
      geoJSONIntersectionLayerId,
      geoJSONIntersectionBoundsLayerId,
    } = drawingTool;

    const newDrawMode = yield select(
      drawtoolSelectors.getDrawModeById,
      drawToolId,
      drawModeId,
    );

    // delete shape
    if (newDrawMode.value === 'DELETE') {
      yield put(
        layerActions.layerChangeGeojson({
          layerId: drawingTool.geoJSONLayerId,
          geojson: emptyGeoJSON,
        }),
      );

      // clear intersection shape
      if (geoJSONIntersectionLayerId) {
        yield put(
          layerActions.layerChangeGeojson({
            layerId: geoJSONIntersectionLayerId,
            geojson: emptyGeoJSON,
          }),
        );
      }

      yield put(
        layerActions.toggleFeatureMode({
          layerId: drawingTool.geoJSONLayerId,
          isInEditMode: false,
          drawMode: '',
        }),
      );
      return;
    }

    // disable tool
    if (!drawingTool.activeDrawModeId && newDrawMode.isSelectable) {
      yield put(
        layerActions.toggleFeatureMode({
          layerId: drawingTool.geoJSONLayerId,
          isInEditMode: false,
          drawMode: '',
        }),
      );
      return;
    }

    // check tool is selected of existing drawn shape
    const currentGeoJSONLayer = yield select(
      layerSelectors.getLayerById,
      drawingTool.geoJSONLayerId,
    );
    const { geojson: currentGeoJSON, selectedFeatureIndex = 0 } =
      currentGeoJSONLayer || {};

    const currentSelectionType =
      currentGeoJSON?.features[selectedFeatureIndex]?.properties?.selectionType;
    const newSelectionType = newDrawMode.shape.properties?.selectionType;
    const isNewToolSelected = currentSelectionType !== newSelectionType;

    const shouldUpdateNewShape =
      !currentGeoJSON?.features?.length ||
      shouldAllowMultipleShapes ||
      isNewToolSelected;

    // don't change anything if same tool is selected again
    if (shouldUpdateNewShape && shouldUpdateShape) {
      const geoJSONFeatureCollection = getFeatureCollection(
        newDrawMode.shape,
        shouldAllowMultipleShapes,
        currentGeoJSON,
      );
      const newGeoJSON = getGeoJson(
        geoJSONFeatureCollection,
        shouldAllowMultipleShapes,
      );

      if (shouldAllowMultipleShapes) {
        moveFeature(currentGeoJSON, newGeoJSON, selectedFeatureIndex, '');
      }

      yield put(
        layerActions.setSelectedFeature({
          layerId: drawingTool.geoJSONLayerId,
          selectedFeatureIndex: newGeoJSON.features.length - 1,
        }),
      );

      yield put(
        layerActions.updateFeature({
          layerId: drawingTool.geoJSONLayerId,
          geojson: newGeoJSON,
          ...(geoJSONIntersectionLayerId && {
            geoJSONIntersectionLayerId,
          }),
          ...(geoJSONIntersectionBoundsLayerId && {
            geoJSONIntersectionBoundsLayerId,
          }),
        }),
      );
    }
    yield put(
      layerActions.toggleFeatureMode({
        layerId: drawingTool.geoJSONLayerId,
        isInEditMode: newDrawMode.isSelectable,
        drawMode: newDrawMode.value,
      }),
    );
  } catch (error) {
    // eslint-disable-next-line no-console
    console.log('error changeDrawToolSaga', error);
  }
}

function* drawingSaga(): SagaIterator {
  yield takeLatest(drawtoolActions.registerDrawTool, registerDrawToolSaga);
  yield takeLatest(drawtoolActions.changeDrawToolMode, changeDrawToolSaga);
}

export default drawingSaga;
