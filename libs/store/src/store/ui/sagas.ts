/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

/* eslint-disable require-yield */
import { takeLatest, select, put } from 'redux-saga/effects';
import { SagaIterator } from 'redux-saga';

import { uiActions } from './reducer';
import * as uiSelectors from './selectors';
import { mapActions, mapSelectors } from '../mapStore/map';

export function* registerMapUISaga(): SagaIterator {
  const fields = yield select(mapSelectors.getAllMapIds);
  if (fields.length === 1) {
    // if first map is in store, we open the legend for it
    const source = yield select(uiSelectors.getDialogSource, 'legend');
    yield put(
      uiActions.setActiveMapIdForDialog({
        type: 'legend',
        mapId: fields[0],
        setOpen: true,
        source,
      }),
    );
  }
}

export function* unregisterUIMapSaga(): SagaIterator {
  const fields = yield select(mapSelectors.getAllMapIds);
  const activeUI = yield select(uiSelectors.getDialogDetailsByType, 'legend');
  if (fields.length && activeUI && !fields.includes(activeUI.activeMapId)) {
    // if active map is removed, we set activeMapId to the first mapId in the store
    yield put(
      uiActions.setActiveMapIdForDialog({
        type: 'legend',
        mapId: fields[0],
        setOpen: activeUI.isOpen,
      }),
    );
  } else if (!fields.length) {
    // if no more maps, we hide the legend
    yield put(
      uiActions.setToggleOpenDialog({
        type: 'legend',
        setOpen: false,
      }),
    );
  }
}

export function* rootSaga(): SagaIterator {
  yield takeLatest(mapActions.registerMap.type, registerMapUISaga);
  yield takeLatest(mapActions.unregisterMap.type, unregisterUIMapSaga);
}

export default rootSaga;
