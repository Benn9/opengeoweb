/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */
import { useCallback, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import {
  uiTypes,
  uiActions,
  CoreAppStore,
  uiSelectors,
  mapSelectors,
} from '../../store';

export interface SetupDialogReturnValue {
  setDialogOrder: () => void;
  dialogOrder: number;
  onCloseDialog: () => void;
  uiSource: uiTypes.Source;
  isDialogOpen: boolean;
  uiIsLoading: boolean;
  uiError: string;
  setFocused: (focused: boolean) => void;
}

export const useSetupDialog = (
  dialogType: uiTypes.DialogType,
  source: uiTypes.Source = 'app',
): SetupDialogReturnValue => {
  const dispatch = useDispatch();

  const onCloseDialog = useCallback((): void => {
    dispatch(
      uiActions.setToggleOpenDialog({
        type: dialogType,
        setOpen: false,
      }),
    );
  }, [dialogType, dispatch]);

  const mapId = useSelector((store: CoreAppStore) =>
    uiSelectors.getDialogMapId(store, dialogType),
  );

  const isMapPresent = useSelector((store: CoreAppStore) =>
    mapSelectors.getIsMapPresent(store, mapId),
  );

  // Check to ensure the currently active map is still present on screen - if not, close the dialog
  useEffect(() => {
    if (mapId !== '' && !isMapPresent) {
      onCloseDialog();
    }
  }, [mapId, isMapPresent, onCloseDialog]);

  const registerDialog = useCallback((): void => {
    dispatch(
      uiActions.registerDialog({
        type: dialogType,
        setOpen: false,
        source,
      }),
    );
  }, [dialogType, dispatch, source]);

  const unregisterDialog = useCallback((): void => {
    dispatch(
      uiActions.unregisterDialog({
        type: dialogType,
      }),
    );
  }, [dialogType, dispatch]);

  // Register this dialog in the store
  useEffect(() => {
    registerDialog();
    return (): void => {
      unregisterDialog();
    };
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const uiIsOrderedOnTop = useSelector((store: CoreAppStore) =>
    uiSelectors.getDialogIsOrderedOnTop(store, dialogType),
  );
  const setDialogOrder = useCallback((): void => {
    if (!uiIsOrderedOnTop) {
      dispatch(
        uiActions.orderDialog({
          type: dialogType,
        }),
      );
    }
  }, [dialogType, dispatch, uiIsOrderedOnTop]);

  const dialogOrder: number = useSelector((store: CoreAppStore) =>
    uiSelectors.getDialogOrder(store, dialogType),
  );

  const uiSource = useSelector((store: CoreAppStore) =>
    uiSelectors.getDialogSource(store, dialogType),
  );

  const isDialogOpen = useSelector((store: CoreAppStore) =>
    uiSelectors.getisDialogOpen(store, dialogType),
  );

  const uiIsLoading = useSelector((store: CoreAppStore) =>
    uiSelectors.getDialogIsLoading(store, dialogType),
  );

  const uiError = useSelector((store: CoreAppStore) =>
    uiSelectors.getDialogError(store, dialogType),
  );

  const setFocused = (focused: boolean): void => {
    dispatch(
      uiActions.setDialogFocused({
        type: dialogType,
        focused,
      }),
    );
  };

  return {
    setDialogOrder,
    dialogOrder,
    onCloseDialog,
    uiSource,
    isDialogOpen,
    uiIsLoading,
    uiError,
    setFocused,
  };
};
