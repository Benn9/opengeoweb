/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */
import * as utils from '@opengeoweb/api';
import { CreateApiProps, createFakeApiInstance } from '@opengeoweb/api';
import { LayerType } from '@opengeoweb/webmap';
import { createApi, getIdFromUrl } from './api';
import {
  WorkspacePreset,
  WorkspacePresetFromBE,
} from '../store/workspace/types';
import { ViewPreset } from '../store/viewPresets/types';

describe('src/utils/api', () => {
  const fakeAxiosInstance = createFakeApiInstance();
  const fakeApiParams: CreateApiProps = {
    config: {
      baseURL: 'fakeURL',
      appURL: 'fakeUrl',
      authTokenURL: 'anotherFakeUrl',
      authClientId: 'fakeauthClientId',
    },
    auth: {
      username: 'Michael Jackson',
      token: '1223344',
      refresh_token: '33455214',
    },
    onSetAuth: jest.fn(),
  };
  const nonAuthFakeApiParams: CreateApiProps = {
    ...fakeApiParams,
    auth: undefined,
  };

  describe('getIdFromUrl', () => {
    it('should an id from url', () => {
      expect(getIdFromUrl(null!)).toBeNull();
      expect(
        getIdFromUrl(
          'https://0.0.0.1:8081/viewpreset/bcf71f54-0da9-11ed-b5bd-0644f2de783d',
        ),
      ).toEqual('bcf71f54-0da9-11ed-b5bd-0644f2de783d');
      expect(
        getIdFromUrl(
          'https://0.0.0.1:8081/bcf71f54-0da9-11ed-b5bd-0644f2de783d',
        ),
      ).toEqual('bcf71f54-0da9-11ed-b5bd-0644f2de783d');
      expect(
        getIdFromUrl(
          'https://0.0.0.1:8081/viewpreset/acf71f54-xs90-11ed-b5bd-0644f2de783d',
        ),
      ).toEqual('acf71f54-xs90-11ed-b5bd-0644f2de783d');
    });
  });
  describe('createApi', () => {
    it('should contain all api calls', async () => {
      const api = createApi(fakeApiParams);
      expect(api.getWorkspacePresets).toBeTruthy();
      expect(api.getWorkspacePreset).toBeTruthy();
      expect(api.saveWorkspacePreset).toBeTruthy();
      expect(api.saveWorkspacePresetAs).toBeTruthy();
      expect(api.deleteWorkspacePreset).toBeTruthy();
      expect(api.getViewPresets).toBeTruthy();
      expect(api.getViewPreset).toBeTruthy();
      expect(api.saveViewPreset).toBeTruthy();
      expect(api.saveViewPresetAs).toBeTruthy();
      expect(api.deleteViewPreset).toBeTruthy();
    });

    it('should call with the right params for getWorkspacePresets if no scope passed', async () => {
      jest
        .spyOn(utils, 'createApiInstance')
        .mockReturnValueOnce(fakeAxiosInstance);
      const spy = jest.spyOn(fakeAxiosInstance, 'get');
      const api = createApi(fakeApiParams);

      const params = {};
      await api.getWorkspacePresets(params);
      expect(spy).toHaveBeenCalledWith('/workspacepreset', { params });
    });

    it('should call with the right params for getWorkspacePresets if one scope passed', async () => {
      jest
        .spyOn(utils, 'createApiInstance')
        .mockReturnValueOnce(fakeAxiosInstance);
      const spy = jest.spyOn(fakeAxiosInstance, 'get');
      const api = createApi(fakeApiParams);

      const params = { scope: 'system' };
      await api.getWorkspacePresets(params);
      expect(spy).toHaveBeenCalledWith('/workspacepreset', { params });
    });

    it('should call with the right params for getWorkspacePresets if multiple scopes passed', async () => {
      jest
        .spyOn(utils, 'createApiInstance')
        .mockReturnValueOnce(fakeAxiosInstance);
      const spy = jest.spyOn(fakeAxiosInstance, 'get');
      const api = createApi(fakeApiParams);

      const params = { scope: 'system,user' };
      await api.getWorkspacePresets(params);
      expect(spy).toHaveBeenCalledWith('/workspacepreset', { params });
    });

    it('should call with the right params for getWorkspacePreset', async () => {
      jest
        .spyOn(utils, 'createApiInstance')
        .mockReturnValueOnce(fakeAxiosInstance);
      const spy = jest.spyOn(fakeAxiosInstance, 'get');
      const api = createApi(fakeApiParams);

      const presetId = 'test-1';
      await api.getWorkspacePreset(presetId);
      expect(spy).toHaveBeenCalledWith(`/workspacepreset/${presetId}`);
    });

    it('should call with the right params for saveWorkspacePreset', async () => {
      jest
        .spyOn(utils, 'createApiInstance')
        .mockReturnValueOnce(fakeAxiosInstance);
      const spy = jest.spyOn(fakeAxiosInstance, 'put');
      const api = createApi(fakeApiParams);

      const workspacePreset: WorkspacePresetFromBE = {
        id: 'screenConfigRadarTemp',
        title: 'Radar and temperature',
        views: [
          {
            mosaicNodeId: 'radarA',
            viewPresetId: 'radar',
          },
          { mosaicNodeId: 'tempB', viewPresetId: 'temp' },
        ],
        syncGroups: [
          { id: 'Area_RadarTemp', type: 'SYNCGROUPS_TYPE_SETBBOX' },
          { id: 'Time_RadarTemp', type: 'SYNCGROUPS_TYPE_SETTIME' },
        ],
        mosaicNode: {
          direction: 'row',
          first: 'radarA',
          second: 'tempB',
          splitPercentage: 50,
        },
        scope: 'system',
        abstract: 'This can contain an abstract',
      };
      await api.saveWorkspacePreset(workspacePreset.id!, workspacePreset);
      expect(spy).toHaveBeenCalledWith(
        `/workspacepreset/${workspacePreset.id}/`,
        workspacePreset,
      );
    });

    it('should call with the right params for saveWorkspacePresetAs', async () => {
      jest
        .spyOn(utils, 'createApiInstance')
        .mockReturnValueOnce(fakeAxiosInstance);
      const spy = jest.spyOn(fakeAxiosInstance, 'post');
      const api = createApi(fakeApiParams);

      const workspacePreset: WorkspacePreset = {
        title: 'Radar and temperature',
        views: {
          allIds: ['radar', 'temp'],
        },
        syncGroups: [
          { id: 'Area_RadarTemp', type: 'SYNCGROUPS_TYPE_SETBBOX' },
          { id: 'Time_RadarTemp', type: 'SYNCGROUPS_TYPE_SETTIME' },
        ],
        mosaicNode: {
          direction: 'row',
          first: 'radar',
          second: 'temp',
          splitPercentage: 50,
        },
        scope: 'system',
        abstract: 'This can contain an abstract',
      };
      await api.saveWorkspacePresetAs(workspacePreset);
      expect(spy).toHaveBeenCalledWith(`/workspacepreset/`, workspacePreset);
    });

    it('should call with the right params for deleteWorkspacePreset', async () => {
      jest
        .spyOn(utils, 'createApiInstance')
        .mockReturnValueOnce(fakeAxiosInstance);
      const spy = jest.spyOn(fakeAxiosInstance, 'delete');
      const api = createApi(fakeApiParams);

      const presetId = 'test-1';
      await api.deleteWorkspacePreset(presetId);
      expect(spy).toHaveBeenCalledWith(`/workspacepreset/${presetId}`);
    });

    it('should call with the right params for getViewPresets if no scope passed', async () => {
      jest
        .spyOn(utils, 'createApiInstance')
        .mockReturnValueOnce(fakeAxiosInstance);
      const spy = jest.spyOn(fakeAxiosInstance, 'get');
      const api = createApi(fakeApiParams);

      const params = {};
      await api.getViewPresets(params);
      expect(spy).toHaveBeenCalledWith('/viewpreset', { params });
    });

    it('should call with the right params for getViewPresets if one scope passed', async () => {
      jest
        .spyOn(utils, 'createApiInstance')
        .mockReturnValueOnce(fakeAxiosInstance);
      const spy = jest.spyOn(fakeAxiosInstance, 'get');
      const api = createApi(fakeApiParams);

      const params = { scope: 'system' };
      await api.getViewPresets(params);
      expect(spy).toHaveBeenCalledWith('/viewpreset', { params });
    });

    it('should call with the right params for getViewPresets if multiple scopes passed', async () => {
      jest
        .spyOn(utils, 'createApiInstance')
        .mockReturnValueOnce(fakeAxiosInstance);
      const spy = jest.spyOn(fakeAxiosInstance, 'get');
      const api = createApi(fakeApiParams);

      const params = { scope: 'system,user' };
      await api.getViewPresets(params);
      expect(spy).toHaveBeenCalledWith('/viewpreset', { params });
    });

    it('should call with the right params for getViewPreset', async () => {
      jest
        .spyOn(utils, 'createApiInstance')
        .mockReturnValueOnce(fakeAxiosInstance);
      const spy = jest.spyOn(fakeAxiosInstance, 'get');
      const api = createApi(fakeApiParams);

      const presetId = 'test-1';
      await api.getViewPreset(presetId);
      expect(spy).toHaveBeenCalledWith(`/viewpreset/${presetId}`);
    });

    it('should call with the right params for saveViewPreset', async () => {
      jest
        .spyOn(utils, 'createApiInstance')
        .mockReturnValueOnce(fakeAxiosInstance);
      const spy = jest.spyOn(fakeAxiosInstance, 'put');
      const api = createApi(fakeApiParams);

      const preset: ViewPreset = {
        componentType: 'Map',
        id: 'Radar',
        keywords: 'WMS,Radar',
        title: 'My new radar',
        initialProps: {
          mapPreset: {
            layers: [
              {
                id: 'base-layer-1',
                name: 'OpenStreetMap_NL',
                type: 'twms',
                layerType: LayerType.baseLayer,
                enabled: true,
              },
              {
                enabled: true,
                format: 'image/png',
                layerType: LayerType.mapLayer,
                name: 'RAD_NL25_PCP_CM',
                service: 'https://geoservices.knmi.nl/wms?dataset=RADAR&',
                style: 'precip-blue-transparent/nearest',
              },
            ],
            proj: {
              bbox: {
                bottom: 6408480.4514,
                left: 58703.6377,
                right: 3967387.5161,
                top: 11520588.9031,
              },
              srs: 'EPSG:3857',
            },
            showTimeSlider: true,
          },
        },
      };

      await api.saveViewPreset(preset.id!, preset);
      expect(spy).toHaveBeenCalledWith(`/viewpreset/${preset.id}/`, preset);
    });

    it('should call with the right params for saveViewPresetAs', async () => {
      jest
        .spyOn(utils, 'createApiInstance')
        .mockReturnValueOnce(fakeAxiosInstance);
      const spy = jest.spyOn(fakeAxiosInstance, 'post');
      const api = createApi(fakeApiParams);

      const preset: ViewPreset = {
        componentType: 'Map',
        id: 'Radar',
        keywords: 'WMS,Radar',
        title: 'My new radar',
        initialProps: {
          mapPreset: {
            layers: [
              {
                id: 'base-layer-1',
                name: 'OpenStreetMap_NL',
                type: 'twms',
                layerType: LayerType.baseLayer,
                enabled: true,
              },
              {
                enabled: true,
                format: 'image/png',
                layerType: LayerType.mapLayer,
                name: 'RAD_NL25_PCP_CM',
                service: 'https://geoservices.knmi.nl/wms?dataset=RADAR&',
                style: 'precip-blue-transparent/nearest',
              },
            ],
            proj: {
              bbox: {
                bottom: 6408480.4514,
                left: 58703.6377,
                right: 3967387.5161,
                top: 11520588.9031,
              },
              srs: 'EPSG:3857',
            },
            showTimeSlider: true,
          },
        },
      };

      await api.saveViewPresetAs(preset);
      expect(spy).toHaveBeenCalledWith(`/viewpreset/`, preset);
    });

    it('should use non auth axios instance when user is unauthenticated', async () => {
      jest
        .spyOn(utils, 'createNonAuthApiInstance')
        .mockReturnValueOnce(fakeAxiosInstance);
      const spy = jest.spyOn(fakeAxiosInstance, 'get');
      const api = createApi(nonAuthFakeApiParams);

      const presetId = 'test-1';
      await api.getViewPreset(presetId);
      expect(spy).toHaveBeenCalledWith(`/viewpreset/${presetId}`);
    });
  });
});
