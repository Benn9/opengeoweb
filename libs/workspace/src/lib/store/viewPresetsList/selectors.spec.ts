/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */

import { AppStore } from '../store';
import * as viewPresetSelectors from './selectors';
import { ViewPresetListItem } from './types';

describe('store/viewPresetsList/selectors', () => {
  describe('getViewPresetsListStore', () => {
    it('should return the viewstore', () => {
      const mockStore: AppStore = {
        viewPresetsList: {
          entities: {},
          ids: [],
        },
      };

      expect(viewPresetSelectors.getViewPresetsListStore(mockStore)).toEqual(
        mockStore.viewPresetsList,
      );

      expect(viewPresetSelectors.getViewPresetsListStore(null!)).toBeNull();
    });
  });

  describe('getViewPresetsList', () => {
    it('should return viewPresetsList', () => {
      const mockStore: AppStore = {
        viewPresetsList: {
          entities: {
            preset: {
              id: 'mapPreset-1',
              scope: 'system',
              title: 'Layer manager preset',
              date: '2022-06-01T12:34:27.787192',
            } as ViewPresetListItem,
            preset_2: {
              id: 'mapPreset-2',
              scope: 'system',
              title: 'Layer manager preset 2',
              date: '2022-06-01T12:34:27.787192',
            } as ViewPresetListItem,
          },
          ids: ['preset', 'preset_2'],
        },
      };

      expect(viewPresetSelectors.getViewPresetsList(mockStore)).toEqual(
        mockStore.viewPresetsList?.ids.map(
          (id) => mockStore.viewPresetsList?.entities[id],
        ),
      );
    });

    it('should return empty list when no viewPresetsList in store', () => {
      const mockStore: AppStore = {
        viewPresetsList: {
          entities: {},
          ids: [],
        },
      };

      expect(viewPresetSelectors.getViewPresetsList(mockStore)).toEqual([]);
    });
  });

  describe('getFilteredViewPresetListForView', () => {
    it('should return general view presets list if no filtered results for panel', () => {
      const mockStore: AppStore = {
        viewPresetsList: {
          entities: {
            preset: {
              id: 'mapPreset-1',
              scope: 'system',
              title: 'Layer manager preset',
              date: '2022-06-01T12:34:27.787192',
            } as ViewPresetListItem,
            preset_2: {
              id: 'mapPreset-2',
              scope: 'system',
              title: 'Layer manager preset 2',
              date: '2022-06-01T12:34:27.787192',
            } as ViewPresetListItem,
          },
          ids: ['preset', 'preset_2'],
        },
        viewPresets: {
          entities: {
            'panel-1': {
              panelId: 'viewPreset-1',
              activeViewPresetId: 'preset-1',
              hasChanges: true,
              isFetching: false,
              error: undefined,
              isViewPresetListDialogOpen: true,
              filters: [],
              searchQuery: '',
              filterResults: { ids: [], entities: {} },
            },
            'panel-2': {
              panelId: 'viewPreset-2',
              activeViewPresetId: 'preset-2',
              hasChanges: false,
              isFetching: false,
              error: undefined,
              isViewPresetListDialogOpen: false,
              filters: [],
              searchQuery: '',
              filterResults: { ids: [], entities: {} },
            },
          },
          ids: ['panel-1', 'panel-2'],
        },
      };

      expect(
        viewPresetSelectors.getFilteredViewPresetListForView(
          mockStore,
          'panel-1',
        ),
      ).toEqual(
        mockStore.viewPresetsList?.ids.map(
          (id) => mockStore.viewPresetsList?.entities[id],
        ),
      );
      expect(
        viewPresetSelectors.getFilteredViewPresetListForView(
          mockStore,
          'panel-2',
        ),
      ).toEqual(
        mockStore.viewPresetsList?.ids.map(
          (id) => mockStore.viewPresetsList?.entities[id],
        ),
      );
    });

    it('should return filtered view presets list from filter results if set due to search', () => {
      const filterResultsPanel1 = {
        ids: ['view1'],
        entities: {
          view1: {
            id: 'view1',
            scope: 'system',
            title: 'Layer manager preset 2',
            date: '2022-06-01T12:34:27.787192',
          } as ViewPresetListItem,
        },
      };
      const filterResultsPanel2 = {
        ids: ['view1', 'view2'],
        entities: {
          view1: {
            id: 'view1',
            scope: 'system',
            title: 'Layer manager preset 2',
            date: '2022-06-01T12:34:27.787192',
          } as ViewPresetListItem,
          view2: {
            id: 'view2',
            scope: 'system',
            title: 'Layer manager preset 2',
            date: '2022-06-01T12:34:27.787192',
          } as ViewPresetListItem,
        },
      };
      const mockStore: AppStore = {
        viewPresetsList: {
          entities: {
            preset: {
              id: 'mapPreset-1',
              scope: 'system',
              title: 'Layer manager preset',
              date: '2022-06-01T12:34:27.787192',
            } as ViewPresetListItem,
            preset_2: {
              id: 'mapPreset-2',
              scope: 'system',
              title: 'Layer manager preset 2',
              date: '2022-06-01T12:34:27.787192',
            } as ViewPresetListItem,
          },
          ids: ['preset', 'preset_2'],
        },
        viewPresets: {
          entities: {
            'panel-1': {
              panelId: 'viewPreset-1',
              activeViewPresetId: 'preset-1',
              hasChanges: true,
              isFetching: false,
              error: undefined,
              isViewPresetListDialogOpen: true,
              filters: [
                {
                  label: 'My presets',
                  id: 'user',
                  type: 'scope',
                  isSelected: true,
                  isDisabled: false,
                },
                {
                  label: 'System presets',
                  id: 'system',
                  type: 'scope',
                  isSelected: true,
                  isDisabled: false,
                },
              ],
              searchQuery: '',
              filterResults: filterResultsPanel1,
            },
            'panel-2': {
              panelId: 'viewPreset-2',
              activeViewPresetId: 'preset-2',
              hasChanges: false,
              isFetching: false,
              error: undefined,
              isViewPresetListDialogOpen: false,
              filters: [],
              searchQuery: 'abcde',
              filterResults: filterResultsPanel2,
            },
          },
          ids: ['panel-1', 'panel-2'],
        },
      };

      // panel one has no search so should equal original list
      expect(
        viewPresetSelectors.getFilteredViewPresetListForView(
          mockStore,
          'panel-1',
        ),
      ).toEqual(
        mockStore.viewPresetsList?.ids.map(
          (id) => mockStore.viewPresetsList?.entities[id],
        ),
      );

      expect(
        viewPresetSelectors.getFilteredViewPresetListForView(
          mockStore,
          'panel-2',
        ),
      ).toEqual(
        mockStore.viewPresets?.entities['panel-2']?.filterResults.ids.map(
          (id) =>
            mockStore.viewPresets?.entities['panel-2']?.filterResults.entities[
              id
            ],
        ),
      );
    });

    it('should return filtered view presets list from filter results if set', () => {
      const filterResultsPanel1 = {
        ids: ['view1'],
        entities: {
          view1: {
            id: 'view1',
            scope: 'system',
            title: 'Layer manager preset 2',
            date: '2022-06-01T12:34:27.787192',
          } as ViewPresetListItem,
        },
      };
      const filterResultsPanel2 = {
        ids: ['view1', 'view2'],
        entities: {
          view1: {
            id: 'view1',
            scope: 'system',
            title: 'Layer manager preset 2',
            date: '2022-06-01T12:34:27.787192',
          } as ViewPresetListItem,
          view2: {
            id: 'view2',
            scope: 'system',
            title: 'Layer manager preset 2',
            date: '2022-06-01T12:34:27.787192',
          } as ViewPresetListItem,
        },
      };
      const mockStore: AppStore = {
        viewPresetsList: {
          entities: {
            preset: {
              id: 'mapPreset-1',
              scope: 'system',
              title: 'Layer manager preset',
              date: '2022-06-01T12:34:27.787192',
            } as ViewPresetListItem,
            preset_2: {
              id: 'mapPreset-2',
              scope: 'system',
              title: 'Layer manager preset 2',
              date: '2022-06-01T12:34:27.787192',
            } as ViewPresetListItem,
          },
          ids: ['preset', 'preset_2'],
        },
        viewPresets: {
          entities: {
            'panel-1': {
              panelId: 'viewPreset-1',
              activeViewPresetId: 'preset-1',
              hasChanges: true,
              isFetching: false,
              error: undefined,
              isViewPresetListDialogOpen: true,
              filters: [
                {
                  label: 'My presets',
                  id: 'user',
                  type: 'scope',
                  isSelected: true,
                  isDisabled: false,
                },
                {
                  label: 'System presets',
                  id: 'system',
                  type: 'scope',
                  isSelected: true,
                  isDisabled: false,
                },
              ],
              searchQuery: '',
              filterResults: filterResultsPanel1,
            },
            'panel-2': {
              panelId: 'viewPreset-2',
              activeViewPresetId: 'preset-2',
              hasChanges: false,
              isFetching: false,
              error: undefined,
              isViewPresetListDialogOpen: false,
              filters: [
                {
                  label: 'My presets',
                  id: 'user',
                  type: 'scope',
                  isSelected: true,
                  isDisabled: false,
                },
                {
                  label: 'System presets',
                  id: 'system',
                  type: 'scope',
                  isSelected: false,
                  isDisabled: false,
                },
              ],
              searchQuery: '',
              filterResults: filterResultsPanel2,
            },
          },
          ids: ['panel-1', 'panel-2'],
        },
      };

      // panel one has no filters set (all are selected) so should equal original list
      expect(
        viewPresetSelectors.getFilteredViewPresetListForView(
          mockStore,
          'panel-1',
        ),
      ).toEqual(
        mockStore.viewPresetsList?.ids.map(
          (id) => mockStore.viewPresetsList?.entities[id],
        ),
      );

      expect(
        viewPresetSelectors.getFilteredViewPresetListForView(
          mockStore,
          'panel-2',
        ),
      ).toEqual(
        mockStore.viewPresets?.entities['panel-2']?.filterResults.ids.map(
          (id) =>
            mockStore.viewPresets?.entities['panel-2']?.filterResults.entities[
              id
            ],
        ),
      );
    });

    it('should return filtered view presets list from filter results if set even if no results', () => {
      const mockStore: AppStore = {
        viewPresetsList: {
          entities: {
            preset: {
              id: 'mapPreset-1',
              scope: 'system',
              title: 'Layer manager preset',
              date: '2022-06-01T12:34:27.787192',
            } as ViewPresetListItem,
            preset_2: {
              id: 'mapPreset-2',
              scope: 'system',
              title: 'Layer manager preset 2',
              date: '2022-06-01T12:34:27.787192',
            } as ViewPresetListItem,
          },
          ids: ['preset', 'preset_2'],
        },
        viewPresets: {
          entities: {
            'panel-1': {
              panelId: 'viewPreset-1',
              activeViewPresetId: 'preset-1',
              hasChanges: true,
              isFetching: false,
              error: undefined,
              isViewPresetListDialogOpen: true,
              filters: [
                {
                  label: 'My presets',
                  id: 'user',
                  type: 'scope',
                  isSelected: true,
                  isDisabled: false,
                },
                {
                  label: 'System presets',
                  id: 'system',
                  type: 'scope',
                  isSelected: false,
                  isDisabled: false,
                },
              ],
              searchQuery: '',
              filterResults: { ids: [], entities: {} },
            },
          },
          ids: ['panel-1'],
        },
      };

      expect(
        viewPresetSelectors.getFilteredViewPresetListForView(
          mockStore,
          'panel-1',
        ),
      ).toEqual([]);
    });

    it('should return empty list when no viewPresetsList in store', () => {
      const mockStore: AppStore = {
        viewPresetsList: {
          entities: {},
          ids: [],
        },
      };

      expect(viewPresetSelectors.getViewPresetsList(mockStore)).toEqual([]);
    });
  });
});
