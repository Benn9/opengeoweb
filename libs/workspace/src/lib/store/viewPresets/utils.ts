/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */

import { getApi } from '@opengeoweb/api';
import { API_NAME, PresetsApi } from '../../utils/api';
import { PresetAction, ViewPresetsListFilter } from './types';
import { FetchViewPresetsListParams } from '../viewPresetsList/types';
import { getCleanSearchQuery } from '../../utils/utils';

export const MAPPRESET_ACTIVE_TITLE = 'New map preset';
export const MAPPRESET_DIALOG_TITLE_SAVE_AS = 'Save map preset as';
export const MAPPRESET_DIALOG_TITLE_DELETE = 'Delete map preset';
export const MAPPRESET_DIALOG_TITLE_EDIT = 'Edit map preset';

export const getSnackbarMessage = (
  action: PresetAction,
  title: string,
): string => {
  // Trim and add quotes
  const trimmedTitle = `"${title.trim()}"`;
  switch (action) {
    case PresetAction.DELETE:
      return `Map preset ${trimmedTitle} successfully deleted`;
    case PresetAction.SAVE_AS:
      return `New map preset ${trimmedTitle} successfully saved`;
    case PresetAction.EDIT:
      return `Map preset ${trimmedTitle} successfully edited`;
    default:
      return `Map preset ${trimmedTitle} successfully saved`;
  }
};

export const getPresetsApi = (): PresetsApi => getApi<PresetsApi>(API_NAME);

export const emptyMapViewPreset = {
  componentType: 'Map' as const,
  keywords: '',
  scope: 'user' as const,
  title: '',
  initialProps: {
    mapPreset: {},
  },
};

// Returns object of applied user filters such as scope/search etc
// If all filter chips are selected, an empty string is returned for that filter which is interpreted in the BE as "return all" _
export const constructFilterParams = (
  filters: ViewPresetsListFilter[],
  searchQuery: string,
): FetchViewPresetsListParams => {
  // get filters
  const isAllSelectedForFilters = filters.every(
    (filter: ViewPresetsListFilter) => filter.isSelected,
  );
  // If all filters are selected, ensure we return nothing for scope
  const { scope } = isAllSelectedForFilters
    ? { scope: [] }
    : filters.reduce<{ scope: string[] }>(
        (filterList, filter) => {
          if (filter.isSelected && filter.type === 'scope') {
            return {
              ...filter,
              scope: filterList.scope.concat(filter.id),
            };
          }
          return filterList;
        },
        { scope: [] },
      );

  // get search
  const search = getCleanSearchQuery(searchQuery);

  return {
    ...(scope.length && {
      scope: scope.toString(),
    }),
    ...(search.length && {
      search,
    }),
  };
};
