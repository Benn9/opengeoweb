/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */

import { call, put, select, takeLatest, takeLeading } from 'redux-saga/effects';
import { mapActions, mapSelectors } from '@opengeoweb/store';
import { snackbarActions } from '@opengeoweb/snackbar';
import {
  fetchInitialViewPresetsSaga,
  fetchViewPresetsSaga,
  MESSAGE_SAVE_PRESET,
  onSuccessViewPresetActionSaga,
  rootSaga,
  saveViewPresetSaga,
  selectViewPresetSaga,
} from './sagas';
import { viewPresetActions, viewPresetSelectors } from '.';
import { createApi as createFakeApi } from '../../utils/fakeApi';
import { emptyMapViewPreset, getPresetsApi, getSnackbarMessage } from './utils';
import {
  ViewPresetType,
  PresetAction,
  SaveViewPresetPayload,
  SelectViewPresetPayload,
} from './types';
import { workspaceActions } from '../workspace/reducer';

describe('fetchedInitialViewPresets', () => {
  it('should fetch initial view presets', () => {
    const generator = fetchInitialViewPresetsSaga();

    const mockApi = createFakeApi();
    const presetsApi = generator.next().value;

    expect(presetsApi).toEqual(call(getPresetsApi));
    const apiResult = generator.next(mockApi).value;
    expect(apiResult).toEqual(call(mockApi.getViewPresets));
    const mockResult = [
      {
        id: 'Radar',
        date: '2022-06-01T12:34:27.787184',
        title: 'Radar',
        scope: 'user' as const,
      },
      {
        id: 'Temperature',
        date: '2022-06-01T12:34:27.787192',
        title: 'Temperature',
        scope: 'system' as const,
      },
    ];
    expect(generator.next({ data: mockResult }).value).toEqual(
      put(
        viewPresetActions.fetchedInitialViewPresets({
          viewPresets: mockResult,
        }),
      ),
    );

    expect(generator.next().done).toBeTruthy();
  });

  it('should trigger error when fetching fails', () => {
    const generator = fetchInitialViewPresetsSaga();

    const mockApi = createFakeApi();
    const presetsApi = generator.next().value;
    expect(presetsApi).toEqual(call(getPresetsApi));
    const apiResult = generator.next(mockApi).value;
    expect(apiResult).toEqual(call(mockApi.getViewPresets));
    expect(generator.next().value).toEqual(
      put(
        viewPresetActions.errorInitialViewPresets({
          error: {
            message: 'Error fetching map preset list',
            type: ViewPresetType.PRESET_LIST,
          },
        }),
      ),
    );
    expect(generator.next().done).toBeTruthy();
  });
});

describe('fetchMapPresetsSaga', () => {
  it('should fetch view presets', () => {
    const panelId = 'test';
    const generator = fetchViewPresetsSaga(
      viewPresetActions.fetchViewPresets({
        panelId,
        filterParams: { scope: 'user,system' },
      }),
    );

    const mockApi = createFakeApi();
    const presetsApi = generator.next().value;
    expect(presetsApi).toEqual(call(getPresetsApi));
    const apiResult = generator.next(mockApi).value;
    expect(apiResult).toEqual(
      call(mockApi.getViewPresets, { scope: 'user,system' }),
    );
    const mockResult = [
      {
        id: 'Radar',
        date: '2022-06-01T12:34:27.787184',
        title: 'Radar',
        scope: 'user' as const,
      },
      {
        id: 'Temperature',
        date: '2022-06-01T12:34:27.787192',
        title: 'Temperature',
        scope: 'system' as const,
      },
    ];
    expect(generator.next({ data: mockResult }).value).toEqual(
      put(
        viewPresetActions.fetchedViewPresets({
          viewPresets: mockResult,
          panelId,
          filterParams: { scope: 'user,system' },
        }),
      ),
    );

    expect(generator.next().done).toBeTruthy();
  });

  it('should trigger error when fetching fails', () => {
    const panelId = 'test';
    const generator = fetchViewPresetsSaga(
      viewPresetActions.fetchViewPresets({
        panelId,
        filterParams: { scope: 'user,system' },
      }),
    );

    const mockApi = createFakeApi();
    const presetsApi = generator.next().value;
    expect(presetsApi).toEqual(call(getPresetsApi));
    const apiResult = generator.next(mockApi).value;
    expect(apiResult).toEqual(
      call(mockApi.getViewPresets, { scope: 'user,system' }),
    );
    expect(generator.next().value).toEqual(
      put(
        viewPresetActions.errorViewPreset({
          error: {
            message: 'Error fetching map preset list',
            type: ViewPresetType.PRESET_LIST,
          },
          panelId,
        }),
      ),
    );
    expect(generator.next().done).toBeTruthy();
  });
});

describe('saveViewPresetSaga', () => {
  it('should save view preset', () => {
    const payload: SaveViewPresetPayload = {
      panelId: 'test-1',
      viewPreset: {
        title: 'test preset',
        componentType: 'Map',
        initialProps: {
          mapPreset: {
            layers: [],
          },
        },
        keywords: 'key,key2',
      },
      viewPresetId: 'preset-id-1',
    };
    const generator = saveViewPresetSaga(
      viewPresetActions.saveViewPreset(payload),
    );

    const mockApi = createFakeApi();
    const presetsApi = generator.next().value;
    expect(presetsApi).toEqual(call(getPresetsApi));
    const apiResult = generator.next(mockApi).value;
    expect(apiResult).toEqual(
      call(mockApi.saveViewPreset, payload.viewPresetId, payload.viewPreset),
    );

    expect(generator.next().value).toEqual(
      put(
        snackbarActions.openSnackbar({
          message: getSnackbarMessage(
            PresetAction.SAVE,
            payload.viewPreset.title,
          ),
        }),
      ),
    );

    expect(generator.next().value).toEqual(
      put(
        viewPresetActions.savedViewPreset({
          panelId: payload.panelId,
          viewPresetId: payload.viewPresetId,
        }),
      ),
    );

    expect(generator.next().done).toBeTruthy();
  });

  it('should trigger error when save fails', () => {
    const payload: SaveViewPresetPayload = {
      panelId: 'test-1',
      viewPreset: {
        title: 'test preset',
        componentType: 'Map',
        initialProps: {
          mapPreset: {
            layers: [],
          },
        },
        keywords: 'key,key2',
      },
      viewPresetId: 'preset-id-1',
    };
    const generator = saveViewPresetSaga(
      viewPresetActions.saveViewPreset(payload),
    );

    const presetsApi = generator.next().value;
    expect(presetsApi).toEqual(call(getPresetsApi));

    expect(generator.next(() => new Error('trigger be error')).value).toEqual(
      put(
        viewPresetActions.errorViewPreset({
          error: {
            message: MESSAGE_SAVE_PRESET,
            type: ViewPresetType.PRESET_DETAIL,
          },
          panelId: payload.panelId,
          viewPresetId: payload.viewPresetId,
        }),
      ),
    );
    expect(generator.next().done).toBeTruthy();
  });
});

describe('selectViewPresetSaga', () => {
  it('should select an existing preset', () => {
    const payload: SelectViewPresetPayload = {
      panelId: 'test-1',
      viewPresetId: 'preset-id-1',
    };
    const generator = selectViewPresetSaga(
      viewPresetActions.selectViewPreset(payload),
    );

    expect(generator.next().value).toEqual(
      select(mapSelectors.isAnimating, payload.panelId),
    );

    expect(generator.next().value).toEqual(
      put(
        workspaceActions.fetchWorkspaceViewPreset({
          mosaicNodeId: payload.panelId,
          viewPresetId: payload.viewPresetId,
        }),
      ),
    );

    expect(generator.next().done).toBeTruthy();
  });

  it('should select a new preset', () => {
    const payload: SelectViewPresetPayload = {
      panelId: 'test-1',
      viewPresetId: '',
    };
    const generator = selectViewPresetSaga(
      viewPresetActions.selectViewPreset(payload),
    );

    expect(generator.next().value).toEqual(
      select(mapSelectors.isAnimating, payload.panelId),
    );

    expect(generator.next().value).toEqual(
      put(
        mapActions.setMapPreset({
          mapId: payload.panelId,
          initialProps: emptyMapViewPreset.initialProps,
        }),
      ),
    );

    expect(generator.next().value).toEqual(
      put(
        workspaceActions.updateWorkspaceView({
          viewPreset: emptyMapViewPreset,
          mosaicNodeId: payload.panelId,
        }),
      ),
    );

    expect(generator.next().value).toEqual(
      put(
        viewPresetActions.fetchedViewPreset({
          panelId: payload.panelId,
          viewPresetId: payload.viewPresetId,
          viewPreset: emptyMapViewPreset,
        }),
      ),
    );

    expect(generator.next().done).toBeTruthy();
  });

  it('should stop animating map if map is animating after selecting a preset', () => {
    const payload: SelectViewPresetPayload = {
      panelId: 'test-1',
      viewPresetId: 'preset-id-1',
    };
    const generator = selectViewPresetSaga(
      viewPresetActions.selectViewPreset(payload),
    );

    expect(generator.next().value).toEqual(
      select(mapSelectors.isAnimating, payload.panelId),
    );

    expect(generator.next(true).value).toEqual(
      put(mapActions.mapStopAnimation({ mapId: payload.panelId })),
    );

    expect(generator.next().value).toEqual(
      put(
        workspaceActions.fetchWorkspaceViewPreset({
          mosaicNodeId: payload.panelId,
          viewPresetId: payload.viewPresetId,
        }),
      ),
    );

    expect(generator.next().done).toBeTruthy();
  });
});

describe('onSuccessViewPresetActionSaga', () => {
  it('should handle success for delete action', () => {
    const payload = {
      action: PresetAction.DELETE,
      viewPresetId: 'preset-1',
      title: 'test preset',
      panelId: 'map-1',
    };
    const generator = onSuccessViewPresetActionSaga(
      viewPresetActions.onSuccessViewPresetAction(payload),
    );

    expect(generator.next().value).toEqual(
      put(
        snackbarActions.openSnackbar({
          message: getSnackbarMessage(payload.action, payload.title),
        }),
      ),
    );

    expect(generator.next().value).toEqual(
      select(viewPresetSelectors.getViewPresetActiveId, payload.panelId),
    );

    expect(generator.next(payload.panelId).value).toEqual(
      select(
        viewPresetSelectors.getViewPresetListFiltersForView,
        payload.panelId,
      ),
    );

    expect(generator.next([]).value).toEqual(
      select(
        viewPresetSelectors.getViewPresetListSearchQueryForView,
        payload.panelId,
      ),
    );

    expect(generator.next('').value).toEqual(
      call(
        fetchViewPresetsSaga,
        viewPresetActions.fetchViewPresets({
          panelId: payload.panelId,
          filterParams: {},
        }),
      ),
    );

    expect(generator.next().value).toEqual(
      put(
        viewPresetActions.setViewPresetHasChanges({
          panelId: payload.panelId,
          hasChanges: false,
        }),
      ),
    );

    expect(generator.next().done).toBeTruthy();
  });

  it('should select new preset after deleting active view preset', () => {
    const payload = {
      action: PresetAction.DELETE,
      viewPresetId: 'preset-1',
      title: 'test preset',
      panelId: 'map-1',
    };
    const generator = onSuccessViewPresetActionSaga(
      viewPresetActions.onSuccessViewPresetAction(payload),
    );

    expect(generator.next().value).toEqual(
      put(
        snackbarActions.openSnackbar({
          message: getSnackbarMessage(payload.action, payload.title),
        }),
      ),
    );

    expect(generator.next().value).toEqual(
      select(viewPresetSelectors.getViewPresetActiveId, payload.panelId),
    );

    expect(generator.next(payload.viewPresetId).value).toEqual(
      put(
        viewPresetActions.setActiveViewPresetId({
          panelId: payload.panelId,
          viewPresetId: '',
        }),
      ),
    );

    expect(generator.next(payload.panelId).value).toEqual(
      select(
        viewPresetSelectors.getViewPresetListFiltersForView,
        payload.panelId,
      ),
    );

    expect(generator.next([]).value).toEqual(
      select(
        viewPresetSelectors.getViewPresetListSearchQueryForView,
        payload.panelId,
      ),
    );

    expect(generator.next('').value).toEqual(
      call(
        fetchViewPresetsSaga,
        viewPresetActions.fetchViewPresets({
          panelId: payload.panelId,
          filterParams: {},
        }),
      ),
    );

    expect(generator.next().value).toEqual(
      put(
        viewPresetActions.setViewPresetHasChanges({
          panelId: payload.panelId,
          hasChanges: false,
        }),
      ),
    );

    expect(generator.next().done).toBeTruthy();
  });

  it('should handle success for save as action', () => {
    const payload = {
      action: PresetAction.SAVE_AS,
      viewPresetId: 'preset-1',
      title: 'test preset',
      panelId: 'map-1',
    };
    const generator = onSuccessViewPresetActionSaga(
      viewPresetActions.onSuccessViewPresetAction(payload),
    );

    expect(generator.next().value).toEqual(
      put(
        snackbarActions.openSnackbar({
          message: getSnackbarMessage(payload.action, payload.title),
        }),
      ),
    );

    expect(generator.next().value).toEqual(
      select(viewPresetSelectors.getViewPresetActiveId, payload.panelId),
    );

    expect(generator.next(payload.panelId).value).toEqual(
      select(
        viewPresetSelectors.getViewPresetListFiltersForView,
        payload.panelId,
      ),
    );

    expect(generator.next([]).value).toEqual(
      select(
        viewPresetSelectors.getViewPresetListSearchQueryForView,
        payload.panelId,
      ),
    );

    expect(generator.next('').value).toEqual(
      call(
        fetchViewPresetsSaga,
        viewPresetActions.fetchViewPresets({
          panelId: payload.panelId,
          filterParams: {},
        }),
      ),
    );

    expect(generator.next().value).toEqual(
      put(
        viewPresetActions.setViewPresetHasChanges({
          panelId: payload.panelId,
          hasChanges: false,
        }),
      ),
    );

    expect(generator.next().value).toEqual(
      put(
        viewPresetActions.setActiveViewPresetId({
          panelId: payload.panelId,
          viewPresetId: payload.viewPresetId,
        }),
      ),
    );

    expect(generator.next().done).toBeTruthy();
  });
});

describe('rootSaga', () => {
  it('should catch rootSaga actions and fire corresponding sagas', () => {
    const generator = rootSaga();
    expect(generator.next().value).toEqual(
      takeLeading(
        viewPresetActions.fetchInitialViewPresets,
        fetchInitialViewPresetsSaga,
      ),
    );
    expect(generator.next().value).toEqual(
      takeLatest(viewPresetActions.fetchViewPresets, fetchViewPresetsSaga),
    );

    expect(generator.next().value).toEqual(
      takeLatest(viewPresetActions.saveViewPreset, saveViewPresetSaga),
    );

    expect(generator.next().value).toEqual(
      takeLatest(viewPresetActions.selectViewPreset, selectViewPresetSaga),
    );

    expect(generator.next().value).toEqual(
      takeLatest(
        viewPresetActions.onSuccessViewPresetAction,
        onSuccessViewPresetActionSaga,
      ),
    );

    expect(generator.next().done).toBeTruthy();
  });
});
