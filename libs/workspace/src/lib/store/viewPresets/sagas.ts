/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */

import { put, select, call, takeLatest, takeLeading } from 'redux-saga/effects';
import { SagaIterator } from 'redux-saga';
import { snackbarActions } from '@opengeoweb/snackbar';
import { mapStoreActions, mapSelectors } from '@opengeoweb/store';
import { viewPresetActions } from './reducer';
import * as mapPresetSelectors from './selectors';
import { ViewPresetType, PresetAction } from './types';
import {
  constructFilterParams,
  emptyMapViewPreset,
  getPresetsApi,
  getSnackbarMessage,
} from './utils';
import { workspaceActions } from '../workspace/reducer';
import { viewPresetSelectors } from '.';

export const MESSAGE_FETCH_PRESET =
  'Preset could not be loaded: Select a different one or try again.';
export const MESSAGE_SAVE_PRESET =
  'Map preset not saved: an error occurred during saving. Please try again later.';

export function* fetchInitialViewPresetsSaga(): SagaIterator {
  try {
    const presetsApi = yield call(getPresetsApi);
    // No filters so we don't need to pass any params for this viewPresets list call
    const { data: viewPresets } = yield call(presetsApi.getViewPresets);
    yield put(viewPresetActions.fetchedInitialViewPresets({ viewPresets }));
  } catch (error) {
    yield put(
      viewPresetActions.errorInitialViewPresets({
        error: {
          message: 'Error fetching map preset list',
          type: ViewPresetType.PRESET_LIST,
        },
      }),
    );
  }
}

export function* fetchViewPresetsSaga({
  payload,
}: ReturnType<typeof viewPresetActions.fetchViewPresets>): SagaIterator {
  const { panelId, filterParams } = payload;

  try {
    const presetsApi = yield call(getPresetsApi);
    const { data: viewPresets } = yield call(
      presetsApi.getViewPresets,
      filterParams,
    );

    yield put(
      viewPresetActions.fetchedViewPresets({
        viewPresets,
        panelId,
        filterParams,
      }),
    );
  } catch (error) {
    yield put(
      viewPresetActions.errorViewPreset({
        error: {
          message: 'Error fetching map preset list',
          type: ViewPresetType.PRESET_LIST,
        },
        panelId,
      }),
    );
  }
}

export function* saveViewPresetSaga({
  payload,
}: ReturnType<typeof viewPresetActions.saveViewPreset>): SagaIterator {
  const { panelId, viewPreset, viewPresetId } = payload;
  try {
    const presetsApi = yield call(getPresetsApi);
    yield call(presetsApi.saveViewPreset, viewPresetId, viewPreset);
    yield put(
      snackbarActions.openSnackbar({
        message: getSnackbarMessage(PresetAction.SAVE, viewPreset.title),
      }),
    );
    yield put(
      viewPresetActions.savedViewPreset({
        panelId,
        viewPresetId,
      }),
    );
  } catch (error) {
    yield put(
      viewPresetActions.errorViewPreset({
        error: {
          message: MESSAGE_SAVE_PRESET,
          type: ViewPresetType.PRESET_DETAIL,
        },
        panelId,
        viewPresetId,
      }),
    );
  }
}

export function* selectViewPresetSaga({
  payload,
}: ReturnType<typeof viewPresetActions.selectViewPreset>): SagaIterator {
  const { panelId, viewPresetId = '' } = payload;
  const isMapAnimating = yield select(mapSelectors.isAnimating, panelId);
  if (isMapAnimating) {
    yield put(mapStoreActions.mapStopAnimation({ mapId: panelId }));
  }

  // fetch existing map preset selected
  if (viewPresetId !== '') {
    yield put(
      workspaceActions.fetchWorkspaceViewPreset({
        mosaicNodeId: panelId,
        viewPresetId,
      }),
    );
  } else {
    // new map preset selected
    yield put(
      mapStoreActions.setMapPreset({
        mapId: panelId,
        initialProps: emptyMapViewPreset.initialProps,
      }),
    );
    // update workspace with new empy view map
    yield put(
      workspaceActions.updateWorkspaceView({
        viewPreset: emptyMapViewPreset,
        mosaicNodeId: panelId,
      }),
    );

    yield put(
      viewPresetActions.fetchedViewPreset({
        panelId,
        viewPresetId,
        viewPreset: emptyMapViewPreset,
      }),
    );
  }
}

export function* onSuccessViewPresetActionSaga({
  payload,
}: ReturnType<
  typeof viewPresetActions.onSuccessViewPresetAction
>): SagaIterator {
  const { action, viewPresetId, title, panelId } = payload;
  try {
    yield put(
      snackbarActions.openSnackbar({
        message: getSnackbarMessage(action, title),
      }),
    );
    const activeMapPresetId = yield select(
      mapPresetSelectors.getViewPresetActiveId,
      panelId,
    );

    // if active preset has been deleted, select the empty new preset
    if (action === PresetAction.DELETE && viewPresetId === activeMapPresetId) {
      yield put(
        viewPresetActions.setActiveViewPresetId({
          panelId,
          viewPresetId: '',
        }),
      );
    }

    // Retrieve new list with current filter and search query
    const viewPresetFilters = yield select(
      viewPresetSelectors.getViewPresetListFiltersForView,
      panelId,
    );
    // Retrieve new list with current filter
    const viewPresetSearchQuery = yield select(
      viewPresetSelectors.getViewPresetListSearchQueryForView,
      panelId,
    );

    yield call(
      fetchViewPresetsSaga,
      viewPresetActions.fetchViewPresets({
        panelId,
        filterParams: constructFilterParams(
          viewPresetFilters,
          viewPresetSearchQuery,
        ),
      }),
    );

    if (action !== PresetAction.EDIT) {
      yield put(
        viewPresetActions.setViewPresetHasChanges({
          panelId,
          hasChanges: false,
        }),
      );
    }

    if (action === PresetAction.SAVE_AS && viewPresetId) {
      yield put(
        viewPresetActions.setActiveViewPresetId({
          panelId,
          viewPresetId,
        }),
      );
    }
  } catch (error) {
    // TODO: use error handling of mapPreset https://gitlab.com/opengeoweb/opengeoweb/-/issues/3043
  }
}

export function* rootSaga(): SagaIterator {
  yield takeLeading(
    viewPresetActions.fetchInitialViewPresets,
    fetchInitialViewPresetsSaga,
  );
  yield takeLatest(viewPresetActions.fetchViewPresets, fetchViewPresetsSaga);
  yield takeLatest(viewPresetActions.saveViewPreset, saveViewPresetSaga);
  yield takeLatest(viewPresetActions.selectViewPreset, selectViewPresetSaga);

  yield takeLatest(
    viewPresetActions.onSuccessViewPresetAction,
    onSuccessViewPresetActionSaga,
  );
}

export default rootSaga;
