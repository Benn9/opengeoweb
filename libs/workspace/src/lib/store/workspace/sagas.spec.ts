/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */

import {
  mapActions,
  routerActions,
  syncGroupsActions,
} from '@opengeoweb/store';
import {
  debounce,
  put,
  takeLatest,
  call,
  delay,
  all,
  takeEvery,
  select,
} from 'redux-saga/effects';

import workspaceSaga, {
  changePresetSaga,
  updateViewsSaga,
  fetchWorkspaceSaga,
  fetchWorkspaceViewPresetSaga,
  registerMapSaga,
  saveWorkspacePresetSaga,
  unregisterMapSaga,
} from './sagas';
import { workspaceActions } from './reducer';
import {
  createApi as createFakeApi,
  ERROR_NOT_FOUND,
} from '../../utils/fakeApi';
import {
  WorkspacePreset,
  WorkspacePresetFromBE,
  WorkspaceErrorType,
  WorkspacePresetAction,
  WorkspaceSyncGroup,
} from './types';
import {
  getListInChunks,
  getLoadingWorkspace,
  getSyncGroups,
  getViewPresetState,
  getWorkspaceApi,
} from './utils';
import { PresetsApi } from '../../utils/api';
import { workspaceListActions } from '../workspaceList/reducer';
import { getWorkspaceState, getViewType } from './selectors';
import { viewPresetActions } from '../viewPresets';
import { getWorkspaceRouteUrl } from '../../utils/routes';

const { setPreset, changePreset, updateWorkspaceViews, fetchWorkspace } =
  workspaceActions;

describe('store/sagas', () => {
  describe('workspaceSaga', () => {
    it('should intercept actions and fire sagas', () => {
      const generator = workspaceSaga();
      expect(generator.next().value).toEqual(
        takeLatest(mapActions.registerMap, registerMapSaga),
      );
      expect(generator.next().value).toEqual(
        takeLatest(mapActions.unregisterMap, unregisterMapSaga),
      );
      expect(generator.next().value).toEqual(
        takeLatest(changePreset.type, changePresetSaga),
      );
      expect(generator.next().value).toEqual(
        takeLatest(fetchWorkspace.type, fetchWorkspaceSaga),
      );
      expect(generator.next().value).toEqual(
        takeLatest(
          workspaceActions.saveWorkspacePreset,
          saveWorkspacePresetSaga,
        ),
      );
      expect(generator.next().value).toEqual(
        takeEvery(
          workspaceActions.fetchWorkspaceViewPreset.type,
          fetchWorkspaceViewPresetSaga,
        ),
      );
      expect(generator.next().value).toEqual(
        debounce(100, updateWorkspaceViews.type, updateViewsSaga),
      );
      expect(generator.next().done).toBeTruthy();
    });
  });

  describe('registerMapSaga', () => {
    it('should register mapPresets when viewType is tiledWindow', async () => {
      const mapId = 'test-1';
      const viewType = 'tiledWindow';
      const workspaceState: WorkspacePreset = {
        title: 'test title',
        mosaicNode: 'mosaic-id',
        views: {
          byId: {
            [mapId]: {
              id: 'test-id',
              componentType: 'Map',
            },
          },
          allIds: [mapId],
        },
      };
      const generator = registerMapSaga(mapActions.registerMap({ mapId }));
      expect(generator.next().value).toEqual(select(getViewType));
      expect(generator.next(viewType).value).toEqual(select(getWorkspaceState));
      expect(generator.next(workspaceState).value).toEqual(
        put(
          viewPresetActions.registerViewPreset({
            panelId: mapId,
            viewPresetId: workspaceState.views.byId![mapId].id!,
          }),
        ),
      );
      expect(generator.next().done).toBeTruthy();
    });

    it('should register mapPresets when viewType is tiledWindow and use first id for multiple maps in single view', async () => {
      const mapId = 'test-1';
      const viewType = 'tiledWindow';
      const workspaceState: WorkspacePreset = {
        title: 'test title',
        mosaicNode: 'mosaic-id',
        views: {
          byId: {
            'multi-1': {
              id: 'multi-test-id',
              componentType: 'ModelRunInterval',
            },
          },
          allIds: ['multi-1'],
        },
      };
      const generator = registerMapSaga(mapActions.registerMap({ mapId }));
      expect(generator.next().value).toEqual(select(getViewType));
      expect(generator.next(viewType).value).toEqual(select(getWorkspaceState));
      expect(generator.next(workspaceState).value).toEqual(
        put(
          viewPresetActions.registerViewPreset({
            panelId: mapId,
            viewPresetId:
              workspaceState.views.byId![workspaceState.views.allIds[0]].id!,
          }),
        ),
      );
      expect(generator.next().done).toBeTruthy();
    });

    it('should not register mapPresets when viewType other than tiledWindow', async () => {
      const mapId = 'test-1';
      const viewType = 'multiWindow';
      const generator = registerMapSaga(mapActions.registerMap({ mapId }));
      expect(generator.next().value).toEqual(select(getViewType));
      expect(generator.next(viewType).done).toBeTruthy();
    });
  });

  describe('unregisterMapSaga', () => {
    it('should unregister mapPresets when viewType is tiledWindow', async () => {
      const mapId = 'test-1';
      const viewType = 'tiledWindow';
      const generator = unregisterMapSaga(mapActions.unregisterMap({ mapId }));
      expect(generator.next().value).toEqual(select(getViewType));
      expect(generator.next(viewType).value).toEqual(
        put(
          viewPresetActions.unregisterViewPreset({
            panelId: mapId,
          }),
        ),
      );
      expect(generator.next().done).toBeTruthy();
    });
    it('should not unregister mapPresets when viewType is not tiledWindow', async () => {
      const mapId = 'test-1';
      const viewType = 'multiWindow';
      const generator = unregisterMapSaga(mapActions.unregisterMap({ mapId }));
      expect(generator.next().value).toEqual(select(getViewType));
      expect(generator.next(viewType).done).toBeTruthy();
    });
  });

  describe('changePresetSaga', () => {
    const workspaceStore: WorkspacePreset = {
      id: 'radarSingleMapScreenConfig',
      title: 'Radar view',
      views: {
        allIds: ['radarView'],
        byId: {
          radarView: {
            title: 'Radar view',
            componentType: 'Map',
            initialProps: {
              mapPreset: {
                layers: [],
                proj: {
                  bbox: {
                    left: 58703.6377,
                    bottom: 6408480.4514,
                    right: 3967387.5161,
                    top: 11520588.9031,
                  },
                  srs: 'EPSG:3857',
                },
                activeLayerId: 'radar_precipitation_intensity_nordic_id',
                toggleTimestepAuto: false,
                setTimestep: 5,
              },
            },
          },
        },
      },
      mosaicNode: 'radarView',
      syncGroups: [],
    };

    const changeAction = changePreset({
      workspacePreset: {
        id: 'harmSingleMapScreenConfig',
        title: 'HARM',
        views: {
          allIds: ['harm'],
          byId: {
            harm: {
              title: 'HARM',
              componentType: 'Map',
              initialProps: {
                mapPreset: {
                  layers: [],
                  proj: {
                    bbox: {
                      left: -450651.2255879827,
                      bottom: 6490531.093143953,
                      right: 1428345.8183648037,
                      top: 7438773.776232235,
                    },
                    srs: 'EPSG:3857',
                  },
                },
              },
            },
          },
        },
        mosaicNode: 'harm',
      },
    });

    it('should trigger setPreset when switching presets and register default sync groups', () => {
      const generator = changePresetSaga(changeAction);

      expect(generator.next().value).toEqual(delay(0));

      expect(generator.next(workspaceStore).value).toEqual(
        put(syncGroupsActions.syncGroupClear()),
      );

      expect(generator.next().value).toEqual(
        put(setPreset(changeAction.payload)),
      );

      const allSyncGroups = getSyncGroups(undefined);

      expect(generator.next().value).toEqual(
        all(
          allSyncGroups.map((syncGroup) =>
            put(syncGroupsActions.syncGroupAddGroup(syncGroup)),
          ),
        ),
      );
      expect(generator.next().done).toBeTruthy();
    });

    it('should reset setPreset when switching to same preset and register default sync groups', () => {
      const workspaceTestId = 'test-id-1';
      const testPreset: WorkspacePreset = {
        id: workspaceTestId,
        title: 'HARM',
        views: {
          allIds: ['harm'],
          byId: {
            harm: {
              title: 'HARM',
              componentType: 'Map',
              initialProps: {
                mapPreset: {
                  layers: [],
                  proj: {
                    bbox: {
                      left: -450651.2255879827,
                      bottom: 6490531.093143953,
                      right: 1428345.8183648037,
                      top: 7438773.776232235,
                    },
                    srs: 'EPSG:3857',
                  },
                },
              },
            },
          },
        },
        mosaicNode: 'harm',
      };

      const generator = changePresetSaga(
        changePreset({
          workspacePreset: testPreset,
        }),
      );
      expect(generator.next().value).toEqual(delay(0));

      expect(generator.next().value).toEqual(
        put(syncGroupsActions.syncGroupClear()),
      );
      expect(generator.next().value).toEqual(
        put(workspaceActions.setPreset({ workspacePreset: testPreset })),
      );

      const allSyncGroups = getSyncGroups(undefined);

      expect(generator.next().value).toEqual(
        all(
          allSyncGroups.map((syncGroup) =>
            put(syncGroupsActions.syncGroupAddGroup(syncGroup)),
          ),
        ),
      );
      expect(generator.next().done).toBeTruthy();
    });

    it('should trigger setPreset when switching presets and register given workspace sync groups', () => {
      const testActionWithSyncGroups: WorkspacePreset = {
        id: 'harmSingleMapScreenConfig',
        title: 'HARM',
        views: {
          allIds: ['harm'],
          byId: {
            harm: {
              title: 'HARM',
              componentType: 'Map',
              initialProps: {
                mapPreset: {
                  layers: [],
                  proj: {
                    bbox: {
                      left: -450651.2255879827,
                      bottom: 6490531.093143953,
                      right: 1428345.8183648037,
                      top: 7438773.776232235,
                    },
                    srs: 'EPSG:3857',
                  },
                },
              },
            },
          },
        },
        mosaicNode: 'harm',
        syncGroups: [
          { id: 'areaLayerGroupA', type: 'SYNCGROUPS_TYPE_SETBBOX' as const },
          { id: 'timeLayerGroupA', type: 'SYNCGROUPS_TYPE_SETTIME' as const },
          {
            id: 'layerGroupA',
            type: 'SYNCGROUPS_TYPE_SETLAYERACTIONS' as const,
          },
          {
            id: 'layerGroupB',
            type: 'SYNCGROUPS_TYPE_SETLAYERACTIONS' as const,
          },
        ],
      };

      const generator = changePresetSaga(
        workspaceActions.changePreset({
          workspacePreset: testActionWithSyncGroups,
        }),
      );

      expect(generator.next().value).toEqual(delay(0));

      expect(generator.next(workspaceStore).value).toEqual(
        put(syncGroupsActions.syncGroupClear()),
      );

      expect(generator.next().value).toEqual(
        put(setPreset({ workspacePreset: testActionWithSyncGroups })),
      );

      const allSyncGroups = getSyncGroups(testActionWithSyncGroups.syncGroups);
      expect(generator.next().value).toEqual(
        all(
          allSyncGroups.map((syncGroup) =>
            put(syncGroupsActions.syncGroupAddGroup(syncGroup)),
          ),
        ),
      );
      expect(generator.next().done).toBeTruthy();
    });
  });

  describe('saveWorkspacePresetSaga', () => {
    const workspaceData: WorkspacePresetFromBE = {
      abstract: 'some abstract',
      id: 'workspaceId',
      mosaicNode: 'test',
      scope: 'user',
      syncGroups: [
        {
          id: 'Default group for SYNCGROUPS_TYPE_SETBBOX',
          type: 'SYNCGROUPS_TYPE_SETBBOX',
        },
        {
          id: 'Default group for SYNCGROUPS_TYPE_SETTIME',
          type: 'SYNCGROUPS_TYPE_SETTIME',
        },
      ],
      title: 'test workspace',
      viewType: 'singleWindow',
      views: [{ mosaicNodeId: 'test', viewPresetId: 'screenSat' }],
    };
    it('should handle saveWorkspacePresetSaga', () => {
      const mockApi = createFakeApi();
      const generator = saveWorkspacePresetSaga(
        workspaceActions.saveWorkspacePreset({
          workspace: workspaceData,
        }),
      );

      expect(generator.next().value).toEqual(call(getWorkspaceApi));

      expect(generator.next(mockApi).value).toEqual(
        call(mockApi.saveWorkspacePreset, workspaceData.id!, workspaceData),
      );

      expect(generator.next().value).toEqual(
        put(workspaceActions.loadedWorkspace(undefined!)),
      );

      expect(generator.next().value).toEqual(
        put(
          workspaceListActions.onSuccessWorkspacePresetAction({
            action: WorkspacePresetAction.SAVE,
            workspaceId: workspaceData.id!,
            title: workspaceData.title,
          }),
        ),
      );

      expect(generator.next().done).toBeTruthy();
    });

    it('should handle errors', () => {
      const generator = saveWorkspacePresetSaga(
        workspaceActions.saveWorkspacePreset({
          workspace: workspaceData,
        }),
      );
      expect(generator.next().value).toEqual(call(getWorkspaceApi));

      expect(generator.next(new Error()).value).toEqual(
        put(
          workspaceActions.errorWorkspace({
            error: {
              type: WorkspaceErrorType.SAVE,
              message: expect.any(String),
            },
            workspaceId: workspaceData.id!,
          }),
        ),
      );

      expect(generator.next().done).toBeTruthy();
    });
  });
  describe('updateViewsSaga', () => {
    it('should handle updateViewsSaga', async () => {
      const generator = updateViewsSaga();
      expect(generator.next().value).toEqual(
        call(window.dispatchEvent, new Event('resize')),
      );
      expect(generator.next().done).toBeTruthy();
    });
  });

  describe('fetchWorkspaceViewPresetSaga', () => {
    it('should trigger fetching of full viewPresets', () => {
      const testIds = ['Area_radarTemp', 'Time_radarTemp'];
      const testId = 'screenConfigRadarTemp';

      const mockResult = {
        data: {
          id: testId,
          initialProps: { syncGroupsIds: testIds },
          componentType: 'Map' as const,
          title: 'test',
          keywords: '',
        },
      };

      const mosaicNodeId = 'screenA';

      const mockApi = createFakeApi();
      const generator = fetchWorkspaceViewPresetSaga(
        workspaceActions.fetchWorkspaceViewPreset({
          viewPresetId: testId,
          mosaicNodeId,
        }),
      );

      expect(generator.next().value).toEqual(
        put(
          workspaceActions.updateWorkspaceView({
            mosaicNodeId,
            viewPreset: getViewPresetState(testId, mosaicNodeId, 'loading'),
          }),
        ),
      );

      expect(generator.next().value).toEqual(call(getWorkspaceApi));

      expect(generator.next(mockApi).value).toEqual(
        call(mockApi.getViewPreset, testId),
      );

      expect(generator.next(mockResult).value).toEqual(
        put(
          viewPresetActions.fetchedViewPreset({
            viewPreset: mockResult.data,
            viewPresetId: testId,
            panelId: mosaicNodeId,
          }),
        ),
      );

      expect(generator.next(mockResult).value).toEqual(
        all(
          testIds.map((syncId) =>
            put(
              syncGroupsActions.syncGroupAddTarget({
                groupId: syncId,
                targetId: mosaicNodeId,
                linked: true,
              }),
            ),
          ),
        ),
      );

      expect(generator.next().value).toEqual(
        put(
          workspaceActions.updateWorkspaceView({
            mosaicNodeId,
            viewPreset: mockResult.data,
          }),
        ),
      );

      expect(generator.next().done).toBeTruthy();
    });

    it('should trigger fetching of full viewPresets and not add syncgroups', () => {
      const mockResult = {
        data: {
          initialProps: { syncGroupsIds: [] },
          componentType: 'Map' as const,
          id: 'test',
          title: 'test',
          keywords: '',
        },
      };

      const testId = 'screenConfigRadarTemp';
      const mosaicNodeId = 'ScreenA';

      const mockApi = createFakeApi();
      const generator = fetchWorkspaceViewPresetSaga(
        workspaceActions.fetchWorkspaceViewPreset({
          viewPresetId: testId,
          mosaicNodeId,
        }),
      );

      expect(generator.next().value).toEqual(
        put(
          workspaceActions.updateWorkspaceView({
            mosaicNodeId,
            viewPreset: getViewPresetState(testId, mosaicNodeId, 'loading'),
          }),
        ),
      );

      expect(generator.next().value).toEqual(call(getWorkspaceApi));

      expect(generator.next(mockApi).value).toEqual(
        call(mockApi.getViewPreset, testId),
      );

      expect(generator.next(mockResult).value).toEqual(
        put(
          viewPresetActions.fetchedViewPreset({
            viewPreset: mockResult.data,
            viewPresetId: mockResult.data.id,
            panelId: mosaicNodeId,
          }),
        ),
      );

      expect(generator.next(mockResult).value).toEqual(
        put(
          workspaceActions.updateWorkspaceView({
            mosaicNodeId,
            viewPreset: mockResult.data,
          }),
        ),
      );

      expect(generator.next().done).toBeTruthy();
    });

    it('should handle errors', () => {
      const testId = 'screenConfigRadarTemp';
      const mosaicNodeId = 'screenA';

      const generator = fetchWorkspaceViewPresetSaga(
        workspaceActions.fetchWorkspaceViewPreset({
          viewPresetId: testId,
          mosaicNodeId,
        }),
      );

      expect(generator.next().value).toEqual(
        put(
          workspaceActions.updateWorkspaceView({
            mosaicNodeId,
            viewPreset: getViewPresetState(testId, mosaicNodeId, 'loading'),
          }),
        ),
      );

      expect(generator.next().value).toEqual(call(getWorkspaceApi));

      expect(generator.next().value).toEqual(
        put(
          workspaceActions.updateWorkspaceView({
            mosaicNodeId,
            viewPreset: getViewPresetState(testId, mosaicNodeId, 'error'),
          }),
        ),
      );
      expect(generator.next().done).toBeTruthy();
    });
  });

  describe('fetchWorkspaceSaga', () => {
    it('should handle fetchWorkspaceSaga', async () => {
      const testWorkspaceId = 'test';

      const action = fetchWorkspace({ workspaceId: testWorkspaceId });
      const generator = fetchWorkspaceSaga(action);
      expect(generator.next().value).toEqual(
        put(
          routerActions.navigateToUrl({
            url: getWorkspaceRouteUrl(testWorkspaceId),
          }),
        ),
      );
      expect(generator.next().value).toEqual(
        put(
          workspaceListActions.toggleWorkspaceDialog({
            isWorkspaceListDialogOpen: false,
          }),
        ),
      );

      expect(generator.next(testWorkspaceId).value).toEqual(
        call(getWorkspaceApi),
      );
      const mockApi = createFakeApi();

      expect(generator.next(mockApi).value).toEqual(
        call(mockApi.getWorkspacePreset, testWorkspaceId),
      );

      const mockResult = {
        data: {
          views: [
            { mosaicNodeId: 'viewA', viewPresetId: 'screenConfigRadarTemp' },
            { mosaicNodeId: 'viewB', viewPresetId: 'screenConfigTimeSeries' },
          ],

          id: testWorkspaceId,
          title: '',
          mosaicNode: '',
          syncGroups: [],
        },
      };

      expect(generator.next(mockResult).value).toEqual(
        put(
          workspaceActions.loadedWorkspace({
            workspaceId: testWorkspaceId,
            workspace: mockResult.data,
          }),
        ),
      );

      expect(generator.next().value).toEqual(
        put(
          workspaceActions.changePreset({
            workspacePreset: getLoadingWorkspace(mockResult.data),
          }),
        ),
      );

      expect(generator.next().value).toEqual(
        all(
          mockResult.data.views.map((view) =>
            call(
              fetchWorkspaceViewPresetSaga,
              workspaceActions.fetchWorkspaceViewPreset({
                viewPresetId: view.viewPresetId,
                mosaicNodeId: view.mosaicNodeId,
              }),
            ),
          ),
        ),
      );
      expect(generator.next().done).toBeTruthy();
    });

    it('should handle fetchWorkspaceSaga', async () => {
      const testWorkspaceId = 'test';
      const testSyncGroups = [] as WorkspaceSyncGroup[];

      const action = fetchWorkspace({ workspaceId: testWorkspaceId });
      const generator = fetchWorkspaceSaga(action);

      expect(generator.next(testWorkspaceId).value).toEqual(
        put(
          routerActions.navigateToUrl({
            url: getWorkspaceRouteUrl(testWorkspaceId),
          }),
        ),
      );
      expect(generator.next(testWorkspaceId).value).toEqual(
        put(
          workspaceListActions.toggleWorkspaceDialog({
            isWorkspaceListDialogOpen: false,
          }),
        ),
      );

      expect(generator.next(testWorkspaceId).value).toEqual(
        call(getWorkspaceApi),
      );
      const mockApi = createFakeApi();

      expect(generator.next(mockApi).value).toEqual(
        call(mockApi.getWorkspacePreset, testWorkspaceId),
      );

      const mockResult = {
        data: {
          views: [{ mosaicNodeId: 'viewA', viewPresetId: 'testWorkspaceIds' }],
          id: testWorkspaceId,
          title: '',
          mosaicNode: 'testWorkspaceIds',
          syncGroups: testSyncGroups,
        },
      };

      expect(generator.next(mockResult).value).toEqual(
        put(
          workspaceActions.loadedWorkspace({
            workspaceId: testWorkspaceId,
            workspace: mockResult.data,
          }),
        ),
      );

      expect(generator.next().value).toEqual(
        put(
          workspaceActions.changePreset({
            workspacePreset: getLoadingWorkspace(mockResult.data),
          }),
        ),
      );

      expect(generator.next().value).toEqual(
        all(
          mockResult.data.views.map((view) =>
            call(
              fetchWorkspaceViewPresetSaga,
              workspaceActions.fetchWorkspaceViewPreset({
                viewPresetId: view.viewPresetId,
                mosaicNodeId: view.mosaicNodeId,
              }),
            ),
          ),
        ),
      );
      expect(generator.next().done).toBeTruthy();
    });

    it('should load viewPresets in batches of 3', async () => {
      const testWorkspaceId = 'test';
      const testSyncGroups = [
        {
          id: 'Area_radarTemp',
          type: 'SYNCGROUPS_TYPE_SETBBOX' as const,
          title: 'area temp',
        },
        {
          id: 'Time_radarTemp',
          type: 'SYNCGROUPS_TYPE_SETTIME' as const,
          title: 'time box',
        },
      ];

      const action = fetchWorkspace({ workspaceId: testWorkspaceId });
      const generator = fetchWorkspaceSaga(action);

      expect(generator.next(testWorkspaceId).value).toEqual(
        put(
          routerActions.navigateToUrl({
            url: getWorkspaceRouteUrl(testWorkspaceId),
          }),
        ),
      );

      expect(generator.next().value).toEqual(
        put(
          workspaceListActions.toggleWorkspaceDialog({
            isWorkspaceListDialogOpen: false,
          }),
        ),
      );

      expect(generator.next(testWorkspaceId).value).toEqual(
        call(getWorkspaceApi),
      );
      const mockApi = createFakeApi();

      expect(generator.next(mockApi).value).toEqual(
        call(mockApi.getWorkspacePreset, testWorkspaceId),
      );

      const mockResult = {
        data: {
          views: [
            { mosaicNodeId: 'viewA', viewPresetId: 'screenConfigRadarTemp' },
            { mosaicNodeId: 'viewB', viewPresetId: 'screenConfigTimeSeries' },
            { mosaicNodeId: 'viewC', viewPresetId: 'screenConfigRadarTemp-2' },
            { mosaicNodeId: 'viewD', viewPresetId: 'screenConfigTimeSeries-2' },
            { mosaicNodeId: 'viewE', viewPresetId: 'screenConfigRadarTemp-3' },
            { mosaicNodeId: 'viewF', viewPresetId: 'screenConfigTimeSeries-3' },
          ],
          id: testWorkspaceId,
          title: '',
          mosaicNode: '',
          syncGroups: testSyncGroups,
        },
      };

      expect(generator.next(mockResult).value).toEqual(
        put(
          workspaceActions.loadedWorkspace({
            workspaceId: testWorkspaceId,
            workspace: mockResult.data,
          }),
        ),
      );

      expect(generator.next().value).toEqual(
        put(
          workspaceActions.changePreset({
            workspacePreset: getLoadingWorkspace(mockResult.data),
          }),
        ),
      );

      const chunks = getListInChunks(mockResult.data.views);

      expect(generator.next().value).toEqual(
        all(
          chunks[0].map((view) =>
            call(
              fetchWorkspaceViewPresetSaga,
              workspaceActions.fetchWorkspaceViewPreset({
                viewPresetId: view.viewPresetId,
                mosaicNodeId: view.mosaicNodeId,
              }),
            ),
          ),
        ),
      );
      expect(generator.next().value).toEqual(
        all(
          chunks[1].map((view) =>
            call(
              fetchWorkspaceViewPresetSaga,
              workspaceActions.fetchWorkspaceViewPreset({
                viewPresetId: view.viewPresetId,
                mosaicNodeId: view.mosaicNodeId,
              }),
            ),
          ),
        ),
      );

      expect(generator.next().done).toBeTruthy();
    });

    it('should handle errors', () => {
      const testWorkspaceId = 'test';
      const action = fetchWorkspace({ workspaceId: testWorkspaceId });
      const generator = fetchWorkspaceSaga(action);

      expect(generator.next(testWorkspaceId).value).toEqual(
        put(
          routerActions.navigateToUrl({
            url: getWorkspaceRouteUrl(testWorkspaceId),
          }),
        ),
      );
      expect(generator.next(testWorkspaceId).value).toEqual(
        put(
          workspaceListActions.toggleWorkspaceDialog({
            isWorkspaceListDialogOpen: false,
          }),
        ),
      );

      expect(generator.next(testWorkspaceId).value).toEqual(
        call(getWorkspaceApi),
      );
      const mockApi: PresetsApi = {
        ...createFakeApi(),
        getWorkspacePreset: () => {
          return Promise.reject(new Error('not found'));
        },
      } as PresetsApi;
      expect(generator.next(mockApi).value).toEqual(
        call(mockApi.getWorkspacePreset, testWorkspaceId),
      );

      expect(generator.next(mockApi).value).toEqual(
        put(
          workspaceActions.errorWorkspace({
            workspaceId: testWorkspaceId,
            error: {
              type: WorkspaceErrorType.NOT_FOUND,
              message: ERROR_NOT_FOUND,
            },
          }),
        ),
      );
    });
  });
});
