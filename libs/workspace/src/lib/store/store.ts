/* eslint-disable @typescript-eslint/ban-ts-comment */
/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */

import { createStore } from '@redux-eggs/redux-toolkit';
import { getSagaExtension } from '@redux-eggs/saga-extension';
import { DevToolsEnhancerOptions } from '@reduxjs/toolkit';

import { produce } from 'immer';

import type { CoreAppStore } from '@opengeoweb/store';
import type { WorkspaceModuleStore } from './config';

export interface AppStore extends WorkspaceModuleStore, CoreAppStore {}

const devToolsStateSanitizer: DevToolsEnhancerOptions = {
  stateSanitizer: (state) => {
    return produce(state, (draftState) => {
      // @ts-ignore
      if (state['services']) {
        // @ts-ignore
        Object.values(state['services'].byId).forEach((service) => {
          // @ts-ignore
          draftState['services']!.byId[service.id].layers =
            // @ts-ignore
            service.layers.length;
        });
      }
    });
  },
};

const store = createStore({
  extensions: [getSagaExtension({})],
  devTools: devToolsStateSanitizer,
});

export { store };
