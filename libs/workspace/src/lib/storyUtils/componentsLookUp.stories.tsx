/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */
import * as React from 'react';
import { darkTheme, lightTheme } from '@opengeoweb/theme';
import { ApiProvider } from '@opengeoweb/api';
import { createFakeApi as createFakeTafApi } from '@opengeoweb/taf';
import { createFakeApi as createFakeSigmetAirmetApi } from '@opengeoweb/sigmet-airmet';

import { useDispatch } from 'react-redux';
import defaultScreenPresets from './screenPresets.json';
import { store } from '../store/store';
import { WorkspaceViewConnect } from '../components/WorkspaceView';
import {
  WorkspacePreset,
  WorkspaceSetPresetPayload,
} from '../store/workspace/types';
import { componentsLookUp } from './componentsLookUp';
import { WorkspaceWrapperProviderWithStore } from '../components/Providers/Providers';
import { workspaceActions } from '../store/workspace/reducer';

/* Returns presets as object */
const getScreenPresetsAsObject = (
  presets: WorkspacePreset[],
): Record<string, WorkspacePreset> =>
  presets.reduce<Record<string, WorkspacePreset>>(
    (filteredPreset, preset) => ({
      ...filteredPreset,
      [preset.id!]: preset,
    }),
    {},
  );

const {
  screenConfigRadarTemp,
  screenConfigHarmonie,
  screenConfigHarmoniePrecipMSLRefTimePresetWithoutMultiMapLayerManager,
  screenConfigHarmoniePresetWithoutMultiMapLayerManager,
  screenConfigRadAndObs,
  screenConfigTaf,
  screenConfigSigmet,
  screenConfigAirmet,
  screenConfigActions,
  screenConfigError,
} = getScreenPresetsAsObject(
  defaultScreenPresets as unknown as WorkspacePreset[],
);

export default {
  title: 'Examples/ComponentType',
};

interface ComponentTypeDemoProps {
  screenPreset: WorkspacePreset;
  createApi?: () => void;
}

const ComponentTypeDemo: React.FC<ComponentTypeDemoProps> = ({
  screenPreset,
  createApi = (): void => {},
}: ComponentTypeDemoProps): React.ReactElement => {
  const dispatch = useDispatch();

  const changePreset = React.useCallback(
    (payload: WorkspaceSetPresetPayload): void => {
      dispatch(workspaceActions.changePreset(payload));
    },
    [dispatch],
  );
  React.useEffect(() => {
    changePreset({ workspacePreset: screenPreset });
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);
  return (
    <div
      style={{
        height: '100vh',
      }}
    >
      <ApiProvider createApi={createApi}>
        <WorkspaceViewConnect componentsLookUp={componentsLookUp} />
      </ApiProvider>
    </div>
  );
};

// each demo has a preset with different componentType prop
export const Map = (): React.ReactElement => (
  <WorkspaceWrapperProviderWithStore store={store} theme={lightTheme}>
    <ComponentTypeDemo screenPreset={screenConfigRadarTemp} />
  </WorkspaceWrapperProviderWithStore>
);

export const HarmonieTempAndPrecipPreset = (): React.ReactElement => (
  <WorkspaceWrapperProviderWithStore store={store} theme={lightTheme}>
    <ComponentTypeDemo
      screenPreset={screenConfigHarmoniePresetWithoutMultiMapLayerManager}
    />
  </WorkspaceWrapperProviderWithStore>
);
HarmonieTempAndPrecipPreset.storyName = 'HarmonieTempAndPrecipPreset';

export const ModelRunInterval = (): React.ReactElement => (
  <WorkspaceWrapperProviderWithStore store={store} theme={lightTheme}>
    <ComponentTypeDemo
      screenPreset={
        screenConfigHarmoniePrecipMSLRefTimePresetWithoutMultiMapLayerManager
      }
    />
  </WorkspaceWrapperProviderWithStore>
);
ModelRunInterval.storyName = 'ModelRunInterval';

export const MultiMap = (): React.ReactElement => (
  <WorkspaceWrapperProviderWithStore store={store} theme={lightTheme}>
    <ComponentTypeDemo screenPreset={screenConfigRadAndObs} />
  </WorkspaceWrapperProviderWithStore>
);
MultiMap.storyName = 'MultiMap';

export const TafModule = (): React.ReactElement => (
  <WorkspaceWrapperProviderWithStore store={store} theme={lightTheme}>
    <ComponentTypeDemo
      screenPreset={screenConfigTaf}
      createApi={createFakeTafApi}
    />
  </WorkspaceWrapperProviderWithStore>
);
TafModule.storyName = 'TafModule';

export const SigmetModule = (): React.ReactElement => (
  <WorkspaceWrapperProviderWithStore store={store} theme={lightTheme}>
    <ComponentTypeDemo
      screenPreset={screenConfigSigmet}
      createApi={createFakeSigmetAirmetApi}
    />
  </WorkspaceWrapperProviderWithStore>
);
SigmetModule.storyName = 'SigmetModule';

export const AirmetModule = (): React.ReactElement => (
  <WorkspaceWrapperProviderWithStore store={store} theme={lightTheme}>
    <ComponentTypeDemo
      screenPreset={screenConfigAirmet}
      createApi={createFakeSigmetAirmetApi}
    />
  </WorkspaceWrapperProviderWithStore>
);
AirmetModule.storyName = 'AirmetModule';

export const ActionsExample = (): React.ReactElement => (
  <WorkspaceWrapperProviderWithStore store={store} theme={darkTheme}>
    <ComponentTypeDemo screenPreset={screenConfigActions} />
  </WorkspaceWrapperProviderWithStore>
);
ActionsExample.storyName = 'ActionsExample';

export const ErrorExample = (): React.ReactElement => (
  <WorkspaceWrapperProviderWithStore store={store} theme={lightTheme}>
    <ComponentTypeDemo screenPreset={screenConfigError} />
  </WorkspaceWrapperProviderWithStore>
);
ErrorExample.storyName = 'ErrorExample';

export const MapWithTimeslider = (): React.ReactElement => (
  <WorkspaceWrapperProviderWithStore store={store} theme={lightTheme}>
    <ComponentTypeDemo screenPreset={screenConfigHarmonie} />
  </WorkspaceWrapperProviderWithStore>
);
MapWithTimeslider.storyName = 'Map with timeslider';
