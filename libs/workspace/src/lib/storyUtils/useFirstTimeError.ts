/* *
/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';

export const DEFAULT_ERROR_TITLE = 'Something went wrong, try again';
export const TIMEOUT = 1000;

export const useFirstTimeError = (
  initialValue = 0,
  errorTitle = DEFAULT_ERROR_TITLE,
): { fetch: <T>(promise: Promise<T>) => Promise<T> } => {
  const testCounter = React.useRef(initialValue);

  const fetch = <T>(promise: Promise<T>): Promise<T> => {
    if (testCounter.current <= 0) {
      testCounter.current += 1;
      return new Promise((_, reject) => {
        setTimeout(() => {
          reject(new Error(errorTitle));
        }, TIMEOUT);
      });
    }
    return promise;
  };

  return { fetch };
};
