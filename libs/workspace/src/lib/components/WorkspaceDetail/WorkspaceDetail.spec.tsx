/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */

import React from 'react';
import { render, fireEvent, screen, waitFor } from '@testing-library/react';
import { createMockStoreWithEggs } from '@opengeoweb/shared';
import { routerActions } from '@opengeoweb/store';
import { LayerType } from '@opengeoweb/webmap';
import { WorkspaceDetail } from './WorkspaceDetail';
import { componentsLookUp } from '../../storyUtils/componentsLookUp';
import { WorkspaceWrapperProviderWithStore } from '../Providers/Providers';
import { workspaceActions } from '../../store/workspace/reducer';
import * as workspaceSelectors from '../../store/workspace/selectors';
import {
  ViewPresetListItem,
  WorkspaceError,
  WorkspaceErrorType,
  WorkspaceSetPresetPayload,
} from '../../store/workspace/types';
import { emptyMapWorkspace } from '../../store/workspaceList/utils';
import { AppStore } from '../../store/store';
import { getWorkspaceRouteUrl } from '../../utils/routes';
import { viewPresetActions } from '../../store/viewPresets';
import { PresetAction } from '../../store/viewPresets/types';
import { createApi as createFakeApi } from '../../utils/fakeApi';
import { PresetsApi } from '../../utils/api';
import { MAPPRESET_DIALOG_TITLE_DELETE } from '../../store/viewPresets/utils';

const fakeWorkspaceState: AppStore = {
  workspaceList: {
    error: undefined,
    isFetching: false,
    isWorkspaceListDialogOpen: false,
    ids: ['workspaceList-1', 'workspaceList-2', 'workspaceList-3'],
    entities: {
      'workspaceList-1': {
        id: 'workspaceList-1',
        scope: 'system' as const,
        title: 'Workspace list item',
        date: '2022-06-01T12:34:27.787192',
        viewType: 'singleWindow' as const,
        abstract: '',
      },
      'workspaceList-2': {
        id: 'workspaceList-2',
        scope: 'system' as const,
        title: 'Workspace list item',
        date: '2022-06-01T12:34:27.787192',
        viewType: 'singleWindow' as const,
        abstract: '',
      },
      'workspaceList-3': {
        id: 'workspaceList-3',
        scope: 'system' as const,
        title: 'Workspace list item',
        date: '2022-06-01T12:34:27.787192',
        viewType: 'singleWindow' as const,
        abstract: '',
      },
    },
  },
};

const fakeSyncGroupState = {
  syncronizationGroupStore: {
    sources: {
      byId: {
        emptyMapView: {
          types: [
            'SYNCGROUPS_TYPE_SETTIME' as const,
            'SYNCGROUPS_TYPE_SETBBOX' as const,
          ],
          payloadByType: {},
        },
      },
      allIds: ['emptyMapView'],
    },
    groups: {
      byId: {
        SYNCGROUPS_TYPE_SETBBOX_A: {
          title: 'Default group for SYNCGROUPS_TYPE_SETBBOX_A',
          type: 'SYNCGROUPS_TYPE_SETBBOX' as const,
          payloadByType: {
            SYNCGROUPS_TYPE_SETTIME: null!,
            SYNCGROUPS_TYPE_SETBBOX: null!,
          },
          targets: {
            allIds: [],
            byId: {},
          },
        },
        SYNCGROUPS_TYPE_SETTIME_A: {
          title: 'Default group for SYNCGROUPS_TYPE_SETTIME_A',
          type: 'SYNCGROUPS_TYPE_SETTIME' as const,
          payloadByType: {
            SYNCGROUPS_TYPE_SETTIME: null!,
            SYNCGROUPS_TYPE_SETBBOX: null!,
          },
          targets: {
            allIds: [],
            byId: {},
          },
        },
      },
      allIds: [
        'SYNCGROUPS_TYPE_SETBBOX_A' as const,
        'SYNCGROUPS_TYPE_SETTIME_A' as const,
      ],
    },
    viewState: {
      timeslider: {
        groups: [
          {
            id: 'SYNCGROUPS_TYPE_SETTIME_A',
            selected: [],
          },
        ],
        sourcesById: [
          {
            id: 'emptyMapView',
            name: 'emptyMapView',
          },
        ],
      },
      zoompane: {
        groups: [
          {
            id: 'SYNCGROUPS_TYPE_SETBBOX_A',
            selected: [],
          },
        ],
        sourcesById: [
          {
            id: 'emptyMapView',
            name: 'emptyMapView',
          },
        ],
      },
      level: {
        groups: [],
        sourcesById: [],
      },
    },
  },
};

describe('WorkspaceDetail', () => {
  it('should render correctly', async () => {
    const store = createMockStoreWithEggs({});
    const testChildren = 'some children';
    render(
      <WorkspaceWrapperProviderWithStore store={store}>
        <WorkspaceDetail componentsLookUp={componentsLookUp}>
          <p>{testChildren}</p>
        </WorkspaceDetail>
      </WorkspaceWrapperProviderWithStore>,
    );

    expect(screen.getByTestId('workspace')).toBeTruthy();
    expect(screen.getByText(testChildren)).toBeTruthy();
  });

  it('should fetch initial view presets', async () => {
    const store = createMockStoreWithEggs({});
    render(
      <WorkspaceWrapperProviderWithStore store={store}>
        <WorkspaceDetail componentsLookUp={componentsLookUp}>
          <p>some children</p>
        </WorkspaceDetail>
      </WorkspaceWrapperProviderWithStore>,
    );

    expect(screen.getByTestId('workspace')).toBeTruthy();

    expect(store.getActions()).toContainEqual(
      viewPresetActions.fetchInitialViewPresets(),
    );
  });

  it('should not fetch initial view presets when present in store', async () => {
    const mockState: AppStore = {
      viewPresetsList: {
        entities: {
          preset: {
            id: 'mapPreset-1',
            scope: 'system',
            title: 'Layer manager preset',
            date: '2022-06-01T12:34:27.787192',
          } as ViewPresetListItem,
          preset_2: {
            id: 'mapPreset-2',
            scope: 'system',
            title: 'Layer manager preset 2',
            date: '2022-06-01T12:34:27.787192',
          } as ViewPresetListItem,
        },
        ids: ['preset', 'preset_2'],
      },
    };
    const store = createMockStoreWithEggs(mockState);
    render(
      <WorkspaceWrapperProviderWithStore store={store}>
        <WorkspaceDetail componentsLookUp={componentsLookUp}>
          <p>some children</p>
        </WorkspaceDetail>
      </WorkspaceWrapperProviderWithStore>,
    );

    expect(screen.getByTestId('workspace')).toBeTruthy();

    expect(store.getActions()).not.toContainEqual(
      viewPresetActions.fetchInitialViewPresets(),
    );
  });

  it('should show loading bar when isLoading and keep WorkspacePage visible', async () => {
    const mockState: AppStore = {
      workspace: {
        id: 'emptyMap',
        title: 'Empty Map',
        views: {
          allIds: ['emptyMapView'],
          byId: {
            emptyMapView: {
              title: 'Empty Map',
              componentType: 'Map',
              initialProps: {
                mapPreset: {
                  layers: [],
                  proj: {
                    bbox: {
                      left: 58703.6377,
                      bottom: 6408480.4514,
                      right: 3967387.5161,
                      top: 11520588.9031,
                    },
                    srs: 'EPSG:3857',
                  },
                  shouldShowLayerManager: true,
                  dockedLayerManagerSize: 'sizeSmall',
                },
                syncGroupsIds: [],
              },
            },
          },
        },
        mosaicNode: 'emptyMapView',
        isLoading: true,
        syncGroups: [],
      },
      ...fakeSyncGroupState,
    } as AppStore;
    const store = createMockStoreWithEggs(mockState);

    render(
      <WorkspaceWrapperProviderWithStore store={store}>
        <WorkspaceDetail componentsLookUp={componentsLookUp} />
      </WorkspaceWrapperProviderWithStore>,
    );

    expect(screen.getByTestId('loading-bar')).toBeTruthy();
    expect(screen.getByTestId('WorkspacePage')).toBeTruthy();
  });

  it('should render empty map preset when no workspace id', async () => {
    const store = createMockStoreWithEggs({});

    render(
      <WorkspaceWrapperProviderWithStore store={store}>
        <WorkspaceDetail componentsLookUp={componentsLookUp} />
      </WorkspaceWrapperProviderWithStore>,
    );

    const actions = store.getActions();

    const expectedResult = [
      workspaceActions.changePreset({
        workspacePreset: emptyMapWorkspace,
      }),
      routerActions.navigateToUrl({ url: getWorkspaceRouteUrl() }),
    ];

    const foundAction = expectedResult[0];
    expect(actions).toContainEqual(foundAction);
    expect(
      (foundAction.payload as WorkspaceSetPresetPayload).workspacePreset.scope,
    ).toEqual('system');
  });

  it('should render workspace when prop workspaceId is given', async () => {
    const store = createMockStoreWithEggs({});
    const testWorkspaceId = 'test';

    render(
      <WorkspaceWrapperProviderWithStore store={store}>
        <WorkspaceDetail
          workspaceId={testWorkspaceId}
          componentsLookUp={componentsLookUp}
        />
      </WorkspaceWrapperProviderWithStore>,
    );

    const actions = store.getActions();

    const expectedResult = [
      workspaceActions.fetchWorkspace({ workspaceId: testWorkspaceId }),
    ];

    expect(actions).toContainEqual(expectedResult[0]);
  });

  it('should show not found error', async () => {
    const testError: WorkspaceError = {
      message: 'test error',
      type: WorkspaceErrorType.NOT_FOUND,
    };

    const testId = 'emptyMap';
    const mockState: AppStore = {
      ...fakeWorkspaceState,
      workspace: {
        id: testId,
        title: 'Empty Map',
        views: {
          allIds: ['emptyMapView'],
          byId: {
            emptyMapView: {
              title: 'Empty Map',
              componentType: 'Map',
              initialProps: {
                mapPreset: {
                  layers: [],
                  proj: {
                    bbox: {
                      left: 58703.6377,
                      bottom: 6408480.4514,
                      right: 3967387.5161,
                      top: 11520588.9031,
                    },
                    srs: 'EPSG:3857',
                  },
                  shouldShowLayerManager: true,
                  dockedLayerManagerSize: 'sizeSmall',
                },
                syncGroupsIds: [],
              },
            },
          },
        },
        mosaicNode: 'emptyMapView',
        isLoading: false,
        syncGroups: [],
        error: testError,
      },
    };
    const store = createMockStoreWithEggs(mockState);

    render(
      <WorkspaceWrapperProviderWithStore store={store}>
        <WorkspaceDetail
          workspaceId={testId}
          componentsLookUp={componentsLookUp}
        />
      </WorkspaceWrapperProviderWithStore>,
    );

    const actionResults = store.getActions();

    expect(actionResults).toEqual([
      viewPresetActions.fetchInitialViewPresets(),
      workspaceActions.fetchWorkspace({ workspaceId: testId }),
      // workspaceListActions.fetchWorkspaceList(),
    ]);
    expect(
      await screen.findByText(`Failed to fetch workspace ${testId}`),
    ).toBeTruthy();
    expect(screen.queryByText('Try again')).toBeFalsy();
    expect(screen.queryByTestId('WorkspacePage')).toBeFalsy();
  });

  it('should show generic error and be able to try again', async () => {
    const testError: WorkspaceError = {
      message: 'test error',
      type: WorkspaceErrorType.GENERIC,
    };
    const testId = 'emptyMap';
    const mockState: AppStore = {
      ...fakeWorkspaceState,
      workspace: {
        id: testId,
        title: 'Empty Map',
        views: {
          allIds: ['emptyMapView'],
          byId: {
            emptyMapView: {
              title: 'Empty Map',
              componentType: 'Map',
              initialProps: {
                mapPreset: {
                  layers: [],
                  proj: {
                    bbox: {
                      left: 58703.6377,
                      bottom: 6408480.4514,
                      right: 3967387.5161,
                      top: 11520588.9031,
                    },
                    srs: 'EPSG:3857',
                  },
                  shouldShowLayerManager: true,
                  dockedLayerManagerSize: 'sizeSmall',
                },
                syncGroupsIds: [],
              },
            },
          },
        },
        mosaicNode: 'emptyMapView',
        isLoading: false,
        syncGroups: [],
        error: testError,
      },
    };
    const store = createMockStoreWithEggs(mockState);

    render(
      <WorkspaceWrapperProviderWithStore store={store}>
        <WorkspaceDetail
          workspaceId={testId}
          componentsLookUp={componentsLookUp}
        />
      </WorkspaceWrapperProviderWithStore>,
    );

    const actionResults = store.getActions();
    const expectedInitialResults = [
      viewPresetActions.fetchInitialViewPresets(),
      workspaceActions.fetchWorkspace({ workspaceId: testId }),
    ];
    expect(actionResults).toEqual(expectedInitialResults);
    expect(
      await screen.findByText(`Failed to fetch workspace ${testId}`),
    ).toBeTruthy();
    expect(screen.getByText('TRY AGAIN')).toBeTruthy();
    expect(screen.queryByTestId('WorkspacePage')).toBeFalsy();

    fireEvent.click(screen.getByText('TRY AGAIN'));

    expect(store.getActions()).toEqual([
      ...expectedInitialResults,
      workspaceActions.fetchWorkspace({ workspaceId: testId }),
    ]);
  });

  it('should show save error and be able to try again', async () => {
    const testError: WorkspaceError = {
      message: 'test error',
      type: WorkspaceErrorType.SAVE,
    };
    const testId = 'emptyMap';
    const testTitle = 'Test title';
    const mockState: AppStore = {
      ...fakeWorkspaceState,
      workspace: {
        id: testId,
        title: testTitle,
        views: {
          allIds: ['emptyMapView'],
          byId: {
            emptyMapView: {
              title: 'Empty Map',
              componentType: 'Map',
              initialProps: {
                mapPreset: {
                  layers: [],
                  proj: {
                    bbox: {
                      left: 58703.6377,
                      bottom: 6408480.4514,
                      right: 3967387.5161,
                      top: 11520588.9031,
                    },
                    srs: 'EPSG:3857',
                  },
                  shouldShowLayerManager: true,
                  dockedLayerManagerSize: 'sizeSmall',
                },
                syncGroupsIds: [],
              },
            },
          },
        },
        mosaicNode: 'emptyMapView',
        isLoading: false,
        syncGroups: [],
        error: testError,
      },
      ...fakeSyncGroupState,
    } as AppStore;
    const store = createMockStoreWithEggs(mockState);

    render(
      <WorkspaceWrapperProviderWithStore store={store}>
        <WorkspaceDetail
          workspaceId={testId}
          componentsLookUp={componentsLookUp}
        />
      </WorkspaceWrapperProviderWithStore>,
    );

    expect(
      await screen.findByText(`Failed to save workspace "${testTitle}"`),
    ).toBeTruthy();
    expect(screen.getByText('TRY AGAIN')).toBeTruthy();
    expect(screen.getByTestId('WorkspacePage')).toBeTruthy();

    fireEvent.click(screen.getByText('TRY AGAIN'));

    expect(store.getActions()).toContainEqual(
      workspaceActions.saveWorkspacePreset({
        workspace: workspaceSelectors.getWorkspaceData(mockState as AppStore),
      }),
    );
  });

  it('should reset the active preset when user deletes it', async () => {
    const mapId = 'mapId_123';
    const testViewPresets: ViewPresetListItem[] = [
      {
        id: 'preset-1',
        scope: 'user',
        title: 'Observations',
        date: '2022-06-01T12:34:27.787192',
      },
    ];

    const mockDelete = jest.fn();
    const createApi = (): PresetsApi => {
      return {
        ...createFakeApi(),
        getViewPresets: (): Promise<{ data: ViewPresetListItem[] }> => {
          return new Promise((resolve) => {
            resolve({
              data: testViewPresets,
            });
          });
        },
        getViewPreset: jest.fn(),
        saveViewPreset: jest.fn(),
        saveViewPresetAs: jest.fn(),
        deleteViewPreset: mockDelete,
      };
    };

    const mockState: AppStore = {
      viewPresetsList: {
        entities: {
          'preset-1': testViewPresets[0],
        },
        ids: ['preset-1'],
      },
      viewPresets: {
        entities: {
          [mapId]: {
            activeViewPresetId: 'preset-1',
            hasChanges: false,
            isFetching: false,
            error: undefined,
            panelId: mapId,
            isViewPresetListDialogOpen: false,
            filters: [],
            searchQuery: '',
            filterResults: { ids: [], entities: {} },
          },
        },
        ids: [mapId],
        viewPresetDialog: {
          title: MAPPRESET_DIALOG_TITLE_DELETE,
          action: PresetAction.DELETE,
          viewPresetId: testViewPresets[0].id,
          panelId: mapId,
          formValues: {
            title: testViewPresets[0].title,
            initialProps: {
              mapPreset: {
                layers: [
                  {
                    name: 'WorldMap_Light_Grey_Canvas',
                    type: 'twms',
                    dimensions: [],
                    id: 'layerid_100',
                    opacity: 1,
                    enabled: true,
                    layerType: LayerType.baseLayer,
                  },
                  {
                    service:
                      'https://geoservices.knmi.nl/wms?DATASET=baselayers&',
                    name: 'countryborders',
                    format: 'image/png',
                    dimensions: [],
                    id: 'layerid_109',
                    opacity: 1,
                    enabled: true,
                    layerType: LayerType.overLayer,
                  },
                ],
                activeLayerId: 'layerid_107',
                autoTimeStepLayerId: 'layerid_107',
                autoUpdateLayerId: 'layerid_107',
                proj: {
                  bbox: {
                    left: -450651.2255879827,
                    bottom: 6490531.093143953,
                    right: 1428345.8183648037,
                    top: 7438773.776232235,
                  },
                  srs: 'EPSG:3857',
                },
                shouldAnimate: false,
                shouldAutoUpdate: false,
                showTimeSlider: true,
                displayMapPin: false,
                shouldShowZoomControls: true,
                animationPayload: {
                  interval: 5,
                  speed: 4,
                },
                toggleTimestepAuto: true,
                shouldShowLegend: false,
              },
              syncGroupsIds: [],
            },
          },
        },
      },
    };
    const store = createMockStoreWithEggs(mockState);
    const testWorkspaceId = 'test-workspace-1';
    render(
      <WorkspaceWrapperProviderWithStore store={store} createApi={createApi}>
        <WorkspaceDetail
          workspaceId={testWorkspaceId}
          componentsLookUp={componentsLookUp}
        />
      </WorkspaceWrapperProviderWithStore>,
    );

    expect(screen.getByTestId('map-preset-dialog')).toBeTruthy();

    fireEvent.click(screen.getByTestId('confirmationDialog-confirm'));
    await waitFor(() => {
      expect(mockDelete).toHaveBeenCalled();
    });
    expect(mockDelete).toHaveBeenCalledWith(testViewPresets[0].id);

    expect(store.getActions()).toEqual([
      workspaceActions.fetchWorkspace({ workspaceId: testWorkspaceId }),
      viewPresetActions.closeViewPresetDialog(),
      viewPresetActions.onSuccessViewPresetAction({
        panelId: mapId,
        viewPresetId: 'preset-1',
        action: PresetAction.DELETE,
        title: testViewPresets[0].title,
      }),
    ]);
  });
});
