/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */
import * as React from 'react';
import { fireEvent, render, screen } from '@testing-library/react';
import { createMockStoreWithEggs } from '@opengeoweb/shared';
import {
  defaultComponentsLookUp,
  ViewError,
  ViewLoading,
} from './DefaultComponentsLookUp';
import { WorkspaceWrapperProviderWithStore } from '../Providers/Providers';
import { workspaceActions } from '../../store/workspace/reducer';
import { AppStore } from '../../store/store';

describe('components/DefaultComponentsLookUp/DefaultComponentsLookUp', () => {
  describe('ViewLoading', () => {
    it('should render with default props', () => {
      const props = {
        viewPresetId: 'test-1',
      };
      render(<ViewLoading {...props} />);

      expect(screen.getByText(`Loading ${props.viewPresetId}`)).toBeTruthy();
    });
  });

  describe('ViewError', () => {
    it('should render with default props', () => {
      const props = {
        viewPresetId: 'test-1',
        mosaicNodeId: 'screen1',
      };
      const mockState: AppStore = {
        workspace: {
          id: 'preset1',
          title: 'Preset 1',
          views: {
            allIds: ['screen1'],
            byId: {
              screen1: {
                title: 'screen 1',
                componentType: 'ViewError',
              },
            },
          },
          mosaicNode: 'screen1',
        },
      };
      const store = createMockStoreWithEggs(mockState);

      render(
        <WorkspaceWrapperProviderWithStore store={store}>
          <ViewError {...props} />
        </WorkspaceWrapperProviderWithStore>,
      );

      expect(
        screen.getByText(`Failed to fetch view ${props.viewPresetId}`),
      ).toBeTruthy();
    });

    it('should be able to refetch a view', () => {
      const props = {
        viewPresetId: 'test-1',
        mosaicNodeId: 'screen1',
      };
      const mockState: AppStore = {
        workspace: {
          id: 'preset1',
          title: 'Preset 1',
          views: {
            allIds: ['screen1'],
            byId: {
              screen1: {
                title: 'screen 1',
                componentType: 'ViewError',
              },
            },
          },
          mosaicNode: 'screen1',
        },
      };
      const store = createMockStoreWithEggs(mockState);

      expect(store.getActions()).toHaveLength(0);

      render(
        <WorkspaceWrapperProviderWithStore store={store}>
          <ViewError {...props} />
        </WorkspaceWrapperProviderWithStore>,
      );

      fireEvent.click(screen.getByText('TRY AGAIN'));

      expect(store.getActions()).toEqual([
        workspaceActions.fetchWorkspaceViewPreset({
          viewPresetId: props.viewPresetId,
          mosaicNodeId: props.mosaicNodeId,
        }),
      ]);
    });
  });
  describe('defaultComponentsLookUp', () => {
    it('should return ViewLoading', () => {
      const props = {
        componentType: 'ViewLoading',
        viewPresetId: 'test-1',
        mosaicNodeId: 'screen1',
      };
      const mockState: AppStore = {
        workspace: {
          id: 'preset1',
          title: 'Preset 1',
          views: {
            allIds: [],
            byId: {},
          },
          mosaicNode: {
            direction: 'row',
            first: 'screen1',
            second: 'screen2',
          },
        },
      };
      const store = createMockStoreWithEggs(mockState);

      render(
        <WorkspaceWrapperProviderWithStore store={store}>
          {defaultComponentsLookUp({
            viewPresetId: props.viewPresetId,
            componentType: props.componentType,
            mosaicNodeId: props.mosaicNodeId,
          })}
        </WorkspaceWrapperProviderWithStore>,
      );
      expect(screen.getByText(`Loading ${props.viewPresetId}`)).toBeTruthy();
    });

    it('should return ViewError', () => {
      const props = {
        componentType: 'ViewError',
        viewPresetId: 'test-1',
        mosaicNodeId: 'screen1',
      };
      const mockState: AppStore = {
        workspace: {
          id: 'preset1',
          title: 'Preset 1',
          views: {
            allIds: [],
            byId: {},
          },
          mosaicNode: {
            direction: 'row',
            first: 'screen1',
            second: 'screen2',
          },
        },
      };
      const store = createMockStoreWithEggs(mockState);

      render(
        <WorkspaceWrapperProviderWithStore store={store}>
          {defaultComponentsLookUp({
            viewPresetId: props.viewPresetId,
            componentType: props.componentType,
            mosaicNodeId: props.mosaicNodeId,
          })}
        </WorkspaceWrapperProviderWithStore>,
      );
      expect(
        screen.getByText(`Failed to fetch view ${props.viewPresetId}`),
      ).toBeTruthy();
    });

    it('should return null', () => {
      const props = {
        componentType: '',
        viewPresetId: 'test-1',
        mosaicNodeId: 'screen1',
      };
      expect(
        defaultComponentsLookUp({
          viewPresetId: props.viewPresetId,
          componentType: props.componentType,
          mosaicNodeId: props.mosaicNodeId,
        }),
      ).toEqual(null);

      const props2 = {
        componentType: 'Map',
        viewPresetId: 'test-1',
        mosaicNodeId: 'screen1',
      };
      expect(
        defaultComponentsLookUp({
          viewPresetId: props2.viewPresetId,
          componentType: props2.componentType,
          mosaicNodeId: props2.mosaicNodeId,
        }),
      ).toEqual(null);
    });
  });
});
