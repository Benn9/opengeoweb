/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */

import Box from '@mui/material/Box';
import { darkTheme, lightTheme } from '@opengeoweb/theme';
import React from 'react';
import { useDispatch } from 'react-redux';
import { store } from '../../store/store';
import { WorkspaceWrapperProviderWithStore } from '../Providers/Providers';
import { WorkspaceTopBar } from './WorkspaceTopBar';
import { workspaceActions } from '../../store/workspace/reducer';
import { WorkspacePreset } from '../../store/workspace/types';

export default {
  title: 'components/Workspace/WorkspaceTopBar',
};

const screenConfig: WorkspacePreset = {
  id: 'preset1',
  title: 'temp',
  views: {
    allIds: ['screen1', 'screen2'],
    byId: {
      screen1: { componentType: 'Map' },
      screen2: { componentType: 'Map' },
    },
  },
  mosaicNode: {
    direction: 'row',
    first: 'screen1',
    second: 'screen2',
  },
};

const screenConfigLongText = {
  ...screenConfig,
  title: 'long title is loooooooooooooooooong',
};

interface WorkspaceTopBarStoryComponentProps {
  screenConfig: WorkspacePreset;
  hasChanges?: boolean;
}

const WorkspaceTopBarStoryComponent: React.FC<WorkspaceTopBarStoryComponentProps> =
  ({ screenConfig, hasChanges = false }) => {
    const dispatch = useDispatch();
    dispatch(workspaceActions.setPreset({ workspacePreset: screenConfig }));
    if (hasChanges) {
      dispatch(
        workspaceActions.updateWorkspaceViews({
          mosaicNode: screenConfig.mosaicNode,
        }),
      );
    }
    return (
      <Box sx={{ width: 260, padding: 1 }}>
        <WorkspaceTopBar workspaceId={screenConfig.id} />
      </Box>
    );
  };

export const WorkspaceTopBarLight = (): React.ReactElement => {
  return (
    <WorkspaceWrapperProviderWithStore theme={lightTheme} store={store}>
      <WorkspaceTopBarStoryComponent screenConfig={screenConfig} />
    </WorkspaceWrapperProviderWithStore>
  );
};

export const WorkspaceTopBarLightModified = (): React.ReactElement => {
  return (
    <WorkspaceWrapperProviderWithStore theme={lightTheme} store={store}>
      <WorkspaceTopBarStoryComponent screenConfig={screenConfig} hasChanges />
    </WorkspaceWrapperProviderWithStore>
  );
};

export const WorkspaceTopBarDark = (): React.ReactElement => {
  return (
    <WorkspaceWrapperProviderWithStore theme={darkTheme} store={store}>
      <WorkspaceTopBarStoryComponent screenConfig={screenConfig} />
    </WorkspaceWrapperProviderWithStore>
  );
};

export const WorkspaceTopBarDarkModified = (): React.ReactElement => {
  return (
    <WorkspaceWrapperProviderWithStore theme={darkTheme} store={store}>
      <WorkspaceTopBarStoryComponent screenConfig={screenConfig} hasChanges />
    </WorkspaceWrapperProviderWithStore>
  );
};

export const WorkspaceTopBarLongText = (): React.ReactElement => {
  return (
    <WorkspaceWrapperProviderWithStore theme={lightTheme} store={store}>
      <WorkspaceTopBarStoryComponent screenConfig={screenConfigLongText} />
    </WorkspaceWrapperProviderWithStore>
  );
};

export const WorkspaceTopBarLongTextModified = (): React.ReactElement => {
  return (
    <WorkspaceWrapperProviderWithStore theme={lightTheme} store={store}>
      <WorkspaceTopBarStoryComponent
        screenConfig={screenConfigLongText}
        hasChanges
      />
    </WorkspaceWrapperProviderWithStore>
  );
};

WorkspaceTopBarLight.parameters = {
  zeplinLink: [
    {
      name: 'Light theme',
      link: 'https://app.zeplin.io/project/5ecf84a3c6ae1047a368f393/screen/64007f6e01c60f40373838dd/version/6408ab735bfa6d1f591c2871',
    },
  ],
};
WorkspaceTopBarLight.storyName = 'WorkspaceTopBarLight (takeSnapshot)';

WorkspaceTopBarLightModified.parameters = {
  zeplinLink: [
    {
      name: ' Light theme',
      link: 'https://app.zeplin.io/project/5ecf84a3c6ae1047a368f393/screen/64007f6e937bcd51624b8008/version/6436c8289bc5b93b9a3fd4c3',
    },
  ],
};
WorkspaceTopBarLightModified.storyName =
  'WorkspaceTopBarLightModified (takeSnapshot)';

WorkspaceTopBarDark.parameters = {
  zeplinLink: [
    {
      name: 'Dark theme',
      link: 'https://app.zeplin.io/project/5ecf84a3c6ae1047a368f393/screen/64107547f399ec0ea49b1cdc/version/64107547f399ec0ea49b1cdd',
    },
  ],
};
WorkspaceTopBarDark.storyName = 'WorkspaceTopBarDark (takeSnapshot)';

WorkspaceTopBarDarkModified.parameters = {
  zeplinLink: [
    {
      name: ' Dark theme',
      link: 'https://app.zeplin.io/project/5ecf84a3c6ae1047a368f393/screen/641198ba66b7870fc97bc1e5/version/6436c837a06a9a36f8f01a01',
    },
  ],
};
WorkspaceTopBarDarkModified.storyName =
  'WorkspaceTopBarDarkModified (takeSnapshot)';

WorkspaceTopBarLongText.storyName = 'WorkspaceTopBarLongText (takeSnapshot)';
WorkspaceTopBarLongTextModified.storyName =
  'WorkspaceTopBarLongTextModified(takeSnapshot)';
