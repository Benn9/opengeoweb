/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */

import React from 'react';
import { render, screen } from '@testing-library/react';
import { lightTheme } from '@opengeoweb/theme';
import { createMockStoreWithEggs } from '@opengeoweb/shared';
import { WorkspaceTopBar } from './WorkspaceTopBar';
import { WorkspaceWrapperProviderWithStore } from '../Providers/Providers';
import { WorkspacePreset } from '../../store/workspace/types';
import { workspaceListActions } from '../../store/workspaceList/reducer';
import { AppStore } from '../../store/store';

describe('workspace/components/WorkspaceTopBar', () => {
  const mosaicNode = {
    direction: 'row',
    first: 'screen1',
    second: 'screen2',
  } as const;
  const screenConfig: WorkspacePreset = {
    id: 'preset1',
    title: 'Preset 1',
    views: {
      allIds: ['screen1', 'screen2'],
      byId: {
        screen1: {
          title: 'screen 1',
          componentType: 'MyTestComponent',
          initialProps: { mapPreset: [{}], syncGroupsIds: [] },
        },
        screen2: {
          title: 'screen 2',
          componentType: 'MyTestComponent',
          initialProps: { mapPreset: [{}], syncGroupsIds: [] },
        },
      },
    },
    mosaicNode,
  };
  it('should render tab', async () => {
    const mockState: AppStore = {
      workspace: screenConfig,
    };
    const store = createMockStoreWithEggs(mockState);
    const props = {
      workspaceId: 'test',
    };
    render(
      <WorkspaceWrapperProviderWithStore theme={lightTheme} store={store}>
        <WorkspaceTopBar {...props} />
      </WorkspaceWrapperProviderWithStore>,
    );
    expect(await screen.findByRole('tablist')).toBeTruthy();
    expect(screen.getByText('Preset 1')).toBeTruthy();

    expect(store.getActions()).toEqual([
      workspaceListActions.fetchWorkspaceList({}),
    ]);
  });

  it('should trigger open workspaceDialog if no workspaceId is given', async () => {
    const mockState: AppStore = {
      workspace: screenConfig,
    };
    const store = createMockStoreWithEggs(mockState);
    render(
      <WorkspaceWrapperProviderWithStore theme={lightTheme} store={store}>
        <WorkspaceTopBar />
      </WorkspaceWrapperProviderWithStore>,
    );
    expect(await screen.findByRole('tablist')).toBeTruthy();
    expect(screen.getByText('Preset 1')).toBeTruthy();

    expect(store.getActions()).toEqual([
      workspaceListActions.fetchWorkspaceList({}),
      workspaceListActions.toggleWorkspaceDialog({
        isWorkspaceListDialogOpen: true,
      }),
    ]);
  });
});
