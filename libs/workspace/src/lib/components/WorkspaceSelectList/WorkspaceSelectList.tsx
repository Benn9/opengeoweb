/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { Box, BoxProps } from '@mui/material';
import { FixedSizeList as List } from 'react-window';
import { getHeight } from '@opengeoweb/shared';
import {
  WorkspacePresetListItem,
  WorkspacePresetAction,
} from '../../store/workspace/types';
import WorkspaceSelectListRow, {
  WORKSPACE_SELECT_LIST_ROW_HEIGHT,
} from './WorkspaceSelectListRow';
import { emptyMapWorkspace } from '../../store/workspaceList/utils';
import { useGetElementHeight } from '../WorkspaceMenu/utils';
import { DEFAULT_FILTER_HEIGHT } from '../WorkspaceFilter/WorkspaceFilter';

export const emptyWorkspaceListItem: WorkspacePresetListItem = {
  id: emptyMapWorkspace.id,
  title: emptyMapWorkspace.title,
  scope: emptyMapWorkspace.scope,
  viewType: emptyMapWorkspace.viewType,
  abstract: emptyMapWorkspace.abstract,
} as WorkspacePresetListItem;

export interface WorkspaceSelectListProps extends BoxProps {
  workspacePresets: WorkspacePresetListItem[];
  currentSelectedId: string;
  onClickPreset: (id: string) => void;
  onClickWorkspacePresetOption: (
    presetId: string,
    workspaceAction: WorkspacePresetAction,
    workspacePreset: WorkspacePresetListItem,
  ) => void;
  hideNewWorkspace?: boolean;
  isLoggedIn?: boolean;
  searchQuery?: string;
  newPreset?: WorkspacePresetListItem;
  testIdPrefix?: string; // Specifies if used for workspace or viewpreset, added to testids
}

export const DEFAULT_DIALOG_HEIGHT = 900;
const MIN_HEIGHT_SCROLLABLE_LIST = 88; // Slightly less than 2 rows
const LIST_PADDING_TOP = 8;
const DEFAULT_PARENT_PADDING = 8;
const DEFAULT_RESULTS_HEIGHT = 22;

const useGetRequiredHeights = (
  ref: React.ForwardedRef<HTMLDivElement>,
): {
  resultsRef: React.RefObject<HTMLDivElement>;
  rowHeight: number;
  listHeight: number;
} => {
  // Used to store the height of the list dialog content
  const [dialogHeight, setDialogHeight] = React.useState<number>(
    DEFAULT_DIALOG_HEIGHT,
  );
  // Used to store the padding height surrounding the list and the filters
  const [parentPaddingHeight, setParentPaddingHeight] = React.useState<number>(
    DEFAULT_PARENT_PADDING,
  );

  // Obtain the height of the results div
  const resultsRef = React.useRef<HTMLDivElement>(null);
  const { height: resultsHeight } = useGetElementHeight(
    resultsRef,
    DEFAULT_RESULTS_HEIGHT,
  );

  // Obtain the height of the filters and search
  const { height: filterHeight } = useGetElementHeight(
    ref as React.RefObject<HTMLDivElement>,
    DEFAULT_FILTER_HEIGHT,
  );

  // Obtain the height of of the list dialog content
  React.useLayoutEffect(() => {
    const dialogElement = resultsRef.current?.parentElement?.parentElement
      ?.parentElement?.parentElement as HTMLDivElement;
    setDialogHeight(dialogElement.offsetHeight);

    const observer = new ResizeObserver(() => {
      const height = getHeight(dialogElement);
      height && setDialogHeight(height);
    });
    observer.observe(dialogElement);

    return (): void => {
      observer.disconnect();
    };
  }, [resultsRef]);

  // Obtain the padding height surrounding the list and the filters
  React.useEffect(() => {
    const parentElement = resultsRef.current?.parentElement
      ?.parentElement as HTMLDivElement;
    const style = getComputedStyle(parentElement);
    const paddingTop = parseInt(style.paddingTop, 10);
    paddingTop && setParentPaddingHeight(paddingTop);
  }, []);

  const heightOtherElements =
    filterHeight + resultsHeight + LIST_PADDING_TOP + 2 * parentPaddingHeight;

  // row height = height + margin
  const rowHeight = WORKSPACE_SELECT_LIST_ROW_HEIGHT + 4;
  const availableSpaceForList = dialogHeight - heightOtherElements;
  const listHeight =
    availableSpaceForList && availableSpaceForList < MIN_HEIGHT_SCROLLABLE_LIST
      ? MIN_HEIGHT_SCROLLABLE_LIST
      : availableSpaceForList;

  return {
    resultsRef,
    rowHeight,
    listHeight,
  };
};

const WorkspaceSelectList: React.FC<WorkspaceSelectListProps> =
  React.forwardRef<HTMLDivElement, WorkspaceSelectListProps>(
    (
      {
        workspacePresets,
        currentSelectedId,
        onClickPreset,
        onClickWorkspacePresetOption,
        hideNewWorkspace,
        isLoggedIn,
        searchQuery,
        newPreset = emptyWorkspaceListItem,
        testIdPrefix = 'workspace',
      }: WorkspaceSelectListProps,
      ref,
    ) => {
      const { resultsRef, rowHeight, listHeight } = useGetRequiredHeights(ref);

      // Height of other elements in dialog besides the list:
      // filter, results text , padding list top and 2x padding around dialog children (top & bottom)
      const items = !hideNewWorkspace
        ? [newPreset, ...workspacePresets]
        : workspacePresets;
      // console.log(filterHeight, resultsHeight, parentPaddingHeight);
      return (
        <Box data-testid={`${testIdPrefix}-selectList`}>
          <Box ref={resultsRef} sx={{ marginBottom: '4px', fontSize: '12px' }}>
            {workspacePresets.length} results
          </Box>
          <Box
            sx={{
              paddingTop: `${LIST_PADDING_TOP}px`,
            }}
          >
            <List
              height={listHeight}
              itemCount={items.length}
              itemSize={rowHeight}
              width="100%"
            >
              {({ index, style }): JSX.Element => {
                const preset = items[index];
                return (
                  <div style={style} key={preset.id || 'emptyPreset'}>
                    <WorkspaceSelectListRow
                      workspacePreset={preset}
                      onClickPreset={onClickPreset}
                      onClickWorkspacePresetOption={
                        onClickWorkspacePresetOption
                      }
                      isEditable={isLoggedIn}
                      searchQuery={searchQuery}
                      isSelected={
                        currentSelectedId === preset.id ||
                        (currentSelectedId === '' && preset.id === undefined)
                      }
                      testIdPrefix={testIdPrefix}
                    />
                  </div>
                );
              }}
            </List>
          </Box>
        </Box>
      );
    },
  );

export default WorkspaceSelectList;
