/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */

import React from 'react';
import { createMockStoreWithEggs } from '@opengeoweb/shared';
import { render, fireEvent, screen } from '@testing-library/react';
import { lightTheme } from '@opengeoweb/theme';
import { WorkspaceWrapperProviderWithStore } from '../Providers/Providers';
import { WorkspaceMenuButtonConnect } from './WorkspaceMenuButtonConnect';
import { workspaceListActions } from '../../store/workspaceList/reducer';

describe('WorkspaceMenuButtonConnect', () => {
  it('should render correctly and trigger workspaceDialog if no workspaceId given', async () => {
    const store = createMockStoreWithEggs({});
    const props = {
      workspaceId: undefined!,
    };

    render(
      <WorkspaceWrapperProviderWithStore theme={lightTheme} store={store}>
        <WorkspaceMenuButtonConnect {...props} />
      </WorkspaceWrapperProviderWithStore>,
    );
    expect(await screen.findByTestId('workspaceMenuButton')).toBeTruthy();
    expect(store.getActions()).toEqual([
      workspaceListActions.fetchWorkspaceList({}),
      workspaceListActions.toggleWorkspaceDialog({
        isWorkspaceListDialogOpen: true,
      }),
    ]);
  });

  it('should render trigger workspaceDialog for empty workspace', async () => {
    const store = createMockStoreWithEggs({});
    const props = {
      workspaceId: '',
    };

    render(
      <WorkspaceWrapperProviderWithStore theme={lightTheme} store={store}>
        <WorkspaceMenuButtonConnect {...props} />
      </WorkspaceWrapperProviderWithStore>,
    );
    expect(store.getActions()).toEqual([
      workspaceListActions.fetchWorkspaceList({}),
      workspaceListActions.toggleWorkspaceDialog({
        isWorkspaceListDialogOpen: true,
      }),
    ]);
  });

  it('should render trigger workspaceDialog for workspace emptyMap', async () => {
    const store = createMockStoreWithEggs({});
    const props = {
      workspaceId: 'emptyMap',
    };

    render(
      <WorkspaceWrapperProviderWithStore theme={lightTheme} store={store}>
        <WorkspaceMenuButtonConnect {...props} />
      </WorkspaceWrapperProviderWithStore>,
    );
    expect(store.getActions()).toEqual([
      workspaceListActions.fetchWorkspaceList({}),
      workspaceListActions.toggleWorkspaceDialog({
        isWorkspaceListDialogOpen: true,
      }),
    ]);
  });

  it('should not trigger workspaceDialog if workspaceId given', async () => {
    const store = createMockStoreWithEggs({});
    const props = {
      workspaceId: 'test-1',
    };

    render(
      <WorkspaceWrapperProviderWithStore theme={lightTheme} store={store}>
        <WorkspaceMenuButtonConnect {...props} />
      </WorkspaceWrapperProviderWithStore>,
    );
    expect(await screen.findByTestId('workspaceMenuButton')).toBeTruthy();
    expect(store.getActions()).toEqual([
      workspaceListActions.fetchWorkspaceList({}),
    ]);
  });

  it('should be able to trigger dialog', async () => {
    const store = createMockStoreWithEggs({});
    const props = {
      workspaceId: 'test-1',
    };

    render(
      <WorkspaceWrapperProviderWithStore theme={lightTheme} store={store}>
        <WorkspaceMenuButtonConnect {...props} />
      </WorkspaceWrapperProviderWithStore>,
    );
    expect(await screen.findByTestId('workspaceMenuButton')).toBeTruthy();
    expect(store.getActions()).toEqual([
      workspaceListActions.fetchWorkspaceList({}),
    ]);

    // toggle
    fireEvent.click(screen.getByTestId('workspaceMenuButton'));

    expect(store.getActions()).toEqual([
      workspaceListActions.fetchWorkspaceList({}),
      workspaceListActions.toggleWorkspaceDialog({
        isWorkspaceListDialogOpen: true,
      }),
    ]);
  });
});
