/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */

import Box from '@mui/material/Box';
import { darkTheme, lightTheme } from '@opengeoweb/theme';
import React from 'react';
import { store } from '../../store';
import { WorkspaceWrapperProviderWithStore } from '../Providers/Providers';
import { WorkspaceMenuButton } from './WorkspaceMenuButton';

export default {
  title: 'components/Workspace/WorkspaceMenuButton',
};

export const WorkspaceMenuButtonLight = (): React.ReactElement => {
  return (
    <Box sx={{ padding: 1 }}>
      <WorkspaceWrapperProviderWithStore theme={lightTheme} store={store}>
        <WorkspaceMenuButton />
      </WorkspaceWrapperProviderWithStore>
    </Box>
  );
};

export const WorkspaceMenuButtonDark = (): React.ReactElement => {
  return (
    <Box sx={{ padding: 1 }}>
      <WorkspaceWrapperProviderWithStore theme={darkTheme} store={store}>
        <WorkspaceMenuButton />
      </WorkspaceWrapperProviderWithStore>
    </Box>
  );
};

WorkspaceMenuButtonLight.parameters = {
  zeplinLink: [
    {
      name: 'Light theme',
      link: 'https://app.zeplin.io/project/5ecf84a3c6ae1047a368f393/screen/64007f6e01c60f40373838dd/',
    },
  ],
};

WorkspaceMenuButtonDark.parameters = {
  zeplinLink: [
    {
      name: 'Dark theme',
      link: 'https://app.zeplin.io/project/5ecf84a3c6ae1047a368f393/screen/64107547f399ec0ea49b1cdc',
    },
  ],
};
