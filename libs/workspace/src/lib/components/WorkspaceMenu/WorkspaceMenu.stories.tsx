/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */

import Box from '@mui/material/Box';
import { darkTheme, lightTheme } from '@opengeoweb/theme';
import React from 'react';
import { createMockStoreWithEggs } from '@opengeoweb/shared';
import { WorkspaceWrapperProviderWithStore } from '../Providers/Providers';
import { WorkspaceMenu } from './WorkspaceMenu';
import { AppStore } from '../../store/store';

export default {
  title: 'components/Workspace/WorkspaceMenu',
};

const mockState: AppStore = {
  workspaceList: {
    error: undefined,
    isFetching: false,
    isWorkspaceListDialogOpen: false,
    ids: ['workspaceList-1', 'workspaceList-2', 'workspaceList-3'],
    entities: {
      'workspaceList-1': {
        id: 'workspaceList-1',
        scope: 'system',
        title: 'Radar',
        date: '2022-06-01T12:34:27.787192',
        viewType: 'singleWindow',
        abstract: 'radar',
      },
      'workspaceList-2': {
        id: 'workspaceList-2',
        scope: 'system',
        title: 'Radar harmonie',
        date: '2022-06-01T12:34:27.787192',
        viewType: 'singleWindow',
        abstract: 'radar and harmonie',
      },
      'workspaceList-3': {
        id: 'workspaceList-3',
        scope: 'system',
        title: 'Harmonie radar',
        date: '2022-06-01T12:34:27.787192',
        viewType: 'singleWindow',
        abstract: '',
      },
    },
    workspaceListFilters: [
      {
        label: 'My presets',
        id: 'user',
        type: 'scope',
        isSelected: true,
        isDisabled: false,
      },
      {
        label: 'System presets',
        id: 'system',
        type: 'scope',
        isSelected: true,
        isDisabled: false,
      },
    ],
    searchQuery: 'rad',
  },
};

export const WorkspaceMenuSearchLight = (): React.ReactElement => {
  const store = createMockStoreWithEggs(mockState);
  return (
    <WorkspaceWrapperProviderWithStore theme={lightTheme} store={store}>
      <Box sx={{ padding: 1, width: 350, height: '100vh' }}>
        <WorkspaceMenu hideBackdrop isOpen={true} closeMenu={(): void => {}} />
      </Box>
    </WorkspaceWrapperProviderWithStore>
  );
};
WorkspaceMenuSearchLight.storyName = 'WorkspaceMenuSearchLight (takeSnapshot)';

export const WorkspaceMenuSearchDark = (): React.ReactElement => {
  const store = createMockStoreWithEggs(mockState);
  return (
    <WorkspaceWrapperProviderWithStore theme={darkTheme} store={store}>
      <Box sx={{ padding: 1, width: 350, height: '100vh' }}>
        <WorkspaceMenu hideBackdrop isOpen={true} closeMenu={(): void => {}} />
      </Box>
    </WorkspaceWrapperProviderWithStore>
  );
};
WorkspaceMenuSearchDark.storyName = 'WorkspaceMenuSearchDark (takeSnapshot)';
