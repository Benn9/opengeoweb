/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */
import * as React from 'react';
import { Box } from '@mui/material';

import { darkTheme, lightTheme } from '@opengeoweb/theme';
import { WorkspaceWrapperProviderWithStore } from '../Providers/Providers';
import { store } from '../../store';
import WorkspaceFilter from './WorkspaceFilter';
import { workspaceListFilterOptions } from '../../store/workspaceList/reducer';

export default {
  title: 'components/Workspace/WorkspaceFilter',
};

export const WorkspaceFilterLight = (): React.ReactElement => {
  return (
    <WorkspaceWrapperProviderWithStore theme={lightTheme} store={store}>
      <Box
        sx={{
          padding: 1,
          width: '400px',
          backgroundColor: 'geowebColors.background.surfaceApp',
        }}
      >
        <WorkspaceFilter filters={workspaceListFilterOptions} isAllSelected />
      </Box>
    </WorkspaceWrapperProviderWithStore>
  );
};

export const WorkspaceFilterDark = (): React.ReactElement => {
  return (
    <WorkspaceWrapperProviderWithStore theme={darkTheme} store={store}>
      <Box
        sx={{
          padding: 1,
          width: '400px',
          backgroundColor: 'geowebColors.background.surfaceApp',
        }}
      >
        <WorkspaceFilter filters={workspaceListFilterOptions} isAllSelected />
      </Box>
    </WorkspaceWrapperProviderWithStore>
  );
};

WorkspaceFilterLight.parameters = {
  zeplinLink: [
    {
      name: 'Light theme',
      link: 'https://app.zeplin.io/project/5ecf84a3c6ae1047a368f393/screen/6405b435ec5def65d04cef1c',
    },
  ],
};
WorkspaceFilterLight.storyName = 'WorkspaceFilterLight (takeSnapshot)';

WorkspaceFilterDark.parameters = {
  zeplinLink: [
    {
      name: 'Dark theme',
      link: 'https://app.zeplin.io/project/5ecf84a3c6ae1047a368f393/screen/62d7b070d54ce81683e9c35b',
    },
  ],
};
WorkspaceFilterDark.storyName = 'WorkspaceFilterDark (takeSnapshot)';
