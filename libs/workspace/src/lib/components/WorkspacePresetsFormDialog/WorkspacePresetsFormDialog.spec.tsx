/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { fireEvent, render, waitFor, screen } from '@testing-library/react';
import { createMockStoreWithEggs } from '@opengeoweb/shared';
import MapPresetsFormDialog, {
  DIALOG_BUTTON_DELETE,
  DIALOG_BUTTON_EDIT,
  DIALOG_BUTTON_SAVE,
  DIALOG_TITLE_DELETE,
  DIALOG_TITLE_DUPLICATE,
  DIALOG_TITLE_SAVE_ALL,
  DIALOG_TITLE_SAVE_AS,
  WorkspacePresetsFormDialogProps,
  getConfirmLabel,
  getDialogTitle,
} from './WorkspacePresetsFormDialog';
import { createApi as createFakeApi } from '../../utils/fakeApi';
import { PresetsApi } from '../../utils/api';
import { WorkspacePresetAction } from '../../store/workspace/types';
import { WorkspaceWrapperProviderWithStore } from '../Providers/Providers';

describe('components/MapPresetsDialog/MapPresetsFormDialog', () => {
  it('should cancel save as', async () => {
    const store = createMockStoreWithEggs({});
    const props: WorkspacePresetsFormDialogProps = {
      isOpen: true,
      action: WorkspacePresetAction.DUPLICATE,
      presetId: 'Map',
      formValues: {
        title: 'Map preset',
        abstract: 'some abstract',
      },
      onClose: jest.fn(),
      onFormSubmit: jest.fn(),
      isLoading: false,
    };

    render(
      <WorkspaceWrapperProviderWithStore store={store}>
        <MapPresetsFormDialog {...props} />
      </WorkspaceWrapperProviderWithStore>,
    );

    expect(await screen.findByTestId('workspace-preset-dialog')).toBeTruthy();
    expect(screen.getByTestId('workspace-preset-form')).toBeTruthy();

    // should cancel
    fireEvent.click(screen.getByTestId('confirmationDialog-cancel'));
    expect(props.onClose).toHaveBeenCalledTimes(1);
    expect(props.onFormSubmit).not.toHaveBeenCalled();
  });

  it('should show spinner when loading', async () => {
    const store = createMockStoreWithEggs({});
    const props: WorkspacePresetsFormDialogProps = {
      isOpen: true,
      action: WorkspacePresetAction.DUPLICATE,
      presetId: 'Map',
      formValues: {
        title: 'Map preset',
        abstract: 'some abstract',
      },
      onClose: jest.fn(),
      onFormSubmit: jest.fn(),
      isLoading: true,
    };

    render(
      <WorkspaceWrapperProviderWithStore store={store}>
        <MapPresetsFormDialog {...props} />
      </WorkspaceWrapperProviderWithStore>,
    );

    expect(screen.getByTestId('confirm-dialog-spinner')).toBeTruthy();
  });

  it('should close after duplicate', async () => {
    const store = createMockStoreWithEggs({});
    const props = {
      isOpen: true,
      action: WorkspacePresetAction.DUPLICATE,
      presetId: 'Map',
      formValues: {
        title: 'Map preset',
      },
      onClose: jest.fn(),
      onFormSubmit: jest.fn(),
      isLoading: false,
    };

    render(
      <WorkspaceWrapperProviderWithStore store={store}>
        <MapPresetsFormDialog {...props} />
      </WorkspaceWrapperProviderWithStore>,
    );

    expect(await screen.findByTestId('workspace-preset-dialog')).toBeTruthy();
    expect(screen.getByTestId('workspace-preset-form')).toBeTruthy();

    // should close
    fireEvent.click(screen.getByTestId('customDialog-close'));
    expect(props.onClose).toHaveBeenCalledTimes(1);
    expect(props.onFormSubmit).not.toHaveBeenCalled();
  });

  it('should call onFormSubmit for duplicationg a preset', async () => {
    const store = createMockStoreWithEggs({});
    const testPresetFromBE = {
      title: 'Radar and Radar',
      scope: 'user' as const,
      abstract: 'This can contain an abstract',
      viewType: 'multiWindow' as const,
      views: [
        { mosaicNodeId: 'view-1', viewPresetId: 'radar' },
        { mosaicNodeId: 'view-2', viewPresetId: 'radar' },
      ],
      syncGroups: [
        { id: 'Area_RadarTemp', type: 'SYNCGROUPS_TYPE_SETBBOX' as const },
        { id: 'Time_RadarTemp', type: 'SYNCGROUPS_TYPE_SETTIME' as const },
      ],
      mosaicNode: 'test-view',
    };

    const props = {
      isOpen: true,
      action: WorkspacePresetAction.DUPLICATE,
      presetId: 'Map',
      formValues: testPresetFromBE,
      onClose: jest.fn(),
      onFormSubmit: jest.fn(),
      isLoading: false,
    } as WorkspacePresetsFormDialogProps;

    render(
      <WorkspaceWrapperProviderWithStore store={store}>
        <MapPresetsFormDialog {...props} />
      </WorkspaceWrapperProviderWithStore>,
    );

    expect(await screen.findByTestId('workspace-preset-dialog')).toBeTruthy();
    expect(screen.getByTestId('workspace-preset-form')).toBeTruthy();

    // should close
    fireEvent.click(screen.getByTestId('confirmationDialog-confirm'));
    await waitFor(() => {
      expect(props.onFormSubmit).toHaveBeenCalledWith(
        WorkspacePresetAction.DUPLICATE,
        props.presetId,
        props.formValues,
      );
    });
  });

  it('should call onFormSubmit for saving preset as', async () => {
    const store = createMockStoreWithEggs({});
    const testPresetFromBE = {
      title: 'Radar and Radar',
      scope: 'user' as const,
      abstract: 'This can contain an abstract',
      viewType: 'multiWindow' as const,
      views: [
        { mosaicNodeId: 'view-1', viewPresetId: 'radar' },
        { mosaicNodeId: 'view-2', viewPresetId: 'radar' },
      ],
      syncGroups: [
        { id: 'Area_RadarTemp', type: 'SYNCGROUPS_TYPE_SETBBOX' as const },
        { id: 'Time_RadarTemp', type: 'SYNCGROUPS_TYPE_SETTIME' as const },
      ],
      mosaicNode: 'test-view',
    };

    const props = {
      isOpen: true,
      action: WorkspacePresetAction.SAVE_AS,
      presetId: 'Map',
      formValues: testPresetFromBE,
      onClose: jest.fn(),
      onFormSubmit: jest.fn(),
      isLoading: false,
    };

    render(
      <WorkspaceWrapperProviderWithStore store={store}>
        <MapPresetsFormDialog {...props} />
      </WorkspaceWrapperProviderWithStore>,
    );

    expect(await screen.findByTestId('workspace-preset-dialog')).toBeTruthy();
    expect(screen.getByTestId('workspace-preset-form')).toBeTruthy();

    // should close
    fireEvent.click(screen.getByTestId('confirmationDialog-confirm'));
    await waitFor(() => {
      expect(props.onFormSubmit).toHaveBeenCalledWith(
        WorkspacePresetAction.SAVE_AS,
        props.presetId,
        props.formValues,
      );
    });
  });

  it('should not post empty title', async () => {
    const store = createMockStoreWithEggs({});
    const props = {
      isOpen: true,
      action: WorkspacePresetAction.DUPLICATE,
      presetId: 'Map workspace',
      formValues: {
        title: '',
      },
      onClose: jest.fn(),
      onFormSubmit: jest.fn(),
      isLoading: false,
    };

    render(
      <WorkspaceWrapperProviderWithStore store={store}>
        <MapPresetsFormDialog {...props} />
      </WorkspaceWrapperProviderWithStore>,
    );

    const dialog = await screen.findByTestId('workspace-preset-dialog');
    expect(screen.getByTestId('workspace-preset-form')).toBeTruthy();
    expect(dialog).toBeTruthy();

    // try to post
    fireEvent.click(screen.getByTestId('confirmationDialog-confirm'));
    await waitFor(() => {
      expect(screen.getByRole('alert')).toBeTruthy();
    });

    expect(props.onClose).toHaveBeenCalledTimes(0);
    expect(await screen.findByText('This field is required')).toBeTruthy();

    fireEvent.change(screen.getByRole('textbox', { name: /Name/i }), {
      target: {
        value: 'new dialog title',
      },
    });

    await waitFor(() => {
      expect(screen.queryByRole('alert')).toBeFalsy();
    });

    fireEvent.click(screen.getByTestId('confirmationDialog-confirm'));

    await waitFor(() => {
      expect(props.onFormSubmit).toHaveBeenCalledTimes(1);
    });
  });

  it('should show error', async () => {
    const store = createMockStoreWithEggs({});
    const props: WorkspacePresetsFormDialogProps = {
      isOpen: true,
      action: WorkspacePresetAction.DUPLICATE,
      presetId: 'workspace1',
      formValues: {
        title: 'workspace preset',
        abstract: 'some abstract',
      },
      onClose: jest.fn(),
      onFormSubmit: jest.fn(),
      isLoading: false,
      error: 'this is a test error message',
    };

    const error = new Error('test error');
    const retrieveWorkspaceListPresetAsWatcher = jest.fn(
      () =>
        new Promise((resolve) => {
          resolve(props.presetId);
        }),
    );

    const createApi = (): PresetsApi => {
      return {
        ...createFakeApi(),
        getWorkspacePreset:
          retrieveWorkspaceListPresetAsWatcher as unknown as PresetsApi['getWorkspacePreset'],
        saveWorkspacePresetAs: (): Promise<string> => {
          return new Promise((resolve, reject) => {
            reject(error);
          });
        },
      };
    };

    render(
      <WorkspaceWrapperProviderWithStore store={store} createApi={createApi}>
        <MapPresetsFormDialog {...props} />
      </WorkspaceWrapperProviderWithStore>,
    );

    expect(screen.getByTestId('workspace-preset-dialog')).toBeTruthy();
    expect(screen.getByTestId('workspace-preset-form')).toBeTruthy();

    expect(screen.getByTestId('alert-banner')).toBeTruthy();
    expect(screen.getByText(props.error!)).toBeTruthy();
  });

  it('should delete a preset', async () => {
    const store = createMockStoreWithEggs({});
    const props: WorkspacePresetsFormDialogProps = {
      isOpen: true,
      action: WorkspacePresetAction.DELETE,
      presetId: 'test123',
      formValues: {
        title: 'workspace preset 1',
        abstract: 'some abstract',
      },
      onClose: jest.fn(),
      onFormSubmit: jest.fn(),
      isLoading: false,
    };

    render(
      <WorkspaceWrapperProviderWithStore store={store}>
        <MapPresetsFormDialog {...props} />
      </WorkspaceWrapperProviderWithStore>,
    );

    expect(screen.getByTestId('workspace-preset-dialog')).toBeTruthy();

    // should delete
    fireEvent.click(screen.getByTestId('confirmationDialog-confirm'));

    await waitFor(() => {
      expect(props.onFormSubmit).toHaveBeenCalledWith(
        props.action,
        props.presetId,
        props.formValues,
      );
    });
  });

  it('should show correct data when reopening', async () => {
    const store = createMockStoreWithEggs({});
    const props = {
      isOpen: true,
      action: WorkspacePresetAction.DUPLICATE,
      presetId: 'workspace123',
      formValues: {
        title: 'test title 1',
        abstract: 'some abstract',
      },
      onClose: jest.fn(),
      onFormSubmit: jest.fn(),
      isLoading: false,
    };

    const { rerender } = render(
      <WorkspaceWrapperProviderWithStore store={store}>
        <MapPresetsFormDialog {...props} />
      </WorkspaceWrapperProviderWithStore>,
    );

    expect(await screen.findByTestId('workspace-preset-dialog')).toBeTruthy();
    expect(screen.getByTestId('workspace-preset-form')).toBeTruthy();
    expect(
      screen.getByRole('textbox', { name: 'Name' }).getAttribute('value'),
    ).toEqual(props.formValues.title);
    expect(
      screen.getByRole('textbox', { name: 'Abstract' }).textContent,
    ).toEqual(props.formValues.abstract);

    // should cancel
    fireEvent.click(screen.getByTestId('confirmationDialog-cancel'));
    expect(props.onClose).toHaveBeenCalledTimes(1);
    expect(props.onFormSubmit).not.toHaveBeenCalled();

    const newProps = {
      ...props,
      formValues: { title: 'test title 2', abstract: 'new abstract' },
    };
    rerender(
      <WorkspaceWrapperProviderWithStore store={store}>
        <MapPresetsFormDialog {...newProps} />
      </WorkspaceWrapperProviderWithStore>,
    );

    expect(await screen.findByTestId('workspace-preset-dialog')).toBeTruthy();
    expect(screen.getByTestId('workspace-preset-form')).toBeTruthy();
    expect(
      screen.getByRole('textbox', { name: 'Name' }).getAttribute('value'),
    ).toEqual(newProps.formValues.title);
    expect(
      screen.getByRole('textbox', { name: 'Abstract' }).textContent,
    ).toEqual(newProps.formValues.abstract);
  });
  describe('getDialogTitle', () => {
    it('should return the correct title based on the action passed', () => {
      expect(getDialogTitle(WorkspacePresetAction.DELETE)).toEqual(
        DIALOG_TITLE_DELETE,
      );
      expect(getDialogTitle(WorkspacePresetAction.DUPLICATE)).toEqual(
        DIALOG_TITLE_DUPLICATE,
      );
      expect(getDialogTitle(WorkspacePresetAction.SAVE_AS)).toEqual(
        DIALOG_TITLE_SAVE_AS,
      );
      expect(getDialogTitle(WorkspacePresetAction.SAVE_ALL)).toEqual(
        DIALOG_TITLE_SAVE_ALL,
      );
      expect(getDialogTitle(WorkspacePresetAction.EDIT)).toEqual(
        DIALOG_BUTTON_EDIT,
      );

      expect(
        getDialogTitle('SOME FAKE ACTION' as WorkspacePresetAction),
      ).toEqual(DIALOG_TITLE_SAVE_AS);
    });
  });

  describe('getConfirmLabel', () => {
    it('should return the correct label based on the action passed', () => {
      expect(getConfirmLabel(WorkspacePresetAction.DELETE)).toEqual(
        DIALOG_BUTTON_DELETE,
      );
      expect(getConfirmLabel(WorkspacePresetAction.DUPLICATE)).toEqual(
        DIALOG_BUTTON_SAVE,
      );
      expect(getConfirmLabel(WorkspacePresetAction.SAVE_ALL)).toEqual(
        DIALOG_BUTTON_SAVE,
      );

      expect(
        getConfirmLabel('SOME FAKE ACTION' as WorkspacePresetAction),
      ).toEqual(DIALOG_BUTTON_SAVE);
    });
  });
});
