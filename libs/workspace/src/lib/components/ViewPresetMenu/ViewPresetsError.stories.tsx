/* *
/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { StoryWrapper } from '../../storyUtils/StoryWrapper';
import { createApi as createFakeApi } from '../../utils/fakeApi';
import {
  ViewPresetListItem,
  WorkspacePresetFromBE,
} from '../../store/workspace/types';
import { PresetsApi } from '../../utils/api';
import { useFirstTimeError } from '../../storyUtils/useFirstTimeError';
import { ViewPreset } from '../../store/viewPresets/types';

export default {
  title: 'components/ViewPresets/ViewPreset Errors',
};

const testId = 'test-1';
const fakeErrorApi = {
  getWorkspacePreset: (): Promise<{ data: WorkspacePresetFromBE }> => {
    const testWorkspace: WorkspacePresetFromBE = {
      id: testId,
      title: 'Test view preset error',
      scope: 'system',
      abstract: '',
      viewType: 'multiWindow',
      views: [{ mosaicNodeId: 'screen_no_1', viewPresetId: 'screenRadar' }],
      syncGroups: [
        {
          id: 'Area_RadarTemp',
          type: 'SYNCGROUPS_TYPE_SETBBOX',
        },
      ],
      mosaicNode: 'screen_no_1',
    };

    return new Promise((resolve) => {
      resolve({ data: testWorkspace });
    });
  },
};

export const GetViewPresetsError = (): React.ReactElement => {
  const { fetch } = useFirstTimeError();
  const createFakeApiWithError = (): PresetsApi => {
    const currentFakeApi = createFakeApi();
    return {
      ...currentFakeApi,
      ...fakeErrorApi,
      getViewPresets: (): Promise<{ data: ViewPresetListItem[] }> => {
        return fetch(currentFakeApi.getViewPresets());
      },
    };
  };
  return (
    <StoryWrapper workspaceId={testId} createApi={createFakeApiWithError} />
  );
};
GetViewPresetsError.storyName = `getViewPresets`;

export const DeleteViewPresetError = (): React.ReactElement => {
  const { fetch } = useFirstTimeError();
  const createFakeApiWithError = (): PresetsApi => {
    const currentFakeApi = createFakeApi();
    return {
      ...currentFakeApi,
      ...fakeErrorApi,
      deleteViewPreset: (presetId: string): Promise<void> => {
        return fetch(currentFakeApi.deleteViewPreset(presetId));
      },
    };
  };
  return (
    <StoryWrapper workspaceId={testId} createApi={createFakeApiWithError} />
  );
};
DeleteViewPresetError.storyName = `deleteViewPreset`;

export const DuplicateViewPresetError = (): React.ReactElement => {
  const { fetch } = useFirstTimeError();
  const createFakeApiWithError = (): PresetsApi => {
    const currentFakeApi = createFakeApi();
    return {
      ...currentFakeApi,
      ...fakeErrorApi,
      saveViewPresetAs: (data: ViewPreset): Promise<string> => {
        return fetch(currentFakeApi.saveViewPresetAs(data));
      },
    };
  };
  return (
    <StoryWrapper workspaceId={testId} createApi={createFakeApiWithError} />
  );
};
DuplicateViewPresetError.storyName = `duplicateViewPreset`;

export const SaveViewPresetError = (): React.ReactElement => {
  const { fetch } = useFirstTimeError();
  const createFakeApiWithError = (): PresetsApi => {
    const currentFakeApi = createFakeApi();
    return {
      ...currentFakeApi,
      ...fakeErrorApi,
      saveViewPreset: (presetId: string, data: ViewPreset): Promise<void> => {
        return fetch(currentFakeApi.saveViewPreset(presetId, data));
      },
    };
  };
  return (
    <StoryWrapper workspaceId={testId} createApi={createFakeApiWithError} />
  );
};
SaveViewPresetError.storyName = `saveViewPreset`;
