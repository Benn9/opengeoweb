/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */

import { Box, Typography } from '@mui/material';
import { mapTypes } from '@opengeoweb/store';
import {
  ReactHookFormHiddenInput,
  ReactHookFormTextField,
} from '@opengeoweb/form-fields';
import { AlertBanner } from '@opengeoweb/shared';
import React from 'react';
import { PresetAction } from '../../store/viewPresets/types';
import { emptyMapViewPreset } from '../../store/viewPresets/utils';

const validateTitle = (value: string): boolean | string => {
  if (!value) {
    return true;
  }
  return !value.trim().length ? 'This field is required' : true;
};

export type MapPresetFormValues = {
  title?: string;
  initialProps?: {
    mapPreset: mapTypes.MapPreset;
    syncGroupsIds?: string[];
  };
};

interface MapPresetsFormProps {
  error?: string;
  onSubmit?: () => void;
  action: PresetAction;
  formValues: MapPresetFormValues;
}

const MapPresetsForm: React.FC<MapPresetsFormProps> = ({
  error,
  onSubmit,
  action,
  formValues,
}: MapPresetsFormProps) => {
  const fieldName = 'title';
  const { title, ...otherFormValues } = {
    ...emptyMapViewPreset,
    ...formValues,
  };
  const hiddenFields = Object.keys(otherFormValues).map((name) => ({
    name,
    value: otherFormValues[name as keyof typeof otherFormValues],
  }));

  React.useEffect(() => {
    // autoFocus
    const field = document.querySelector(
      `[name="${fieldName}"]`,
    ) as HTMLInputElement;
    field?.focus();
  }, []);

  const onSubmitForm = (event: React.FormEvent<HTMLFormElement>): void => {
    event.preventDefault();
    onSubmit!();
  };

  return (
    <>
      {Boolean(error) && (
        <Box sx={{ marginBottom: 2 }}>
          <AlertBanner title={error!} />
        </Box>
      )}
      {(action === PresetAction.SAVE_AS || action === PresetAction.EDIT) && (
        <form data-testid="map-preset-form" onSubmit={onSubmitForm}>
          <ReactHookFormTextField
            name="title"
            label="Map preset name"
            rules={{
              required: true,
              validate: {
                validateTitle,
              },
            }}
            defaultValue={title}
          />
          {hiddenFields.map(({ name, value }) => (
            <ReactHookFormHiddenInput
              key={name}
              name={name}
              defaultValue={value}
              data-testid={name}
            />
          ))}
        </form>
      )}
      {action === PresetAction.DELETE && (
        <>
          <Typography variant="body1">
            {`Are you sure to want to delete "${title}" map preset?`}
          </Typography>
          <ReactHookFormHiddenInput
            key="title"
            name="title"
            defaultValue={title}
          />
        </>
      )}
    </>
  );
};

export default MapPresetsForm;
