/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */

import { createStore } from '@redux-eggs/redux-toolkit';
import { darkTheme } from '@opengeoweb/theme';
import React from 'react';
import { AxiosError, AxiosResponseHeaders } from 'axios';
import { Box } from '@mui/material';
import MapPresetsFormDialog from './MapPresetsFormDialog';
import { createApi as createFakeApi } from '../../utils/fakeApi';
import { PresetsApi } from '../../utils/api';
import { PresetAction } from '../../store/viewPresets/types';
import { WorkspaceWrapperProviderWithStore } from '../Providers';
import {
  MAPPRESET_DIALOG_TITLE_SAVE_AS,
  MAPPRESET_DIALOG_TITLE_DELETE,
  MAPPRESET_DIALOG_TITLE_EDIT,
} from '../../store/viewPresets/utils';

export default { title: 'components/ViewPresets/MapPresetsFormDialog' };

const store = createStore();

const SaveAsDemo = (): React.ReactElement => (
  <Box sx={{ minHeight: 1080 }}>
    <MapPresetsFormDialog
      isOpen
      title={MAPPRESET_DIALOG_TITLE_SAVE_AS}
      action={PresetAction.SAVE_AS}
      viewPresetId="Map"
      formValues={{
        title: 'Map preset',
      }}
      onClose={(): void => {}}
      onSuccess={(): void => {}}
    />
  </Box>
);

export const SaveAsDemoLightTheme = (): React.ReactElement => (
  <WorkspaceWrapperProviderWithStore store={store}>
    <SaveAsDemo />
  </WorkspaceWrapperProviderWithStore>
);

export const SaveAsDemoDarkTheme = (): React.ReactElement => (
  <WorkspaceWrapperProviderWithStore theme={darkTheme} store={store}>
    <SaveAsDemo />
  </WorkspaceWrapperProviderWithStore>
);

SaveAsDemoLightTheme.storyName = 'Save As light theme (takeSnapshot)';

SaveAsDemoDarkTheme.storyName = 'Save As dark theme (takeSnapshot)';

const DeleteDemo = (): React.ReactElement => (
  <Box sx={{ minHeight: 1080 }}>
    <MapPresetsFormDialog
      isOpen
      title={MAPPRESET_DIALOG_TITLE_DELETE}
      action={PresetAction.DELETE}
      viewPresetId="Map"
      formValues={{
        title: 'Radar',
      }}
      onClose={(): void => {}}
      onSuccess={(): void => {}}
    />
  </Box>
);

export const DeleteDemoLightTheme = (): React.ReactElement => (
  <WorkspaceWrapperProviderWithStore store={store}>
    <DeleteDemo />
  </WorkspaceWrapperProviderWithStore>
);

export const DeleteDemoDarkTheme = (): React.ReactElement => (
  <WorkspaceWrapperProviderWithStore theme={darkTheme} store={store}>
    <DeleteDemo />
  </WorkspaceWrapperProviderWithStore>
);

DeleteDemoLightTheme.storyName = 'Delete light theme (takeSnapshot)';

DeleteDemoDarkTheme.storyName = 'Delete dark theme (takeSnapshot)';

const EditDemo = (): React.ReactElement => (
  <Box sx={{ minHeight: 1080 }}>
    <MapPresetsFormDialog
      isOpen
      title={MAPPRESET_DIALOG_TITLE_EDIT}
      action={PresetAction.EDIT}
      viewPresetId="Map"
      formValues={{
        title: 'Radar',
      }}
      onClose={(): void => {}}
      onSuccess={(): void => {}}
    />
  </Box>
);

export const EditDemoLightTheme = (): React.ReactElement => (
  <WorkspaceWrapperProviderWithStore store={store}>
    <EditDemo />
  </WorkspaceWrapperProviderWithStore>
);

export const EditDemoDarkTheme = (): React.ReactElement => (
  <WorkspaceWrapperProviderWithStore theme={darkTheme} store={store}>
    <EditDemo />
  </WorkspaceWrapperProviderWithStore>
);

const fakeBackendErrorSaveAs: AxiosError = {
  isAxiosError: true,
  config: undefined!,
  toJSON: undefined!,
  name: 'API error',
  message: '',
  response: {
    data: 'Could not save preset: Preset title already in use, please choose a different title',
    status: 400,
    statusText: '',
    config: undefined!,
    headers: [] as unknown as AxiosResponseHeaders,
  },
};

const fakeBackendErrorDelete: AxiosError = {
  isAxiosError: true,
  config: undefined!,
  toJSON: undefined!,
  name: 'API error',
  message: '',
  response: {
    data: 'Could not delete preset: Error deleting preset',
    status: 400,
    statusText: '',
    config: undefined!,
    headers: [] as unknown as AxiosResponseHeaders,
  },
};

const createApiWithErrors = (): PresetsApi => {
  return {
    ...createFakeApi(),
    saveViewPresetAs: (): Promise<string> => {
      return new Promise((_, reject) => {
        setTimeout(() => {
          reject(fakeBackendErrorSaveAs);
        }, 1000);
      });
    },
    deleteViewPreset: (): Promise<void> => {
      return new Promise((_, reject) => {
        setTimeout(() => {
          reject(fakeBackendErrorDelete);
        }, 1000);
      });
    },
  };
};

export const SaveAsWithError = (): React.ReactElement => (
  <WorkspaceWrapperProviderWithStore
    store={store}
    createApi={createApiWithErrors}
  >
    <SaveAsDemo />
  </WorkspaceWrapperProviderWithStore>
);

export const DeleteWithError = (): React.ReactElement => (
  <WorkspaceWrapperProviderWithStore
    store={store}
    createApi={createApiWithErrors}
  >
    <DeleteDemo />
  </WorkspaceWrapperProviderWithStore>
);
