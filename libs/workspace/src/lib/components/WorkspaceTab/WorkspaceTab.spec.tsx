/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */

import React from 'react';
import { createMockStoreWithEggs } from '@opengeoweb/shared';
import { render, screen } from '@testing-library/react';
import { lightTheme } from '@opengeoweb/theme';
import { WorkspaceTab } from './WorkspaceTab';
import { WorkspaceWrapperProviderWithStore } from '../Providers/Providers';
import { workspaceListActions } from '../../store/workspaceList/reducer';

describe('WorkspaceMenuButton', () => {
  it('should render correctly', async () => {
    const props = {
      title: 'title',
      activeTab: true,
    };
    const store = createMockStoreWithEggs({});
    render(
      <WorkspaceWrapperProviderWithStore store={store} theme={lightTheme}>
        <WorkspaceTab {...props} />
      </WorkspaceWrapperProviderWithStore>,
    );
    expect(await screen.findByTestId('workspaceTab')).toBeTruthy();
    expect(screen.getByText('title')).toBeTruthy();

    expect(store.getActions()).toEqual([
      workspaceListActions.fetchWorkspaceList({}),
      workspaceListActions.toggleWorkspaceDialog({
        isWorkspaceListDialogOpen: true,
      }),
    ]);
  });

  it('should render tab without activeTab', async () => {
    const props = {
      title: 'title',
    };
    const store = createMockStoreWithEggs({});
    render(
      <WorkspaceWrapperProviderWithStore store={store} theme={lightTheme}>
        <WorkspaceTab {...props} />
      </WorkspaceWrapperProviderWithStore>,
    );
    expect(await screen.findByTestId('workspaceTab')).toBeTruthy();
    expect(screen.getByText('title')).toBeTruthy();
  });

  it('should not open the menu on map with workspaceId', async () => {
    const props = {
      title: 'title',
      activeTab: true,
      workspaceId: 'test-1',
    };
    const store = createMockStoreWithEggs({});
    render(
      <WorkspaceWrapperProviderWithStore store={store} theme={lightTheme}>
        <WorkspaceTab {...props} />
      </WorkspaceWrapperProviderWithStore>,
    );
    expect(await screen.findByTestId('workspaceTab')).toBeTruthy();
    expect(screen.getByText('title')).toBeTruthy();

    expect(store.getActions()).toEqual([
      workspaceListActions.fetchWorkspaceList({}),
    ]);
  });
});
