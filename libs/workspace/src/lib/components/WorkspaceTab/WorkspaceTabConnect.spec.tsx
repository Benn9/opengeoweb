/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */

import React from 'react';
import { fireEvent, render, screen, waitFor } from '@testing-library/react';
import { lightTheme } from '@opengeoweb/theme';
import { createMockStoreWithEggs } from '@opengeoweb/shared';
import { WorkspaceTabConnect } from './WorkspaceTabConnect';
import { WorkspaceWrapperProviderWithStore } from '../Providers/Providers';
import {
  WorkspacePreset,
  WorkspacePresetAction,
} from '../../store/workspace/types';
import { WorkspaceListState } from '../../store/workspaceList/types';
import { workspaceListActions } from '../../store/workspaceList/reducer';
import { AppStore } from '../../store/store';

describe('workspace/components/WorkspaceTabConnect', () => {
  const mosaicNode = {
    direction: 'row',
    first: 'screen1',
    second: 'screen2',
  } as const;
  const screenConfig: WorkspacePreset = {
    id: 'preset1',
    title: 'Preset 1',
    views: {
      allIds: ['screen1', 'screen2'],
      byId: {
        screen1: {
          title: 'screen 1',
          componentType: 'MyTestComponent',
          initialProps: { mapPreset: [{}], syncGroupsIds: [] },
        },
        screen2: {
          title: 'screen 2',
          componentType: 'MyTestComponent',
          initialProps: { mapPreset: [{}], syncGroupsIds: [] },
        },
      },
    },
    mosaicNode,
  };
  it('should render the title, workspacemenu should be hidden', async () => {
    const mockState: AppStore = {
      workspace: screenConfig,
    };
    const store = createMockStoreWithEggs(mockState);
    const props = {
      workspaceId: 'test-1',
    };
    render(
      <WorkspaceWrapperProviderWithStore theme={lightTheme} store={store}>
        <WorkspaceTabConnect {...props} />
      </WorkspaceWrapperProviderWithStore>,
    );
    expect(screen.getByText('Preset 1')).toBeTruthy();
    expect(
      (await screen.findByTestId('workspaceMenuBackdrop'))
        .getAttribute('style')!
        .includes('visibility: hidden'),
    ).toBeTruthy();
  });

  const screenConfigEmptyView: WorkspacePreset = {
    id: 'emptyMap',
    title: 'Empty map',
    views: {
      allIds: ['emptyMap'],
      byId: {
        emptyMapView: {
          title: 'Empty map',
          componentType: 'map',
          initialProps: { mapPreset: [{}], syncGroupsIds: [] },
        },
      },
    },
    mosaicNode,
  };
  it('should open the menu on an empty map', async () => {
    const mockState: AppStore = {
      workspace: screenConfigEmptyView,
    };
    const store = createMockStoreWithEggs(mockState);
    const props = {
      workspaceId: undefined!,
    };
    render(
      <WorkspaceWrapperProviderWithStore theme={lightTheme} store={store}>
        <WorkspaceTabConnect {...props} />
      </WorkspaceWrapperProviderWithStore>,
    );
    expect(store.getActions()).toEqual([
      workspaceListActions.fetchWorkspaceList({}),
      workspaceListActions.toggleWorkspaceDialog({
        isWorkspaceListDialogOpen: true,
      }),
    ]);
  });

  it('should not open the menu on map with workspaceId', async () => {
    const mockState: AppStore = {
      workspace: screenConfigEmptyView,
    };
    const store = createMockStoreWithEggs(mockState);
    const props = {
      workspaceId: 'test',
    };
    render(
      <WorkspaceWrapperProviderWithStore theme={lightTheme} store={store}>
        <WorkspaceTabConnect {...props} />
      </WorkspaceWrapperProviderWithStore>,
    );
    expect(store.getActions()).toEqual([
      workspaceListActions.fetchWorkspaceList({}),
    ]);
  });

  it('should not include the action dialog if no options set in store', () => {
    const mockState: AppStore = {
      workspace: screenConfigEmptyView,
      workspaceList: {
        isFetching: false,
        error: undefined,
        isWorkspaceListDialogOpen: false,
        ids: ['workspaceList-1'],
        entities: {
          'workspaceList-1': {
            id: 'workspaceList-1',
            scope: 'system' as const,
            title: 'Workspace list item',
            date: '2022-06-01T12:34:27.787192',
            viewType: 'singleWindow' as const,
            abstract: '',
          },
        },
      } as WorkspaceListState,
    };
    const store = createMockStoreWithEggs(mockState);
    render(
      <WorkspaceWrapperProviderWithStore theme={lightTheme} store={store}>
        <WorkspaceTabConnect workspaceId="workspaceList-1" />
      </WorkspaceWrapperProviderWithStore>,
    );
    expect(screen.queryByTestId('workspace-preset-dialog')).toBeFalsy();
  });

  it('should include the action dialog if options set in store, closing should dispatch a call', () => {
    const mockState: AppStore = {
      workspace: screenConfigEmptyView,
      workspaceList: {
        isFetching: false,
        error: undefined,
        isWorkspaceListDialogOpen: false,
        workspaceActionDialog: {
          action: WorkspacePresetAction.DELETE,
          presetId: 'workspaceList-1',
          formValues: {
            title: 'Workspace list item',
          },
        },
        ids: ['workspaceList-1'],
        entities: {
          'workspaceList-1': {
            id: 'workspaceList-1',
            scope: 'system' as const,
            title: 'Workspace list item',
            date: '2022-06-01T12:34:27.787192',
            viewType: 'singleWindow' as const,
            abstract: '',
          },
        },
      } as WorkspaceListState,
    };
    const store = createMockStoreWithEggs(mockState);
    render(
      <WorkspaceWrapperProviderWithStore theme={lightTheme} store={store}>
        <WorkspaceTabConnect workspaceId="workspaceList-1" />
      </WorkspaceWrapperProviderWithStore>,
    );
    expect(screen.getByTestId('workspace-preset-dialog')).toBeTruthy();
    expect(
      screen.getByText(
        `Are you sure to want to delete the "${
          mockState.workspaceList!.workspaceActionDialog!.formValues.title
        }" workspace preset?`,
      ),
    ).toBeTruthy();

    // Click close button
    fireEvent.click(screen.getByTestId('customDialog-close'));
    const expectedUpdateAction =
      workspaceListActions.closeWorkspaceActionDialogOptions();
    expect(store.getActions()).toContainEqual(expectedUpdateAction);
  });

  it('should handle onSubmitForm', async () => {
    const testDialog = {
      action: WorkspacePresetAction.DELETE,
      presetId: 'workspaceList-1',
      formValues: {
        title: 'Workspace list item',
        abstract: 'some abs',
      },
    };

    const mockState: AppStore = {
      workspace: screenConfigEmptyView,
      workspaceList: {
        isFetching: false,
        error: undefined,
        isWorkspaceListDialogOpen: false,
        workspaceActionDialog: testDialog,
        ids: ['workspaceList-1'],
        entities: {
          'workspaceList-1': {
            id: 'workspaceList-1',
            scope: 'system' as const,
            title: 'Workspace list item',
            date: '2022-06-01T12:34:27.787192',
            viewType: 'singleWindow' as const,
            abstract: '',
          },
        },
      } as WorkspaceListState,
    };
    const store = createMockStoreWithEggs(mockState);
    render(
      <WorkspaceWrapperProviderWithStore theme={lightTheme} store={store}>
        <WorkspaceTabConnect workspaceId="workspaceList-1" />
      </WorkspaceWrapperProviderWithStore>,
    );
    expect(screen.getByTestId('workspace-preset-dialog')).toBeTruthy();
    expect(
      screen.getByText(
        `Are you sure to want to delete the "${
          mockState.workspaceList!.workspaceActionDialog!.formValues.title
        }" workspace preset?`,
      ),
    ).toBeTruthy();

    fireEvent.click(screen.getByText('Delete'));

    const expectedUpdateAction =
      workspaceListActions.submitFormWorkspaceActionDialogOptions({
        presetAction: WorkspacePresetAction.DELETE,
        presetId: testDialog.presetId,
        data: testDialog.formValues as unknown as WorkspacePreset,
      });
    await waitFor(() => {
      expect(store.getActions()).toContainEqual(expectedUpdateAction);
    });
  });
});
