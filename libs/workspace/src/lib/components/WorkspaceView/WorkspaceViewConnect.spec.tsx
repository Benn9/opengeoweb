/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { fireEvent, render, screen, waitFor } from '@testing-library/react';
import {
  createMockStoreWithEggs,
  createMockStoreWithEggsAndMiddleware,
} from '@opengeoweb/shared';
import createSagaMiddleware from 'redux-saga';
import { uiActions } from '@opengeoweb/store';
import WorkspaceViewConnect from './WorkspaceViewConnect';
import {
  WorkspaceComponentLookupPayload,
  WorkspacePreset,
} from '../../store/workspace/types';
import { WorkspaceWrapperProviderWithStore } from '../Providers/Providers';
import workspaceSaga from '../../store/workspace/sagas';
import { workspaceActions } from '../../store/workspace/reducer';
import { AppStore } from '../../store/store';
import { viewPresetActions } from '../../store/viewPresets';

const componentsLookUp = (
  payload: WorkspaceComponentLookupPayload,
): React.ReactElement => {
  const { componentType, initialProps: props, id } = payload;
  switch (componentType) {
    case 'MyTestComponent':
      return (
        <div data-testid="MyTestComponentTestId">
          {id}
          {JSON.stringify(props)}
        </div>
      );
    default:
      return null!;
  }
};

describe('components/WorkspaceView/WorkspaceViewConnect', () => {
  const mosaicNode = {
    direction: 'row',
    first: 'screen1',
    second: 'screen2',
  } as const;
  const screenConfig: WorkspacePreset = {
    id: 'preset1',
    title: 'Preset 1',
    views: {
      allIds: ['screen1', 'screen2'],
      byId: {
        screen1: {
          id: 'test-screen-1',
          title: 'screen 1',
          componentType: 'MyTestComponent',
          initialProps: { mapPreset: [{}], syncGroupsIds: [] },
        },
        screen2: {
          id: 'test-screen-2',
          title: 'screen 2',
          componentType: 'MyTestComponent',
          initialProps: { mapPreset: [{}], syncGroupsIds: [] },
        },
      },
    },
    mosaicNode,
  };
  it('should return a view with correct title', () => {
    const mockState: AppStore = {
      workspace: screenConfig,
    };
    const store = createMockStoreWithEggs(mockState);
    const { baseElement } = render(
      <WorkspaceWrapperProviderWithStore store={store}>
        <WorkspaceViewConnect componentsLookUp={componentsLookUp} />
      </WorkspaceWrapperProviderWithStore>,
    );
    expect(
      // eslint-disable-next-line testing-library/no-node-access
      baseElement.querySelector('.mosaic-window-title')!.textContent,
    ).toEqual(
      `${
        mockState.workspace!.views!.byId![mockState.workspace!.views.allIds[0]]
          .title
      }${mockState.workspace!.views.allIds[0]}`,
    );
  });

  it('should expand a view', () => {
    const mockState: AppStore = {
      workspace: screenConfig,
    };
    const store = createMockStoreWithEggs(mockState);
    render(
      <WorkspaceWrapperProviderWithStore store={store}>
        <WorkspaceViewConnect componentsLookUp={componentsLookUp} />
      </WorkspaceWrapperProviderWithStore>,
    );
    fireEvent.click(screen.getAllByLabelText('Expand window')[0]);
    const expectedAction = workspaceActions.updateWorkspaceViews({
      mosaicNode: {
        ...mosaicNode,
        splitPercentage: 70,
      },
    });
    expect(store.getActions()).toContainEqual(expectedAction);
  });

  it('should set active window', () => {
    const mockState: AppStore = {
      workspace: screenConfig,
    };
    const store = createMockStoreWithEggs(mockState);
    render(
      <WorkspaceWrapperProviderWithStore store={store}>
        <WorkspaceViewConnect componentsLookUp={componentsLookUp} />
      </WorkspaceWrapperProviderWithStore>,
    );

    const expectedInitialActions = [
      viewPresetActions.registerViewPreset({
        panelId: screenConfig.views.allIds[0],
        viewPresetId: expect.any(String),
      }),
      viewPresetActions.registerViewPreset({
        panelId: screenConfig.views.allIds[1],
        viewPresetId: expect.any(String),
      }),
      uiActions.setActiveWindowId({
        activeWindowId: 'screen1',
      }),
    ];

    expect(store.getActions()).toEqual(expectedInitialActions);

    const toolbar = screen.getAllByText('screen 2')[0];
    fireEvent.click(toolbar);

    expect(store.getActions()).toEqual([
      ...expectedInitialActions,
      uiActions.setActiveWindowId({
        activeWindowId: 'screen2',
      }),
    ]);
  });

  it('should not set active window on click if window is already active', () => {
    const mockState: AppStore = {
      workspace: screenConfig,
      ui: {
        order: [],
        activeWindowId: 'screen1',
        dialogs: {},
      },
    };
    const store = createMockStoreWithEggs(mockState);
    render(
      <WorkspaceWrapperProviderWithStore store={store}>
        <WorkspaceViewConnect componentsLookUp={componentsLookUp} />
      </WorkspaceWrapperProviderWithStore>,
    );

    const expectedInitialActions = [
      viewPresetActions.registerViewPreset({
        panelId: screenConfig.views.allIds[0],
        viewPresetId: expect.any(String),
      }),
      viewPresetActions.registerViewPreset({
        panelId: screenConfig.views.allIds[1],
        viewPresetId: expect.any(String),
      }),
      uiActions.setActiveWindowId({
        activeWindowId: 'screen1',
      }),
    ];

    expect(store.getActions()).toEqual(expectedInitialActions);

    const toolbar = screen.getAllByText('screen 1')[0];
    fireEvent.click(toolbar);

    expect(store.getActions()).toEqual(expectedInitialActions);
  });

  it('should split a view vertically', () => {
    const mockState: AppStore = {
      workspace: screenConfig,
    };
    const store = createMockStoreWithEggs(mockState);
    render(
      <WorkspaceWrapperProviderWithStore store={store}>
        <WorkspaceViewConnect componentsLookUp={componentsLookUp} />
      </WorkspaceWrapperProviderWithStore>,
    );
    fireEvent.click(screen.getAllByLabelText('Split window vertically')[0]);

    const expectedAddAction = workspaceActions.addWorkspaceView({
      componentType: 'Map',
      id: expect.any(String),
      initialProps: {
        mapPreset: {},
        syncGroupsIds: [],
      },
    });

    const expectedUpdateAction = workspaceActions.updateWorkspaceViews({
      mosaicNode: {
        direction: 'row',
        first: {
          direction: 'row',
          first: 'screen1',
          second: expect.any(String),
        },
        second: 'screen2',
      },
    });
    expect(store.getActions()).toContainEqual(expectedAddAction);
    expect(store.getActions()).toContainEqual(expectedUpdateAction);
  });

  it('should split a view horizontally', () => {
    const mockState: AppStore = {
      workspace: screenConfig,
    };
    const store = createMockStoreWithEggs(mockState);
    render(
      <WorkspaceWrapperProviderWithStore store={store}>
        <WorkspaceViewConnect componentsLookUp={componentsLookUp} />
      </WorkspaceWrapperProviderWithStore>,
    );
    fireEvent.click(screen.getAllByLabelText('Split window horizontally')[0]);

    const expectedAddAction = workspaceActions.addWorkspaceView({
      componentType: 'Map',
      id: expect.any(String),
      initialProps: {
        mapPreset: {},
        syncGroupsIds: [],
      },
    });

    const expectedUpdateAction = workspaceActions.updateWorkspaceViews({
      mosaicNode: {
        direction: 'row',
        first: {
          direction: 'column',
          first: 'screen1',
          second: expect.any(String),
        },
        second: 'screen2',
      },
    });
    expect(store.getActions()).toContainEqual(expectedAddAction);
    expect(store.getActions()).toContainEqual(expectedUpdateAction);
  });

  it('should resize a view', async () => {
    const resizeSpy = jest.fn();
    window.addEventListener('resize', resizeSpy);

    const mockState: AppStore = {
      workspace: screenConfig,
    };
    const sagaMiddleware = createSagaMiddleware();
    const store = createMockStoreWithEggsAndMiddleware(
      mockState,
      sagaMiddleware,
    );
    sagaMiddleware.run(workspaceSaga);

    const { baseElement } = render(
      <WorkspaceWrapperProviderWithStore store={store}>
        <WorkspaceViewConnect componentsLookUp={componentsLookUp} />
      </WorkspaceWrapperProviderWithStore>,
    );
    // eslint-disable-next-line testing-library/no-node-access
    const draggableLine = baseElement.querySelector('.mosaic-split-line')!;
    fireEvent.mouseDown(draggableLine, { clientX: 1 });
    fireEvent.mouseUp(draggableLine);
    const expectedAction = workspaceActions.updateWorkspaceViews({
      mosaicNode: {
        direction: 'row',
        first: 'screen1',
        second: 'screen2',
        splitPercentage: expect.any(Number),
      },
    });
    expect(store.getActions()).toContainEqual(expectedAction);
    await waitFor(() => expect(resizeSpy).toHaveBeenCalled());
  });

  it('should resize a view and should update views afterwards with 1 action', async () => {
    const resizeSpy = jest.fn();
    window.addEventListener('resize', resizeSpy);

    const mockState: AppStore = {
      workspace: screenConfig,
    };
    const sagaMiddleware = createSagaMiddleware();
    const store = createMockStoreWithEggsAndMiddleware(
      mockState,
      sagaMiddleware,
    );
    sagaMiddleware.run(workspaceSaga);

    const { baseElement } = render(
      <WorkspaceWrapperProviderWithStore store={store}>
        <WorkspaceViewConnect componentsLookUp={componentsLookUp} />
      </WorkspaceWrapperProviderWithStore>,
    );

    // eslint-disable-next-line testing-library/no-node-access
    const draggableLine = baseElement.querySelector('.mosaic-split-line')!;
    fireEvent.mouseDown(draggableLine, { clientX: 1 });
    fireEvent.mouseMove(draggableLine, { clientX: 100 });
    fireEvent.mouseMove(draggableLine, { clientX: 50 });
    fireEvent.mouseMove(draggableLine, { clientX: 150 });
    fireEvent.mouseUp(draggableLine);
    const expectedAction = workspaceActions.updateWorkspaceViews({
      mosaicNode: {
        direction: 'row',
        first: 'screen1',
        second: 'screen2',
        splitPercentage: expect.any(Number),
      },
    });
    expect(store.getActions()).toContainEqual(expectedAction);
    await waitFor(() => expect(resizeSpy).toHaveBeenCalled());
  });

  it('should not have unexpand after split window', async () => {
    const mockState: AppStore = {
      workspace: screenConfig,
    };
    const sagaMiddleware = createSagaMiddleware();
    const store = createMockStoreWithEggsAndMiddleware(
      mockState,
      sagaMiddleware,
    );
    sagaMiddleware.run(workspaceSaga);

    const { rerender } = render(
      <WorkspaceWrapperProviderWithStore store={store}>
        <WorkspaceViewConnect componentsLookUp={componentsLookUp} />
      </WorkspaceWrapperProviderWithStore>,
    );

    // First of all there should be two expand buttons
    const expandButtons = screen.getAllByLabelText('Expand window');
    expect(expandButtons).toHaveLength(2);
    expect(screen.queryByLabelText('Unexpand window')).toBeNull();

    // Now we click one expand button
    fireEvent.click(expandButtons[0]);
    const expectedAction = workspaceActions.updateWorkspaceViews({
      mosaicNode: {
        ...mosaicNode,
        splitPercentage: 70,
      },
    });
    expect(store.getActions()).toContainEqual(expectedAction);

    // And expect to find one expand button and on unexpand button
    expect(screen.getAllByLabelText('Expand window')).toHaveLength(1);
    expect(screen.getAllByLabelText('Unexpand window')).toHaveLength(1);

    // Now we split a window
    fireEvent.click(screen.getAllByLabelText('Split window horizontally')[0]);
    const expectedAddAction = workspaceActions.addWorkspaceView({
      componentType: 'Map',
      id: expect.any(String),
      initialProps: {
        mapPreset: {},
        syncGroupsIds: [],
      },
    });

    const expectedUpdateAction = workspaceActions.updateWorkspaceViews({
      mosaicNode: {
        direction: 'row',
        first: {
          direction: 'column',
          first: 'screen1',
          second: expect.any(String),
        },
        second: 'screen2',
        splitPercentage: 70,
      },
    });
    const actions = store.getActions();
    expect(actions[5]).toEqual(expectedAddAction);

    const workspaceUpdateViewsAction = actions[6];
    expect(workspaceUpdateViewsAction).toEqual(expectedUpdateAction);

    // grab the action, update the screenconfig and rerender
    const updatedScreenConfig: WorkspacePreset = {
      id: screenConfig.id,
      title: screenConfig.title,
      views: screenConfig.views,
      mosaicNode: {
        ...workspaceUpdateViewsAction.payload.mosaicNode,
      },
    };

    const updatedMockState: AppStore = {
      workspace: updatedScreenConfig,
    };
    const updatedStore = createMockStoreWithEggs(updatedMockState);

    rerender(
      <WorkspaceWrapperProviderWithStore store={updatedStore}>
        <WorkspaceViewConnect componentsLookUp={componentsLookUp} />
      </WorkspaceWrapperProviderWithStore>,
    );

    expect(screen.getAllByLabelText('Expand window')).toHaveLength(3);
    expect(screen.queryByLabelText('Unexpand window')).toBeNull();
  });

  it('should not have unexpand after resize window', async () => {
    const resizeSpy = jest.fn();
    window.addEventListener('resize', resizeSpy);

    const mockState: AppStore = {
      workspace: screenConfig,
    };
    const sagaMiddleware = createSagaMiddleware();
    const store = createMockStoreWithEggsAndMiddleware(
      mockState,
      sagaMiddleware,
    );
    sagaMiddleware.run(workspaceSaga);

    const { baseElement, rerender } = render(
      <WorkspaceWrapperProviderWithStore store={store}>
        <WorkspaceViewConnect componentsLookUp={componentsLookUp} />
      </WorkspaceWrapperProviderWithStore>,
    );

    // First of all there should be two expand buttons
    const expandButtons = screen.getAllByLabelText('Expand window');
    expect(expandButtons).toHaveLength(2);
    expect(screen.queryByLabelText('Unexpand window')).toBeNull();

    // Now we click one expand button
    fireEvent.click(expandButtons[0]);

    // And expect to find one expand button and on unexpand button
    expect(screen.getAllByLabelText('Expand window')).toHaveLength(1);
    expect(screen.getAllByLabelText('Unexpand window')).toHaveLength(1);

    // Now we resize a window
    const draggableLine =
      // eslint-disable-next-line testing-library/no-node-access
      baseElement.getElementsByClassName('mosaic-split-line')[0];
    fireEvent.mouseDown(draggableLine, { clientX: 1 });
    fireEvent.mouseUp(draggableLine);
    const expectedAction = workspaceActions.updateWorkspaceViews({
      mosaicNode: {
        direction: 'row',
        first: 'screen1',
        second: 'screen2',
        splitPercentage: expect.any(Number),
      },
    });
    const workspaceUpdateViewsAction = store.getActions()[3];
    expect(workspaceUpdateViewsAction).toEqual(expectedAction);
    await waitFor(() => expect(resizeSpy).toHaveBeenCalled());

    // grab the action, update the screenconfig and rerender
    const updatedScreenConfig = {
      id: screenConfig.id,
      title: screenConfig.title,
      views: screenConfig.views,
      mosaicNode: {
        ...workspaceUpdateViewsAction.payload.mosaicNode,
      },
    };

    const updatedMockState = {
      workspace: updatedScreenConfig,
    };
    const updatedStore = createMockStoreWithEggs(updatedMockState);

    rerender(
      <WorkspaceWrapperProviderWithStore store={updatedStore}>
        <WorkspaceViewConnect componentsLookUp={componentsLookUp} />
      </WorkspaceWrapperProviderWithStore>,
    );

    expect(screen.getAllByLabelText('Expand window')).toHaveLength(2);
    expect(screen.queryByLabelText('Unexpand window')).toBeNull();
  });

  it('should close a view', async () => {
    const mockState: AppStore = {
      workspace: screenConfig,
    };
    const store = createMockStoreWithEggs(mockState);
    render(
      <WorkspaceWrapperProviderWithStore store={store}>
        <WorkspaceViewConnect componentsLookUp={componentsLookUp} />
      </WorkspaceWrapperProviderWithStore>,
    );
    fireEvent.click(screen.getAllByLabelText('Close window')[0]);
    const expectedAction = workspaceActions.updateWorkspaceViews({
      mosaicNode: 'screen2',
    });
    expect(store.getActions()).toContainEqual(expectedAction);
  });

  it('should listen to window orientation change event', async () => {
    jest.spyOn(window, 'addEventListener');
    jest.spyOn(window, 'removeEventListener');

    const mockState: AppStore = {
      workspace: screenConfig,
    };
    const store = createMockStoreWithEggs(mockState);
    render(
      <WorkspaceWrapperProviderWithStore store={store}>
        <WorkspaceViewConnect componentsLookUp={componentsLookUp} />
      </WorkspaceWrapperProviderWithStore>,
    );

    // Trigger the window orientation change event.
    window.dispatchEvent(new Event('orientationchange'));

    await waitFor(() => {
      expect(window.addEventListener).toBeCalled();
    });
    expect(window.addEventListener).toBeCalledWith(
      'orientationchange',
      expect.any(Function),
    );
    fireEvent.click(screen.getAllByTestId('close-btn')[0]);
    await waitFor(() => expect(window.removeEventListener).toBeCalled());
  });
});
