/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { fireEvent, screen, render } from '@testing-library/react';
import { createMockStoreWithEggs } from '@opengeoweb/shared';
import { Mosaic, MosaicNode } from 'react-mosaic-component';
import { MosaicKey } from 'react-mosaic-component/lib/types';
import WorkspaceTileConnect from './WorkspaceTileConnect';
import { WorkspaceComponentLookupPayload } from '../../store/workspace/types';
import { WorkspaceWrapperProviderWithStore } from '../Providers/Providers';
import { workspaceActions } from '../../store/workspace/reducer';
import { viewPresetActions } from '../../store/viewPresets';
import { AppStore } from '../../store/store';
import { ViewPresetType } from '../../store/viewPresets/types';

const componentsLookUp = (
  payload: WorkspaceComponentLookupPayload,
): React.ReactElement => {
  const { componentType, initialProps: props, id } = payload;
  switch (componentType) {
    case 'MyTestComponent':
    default:
      return (
        <div data-testid="MyTestComponentTestId">
          {id}
          {JSON.stringify(props)}
        </div>
      );
  }
};

describe('components/WorkspaceView/WorkspaceTileConnect', () => {
  const screenConfig = {
    id: 'preset1',
    title: 'Preset 1',
    views: {
      allIds: ['screen1', 'screen2'],
      byId: {
        screen1: {
          title: 'screen 1',
          id: 'viewpreset1',
          componentType: 'MyTestComponent',
          initialProps: { mapPreset: [{}], syncGroupsIds: [] },
        },
        screen2: {
          title: 'screen 2',
          id: 'viewpreset2',
          componentType: 'Map',
          initialProps: {
            mapPreset: {
              layers: [
                {
                  service: 'https://geoservices.knmi.nl/wms?dataset=RADAR&',
                  name: 'RAD_NL25_PCP_CM',
                  format: 'image/png',
                  enabled: true,
                  layerType: 'mapLayer',
                },
              ],
              proj: {
                bbox: {
                  left: -450651.2255879827,
                  bottom: 6490531.093143953,
                  right: 1428345.8183648037,
                  top: 7438773.776232235,
                },
                srs: 'EPSG:3857',
              },
            },
          },
        },
      },
    },
    mosaicNode: {
      direction: 'row' as const,
      first: 'screen1',
      second: 'screen2',
    },
  };
  it('should add a view', () => {
    const mockState: AppStore = {
      workspace: screenConfig,
    };
    const store = createMockStoreWithEggs(mockState);
    const props = {
      componentsLookUp,
      path: [],
      mosaicNodeId: 'screen1',
    };

    render(
      <WorkspaceWrapperProviderWithStore store={store}>
        <Mosaic
          value={screenConfig.mosaicNode as MosaicNode<MosaicKey>}
          renderTile={(): React.ReactElement => (
            <WorkspaceTileConnect {...props} />
          )}
          onChange={jest.fn()}
        />
      </WorkspaceWrapperProviderWithStore>,
    );

    expect(store.getActions()).toHaveLength(2);
    expect(store.getActions()).toEqual([
      viewPresetActions.registerViewPreset({
        panelId: props.mosaicNodeId,
        viewPresetId: expect.any(String),
      }),
      viewPresetActions.registerViewPreset({
        panelId: props.mosaicNodeId,
        viewPresetId: expect.any(String),
      }),
    ]);

    fireEvent.click(screen.getAllByTestId('split-horizontal-btn')[0]);

    expect(store.getActions()).toHaveLength(3);
    expect(store.getActions()).toEqual([
      viewPresetActions.registerViewPreset({
        panelId: props.mosaicNodeId,
        viewPresetId: expect.any(String),
      }),
      viewPresetActions.registerViewPreset({
        panelId: props.mosaicNodeId,
        viewPresetId: expect.any(String),
      }),
      workspaceActions.addWorkspaceView({
        componentType: 'Map',
        id: expect.any(String),
        initialProps: {
          mapPreset: {},
          syncGroupsIds: [],
        },
      }),
    ]);
  });

  it('should render default ViewLoading component', () => {
    const screenConfig2 = {
      id: 'preset1',
      title: 'Preset 1',
      views: {
        allIds: ['screen1'],
        byId: {
          screen1: {
            title: 'screen 1',
            id: 'viewpreset1',
            componentType: 'ViewLoading',
            initialProps: {},
          },
        },
      },
      mosaicNode: {
        direction: 'row' as const,
        first: 'screen1',
        second: 'second',
      },
    };

    const mockState: AppStore = {
      workspace: screenConfig2,
    };
    const store = createMockStoreWithEggs(mockState);
    const props = {
      componentsLookUp,
      path: [],
      mosaicNodeId: 'screen1',
    };

    render(
      <WorkspaceWrapperProviderWithStore store={store}>
        <Mosaic
          value={screenConfig.mosaicNode as MosaicNode<MosaicKey>}
          renderTile={(): React.ReactElement => (
            <WorkspaceTileConnect {...props} />
          )}
          onChange={jest.fn()}
        />
      </WorkspaceWrapperProviderWithStore>,
    );

    expect(screen.queryAllByText('Loading viewpreset1')).toHaveLength(2);
  });

  it('should render default ViewError component', () => {
    const screenConfig2 = {
      id: 'preset1',
      title: 'Preset 1',
      views: {
        allIds: ['screen1'],
        byId: {
          screen1: {
            title: 'screen 1',
            id: 'viewPreset1',
            componentType: 'ViewError',
            initialProps: {},
          },
        },
      },
      mosaicNode: {
        direction: 'row' as const,
        first: 'screen1',
        second: '',
      },
    };

    const mockState: AppStore = {
      workspace: screenConfig2,
    };
    const store = createMockStoreWithEggs(mockState);
    const props = {
      componentsLookUp,
      path: [],
      mosaicNodeId: 'screen1',
    };

    render(
      <WorkspaceWrapperProviderWithStore store={store}>
        <Mosaic
          value={screenConfig.mosaicNode as MosaicNode<MosaicKey>}
          renderTile={(): React.ReactElement => (
            <WorkspaceTileConnect {...props} />
          )}
          onChange={jest.fn()}
        />
      </WorkspaceWrapperProviderWithStore>,
    );

    expect(
      screen.queryAllByText('Failed to fetch view viewPreset1'),
    ).toHaveLength(2);
  });

  it('should use the bbox and srs of the clicked view when splitting a map', () => {
    const bbox = {
      // different bbox than in screenconfig to simulate user has moved the map
      left: 299317.22579447436,
      bottom: 4028678.1331711058,
      right: 2178314.269747261,
      top: 4976920.816259388,
    };
    const srs = 'EPSG:32661';
    const mockState: AppStore = {
      workspace: screenConfig,
      webmap: {
        byId: {
          screen2: {
            bbox,
            srs,
            id: 'test',
            isAnimating: false,
            isAutoUpdating: false,
            isEndTimeOverriding: false,
            baseLayers: [],
            overLayers: [],
            mapLayers: [],
            featureLayers: [],
          },
        },
        allIds: ['screen2'],
      },
    };
    const store = createMockStoreWithEggs(mockState);
    const props = {
      componentsLookUp,
      path: [],
      mosaicNodeId: 'screen2',
    };

    render(
      <WorkspaceWrapperProviderWithStore store={store}>
        <Mosaic
          value={screenConfig.mosaicNode as MosaicNode<MosaicKey>}
          renderTile={(): React.ReactElement => (
            <WorkspaceTileConnect {...props} />
          )}
          onChange={jest.fn()}
        />
      </WorkspaceWrapperProviderWithStore>,
    );

    expect(store.getActions()).toHaveLength(2);

    fireEvent.click(screen.getAllByTestId('split-vertical-btn')[0]);

    expect(store.getActions()).toHaveLength(3);
    expect(store.getActions()).toEqual([
      viewPresetActions.registerViewPreset({
        panelId: props.mosaicNodeId,
        viewPresetId: expect.any(String),
      }),
      viewPresetActions.registerViewPreset({
        panelId: props.mosaicNodeId,
        viewPresetId: expect.any(String),
      }),
      workspaceActions.addWorkspaceView({
        componentType: 'Map',
        id: expect.any(String),
        initialProps: {
          mapPreset: { proj: { bbox, srs } },
          syncGroupsIds: [],
        },
      }),
    ]);
  });

  it('should create a new id for a splitted view', () => {
    const screenConfig2 = {
      id: 'preset1',
      title: 'Preset 1',
      views: {
        allIds: ['1_screen', '2_screen'],
        byId: {
          '1_screen': {
            title: 'screen 1',
            id: 'viewpreset1',
            componentType: 'MyTestComponent',
            initialProps: { mapPreset: [{}], syncGroupsIds: [] },
          },
          '2_screen': {
            title: 'screen 2',
            id: 'viewpreset2',
            componentType: 'Map',
            initialProps: {
              mapPreset: {
                layers: [
                  {
                    service: 'https://geoservices.knmi.nl/wms?dataset=RADAR&',
                    name: 'RAD_NL25_PCP_CM',
                    format: 'image/png',
                    enabled: true,
                    layerType: 'mapLayer',
                  },
                ],
                proj: {
                  bbox: {
                    left: -450651.2255879827,
                    bottom: 6490531.093143953,
                    right: 1428345.8183648037,
                    top: 7438773.776232235,
                  },
                  srs: 'EPSG:3857',
                },
              },
            },
          },
        },
      },
      mosaicNode: {
        direction: 'row' as const,
        first: '1_screen',
        second: '2_screen',
      },
    };
    const bbox = {
      // different bbox than in screenconfig to simulate user has moved the map
      left: 299317.22579447436,
      bottom: 4028678.1331711058,
      right: 2178314.269747261,
      top: 4976920.816259388,
    };
    const srs = 'EPSG:32661';
    const mockState: AppStore = {
      workspace: screenConfig2,
      webmap: {
        byId: {
          screen_no_1: {
            bbox,
            srs,
            id: 'test',
            isAnimating: false,
            isAutoUpdating: false,
            isEndTimeOverriding: false,
            baseLayers: [],
            overLayers: [],
            mapLayers: [],
            featureLayers: [],
          },
        },
        allIds: ['screen_no_1'],
      },
    };
    const store = createMockStoreWithEggs(mockState);
    const props = {
      componentsLookUp,
      path: [],
      mosaicNodeId: 'screen_no_1',
    };

    render(
      <WorkspaceWrapperProviderWithStore store={store}>
        <Mosaic
          value={screenConfig.mosaicNode as MosaicNode<MosaicKey>}
          renderTile={(): React.ReactElement => (
            <WorkspaceTileConnect {...props} />
          )}
          onChange={jest.fn()}
        />
      </WorkspaceWrapperProviderWithStore>,
    );

    expect(store.getActions()).toHaveLength(2);

    fireEvent.click(screen.getAllByTestId('split-vertical-btn')[0]);

    expect(store.getActions()).toHaveLength(3);
    expect(store.getActions()).toEqual([
      viewPresetActions.registerViewPreset({
        panelId: props.mosaicNodeId,
        viewPresetId: expect.any(String),
      }),
      viewPresetActions.registerViewPreset({
        panelId: props.mosaicNodeId,
        viewPresetId: expect.any(String),
      }),
      workspaceActions.addWorkspaceView({
        componentType: 'Map',
        id: '3_screen',
        initialProps: {
          mapPreset: { proj: { bbox, srs } },
          syncGroupsIds: [],
        },
      }),
    ]);
  });

  it('should unregister view on unmount', () => {
    const mockState: AppStore = {
      workspace: screenConfig,
    };
    const store = createMockStoreWithEggs(mockState);
    const props = {
      componentsLookUp,
      path: [],
      mosaicNodeId: 'screen1',
    };

    const { unmount } = render(
      <WorkspaceWrapperProviderWithStore store={store}>
        <Mosaic
          value={screenConfig.mosaicNode as MosaicNode<MosaicKey>}
          renderTile={(): React.ReactElement => (
            <WorkspaceTileConnect {...props} />
          )}
          onChange={jest.fn()}
        />
      </WorkspaceWrapperProviderWithStore>,
    );

    expect(store.getActions()).toHaveLength(2);
    expect(store.getActions()).toEqual([
      viewPresetActions.registerViewPreset({
        panelId: props.mosaicNodeId,
        viewPresetId: expect.any(String),
      }),
      viewPresetActions.registerViewPreset({
        panelId: props.mosaicNodeId,
        viewPresetId: expect.any(String),
      }),
    ]);
    unmount();
    expect(store.getActions()).toEqual([
      viewPresetActions.registerViewPreset({
        panelId: props.mosaicNodeId,
        viewPresetId: expect.any(String),
      }),
      viewPresetActions.registerViewPreset({
        panelId: props.mosaicNodeId,
        viewPresetId: expect.any(String),
      }),
      viewPresetActions.unregisterViewPreset({
        panelId: props.mosaicNodeId,
      }),
      viewPresetActions.unregisterViewPreset({
        panelId: props.mosaicNodeId,
      }),
    ]);
  });

  it('should show loading bar when viewpreset is loading', () => {
    const mapId = 'test-1';

    const mockState: AppStore = {
      workspace: screenConfig,
      viewPresets: {
        entities: {
          [mapId]: {
            error: undefined,
            panelId: mapId,
            hasChanges: false,
            isFetching: true,
            activeViewPresetId: '',
            isViewPresetListDialogOpen: false,
            filters: [],
            searchQuery: '',
            filterResults: { ids: [], entities: {} },
          },
        },
        ids: [mapId],
      },
    };
    const store = createMockStoreWithEggs(mockState);
    const props = {
      componentsLookUp,
      path: [],
      mosaicNodeId: mapId,
    };

    render(
      <WorkspaceWrapperProviderWithStore store={store}>
        <Mosaic
          value={screenConfig.mosaicNode as MosaicNode<MosaicKey>}
          renderTile={(): React.ReactElement => (
            <WorkspaceTileConnect {...props} />
          )}
          onChange={jest.fn()}
        />
      </WorkspaceWrapperProviderWithStore>,
    );

    expect(screen.getAllByTestId('loading-bar')).toBeTruthy();
  });

  it('should show viewpreset list dialog and close it', () => {
    const mapId = 'test-1';

    const mockState: AppStore = {
      workspace: screenConfig,
      viewPresets: {
        entities: {
          [mapId]: {
            error: undefined,
            panelId: mapId,
            hasChanges: false,
            isFetching: true,
            activeViewPresetId: '',
            isViewPresetListDialogOpen: true,
            filters: [],
            searchQuery: '',
            filterResults: { ids: [], entities: {} },
          },
        },
        ids: [mapId],
      },
    };
    const store = createMockStoreWithEggs(mockState);
    const props = {
      componentsLookUp,
      path: [],
      mosaicNodeId: mapId,
    };

    render(
      <WorkspaceWrapperProviderWithStore store={store}>
        <Mosaic
          value={screenConfig.mosaicNode as MosaicNode<MosaicKey>}
          renderTile={(): React.ReactElement => (
            <WorkspaceTileConnect {...props} />
          )}
          onChange={jest.fn()}
        />
      </WorkspaceWrapperProviderWithStore>,
    );

    const backDrops = screen.queryAllByTestId('viewpresetMenuBackdrop');
    expect(backDrops).toBeTruthy();

    fireEvent.click(backDrops[0]);

    expect(store.getActions()).toContainEqual(
      viewPresetActions.toggleViewPresetListDialog({
        panelId: mapId,
        isViewPresetListDialogOpen: false,
      }),
    );
  });

  it('should show error when viewpreset has error', () => {
    const mapId = 'test-1';
    const testError = {
      type: ViewPresetType.PRESET_DETAIL,
      message: 'Testing something went wrong',
    };

    const mockState: AppStore = {
      workspace: screenConfig,
      viewPresets: {
        entities: {
          [mapId]: {
            error: testError,
            panelId: mapId,
            hasChanges: false,
            isFetching: true,
            activeViewPresetId: '',
            isViewPresetListDialogOpen: false,
            filters: [],
            searchQuery: '',
            filterResults: { ids: [], entities: {} },
          },
        },
        ids: [mapId],
      },
    };
    const store = createMockStoreWithEggs(mockState);
    const props = {
      componentsLookUp,
      path: [],
      mosaicNodeId: mapId,
    };

    render(
      <WorkspaceWrapperProviderWithStore store={store}>
        <Mosaic
          value={screenConfig.mosaicNode as MosaicNode<MosaicKey>}
          renderTile={(): React.ReactElement => (
            <WorkspaceTileConnect {...props} />
          )}
          onChange={jest.fn()}
        />
      </WorkspaceWrapperProviderWithStore>,
    );

    expect(screen.getAllByText(testError.message)).toBeTruthy();
  });
});
