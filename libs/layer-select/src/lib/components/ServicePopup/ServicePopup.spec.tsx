/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import {
  fireEvent,
  render,
  screen,
  waitFor,
  act,
} from '@testing-library/react';
import userEvent from '@testing-library/user-event';

import {
  getCapabilities,
  mockGetCapabilities,
  WMJSService,
} from '@opengeoweb/webmap';
import { layerSelectTypes } from '@opengeoweb/store';
import { ThemeWrapper } from '@opengeoweb/theme';
import {
  ServicePopup,
  ADD_HEADING,
  COPY_URL_MESSAGE,
  EDIT_HEADING,
  getSuccesMessage,
  SAVE_HEADING,
  ServicePopupProps,
} from './ServicePopup';

import {
  VALIDATIONS_SERVICE_EXISTING,
  VALIDATIONS_NAME_EXISTING,
  VALIDATIONS_SERVICE_VALID_URL,
  VALIDATIONS_REQUEST_FAILED,
  VALIDATIONS_FIELD_REQUIRED,
} from './utils';
import * as utils from './utils';

const mockServiceFromStore = {
  id: 'serviceid_1',
  title: 'service test title',
  abstract: 'service Abstract',
};

jest.mock('@opengeoweb/webmap', () => ({
  ...jest.requireActual('@opengeoweb/webmap'),
  WMGetServiceFromStore: (): WMJSService => mockServiceFromStore as WMJSService,
}));

const serviceUrlLabel = /this service refers to this url/i;
const serviceNameLabel = /service name/i;
const serviceAbstractLabel = /abstracts/i;

describe('src/lib/components/LayerManager/ServicePopup/ServicePopup', () => {
  const mockServices: layerSelectTypes.ActiveServiceObjectEntities = {
    serviceid_1: {
      serviceName: 'Radar Norway',
      serviceUrl: 'https://halo-wms.met.no/halo/default.map?',
      enabled: true,
      filterIds: [],
      scope: 'user',
    },
  };

  jest
    .spyOn(getCapabilities, 'getLayersFromService')
    .mockImplementation(mockGetCapabilities.mockGetLayersFromService);

  jest
    .spyOn(getCapabilities, 'getLayersFlattenedFromService')
    .mockImplementation(
      // eslint-disable-next-line @typescript-eslint/no-explicit-any
      mockGetCapabilities.mockGetLayersFlattenedFromService as any,
    );

  const user = userEvent.setup();

  it('should render correctly', async () => {
    render(
      <ThemeWrapper>
        <ServicePopup
          servicePopupVariant="add"
          closePopup={jest.fn()}
          isOpen={true}
          serviceSetLayers={jest.fn()}
          services={mockServices}
        />
      </ThemeWrapper>,
    );
    expect(screen.getByRole('dialog').textContent).toBeTruthy();
  });

  it('should render the add title when using the add variant', () => {
    render(
      <ThemeWrapper>
        <ServicePopup
          servicePopupVariant="add"
          closePopup={jest.fn()}
          isOpen={true}
          serviceSetLayers={jest.fn()}
          services={mockServices}
        />
      </ThemeWrapper>,
    );
    expect(screen.getByRole('heading').textContent).toBe(ADD_HEADING);

    const input = screen.getByRole('textbox', {
      name: serviceUrlLabel,
    });
    expect(input.getAttribute('disabled')).toBeFalsy();
  });

  it('should render correct abstract', () => {
    const testAbstract = 'testing abstract';
    render(
      <ThemeWrapper>
        <ServicePopup
          servicePopupVariant="edit"
          closePopup={jest.fn()}
          isOpen={true}
          serviceSetLayers={jest.fn()}
          services={mockServices}
          serviceAbstracts={testAbstract}
        />
      </ThemeWrapper>,
    );

    expect(
      screen.getByRole('textbox', {
        name: serviceAbstractLabel,
      }).innerHTML,
    ).toEqual(testAbstract);
  });

  it('should render the edit title when using the edit variant', () => {
    render(
      <ThemeWrapper>
        <ServicePopup
          servicePopupVariant="edit"
          closePopup={jest.fn()}
          isOpen={true}
          serviceSetLayers={jest.fn()}
          services={mockServices}
        />
      </ThemeWrapper>,
    );
    expect(screen.getByRole('heading').textContent).toBe(EDIT_HEADING);

    expect(
      screen
        .getByRole('textbox', {
          name: serviceUrlLabel,
        })
        .getAttribute('disabled'),
    ).toBeDefined();
  });

  it('should render the correct values for edit variant', () => {
    const props = {
      serviceUrl: 'http://www.test.nl',
      serviceName: 'test name',
      serviceAbstracts: 'test abstract',
    };

    render(
      <ThemeWrapper>
        <ServicePopup
          servicePopupVariant="edit"
          closePopup={jest.fn()}
          isOpen={true}
          serviceSetLayers={jest.fn()}
          services={mockServices}
          {...props}
        />
      </ThemeWrapper>,
    );
    expect(
      screen
        .getByRole('textbox', {
          name: serviceUrlLabel,
        })
        .getAttribute('value'),
    ).toEqual(props.serviceUrl);
    expect(
      screen
        .getByRole('textbox', {
          name: serviceNameLabel,
        })
        .getAttribute('value'),
    ).toEqual(props.serviceName);
    expect(
      screen.getByRole('textbox', {
        name: serviceAbstractLabel,
      }).innerHTML,
    ).toEqual(props.serviceAbstracts);

    const button = screen.getByTestId('saveServiceButton');
    expect(button.getAttribute('disabled')).toBeFalsy();
  });

  it('should render the save title when using the save variant', () => {
    render(
      <ThemeWrapper>
        <ServicePopup
          servicePopupVariant="save"
          closePopup={jest.fn()}
          isOpen={true}
          serviceSetLayers={jest.fn()}
          services={mockServices}
        />
      </ThemeWrapper>,
    );
    expect(screen.getByRole('heading').textContent).toBe(SAVE_HEADING);
  });

  it('should call the function that closes the popup on click', () => {
    const closePopup = jest.fn();
    render(
      <ThemeWrapper>
        <ServicePopup
          servicePopupVariant="save"
          closePopup={closePopup}
          isOpen={true}
          serviceSetLayers={jest.fn()}
          services={mockServices}
        />
      </ThemeWrapper>,
    );

    fireEvent.click(screen.getByRole('button', { name: /close/i }));
    expect(closePopup).toHaveBeenCalledTimes(1);

    fireEvent.click(screen.getByRole('button', { name: /cancel/i }));
    expect(closePopup).toBeCalledTimes(2);
  });

  it('should not show dialog when isOpen = false', () => {
    render(
      <ThemeWrapper>
        <ServicePopup
          servicePopupVariant="save"
          closePopup={jest.fn()}
          isOpen={false}
          serviceSetLayers={jest.fn()}
          services={mockServices}
        />
      </ThemeWrapper>,
    );

    expect(screen.queryByRole('dialog')).toBeFalsy();
  });

  it('should prefill the url field when activeService has serviceUrl', async () => {
    const testUrl = 'https://google.com';
    render(
      <ThemeWrapper>
        <ServicePopup
          servicePopupVariant="save"
          services={mockServices}
          isOpen
          serviceUrl={testUrl}
          serviceSetLayers={jest.fn()}
        />
      </ThemeWrapper>,
    );

    await waitFor(() => {
      const urlField = screen.getByRole('textbox', {
        name: serviceUrlLabel,
      }) as HTMLInputElement;
      expect(urlField.value).toBe(testUrl);
    });
  });

  it('should disable save button if url aready exists in store', async () => {
    const testUrl = 'https://halo-wms.met.no/halo/default.map?';

    render(
      <ThemeWrapper>
        <ServicePopup
          servicePopupVariant="save"
          isOpen
          serviceUrl={testUrl}
          serviceSetLayers={jest.fn()}
          services={mockServices}
        />
      </ThemeWrapper>,
    );

    const serviceUrlTextField = screen.getByRole('textbox', {
      name: serviceUrlLabel,
    });
    fireEvent.change(serviceUrlTextField, { target: { value: testUrl } });
    await waitFor(() => {
      const saveButton = screen.getByText('save') as HTMLButtonElement;
      expect(saveButton.disabled).toBe(true);
    });
  });

  it('should disable save button if service name aready exists in store', async () => {
    render(
      <ThemeWrapper>
        <ServicePopup
          servicePopupVariant="save"
          isOpen
          serviceSetLayers={jest.fn()}
          services={mockServices}
        />
      </ThemeWrapper>,
    );

    const serviceNameTextField = screen.getByRole('textbox', {
      name: /service name/i,
    });
    fireEvent.change(serviceNameTextField, {
      target: { value: 'Radar Norway' },
    });

    await waitFor(() => {
      const saveButton = screen.getByText('save') as HTMLButtonElement;
      expect(saveButton.disabled).toBe(true);
    });
  });

  it('should show error message, if service URL is already in store', async () => {
    render(
      <ThemeWrapper>
        <ServicePopup
          servicePopupVariant="save"
          isOpen
          serviceSetLayers={jest.fn()}
          services={mockServices}
        />
      </ThemeWrapper>,
    );

    const serviceUrlField = screen.getByRole('textbox', {
      name: serviceUrlLabel,
    });
    fireEvent.change(serviceUrlField, {
      target: { value: 'https://halo-wms.met.no/halo/default.map?' },
    });

    const errorText = await screen.findByText(VALIDATIONS_SERVICE_EXISTING);
    expect(errorText).toBeTruthy();
  });

  it('should show error message, if service name is already in store', async () => {
    render(
      <ThemeWrapper>
        <ServicePopup
          servicePopupVariant="save"
          isOpen
          serviceSetLayers={jest.fn()}
          services={mockServices}
        />
      </ThemeWrapper>,
    );

    const serviceNameField = screen.getByRole('textbox', {
      name: /service name/i,
    });

    fireEvent.change(serviceNameField, {
      target: { value: 'Radar Norway' },
    });

    expect(
      await screen.findByText(VALIDATIONS_NAME_EXISTING),
    ).toBeInTheDocument();
  });

  it('should show error message, if service url is invalid', async () => {
    render(
      <ThemeWrapper>
        <ServicePopup
          servicePopupVariant="save"
          isOpen
          serviceSetLayers={jest.fn()}
          services={mockServices}
        />
      </ThemeWrapper>,
    );

    const serviceUrlField = screen.getByRole('textbox', {
      name: serviceUrlLabel,
    });
    fireEvent.change(serviceUrlField, {
      target: { value: 'asdasdasds' },
    });

    const errorText = await screen.findByText(VALIDATIONS_SERVICE_VALID_URL);
    expect(errorText).toBeTruthy();
  });

  it('should show error message, if download fails to start', async () => {
    render(
      <ThemeWrapper>
        <ServicePopup
          servicePopupVariant="save"
          isOpen
          serviceSetLayers={jest.fn()}
          services={mockServices}
        />
      </ThemeWrapper>,
    );

    const serviceNameTextField = screen.getByRole('textbox', {
      name: /service name/i,
    });
    fireEvent.change(serviceNameTextField, {
      target: { value: 'New service' },
    });

    const serviceUrlField = screen.getByRole('textbox', {
      name: serviceUrlLabel,
    });
    fireEvent.blur(serviceUrlField, {
      target: { value: 'https://asdasd' },
    });

    const saveButton = screen.getByTestId(
      'saveServiceButton',
    ) as HTMLButtonElement;
    fireEvent.click(saveButton);

    const errorText = await screen.findByText(VALIDATIONS_REQUEST_FAILED);
    expect(errorText).toBeTruthy();
  });

  it('should hide buttons and disable input if show mode', async () => {
    Object.defineProperty(navigator, 'clipboard', {
      value: {
        writeText: () => {},
      },
      writable: true,
      configurable: true,
    });
    jest.spyOn(navigator.clipboard, 'writeText');

    render(
      <ThemeWrapper>
        <ServicePopup
          servicePopupVariant="show"
          isOpen
          serviceSetLayers={jest.fn()}
          services={mockServices}
          serviceId="serviceid_1"
        />
      </ThemeWrapper>,
    );

    expect(
      screen.getByRole('heading', { name: /service/i }),
    ).toBeInTheDocument();

    expect(
      screen.getAllByRole('button', {
        name: /close/i,
      }),
    ).toHaveLength(2);

    expect(
      screen.queryByRole('button', {
        name: /save/i,
      }),
    ).not.toBeInTheDocument();

    expect(
      screen.getByRole('textbox', { name: /service name/i }),
    ).toHaveAttribute('readonly');
    expect(
      screen.getByRole('textbox', { name: serviceUrlLabel }),
    ).toHaveAttribute('readonly');
    expect(screen.getByRole('textbox', { name: /abstracts/i })).toHaveAttribute(
      'readonly',
    );

    const copyButton = screen.getByRole('button', { name: /copy url/i });
    await user.click(copyButton);
    expect(navigator.clipboard.writeText).toHaveBeenCalled();
  });

  it('should show error message on focus if service name is invalid', async () => {
    render(
      <ThemeWrapper>
        <ServicePopup
          servicePopupVariant="save"
          isOpen
          serviceSetLayers={jest.fn()}
          services={mockServices}
          serviceUrl="https://halo-wms.met.no/halo/default.map?"
        />
      </ThemeWrapper>,
    );

    const serviceNameField = screen.getByRole('textbox', {
      name: serviceUrlLabel,
    });

    fireEvent.focus(serviceNameField);

    expect(
      await screen.findByText(VALIDATIONS_SERVICE_EXISTING),
    ).toBeInTheDocument();
  });

  it('should NOT show error message on focus if service name is blank', async () => {
    render(
      <ThemeWrapper>
        <ServicePopup
          servicePopupVariant="save"
          isOpen
          serviceSetLayers={jest.fn()}
          services={mockServices}
          serviceUrl=""
        />
      </ThemeWrapper>,
    );

    const serviceNameField = screen.getByRole('textbox', {
      name: serviceUrlLabel,
    });

    fireEvent.focus(serviceNameField);
    expect(
      screen.queryByText(VALIDATIONS_FIELD_REQUIRED),
    ).not.toBeInTheDocument();
  });

  it('should start fetching service data when blur on service url field and prefill name and abstract', async () => {
    render(
      <ThemeWrapper>
        <ServicePopup
          servicePopupVariant="save"
          isOpen
          serviceSetLayers={jest.fn()}
          services={{}}
          serviceName="test service"
          serviceUrl="https://mockUrlWithChildren.nl"
        />
      </ThemeWrapper>,
    );

    const serviceUrlField = screen.getByRole('textbox', {
      name: serviceUrlLabel,
    });
    const serviceNameField = screen.getByRole('textbox', {
      name: serviceNameLabel,
    });

    const serviceAbstractField = screen.getByRole('textbox', {
      name: serviceAbstractLabel,
    });

    expect(serviceNameField.getAttribute('value')).toEqual('test service');
    expect(serviceAbstractField.innerHTML).toEqual('');
    fireEvent.blur(serviceUrlField);

    expect(serviceUrlField.getAttribute('disabled')).toBeDefined();

    expect(
      screen
        .getByRole('textbox', {
          name: serviceUrlLabel,
        })
        .getAttribute('disabled'),
    ).toBeDefined();

    expect(
      screen.getByTestId('saveServiceButton').getAttribute('disabled'),
    ).toBeDefined();

    await waitFor(() => {
      expect(serviceNameField.getAttribute('value')).toEqual(
        mockServiceFromStore.title,
      );
    });
    expect(serviceAbstractField.innerHTML).toEqual(
      mockServiceFromStore.abstract,
    );
  });

  it('should show error message, if download fails to start on blur', async () => {
    render(
      <ThemeWrapper>
        <ServicePopup
          servicePopupVariant="save"
          isOpen
          serviceSetLayers={jest.fn()}
          services={mockServices}
          serviceUrl="not a valid url"
        />
      </ThemeWrapper>,
    );

    const serviceUrlField = screen.getByRole('textbox', {
      name: serviceUrlLabel,
    });

    fireEvent.blur(serviceUrlField);

    const errorText = await screen.findByText(VALIDATIONS_SERVICE_VALID_URL);
    expect(errorText).toBeTruthy();
  });

  it('should start fetching if url is valid', async () => {
    const mockServiceUrl = 'https://mockUrlWithChildren.nl';
    render(
      <ThemeWrapper>
        <ServicePopup
          servicePopupVariant="save"
          isOpen
          serviceSetLayers={jest.fn()}
          services={mockServices}
          serviceUrl={mockServiceUrl}
        />
      </ThemeWrapper>,
    );

    const serviceUrlField = screen.getByRole('textbox', {
      name: serviceUrlLabel,
    });
    const serviceNameField = screen.getByRole('textbox', {
      name: serviceNameLabel,
    });

    const serviceAbstract = screen.getByRole('textbox', {
      name: serviceAbstractLabel,
    });

    expect(serviceUrlField.getAttribute('value')).toEqual(mockServiceUrl);
    expect(serviceNameField.getAttribute('value')).toEqual('');
    expect(serviceAbstract.innerHTML).toEqual('');

    await waitFor(() => {
      expect(serviceNameField.getAttribute('value')).toEqual(
        mockServiceFromStore.title,
      );
    });
    expect(serviceAbstract.innerHTML).toEqual(mockServiceFromStore.abstract);
  });

  it('should fire serviceSetLayers action with correct layer information when adding a service', async () => {
    const mockUrl = 'https://mockUrlWithChildren.nl';
    const props: ServicePopupProps = {
      servicePopupVariant: 'add',
      closePopup: jest.fn(),
      isOpen: true,
      serviceSetLayers: jest.fn(),
      services: mockServices,
      showSnackbar: jest.fn(),
    };
    render(
      <ThemeWrapper>
        <ServicePopup {...props} />
      </ThemeWrapper>,
    );
    expect(screen.getByRole('dialog').textContent).toBeTruthy();
    const urlInput = screen.getByRole('textbox', { name: serviceUrlLabel });
    fireEvent.input(urlInput, { target: { value: mockUrl } });
    fireEvent.blur(urlInput);

    await waitFor(() => {
      expect(
        screen
          .getByRole('textbox', { name: serviceNameLabel })
          .getAttribute('value'),
      ).toEqual(mockServiceFromStore.title);
    });

    fireEvent.click(screen.getByRole('button', { name: /save/i }));
    await waitFor(() =>
      expect(props.serviceSetLayers).toHaveBeenCalledWith({
        id: 'serviceid_1',
        scope: 'user',
        layers: [
          {
            abstract: '',
            dimensions: [],
            geographicBoundingBox: undefined,
            keywords: [],
            leaf: true,
            name: 'RAD_NL25_PCP_CM',
            path: [],
            styles: [],
            title: 'RAD_NL25_PCP_CM',
          },
          {
            abstract: '',
            dimensions: [],
            geographicBoundingBox: undefined,
            keywords: [],
            leaf: true,
            name: 'RADNL_OPER_R___25PCPRR_L3_KNMI',
            path: [],
            styles: [],
            title: 'RADNL_OPER_R___25PCPRR_L3_KNMI',
          },
        ],
        name: 'service test title',
        serviceUrl: 'https://mockUrlWithChildren.nl',
        abstract: 'service Abstract',
      }),
    );
    await waitFor(() => expect(props.showSnackbar).toHaveBeenCalledTimes(1));
    await waitFor(() => expect(props.closePopup).toHaveBeenCalledTimes(1));
  });

  it('should show snackbar after copy a url', async () => {
    Object.defineProperty(navigator, 'clipboard', {
      value: {
        writeText: () => {},
      },
      writable: true,
      configurable: true,
    });
    jest.spyOn(navigator.clipboard, 'writeText');

    const props: ServicePopupProps = {
      servicePopupVariant: 'show',
      isOpen: true,
      serviceSetLayers: jest.fn(),
      services: mockServices,
      serviceId: 'serviceid_1',
      serviceUrl: 'test.url',
      showSnackbar: jest.fn(),
    };

    render(
      <ThemeWrapper>
        <ServicePopup {...props} />
      </ThemeWrapper>,
    );

    const copyButton = screen.getByRole('button', { name: /copy url/i });
    await user.click(copyButton);
    expect(navigator.clipboard.writeText).toHaveBeenCalledWith(
      props.serviceUrl,
    );

    expect(props.showSnackbar).toHaveBeenCalledWith(COPY_URL_MESSAGE);
  });

  it('should show snackbar after saving a service', async () => {
    const props: ServicePopupProps = {
      servicePopupVariant: 'save',
      isOpen: true,
      serviceSetLayers: jest.fn(),
      services: {},
      serviceName: 'test service',
      serviceUrl: 'https://mockUrlWithChildren.nl',
      showSnackbar: jest.fn(),
      closePopup: jest.fn(),
    };
    render(
      <ThemeWrapper>
        <ServicePopup {...props} />
      </ThemeWrapper>,
    );

    const serviceNameField = screen.getByRole('textbox', {
      name: serviceNameLabel,
    });

    await waitFor(() => {
      expect(serviceNameField.getAttribute('value')).toEqual(
        mockServiceFromStore.title,
      );
    });

    const saveButton = screen.getByTestId('saveServiceButton');
    fireEvent.click(saveButton);

    await waitFor(() =>
      expect(props.serviceSetLayers).toHaveBeenCalledTimes(1),
    );

    await waitFor(() =>
      expect(props.showSnackbar).toHaveBeenCalledWith(
        getSuccesMessage(mockServiceFromStore.title),
      ),
    );
    await waitFor(() => expect(props.closePopup).toHaveBeenCalledTimes(1));
  });

  it('should autoFocus on serviceUrl for new service', () => {
    render(
      <ThemeWrapper>
        <ServicePopup
          servicePopupVariant="add"
          closePopup={jest.fn()}
          isOpen={true}
          serviceSetLayers={jest.fn()}
          services={mockServices}
        />
      </ThemeWrapper>,
    );
    const input = screen.getByRole('textbox', {
      name: serviceUrlLabel,
    });

    expect(input.matches(':focus')).toBeTruthy();
  });

  it('should display loading indicator when adding new service is in process', async () => {
    jest.useFakeTimers();
    const url = 'https://geoservices.knmi.nl/wms?dataset=RADAR&';
    jest.spyOn(utils, 'loadWMSService').mockImplementationOnce(
      () =>
        new Promise((resolve) => {
          setTimeout(() => {
            resolve({
              id: url,
              name: 'Precipitation Radar NL',
              serviceUrl: url,
              abstract: '',
              layers: [],
              scope: 'user',
            });
          }, 1000);
        }),
    );
    const props: ServicePopupProps = {
      servicePopupVariant: 'save',
      closePopup: jest.fn(),
      isOpen: true,
      serviceSetLayers: jest.fn(),
      services: mockServices,
      showSnackbar: jest.fn(),
    };
    render(
      <ThemeWrapper>
        <ServicePopup {...props} />
      </ThemeWrapper>,
    );

    const serviceNameTextField = screen.getByRole('textbox', {
      name: /service name/i,
    });
    fireEvent.change(serviceNameTextField, {
      target: { value: 'Precipitation Radar NL' },
    });

    const serviceUrlField = screen.getByRole('textbox', {
      name: serviceUrlLabel,
    });
    fireEvent.blur(serviceUrlField, {
      target: { value: url },
    });

    fireEvent.click(screen.getByText('save'));

    await waitFor(() => {
      expect(screen.getByTestId('loadingIndicator')).toBeTruthy();
    });
    expect(props.serviceSetLayers).not.toHaveBeenCalled();
    await act(async () => {
      jest.advanceTimersByTime(1000);
    });
    await waitFor(() => {
      expect(screen.queryByTestId('loadingIndicator')).toBeFalsy();
    });
    const errorText = await screen.findByText(VALIDATIONS_REQUEST_FAILED);
    expect(errorText).toBeTruthy();

    jest.clearAllTimers();
    jest.useRealTimers();
  });
});
