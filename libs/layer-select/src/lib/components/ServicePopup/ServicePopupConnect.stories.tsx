/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */

import React from 'react';
import { createMockStoreWithEggs } from '@opengeoweb/shared';
import { layerSelectTypes } from '@opengeoweb/store';
import { ServicePopupConnect } from './ServicePopupConnect';
import { TestThemeStoreProvider } from '../testUtils/TestThemeStoreProvider';

export default {
  title: 'components/LayerSelect/ServicePopupConnect',
};

const mockState: layerSelectTypes.LayerSelectModuleState = {
  layerSelect: {
    servicePopup: {
      variant: 'add',
      isOpen: true,
      serviceId: 'exampleservice',
      url: 'exampleservice',
    },
    allServicesEnabled: true,
    filters: {
      searchFilter: '',
      activeServices: {
        entities: {
          exampleService: {
            serviceName: 'Example service name',
            serviceUrl: 'https://maps.dwd.de/geoserver/ows?',
            abstract: 'Example abstracts of a service',
            enabled: true,
            scope: 'system',
            filterIds: ['exampleService'],
          },
        },
        ids: ['exampleService'],
      },
      filters: {
        entities: {},
        ids: [],
      },
    },
    activeLayerInfo: {
      serviceName: '',
      name: '',
      title: '',
      leaf: false,
      path: [],
    },
  },
};

const store = createMockStoreWithEggs(mockState);

export const ServicePopupAddWithExistingServiceError: React.FC = () => (
  <TestThemeStoreProvider store={store}>
    <ServicePopupConnect />
  </TestThemeStoreProvider>
);
