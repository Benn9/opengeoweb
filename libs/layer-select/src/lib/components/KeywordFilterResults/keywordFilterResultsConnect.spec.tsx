/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { render, screen } from '@testing-library/react';
import { createMockStoreWithEggs } from '@opengeoweb/shared';
import { layerSelectTypes } from '@opengeoweb/store';
import { KeywordFilterResultsConnect } from './KeywordFilterResultsConnect';
import { TestThemeStoreProvider } from '../testUtils/TestThemeStoreProvider';

describe('src/components/LayerSelect/KeywordFilterResultsConnect', () => {
  it('should render component', async () => {
    const filterId = 'keyword-keyword_A';
    const filters: layerSelectTypes.Filters = {
      entities: {
        [filterId]: {
          id: filterId,
          name: 'keyword_A',
          amount: 1,
          amountVisible: 1,
          checked: true,
          type: layerSelectTypes.FilterType.Keyword,
        },
      },
      ids: [filterId],
    };
    const mockState = {
      layerSelect: {
        filters: {
          filters,
        },
      },
      ui: {
        dialogs: {
          keywordFilter: {
            isOpen: true,
          },
        },
      },
    };
    const store = createMockStoreWithEggs(mockState);

    render(
      <TestThemeStoreProvider store={store}>
        <KeywordFilterResultsConnect />
      </TestThemeStoreProvider>,
    );

    const component = screen.queryByTestId('keywordFilterResults');
    expect(component).toBeTruthy();
  });
});
