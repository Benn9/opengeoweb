/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { fireEvent, render, screen } from '@testing-library/react';
import { createMockStoreWithEggs } from '@opengeoweb/shared';
import { layerSelectActions, layerSelectTypes } from '@opengeoweb/store';
import {
  KeywordFilterResultsListItemConnect,
  KeywordFilterResultsListItemConnectProps,
} from './KeywordFilterResultsListItemConnect';
import { TestThemeStoreProvider } from '../testUtils/TestThemeStoreProvider';

describe('src/components/LayerSelect/KeywordFilterResultsListItemConnect', () => {
  const filterId1 = 'keyword1';
  const filterId2 = 'keyword2';
  const filter1: layerSelectTypes.Filter = {
    id: filterId1,
    name: 'keyword1',
    amount: 1,
    amountVisible: 1,
    checked: true,
    type: layerSelectTypes.FilterType.Keyword,
  };
  const filter2: layerSelectTypes.Filter = {
    id: filterId2,
    name: 'keyword2',
    amount: 1,
    amountVisible: 1,
    checked: true,
    type: layerSelectTypes.FilterType.Keyword,
  };
  const filters: layerSelectTypes.Filters = {
    entities: {
      [filterId1]: filter1,
      [filterId2]: filter2,
    },
    ids: [filterId1, filterId2],
  };
  const mockState = {
    layerSelect: {
      filters: {
        filters,
      },
    },
    ui: {
      dialogs: {
        keywordFilter: {
          isOpen: true,
        },
      },
    },
  };
  const store = createMockStoreWithEggs(mockState);

  const props: KeywordFilterResultsListItemConnectProps = {
    filter: filter1,
  };
  it('should render the component', () => {
    render(
      <TestThemeStoreProvider store={store}>
        <KeywordFilterResultsListItemConnect {...props} />
      </TestThemeStoreProvider>,
    );

    const component = screen.queryByTestId('filterResultListItem');
    expect(component).toBeTruthy();
  });

  it('clicking checkbox should dispatch an acton to toggle one keyword', () => {
    render(
      <TestThemeStoreProvider store={store}>
        <KeywordFilterResultsListItemConnect {...props} />
      </TestThemeStoreProvider>,
    );
    store.clearActions();
    const checkbox = screen.queryByRole('checkbox') as HTMLInputElement;
    fireEvent.click(checkbox);

    const expectedAction = [
      layerSelectActions.toggleFilter({
        filterIds: ['keyword1'],
      }),
    ];

    expect(store.getActions()).toEqual(expectedAction);
  });

  it('should dispatch an acton that checks only one checkbox', async () => {
    render(
      <TestThemeStoreProvider store={store}>
        <KeywordFilterResultsListItemConnect {...props} />
      </TestThemeStoreProvider>,
    );
    store.clearActions();

    const filterResultsListItems = screen.queryAllByTestId(
      'filterResultListItem',
    );
    fireEvent.mouseOver(filterResultsListItems[0]);
    fireEvent.focus(filterResultsListItems[0]);

    const onlyButton = screen.queryByTestId('onlyButton')!;
    fireEvent.click(onlyButton);

    const expectedAction = [
      layerSelectActions.enableOnlyOneFilter({
        filterId: 'keyword1',
      }),
    ];

    expect(store.getActions()).toEqual(expectedAction);
  });
});
