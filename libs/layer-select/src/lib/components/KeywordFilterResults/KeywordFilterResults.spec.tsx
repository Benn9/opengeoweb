/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { fireEvent, render, screen } from '@testing-library/react';
import { createMockStoreWithEggs } from '@opengeoweb/shared';
import { layerSelectTypes } from '@opengeoweb/store';
import {
  KeywordFilterResults,
  KeywordFilterResultsProps,
  DIALOG_TITLE,
} from './KeywordFilterResults';
import { TestThemeStoreProvider } from '../testUtils/TestThemeStoreProvider';

describe('src/components/LayerSelect/KeywordFilterResults', () => {
  const filterId1 = 'keyword1';
  const filterId2 = 'keyword2';
  const filter1: layerSelectTypes.Filter = {
    id: filterId1,
    name: 'keyword1',
    amount: 1,
    amountVisible: 1,
    checked: true,
    type: layerSelectTypes.FilterType.Keyword,
  };
  const filter2: layerSelectTypes.Filter = {
    id: filterId2,
    name: 'keyword2',
    amount: 1,
    amountVisible: 1,
    checked: true,
    type: layerSelectTypes.FilterType.Keyword,
  };
  const filters: layerSelectTypes.Filters = {
    entities: {
      [filterId1]: filter1,
      [filterId2]: filter2,
    },
    ids: ['keyword1', 'keyword2'],
  };
  const mockState = {
    layerSelect: {
      filters: {
        filters,
      },
    },
    ui: {
      dialogs: {
        keywordFilter: {
          isOpen: true,
        },
      },
    },
  };
  const store = createMockStoreWithEggs(mockState);

  const props: KeywordFilterResultsProps = {
    filters: [filter1, filter2],
    isOpen: true,
  };
  it('should render the component', async () => {
    render(
      <TestThemeStoreProvider store={store}>
        <KeywordFilterResults {...props} />
      </TestThemeStoreProvider>,
    );

    const component = screen.queryByTestId('keywordFilterResults');
    expect(component).toBeTruthy();

    expect(await screen.findByText(DIALOG_TITLE)).toBeVisible();
  });

  it('should toggle all checkboxes', () => {
    render(
      <TestThemeStoreProvider store={store}>
        <KeywordFilterResults {...props} />
      </TestThemeStoreProvider>,
    );

    const customSwitch = screen.queryByTestId('customSwitch')!;

    const checkboxes = screen.queryAllByRole('checkbox') as HTMLInputElement[];
    checkboxes.forEach((checkbox) => {
      expect(checkbox.checked).toBe(true);
    });

    fireEvent.click(customSwitch);
    setTimeout(() => {
      const checkboxes = screen.queryAllByRole(
        'checkbox',
      ) as HTMLInputElement[];
      checkboxes.forEach((checkbox) => {
        expect(checkbox.checked).toBe(false);
      });
    }, 2000);

    fireEvent.click(customSwitch);
    setTimeout(() => {
      const checkboxes = screen.queryAllByRole(
        'checkbox',
      ) as HTMLInputElement[];
      checkboxes.forEach((checkbox) => {
        expect(checkbox.checked).toBe(true);
      });
    }, 2000);
  });

  it('should toggle only one checkbox to true', async () => {
    render(
      <TestThemeStoreProvider store={store}>
        <KeywordFilterResults {...props} />
      </TestThemeStoreProvider>,
    );

    const filterResultsListItems = screen.queryAllByTestId(
      'filterResultListItem',
    );
    fireEvent.mouseOver(filterResultsListItems[0]);
    fireEvent.focus(filterResultsListItems[0]);

    const onlyButton = screen.queryByTestId('onlyButton')!;
    fireEvent.click(onlyButton);

    setTimeout(() => {
      const checkboxes = screen.queryAllByRole(
        'checkbox',
      ) as HTMLInputElement[];
      checkboxes.forEach((checkbox, index) => {
        if (index === 0) {
          expect(checkbox.checked).toBe(true);
        } else {
          expect(checkbox.checked).toBe(false);
        }
      });
    }, 2000);
  });
});
