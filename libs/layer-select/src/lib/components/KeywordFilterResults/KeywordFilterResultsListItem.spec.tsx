/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';

import { fireEvent, render, screen, within } from '@testing-library/react';
import { layerSelectTypes } from '@opengeoweb/store';
import { ThemeWrapper } from '@opengeoweb/theme';
import {
  FilterListItem,
  FilterListItemProps,
} from './KeywordFilterResultsListItem';

describe('src/components/LayerSelect/KeywordFilterResultsListItem', () => {
  const filterId = 'filterId';
  const filterName = 'keyword1';
  const props: FilterListItemProps = {
    filter: {
      id: filterId,
      name: filterName,
      amount: 1,
      amountVisible: 1,
      checked: true,
      type: layerSelectTypes.FilterType.Keyword,
    },
    toggleFilter: jest.fn(),
    enableOnlyOneFilter: jest.fn(),
  };
  it('should work correctly', () => {
    render(
      <ThemeWrapper>
        <FilterListItem {...props} />
      </ThemeWrapper>,
    );

    const listItem = screen.getByRole('listitem');
    expect(listItem).toHaveTextContent(`${filterName} (1)`);

    // should call toggleFilter with correct value
    const checkbox = within(listItem).getByRole('checkbox');
    fireEvent.click(checkbox);
    expect(props.toggleFilter).toBeCalledWith([filterId]);

    // should call enableOnlyOneFilter with correct value
    fireEvent.mouseOver(listItem);
    fireEvent.click(screen.getByText(/only/i));
    expect(props.enableOnlyOneFilter).toBeCalledWith(filterId);
  });
});
