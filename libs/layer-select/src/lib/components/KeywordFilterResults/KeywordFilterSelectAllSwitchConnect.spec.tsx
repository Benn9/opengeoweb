/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { fireEvent, render, screen } from '@testing-library/react';
import { createMockStoreWithEggs } from '@opengeoweb/shared';
import { layerSelectTypes, layerSelectActions } from '@opengeoweb/store';
import { KeywordFilterSelectAllSwitchConnect } from './KeywordFilterSelectAllSwitchConnect';
import { TestThemeStoreProvider } from '../testUtils/TestThemeStoreProvider';

describe('src/components/LayerSelect/KeywordFilterSelectAllSwitchConnect', () => {
  const filters: layerSelectTypes.Filters = {
    entities: {
      keyword1: {
        id: 'keyword1',
        name: 'keyword1',
        amount: 1,
        amountVisible: 1,
        checked: true,
        type: layerSelectTypes.FilterType.Keyword,
      },
      keyword2: {
        id: 'keyword2',
        name: 'keyword2',
        amount: 1,
        amountVisible: 1,
        checked: false,
        type: layerSelectTypes.FilterType.Keyword,
      },
    },
    ids: ['keyword1', 'keyword2'],
  };
  const mockState = {
    layerSelect: {
      filters: {
        filters,
      },
    },
    ui: {
      dialogs: {
        keywordFilter: {
          isOpen: true,
        },
      },
    },
  };

  const store = createMockStoreWithEggs(mockState);
  const filtersAllTrue: layerSelectTypes.Filters = {
    entities: {
      keyword1: {
        id: 'keyword1',
        name: 'keyword1',
        amount: 1,
        amountVisible: 1,
        checked: true,
        type: layerSelectTypes.FilterType.Keyword,
      },
      keyword2: {
        id: 'keyword2',
        name: 'keyword2',
        amount: 1,
        amountVisible: 1,
        checked: true,
        type: layerSelectTypes.FilterType.Keyword,
      },
    },
    ids: ['keyword1', 'keyword2'],
  };
  const mockStateAllTrue = {
    layerSelect: {
      filters: {
        filters: filtersAllTrue,
      },
    },
    ui: {
      dialogs: {
        keywordFilter: {
          isOpen: true,
        },
      },
    },
  };

  const storeAllTrue = createMockStoreWithEggs(mockStateAllTrue);

  it('should render the component', () => {
    render(
      <TestThemeStoreProvider store={store}>
        <KeywordFilterSelectAllSwitchConnect />
      </TestThemeStoreProvider>,
    );

    const component = screen.getByTestId('customSwitch');
    expect(component).toBeTruthy();
  });

  it('should call action with all false keywords to toggle all true', () => {
    render(
      <TestThemeStoreProvider store={store}>
        <KeywordFilterSelectAllSwitchConnect />
      </TestThemeStoreProvider>,
    );

    const customSwitch = screen.queryAllByRole(
      'checkbox',
    )[0] as HTMLInputElement;
    fireEvent.click(customSwitch);
    const expectedActionToTrue = [
      layerSelectActions.toggleFilter({
        filterIds: ['keyword2'],
      }),
    ];
    expect(store.getActions()).toEqual(expectedActionToTrue);
  });

  it('should call action with all keywords to toggle all false when all are true', () => {
    render(
      <TestThemeStoreProvider store={storeAllTrue}>
        <KeywordFilterSelectAllSwitchConnect />
      </TestThemeStoreProvider>,
    );

    const customSwitch = screen.queryAllByRole(
      'checkbox',
    )[0] as HTMLInputElement;
    fireEvent.click(customSwitch);
    const expectedActionToFalse = [
      layerSelectActions.toggleFilter({
        filterIds: ['keyword1', 'keyword2'],
      }),
    ];
    expect(storeAllTrue.getActions()).toEqual(expectedActionToFalse);
  });
});
