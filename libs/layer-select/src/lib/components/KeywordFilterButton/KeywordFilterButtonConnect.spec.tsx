/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { fireEvent, render, screen } from '@testing-library/react';
import { createMockStoreWithEggs } from '@opengeoweb/shared';
import { lightTheme } from '@opengeoweb/theme';
import { uiActions, uiTypes } from '@opengeoweb/store';
import { KeywordFilterButtonConnect } from './KeywordFilterButtonConnect';
import { TestThemeStoreProvider } from '../testUtils/TestThemeStoreProvider';

const props = {
  mapId: 'mapid-1',
};
describe('src/components/LayerSelect/KeywordFilterButton', () => {
  it('should render the component', () => {
    const store = createMockStoreWithEggs({});

    render(
      <TestThemeStoreProvider theme={lightTheme} store={store}>
        <KeywordFilterButtonConnect {...props} />
      </TestThemeStoreProvider>,
    );

    const button = screen.queryByTestId('keywordFilterButton');
    expect(button).toBeTruthy();
  });

  it('should dispatch action with passed in mapid when clicked', () => {
    const mockState = {
      ui: {
        keywordFilter: {
          type: uiTypes.DialogTypes.KeywordFilter,
          activeMapId: 'map1',
          isOpen: false,
        },
      },
    };
    const store = createMockStoreWithEggs(mockState);
    const props = {
      mapId: 'mapId_123',
    };
    render(
      <TestThemeStoreProvider store={store}>
        <KeywordFilterButtonConnect {...props} />
      </TestThemeStoreProvider>,
    );

    // button should be present
    expect(screen.getByTestId('keywordFilterButton')).toBeTruthy();

    // open the keyword filter dialog
    fireEvent.click(screen.getByTestId('keywordFilterButton'));
    const expectedAction = [
      uiActions.setActiveMapIdForDialog({
        type: uiTypes.DialogTypes.KeywordFilter,
        mapId: props.mapId,
        setOpen: true,
        source: 'app',
      }),
    ];

    expect(store.getActions()).toEqual(expectedAction);
  });

  it('should dispatch action to close dialog when dialog is already opened for corresponding map', () => {
    const mockState = {
      ui: {
        dialogs: {
          keywordFilter: {
            type: uiTypes.DialogTypes.KeywordFilter,
            activeMapId: 'mapId_123',
            isOpen: true,
            source: 'app',
          },
        },
      },
    };
    const store = createMockStoreWithEggs(mockState);

    const props = {
      mapId: 'mapId_123',
    };
    render(
      <TestThemeStoreProvider store={store}>
        <KeywordFilterButtonConnect {...props} />
      </TestThemeStoreProvider>,
    );

    // button should be present
    expect(screen.getByTestId('keywordFilterButton')).toBeTruthy();

    // close the keyword filter dialog
    fireEvent.click(screen.getByTestId('keywordFilterButton'));
    const expectedAction = [
      uiActions.setActiveMapIdForDialog({
        type: uiTypes.DialogTypes.KeywordFilter,
        mapId: props.mapId,
        setOpen: false,
        source: 'app',
      }),
    ];

    expect(store.getActions()).toEqual(expectedAction);
  });

  it('should dispatch action with correct mapId when clicked with isMultiMap', () => {
    const mapId1 = 'mapId123';
    const mapId2 = 'mapId1234';
    const mockState = {
      ui: {
        layerSelect: {
          type: `${uiTypes.DialogTypes.KeywordFilter}-${mapId2}`,
          activeMapId: 'map1',
          isOpen: false,
        },
      },
    };
    const store = createMockStoreWithEggs(mockState);

    render(
      <>
        <TestThemeStoreProvider store={store}>
          <KeywordFilterButtonConnect mapId={mapId1} isMultiMap />
        </TestThemeStoreProvider>
        <TestThemeStoreProvider store={store}>
          <KeywordFilterButtonConnect mapId={mapId2} isMultiMap />
        </TestThemeStoreProvider>
      </>,
    );

    for (const button of screen.getAllByTestId('keywordFilterButton')) {
      expect(button).toBeTruthy();
      fireEvent.click(button);
    }

    const expectedAction = [
      uiActions.setActiveMapIdForDialog({
        type: `${uiTypes.DialogTypes.KeywordFilter}-${mapId1}`,
        mapId: mapId1,
        setOpen: true,
        source: 'app',
      }),
      uiActions.setActiveMapIdForDialog({
        type: `${uiTypes.DialogTypes.KeywordFilter}-${mapId2}`,
        mapId: mapId2,
        setOpen: true,
        source: 'app',
      }),
    ];

    expect(store.getActions()).toEqual(expectedAction);
  });
});
