/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */

import { layerSelectTypes } from '@opengeoweb/store';
import { sortByService } from './LayerSelectUtils';

describe('src/components/LayerSelect/LayerSelectUtils', () => {
  const activeServices: layerSelectTypes.ActiveServiceObjectEntities = {
    serviceid_1: {
      serviceName: 'name_c',
      serviceUrl: 'https://service2',
      enabled: true,
      filterIds: [],
    },
    serviceid_2: {
      serviceName: 'name_a',
      serviceUrl: 'https://service1',
      enabled: true,
      filterIds: [],
    },
    serviceid_3: {
      serviceName: 'name_b',
      serviceUrl: 'https://service2',
      enabled: true,
      filterIds: [],
    },
  };

  it('should sort services correctly', () => {
    const sorted = sortByService(activeServices);
    expect(
      Object.values(sorted).map(
        (service: layerSelectTypes.ActiveServiceObject) => service.serviceName,
      ),
    ).toEqual(['name_a', 'name_b', 'name_c']);
    activeServices['serviceid_4'] = {
      serviceName: 'name_d',
      serviceUrl: 'https://service2',
      enabled: true,
      filterIds: [],
    };
    activeServices['serviceid_5'] = {
      serviceName: 'name_ab',
      serviceUrl: 'https://service2',
      enabled: true,
      filterIds: [],
    };
    const resorted = sortByService(activeServices);
    expect(
      Object.values(resorted).map(
        (service: layerSelectTypes.ActiveServiceObject) => service.serviceName,
      ),
    ).toEqual(['name_a', 'name_ab', 'name_b', 'name_c', 'name_d']);
  });
});
