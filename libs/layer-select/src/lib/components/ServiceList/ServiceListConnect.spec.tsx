/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { act, render, screen } from '@testing-library/react';
import { createStore, serviceActions } from '@opengeoweb/store';
import userEvent from '@testing-library/user-event';
import { ServiceListConnect } from './ServiceListConnect';
import { TestThemeStoreProvider } from '../testUtils/TestThemeStoreProvider';

describe('src/components/LayerSelect/ServiceList', () => {
  it('should work as expected', async () => {
    const store = createStore();
    render(
      <TestThemeStoreProvider store={store}>
        <ServiceListConnect setHeight={jest.fn()} />,
      </TestThemeStoreProvider>,
    );

    const serviceName0 = 'abc';
    const serviceName1 = 'xyz';
    act(() => {
      store.dispatch(
        serviceActions.serviceSetLayers({
          id: 'id1',
          layers: [
            { leaf: true, name: 'layerName1', path: [], title: 'layerTitle1' },
          ],
          name: serviceName1,
          scope: 'system',
          serviceUrl: 'serviceUrl1',
        }),
      );

      store.dispatch(
        serviceActions.serviceSetLayers({
          id: 'id0',
          layers: [
            { leaf: true, name: 'layerName0', path: [], title: 'layerTitle0' },
          ],
          name: serviceName0,
          scope: 'system',
          serviceUrl: 'serviceUrl0',
        }),
      );
    });

    const user = userEvent.setup();

    // should render buttons in order by their name
    const buttons = screen.getAllByRole('button');
    const ALL = 'All';
    expect(buttons[0]).toHaveTextContent(ALL);
    expect(buttons[1]).toHaveTextContent(serviceName0);
    expect(buttons[2]).toHaveTextContent(serviceName1);

    // initially All button is pressed
    screen.getByRole('button', { name: ALL, pressed: true });
    screen.getByRole('button', { name: serviceName0, pressed: false });
    screen.getByRole('button', { name: serviceName1, pressed: false });

    // click name1
    await user.click(screen.getByRole('button', { name: serviceName1 }));

    screen.getByRole('button', { name: ALL, pressed: false });
    screen.getByRole('button', { name: serviceName0, pressed: false });
    screen.getByRole('button', { name: serviceName1, pressed: true });

    // click All
    await user.click(screen.getByRole('button', { name: ALL }));

    await screen.findByRole('button', { name: ALL, pressed: true });
    screen.getByRole('button', { name: serviceName0, pressed: false });
    screen.getByRole('button', { name: serviceName1, pressed: false });

    // click All again does nothing
    await user.click(screen.getByRole('button', { name: ALL }));

    screen.getByRole('button', { name: ALL, pressed: true });
    screen.getByRole('button', { name: serviceName0, pressed: false });
    screen.getByRole('button', { name: serviceName1, pressed: false });

    // click name0
    await user.click(screen.getByRole('button', { name: serviceName0 }));

    screen.getByRole('button', { name: ALL, pressed: false });
    screen.getByRole('button', { name: serviceName0, pressed: true });
    screen.getByRole('button', { name: serviceName1, pressed: false });

    // click name0 again turns All button on
    await user.click(screen.getByRole('button', { name: serviceName0 }));

    screen.getByRole('button', { name: ALL, pressed: true });
    screen.getByRole('button', { name: serviceName0, pressed: false });
    screen.getByRole('button', { name: serviceName1, pressed: false });

    // click name 1
    await user.click(screen.getByRole('button', { name: serviceName1 }));

    screen.getByRole('button', { name: ALL, pressed: false });
    screen.getByRole('button', { name: serviceName0, pressed: false });
    screen.getByRole('button', { name: serviceName1, pressed: true });
  });
});
