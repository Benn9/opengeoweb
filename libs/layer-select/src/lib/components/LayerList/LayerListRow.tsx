/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */

import React from 'react';
import { Box, Grid, Typography } from '@mui/material';
import { SearchHighlight } from '@opengeoweb/shared';
import { layerTypes, serviceTypes } from '@opengeoweb/store';
import { LayerAddRemoveButton } from './LayerAddRemoveButton';
import { LayerInfoButtonConnect } from '../LayerInfo';
import { LayerListDimensions } from './LayerListDimensions';
import {
  widthToColumns,
  widthToRowHeight,
} from '../LayerSelect/LayerSelectUtils';
import { getDimensionsList } from '../LayerInfo/LayerInfoUtils';

interface AddLayerParams {
  serviceUrl: string;
  layerName: string;
}

interface DeleteLayerParams {
  layerId: string;
  layerIndex: number;
}
interface LayerListRowProps {
  layer: serviceTypes.ServiceLayer;
  layerIndex: number;
  service: serviceTypes.ReduxService;
  addLayer: ({ serviceUrl, layerName }: AddLayerParams) => void;
  deleteLayer: ({ layerId, layerIndex }: DeleteLayerParams) => void;
  mapLayers: layerTypes.ReduxLayer[];
  mapId: string;
  layerSelectWidth: number;
  searchFilter: string;
}

const layerInfoStyle = {
  fontSize: '10px',
  textOverflow: 'ellipsis',
  overflow: 'hidden',
  whiteSpace: 'nowrap',
  lineHeight: 1,
};

export const LayerListRow: React.FC<LayerListRowProps> = ({
  layer,
  layerIndex,
  service,
  addLayer,
  deleteLayer,
  mapLayers,
  mapId,
  layerSelectWidth,
  searchFilter,
}) => {
  const rowHeight = widthToRowHeight(layerSelectWidth);
  const columns = widthToColumns(layerSelectWidth);
  const dimensions = getDimensionsList(layer.dimensions!);

  return (
    <Grid
      data-testid="layerListLayerRow"
      container
      justifyContent="center"
      alignItems="center"
      sx={{
        backgroundColor: 'geowebColors.cards.cardContainer',
        padding: '8px',
        marginBottom: '4px',
        height: rowHeight,
        border: 'solid 1px',
        borderColor: 'geowebColors.cards.cardContainerBorder',
      }}
    >
      <Grid container item sx={columns.column8}>
        <Grid item xs={12}>
          <Box
            sx={{
              fontSize: '12px',
              height: '14px',
              lineHeight: '14px',
              fontWeight: 500,
              textOverflow: 'ellipsis',
              overflow: 'hidden',
            }}
          >
            <SearchHighlight text={layer.title} search={searchFilter} />
          </Box>
        </Grid>
        <Grid item container direction="column" sx={columns.column1}>
          <Box
            sx={{
              fontSize: '12px',
              height: '18px',
              lineHeight: '18px',
              textOverflow: 'ellipsis',
              overflow: 'hidden',
            }}
          >
            <SearchHighlight text={service.name!} search={searchFilter} />
          </Box>
          <LayerInfoButtonConnect
            layer={layer}
            mapId={mapId}
            serviceName={service.name!}
          />
        </Grid>
        <Grid item sx={columns.column2}>
          <Grid item sx={columns.column3}>
            {layer.abstract && (
              <Typography data-testid="layerAbstract" sx={layerInfoStyle}>
                <SearchHighlight text={layer.abstract} search={searchFilter} />
              </Typography>
            )}
          </Grid>
          <Grid item container sx={columns.column4}>
            <Grid item sx={columns.column5}>
              <LayerListDimensions dimensions={dimensions} />
            </Grid>
            <Grid item sx={columns.column6}>
              {layer.path.length > 0 && (
                <Typography sx={layerInfoStyle}>
                  Groups:{' '}
                  <SearchHighlight
                    text={layer.path.join(' / ')}
                    search={searchFilter}
                  />
                </Typography>
              )}
              {layer.keywords?.length! > 0 && (
                <Typography sx={layerInfoStyle}>
                  Keywords:{' '}
                  <SearchHighlight
                    text={layer
                      .keywords!.slice()
                      .sort((a, b) => {
                        return a.localeCompare(b);
                      })
                      .join(', ')}
                    search={searchFilter}
                  />
                </Typography>
              )}
            </Grid>
          </Grid>
        </Grid>
      </Grid>
      <Grid
        container
        item
        sx={columns.column9}
        justifyContent="flex-end"
        alignItems="center"
      >
        <Box sx={columns.column7}>
          <LayerAddRemoveButton
            layer={layer as unknown as layerTypes.ReduxLayer}
            layerIndex={layerIndex}
            serviceUrl={service.serviceUrl!}
            addLayer={addLayer}
            deleteLayer={deleteLayer}
            mapLayers={mapLayers}
          />
        </Box>
      </Grid>
    </Grid>
  );
};
