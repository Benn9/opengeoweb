/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */
import * as React from 'react';
import { InputAdornment, SxProps, Theme } from '@mui/material';
import { useController, useFormContext } from 'react-hook-form';
import moment, { Moment } from 'moment-timezone';
import {
  DateTimePicker,
  DateTimePickerProps,
  DateTimePickerSlotsComponentsProps,
} from '@mui/x-date-pickers';
import { CalendarToday } from '@opengeoweb/theme';
import ReactHookFormFormControl from './ReactHookFormFormControl';
import { ReactHookFormInput } from './types';
import { getErrors } from './formUtils';
import { defaultProps } from './utils';

const OpenPicker = (): React.ReactElement => <CalendarToday />;

// TODO: Place this default setting somewhere more appropriate, should only need to fire once https://gitlab.com/opengeoweb/opengeoweb/-/issues/331
moment.tz.setDefault('Etc/GMT-0');

export const getFormattedValue = (value: Moment | string): string | Moment => {
  const isMoment = moment(value).isValid();
  return isMoment ? moment.utc(value).format() : value;
};

type ReactHookKeyboardDateTimePickerProps = DateTimePickerProps<Moment> &
  DateTimePickerSlotsComponentsProps<Moment> &
  ReactHookFormInput<{
    defaultNullValue?: string | null;
    value?: string;
    onChange?: (value: string | Moment) => void;
    format?: string;
    sx?: SxProps<Theme>;
  }> & { 'data-testid'?: string; shouldHideUTC?: boolean };

const ReactHookKeyboardDateTimePicker: React.FC<
  ReactHookKeyboardDateTimePickerProps
> = ({
  name,
  rules,
  disabled,
  label = 'Select date and time',
  format = 'YYYY/MM/DD HH:mm',
  openTo = 'hours',
  defaultNullValue = null,
  helperText = '',
  onChange = (): void => {},
  className,
  sx,
  isReadOnly,
  inputAdornment,
  shouldHideUTC,
  ...otherProps
}: ReactHookKeyboardDateTimePickerProps) => {
  const { control } = useFormContext();
  const {
    field: { onChange: onChangeField, value, ref },
    formState: { errors: formErrors },
  } = useController({
    name,
    control,
    rules,
    defaultValue: defaultNullValue,
    ...defaultProps,
  });

  const errors = getErrors(name, formErrors);

  // Ensure value is a Moment object
  const dateValue = value ? moment(value) : null;

  return (
    <ReactHookFormFormControl
      disabled={disabled}
      errors={errors}
      className={className}
      sx={sx}
      isReadOnly={isReadOnly}
    >
      <DateTimePicker
        desktopModeMediaQuery="@media (min-width: 720px)"
        ampm={false}
        format={format}
        label={label}
        value={dateValue}
        openTo={openTo}
        disabled={disabled}
        onChange={(value: Moment): void => {
          onChangeField(getFormattedValue(value));
          onChange(getFormattedValue(value));
        }}
        inputRef={ref}
        slotProps={{
          textField: {
            variant: 'filled',
            helperText,
            error: !!errors,
            name,
            placeholder: disabled ? '' : format.toLowerCase(),
            InputProps: {
              ...(!shouldHideUTC && {
                endAdornment: (
                  <InputAdornment style={{ paddingTop: '16px' }} position="end">
                    UTC
                  </InputAdornment>
                ),
              }),
            },

            // eslint-disable-next-line @typescript-eslint/ban-ts-comment
            // @ts-ignore
            'data-testid': otherProps['data-testid'],
            ...(isReadOnly && {
              readOnly: true,
            }),
          },
          inputAdornment: {
            ...(inputAdornment || { position: 'start' }),
          },
          openPickerButton: {
            size: 'small',
            sx: { padding: 0 },
          },
        }}
        slots={{
          openPickerIcon: OpenPicker,
        }}
        timeSteps={{ minutes: 1 }}
        {...otherProps}
      />
    </ReactHookFormFormControl>
  );
};
export default ReactHookKeyboardDateTimePicker;
