/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */
import { Button } from '@mui/material';
import * as React from 'react';
import { useFormContext } from 'react-hook-form';

import { ReactHookFormDateTime, isValidDate } from '.';
import {
  FormFieldsWrapper,
  FormFieldsWrapperProps,
  StoryLayoutProps,
  zeplinLinks,
} from './Providers';

export default {
  title: 'ReactHookForm/Date Time',
};

const DateTimeDemoLayout = ({
  children,
}: StoryLayoutProps): React.ReactElement => (
  <>
    <ReactHookFormDateTime
      name="dateTime-A"
      rules={{
        required: true,
        validate: {
          isValidDate,
        },
      }}
      label="Test label"
    />

    <ReactHookFormDateTime
      name="dateTime-B"
      rules={{
        required: true,
        validate: {
          isValidDate,
        },
      }}
    />

    <ReactHookFormDateTime
      name="dateTime-readonly"
      label="readonly disabled"
      disabled
      isReadOnly
      rules={{
        required: true,
        validate: {
          isValidDate,
        },
      }}
    />

    <ReactHookFormDateTime
      name="dateTime-C"
      disabled
      rules={{
        required: true,
        validate: {
          isValidDate,
        },
      }}
    />

    <ReactHookFormDateTime
      name="dateTime-D"
      disabled
      rules={{
        required: true,
        validate: {
          isValidDate,
        },
      }}
    />

    <ReactHookFormDateTime
      name="dateTime-E"
      label="Spaceweather example"
      disablePast
      format="YYYY-MM-DD HH:mm"
      rules={{
        required: true,
        validate: {
          isValidDate,
        },
      }}
      shouldHideUTC
      sx={{
        width: 200,
        marginRight: 2,
      }}
    />

    {children}
  </>
);

const DateTimeDemo = (): React.ReactElement => {
  const { handleSubmit } = useFormContext();

  return (
    <DateTimeDemoLayout>
      <Button
        variant="contained"
        color="secondary"
        onClick={(): void => {
          handleSubmit(() => {})();
        }}
      >
        Validate
      </Button>
    </DateTimeDemoLayout>
  );
};

export const DateTime = (): React.ReactElement => (
  <FormFieldsWrapper
    options={{
      mode: 'onChange',
      reValidateMode: 'onChange',
      defaultValues: {
        'dateTime-B': '2021-01-01T12:00:00Z',
        'dateTime-D': '2021-01-01T12:00:00Z',
        'dateTime-readonly': '2021-01-01T12:00:00Z',
      },
    }}
  >
    <DateTimeDemo />
  </FormFieldsWrapper>
);

DateTime.parameters = {
  zeplinLink: zeplinLinks,
};

// snapshots
const SnapShotLayout = ({
  ...props
}: FormFieldsWrapperProps): React.ReactElement => (
  <FormFieldsWrapper
    {...props}
    options={{
      mode: 'onChange',
      reValidateMode: 'onChange',
      defaultValues: {
        'dateTime-B': '2021-01-01T12:00:00Z',
        'dateTime-D': '2021-01-01T12:00:00Z',
        'dateTime-readonly': '2021-01-01T12:00:00Z',
      },
    }}
  >
    <DateTimeDemoLayout />
  </FormFieldsWrapper>
);

// light theme
export const DateTimeLightTheme = (): React.ReactElement => <SnapShotLayout />;
DateTimeLightTheme.storyName = 'Date Time light theme (takeSnapshot)';
DateTimeLightTheme.parameters = {
  zeplinLink: [zeplinLinks[0]],
};

// dark theme
export const DateTimeDarkTheme = (): React.ReactElement => (
  <SnapShotLayout isDarkTheme />
);
DateTimeDarkTheme.storyName = 'Date Time dark theme (takeSnapshot)';
DateTimeDarkTheme.parameters = {
  zeplinLink: [zeplinLinks[1]],
};
