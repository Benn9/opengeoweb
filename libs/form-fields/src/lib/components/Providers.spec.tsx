/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */

import { render, screen } from '@testing-library/react';
import React from 'react';

import { FormFieldsWrapper, FormFieldsWrapperOldTheme } from './Providers';

describe('components/Providers', () => {
  describe('FormFieldsWrapper', () => {
    it('should render with default props', async () => {
      render(
        <FormFieldsWrapper>
          <p>test child</p>
        </FormFieldsWrapper>,
      );

      expect(await screen.findByText('test child')).toBeTruthy();
    });
  });

  describe('FormFieldsWrapperOldTheme', () => {
    it('should render with default props', async () => {
      render(
        <FormFieldsWrapperOldTheme>
          <p>test child</p>
        </FormFieldsWrapperOldTheme>,
      );

      expect(await screen.findByText('test child')).toBeTruthy();
    });
  });
});
