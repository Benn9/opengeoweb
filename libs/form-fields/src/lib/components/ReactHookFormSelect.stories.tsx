/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */
import { Button, MenuItem } from '@mui/material';
import * as React from 'react';
import { useFormContext } from 'react-hook-form';

import { ReactHookFormSelect } from '.';
import { FormFieldsWrapper, StoryLayoutProps, zeplinLinks } from './Providers';

export default {
  title: 'ReactHookForm/Select',
};

const SelectDemoLayout = ({
  children,
}: StoryLayoutProps): React.ReactElement => (
  <>
    <ReactHookFormSelect
      name="select1"
      label="Unit"
      rules={{
        required: true,
      }}
    >
      <MenuItem value="">-</MenuItem>
      <MenuItem value="FL" key="FL">
        FL
      </MenuItem>
      <MenuItem value="M" key="M">
        M
      </MenuItem>
      <MenuItem value="FT" key="FT">
        FT
      </MenuItem>
    </ReactHookFormSelect>

    <ReactHookFormSelect
      name="select2"
      label="Unit"
      defaultValue="FT"
      rules={{
        required: true,
      }}
    >
      <MenuItem value="FL" key="FL">
        FL
      </MenuItem>
      <MenuItem value="M" key="M">
        M
      </MenuItem>
      <MenuItem value="FT" key="FT">
        FT
      </MenuItem>
    </ReactHookFormSelect>

    <ReactHookFormSelect
      name="select3"
      label="Unit"
      defaultValue="FT"
      disabled
      rules={{
        required: true,
      }}
    >
      <MenuItem value="FL" key="FL">
        FL
      </MenuItem>
      <MenuItem value="M" key="M">
        M
      </MenuItem>
      <MenuItem value="FT" key="FT">
        FT
      </MenuItem>
    </ReactHookFormSelect>

    <ReactHookFormSelect
      name="readonly"
      label="Readonly"
      defaultValue="M"
      disabled
      isReadOnly
      rules={{
        required: true,
      }}
    >
      <MenuItem value="FL" key="FL">
        FL
      </MenuItem>
      <MenuItem value="M" key="M">
        M
      </MenuItem>
      <MenuItem value="FT" key="FT">
        FT
      </MenuItem>
    </ReactHookFormSelect>
    {children}
  </>
);

const ReactHookFormSelectDemo = (): React.ReactElement => {
  const { handleSubmit } = useFormContext();

  return (
    <SelectDemoLayout>
      <Button
        variant="contained"
        color="secondary"
        onClick={(): void => {
          handleSubmit(() => {})();
        }}
      >
        Validate
      </Button>
    </SelectDemoLayout>
  );
};

export const Select = (): React.ReactElement => (
  <FormFieldsWrapper>
    <ReactHookFormSelectDemo />
  </FormFieldsWrapper>
);

// light theme
export const SelectLightTheme = (): React.ReactElement => (
  <FormFieldsWrapper>
    <SelectDemoLayout />
  </FormFieldsWrapper>
);
SelectLightTheme.storyName = 'Select light theme (takeSnapshot)';
SelectLightTheme.parameters = {
  zeplinLink: [zeplinLinks[0]],
};

// dark theme
export const SelectDarkTheme = (): React.ReactElement => (
  <FormFieldsWrapper isDarkTheme>
    <SelectDemoLayout />
  </FormFieldsWrapper>
);
SelectDarkTheme.storyName = 'Select dark theme (takeSnapshot)';
SelectDarkTheme.parameters = {
  zeplinLink: [zeplinLinks[1]],
};
