/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */
import * as React from 'react';
import { TimeRange } from 'pondjs';
import { fireEvent, render, screen } from '@testing-library/react';
import { dateUtils } from '@opengeoweb/shared';
import ResetTimeRangeButton from './ResetTimeRangeButton';
import { defaultTimeRange } from '../../utils/defaultTimeRange';

describe('src/components/TimeSeries/ResetTimeRangeButton', () => {
  it('should be hidden when current timerange is default timerange', async () => {
    const props = {
      timeRange: defaultTimeRange,
      onResetTimeRange: jest.fn(),
    };
    render(<ResetTimeRangeButton {...props} />);

    expect(screen.queryByTestId('reset-timerange-button')).toBeFalsy();
  });

  it('should reset the timerange to default when clicked', async () => {
    const dummyTimeRange = new TimeRange(
      dateUtils.sub(dateUtils.utc(), { days: 5 }),
      dateUtils.sub(dateUtils.utc(), { days: 2 }),
    );

    const props = {
      timeRange: dummyTimeRange,
      onResetTimeRange: jest.fn(),
    };
    render(<ResetTimeRangeButton {...props} />);

    const resetButton = screen.getByTestId('reset-timerange-button');
    fireEvent.click(resetButton);
    expect(props.onResetTimeRange).toHaveBeenCalledTimes(1);
  });
  it('should show tooltip when hovering over button', async () => {
    const dummyTimeRange = new TimeRange(
      dateUtils.sub(dateUtils.utc(), { days: 5 }),
      dateUtils.sub(dateUtils.utc(), { days: 2 }),
    );

    const props = {
      timeRange: dummyTimeRange,
      onResetTimeRange: jest.fn(),
    };
    render(<ResetTimeRangeButton {...props} />);

    fireEvent.mouseOver(screen.getByTestId('reset-timerange-button'));
    // Wait until tooltip appears
    expect(await screen.findByText('Reset timeline')).toBeTruthy();
  });
});
