/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */
import * as React from 'react';
import { dateUtils } from '@opengeoweb/shared';

export const TimeTrackerContext = React.createContext<TimeTrackerContextState>(
  null!,
);

interface TimeTrackerContextState {
  tracker: Date;
  trackerX: number;
  onChangeTracker: (date: Date, scale: () => void) => void;
}

interface TimeTrackerProviderProps {
  children: React.ReactNode;
}

export const TimeTrackerProvider: React.FC<TimeTrackerProviderProps> = ({
  children,
}: TimeTrackerProviderProps) => {
  const [tracker, setTracker] = React.useState(dateUtils.utc());
  const [trackerX, setTrackerX] = React.useState<void | number>(null!);

  const onChangeTracker = React.useCallback(
    (time: Date, scale: (_time: Date) => void): void => {
      const newTracker = time || dateUtils.utc();
      setTracker(newTracker);
      setTrackerX(time && scale(time));
    },
    [],
  );

  const contextValue = React.useMemo(
    () => ({ tracker, trackerX, onChangeTracker } as TimeTrackerContextState),
    [tracker, trackerX, onChangeTracker],
  );

  return (
    <TimeTrackerContext.Provider value={contextValue}>
      {children}
    </TimeTrackerContext.Provider>
  );
};

export const useTimeTrackerContext = (): Partial<TimeTrackerContextState> => {
  const context = React.useContext(TimeTrackerContext);
  return context;
};

export default TimeTrackerContext;
