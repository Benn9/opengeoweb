/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */
import * as React from 'react';
import { fireEvent, render, waitFor, screen } from '@testing-library/react';
import { createFakeApiInstance } from '@opengeoweb/api';
import TimeSeries, { config } from './TimeSeries';
import { TestWrapper } from '../../utils/testUtils';
import {
  createApi as createFakeApi,
  getDummyTimeSerie,
} from '../../utils/fakeApi';
import { SWErrors, TimeseriesResponseData } from '../../types';
import { SpaceWeatherApi } from '../../utils/api';

describe('src/components/TimeSeries/TimeSeries', () => {
  beforeAll(() => jest.spyOn(console, 'warn').mockImplementation());

  const fakeAxiosInstance = createFakeApiInstance();
  // Need to set the offsetWidth for Resizable to render its children
  Object.defineProperty(HTMLElement.prototype, 'offsetWidth', {
    configurable: true,
    value: 500,
  });

  it('should load graphs', async () => {
    const fakeApi = (): SpaceWeatherApi => ({
      ...createFakeApi(),
      getTimeSeriesMultiple: (
        params,
      ): Promise<{ data: TimeseriesResponseData }[]> => {
        return Promise.all(
          params.map((param) => {
            return fakeAxiosInstance.get('/timeseries/data').then(() => ({
              data: getDummyTimeSerie(param.stream!, param.parameter!),
            }));
          }),
        );
      },
    });

    render(
      <TestWrapper createApi={fakeApi}>
        <TimeSeries />
      </TestWrapper>,
    );

    // each graph should show loading state
    await waitFor(() => {
      config.forEach((graph) => {
        expect(screen.getByText(`Loading ${graph.title}`)).toBeTruthy();
      });
    });
    // wait for all graphs to be loaded
    await waitFor(() => {
      expect(screen.getAllByTestId('graph-container').length).toEqual(
        config.length,
      );
    });
  });

  it('should show the ResetTimeRangeButton only on hover', async () => {
    render(
      <TestWrapper>
        <TimeSeries />
      </TestWrapper>,
    );

    // wait for all graphs to be loaded
    await waitFor(() => {
      expect(screen.getAllByTestId('graph-container').length).toEqual(
        config.length,
      );
    });

    expect(screen.queryByTestId('reset-timerange-button-div')).toBeFalsy();
    fireEvent.mouseEnter(screen.getAllByTestId('graph-container')[0]);
    expect(screen.getByTestId('reset-timerange-button-div')).toBeTruthy();
    fireEvent.mouseLeave(screen.getAllByTestId('graph-container')[0]);
    expect(screen.queryByTestId('reset-timerange-button-div')).toBeFalsy();
  });

  it('should show an error message for each graph when loading fails', async () => {
    const fakeApi = (): SpaceWeatherApi => ({
      ...createFakeApi(),
      getTimeSeriesMultiple: (): Promise<{ data: TimeseriesResponseData }[]> =>
        Promise.reject(new Error('test error message')),
    });

    render(
      <TestWrapper createApi={fakeApi}>
        <TimeSeries />
      </TestWrapper>,
    );

    await waitFor(() => {
      expect(screen.getAllByRole('alert').length).toEqual(config.length);
      screen.getAllByRole('alert').forEach((alert) => {
        expect(alert.textContent).toEqual('test error message');
      });
    });
  });

  it('should load the graphs but show the small error overlay if one of multiple series in one graph fails', async () => {
    const fakeApi = (): SpaceWeatherApi => ({
      ...createFakeApi(),
      getTimeSeriesMultiple: (
        params,
      ): Promise<{ data: TimeseriesResponseData }[]> => {
        return Promise.all(
          params.map((param) => {
            if (param.stream === 'kp') {
              return Promise.reject(
                new Error('Network test error message'),
              ).catch((error) => {
                return error;
              });
            }
            return fakeAxiosInstance.get('/timeseries/data').then(() => ({
              data: getDummyTimeSerie(param.stream!, param.parameter!),
            }));
          }),
        );
      },
    });

    render(
      <TestWrapper createApi={fakeApi}>
        <TimeSeries />
      </TestWrapper>,
    );

    await waitFor(() => {
      // expect all graphs to have loaded
      expect(screen.getAllByTestId('graph-container').length).toEqual(
        config.length,
      );
    });
    // But the error to be visible
    expect(screen.getByTestId('temp-error-inner-graph')).toBeTruthy();
    expect(screen.getByRole('alert')).toBeTruthy();
    expect(screen.getByRole('alert').textContent).toEqual(
      'Network test error message',
    );
  });
  it('should not show an error when a request was cancelled', async () => {
    const fakeApi = (): SpaceWeatherApi => ({
      ...createFakeApi(),
      getTimeSeriesMultiple: (
        params,
      ): Promise<{ data: TimeseriesResponseData }[]> => {
        return Promise.all(
          params.map((param) => {
            if (param.stream === 'xray') {
              return Promise.reject(new Error(SWErrors.USER_CANCELLED));
            }
            return fakeAxiosInstance.get('/timeseries/data').then(() => ({
              data: getDummyTimeSerie(param.stream!, param.parameter!),
            }));
          }),
        );
      },
    });

    render(
      <TestWrapper createApi={fakeApi}>
        <TimeSeries />
      </TestWrapper>,
    );

    await waitFor(() => {
      // expect all graphs to have loaded minus one
      expect(screen.getAllByTestId('graph-container').length).toEqual(
        config.length - 1,
      );
    });
    // and no error to be visible
    expect(screen.queryByTestId('temp-error-inner-graph')).toBeFalsy();
    expect(screen.queryByRole('alert')).toBeFalsy();
  });
});
