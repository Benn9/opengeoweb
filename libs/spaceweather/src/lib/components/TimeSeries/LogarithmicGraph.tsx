/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */
import * as React from 'react';
import {
  ChartRow,
  Charts,
  Baseline,
  YAxis,
  LineChart,
} from 'react-timeseries-charts';
import { TimeSeries } from 'pondjs';
import {
  titleStyle,
  titleBoxStyle,
  yAxisStyle,
  baselineStyle,
  thresholdStyle,
  style,
  yAxisStyleWithoutValues,
} from './styles';
import { GraphItem } from './types';
import { getGraphHeightInPx } from './TimeSeries.utils';

const graphHeight = getGraphHeightInPx();

const LogarithmicGraph: React.FC<GraphItem> = ({
  id,
  title,
  yMinValue,
  yMaxValue,
  columns,
  series,
  threshold = [],
  tickCount,
  tickValues,
}: GraphItem) => {
  return (
    <ChartRow
      title={title}
      titleStyle={titleStyle}
      titleBoxStyle={titleBoxStyle}
      height={graphHeight}
      key={id}
    >
      <YAxis // Axis to display custom ticks and values
        id="values"
        min={1}
        max={tickCount}
        tickCount={tickCount}
        format={tickValues}
        type="linear"
        width={39}
        style={yAxisStyle}
        hideAxisLine
      />
      <YAxis // Axis to plot the timeseries in logarithmic scale
        id="y"
        min={yMinValue}
        max={yMaxValue}
        format=",.0f"
        type="log"
        width={1}
        style={yAxisStyleWithoutValues}
      />
      {series.map((serie: typeof TimeSeries, index: number) => (
        // eslint-disable-next-line react/no-array-index-key
        <Charts key={`-${id}-charts-${index}`}>
          <LineChart
            axis="y"
            // eslint-disable-next-line react/no-array-index-key
            key={`line-${index}`}
            breakLine
            series={serie}
            columns={columns}
            style={style}
            interpolation="curveLinear"
          />
          <Baseline
            axis="y"
            key="baseline"
            style={baselineStyle}
            value={yMinValue}
            position="right"
          />
          {threshold &&
            threshold.map((line) => {
              return (
                <Baseline
                  axis="y"
                  key={`threshold-${line}`}
                  style={thresholdStyle}
                  value={line.value}
                  label={line.title}
                  position="right"
                />
              );
            })}
        </Charts>
      ))}
    </ChartRow>
  );
};

export default LogarithmicGraph;
