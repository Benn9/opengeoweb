/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */
import * as React from 'react';
import { Grid } from '@mui/material';
import { ThemeWrapperOldTheme } from '@opengeoweb/theme';
import Notifications from './Notifications';
import { NotificationTriggerProvider } from '../NotificationTrigger';
import { StoryWrapper } from '../../utils/storybookUtils';
import NotificationRow from './NotificationRow';
import { SWEvent } from '../../types';

export default { title: 'components/Notification Section' };

const fakeEvent = {
  eventid: 'METRB1',
  category: 'ELECTRON_FLUX',
  categorydetail: 'ELECTRON_FLUX_2',
  originator: 'METOffice',
  lifecycles: {
    externalprovider: {
      eventid: 'METRB1',
      owner: 'METOffice',
      label: 'ALERT',
      eventstart: '2020-07-13 13:00',
      eventend: '2020-07-13 16:12',
      firstissuetime: '2020-07-13 12:00',
      lastissuetime: '2020-07-13 12:00',
      state: 'ended',
      canbe: [],
    },
    internalprovider: {
      eventid: 'METRB1',
      owner: 'KNMI',
      label: 'ALERT',
      eventstart: '2020-07-13 13:00',
      eventend: '2020-07-13 16:12',
      firstissuetime: '2020-07-13 12:10',
      lastissuetime: '2020-07-13 12:10',
      state: 'ended',
      canbe: [],
    },
  },
};

const fakeCancelledEvent = {
  eventid: 'METRB2',
  category: 'XRAY_RADIO_BLACKOUT',
  categorydetail: '',
  originator: 'METOffice',
  notacknowledged: false,
  lifecycles: {
    externalprovider: {
      eventid: 'METRB2',
      owner: 'METOffice',
      label: 'WARNING',
      eventstart: '2020-07-13 13:00',
      eventend: '2020-07-13 16:12',
      firstissuetime: '2020-07-13 12:00',
      lastissuetime: '2020-07-13 12:00',
      state: 'ended',
      canbe: [],
    },
    internalprovider: {
      eventid: 'METRB2',
      owner: 'KNMI',
      label: 'WARNING',
      eventstart: '2020-07-13 13:00',
      eventend: '',
      firstissuetime: '2020-07-13 12:10',
      lastissuetime: '2020-07-13 12:10',
      state: 'issued',
      canbe: [],
    },
  },
};

const fakeEventWithBell = {
  eventid: 'METRB3',
  category: 'XRAY_RADIO_BLACKOUT',
  categorydetail: '',
  originator: 'METOffice',
  notacknowledged: true,
  lifecycles: {
    externalprovider: {
      eventid: 'METRB3',
      owner: 'METOffice',
      label: 'ALERT',
      eventstart: '2020-07-13 13:00',
      eventend: '',
      firstissuetime: '2020-07-13 12:00',
      lastissuetime: '2020-07-13 12:00',
      state: 'issued',
      canbe: [],
    },
  },
};

const fakeEventWithDraft = {
  eventid: 'METRB4',
  category: 'XRAY_RADIO_BLACKOUT',
  categorydetail: '',
  originator: 'METOffice',
  lifecycles: {
    externalprovider: {
      eventid: 'METRB4',
      owner: 'METOffice',
      label: 'ALERT',
      eventstart: '2020-07-13 13:00',
      eventend: '',
      firstissuetime: '2020-07-13 12:00',
      lastissuetime: '2020-07-13 12:00',
      state: 'issued',
      canbe: [],
    },
    internalprovider: {
      draft: true,
      eventid: 'METRB4',
      owner: 'KNMI',
      label: 'ALERT',
      eventstart: '2020-07-13 13:00',
      eventend: '',
      firstissuetime: '2020-07-13 12:10',
      lastissuetime: '2020-07-13 12:10',
      state: 'draft',
      canbe: [],
    },
  },
};

const fakeExpiredEvent = {
  eventid: 'METRBWARNPRO1084847',
  category: 'PROTON_FLUX',
  categorydetail: 'PROTON_FLUX_10',
  originator: 'METOffice',
  lifecycles: {
    externalprovider: {
      eventid: 'METRBWARNPRO1084847',
      label: 'WARNING',
      owner: 'METOffice',
      firstissuetime: '2020-07-13T11:52:00Z',
      lastissuetime: '2020-07-13T11:52:00Z',
      eventstart: '2020-07-13T05:30:00Z',
      eventend: '2020-07-13T15:00:00Z',
      eventlevel: 'S2',
      state: 'expired',
      canbe: [],
      notifications: [
        {
          eventid: 'METRBWARNPRO1084847',
          category: 'PROTON_FLUX',
          categorydetail: 'PROTON_FLUX_10',
          label: 'WARNING',
          changestateto: 'issued',
          neweventlevel: 'S2',
          neweventstart: '2020-07-13T05:30:00Z',
          neweventend: '2020-07-13T15:00:00Z',
          threshold: 100,
          thresholdunit: 'particles cm^-2 s^-1 sr^-1',
          datasource: 'GOES13',
          message:
            'Biological: No additional risk.\nSatellite operations: Infrequent single-event upsets possible.',
          notificationid: 'METRB1MET1_345830948509345',
          issuetime: '2020-07-13T11:52:00Z',
        },
      ],
    },
  },
};

const fakeKNMIEvent = {
  eventid: 'KNMI002',
  category: 'GEOMAGNETIC',
  categorydetail: 'KP_INDEX',
  originator: 'KNMI',
  notacknowledged: false,
  lifecycles: {
    internalprovider: {
      eventid: 'KNMI002',
      label: 'WARNING',
      owner: 'KNMI',
      eventstart: '2022-01-01T11:20:00Z',
      eventend: '2022-01-01T12:55:00Z',
      eventlevel: 'G3',
      firstissuetime: '2022-01-01T11:13:00Z',
      lastissuetime: '2022-01-01T11:13:00Z',
      state: 'issued',
      canbe: ['updated', 'extended', 'cancelled'],
      notifications: [
        {
          eventid: 'KNMI002',
          category: 'GEOMAGNETIC',
          categorydetail: 'KP_INDEX',
          neweventlevel: 'G3',
          threshold: 7,
          label: 'WARNING',
          changestateto: 'issued',
          neweventstart: '2022-01-01T11:20:00Z',
          neweventend: '2022-01-01T12:55:00Z',
          title: 'Geomagnetic Kp index waarschuwing voor 14-07-2020 11:20UTC',
          message:
            'This warning supersedes any current Geomagnetic Storm Watch.',
          notificationid: 'METRB2MET_8373489',
          datasource: 'DSCVR / BGS',
          issuetime: '2022-01-01T11:13:00Z',
        },
      ],
    },
  },
};

export const NotificationRowDummyDemo = (): React.ReactElement => {
  return (
    <ThemeWrapperOldTheme>
      <NotificationRow
        event={fakeEvent}
        onNotificationRowClick={(): void => {
          // eslint-disable-next-line no-console
          console.log('hello');
        }}
      />
      <NotificationRow
        event={fakeCancelledEvent}
        onNotificationRowClick={(): void => {
          // eslint-disable-next-line no-console
          console.log('hello cancel');
        }}
      />
      <NotificationRow
        event={fakeEventWithBell}
        onNotificationRowClick={(): void => {
          // eslint-disable-next-line no-console
          console.log('hello bell');
        }}
      />
      <NotificationRow
        event={fakeEventWithDraft}
        onNotificationRowClick={(): void => {
          // eslint-disable-next-line no-console
          console.log('hello draft');
        }}
      />
    </ThemeWrapperOldTheme>
  );
};

const NotificationsDemoDummyActionsComponent = (): React.ReactElement => {
  return (
    <Grid container>
      <Grid item xs={6}>
        <NotificationTriggerProvider>
          <Notifications />
        </NotificationTriggerProvider>
      </Grid>
    </Grid>
  );
};

export const NotificationsDemoDummyActions = (): React.ReactElement => {
  return (
    <StoryWrapper>
      <NotificationsDemoDummyActionsComponent />
    </StoryWrapper>
  );
};

export const NotificationsListSnapshot = (): React.ReactElement => {
  const fakeEventList: SWEvent[] = [
    fakeKNMIEvent,
    fakeEvent,
    fakeEventWithBell,
    fakeEventWithDraft,
    fakeCancelledEvent,
    fakeExpiredEvent,
  ];

  return (
    <div style={{ width: 800 }}>
      <StoryWrapper
        createApiFunc={(): {
          getEventList: () => Promise<{ data: SWEvent[] }>;
        } => {
          return {
            getEventList: (): Promise<{ data: SWEvent[] }> => {
              return Promise.resolve({ data: fakeEventList });
            },
          };
        }}
      >
        <Notifications />
      </StoryWrapper>
    </div>
  );
};

NotificationsListSnapshot.storyName = 'Notifications List (takeSnapshot)';
