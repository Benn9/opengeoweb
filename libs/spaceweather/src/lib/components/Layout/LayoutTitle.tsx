/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */
import * as React from 'react';
import { Grid, Typography } from '@mui/material';

/**
 * Layout Title
 * Shows Space Weather title
 *
 * @example
 * ``` <LayoutTitle /> ```
 */
const LayoutTitle: React.FC = () => {
  return (
    <Grid
      item
      sx={{
        boxShadow: 1,
      }}
    >
      <Typography
        sx={{
          textAlign: 'center',
          padding: '10px',
        }}
        variant="subtitle1"
      >
        Space Weather Forecast
      </Typography>
    </Grid>
  );
};
export default LayoutTitle;
