/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */
import * as React from 'react';
import { render, screen } from '@testing-library/react';
import LifeCycleColumn, {
  formatDateWithUTC,
  LifeCycleColumnProps,
} from './LifeCycleColumn';
import {
  fakeEventList,
  mockEventAcknowledgedExternal,
  mockEventAcknowledgedExternalDraft,
  mockEventUnacknowledgedExternal,
  mockEventWarning,
} from '../../utils/fakedata';
import { EventCategoryDetail } from '../../types';

describe('src/components/LifeCycleDialog/LifeCycleColumn', () => {
  describe('formatDateWithUTC', () => {
    it('should format date and append UTCs', () => {
      expect(formatDateWithUTC('2020-07-12T04:28:00Z')).toEqual(
        '2020-07-12 04:28 UTC',
      );
      expect(formatDateWithUTC(undefined!)).toBeNull();
      expect(formatDateWithUTC(null!)).toBeNull();
      expect(formatDateWithUTC('')).toBeNull();
    });
  });

  it('should display all notifications in the lifecyle except for the drafts', () => {
    const props: LifeCycleColumnProps = {
      lifeCycle:
        mockEventAcknowledgedExternalDraft.lifecycles!.internalprovider!,
      type: 'internal',
      categoryDetail: mockEventAcknowledgedExternalDraft.categorydetail,
    };

    render(<LifeCycleColumn {...props} />);

    const notifications =
      mockEventAcknowledgedExternalDraft.lifecycles!.internalprovider!
        .notifications!;

    expect(
      screen.getAllByTestId('lifecycle-column-notification').length,
    ).toEqual(2);
    expect(
      screen.getByTestId(
        `${notifications[0].notificationid}-${notifications[0].issuetime}`,
      ),
    ).toBeTruthy();
    expect(
      screen.getByTestId(
        `${notifications[1].notificationid}-${notifications[1].issuetime}`,
      ),
    ).toBeTruthy();
    expect(
      screen.queryByTestId(
        `${notifications[2].notificationid}-${notifications[2].issuetime}`,
      ),
    ).toBeFalsy();
  });
  it('should display the notification message with headers for UKMO', () => {
    const props = {
      lifeCycle: mockEventUnacknowledgedExternal.lifecycles!.externalprovider!,
      type: 'external',
      categoryDetail: mockEventUnacknowledgedExternal.categorydetail,
    };

    const notifications =
      mockEventUnacknowledgedExternal.lifecycles!.externalprovider!
        .notifications!;

    render(<LifeCycleColumn {...props} />);

    expect(
      screen.getByTestId('lifecycle-column-message-impact').textContent,
    ).toEqual(notifications[0].message);
  });
  it('should display an external notification message as plain text even if it contains html', () => {
    const testMessage = '<img src="test">';
    const lifeCycle = {
      ...mockEventUnacknowledgedExternal.lifecycles!.externalprovider!,
      notifications: [
        {
          ...mockEventUnacknowledgedExternal.lifecycles!.externalprovider!
            .notifications![0],
          message: testMessage,
        },
      ],
    };
    const props: LifeCycleColumnProps = {
      lifeCycle,
      type: 'external',
      categoryDetail: mockEventUnacknowledgedExternal.categorydetail,
    };

    render(<LifeCycleColumn {...props} />);

    expect(
      screen.getByTestId('lifecycle-column-message-impact').textContent,
    ).toEqual(testMessage);
  });
  it('should display the notification message correctly for KNMI', () => {
    const props = {
      lifeCycle: mockEventAcknowledgedExternal.lifecycles!.internalprovider!,
      type: 'internal',
      categoryDetail: mockEventAcknowledgedExternal.categorydetail,
    };

    const notifications =
      mockEventAcknowledgedExternal.lifecycles!.internalprovider!
        .notifications!;

    render(<LifeCycleColumn {...props} />);

    expect(
      screen.getAllByTestId('lifecycle-column-message')[0].textContent,
    ).toEqual(notifications[0].message);
  });
  it('should display an internal notification message as plain text even if it contains html', () => {
    const testMessage = '<img src="test">';
    const lifeCycle = {
      ...mockEventUnacknowledgedExternal.lifecycles!.externalprovider!,
      notifications: [
        {
          ...mockEventUnacknowledgedExternal.lifecycles!.externalprovider!
            .notifications![0],
          message: testMessage,
        },
      ],
    };
    const props: LifeCycleColumnProps = {
      lifeCycle,
      type: 'internal',
      categoryDetail: mockEventUnacknowledgedExternal.categorydetail,
    };

    render(<LifeCycleColumn {...props} />);

    expect(screen.getByTestId('lifecycle-column-message').textContent).toEqual(
      testMessage,
    );
  });
  it('should display the event end date if passed', () => {
    const props = {
      lifeCycle: mockEventWarning.lifecycles!.internalprovider!,
      type: 'internal',
      categoryDetail: mockEventWarning.categorydetail,
    };

    render(<LifeCycleColumn {...props} />);

    // If passed - eventend should be shown
    expect(screen.getByTestId('lifecycle-column-eventend')).toBeTruthy();
  });
  it('should not display the event end date if not passed', () => {
    // if not passed - eventend should be hidden
    const props = {
      lifeCycle: mockEventUnacknowledgedExternal.lifecycles!.externalprovider!,
      type: 'external',
      categoryDetail: mockEventUnacknowledgedExternal.categorydetail,
    };

    render(<LifeCycleColumn {...props} />);
    expect(screen.queryByTestId('lifecycle-column-eventend')).toBeFalsy();
  });
  it('should display the radio blackout fields if passed', () => {
    const props = {
      lifeCycle: fakeEventList[3].lifecycles!.externalprovider!,
      type: 'external',
      categoryDetail: fakeEventList[3].categorydetail,
    };

    render(<LifeCycleColumn {...props} />);
    expect(
      screen.queryAllByTestId('lifecycle-column-categorydetail').length,
    ).toBe(0);
    expect(screen.getAllByTestId('lifecycle-column-eventend')).toBeTruthy();
    expect(
      screen.getAllByTestId('lifecycle-column-neweventlevel'),
    ).toBeTruthy();
    expect(screen.getAllByTestId('lifecycle-column-datasource')).toBeTruthy();
    expect(screen.getAllByTestId('lifecycle-column-threshold')).toBeTruthy();
    expect(
      screen.getAllByTestId('lifecycle-column-thresholdunit'),
    ).toBeTruthy();
    expect(screen.getAllByTestId('lifecycle-column-xrayclass')).toBeTruthy();
    expect(screen.getAllByTestId('lifecycle-column-peakclass')).toBeTruthy();
    expect(screen.getAllByTestId('lifecycle-column-peakflux')).toBeTruthy();
    expect(screen.getAllByTestId('lifecycle-column-peakfluxtime')).toBeTruthy();
  });
  it('should not display the radio blackout fields if not passed', () => {
    const props = {
      lifeCycle: fakeEventList[3].lifecycles!.externalprovider!,
      type: 'external',
      categoryDetail: fakeEventList[3].categorydetail,
    };
    const notifications = props.lifeCycle.notifications!;
    notifications[0].neweventend = '';
    notifications[0].neweventlevel = '';
    notifications[0].datasource = '';
    notifications[0].threshold = undefined;
    notifications[0].thresholdunit = '';
    notifications[0].xrayclass = '';
    notifications[0].peakclass = '';
    notifications[0].peakflux = undefined;
    notifications[0].peakfluxtime = '';
    notifications[1].neweventend = '';
    notifications[1].neweventlevel = '';
    notifications[1].datasource = '';
    notifications[1].threshold = undefined;
    notifications[1].thresholdunit = '';
    notifications[1].xrayclass = '';
    notifications[1].peakclass = '';
    notifications[1].peakflux = undefined;
    notifications[1].peakfluxtime = '';

    render(<LifeCycleColumn {...props} />);
    expect(screen.queryByTestId('lifecycle-column-categorydetail')).toBeFalsy();
    expect(screen.queryByTestId('lifecycle-column-eventend')).toBeFalsy();
    expect(screen.queryByTestId('lifecycle-column-neweventlevel')).toBeFalsy();
    expect(screen.queryByTestId('lifecycle-column-datasource')).toBeFalsy();
    expect(screen.queryByTestId('lifecycle-column-threshold')).toBeFalsy();
    expect(screen.queryByTestId('lifecycle-column-thresholdunit')).toBeFalsy();
    expect(screen.queryByTestId('lifecycle-column-xrayclass')).toBeFalsy();
    expect(screen.queryByTestId('lifecycle-column-peakclass')).toBeFalsy();
    expect(screen.queryByTestId('lifecycle-column-peakflux')).toBeFalsy();
    expect(screen.queryByTestId('lifecycle-column-peakfluxtime')).toBeFalsy();
  });
  it('should display the geomagnetic sudden impulse alert fields if passed', () => {
    const props = {
      lifeCycle: fakeEventList[1].lifecycles!.externalprovider!,
      type: 'external',
      categoryDetail: fakeEventList[1].categorydetail,
    };

    render(<LifeCycleColumn {...props} />);
    expect(screen.getAllByTestId('lifecycle-column-owner')).toBeTruthy();
    expect(
      screen.getAllByTestId('lifecycle-column-categorydetail'),
    ).toBeTruthy();
    expect(screen.getAllByTestId('lifecycle-column-eventstart')).toBeTruthy();
    expect(screen.queryAllByTestId('lifecycle-column-eventend').length).toBe(0);
    expect(
      screen.queryAllByTestId('lifecycle-column-neweventlevel').length,
    ).toBe(0);
    expect(screen.getAllByTestId('lifecycle-column-datasource')).toBeTruthy();
    expect(screen.queryAllByTestId('lifecycle-column-threshold').length).toBe(
      0,
    );
    expect(screen.getAllByTestId('lifecycle-column-impulsetime')).toBeTruthy();
    expect(
      screen.getAllByTestId('lifecycle-column-magnetometerdeflection'),
    ).toBeTruthy();
  });
  it('should display the geomagnetic sudden impulse warning fields if passed', () => {
    const props = {
      lifeCycle: fakeEventList[8].lifecycles!.externalprovider!,
      type: 'external',
      categoryDetail: fakeEventList[8].categorydetail,
    };

    render(<LifeCycleColumn {...props} />);
    expect(screen.getAllByTestId('lifecycle-column-owner')).toBeTruthy();
    expect(
      screen.getAllByTestId('lifecycle-column-categorydetail'),
    ).toBeTruthy();
    expect(screen.getAllByTestId('lifecycle-column-eventstart')).toBeTruthy();
    expect(screen.getAllByTestId('lifecycle-column-eventend')).toBeTruthy();
    expect(
      screen.queryAllByTestId('lifecycle-column-neweventlevel').length,
    ).toBe(0);
    expect(screen.getAllByTestId('lifecycle-column-datasource')).toBeTruthy();
    expect(screen.queryAllByTestId('lifecycle-column-threshold').length).toBe(
      0,
    );
    expect(screen.queryAllByTestId('lifecycle-column-impulsetime').length).toBe(
      0,
    );
    expect(screen.getAllByTestId('lifecycle-column-shocktime')).toBeTruthy();
    expect(
      screen.getAllByTestId('lifecycle-column-observedpolaritybz'),
    ).toBeTruthy();
    expect(
      screen.getAllByTestId('lifecycle-column-observedsolarwind'),
    ).toBeTruthy();
    expect(
      screen.queryAllByTestId('lifecycle-column-magnetometerdeflection').length,
    ).toBe(0);
  });

  it('should display the geomagnetic storm watch fields if passed', () => {
    const props = {
      lifeCycle: fakeEventList[14].lifecycles!.externalprovider!,
      type: 'external',
      categoryDetail: fakeEventList[14].categorydetail,
    };

    render(<LifeCycleColumn {...props} />);
    expect(screen.getAllByTestId('lifecycle-column-owner')).toBeTruthy();
    expect(
      screen.getAllByTestId('lifecycle-column-categorydetail'),
    ).toBeTruthy();
    expect(screen.getAllByTestId('lifecycle-column-eventstart')).toBeTruthy();
    expect(screen.getAllByTestId('lifecycle-column-eventend')).toBeTruthy();
    expect(
      screen.getAllByTestId('lifecycle-column-initialgscale'),
    ).toBeTruthy();
    expect(screen.queryAllByTestId('lifecycle-column-datasource').length).toBe(
      0,
    );
    expect(screen.queryAllByTestId('lifecycle-column-threshold').length).toBe(
      0,
    );
    expect(screen.queryAllByTestId('lifecycle-column-impulsetime').length).toBe(
      0,
    );
    expect(
      screen.queryAllByTestId('lifecycle-column-neweventlevel').length,
    ).toBe(0);
    expect(screen.queryAllByTestId('lifecycle-column-shocktime').length).toBe(
      0,
    );
    expect(
      screen.queryAllByTestId('lifecycle-column-observedpolaritybz').length,
    ).toBe(0);
    expect(
      screen.queryAllByTestId('lifecycle-column-observedsolarwind').length,
    ).toBe(0);
    expect(
      screen.queryAllByTestId('lifecycle-column-magnetometerdeflection').length,
    ).toBe(0);
  });

  it('should display the geomagnetic kp index alert fields if passed', () => {
    const props = {
      lifeCycle: fakeEventList[11].lifecycles!.externalprovider!,
      type: 'external',
      categoryDetail: fakeEventList[11].categorydetail,
    };

    render(<LifeCycleColumn {...props} />);
    expect(screen.getAllByTestId('lifecycle-column-owner')).toBeTruthy();
    expect(
      screen.getAllByTestId('lifecycle-column-categorydetail'),
    ).toBeTruthy();
    expect(screen.getAllByTestId('lifecycle-column-eventstart')).toBeTruthy();
    expect(screen.queryAllByTestId('lifecycle-column-eventend').length).toBe(0);
    expect(
      screen.queryAllByTestId('lifecycle-column-initialgscale').length,
    ).toBe(0);
    expect(screen.getAllByTestId('lifecycle-column-datasource')).toBeTruthy();
    expect(screen.getAllByTestId('lifecycle-column-threshold')).toBeTruthy();
    expect(screen.queryAllByTestId('lifecycle-column-impulsetime').length).toBe(
      0,
    );
    expect(
      screen.getAllByTestId('lifecycle-column-neweventlevel'),
    ).toBeTruthy();
    expect(screen.queryAllByTestId('lifecycle-column-shocktime').length).toBe(
      0,
    );
    expect(
      screen.queryAllByTestId('lifecycle-column-observedpolaritybz').length,
    ).toBe(0);
    expect(
      screen.queryAllByTestId('lifecycle-column-observedsolarwind').length,
    ).toBe(0);
    expect(
      screen.queryAllByTestId('lifecycle-column-magnetometerdeflection').length,
    ).toBe(0);
  });

  it('should correctly render dates', async () => {
    const lifeCycle = fakeEventList[0].lifecycles!.externalprovider!;
    const props = {
      lifeCycle,
      type: 'internal',
      categoryDetail: fakeEventList[0].categorydetail,
    };

    render(<LifeCycleColumn {...props} />);

    screen
      .queryAllByTestId('lifecycle-issuetime')
      .forEach((element, index) =>
        expect(element.innerHTML).toEqual(
          formatDateWithUTC(lifeCycle.notifications![index].issuetime!),
        ),
      );
    expect(
      screen.queryAllByTestId('lifecycle-column-peakfluxtime')[0].innerHTML,
    ).toEqual(formatDateWithUTC(lifeCycle.notifications![1].peakfluxtime!));
  });

  it('should not display categorydetail if not passed', () => {
    const props = {
      lifeCycle: mockEventUnacknowledgedExternal.lifecycles!.externalprovider!,
      type: 'external',
      categoryDetail: null as unknown as EventCategoryDetail,
    };

    render(<LifeCycleColumn {...props} />);
    expect(screen.queryByTestId('lifecycle-column-categorydetail')).toBeFalsy();
    expect(screen.queryByText('Category detail')).toBeFalsy();
  });
});
