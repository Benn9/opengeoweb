/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */
import * as React from 'react';
import { render, fireEvent, screen } from '@testing-library/react';
import LifeCycleActions from './LifeCycleActions';

describe('src/components/LifeCycleDialog/LifeCycleActions', () => {
  it('should display the correct buttons based on the passed actions and call the appropriate functions', () => {
    const props = {
      actions: ['updated', 'extended', 'summarised'],
      onAction: jest.fn(),
    };

    render(<LifeCycleActions {...props} />);

    expect(screen.getByTestId('updateextend')).toBeTruthy();
    expect(screen.getByTestId('summarise')).toBeTruthy();
    expect(screen.queryByTestId('cancel')).toBeFalsy();

    fireEvent.click(screen.getByTestId('summarise'));
    expect(props.onAction).toHaveBeenCalledWith('Summarise');
    fireEvent.click(screen.getByTestId('updateextend'));
    expect(props.onAction).toHaveBeenCalledWith('Updateextend');
  });

  it('should display the correct buttons based on the passed actions and call the appropriate functions part II ', () => {
    const props = {
      actions: ['updated', 'cancelled'],
      onAction: jest.fn(),
    };

    render(<LifeCycleActions {...props} />);

    expect(screen.getByTestId('updateextend')).toBeTruthy();
    expect(screen.getByTestId('cancel')).toBeTruthy();
    expect(screen.queryByTestId('summarise')).toBeFalsy();

    fireEvent.click(screen.getByTestId('cancel'));
    expect(props.onAction).toHaveBeenCalledWith('Cancel');
    fireEvent.click(screen.getByTestId('updateextend'));
    expect(props.onAction).toHaveBeenCalledWith('Updateextend');
  });
});
