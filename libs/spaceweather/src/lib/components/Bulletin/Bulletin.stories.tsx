/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */
import * as React from 'react';
import { createFakeApiInstance } from '@opengeoweb/api';
import { Bulletin } from '.';
import BulletinHistoryDialog from './BulletinHistoryDialog';
import { StoryWrapper } from '../../utils/storybookUtils';
import { Bulletin as BulletinType } from '../../types';
import { fakeBulletins } from '../../utils/fakedata';

export default { title: 'components/Bulletin Section' };

const fakeAxiosInstance = createFakeApiInstance();

export const BulletinHistoryStory = (): React.ReactElement => (
  <StoryWrapper>
    <BulletinHistoryDialog
      open
      toggleStatus={(): void => {
        /* Do nothing */
      }}
    />
  </StoryWrapper>
);

export const BulletinStory = (): React.ReactElement => (
  <StoryWrapper>
    <div style={{ height: '500px', width: '800px', display: 'flex' }}>
      <Bulletin />
    </div>
  </StoryWrapper>
);

export const BulletinStoryNoIncomingLast24h = (): React.ReactElement => (
  <StoryWrapper
    createApiFunc={(): {
      getBulletin: () => Promise<{ data: BulletinType }>;
      getBulletinHistory: () => Promise<{ data: BulletinType[] }>;
    } => {
      return {
        getBulletin: (): Promise<{ data: BulletinType }> =>
          fakeAxiosInstance.get('/fakeBulletin').then(() => {
            return {
              data: {} as BulletinType,
            };
          }),
        getBulletinHistory: (): Promise<{ data: BulletinType[] }> =>
          fakeAxiosInstance.get('/fakeBulletinHistory').then(() => ({
            data: fakeBulletins,
          })),
      };
    }}
  >
    <div style={{ height: '500px', width: '800px', display: 'flex' }}>
      <Bulletin />
    </div>
  </StoryWrapper>
);

BulletinHistoryStory.storyName = 'Bulletin History (takeSnapshot)';
BulletinStory.storyName = 'Bulletin Section (takeSnapshot)';
BulletinStoryNoIncomingLast24h.storyName =
  'Bulletin Section No Incoming Bulletins (takeSnapshot)';
