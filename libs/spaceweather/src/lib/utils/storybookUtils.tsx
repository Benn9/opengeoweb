/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */
import * as React from 'react';
import { ThemeWrapperOldTheme } from '@opengeoweb/theme';
import { ApiProvider, createFakeApiInstance } from '@opengeoweb/api';
import { ConfirmationServiceProvider } from '@opengeoweb/shared';
import { SWEvent } from '../types';
import {
  createApi as createFakeApi,
  fetchEvent,
  fetchFakeNewNotifications,
  getDummyTimeSerie,
} from './fakeApi';
import { fakeBackendError } from './testUtils';

const fakeAxiosInstance = createFakeApiInstance();

interface WrapperProps {
  children: React.ReactNode;
}

interface ExtWrapperProps extends WrapperProps {
  createApiFunc?: () => void;
}

interface ParamsType {
  parameter: string;
  stream: string;
  time_start: string;
  time_stop: string;
}

export const StoryWrapper: React.FC<ExtWrapperProps> = ({
  children,
  createApiFunc = null!,
}: ExtWrapperProps) => {
  return (
    <ThemeWrapperOldTheme>
      <ApiProvider
        createApi={createApiFunc !== null ? createApiFunc : createFakeApi}
      >
        <ConfirmationServiceProvider>{children}</ConfirmationServiceProvider>
      </ApiProvider>
    </ThemeWrapperOldTheme>
  );
};

export const StoryWrapperWithNetworkError: React.FC<WrapperProps> = ({
  children,
}: WrapperProps) => {
  return (
    <ThemeWrapperOldTheme>
      <ApiProvider
        // eslint-disable-next-line @typescript-eslint/ban-ts-comment
        // @ts-ignore
        createApi={(): { getTimeSeriesMultiple } => {
          return {
            getTimeSeriesMultiple: (): Promise<Record<string, unknown>[]> =>
              Promise.reject(new Error('Network error')),
          };
        }}
      >
        <ConfirmationServiceProvider>{children}</ConfirmationServiceProvider>
      </ApiProvider>
    </ThemeWrapperOldTheme>
  );
};

export const StoryWrapperWithOnlyOneNetworkError: React.FC<WrapperProps> = ({
  children,
}: WrapperProps) => {
  return (
    <ThemeWrapperOldTheme>
      <ApiProvider
        // eslint-disable-next-line @typescript-eslint/ban-ts-comment
        // @ts-ignore
        createApi={(): { getTimeSeriesMultiple } => {
          return {
            getTimeSeriesMultiple: (
              params: ParamsType[],
            ): Promise<Record<string, unknown>[]> => {
              return Promise.all(
                params.map((param) => {
                  if (param.stream === 'kp') {
                    return Promise.reject(new Error('Network error')).catch(
                      (error) => {
                        return error;
                      },
                    );
                  }
                  return fakeAxiosInstance.get('/timeseries/data').then(() => ({
                    data: getDummyTimeSerie(param.stream, param.parameter),
                  }));
                }),
              );
            },
          };
        }}
      >
        <ConfirmationServiceProvider>{children}</ConfirmationServiceProvider>
      </ApiProvider>
    </ThemeWrapperOldTheme>
  );
};

export const StoryWrapperWithErrorOnSave: React.FC<WrapperProps> = ({
  children,
}: WrapperProps) => {
  return (
    <ThemeWrapperOldTheme>
      <ApiProvider
        // eslint-disable-next-line @typescript-eslint/ban-ts-comment
        // @ts-ignore
        createApi={(): // eslint-disable-next-line @typescript-eslint/no-explicit-any
        any => {
          return {
            // dummy calls
            issueNotification: (): Promise<void> => {
              return Promise.reject(fakeBackendError);
            },
            discardDraftNotification: (): Promise<void> => {
              return Promise.reject(fakeBackendError);
            },
            getNewNotifications: (): Promise<{ data: SWEvent[] }> =>
              fakeAxiosInstance
                .get('/notification/newNotifications')
                .then(() => ({
                  data: fetchFakeNewNotifications(),
                })),
            getEvent: (eventid: string): Promise<{ data: SWEvent }> =>
              fakeAxiosInstance.get(`/event/${eventid}`).then(() => ({
                data: fetchEvent(eventid),
              })),
            getTimeSeriesMultiple: (): void => {
              throw new Error('Dummy error');
            },
          };
        }}
      >
        <ConfirmationServiceProvider>{children}</ConfirmationServiceProvider>
      </ApiProvider>
    </ThemeWrapperOldTheme>
  );
};
