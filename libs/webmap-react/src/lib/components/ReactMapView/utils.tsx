/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */

import {
  Dimension,
  LayerFoundation,
  LayerType,
  WMLayer,
  webmapUtils,
} from '@opengeoweb/webmap';
import { differenceInMinutes } from 'date-fns';
import { FeatureLayer } from '../MapDraw';

/**
 * Returns filtered list of props with geoJson
 * @param children React.ReactNode, layers with geoJson
 */
export const getFeatureLayers = (children: React.ReactNode): FeatureLayer[] =>
  children && Array.isArray(children)
    ? children
        // eslint-disable-next-line @typescript-eslint/ban-ts-comment
        // @ts-ignore
        .reduce((acc, val) => acc.concat(val), [])
        .filter(
          (c: { props: { geojson: GeoJSON.FeatureCollection } }) =>
            c && c.props && c.props.geojson,
        )
        .map((c: { props: unknown }) => c.props)
        .reverse()
    : [];
/**
 * Returns true if this is a maplayer and not a baselayer or overlayer
 * @param layer The LayerFoundation object, or the props from the ReactWMJSLayer
 */
export const isAMapLayer = (layer: LayerFoundation): boolean =>
  layer.layerType === LayerType.mapLayer;

/**
 * Returns true if this is a geojsonlayer (layer containing geojson field)
 * @param layer The LayerFoundation object, or the props from the ReactWMJSLayer
 */
export const isAGeoJSONLayer = (layer: LayerFoundation): boolean =>
  layer.geojson !== undefined;

export const getWMJSLayerFromReactLayer = (
  mapId: string,
  wmLayers: WMLayer[],
  reactWebMapJSLayer: React.ReactElement,
  wmLayerIndex: number,
): {
  layer: WMLayer;
  layerArrayMutated: boolean;
} => {
  const { id: reactLayerId, name: layerName } = reactWebMapJSLayer.props;
  const wmjsMap = webmapUtils.getWMJSMapById(mapId);

  /* Some safety checks */
  if (!wmjsMap || !wmLayers || !reactLayerId || !layerName) {
    return { layer: null!, layerArrayMutated: false };
  }

  /* Find the wmlayer by its react layer id */
  const matchingLayerIndex = wmLayers.findIndex(
    (wmLayer) => wmLayer.id === reactLayerId,
  );
  if (matchingLayerIndex === -1) {
    return { layer: null!, layerArrayMutated: false };
  }

  /* 
     The layer order in the react children array index is 
     reversed compared to the layer order in the wmLayer array index 
  */
  const reactLayerIndex = wmLayers.length - 1 - wmLayerIndex;

  const isMapLayer = isAMapLayer(reactWebMapJSLayer.props);
  const mapLayerArrayShouldBeMutated =
    isMapLayer && matchingLayerIndex !== reactLayerIndex;

  /* It seems that a layer was moved in the layerlist, move it in the wmjsMap too */
  if (mapLayerArrayShouldBeMutated) {
    wmjsMap.swapLayers(wmLayers[matchingLayerIndex], wmLayers[reactLayerIndex]);
    wmjsMap.draw();
  }

  return {
    layer: wmLayers[matchingLayerIndex],
    layerArrayMutated: mapLayerArrayShouldBeMutated,
  };
};

export function getIsInsideAcceptanceTime(
  acceptanceTimeInMinutes: number | undefined,
  mapDimensions: Dimension[] | undefined,
  layerDimensions: Dimension[] | undefined,
): boolean {
  const mapCurrentTime = mapDimensions?.find(
    (dimension) => dimension.name === 'time',
  )?.currentValue;

  const layerCurrentTime = layerDimensions?.find(
    (dimension) => dimension.name === 'time',
  )?.currentValue;

  if (
    acceptanceTimeInMinutes === undefined ||
    !mapCurrentTime ||
    !layerCurrentTime
  ) {
    return true;
  }

  const minutesBetween = differenceInMinutes(
    new Date(mapCurrentTime),
    new Date(layerCurrentTime),
  );
  if (Math.abs(minutesBetween) > acceptanceTimeInMinutes) {
    return false;
  }

  return true;
}
