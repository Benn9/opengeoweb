/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import { WMLayer } from '@opengeoweb/webmap';
import * as React from 'react';

interface ReactMapViewLayerProps {
  id: string;
  // eslint-disable-next-line react/no-unused-prop-types
  onLayerReady?: (layer: WMLayer) => void;
}

export const ReactMapViewLayer: React.FC<ReactMapViewLayerProps> = (
  props: ReactMapViewLayerProps,
) => {
  const { id } = props;
  return (
    <div
      style={{
        width: '100%',
        padding: '2px',
        margin: 0,
        wordWrap: 'break-word',
        border: '1px solid rgba(0, 0, 0, 0.5)',
        lineHeight: '14px',
        fontSize: '10px',
        background: 'rgba(255, 255, 255, 0.5)',
        display: 'none',
      }}
    >
      <div>{id}</div>
      <div
        // eslint-disable-next-line react/no-danger
        dangerouslySetInnerHTML={{
          __html: JSON.stringify(props, null, '--').replace(/\n/g, '<br/>'),
        }}
      />
    </div>
  );
};
