/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';

import { produce } from 'immer';
import { webmapUtils } from '@opengeoweb/webmap';
import { ThemeProvider } from '@opengeoweb/theme';
import { FeatureEvent } from './MapDraw';

import { Synops } from './storyComponents/Synops';
import { FeatureInfo } from './storyComponents/FeatureInfo';

import { MapDrawGeoJSON } from './storyComponents/MapDrawGeoJSON';
import { FeatureInfoHTML } from './storyComponents/FeatureInfoHTML';
import { MapDrawDrawFunctionArgs, registerDrawFunction } from './mapDrawUtils';
import { MapView, MapViewLayer } from '../MapView';
import { defaultLayers, publicLayers } from '../../layers';
import {
  simpleMultiPolygon,
  simplePointsGeojson,
  simplePolygonGeoJSON,
} from './storyComponents/geojsonExamples';
import { MapWarningProperties } from '../MapWarning';

export default {
  title: 'components/MapDraw',
};

const srsAndBboxDefault = {
  bbox: {
    left: -2324980.5498391856,
    bottom: 5890854.775012179,
    right: 6393377.702660825,
    top: 11652109.058827976,
  },
  srs: 'EPSG:3857',
};

// --- example 1: Drawing and editing GeoJSON
export const Map1 = (): React.ReactElement => (
  <ThemeProvider>
    <div style={{ height: '100vh' }}>
      <MapDrawGeoJSON />
    </div>
  </ThemeProvider>
);

Map1.storyName = 'Drawing and editing GeoJSON';

// --- example 2: Display GeoJSON Points
export const Map2 = (): React.ReactElement => {
  const [selectedFeature, setSelectedFeature] = React.useState<FeatureEvent>(
    null!,
  );

  const circleDrawFunction = (
    args: MapDrawDrawFunctionArgs,
    fillColor = '#88F',
    radius = 12,
  ): void => {
    const { context: ctx, coord } = args;
    ctx.strokeStyle = '#000';
    ctx.fillStyle = fillColor || '#88F';
    ctx.lineWidth = 1;
    ctx.beginPath();
    ctx.arc(coord.x, coord.y, radius || 10, 0, 2 * Math.PI);
    ctx.fill();
    ctx.stroke();
    ctx.fillStyle = '#000';
    ctx.beginPath();
    ctx.arc(coord.x, coord.y, 2, 0, 2 * Math.PI);
    ctx.fill();
  };
  const circleDrawFunctionId = React.useRef(
    registerDrawFunction(circleDrawFunction),
  );
  const selectedCircleDrawFunctionId = React.useRef(
    registerDrawFunction((args): void => {
      circleDrawFunction(args, '#F00', 12);
    }),
  );

  return (
    <ThemeProvider>
      <div style={{ height: '100vh' }}>
        <MapView
          mapId={React.useRef<string>(webmapUtils.generateMapId()).current}
          srs={srsAndBboxDefault.srs}
          bbox={srsAndBboxDefault.bbox}
        >
          <MapViewLayer {...publicLayers.baseLayer} />
          <MapViewLayer {...defaultLayers.overLayer} />
          <MapViewLayer
            id={webmapUtils.generateLayerId()}
            geojson={produce(simplePointsGeojson, (draft) => {
              /* Set default drawfunction for each feature */
              draft.features.forEach((feature) => {
                // eslint-disable-next-line no-param-reassign
                feature.properties!.drawFunctionId =
                  circleDrawFunctionId.current;
              });

              /* Change drawfunction for selected feature */
              if (selectedFeature) {
                draft.features[
                  selectedFeature.featureIndex
                ].properties!.drawFunctionId =
                  selectedCircleDrawFunctionId.current;
              }
            })}
            onClickFeature={(featureResult: FeatureEvent): void => {
              setSelectedFeature(featureResult);
            }}
          />
        </MapView>
        <div
          style={{
            position: 'absolute',
            left: '50px',
            top: '10px',
            zIndex: 10000,
            backgroundColor: '#CCCCCCC0',
            padding: '20px',
            overflow: 'auto',
            width: '80%',
            fontSize: '11px',
          }}
        >
          {selectedFeature && (
            <pre>{JSON.stringify(selectedFeature, null, 2)}</pre>
          )}
        </div>
      </div>
    </ThemeProvider>
  );
};

Map2.storyName = 'Display Points GeoJSON';

// --- example 3: Synops along buffered route via WFS CQL
export const Map3 = (): React.ReactElement => (
  <ThemeProvider>
    <div style={{ height: '100vh' }}>
      <Synops />
    </div>
  </ThemeProvider>
);

Map3.storyName = 'Synops along buffered route via WFS CQL';

// --- example 4: Custom GetFeatureInfo as JSON
export const Map4 = (): React.ReactElement => (
  <ThemeProvider>
    <div style={{ height: '100vh' }}>
      <FeatureInfo />
    </div>
  </ThemeProvider>
);

Map4.storyName = 'Custom GetFeatureInfo as JSON';

// --- example 5: Custom GetFeatureInfo as HTML
export const Map5 = (): React.ReactElement => (
  <ThemeProvider>
    <div style={{ height: '100vh' }}>
      <FeatureInfoHTML />
    </div>
  </ThemeProvider>
);

Map5.storyName = 'Custom GetFeatureInfo as HTML';

// --- example 6: Display GeoJSON MultiPolygon
export const Map6 = (): React.ReactElement => {
  const [selectedFeature, setSelectedFeature] = React.useState<FeatureEvent>(
    null!,
  );
  return (
    <ThemeProvider>
      <div style={{ height: '100vh' }}>
        <MapView
          mapId={React.useRef<string>(webmapUtils.generateMapId()).current}
          srs={srsAndBboxDefault.srs}
          bbox={srsAndBboxDefault.bbox}
        >
          <MapViewLayer {...publicLayers.baseLayer} />
          <MapViewLayer {...defaultLayers.overLayer} />
          <MapViewLayer
            id={webmapUtils.generateLayerId()}
            geojson={simplePolygonGeoJSON}
            onClickFeature={(featureResult: FeatureEvent): void => {
              setSelectedFeature(featureResult);
            }}
          />
        </MapView>
        {selectedFeature && (
          <MapWarningProperties
            selectedFeatureProperties={selectedFeature.feature.properties!}
          />
        )}
      </div>
    </ThemeProvider>
  );
};

Map6.storyName = 'Display Polygon GeoJSON';

// --- example 7: Display GeoJSON MultiPolygon
export const Map7 = (): React.ReactElement => {
  const [selectedFeature, setSelectedFeature] = React.useState<FeatureEvent>(
    null!,
  );
  return (
    <ThemeProvider>
      <div style={{ height: '100vh' }}>
        <MapView
          mapId={React.useRef<string>(webmapUtils.generateMapId()).current}
          srs={srsAndBboxDefault.srs}
          bbox={srsAndBboxDefault.bbox}
        >
          <MapViewLayer {...publicLayers.baseLayer} />
          <MapViewLayer {...defaultLayers.overLayer} />
          <MapViewLayer
            id={webmapUtils.generateLayerId()}
            geojson={simpleMultiPolygon}
            onClickFeature={(featureResult: FeatureEvent): void => {
              setSelectedFeature(featureResult);
            }}
          />
        </MapView>
        <div
          style={{
            position: 'absolute',
            left: '50px',
            top: '10px',
            zIndex: 10000,
            backgroundColor: '#CCCCCCC0',
            padding: '20px',
            overflow: 'auto',
            width: '80%',
            fontSize: '11px',
            height: '240px',
          }}
        >
          {selectedFeature && (
            <pre>{JSON.stringify(selectedFeature, null, 2)}</pre>
          )}
        </div>
      </div>
    </ThemeProvider>
  );
};

Map7.storyName = 'Display MultiPolygon GeoJSON';
