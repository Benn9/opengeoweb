/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */
import React from 'react';
import { CustomIconButton } from '@opengeoweb/shared';
import { DrawMode } from '../../MapDrawTool';

interface ToolButtonProps {
  mode: DrawMode;
  isSelected: boolean;
  onClick: () => void;
  icon: React.ReactElement;
}

export const ToolButton: React.FC<ToolButtonProps> = ({
  mode,
  isSelected,
  onClick,
  icon,
}: ToolButtonProps) => {
  return (
    <CustomIconButton
      variant="tool"
      tooltipTitle={mode.title}
      isSelected={isSelected}
      onClick={onClick}
      sx={{ marginRight: 1, marginBottom: 1 }}
    >
      {icon}
    </CustomIconButton>
  );
};

export default ToolButton;
