/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */

import React from 'react';
import { Grid, Typography, FormLabel, Switch } from '@mui/material';
import { CustomTooltip } from '@opengeoweb/shared';

interface EditModeButtonProps {
  isInEditMode: boolean;
  onToggleEditMode: (isEditMode: boolean) => void;
  drawMode: string;
}

export const EditModeButton: React.FC<EditModeButtonProps> = ({
  isInEditMode,
  drawMode,
  onToggleEditMode,
}: EditModeButtonProps) => {
  return (
    <Grid item xs={12}>
      <CustomTooltip title="Switch mode">
        <span>
          <FormLabel>View</FormLabel>
          <Switch
            data-testid="switchMode"
            checked={isInEditMode}
            onClick={(): void => onToggleEditMode(!isInEditMode)}
          />
          <FormLabel>Edit</FormLabel>
          {drawMode && (
            <Typography
              component="span"
              sx={{
                display: 'inline-block',
                marginLeft: 2,
                top: -1,
                position: 'relative',
                fontSize: 14,
              }}
            >
              press ESC to exit draw mode
            </Typography>
          )}
        </span>
      </CustomTooltip>
    </Grid>
  );
};

export default EditModeButton;
