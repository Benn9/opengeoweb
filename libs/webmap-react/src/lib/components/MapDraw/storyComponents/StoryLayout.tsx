/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */

import React from 'react';
import { webmapUtils } from '@opengeoweb/webmap';
import { MapView, MapViewLayer, MapViewLayerProps } from '../../MapView';
import { initialBbox } from '../../../layers/defaultStorybookSettings';
import { defaultLayers } from '../../../layers';
import { DrawModeExitCallback } from '../MapDraw';
import { StoryLayoutGrid } from './StoryLayoutGrid';

interface StoryLayoutProps {
  layers: MapViewLayerProps[];
  children: React.ReactElement;
  onExitDrawMode: (reason: DrawModeExitCallback) => void;
}

export const StoryLayout: React.FC<StoryLayoutProps> = ({
  layers,
  children,
  onExitDrawMode,
}: StoryLayoutProps) => {
  return (
    <StoryLayoutGrid
      mapComponent={
        <MapView
          mapId="mapDrawGeoJSONExample"
          srs={initialBbox.srs}
          bbox={initialBbox.bbox}
          onWMJSMount={(mapId): void => {
            const webMap = webmapUtils.getWMJSMapById(mapId);
            webMap.mapPin.hideMapPin();
          }}
        >
          <MapViewLayer {...defaultLayers.baseLayerGrey} />
          <MapViewLayer {...defaultLayers.overLayer} />

          {/** Feature layers */}
          {layers.map((layer) => (
            <MapViewLayer
              key={layer.id}
              {...layer}
              exitDrawModeCallback={onExitDrawMode}
            />
          ))}
        </MapView>
      }
    >
      {children}
    </StoryLayoutGrid>
  );
};

export default StoryLayout;
