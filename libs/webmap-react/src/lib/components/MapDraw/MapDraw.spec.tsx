/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */

import React from 'react';
import { render } from '@testing-library/react';
import * as webmapModule from '@opengeoweb/webmap';
import MapDraw, { MapDrawProps, EDITMODE } from './MapDraw';
import {
  simpleBoxGeoJSON,
  simpleMultiPolygon,
  simplePolygonGeoJSON,
} from './storyComponents/geojsonExamples';

jest.mock('@opengeoweb/webmap', () => ({
  webmapUtils: {
    getWMJSMapById: jest.fn(),
  },
}));

describe('components/MapDraw/MapDraw', () => {
  it('should render with default props', () => {
    const props = {
      isInEditMode: false,
      isInDeleteMode: false,
      drawMode: 'POLYGON',
      onHoverFeature: jest.fn(),
      updateGeojson: jest.fn(),
      exitDrawModeCallback: jest.fn(),
      selectedFeatureIndex: 0,
      geojson: simplePolygonGeoJSON,
      mapId: 'test-id-1',
    };

    const { baseElement } = render(<MapDraw {...props} />);

    expect(baseElement.firstChild).toBeTruthy();
    expect(baseElement.firstChild?.firstChild).toBeTruthy();
  });

  it('handle edit mode', () => {
    const props = {
      isInEditMode: false,
      isInDeleteMode: false,
      drawMode: 'POLYGON',
      onHoverFeature: jest.fn(),
      updateGeojson: jest.fn(),
      exitDrawModeCallback: jest.fn(),
      selectedFeatureIndex: 0,
      geojson: simplePolygonGeoJSON,
    };
    const prevProps = {
      ...props,
      drawMode: '',
    } as unknown as MapDrawProps;

    const testComponent = new MapDraw({
      ...(props as unknown as MapDrawProps),
    });
    const spy = jest.spyOn(testComponent, 'handleDrawMode');

    testComponent.componentDidUpdate(prevProps);

    expect(testComponent.myEditMode).toEqual(EDITMODE.EMPTY);
    expect(spy).toHaveBeenCalledWith(props.drawMode);
  });

  it('handle cancel edit', () => {
    const props = {
      isInEditMode: false,
      isInDeleteMode: false,
      drawMode: 'POLYGON',
      onHoverFeature: jest.fn(),
      updateGeojson: jest.fn(),
      exitDrawModeCallback: jest.fn(),
      selectedFeatureIndex: 0,
      geojson: simplePolygonGeoJSON,
    };

    const prevProps = {
      ...props,
      isInEditMode: true,
    } as unknown as MapDrawProps;

    const testComponent = new MapDraw({
      ...props,
    } as unknown as MapDrawProps);
    testComponent.myEditMode = EDITMODE.DELETE_FEATURES;
    const spy = jest.spyOn(testComponent, 'cancelEdit');
    const drawspy = jest.spyOn(testComponent, 'handleDrawMode');

    testComponent.componentDidUpdate(prevProps);

    expect(testComponent.myEditMode).toEqual(EDITMODE.EMPTY);
    expect(spy).toHaveBeenCalledWith(true);
    expect(drawspy).not.toHaveBeenCalled();
  });

  it('should delete point Feature correctly', () => {
    const props: MapDrawProps = {
      isInEditMode: true,
      isInDeleteMode: false,
      drawMode: 'POLYGON',
      onHoverFeature: jest.fn(),
      updateGeojson: jest.fn(),
      exitDrawModeCallback: jest.fn(),
      selectedFeatureIndex: 0,
      geojson: {
        type: 'FeatureCollection',
        features: [
          {
            type: 'Feature',
            properties: {
              stroke: '#f24a00',
              'stroke-width': 1.5,
              'stroke-opacity': 1,
              fill: '#f24a00',
              'fill-opacity': 0.25,
              selectionType: 'point',
            },
            geometry: {
              type: 'Point',
              coordinates: [6.061753373579083, 52.48421727270884],
            },
          },
        ],
      },
      webmapjs: { draw: jest.fn() },
      mapId: 'test1',
    } as unknown as MapDrawProps;

    const instanceOfMapDraw = new MapDraw({
      ...props,
    } as unknown as MapDrawProps);

    const spy = jest.fn();
    (webmapModule.webmapUtils.getWMJSMapById as jest.Mock).mockImplementation(
      () => ({
        draw: spy,
      }),
    );

    const featureChangeSpy = jest.spyOn(instanceOfMapDraw, 'featureHasChanged');

    instanceOfMapDraw.mouseIsOverVertexNr = 1;
    instanceOfMapDraw.snappedPolygonIndex = 1;
    instanceOfMapDraw.deleteFeature();

    expect(spy).toHaveBeenCalledWith('MapDraw::deletefeatures');
    expect(featureChangeSpy).toHaveBeenCalledWith('deleteFeature');
  });

  it('handle change snappedPolygonIndex nr correctly', () => {
    const props: MapDrawProps = {
      isInEditMode: true,
      isInDeleteMode: false,
      drawMode: 'POLYGON',
      onHoverFeature: jest.fn(),
      updateGeojson: jest.fn(),
      exitDrawModeCallback: jest.fn(),
      selectedFeatureIndex: 0,
      geojson: simpleMultiPolygon,
    } as unknown as MapDrawProps;

    const instanceOfMapDraw = new MapDraw({
      ...props,
    } as unknown as MapDrawProps);

    instanceOfMapDraw.snappedPolygonIndex = 1;
    instanceOfMapDraw.handleGeoJSONUpdate(simpleBoxGeoJSON);
    instanceOfMapDraw.componentDidUpdate(props);
    expect(instanceOfMapDraw.snappedPolygonIndex).toEqual(0);
  });

  it('should call exitDrawModeCallback on double click', () => {
    const props: MapDrawProps = {
      isInEditMode: true,
      isInDeleteMode: false,
      drawMode: 'POLYGON',
      onHoverFeature: jest.fn(),
      updateGeojson: jest.fn(),
      exitDrawModeCallback: jest.fn(),
      selectedFeatureIndex: 0,
      geojson: simpleMultiPolygon,
    } as unknown as MapDrawProps;

    const instanceOfMapDraw = new MapDraw({
      ...props,
    } as unknown as MapDrawProps);

    instanceOfMapDraw.mouseDoubleClick();

    expect(props.exitDrawModeCallback).toHaveBeenCalledWith('doubleClicked');
  });

  it('should call exitDrawModeCallback with default escaped', () => {
    const props: MapDrawProps = {
      isInEditMode: true,
      isInDeleteMode: false,
      drawMode: 'POLYGON',
      onHoverFeature: jest.fn(),
      updateGeojson: jest.fn(),
      exitDrawModeCallback: jest.fn(),
      selectedFeatureIndex: 0,
      geojson: simpleMultiPolygon,
    } as unknown as MapDrawProps;

    const instanceOfMapDraw = new MapDraw({
      ...props,
    } as unknown as MapDrawProps);

    instanceOfMapDraw.handleExitDrawMode();

    expect(props.exitDrawModeCallback).toHaveBeenCalledWith('escaped');
  });

  it('should call exitDrawModeCallback when pressing escape key', () => {
    const props: MapDrawProps = {
      isInEditMode: true,
      isInDeleteMode: false,
      drawMode: 'POLYGON',
      onHoverFeature: jest.fn(),
      updateGeojson: jest.fn(),
      exitDrawModeCallback: jest.fn(),
      selectedFeatureIndex: 0,
      geojson: simpleMultiPolygon,
    } as unknown as MapDrawProps;

    const instanceOfMapDraw = new MapDraw({
      ...props,
    } as unknown as MapDrawProps);

    instanceOfMapDraw.handleKeyDown({ keyCode: 27 } as KeyboardEvent);

    expect(props.exitDrawModeCallback).toHaveBeenCalledWith('escaped');
  });
});
