/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { render, screen } from '@testing-library/react';

import { FeatureLayer, MapDrawContainer } from './MapDrawContainer';

describe('src/components/ReactMapView/MapDrawContainer', () => {
  it('should render feature layers', async () => {
    const featureLayers = [
      {
        id: 'layer-1',
        type: 'string',
        features: [],
      },
      {
        id: 'layer-2',
        type: 'string',
        features: [],
      },
    ] as FeatureLayer[];

    const props = {
      featureLayers,
      mapId: 'test-map-1',
    };

    render(<MapDrawContainer {...props} />);

    featureLayers.forEach(({ id }) => {
      expect(screen.getByTestId(`featureLayer-${id}`)).toBeTruthy();
    });
  });
});
