/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import {
  Dimension,
  LayerOptions,
  WMLayer,
  webmapTestSettings,
  webmapUtils,
} from '@opengeoweb/webmap';

import { ThemeWrapper } from '@opengeoweb/theme';
import { Legend } from './Legend';

export default { title: 'components/Legend' };

export const SimpleLegend = (): React.ReactElement => {
  const radarLayer = webmapTestSettings.makeGeoservicesRadarLayer();
  radarLayer.legendGraphic =
    'https://geoservices.knmi.nl/wms?dataset=RADAR&SERVICE=WMS&&version=1.1.1&service=WMS&request=GetLegendGraphic&layer=RAD_NL25_PCP_CM&format=image/png&STYLE=precip-blue-transparent/nearest';

  webmapUtils.registerWMLayer(radarLayer, radarLayer.id);

  return (
    <ThemeWrapper>
      <Legend layer={radarLayer} />
    </ThemeWrapper>
  );
};

export const SimpleLegendWithMultiDimensions = (): React.ReactElement => {
  const dimensions: Dimension[] = [
    {
      name: 'flight level',
      units: 'hft',
      currentValue: '625',
      values: '25,325,625',
    },
    {
      name: 'elevation',
      units: 'meters',
      currentValue: '9000',
      values: '1000,5000,9000',
    },
    {
      name: 'time',
      units: 'ISO8601',
      currentValue: '2021-04-14T14:40:00Z',
    },
  ];
  const wmStoryLayer = webmapTestSettings.makeGeoservicesRadarLayer(dimensions);

  webmapUtils.registerWMLayer(wmStoryLayer, wmStoryLayer.id);
  wmStoryLayer.legendGraphic =
    'https://geoservices.knmi.nl/wms?dataset=RADAR&SERVICE=WMS&&version=1.1.1&service=WMS&request=GetLegendGraphic&layer=RAD_NL25_PCP_CM&format=image/png&STYLE=precip-rainbow/nearest';

  return (
    <ThemeWrapper>
      <Legend layer={wmStoryLayer} />
    </ThemeWrapper>
  );
};

export const SimpleLegendWithoutDimensionsOrGraphic =
  (): React.ReactElement => {
    const storyLayer = {
      ...webmapTestSettings.defaultReduxLayerRadarKNMI,
      dimensions: [],
    } as LayerOptions;
    webmapUtils.registerWMLayer(
      new WMLayer(storyLayer),
      webmapTestSettings.defaultReduxLayerRadarKNMI.id!,
    );

    return (
      <ThemeWrapper>
        <Legend layer={storyLayer} />
      </ThemeWrapper>
    );
  };
