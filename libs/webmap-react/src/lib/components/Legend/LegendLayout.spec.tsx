/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */
import * as React from 'react';

import { render, screen } from '@testing-library/react';
import { ThemeWrapper } from '@opengeoweb/theme';
import { LegendLayout } from './LegendLayout';

describe('src/components/Legend/LegendLayout', () => {
  it('should render with default props', async () => {
    const props = {
      title: '',
      name: '',
      dimensions: [],
    };

    render(
      <ThemeWrapper>
        <LegendLayout {...props}>
          <p>testing children</p>
        </LegendLayout>
      </ThemeWrapper>,
    );

    expect(await screen.findAllByLabelText('loading')).toBeTruthy();
    expect(await screen.findByText('testing children')).toBeTruthy();
  });

  it('should render title and dimensions', async () => {
    const props = {
      title: 'test title',
      name: '',
      dimensions: [
        {
          name: 'elevation',
          values: '1000,500,100',
          units: 'hPa',
          currentValue: '1000',
        },
        {
          name: 'time',
          values: '2022-10-04T00:00:00Z/2022-10-08T03:00:00Z/PT1H',
          units: 'ISO8601',
          currentValue: '2022-10-04T00:00:00Z',
        },
        {
          name: 'reference_time',
          values: '2022-10-06T00:00:00Z,2022-10-06T03:00:00Z',
          units: 'ISO8601',
          currentValue: '2022-10-06T00:00:00Z',
        },
      ],
    };

    render(
      <ThemeWrapper>
        <LegendLayout {...props}>
          <p>testing children</p>
        </LegendLayout>
      </ThemeWrapper>,
    );

    expect(await screen.findAllByLabelText(props.title)).toBeTruthy();
    expect(await screen.findByText('testing children')).toBeTruthy();

    // dimensions
    expect(
      await screen.findByText(
        `${props.dimensions[0].currentValue} ${props.dimensions[0].units}`,
      ),
    ).toBeTruthy();
    expect(
      await screen.findByText(props.dimensions[1].currentValue),
    ).toBeTruthy();
    expect(
      await screen.findByText(
        `${props.dimensions[2].currentValue} ${props.dimensions[2].units}`,
      ),
    ).toBeTruthy();
  });
});
