/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */

import * as turf from '@turf/turf';
import produce from 'immer';
import {
  booleanClockwise,
  rewind,
  polygonToLineString,
  Polygon,
  Feature,
  LineString,
} from '@turf/turf';
import { FeatureCollection } from 'geojson';
import {
  GeoJsonFeatureType,
  defaultGeoJSONStyleProperties,
  emptyGeoJSON,
} from '../MapDraw/geojsonShapes';
import {
  NEW_FEATURE_CREATED,
  NEW_LINESTRING_CREATED,
  NEW_POINT_CREATED,
} from '../MapDraw/mapDrawUtils';
import { DrawMode } from './types';

/**
 * Adds properties to the first geojson feature based on the given property object.
 * It only extends or changes the properties which are defined in styleConfig,
 * all other properties in the geojson are left unchanged.
 * @param geojson
 * @param featureProperties
 */
export const addFeatureProperties = (
  geojson: GeoJSON.FeatureCollection | undefined,
  featureProperties: GeoJSON.GeoJsonProperties,
  featureIndex = 0,
): GeoJSON.FeatureCollection => {
  if (!geojson) {
    return null!;
  }
  return produce(geojson, (draft) => {
    if (
      draft.features &&
      draft.features.length > 0 &&
      draft.features[featureIndex] !== undefined &&
      draft.features[featureIndex].properties
    ) {
      Object.keys(featureProperties!).forEach((key) => {
        draft.features[featureIndex].properties![key] = featureProperties![key];
      });
    }
  });
};

export const getGeoJSONPropertyValue = (
  property: string,
  properties: GeoJSON.GeoJsonProperties,
  polygonDrawMode: DrawMode | undefined,
  defaultProperties: GeoJSON.GeoJsonProperties = defaultGeoJSONStyleProperties,
): number | string => {
  // if a shape is set, extract the style from there
  if (properties![property] !== undefined) {
    return properties![property];
  }
  // if active polygon tool is preset, retreive style from there
  if (polygonDrawMode) {
    const polygonDrawModeProperty =
      polygonDrawMode.shape.type === 'Feature' &&
      polygonDrawMode.shape.properties![property];

    if (polygonDrawModeProperty !== undefined) {
      return polygonDrawModeProperty;
    }
  }
  // otherwise get values from defaultStyle
  return defaultProperties![property];
};

/**
 * moves geoJSON feature as new feature. Mutates newGeoJSON
 * @constructor
 * @param {GeoJSON.FeatureCollection} currentGeoJSON - current geoJSON
 * @param {GeoJSON.FeatureCollection} newGeoJSON - new geoJSON
 * @param {number} featureLayerIndex - feature layer index
 * @param {string} text - reason of change
 */
export const moveFeature = (
  currentGeoJSON: GeoJSON.FeatureCollection,
  newGeoJSON: GeoJSON.FeatureCollection,
  featureLayerIndex: number,
  reason: string,
): number | undefined => {
  const feature = newGeoJSON.features[featureLayerIndex];
  const currentFeature = currentGeoJSON.features[featureLayerIndex];

  if (feature) {
    const { geometry } = feature;

    if (
      geometry.type === 'Polygon' &&
      geometry.coordinates.length > 1 &&
      reason === NEW_FEATURE_CREATED
    ) {
      const lastCoordinate = geometry.coordinates.pop();
      const copyFeature = {
        ...feature,
        geometry: {
          ...geometry,
          coordinates: [lastCoordinate],
        },
      } as GeoJSON.Feature<GeoJSON.Polygon>;

      newGeoJSON.features.push(copyFeature);

      return newGeoJSON.features.length - 1;
    }
    if (
      geometry.type === 'LineString' &&
      geometry.coordinates.length > 2 &&
      reason === NEW_LINESTRING_CREATED
    ) {
      const lastCoordinate = geometry.coordinates.pop();
      const copyFeature = {
        ...feature,
        geometry: {
          ...geometry,
          coordinates: [lastCoordinate, lastCoordinate],
        },
      } as GeoJSON.Feature<GeoJSON.LineString>;
      newGeoJSON.features.push(copyFeature);
      return newGeoJSON.features.length - 1;
    }
    if (
      geometry.type === 'Point' &&
      currentFeature &&
      currentFeature.geometry.type === 'Point' &&
      currentFeature.geometry.coordinates.length > 0 &&
      reason === NEW_POINT_CREATED
    ) {
      newGeoJSON.features.push(currentFeature);

      return newGeoJSON.features.length - 1;
    }
  }

  return undefined;
};

/**
 * Returns the intersection of two features. In case of a polygon, only the first feature is used.
 * @param a Feature A
 * @param b Feature B
 * @returns The intersection of the two features.
 */
export const intersectGeoJSONS = (
  a: GeoJSON.FeatureCollection,
  b: GeoJSON.FeatureCollection,
  geoJSONProperties = {
    stroke: '#FF0000',
    'stroke-width': 10.0,
    'stroke-opacity': 1,
    fill: '#0000FF',
    'fill-opacity': 1.0,
  },
): GeoJSON.FeatureCollection => {
  const featureA = turf.feature(a.features[0].geometry as turf.Polygon);
  const featureB = turf.feature(b.features[0].geometry as turf.Polygon);
  const options = { tolerance: 0.001, highQuality: true };
  const simplifiedA = turf.simplify(featureA, options);
  const simplifiedB = turf.simplify(featureB, options);
  const intersection = turf.intersect(simplifiedA, simplifiedB);

  return addFeatureProperties(
    {
      type: 'FeatureCollection',
      features:
        intersection === null
          ? [
              {
                type: 'Feature',
                properties: {
                  selectionType: 'poly',
                },
                geometry: {
                  type: 'Polygon',
                  coordinates: [[]],
                },
              },
            ]
          : [intersection],
    },
    geoJSONProperties,
  );
};

/**
 * Adds the intersectionStart and intersectionEnd properties to the GeoJSONS structure
 * @param geoJSONs
 * @returns GeoJSONS extend with intersections
 */

export const createInterSections = (
  geojson: GeoJSON.FeatureCollection,
  otherGeoJSON: GeoJSON.FeatureCollection,
  geoJSONproperties: GeoJSON.GeoJsonProperties = {
    stroke: '#f24a00',
    'stroke-width': 1.5,
    'stroke-opacity': 1,
    fill: '#f24a00',
    'fill-opacity': 0.5,
  },
): GeoJSON.FeatureCollection => {
  const intersections = produce(geojson, () => {
    try {
      return addFeatureProperties(
        intersectGeoJSONS(geojson, otherGeoJSON!),
        geoJSONproperties,
      );
    } catch (error) {
      return addFeatureProperties(geojson, geoJSONproperties);
    }
  });
  return intersections as unknown as GeoJSON.FeatureCollection;
};

export const getGeoJson = (
  geojson: GeoJSON.FeatureCollection,
  shouldAllowMultipleShapes: boolean,
): GeoJSON.FeatureCollection => {
  if (shouldAllowMultipleShapes || !geojson.features.length) {
    return geojson;
  }

  const { geometry } = geojson
    .features[0] as GeoJSON.Feature<GeoJsonFeatureType>;
  if (geometry.type === 'Point' || geometry.type === 'LineString') {
    return geojson;
  }

  return geometry.coordinates.length > 1
    ? produce(geojson, (draft) => {
        const draftFeature = draft
          .features[0] as GeoJSON.Feature<GeoJSON.Polygon>;
        draftFeature.geometry.coordinates = [
          draftFeature.geometry.coordinates[1],
        ];
      })
    : geojson;
};

export const getFeatureCollection = (
  geoJSONFeature: GeoJSON.Feature | GeoJSON.FeatureCollection,
  shouldAllowMultipleShapes: boolean,
  geoJSONFeatureCollection: GeoJSON.FeatureCollection = emptyGeoJSON,
): GeoJSON.FeatureCollection => {
  if (geoJSONFeature.type === 'FeatureCollection') {
    return geoJSONFeature;
  }

  if (shouldAllowMultipleShapes) {
    return {
      ...geoJSONFeatureCollection,
      features: geoJSONFeatureCollection.features.concat(geoJSONFeature),
    };
  }
  return { ...geoJSONFeatureCollection, features: [geoJSONFeature] };
};

// compares 2 geoJSONs and returns true if both have properties.selectionType with the same value
export const isGeoJSONFeatureCreatedByTool = (
  existingJSON: GeoJSON.FeatureCollection,
  newGeoJSON: GeoJSON.Feature | GeoJSON.FeatureCollection,
): boolean => {
  if (!existingJSON.features.length) {
    return false;
  }
  const lastUsedTool = existingJSON.features[0].properties?.selectionType;
  const newTool =
    newGeoJSON.type === 'Feature'
      ? newGeoJSON.properties?.selectionType
      : newGeoJSON.features[0].properties?.selectionType;

  return lastUsedTool !== undefined && newTool !== undefined
    ? lastUsedTool === newTool
    : false;
};

export const rewindGeometry = (
  geoJSON: FeatureCollection,
): FeatureCollection => {
  return produce(geoJSON, (geoJSONDraft) => {
    if (
      geoJSONDraft &&
      geoJSONDraft.features.length > 0 &&
      geoJSONDraft.features[0].geometry.type === 'Polygon' &&
      geoJSONDraft.features[0].geometry.coordinates[0].length > 1
    ) {
      const { geometry } = geoJSON.features[0] as Feature<Polygon>;
      const lineString = polygonToLineString(geometry!) as Feature<LineString>;
      if (booleanClockwise(lineString.geometry!.coordinates)) {
        // eslint-disable-next-line no-param-reassign
        geoJSONDraft.features[0].geometry = rewind(
          geoJSONDraft.features[0].geometry,
        );
      }
    }
  });
};
