/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */

import React from 'react';

import {
  DrawRegion,
  DrawPolygon,
  Edit,
  Location,
  Delete,
} from '@opengeoweb/theme';

import {
  defaultGeoJSONStyleProperties,
  emptyGeoJSON,
  featurePoint,
  featurePolygon,
  lineString,
} from '../MapDraw/geojsonShapes';
import { DrawMode, DrawModeValue } from './types';
import { MapViewLayerProps } from '../MapView';
import { DRAWMODE } from '../MapDraw/MapDraw';

import {
  isGeoJSONFeatureCreatedByTool,
  moveFeature,
  createInterSections,
  getFeatureCollection,
  getGeoJson,
  addFeatureProperties,
} from './utils';

export const defaultIntersectionStyleProperties: GeoJSON.GeoJsonProperties = {
  ...defaultGeoJSONStyleProperties,
  'fill-opacity': 0.5,
};

export const emptyLineString: GeoJSON.Feature = {
  ...lineString.features[0],
  properties: { ...defaultGeoJSONStyleProperties, selectionType: 'linestring' },
};
export const emptyPoint: GeoJSON.Feature = {
  ...featurePoint,
  properties: {
    ...defaultGeoJSONStyleProperties,
    selectionType: 'point',
  },
};
export const emptyPolygon: GeoJSON.Feature = {
  ...featurePolygon,
  properties: { ...defaultGeoJSONStyleProperties, selectionType: 'poly' },
};
export const emptyBox: GeoJSON.Feature = {
  ...featurePolygon,
  properties: { ...defaultGeoJSONStyleProperties, selectionType: 'box' },
};

// TODO: improve this: without a feature, the custom shape does not work on first click
export const emptyIntersectionShape: GeoJSON.FeatureCollection = {
  ...emptyGeoJSON,
  features: [featurePolygon],
};

type DrawLayerType =
  | 'geoJSON'
  | 'geoJSONIntersection'
  | 'geoJSONIntersectionBounds';

export interface MapDrawToolProps {
  geoJSON: GeoJSON.FeatureCollection; // user selection
  geoJSONIntersection?: GeoJSON.FeatureCollection; // result of selection and geoJSONIntersectionBounds
  geoJSONIntersectionBounds?: GeoJSON.FeatureCollection; // static intersection shape
  setGeoJSON: (
    geojson: GeoJSON.Feature | GeoJSON.FeatureCollection,
    reason?: string,
  ) => void;
  setGeoJSONIntersectionBounds: (geojson: GeoJSON.FeatureCollection) => void;
  drawModes: DrawMode[];
  changeDrawMode: (mode: DrawModeValue) => void;
  setDrawModes: (drawModes: DrawMode[]) => void;
  isInEditMode: boolean;
  setEditMode: (shouldEnable: boolean) => void;
  featureLayerIndex: number;
  setFeatureLayerIndex: (newIndex: number) => void;
  activeTool: string;
  changeActiveTool: (newMode: DrawMode) => void;
  setActiveTool: (newToolId: string) => void;
  layers: MapViewLayerProps[];
  getLayer: (layerType: DrawLayerType, layerId: string) => MapViewLayerProps;
  deactivateTool: () => void;
  changeProperties: (geoJSONProperties: GeoJSON.GeoJsonProperties) => void;
  getProperties: () => GeoJSON.GeoJsonProperties;
}

export interface MapDrawToolOptions {
  shouldAllowMultipleShapes?: boolean;
  defaultDrawModes?: DrawMode[];
  defaultGeoJSON?: GeoJSON.FeatureCollection;
  defaultGeoJSONIntersection?: GeoJSON.FeatureCollection;
  defaultGeoJSONIntersectionBounds?: GeoJSON.FeatureCollection;
  defaultGeoJSONIntersectionProperties?: GeoJSON.GeoJsonProperties;
  geoJSONLayerId?: string;
  geoJSONIntersectionLayerId?: string;
  geoJSONIntersectionBoundsLayerId?: string;
}

export const getIcon = (drawModeId: string): React.ReactElement | null => {
  switch (drawModeId) {
    case 'drawtools-point':
      return <Location />;
    case 'drawtools-polygon':
      return <DrawPolygon />;
    case 'drawtools-box':
      return <DrawRegion />;
    case 'drawtools-linestring':
      return <Edit />;
    case 'drawtools-delete':
      return <Delete />;
    default:
      return null;
  }
};

export const defaultPoint = {
  drawModeId: 'drawtools-point',
  value: DRAWMODE.POINT,
  title: 'Point',
  shape: emptyPoint,
  isSelectable: true,
};
export const defaultPolygon = {
  drawModeId: 'drawtools-polygon',
  value: DRAWMODE.POLYGON,
  title: 'Polygon',
  shape: emptyPolygon,
  isSelectable: true,
};
export const defaultBox = {
  drawModeId: 'drawtools-box',
  value: DRAWMODE.BOX,
  title: 'Box',
  shape: emptyBox,
  isSelectable: true,
};
export const defaultLineString = {
  drawModeId: 'drawtools-linestring',
  value: DRAWMODE.LINESTRING,
  title: 'LineString',
  shape: emptyLineString,
  isSelectable: true,
};
export const defaultDelete = {
  drawModeId: 'drawtools-delete',
  value: 'DELETE' as const,
  title: 'Delete',
  shape: emptyGeoJSON,
  isSelectable: false,
};

export const defaultModes = [
  defaultPoint,
  defaultPolygon,
  defaultBox,
  defaultLineString,
  defaultDelete,
];

export const currentlySupportedDrawModes = [defaultPolygon, defaultDelete];

export const useMapDrawTool = ({
  defaultDrawModes = defaultModes,
  shouldAllowMultipleShapes = false,
  defaultGeoJSON = emptyGeoJSON,
  defaultGeoJSONIntersection = emptyIntersectionShape,
  defaultGeoJSONIntersectionBounds,
  defaultGeoJSONIntersectionProperties = defaultIntersectionStyleProperties,
  geoJSONLayerId = 'draw-layer',
  geoJSONIntersectionLayerId = 'intersection-layer',
  geoJSONIntersectionBoundsLayerId = 'static-layer',
}: MapDrawToolOptions): MapDrawToolProps => {
  // geoJSON feature collections
  const [geoJSON, setGeoJSON] =
    React.useState<GeoJSON.FeatureCollection>(defaultGeoJSON);
  const [geoJSONIntersection, setGeoJSONIntersection] =
    React.useState<GeoJSON.FeatureCollection>(defaultGeoJSONIntersection);
  const [geoJSONIntersectionBounds, setGeoJSONIntersectionBounds] =
    React.useState<GeoJSON.FeatureCollection | undefined>(
      defaultGeoJSONIntersectionBounds,
    );
  // state
  const [drawModes, setDrawModes] = React.useState(defaultDrawModes);
  const [activeTool, setActiveTool] = React.useState<string>('');
  const [drawMode, setDrawMode] = React.useState<DrawModeValue>('');
  const [isInEditMode, setEditMode] = React.useState<boolean>(false);
  const [featureLayerIndex, setFeatureLayerIndex] = React.useState<number>(0);

  const changeProperties = (
    styleProperties: GeoJSON.GeoJsonProperties,
  ): void => {
    // update all modes with new properties
    const newModes: DrawMode[] = drawModes.map((mode) => {
      const shape =
        mode.shape.type === 'Feature'
          ? {
              ...mode.shape,
              properties: { ...mode.shape.properties, ...styleProperties },
            }
          : addFeatureProperties(mode.shape, styleProperties);

      return { ...mode, shape };
    });
    setDrawModes(newModes);
    // update current geoJSON with new properties
    const updateGeoJSON = addFeatureProperties(
      geoJSON,
      styleProperties,
      featureLayerIndex,
    );

    setGeoJSON(updateGeoJSON);
  };

  const getProperties = (): GeoJSON.GeoJsonProperties => {
    if (!geoJSON || !geoJSON.features.length) {
      return {};
    }

    return geoJSON.features[featureLayerIndex].properties;
  };

  const changeActiveTool = (newMode: DrawMode): void => {
    const shouldDeleteShape = newMode.value === 'DELETE';
    // reset if same tool is selected
    if (newMode.drawModeId === activeTool || shouldDeleteShape) {
      reset(shouldDeleteShape);
      return;
    }

    setActiveTool(newMode.isSelectable ? newMode.drawModeId : '');

    // updates shape
    const isNewSelectedTool = !isGeoJSONFeatureCreatedByTool(
      geoJSON,
      newMode.shape,
    );
    const shouldUpdateShape =
      !geoJSON.features.length ||
      shouldAllowMultipleShapes ||
      isNewSelectedTool;

    if (shouldUpdateShape) {
      const updatedGeoJSON = changeGeoJSON(newMode.shape);
      setFeatureLayerIndex(updatedGeoJSON.features.length - 1);
    }

    // handle modes and update feature layer index
    setDrawMode(newMode.value);
    setEditMode(!!newMode.value && newMode.isSelectable);
  };

  const changeGeoJSON = (
    updatedGeoJSON: GeoJSON.FeatureCollection | GeoJSON.Feature,
    reason = '',
  ): GeoJSON.FeatureCollection => {
    const geoJSONFeatureCollection = getFeatureCollection(
      updatedGeoJSON,
      shouldAllowMultipleShapes,
      geoJSON,
    );
    const newGeoJSON = getGeoJson(
      geoJSONFeatureCollection,
      shouldAllowMultipleShapes,
    );

    if (shouldAllowMultipleShapes) {
      const newFeatureIndex = moveFeature(
        geoJSON,
        newGeoJSON,
        featureLayerIndex,
        reason,
      );
      if (newFeatureIndex !== undefined) {
        setFeatureLayerIndex(newFeatureIndex);
      }
    }

    setGeoJSON(newGeoJSON);

    if (geoJSONIntersectionBounds) {
      setGeoJSONIntersection(
        createInterSections(
          newGeoJSON,
          geoJSONIntersectionBounds,
          defaultGeoJSONIntersectionProperties,
        ),
      );
    }

    return newGeoJSON;
  };

  const onSetGeoJSONIntersectionBounds = (
    newGeoJSON: GeoJSON.FeatureCollection,
  ): void => {
    setGeoJSONIntersectionBounds(newGeoJSON);
    // reset all other geoJSONs
    setGeoJSONIntersection(emptyIntersectionShape);
    setGeoJSON(emptyGeoJSON);
  };

  const onSetDrawMode = (drawMode: DrawModeValue): void => {
    setDrawMode(drawMode);
    const newActiveTool = drawModes.find((mode) => mode.value === drawMode);
    if (newActiveTool) {
      setActiveTool(newActiveTool.drawModeId);
    }
  };

  const deactivateTool = (): void => {
    setEditMode(false);
    setActiveTool('');
    setDrawMode('');
  };

  const reset = (shouldClearState = false): void => {
    deactivateTool();

    if (shouldClearState) {
      setGeoJSONIntersection(emptyIntersectionShape);
      setGeoJSON(emptyGeoJSON);
      setFeatureLayerIndex(0);
    }
  };

  const getLayer = (
    layerType: DrawLayerType,
    layerId: string,
  ): MapViewLayerProps => {
    // geoJSON
    if (layerType === 'geoJSON') {
      return {
        id: layerId,
        geojson: geoJSON,
        isInEditMode,
        drawMode,
        updateGeojson: changeGeoJSON,
        selectedFeatureIndex: featureLayerIndex,
      };
    }

    // intersections
    return {
      id: layerId,
      geojson:
        layerType === 'geoJSONIntersection'
          ? geoJSONIntersection
          : geoJSONIntersectionBounds,
      isInEditMode: false,
    };
  };

  const layers: MapViewLayerProps[] = [
    ...(geoJSONIntersectionBounds
      ? [
          getLayer(
            'geoJSONIntersectionBounds',
            geoJSONIntersectionBoundsLayerId,
          ),
          getLayer('geoJSONIntersection', geoJSONIntersectionLayerId),
        ]
      : []),
    getLayer('geoJSON', geoJSONLayerId),
  ];

  return {
    geoJSON,
    geoJSONIntersection,
    geoJSONIntersectionBounds,
    setGeoJSON: changeGeoJSON,
    setGeoJSONIntersectionBounds: onSetGeoJSONIntersectionBounds,
    drawModes,
    isInEditMode,
    changeDrawMode: onSetDrawMode,
    setEditMode,
    featureLayerIndex,
    setFeatureLayerIndex,
    activeTool,
    changeActiveTool,
    setActiveTool,
    layers,
    getLayer,
    deactivateTool,
    setDrawModes,
    changeProperties,
    getProperties,
  };
};
