/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */

import React from 'react';
import { Grid, Divider, Typography } from '@mui/material';
import { DrawFIRLand, ThemeProvider } from '@opengeoweb/theme';

import {
  defaultBox,
  defaultDelete,
  defaultIntersectionStyleProperties,
  defaultPoint,
  defaultPolygon,
  getIcon,
  useMapDrawTool,
} from './useMapDrawTool';
import {
  intersectionFeatureBE,
  intersectionFeatureNL,
} from '../MapDraw/storyComponents/geojsonExamples';
import { DrawMode } from './types';
import { DRAWMODE, DrawModeExitCallback } from '../MapDraw/MapDraw';
import { GeoJSONTextField } from '../MapDraw/storyComponents';
import { StoryLayout } from '../MapDraw/storyComponents/StoryLayout';
import { ToolButton } from '../MapDraw/storyComponents/ToolButton';
import { IntersectionSelect } from '../MapDraw/storyComponents/IntersectionSelect';
import { defaultGeoJSONStyleProperties } from '../MapDraw/geojsonShapes';

export default {
  title: 'components/MapDrawTool/multiple drawtools',
};

const getToolIcon = (id: string): React.ReactElement => {
  const defaultIcon = getIcon(id);
  if (defaultIcon) {
    return defaultIcon;
  }
  if (id === 'fir-button') {
    return <DrawFIRLand />;
  }
  return <DrawFIRLand />;
};

// styles and shapes
const featurePropertiesStart = defaultGeoJSONStyleProperties;
const featurePropertiesEnd = {
  stroke: '#6e1e91',
  'stroke-width': 1.5,
  'stroke-opacity': 1,
  fill: '#6e1e91',
  'fill-opacity': 0.25,
};
const geoJSONIntersectionBoundsStyle = {
  stroke: '#000000',
  'stroke-width': 1.5,
  'stroke-opacity': 1,
  fill: '#0075a9',
  'fill-opacity': 0.0,
};

const intersectionShapeNL: GeoJSON.FeatureCollection = {
  type: 'FeatureCollection',
  features: [
    {
      ...intersectionFeatureNL,
      properties: { ...geoJSONIntersectionBoundsStyle, selectionType: 'fir' },
    },
  ],
};

const intersectionShapeBE: GeoJSON.FeatureCollection = {
  type: 'FeatureCollection',
  features: [
    {
      ...intersectionFeatureBE,
      properties: { ...geoJSONIntersectionBoundsStyle, selectionType: 'fir' },
    },
  ],
};

// custom buttons
const shapeButtonNL: DrawMode = {
  drawModeId: 'fir-button',
  value: DRAWMODE.POLYGON,
  title: 'Custom FIR NL polygon',
  shape: intersectionShapeNL,
  isSelectable: false,
};

const getEditModes = (
  geoJSONProperties: GeoJSON.GeoJsonProperties,
): DrawMode[] => [
  {
    ...defaultPoint,
    shape: {
      ...defaultPoint.shape,
      properties: { ...defaultPoint.shape.properties, ...geoJSONProperties },
    },
  },
  {
    ...defaultPolygon,
    shape: {
      ...defaultPolygon.shape,
      properties: { ...defaultPolygon.shape.properties, ...geoJSONProperties },
    },
  },
  {
    ...defaultBox,
    shape: {
      ...defaultBox.shape,
      properties: { ...defaultBox.shape.properties, ...geoJSONProperties },
    },
  },
  {
    ...shapeButtonNL,
    shape: {
      ...shapeButtonNL.shape,
      features: [
        {
          ...intersectionFeatureNL,
          properties: {
            ...intersectionShapeNL.features[0].properties,
            ...geoJSONProperties,
          },
        },
      ],
    } as GeoJSON.FeatureCollection,
  },
  defaultDelete,
];

const updateEditModeButtonsWithFir = (
  drawModes: DrawMode[],
  newFirGeoJSON: GeoJSON.FeatureCollection,
): DrawMode[] => {
  const isNL = newFirGeoJSON === intersectionShapeNL;

  return drawModes.map((mode) => {
    if (mode.drawModeId === 'fir-button') {
      return {
        ...mode,
        shape: isNL ? intersectionShapeNL : intersectionShapeBE,
        icon: isNL ? (
          <DrawFIRLand />
        ) : (
          <DrawFIRLand sx={{ transform: `scaleY(-1)` }} />
        ),
        title: isNL ? 'Custom FIR NL polygon' : 'Custom FIR BE polygon',
      };
    }

    return mode;
  });
};

export const DoubleMapControlsDemo = (): React.ReactElement => {
  const {
    geoJSON: startGeoJSON,
    geoJSONIntersection: startGeoJSONIntersection,
    drawModes: startEditModes,
    activeTool: startActiveTool,
    changeActiveTool: startChangeActiveTool,
    getLayer: startGetLayer,
    deactivateTool: startDeactivateTool,
    setGeoJSONIntersectionBounds: startSetGeoJSONIntersectionBounds,
    setDrawModes: startSetEditModes,
  } = useMapDrawTool({
    defaultGeoJSONIntersectionBounds: intersectionShapeNL,
    defaultDrawModes: getEditModes(featurePropertiesStart),
    defaultGeoJSON: {
      type: 'FeatureCollection',
      features: [
        {
          type: 'Feature',
          properties: {
            ...featurePropertiesStart,
            selectionType: 'box',
          },
          geometry: {
            type: 'Polygon',
            coordinates: [
              [
                [9.451665078283622, 53.21804334226515],
                [4.734608638338736, 53.21804334226515],
                [4.734608638338736, 54.57650543915101],
                [9.451665078283622, 54.57650543915101],
                [9.451665078283622, 53.21804334226515],
              ],
            ],
          },
        },
      ],
    },
    defaultGeoJSONIntersection: {
      type: 'FeatureCollection',
      features: [
        {
          type: 'Feature',
          properties: {
            ...defaultIntersectionStyleProperties,
            selectionType: 'box',
          },
          geometry: {
            type: 'Polygon',
            coordinates: [
              [
                [4.734608638338736, 53.21804334226515],
                [7.181571844927457, 53.21804334226515],
                [7.191667, 53.3],
                [6.5, 53.666667],
                [6.500001364755953, 54.57650543915101],
                [4.734608638338736, 54.57650543915101],
                [4.734608638338736, 53.21804334226515],
              ],
            ],
          },
        },
      ],
    },
  });

  const {
    geoJSON: endGeoJSON,
    geoJSONIntersection: endGeoJSONIntersection,
    drawModes: endEditModes,
    activeTool: endActiveTool,
    changeActiveTool: endChangeActiveTool,
    getLayer: endGetLayer,
    deactivateTool: endDeactivateTool,
    setGeoJSONIntersectionBounds: endSetGeoJSONIntersectionBounds,
    setDrawModes: endSetEditModes,
  } = useMapDrawTool({
    defaultGeoJSONIntersectionBounds: intersectionShapeNL,
    defaultDrawModes: getEditModes(featurePropertiesEnd),
    defaultGeoJSONIntersectionProperties: featurePropertiesEnd,
    defaultGeoJSON: {
      type: 'FeatureCollection',
      features: [
        {
          type: 'Feature',
          properties: {
            ...featurePropertiesEnd,
            selectionType: 'poly',
          },
          geometry: {
            type: 'Polygon',
            coordinates: [
              [
                [2.3408785046353606, 52.57033268070152],
                [8.870833501723242, 51.74975701659361],
                [-0.088053542798945, 50.72496422996941],
                [2.3408785046353606, 52.57033268070152],
              ],
            ],
          },
        },
      ],
    },
    defaultGeoJSONIntersection: {
      type: 'FeatureCollection',
      features: [
        {
          type: 'Feature',
          properties: {
            ...featurePropertiesEnd,
            selectionType: 'poly',
          },
          geometry: {
            type: 'Polygon',
            coordinates: [
              [
                [2.000002, 51.500002],
                [3.370001, 51.369722],
                [3.362223, 51.320002],
                [3.36389, 51.313608],
                [3.373613, 51.309999],
                [3.952501, 51.214441],
                [4.397501, 51.452776],
                [5.078611, 51.391665],
                [5.249839243115953, 51.335557264081125],
                [6.173465572431757, 51.44120940537001],
                [5.94639, 51.811663],
                [6.405001, 51.830828],
                [6.708618091513499, 52.02146813052448],
                [2.8253551673020594, 52.50945174669971],
                [2.000002, 51.500002],
              ],
            ],
          },
        },
      ],
    },
  });

  const layers = [
    startGetLayer('geoJSONIntersectionBounds', 'example-static-layer'),
    endGetLayer('geoJSON', 'example-draw-layer-end'),
    endGetLayer('geoJSONIntersection', 'example-intersection-layer-end'),
    startGetLayer('geoJSON', 'example-draw-layer'),
    startGetLayer('geoJSONIntersection', 'example-intersection-layer'),
  ];

  const onExitDrawMode = (reason: DrawModeExitCallback): void => {
    if (reason === 'escaped') {
      startDeactivateTool();
      endDeactivateTool();
    }
  };

  const onChangeIntersection = (
    newGeoJSON: GeoJSON.FeatureCollection,
  ): void => {
    // deactivate tool modes
    startDeactivateTool();
    endDeactivateTool();
    // update intersection bounds for both shapes
    startSetGeoJSONIntersectionBounds(newGeoJSON);
    endSetGeoJSONIntersectionBounds(newGeoJSON);
    // update controls with new shape for fir
    startSetEditModes(updateEditModeButtonsWithFir(startEditModes, newGeoJSON));
    endSetEditModes(updateEditModeButtonsWithFir(endEditModes, newGeoJSON));
  };

  const onChangeStartTool = (mode: DrawMode): void => {
    endDeactivateTool(); // deactivate end tool
    startChangeActiveTool(mode);
  };

  const onChangeEndTool = (mode: DrawMode): void => {
    startDeactivateTool(); // deactivate start tool
    endChangeActiveTool(mode);
  };

  return (
    <ThemeProvider>
      <StoryLayout layers={layers} onExitDrawMode={onExitDrawMode}>
        <>
          <IntersectionSelect
            intersections={[
              { title: 'NL', geojson: intersectionShapeNL },
              { title: 'BE', geojson: intersectionShapeBE },
            ]}
            onChangeIntersection={onChangeIntersection}
          />

          <Grid item xs={12}>
            <Typography sx={{ margin: 1, marginLeft: 0 }}>
              Start geoJSON
            </Typography>
            {startEditModes.map((mode) => (
              <ToolButton
                key={mode.drawModeId}
                mode={mode}
                onClick={(): void => onChangeStartTool(mode)}
                isSelected={startActiveTool === mode.drawModeId}
                icon={getToolIcon(mode.drawModeId)}
              />
            ))}
            <Divider sx={{ marginBottom: 1 }} />

            <GeoJSONTextField
              title="start geoJSON result"
              geoJSON={startGeoJSON}
              sx={{ maxHeight: 200, height: 200, overflow: 'hidden' }}
              maxRows={7}
            />

            <GeoJSONTextField
              title="start intersection geoJSON result"
              geoJSON={startGeoJSONIntersection as GeoJSON.FeatureCollection}
              sx={{ maxHeight: 200, height: 200, overflow: 'hidden' }}
              maxRows={7}
            />
          </Grid>

          <Grid item xs={12}>
            <Typography sx={{ margin: 1, marginLeft: 0 }}>
              End geoJSON
            </Typography>
            {endEditModes.map((mode) => (
              <ToolButton
                key={mode.drawModeId}
                mode={mode}
                onClick={(): void => onChangeEndTool(mode)}
                isSelected={
                  mode.isSelectable && endActiveTool === mode.drawModeId
                }
                icon={getToolIcon(mode.drawModeId)}
              />
            ))}

            <Divider sx={{ marginBottom: 1 }} />

            <GeoJSONTextField
              title="end geoJSON result"
              geoJSON={endGeoJSON}
              sx={{ maxHeight: 200, height: 200, overflow: 'hidden' }}
              maxRows={7}
            />

            <GeoJSONTextField
              title="end intersection geoJSON result"
              geoJSON={endGeoJSONIntersection as GeoJSON.FeatureCollection}
              sx={{ maxHeight: 200, height: 200, overflow: 'hidden' }}
              maxRows={7}
            />
          </Grid>
        </>
      </StoryLayout>
    </ThemeProvider>
  );
};
