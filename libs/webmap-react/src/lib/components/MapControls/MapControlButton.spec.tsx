/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import {
  render,
  fireEvent,
  waitFor,
  screen,
  waitForElementToBeRemoved,
} from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import { ThemeWrapper } from '@opengeoweb/theme';
import { MapControlButton } from './MapControlButton';

describe('src/components/MapControls/MapControlButton', () => {
  const user = userEvent.setup();

  it('should work with default props', async () => {
    const props = {
      title: 'test tooltip',
      onClick: jest.fn(),
    };

    render(
      <ThemeWrapper>
        <MapControlButton {...props}>
          <span>button content</span>
        </MapControlButton>
      </ThemeWrapper>,
    );
    const button = screen.getByRole('button')!;

    fireEvent.click(button);

    expect(props.onClick).toHaveBeenCalled();
    expect(screen.getByLabelText(props.title)).toBeTruthy();
  });

  it('should show/hide correct tooltip on hover', async () => {
    const props = {
      title: 'test tooltip',
      onClick: jest.fn(),
    };

    render(
      <ThemeWrapper>
        <MapControlButton {...props}>
          <span data-testid="icon">button content</span>
        </MapControlButton>
      </ThemeWrapper>,
    );

    expect(screen.queryByRole('tooltip')).toBeFalsy();
    expect(screen.queryByText(props.title)).toBeFalsy();

    const button = screen.getByTestId('icon');

    await user.hover(button);
    await waitFor(() => {
      expect(screen.getByRole('tooltip')).toBeTruthy();
    });
    expect(screen.getByText(props.title)).toBeTruthy();

    await user.unhover(button);
    await waitForElementToBeRemoved(() => screen.queryByRole('tooltip'), {
      timeout: 3000,
    });
    expect(screen.queryByText(props.title)).toBeFalsy();
  });
});
