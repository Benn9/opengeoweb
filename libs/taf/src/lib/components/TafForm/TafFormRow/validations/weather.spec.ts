/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */

import {
  invalidWeatherBRVisibility,
  invalidWeatherCAVOKMessage,
  invalidWeatherCodeMessage,
  invalidWeatherFGVisibility,
  invalidWeatherMessage,
  invalidWeatherNonEmptyNSWGroups,
  invalidWeatherNonUniqueValue,
  invalidWeatherNSWMessage,
  invalidWeatherVisibilityBelow5000,
  invalidWeatherWithPreviousEmptyWeather,
  parseWeatherToObject,
  validateSecondWeatherField,
  validateThirdWeatherField,
  validateWeatherField,
} from './weather';

describe('TafForm/TafFormRow/validations/weather', () => {
  describe('validateWeatherField', () => {
    it('should accept empty strings', () => {
      expect(validateWeatherField('', '')).toEqual(true);
      expect(validateWeatherField(' ', ' ')).toEqual(true);
    });
    it('should accept NSW if passed true or nothing', () => {
      // uppercase
      expect(validateWeatherField('NSW', '')).toEqual(true);
      expect(validateWeatherField(' NSW', '')).toEqual(true);
      expect(validateWeatherField('NSW', '', true)).toEqual(true);
      expect(validateWeatherField('NSW', '', false)).toEqual(
        invalidWeatherNSWMessage,
      );
      expect(validateWeatherField(' NSW', '', false)).toEqual(
        invalidWeatherNSWMessage,
      );
      // lowercase
      expect(validateWeatherField('nsw', '')).toEqual(true);
      expect(validateWeatherField(' nsw', '')).toEqual(true);
      expect(validateWeatherField('nsw', '', true)).toEqual(true);
      expect(validateWeatherField('nsw', '', false)).toEqual(
        invalidWeatherNSWMessage,
      );
      expect(validateWeatherField(' nsw', '', false)).toEqual(
        invalidWeatherNSWMessage,
      );
    });

    it('should accept WeatherPhenomena', () => {
      // uppercase
      expect(validateWeatherField('-TSSNRA', '')).toEqual(true);
      expect(validateWeatherField('-RADZPL', '')).toEqual(true);
      expect(validateWeatherField('+SNDZ', '')).toEqual(true);
      expect(validateWeatherField('SHRAGR', '')).toEqual(true);
      expect(validateWeatherField('TSSNGSRA', '')).toEqual(true);
      expect(validateWeatherField(' SHRAGR', '')).toEqual(true);
      expect(validateWeatherField('TSSNGS ', '')).toEqual(true);
      // lowercase
      expect(validateWeatherField('-tssnra', '')).toEqual(true);
      expect(validateWeatherField('-radzpl', '')).toEqual(true);
      expect(validateWeatherField('+sndz', '')).toEqual(true);
      expect(validateWeatherField('shragr', '')).toEqual(true);
      expect(validateWeatherField('tssngsra', '')).toEqual(true);
      expect(validateWeatherField(' shragr', '')).toEqual(true);
      expect(validateWeatherField('tssngs ', '')).toEqual(true);
    });

    it('should not accept anything else (if NSW not allowed should only mention weather code in error message)', () => {
      expect(validateWeatherField('5', '', false)).toEqual(
        invalidWeatherCodeMessage,
      );
      // uppercase
      expect(validateWeatherField('-RZPL', '', false)).toEqual(
        invalidWeatherCodeMessage,
      );
      expect(validateWeatherField('FAKE INPUT', '', false)).toEqual(
        invalidWeatherCodeMessage,
      );

      expect(validateWeatherField('SHRAGRSHRAGR', '', false)).toEqual(
        invalidWeatherCodeMessage,
      );
      expect(validateWeatherField('TNGSRA', '', false)).toEqual(
        invalidWeatherCodeMessage,
      );
      // lowercase
      expect(validateWeatherField('-rzpl', '', false)).toEqual(
        invalidWeatherCodeMessage,
      );
      expect(validateWeatherField('fake input', '', false)).toEqual(
        invalidWeatherCodeMessage,
      );

      expect(validateWeatherField('shragrshragr', '', false)).toEqual(
        invalidWeatherCodeMessage,
      );
      expect(validateWeatherField('tngsra', '', false)).toEqual(
        invalidWeatherCodeMessage,
      );
    });

    it('should not accept anything else (if NSW allowed, should mention in error message)', () => {
      expect(validateWeatherField('5', '')).toEqual(invalidWeatherMessage);
      // uppercase
      expect(validateWeatherField('-RZPL', '')).toEqual(invalidWeatherMessage);
      expect(validateWeatherField('FAKE INPUT', '')).toEqual(
        invalidWeatherMessage,
      );
      expect(validateWeatherField('FAKE INPUT', '', true)).toEqual(
        invalidWeatherMessage,
      );
      expect(validateWeatherField('SHRAGRSHRAGR', '')).toEqual(
        invalidWeatherMessage,
      );
      expect(validateWeatherField('TNGSRA', '')).toEqual(invalidWeatherMessage);
      expect(validateWeatherField('TNGSRA', '', true)).toEqual(
        invalidWeatherMessage,
      );
      // lowercase
      expect(validateWeatherField('-rzpl', '')).toEqual(invalidWeatherMessage);
      expect(validateWeatherField('fake input', '')).toEqual(
        invalidWeatherMessage,
      );
      expect(validateWeatherField('fake input', '', true)).toEqual(
        invalidWeatherMessage,
      );
      expect(validateWeatherField('shragrshragr', '')).toEqual(
        invalidWeatherMessage,
      );
      expect(validateWeatherField('tngsra', '')).toEqual(invalidWeatherMessage);
      expect(validateWeatherField('tngsra', '', true)).toEqual(
        invalidWeatherMessage,
      );
    });

    it('should not accept anything if visibility is CAVOK', () => {
      // uppercase
      expect(validateWeatherField('RA', 'CAVOK')).toEqual(
        invalidWeatherCAVOKMessage,
      );
      expect(validateWeatherField('NSW', 'CAVOK')).toEqual(
        invalidWeatherCAVOKMessage,
      );
      expect(validateWeatherField('', 'CAVOK')).toEqual(true);
      expect(validateWeatherField('RA', '')).toEqual(true);
      expect(validateWeatherField('RA', '1000')).toEqual(true);
      // lowercase
      expect(validateWeatherField('ra', 'cavok')).toEqual(
        invalidWeatherCAVOKMessage,
      );
      expect(validateWeatherField('nsw', 'cavok')).toEqual(
        invalidWeatherCAVOKMessage,
      );
      expect(validateWeatherField('', 'cavok')).toEqual(true);
      expect(validateWeatherField('ra', '')).toEqual(true);
      expect(validateWeatherField('ra', '1000')).toEqual(true);
    });

    it('should validate the combination of weather code and visibility range', () => {
      // uppercase
      expect(validateWeatherField('FU', '5000')).toEqual(true);
      expect(validateWeatherField('DU', '4000')).toEqual(true);
      expect(validateWeatherField('FU', '6000')).toEqual(
        invalidWeatherVisibilityBelow5000,
      );
      expect(validateWeatherField('DU', '6000')).toEqual(
        invalidWeatherVisibilityBelow5000,
      );
      expect(validateWeatherField('HZ', '7000')).toEqual(
        invalidWeatherVisibilityBelow5000,
      );
      expect(validateWeatherField('DRDU', '8000')).toEqual(
        invalidWeatherVisibilityBelow5000,
      );
      expect(validateWeatherField('BLDU', '6000')).toEqual(
        invalidWeatherVisibilityBelow5000,
      );
      expect(validateWeatherField('BR', '1000')).toEqual(true);
      expect(validateWeatherField('BR', '5000')).toEqual(true);
      expect(validateWeatherField('BR', '3000')).toEqual(true);
      expect(validateWeatherField('BR', '900')).toEqual(
        invalidWeatherBRVisibility,
      );
      expect(validateWeatherField('BR', '6000')).toEqual(
        invalidWeatherBRVisibility,
      );
      expect(validateWeatherField('FG', '900')).toEqual(true);
      expect(validateWeatherField('FG', '1000')).toEqual(
        invalidWeatherFGVisibility,
      );
      expect(validateWeatherField('RA', '6000')).toEqual(true);
      expect(validateWeatherField('FU', '')).toEqual(true);
      expect(validateWeatherField('FU', '0000')).toEqual(true);
      // lowercase
      expect(validateWeatherField('fu', '5000')).toEqual(true);
      expect(validateWeatherField('du', '4000')).toEqual(true);
      expect(validateWeatherField('fu', '6000')).toEqual(
        invalidWeatherVisibilityBelow5000,
      );
      expect(validateWeatherField('du', '6000')).toEqual(
        invalidWeatherVisibilityBelow5000,
      );
      expect(validateWeatherField('HZ', '7000')).toEqual(
        invalidWeatherVisibilityBelow5000,
      );
      expect(validateWeatherField('drdu', '8000')).toEqual(
        invalidWeatherVisibilityBelow5000,
      );
      expect(validateWeatherField('bldu', '6000')).toEqual(
        invalidWeatherVisibilityBelow5000,
      );
      expect(validateWeatherField('br', '1000')).toEqual(true);
      expect(validateWeatherField('br', '5000')).toEqual(true);
      expect(validateWeatherField('br', '3000')).toEqual(true);
      expect(validateWeatherField('br', '900')).toEqual(
        invalidWeatherBRVisibility,
      );
      expect(validateWeatherField('br', '6000')).toEqual(
        invalidWeatherBRVisibility,
      );
      expect(validateWeatherField('fg', '900')).toEqual(true);
      expect(validateWeatherField('fg', '1000')).toEqual(
        invalidWeatherFGVisibility,
      );
      expect(validateWeatherField('ra', '6000')).toEqual(true);
      expect(validateWeatherField('fu', '')).toEqual(true);
      expect(validateWeatherField('fu', '0000')).toEqual(true);
    });
  });

  describe('validateSecondWeatherField', () => {
    it('should return error for second weather field when first weather field empty', () => {
      // uppercase
      expect(validateSecondWeatherField('TSSNRA', '', '')).toEqual(true);
      expect(validateSecondWeatherField('', '', '')).toEqual(true);
      expect(validateSecondWeatherField('', '  ', '')).toEqual(true);
      expect(validateSecondWeatherField('TSSNRA', 'RADZPL', '')).toEqual(true);
      expect(validateSecondWeatherField('TSSNRA ', 'TSSNGS ', '')).toEqual(
        true,
      );
      expect(validateSecondWeatherField('', 'RA', '')).toEqual(
        invalidWeatherWithPreviousEmptyWeather,
      );
      expect(validateSecondWeatherField(' ', 'RA ', '')).toEqual(
        invalidWeatherWithPreviousEmptyWeather,
      );
      // lowercase
      expect(validateSecondWeatherField('tssnra', '', '')).toEqual(true);
      expect(validateSecondWeatherField('', '', '')).toEqual(true);
      expect(validateSecondWeatherField('', '  ', '')).toEqual(true);
      expect(validateSecondWeatherField('tssnra', 'radzpl', '')).toEqual(true);
      expect(validateSecondWeatherField('tssnra ', 'tssngs ', '')).toEqual(
        true,
      );
      expect(validateSecondWeatherField('', 'ra', '')).toEqual(
        invalidWeatherWithPreviousEmptyWeather,
      );
      expect(validateSecondWeatherField(' ', 'ra ', '')).toEqual(
        invalidWeatherWithPreviousEmptyWeather,
      );
    });
    it('should return error for second weather field if NSW passed', () => {
      expect(validateSecondWeatherField('TSSNRA', 'NSW', '')).toEqual(
        invalidWeatherNSWMessage,
      );
      expect(validateSecondWeatherField('tssnra', 'nsw', '')).toEqual(
        invalidWeatherNSWMessage,
      );
    });

    it('should return error for second weather field when first weather field contains same', () => {
      // uppercase
      expect(validateSecondWeatherField('TSSNRA', 'RADZPL', '')).toEqual(true);
      expect(validateSecondWeatherField('TSSNRA ', 'TSSNGS ', '')).toEqual(
        true,
      );
      expect(validateSecondWeatherField('TSSNRA', 'TSSNRA', '')).toEqual(
        invalidWeatherNonUniqueValue,
      );
      expect(validateSecondWeatherField('RA ', ' RA ', '')).toEqual(
        invalidWeatherNonUniqueValue,
      );
      // lowercase
      expect(validateSecondWeatherField('tssnra', 'radzpl', '')).toEqual(true);
      expect(validateSecondWeatherField('tssnra ', 'tssngs ', '')).toEqual(
        true,
      );
      expect(validateSecondWeatherField('tssnra', 'tssnra', '')).toEqual(
        invalidWeatherNonUniqueValue,
      );
      expect(validateSecondWeatherField('ra ', ' ra ', '')).toEqual(
        invalidWeatherNonUniqueValue,
      );
    });
    it('should return error for second weather field when first weather field contains NSW', () => {
      // uppercase
      expect(validateSecondWeatherField('NSW', '', '')).toEqual(true);
      expect(validateSecondWeatherField('NSW ', ' ', '')).toEqual(true);
      expect(validateSecondWeatherField('NSW', 'TSSNRA', '')).toEqual(
        invalidWeatherNonEmptyNSWGroups,
      );
      expect(validateSecondWeatherField('NSW ', ' RA ', '')).toEqual(
        invalidWeatherNonEmptyNSWGroups,
      );
      // lowercase
      expect(validateSecondWeatherField('nsw', '', '')).toEqual(true);
      expect(validateSecondWeatherField('nsw ', ' ', '')).toEqual(true);
      expect(validateSecondWeatherField('nsw', 'tssnra', '')).toEqual(
        invalidWeatherNonEmptyNSWGroups,
      );
      expect(validateSecondWeatherField('nsw ', ' ra ', '')).toEqual(
        invalidWeatherNonEmptyNSWGroups,
      );
    });
    it('should not accept anything if visibility is CAVOK', () => {
      // uppercase
      expect(validateSecondWeatherField('RA', 'SN', 'CAVOK')).toEqual(
        invalidWeatherCAVOKMessage,
      );
      expect(validateSecondWeatherField('', '', 'CAVOK')).toEqual(true);
      expect(validateSecondWeatherField('RA', 'SN', '')).toEqual(true);
      expect(validateSecondWeatherField('RA', 'SN', '1000')).toEqual(true);
      // lowercase
      expect(validateSecondWeatherField('ra', 'sn', 'CAVOK')).toEqual(
        invalidWeatherCAVOKMessage,
      );
      expect(validateSecondWeatherField('', '', 'cavok')).toEqual(true);
      expect(validateSecondWeatherField('ra', 'sn', '')).toEqual(true);
      expect(validateSecondWeatherField('ra', 'sn', '1000')).toEqual(true);
    });
  });

  describe('validateThirdWeatherField', () => {
    it('should return error for third weather field when first or second weather field empty', () => {
      // uppercase
      expect(
        validateThirdWeatherField('TSSNRA', 'RADZPL', 'TSSNGSRA', ''),
      ).toEqual(true);
      expect(
        validateThirdWeatherField('TSSNRA ', 'TSSNGS ', 'TSSNGSRA  ', ''),
      ).toEqual(true);
      expect(validateThirdWeatherField('', 'TSSNGS', 'RA', '')).toEqual(
        invalidWeatherWithPreviousEmptyWeather,
      );
      expect(validateThirdWeatherField('TSSNGSRA', ' ', 'RA ', '')).toEqual(
        invalidWeatherWithPreviousEmptyWeather,
      );
      expect(validateThirdWeatherField('', ' ', 'RA ', '')).toEqual(
        invalidWeatherWithPreviousEmptyWeather,
      );
      // lowercase
      expect(
        validateThirdWeatherField('tssnra', 'radzpl', 'tssngsra', ''),
      ).toEqual(true);
      expect(
        validateThirdWeatherField('tssnra ', 'tssngs ', 'tssngsra  ', ''),
      ).toEqual(true);
      expect(validateThirdWeatherField('', 'tssngs', 'ra', '')).toEqual(
        invalidWeatherWithPreviousEmptyWeather,
      );
      expect(validateThirdWeatherField('tssngsra', ' ', 'ra ', '')).toEqual(
        invalidWeatherWithPreviousEmptyWeather,
      );
      expect(validateThirdWeatherField('', ' ', 'ra ', '')).toEqual(
        invalidWeatherWithPreviousEmptyWeather,
      );
    });
    it('should return error for thrid weather field when first/second weather field contains same', () => {
      // uppercase
      expect(
        validateThirdWeatherField('TSSNRA', 'RADZPL', 'SHRAGR', ''),
      ).toEqual(true);
      expect(
        validateThirdWeatherField('TSSNRA ', 'TSSNGS ', 'SHRAGR   ', ''),
      ).toEqual(true);
      expect(
        validateThirdWeatherField('TSSNRA', 'TSSNRA', 'TSSNRA', ''),
      ).toEqual(invalidWeatherNonUniqueValue);
      expect(validateThirdWeatherField('RA ', ' RA ', '    RA', '')).toEqual(
        invalidWeatherNonUniqueValue,
      );
      // lowercase
      expect(
        validateThirdWeatherField('tssnra', 'radzpl', 'shragr', ''),
      ).toEqual(true);
      expect(
        validateThirdWeatherField('tssnra ', 'TSSNGS ', 'shragr   ', ''),
      ).toEqual(true);
      expect(
        validateThirdWeatherField('tssnra', 'tssnra', 'tssnra', ''),
      ).toEqual(invalidWeatherNonUniqueValue);
      expect(validateThirdWeatherField('ra ', ' ra ', '    ra', '')).toEqual(
        invalidWeatherNonUniqueValue,
      );
    });
    it('should return error for third weather field if NSW passed', () => {
      // uppercase
      expect(validateThirdWeatherField('TSSNRA', 'RADZPL', 'NSW', '')).toEqual(
        invalidWeatherNSWMessage,
      );
      // lowercase
      expect(validateThirdWeatherField('tssnra', 'radzpl', 'nsw', '')).toEqual(
        invalidWeatherNSWMessage,
      );
    });
    it('should return error for third weather field when first weather field contains NSW', () => {
      // upercase
      expect(validateThirdWeatherField('NSW', '', '', '')).toEqual(true);
      expect(validateThirdWeatherField('NSW ', ' ', '   ', '')).toEqual(true);
      expect(validateThirdWeatherField('NSW', 'RADZPL', 'TSSNRA', '')).toEqual(
        invalidWeatherNonEmptyNSWGroups,
      );
      expect(validateThirdWeatherField('NSW ', '', ' RA ', '')).toEqual(
        invalidWeatherNonEmptyNSWGroups,
      );
      // lowercase
      expect(validateThirdWeatherField('nsw', '', '', '')).toEqual(true);
      expect(validateThirdWeatherField('nsw ', ' ', '   ', '')).toEqual(true);
      expect(validateThirdWeatherField('nsw', 'radzpl', 'tssnra', '')).toEqual(
        invalidWeatherNonEmptyNSWGroups,
      );
      expect(validateThirdWeatherField('nsw ', '', ' ra ', '')).toEqual(
        invalidWeatherNonEmptyNSWGroups,
      );
    });
    it('should not accept anything if visibility is CAVOK', () => {
      // uppercase
      expect(validateThirdWeatherField('RA', 'SN', 'DZ', 'CAVOK')).toEqual(
        invalidWeatherCAVOKMessage,
      );
      expect(validateThirdWeatherField('', '', '', 'CAVOK')).toEqual(true);
      expect(validateThirdWeatherField('RA', 'SN', 'DZ', '')).toEqual(true);
      expect(validateThirdWeatherField('RA', 'SN', 'DZ', '1000')).toEqual(true);
      // lowercase
      expect(validateThirdWeatherField('ra', 'sn', 'dz', 'cavok')).toEqual(
        invalidWeatherCAVOKMessage,
      );
      expect(validateThirdWeatherField('', '', '', 'cavok')).toEqual(true);
      expect(validateThirdWeatherField('ra', 'sn', 'dz', '')).toEqual(true);
      expect(validateThirdWeatherField('ra', 'sn', 'dz', '1000')).toEqual(true);
    });
  });

  describe('parseWeatherToObject', () => {
    it('should trim the weather strings - nothing more', () => {
      expect(parseWeatherToObject({ weather1: 'NSW' })).toEqual({
        weather1: 'NSW',
      });
      expect(parseWeatherToObject({ weather1: 'NSW' })).toEqual({
        weather1: 'NSW',
      });
      expect(
        parseWeatherToObject({
          weather1: 'MIFG',
          weather2: '',
          weather3: '',
        }),
      ).toEqual({
        weather1: 'MIFG',
      });
      expect(
        parseWeatherToObject({
          weather1: '  MIFG',
          weather2: '+SHRA  ',
          weather3: '',
        }),
      ).toEqual({
        weather1: 'MIFG',
        weather2: '+SHRA',
      });
      expect(
        parseWeatherToObject({
          weather1: '-RADZPL',
          weather2: '+SHRA',
          weather3: 'TSSNGSRA ',
        }),
      ).toEqual({
        weather1: '-RADZPL',
        weather2: '+SHRA',
        weather3: 'TSSNGSRA',
      });
    });
  });
});
