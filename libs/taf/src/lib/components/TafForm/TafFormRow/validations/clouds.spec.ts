/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */

import {
  invalidCloudMessage,
  invalidCLoudTypeMessage,
  invalidCloudHeightMessage,
  invalidCloudHeightStepsMessage,
  invalidCloudCoverageMessage,
  validateFirstCloud,
  invalidHeightTypeCombinationMessage,
  invalidVerticalVisibilityMessage,
  validateSecondCloud,
  invalidCloudHeightComparedToPreviousMessage,
  invalidCloud2CoverageTypeCombinationMessage,
  invalidCloud2WithVerticalVisibilityMessage,
  validateThirdCloud,
  invalidCloud3CoverageTypeCombinationMessage,
  invalidCloudWithVerticalVisibilityMessage,
  invalidCloudWithPreviousEmptyCloud,
  invalidCloudWithNSCMessage,
  invalidCloud1Message,
  parseCloudsToObject,
  formatCloudsToString,
  validateFourthCloud,
  invalidCloudWithPreviousType,
  invalidCloudCAVOKMessage,
  invalidVerticalVisibilityHeightMessage,
  invalidVerticalVisibilityHeightFGMessage,
  invalidCloud4TypeMissingMessage,
} from './clouds';

describe('TafForm/TafFormRow/validations/clouds', () => {
  describe('parseCloudsToObject', () => {
    it('should parse cloud strings to a cloud object', () => {
      expect(
        parseCloudsToObject({
          cloud1: 'FEW001',
          cloud2: 'SCT002',
          cloud3: 'BKN003',
          cloud4: 'OVC050TCU',
        }),
      ).toEqual({
        cloud1: {
          coverage: 'FEW',
          height: 1,
        },
        cloud2: {
          coverage: 'SCT',
          height: 2,
        },
        cloud3: {
          coverage: 'BKN',
          height: 3,
        },
        cloud4: {
          coverage: 'OVC',
          height: 50,
          type: 'TCU',
        },
      });
      expect(
        parseCloudsToObject({ cloud1: 'SCT001CB', cloud2: '   ', cloud3: '' }),
      ).toEqual({
        cloud1: {
          coverage: 'SCT',
          height: 1,
          type: 'CB',
        },
      });
      expect(parseCloudsToObject({ cloud1: 'NSC' })).toEqual({
        cloud1: 'NSC',
      });
      expect(
        parseCloudsToObject({ cloud1: 'VV005', cloud2: 'OVC050TCU' }),
      ).toEqual({
        cloud1: { verticalVisibility: 5 },
        cloud2: { coverage: 'OVC', height: 50, type: 'TCU' },
      });
    });
    it('should trim all values', () => {
      expect(
        parseCloudsToObject({
          cloud1: 'FEW001 ',
          cloud2: ' SCT002',
          cloud3: 'BKN003 ',
          cloud4: 'OVC050TCU  ',
        }),
      ).toEqual({
        cloud1: {
          coverage: 'FEW',
          height: 1,
        },
        cloud2: {
          coverage: 'SCT',
          height: 2,
        },
        cloud3: {
          coverage: 'BKN',
          height: 3,
        },
        cloud4: {
          coverage: 'OVC',
          height: 50,
          type: 'TCU',
        },
      });
      expect(
        parseCloudsToObject({
          cloud1: 'SCT001CB ',
          cloud2: '   ',
          cloud3: ' ',
        }),
      ).toEqual({
        cloud1: {
          coverage: 'SCT',
          height: 1,
          type: 'CB',
        },
      });
      expect(parseCloudsToObject({ cloud1: 'NSC' })).toEqual({
        cloud1: 'NSC',
      });
      expect(
        parseCloudsToObject({ cloud1: 'VV005  ', cloud2: ' OVC050TCU' }),
      ).toEqual({
        cloud1: { verticalVisibility: 5 },
        cloud2: { coverage: 'OVC', height: 50, type: 'TCU' },
      });
    });
  });
  describe('formatCloudToString', () => {
    it('should format cloud object to cloud strings', () => {
      expect(
        formatCloudsToString({
          cloud1: {
            coverage: 'SCT',
            height: 1,
            type: 'CB',
          },
        }),
      ).toEqual({ cloud1: 'SCT001CB' });
      expect(
        formatCloudsToString({
          cloud1: {
            coverage: 'FEW',
            height: 1,
          },
          cloud2: {
            coverage: 'SCT',
            height: 2,
          },
          cloud3: {
            coverage: 'BKN',
            height: 3,
          },
          cloud4: {
            coverage: 'OVC',
            height: 50,
            type: 'TCU',
          },
        }),
      ).toEqual({
        cloud1: 'FEW001',
        cloud2: 'SCT002',
        cloud3: 'BKN003',
        cloud4: 'OVC050TCU',
      });
      expect(formatCloudsToString({ cloud1: 'NSC' })).toEqual({
        cloud1: 'NSC',
      });
      expect(
        formatCloudsToString({
          cloud1: { verticalVisibility: 5 },
          cloud2: { coverage: 'OVC', height: 50, type: 'TCU' },
        }),
      ).toEqual({ cloud1: 'VV005', cloud2: 'OVC050TCU' });
      expect(
        formatCloudsToString({
          cloud1: { verticalVisibility: 0 },
        }),
      ).toEqual({ cloud1: 'VV000' });
      // It should not crash on empty height
      expect(
        formatCloudsToString({
          cloud1: {
            coverage: 'SCT',
            type: 'CB',
            height: null,
          },
        }),
      ).toEqual({ cloud1: 'SCT000CB' });
    });
  });
  describe('validateFirstCloud', () => {
    it('should accept empty strings or NSC', () => {
      expect(validateFirstCloud('', '', '', '', '')).toEqual(true);
      expect(validateFirstCloud('  ', '', '', '', '')).toEqual(true);
      expect(validateFirstCloud('NSC', '', '', '', '')).toEqual(true);
      expect(validateFirstCloud('nsc', '', '', '', '')).toEqual(true);
    });
    it('should not accept less than 6 or more than 9 characters', () => {
      expect(validateFirstCloud('12345', '', '', '', '')).toEqual(
        invalidCloud1Message,
      );
      expect(validateFirstCloud('1234567890', '', '', '', '')).toEqual(
        invalidCloud1Message,
      );
    });
    it('should not accept invalid characters', () => {
      expect(validateFirstCloud('A,.DFG', '', '', '', '')).toEqual(
        invalidCloud1Message,
      );
      expect(validateFirstCloud('a,.dfg', '', '', '', '')).toEqual(
        invalidCloud1Message,
      );
    });
    it('should only accept FEW SCT BKN OVC as coverage', () => {
      // uppercase
      expect(validateFirstCloud('FEW001', '', '', '', '')).toEqual(true);
      expect(validateFirstCloud(' FEW001 ', '', '', '', '')).toEqual(true);
      expect(validateFirstCloud('SCT001', '', '', '', '')).toEqual(true);
      expect(validateFirstCloud('BKN001', '', '', '', '')).toEqual(true);
      expect(validateFirstCloud('OVC001', '', '', '', '')).toEqual(true);
      expect(validateFirstCloud('TSC001', '', '', '', '')).toEqual(
        invalidCloudCoverageMessage,
      );
      expect(validateFirstCloud('NSC001', '', '', '', '')).toEqual(
        invalidCloudCoverageMessage,
      );
      // lowercase
      expect(validateFirstCloud('few001', '', '', '', '')).toEqual(true);
      expect(validateFirstCloud(' few001 ', '', '', '', '')).toEqual(true);
      expect(validateFirstCloud('sct001', '', '', '', '')).toEqual(true);
      expect(validateFirstCloud('bkn001', '', '', '', '')).toEqual(true);
      expect(validateFirstCloud('ovc001', '', '', '', '')).toEqual(true);
      expect(validateFirstCloud('tsc001', '', '', '', '')).toEqual(
        invalidCloudCoverageMessage,
      );
      expect(validateFirstCloud('nsc001', '', '', '', '')).toEqual(
        invalidCloudCoverageMessage,
      );
    });
    it('should check for the correct cloud height', () => {
      // uppercase
      expect(validateFirstCloud('FEW000', '', '', '', '')).toEqual(true);
      expect(validateFirstCloud('FEW001', '', '', '', '')).toEqual(true);
      expect(validateFirstCloud('FEW01F', '', '', '', '')).toEqual(
        invalidCloudHeightMessage,
      );
      expect(validateFirstCloud('FEW049', '', '', '', '')).toEqual(true);
      expect(validateFirstCloud('FEW050CB', '', '', '', '')).toEqual(true);
      expect(validateFirstCloud('FEW800CB', '', '', '', '')).toEqual(true);
      expect(validateFirstCloud('FEW990TCU', '', '', '', '')).toEqual(true);
      expect(validateFirstCloud('FEW051', '', '', '', '')).toEqual(
        invalidCloudHeightStepsMessage,
      );
      expect(validateFirstCloud('FEW999CB', '', '', '', '')).toEqual(
        invalidCloudHeightStepsMessage,
      );
      // lowercase
      expect(validateFirstCloud('few000', '', '', '', '')).toEqual(true);
      expect(validateFirstCloud('few001', '', '', '', '')).toEqual(true);
      expect(validateFirstCloud('few01f', '', '', '', '')).toEqual(
        invalidCloudHeightMessage,
      );
      expect(validateFirstCloud('few049', '', '', '', '')).toEqual(true);
      expect(validateFirstCloud('few050cb', '', '', '', '')).toEqual(true);
      expect(validateFirstCloud('few800cb', '', '', '', '')).toEqual(true);
      expect(validateFirstCloud('few990tcu', '', '', '', '')).toEqual(true);
      expect(validateFirstCloud('few051', '', '', '', '')).toEqual(
        invalidCloudHeightStepsMessage,
      );
      expect(validateFirstCloud('few999CB', '', '', '', '')).toEqual(
        invalidCloudHeightStepsMessage,
      );
    });
    it('should accept only TCU, CB or an empty string as type', () => {
      // uppercase
      expect(validateFirstCloud('FEW001', '', '', '', '')).toEqual(true);
      expect(validateFirstCloud('FEW050TCU', '', '', '', '')).toEqual(true);
      expect(validateFirstCloud('FEW050CB', '', '', '', '')).toEqual(true);
      expect(validateFirstCloud('FEW001CBU', '', '', '', '')).toEqual(
        invalidCLoudTypeMessage,
      );
      expect(validateFirstCloud('FEW001TC', '', '', '', '')).toEqual(
        invalidCLoudTypeMessage,
      );
      // lowercase
      expect(validateFirstCloud('few001', '', '', '', '')).toEqual(true);
      expect(validateFirstCloud('few050tcu', '', '', '', '')).toEqual(true);
      expect(validateFirstCloud('few050cb', '', '', '', '')).toEqual(true);
      expect(validateFirstCloud('few001cbu', '', '', '', '')).toEqual(
        invalidCLoudTypeMessage,
      );
      expect(validateFirstCloud('few001tc', '', '', '', '')).toEqual(
        invalidCLoudTypeMessage,
      );
    });
    it('should check for the height and type combination', () => {
      // uppercase
      expect(validateFirstCloud('FEW001', '', '', '', '')).toEqual(true);
      expect(validateFirstCloud('FEW049TCU', '', '', '', '')).toEqual(true);
      expect(validateFirstCloud('FEW050CB', '', '', '', '')).toEqual(true);
      expect(validateFirstCloud('FEW990TCU', '', '', '', '')).toEqual(true);
      expect(validateFirstCloud('FEW050', '', '', '', '')).toEqual(
        invalidHeightTypeCombinationMessage,
      );
      // lowercase
      expect(validateFirstCloud('few001', '', '', '', '')).toEqual(true);
      expect(validateFirstCloud('few049tcu', '', '', '', '')).toEqual(true);
      expect(validateFirstCloud('few050cb', '', '', '', '')).toEqual(true);
      expect(validateFirstCloud('few990tcu', '', '', '', '')).toEqual(true);
      expect(validateFirstCloud('few050', '', '', '', '')).toEqual(
        invalidHeightTypeCombinationMessage,
      );
    });
    it('should validate on vertical visibility', () => {
      // Only accept vertical visibilities of format VVXXX with XXX between 001 and 010
      // if one of the weather fields contains FG XXX should lie between 001 and 005
      // uppercase
      expect(validateFirstCloud('VV001', '', '', '', '')).toEqual(true);
      expect(validateFirstCloud('VV010', '', '', '', '')).toEqual(true);
      expect(validateFirstCloud('VV010', '', 'RA', 'DZ', '')).toEqual(true);
      expect(
        validateFirstCloud('VV007', '', 'RAPLSN', 'VCFG', 'SGRASN'),
      ).toEqual(true);

      expect(validateFirstCloud('VV', '', '', '', '')).toEqual(
        invalidVerticalVisibilityMessage,
      );
      expect(validateFirstCloud('VV01', '', '', '', '')).toEqual(
        invalidVerticalVisibilityMessage,
      );
      expect(validateFirstCloud('VVA01', '', '', '', '')).toEqual(
        invalidVerticalVisibilityMessage,
      );
      expect(validateFirstCloud('VV0100', '', '', '', '')).toEqual(
        invalidVerticalVisibilityMessage,
      );
      expect(validateFirstCloud('VV011', '', '', '', '')).toEqual(
        invalidVerticalVisibilityHeightMessage,
      );
      expect(validateFirstCloud('VV000', '', '', '', '')).toEqual(
        invalidVerticalVisibilityHeightMessage,
      );
      expect(validateFirstCloud('VV011', '', 'FG', '', '')).toEqual(
        invalidVerticalVisibilityHeightFGMessage,
      );
      expect(validateFirstCloud('VV007', '', 'RA', 'FG', '')).toEqual(
        invalidVerticalVisibilityHeightFGMessage,
      );
      expect(validateFirstCloud('VV000', '', '', '', 'FG')).toEqual(
        invalidVerticalVisibilityHeightFGMessage,
      );
      // lowercase
      expect(validateFirstCloud('vv001', '', '', '', '')).toEqual(true);
      expect(validateFirstCloud('vv010', '', '', '', '')).toEqual(true);
      expect(validateFirstCloud('vv010', '', 'ra', 'dz', '')).toEqual(true);
      expect(
        validateFirstCloud('vv007', '', 'raplsn', 'cvfg', 'sgrasn'),
      ).toEqual(true);

      expect(validateFirstCloud('vv', '', '', '', '')).toEqual(
        invalidVerticalVisibilityMessage,
      );
      expect(validateFirstCloud('vv01', '', '', '', '')).toEqual(
        invalidVerticalVisibilityMessage,
      );
      expect(validateFirstCloud('vvA01', '', '', '', '')).toEqual(
        invalidVerticalVisibilityMessage,
      );
      expect(validateFirstCloud('vv0100', '', '', '', '')).toEqual(
        invalidVerticalVisibilityMessage,
      );
      expect(validateFirstCloud('vv011', '', '', '', '')).toEqual(
        invalidVerticalVisibilityHeightMessage,
      );
      expect(validateFirstCloud('vv000', '', '', '', '')).toEqual(
        invalidVerticalVisibilityHeightMessage,
      );
      expect(validateFirstCloud('vv011', '', 'fg', '', '')).toEqual(
        invalidVerticalVisibilityHeightFGMessage,
      );
      expect(validateFirstCloud('vv007', '', 'ra', 'fg', '')).toEqual(
        invalidVerticalVisibilityHeightFGMessage,
      );
      expect(validateFirstCloud('vv000', '', '', '', 'fg')).toEqual(
        invalidVerticalVisibilityHeightFGMessage,
      );
    });
    it('should not accept anything if visibility is CAVOK', () => {
      // uppercase
      expect(validateFirstCloud('FEW001', 'CAVOK', '', '', '')).toEqual(
        invalidCloudCAVOKMessage,
      );
      expect(validateFirstCloud('', 'CAVOK', '', '', '')).toEqual(true);
      expect(validateFirstCloud('FEW001', '', '', '', '')).toEqual(true);
      expect(validateFirstCloud('FEW001', '1000', '', '', '')).toEqual(true);
      // lowercase
      expect(validateFirstCloud('few001', 'cavok', '', '', '')).toEqual(
        invalidCloudCAVOKMessage,
      );
      expect(validateFirstCloud('', 'cavok', '', '', '')).toEqual(true);
      expect(validateFirstCloud('few001', '', '', '', '')).toEqual(true);
      expect(validateFirstCloud('few001', '1000', '', '', '')).toEqual(true);
    });
  });
  describe('validateSecondCloud', () => {
    it('should accept empty strings', () => {
      expect(validateSecondCloud('', '', '')).toEqual(true);
      expect(validateSecondCloud('', '  ', '')).toEqual(true);
      expect(validateSecondCloud(' ', '  ', '')).toEqual(true);
    });
    it('should not accept NSC or VV', () => {
      // uppercase
      expect(validateSecondCloud('FEW000', 'NSC', '')).toEqual(
        invalidCloudMessage,
      );
      expect(validateSecondCloud('FEW000', 'VV001', '')).toEqual(
        invalidCloudMessage,
      );
      // lowercase
      expect(validateSecondCloud('few000', 'nsx', '')).toEqual(
        invalidCloudMessage,
      );
      expect(validateSecondCloud('few000', 'vv001', '')).toEqual(
        invalidCloudMessage,
      );
    });

    it('should check that cloud2 is empty when cloud1 is NSC', () => {
      // uppercase
      expect(validateSecondCloud('NSC', '', '')).toEqual(true);
      expect(validateSecondCloud('NSC', 'SCT000', '')).toEqual(
        invalidCloudWithNSCMessage,
      );
      expect(validateSecondCloud('NSC', 'FEW000', '')).toEqual(
        invalidCloudWithNSCMessage,
      );
      expect(validateSecondCloud('NSC ', 'FEW000', '')).toEqual(
        invalidCloudWithNSCMessage,
      );
      // lowercase
      expect(validateSecondCloud('nsc', '', '')).toEqual(true);
      expect(validateSecondCloud('nsc', 'sct000', '')).toEqual(
        invalidCloudWithNSCMessage,
      );
      expect(validateSecondCloud('nsc', 'few000', '')).toEqual(
        invalidCloudWithNSCMessage,
      );
      expect(validateSecondCloud('nsc ', 'few000', '')).toEqual(
        invalidCloudWithNSCMessage,
      );
    });

    it('should check that cloud1 is not empty', () => {
      // uppercase
      expect(validateSecondCloud('', 'SCT000', '')).toEqual(
        invalidCloudWithPreviousEmptyCloud,
      );
      expect(validateSecondCloud(' ', 'SCT000', '')).toEqual(
        invalidCloudWithPreviousEmptyCloud,
      );
      expect(validateSecondCloud('', 'FEW002', '')).toEqual(
        invalidCloudWithPreviousEmptyCloud,
      );
      // lowercase
      expect(validateSecondCloud('', 'sct000', '')).toEqual(
        invalidCloudWithPreviousEmptyCloud,
      );
      expect(validateSecondCloud(' ', 'sct000', '')).toEqual(
        invalidCloudWithPreviousEmptyCloud,
      );
      expect(validateSecondCloud('', 'few002', '')).toEqual(
        invalidCloudWithPreviousEmptyCloud,
      );
    });

    it('should not accept less than 6 or more than 9 characters', () => {
      expect(validateSecondCloud('FEW000', '12345', '')).toEqual(
        invalidCloudMessage,
      );
      expect(validateSecondCloud('FEW000', '1234567890', '')).toEqual(
        invalidCloudMessage,
      );
      expect(validateSecondCloud('few000', '12345', '')).toEqual(
        invalidCloudMessage,
      );
      expect(validateSecondCloud('few000', '1234567890', '')).toEqual(
        invalidCloudMessage,
      );
    });
    it('should not accept invalid characters', () => {
      expect(validateSecondCloud('FEW000', 'A,.DFG', '')).toEqual(
        invalidCloudMessage,
      );
      expect(validateSecondCloud('few000', 'a,.dfg', '')).toEqual(
        invalidCloudMessage,
      );
    });
    it('should only accept FEW SCT BKN OVC as coverage', () => {
      // uppercase
      expect(validateSecondCloud('FEW000', 'FEW050CB', '')).toEqual(true);
      expect(validateSecondCloud('FEW000', ' SCT002 ', '')).toEqual(true);
      expect(validateSecondCloud('FEW000', 'SCT002', '')).toEqual(true);
      expect(validateSecondCloud('FEW000', 'BKN002', '')).toEqual(true);
      expect(validateSecondCloud('FEW000', 'OVC002', '')).toEqual(true);
      expect(validateSecondCloud('FEW000', 'TSC002', '')).toEqual(
        invalidCloudCoverageMessage,
      );
      // lowercase
      expect(validateSecondCloud('few000', 'few050cb', '')).toEqual(true);
      expect(validateSecondCloud('few000', ' sct002 ', '')).toEqual(true);
      expect(validateSecondCloud('few000', 'sct002', '')).toEqual(true);
      expect(validateSecondCloud('few000', 'bkn002', '')).toEqual(true);
      expect(validateSecondCloud('few000', 'ovc002', '')).toEqual(true);
      expect(validateSecondCloud('few000', 'tsc002', '')).toEqual(
        invalidCloudCoverageMessage,
      );
    });
    it('should check for the correct cloud height', () => {
      // uppercase
      expect(validateSecondCloud('FEW000', 'SCT002', '')).toEqual(true);
      expect(validateSecondCloud('FEW000', 'SCT02F', '')).toEqual(
        invalidCloudHeightMessage,
      );
      expect(validateSecondCloud('FEW000', 'SCT049', '')).toEqual(true);
      expect(validateSecondCloud('FEW000', 'SCT050CB', '')).toEqual(true);
      expect(validateSecondCloud('FEW000', 'SCT800CB', '')).toEqual(true);
      expect(validateSecondCloud('FEW000', 'SCT990TCU', '')).toEqual(true);
      expect(validateSecondCloud('FEW000', 'SCT051', '')).toEqual(
        invalidCloudHeightStepsMessage,
      );
      expect(validateSecondCloud('FEW000', 'SCT999CB', '')).toEqual(
        invalidCloudHeightStepsMessage,
      );
      // lowercase
      expect(validateSecondCloud('few000', 'sct002', '')).toEqual(true);
      expect(validateSecondCloud('few000', 'sct02F', '')).toEqual(
        invalidCloudHeightMessage,
      );
      expect(validateSecondCloud('few000', 'sct049', '')).toEqual(true);
      expect(validateSecondCloud('few000', 'sct050cb', '')).toEqual(true);
      expect(validateSecondCloud('few000', 'sct800cb', '')).toEqual(true);
      expect(validateSecondCloud('few000', 'sct990tcu', '')).toEqual(true);
      expect(validateSecondCloud('few000', 'sct051', '')).toEqual(
        invalidCloudHeightStepsMessage,
      );
      expect(validateSecondCloud('few000', 'sct999cb', '')).toEqual(
        invalidCloudHeightStepsMessage,
      );
    });
    it('should accept only TCU, CB or an empty string as type', () => {
      // uppercase
      expect(validateSecondCloud('FEW000', 'SCT002', '')).toEqual(true);
      expect(validateSecondCloud('FEW000', 'SCT050TCU', '')).toEqual(true);
      expect(validateSecondCloud('FEW000', 'SCT300CB', '')).toEqual(true);
      expect(validateSecondCloud('FEW000', 'SCT060CBU', '')).toEqual(
        invalidCLoudTypeMessage,
      );
      expect(validateSecondCloud('FEW000', 'SCT070TC', '')).toEqual(
        invalidCLoudTypeMessage,
      );
      // lowercase
      expect(validateSecondCloud('few000', 'sct002', '')).toEqual(true);
      expect(validateSecondCloud('few000', 'sct050tcu', '')).toEqual(true);
      expect(validateSecondCloud('few000', 'sct300cb', '')).toEqual(true);
      expect(validateSecondCloud('few000', 'sct060cbu', '')).toEqual(
        invalidCLoudTypeMessage,
      );
      expect(validateSecondCloud('few000', 'sct070tc', '')).toEqual(
        invalidCLoudTypeMessage,
      );
    });
    it('should check for the height and type combination', () => {
      // uppercase
      expect(validateSecondCloud('FEW000', 'SCT002', '')).toEqual(true);
      expect(validateSecondCloud('FEW000', 'SCT049TCU', '')).toEqual(true);
      expect(validateSecondCloud('FEW000', 'SCT050CB', '')).toEqual(true);
      expect(validateSecondCloud('FEW000', 'SCT990TCU', '')).toEqual(true);
      expect(validateSecondCloud('FEW000', 'SCT060', '')).toEqual(
        invalidHeightTypeCombinationMessage,
      );
      expect(validateSecondCloud('FEW000', 'FEW090', '')).toEqual(
        invalidHeightTypeCombinationMessage,
      );
      // lowercase
      expect(validateSecondCloud('few000', 'sct002', '')).toEqual(true);
      expect(validateSecondCloud('few000', 'sct049tcu', '')).toEqual(true);
      expect(validateSecondCloud('few000', 'sct050cb', '')).toEqual(true);
      expect(validateSecondCloud('few000', 'sct990tcu', '')).toEqual(true);
      expect(validateSecondCloud('few000', 'sct060', '')).toEqual(
        invalidHeightTypeCombinationMessage,
      );
      expect(validateSecondCloud('few000', 'few090', '')).toEqual(
        invalidHeightTypeCombinationMessage,
      );
    });
    it('should check for the type and coverage combination', () => {
      // uppercase
      expect(validateSecondCloud('FEW000', 'FEW050CB', '')).toEqual(true);
      expect(validateSecondCloud('FEW000', 'FEW050TCU', '')).toEqual(true);
      expect(validateSecondCloud('FEW000', 'SCT002', '')).toEqual(true);
      expect(validateSecondCloud('FEW000', 'BKN002', '')).toEqual(true);
      expect(validateSecondCloud('FEW000', 'OVC002', '')).toEqual(true);
      // In case the first cloud group contains CB or TCU it should accept FEW without a type
      expect(validateSecondCloud('FEW000CB', 'FEW002', '')).toEqual(true);
      expect(validateSecondCloud('SCT010TCU', 'FEW030', '')).toEqual(true);

      expect(validateSecondCloud('FEW000', 'FEW002', '')).toEqual(
        invalidCloud2CoverageTypeCombinationMessage,
      );
      expect(validateSecondCloud('SCT000', 'FEW002', '')).toEqual(
        invalidCloud2CoverageTypeCombinationMessage,
      );
      // lowercase
      expect(validateSecondCloud('few000', 'few050cb', '')).toEqual(true);
      expect(validateSecondCloud('few000', 'few050tcu', '')).toEqual(true);
      expect(validateSecondCloud('few000', 'sct002', '')).toEqual(true);
      expect(validateSecondCloud('few000', 'bkn002', '')).toEqual(true);
      expect(validateSecondCloud('few000', 'ovc002', '')).toEqual(true);
      // In case the first cloud group contains CB or TCU it should accept FEW without a type
      expect(validateSecondCloud('few000CB', 'few002', '')).toEqual(true);
      expect(validateSecondCloud('sct010tcu', 'few030', '')).toEqual(true);

      expect(validateSecondCloud('few000', 'few002', '')).toEqual(
        invalidCloud2CoverageTypeCombinationMessage,
      );
      expect(validateSecondCloud('sct000', 'few002', '')).toEqual(
        invalidCloud2CoverageTypeCombinationMessage,
      );
    });

    it('should not compare cloud2 when cloud1 is invalid', () => {
      // uppercase
      expect(validateSecondCloud('SCT0010', 'SCT000', '')).toEqual(true);
      expect(validateSecondCloud('VV', 'SCT000', '')).toEqual(true);
      expect(validateSecondCloud('FEW001', 'SCT002', 'CAVOK')).toEqual(true);
      // lowercase
      expect(validateSecondCloud('sct0010', 'sct000', '')).toEqual(true);
      expect(validateSecondCloud('vv', 'sct000', '')).toEqual(true);
      expect(validateSecondCloud('few001', 'sct002', 'cavok')).toEqual(true);
    });
    it('should check that cloud2 has no type when cloud1 cloudgroups contains type', () => {
      // uppercase
      expect(validateSecondCloud('FEW000', 'SCT001', '')).toEqual(true);
      expect(validateSecondCloud('FEW000', 'SCT001CB', '')).toEqual(true);
      expect(validateSecondCloud('FEW000', 'FEW001CB', '')).toEqual(true);
      expect(validateSecondCloud('FEW000TCU', 'FEW001', '')).toEqual(true);
      expect(validateSecondCloud('FEW000TCU', 'SCT001CB', '')).toEqual(
        invalidCloudWithPreviousType,
      );
      expect(validateSecondCloud('FEW000CB', 'FEW001CB', '')).toEqual(
        invalidCloudWithPreviousType,
      );
      // lowercase
      expect(validateSecondCloud('few000', 'sct001', '')).toEqual(true);
      expect(validateSecondCloud('few000', 'sct001cb', '')).toEqual(true);
      expect(validateSecondCloud('few000', 'few001cb', '')).toEqual(true);
      expect(validateSecondCloud('few000tcu', 'few001', '')).toEqual(true);
      expect(validateSecondCloud('few000tcu', 'sct001cb', '')).toEqual(
        invalidCloudWithPreviousType,
      );
      expect(validateSecondCloud('few000cb', 'few001cb', '')).toEqual(
        invalidCloudWithPreviousType,
      );
    });
    it('should compare the height of cloud 2 with cloud 1', () => {
      // uppercase
      expect(validateSecondCloud('FEW001', 'SCT002', '')).toEqual(true);
      expect(validateSecondCloud('FEW001', 'SCT001', '')).toEqual(
        invalidCloudHeightComparedToPreviousMessage,
      );
      expect(validateSecondCloud('FEW003CB', 'FEW001', '')).toEqual(
        invalidCloudHeightComparedToPreviousMessage,
      );
      expect(validateSecondCloud('FEW002', 'SCT001', '')).toEqual(
        invalidCloudHeightComparedToPreviousMessage,
      );

      // lowercase
      expect(validateSecondCloud('few001', 'sct002', '')).toEqual(true);
      expect(validateSecondCloud('few001', 'sct001', '')).toEqual(
        invalidCloudHeightComparedToPreviousMessage,
      );
      expect(validateSecondCloud('few003CB', 'few001', '')).toEqual(
        invalidCloudHeightComparedToPreviousMessage,
      );
      expect(validateSecondCloud('few002', 'sct001', '')).toEqual(
        invalidCloudHeightComparedToPreviousMessage,
      );
    });
    it('should check that type is present when vertical visibility is given', () => {
      // uppercase
      expect(validateSecondCloud('VV001', 'few050CB', '')).toEqual(true);
      expect(validateSecondCloud('VV001', '', '')).toEqual(true);
      expect(validateSecondCloud('VV001', 'SCT002', '')).toEqual(
        invalidCloud2WithVerticalVisibilityMessage,
      );
      // lowercase
      expect(validateSecondCloud('vv001', 'few050cb', '')).toEqual(true);
      expect(validateSecondCloud('vv001', '', '')).toEqual(true);
      expect(validateSecondCloud('vv001', 'sct002', '')).toEqual(
        invalidCloud2WithVerticalVisibilityMessage,
      );
    });
  });

  describe('validateThirdCloud', () => {
    it('should accept empty strings', () => {
      expect(validateThirdCloud('', '', '', '')).toEqual(true);
      expect(validateThirdCloud('', '', '  ', '')).toEqual(true);
    });
    it('should not accept NSC or VV', () => {
      // uppercase
      expect(validateThirdCloud('FEW000', 'SCT001', 'NSC', '')).toEqual(
        invalidCloudMessage,
      );
      expect(validateThirdCloud('FEW000', 'SCT001', 'VV001', '')).toEqual(
        invalidCloudMessage,
      );
      // lowercase
      expect(validateThirdCloud('few000', 'sct001', 'nsc', '')).toEqual(
        invalidCloudMessage,
      );
      expect(validateThirdCloud('few000', 'sct001', 'vv001', '')).toEqual(
        invalidCloudMessage,
      );
    });

    it('should not accept less than 6 or more than 9 characters', () => {
      // uppercase
      expect(validateThirdCloud('FEW000', 'SCT001', '12345', '')).toEqual(
        invalidCloudMessage,
      );
      expect(validateThirdCloud('FEW000', 'SCT001', '1234567890', '')).toEqual(
        invalidCloudMessage,
      );
      // lowercase
      expect(validateThirdCloud('few000', 'sct001', '12345', '')).toEqual(
        invalidCloudMessage,
      );
      expect(validateThirdCloud('few000', 'sct001', '1234567890', '')).toEqual(
        invalidCloudMessage,
      );
    });
    it('should not accept invalid characters', () => {
      expect(validateThirdCloud('FEW000', 'SCT001', 'A,.DFG', '')).toEqual(
        invalidCloudMessage,
      );
      expect(validateThirdCloud('few000', 'sct001', 'a,.dfg', '')).toEqual(
        invalidCloudMessage,
      );
    });
    it('should only accept FEW SCT BKN OVC as coverage', () => {
      // uppercase
      expect(validateThirdCloud('FEW001', 'SCT010', 'FEW050CB', '')).toEqual(
        true,
      );
      expect(validateThirdCloud('FEW001', 'SCT010', ' SCT060TCU ', '')).toEqual(
        true,
      );
      expect(validateThirdCloud('FEW001', 'SCT010', 'SCT050CB', '')).toEqual(
        true,
      );
      expect(validateThirdCloud('FEW001', 'SCT010', 'BKN012', '')).toEqual(
        true,
      );
      expect(validateThirdCloud('FEW001', 'SCT010', 'OVC012', '')).toEqual(
        true,
      );
      expect(validateThirdCloud('FEW000', 'SCT001', 'TSC012', '')).toEqual(
        invalidCloudCoverageMessage,
      );
      // lowercase
      expect(validateThirdCloud('few001', 'sct010', 'few050CB', '')).toEqual(
        true,
      );
      expect(validateThirdCloud('few001', 'sct010', ' sct060TCU ', '')).toEqual(
        true,
      );
      expect(validateThirdCloud('few001', 'sct010', 'sct050CB', '')).toEqual(
        true,
      );
      expect(validateThirdCloud('few001', 'sct010', 'bkn012', '')).toEqual(
        true,
      );
      expect(validateThirdCloud('few001', 'sct010', 'ovc012', '')).toEqual(
        true,
      );
      expect(validateThirdCloud('few000', 'sct001', 'tsc012', '')).toEqual(
        invalidCloudCoverageMessage,
      );
    });
    it('should check for the correct cloud height', () => {
      // uppercase
      expect(validateThirdCloud('FEW000', 'SCT001', 'BKN002', '')).toEqual(
        true,
      );
      expect(validateThirdCloud('FEW000', 'SCT001', 'BKN02F', '')).toEqual(
        invalidCloudHeightMessage,
      );
      expect(validateThirdCloud('FEW000', 'SCT001', 'BKN049', '')).toEqual(
        true,
      );
      expect(validateThirdCloud('FEW000', 'SCT001', 'BKN050CB', '')).toEqual(
        true,
      );
      expect(validateThirdCloud('FEW000', 'SCT001', 'BKN800CB', '')).toEqual(
        true,
      );
      expect(validateThirdCloud('FEW000', 'SCT001', 'BKN990TCU', '')).toEqual(
        true,
      );
      expect(validateThirdCloud('FEW000', 'SCT001', 'BKN051', '')).toEqual(
        invalidCloudHeightStepsMessage,
      );
      expect(validateThirdCloud('FEW000', 'SCT001', 'BKN999CB', '')).toEqual(
        invalidCloudHeightStepsMessage,
      );
      // lowercase
      expect(validateThirdCloud('few000', 'sct001', 'bkn002', '')).toEqual(
        true,
      );
      expect(validateThirdCloud('few000', 'sct001', 'bkn02F', '')).toEqual(
        invalidCloudHeightMessage,
      );
      expect(validateThirdCloud('few000', 'sct001', 'bkn049', '')).toEqual(
        true,
      );
      expect(validateThirdCloud('few000', 'sct001', 'bkn050CB', '')).toEqual(
        true,
      );
      expect(validateThirdCloud('few000', 'sct001', 'bkn800CB', '')).toEqual(
        true,
      );
      expect(validateThirdCloud('few000', 'sct001', 'bkn990TCU', '')).toEqual(
        true,
      );
      expect(validateThirdCloud('few000', 'sct001', 'bkn051', '')).toEqual(
        invalidCloudHeightStepsMessage,
      );
      expect(validateThirdCloud('few000', 'sct001', 'bkn999CB', '')).toEqual(
        invalidCloudHeightStepsMessage,
      );
    });
    it('should accept only TCU, CB or an empty string as type', () => {
      // uppercase
      expect(validateThirdCloud('FEW000', 'SCT001', 'BKN002', '')).toEqual(
        true,
      );
      expect(validateThirdCloud('FEW000', 'SCT001', 'BKN050TCU', '')).toEqual(
        true,
      );
      expect(validateThirdCloud('FEW000', 'SCT001', 'BKN300CB', '')).toEqual(
        true,
      );
      expect(validateThirdCloud('FEW000', 'SCT001', 'BKN060CBU', '')).toEqual(
        invalidCLoudTypeMessage,
      );
      expect(validateThirdCloud('FEW000', 'SCT001', 'BKN070TC', '')).toEqual(
        invalidCLoudTypeMessage,
      );
      // lowercase
      expect(validateThirdCloud('few000', 'sct001', 'bkn002', '')).toEqual(
        true,
      );
      expect(validateThirdCloud('few000', 'sct001', 'bkn050tcu', '')).toEqual(
        true,
      );
      expect(validateThirdCloud('few000', 'sct001', 'bkn300cb', '')).toEqual(
        true,
      );
      expect(validateThirdCloud('few000', 'sct001', 'bkn060cbu', '')).toEqual(
        invalidCLoudTypeMessage,
      );
      expect(validateThirdCloud('few000', 'sct001', 'bkn070tc', '')).toEqual(
        invalidCLoudTypeMessage,
      );
    });
    it('should check for the height and type combination', () => {
      // uppercase
      expect(validateThirdCloud('FEW000', 'SCT001', 'BKN049', '')).toEqual(
        true,
      );
      expect(validateThirdCloud('FEW000', 'SCT001', 'BKN049CB', '')).toEqual(
        true,
      );
      expect(validateThirdCloud('FEW000', 'SCT001', 'BKN990TCU', '')).toEqual(
        true,
      );
      expect(validateThirdCloud('FEW000', 'SCT001', 'BKN050', '')).toEqual(
        invalidHeightTypeCombinationMessage,
      );
      expect(validateThirdCloud('FEW000', 'SCT001', 'BKN100', '')).toEqual(
        invalidHeightTypeCombinationMessage,
      );
      // lowercase
      expect(validateThirdCloud('few000', 'sct001', 'bkn049', '')).toEqual(
        true,
      );
      expect(validateThirdCloud('few000', 'sct001', 'bkn049cb', '')).toEqual(
        true,
      );
      expect(validateThirdCloud('few000', 'sct001', 'bkn990tcu', '')).toEqual(
        true,
      );
      expect(validateThirdCloud('few000', 'sct001', 'bkn050', '')).toEqual(
        invalidHeightTypeCombinationMessage,
      );
      expect(validateThirdCloud('few000', 'sct001', 'bkn100', '')).toEqual(
        invalidHeightTypeCombinationMessage,
      );
    });
    it('should check for the type and coverage combination', () => {
      // uppercase
      expect(validateThirdCloud('FEW000', 'SCT001', 'FEW050CB', '')).toEqual(
        true,
      );
      expect(validateThirdCloud('FEW000', 'SCT001', 'FEW050TCU', '')).toEqual(
        true,
      );
      expect(validateThirdCloud('FEW000', 'SCT001', 'SCT050CB', '')).toEqual(
        true,
      );
      expect(validateThirdCloud('FEW000', 'SCT001', 'SCT050TCU', '')).toEqual(
        true,
      );
      expect(validateThirdCloud('FEW000', 'SCT001', 'BKN002', '')).toEqual(
        true,
      );
      expect(validateThirdCloud('FEW000', 'SCT001', 'OVC002', '')).toEqual(
        true,
      );
      // lowercase
      expect(validateThirdCloud('few000', 'sct001', 'few050CB', '')).toEqual(
        true,
      );
      expect(validateThirdCloud('few000', 'sct001', 'few050TCU', '')).toEqual(
        true,
      );
      expect(validateThirdCloud('few000', 'sct001', 'sct050CB', '')).toEqual(
        true,
      );
      expect(validateThirdCloud('few000', 'sct001', 'sct050TCU', '')).toEqual(
        true,
      );
      expect(validateThirdCloud('few000', 'sct001', 'bkn002', '')).toEqual(
        true,
      );
      expect(validateThirdCloud('few000', 'sct001', 'ovc002', '')).toEqual(
        true,
      );
      // In case the first or second cloud group contains CB or TCU
      // this cloud group should be evaluated as the second cloud and accept SCT without a type but NOT FEW without type
      expect(validateThirdCloud('FEW000CB', 'FEW002', 'SCT003', '')).toEqual(
        true,
      );
      expect(validateThirdCloud('FEW010', 'FEW030TCU', 'SCT040', '')).toEqual(
        true,
      );
      expect(validateThirdCloud('FEW000', 'SCT001', 'SCT002', '')).toEqual(
        invalidCloud3CoverageTypeCombinationMessage,
      );
      expect(validateThirdCloud('FEW000', 'SCT001', 'FEW002', '')).toEqual(
        invalidCloud3CoverageTypeCombinationMessage,
      );
      expect(validateThirdCloud('FEW000CB', 'FEW001', 'FEW002', '')).toEqual(
        invalidCloud2CoverageTypeCombinationMessage,
      );
      expect(validateThirdCloud('FEW000', 'FEW001CB', 'FEW002', '')).toEqual(
        invalidCloud2CoverageTypeCombinationMessage,
      );

      expect(validateThirdCloud('FEW000', 'SCT001', 'FEW002', '')).toEqual(
        invalidCloud3CoverageTypeCombinationMessage,
      );
      expect(validateThirdCloud('FEW000', 'SCT001', 'SCT002', '')).toEqual(
        invalidCloud3CoverageTypeCombinationMessage,
      );
      // lowercase
      expect(validateThirdCloud('few000CB', 'few002', 'sct003', '')).toEqual(
        true,
      );
      expect(validateThirdCloud('few010', 'few030TCU', 'sct040', '')).toEqual(
        true,
      );
      expect(validateThirdCloud('few000', 'sct001', 'sct002', '')).toEqual(
        invalidCloud3CoverageTypeCombinationMessage,
      );
      expect(validateThirdCloud('few000', 'sct001', 'few002', '')).toEqual(
        invalidCloud3CoverageTypeCombinationMessage,
      );
      expect(validateThirdCloud('few000CB', 'few001', 'few002', '')).toEqual(
        invalidCloud2CoverageTypeCombinationMessage,
      );
      expect(validateThirdCloud('few000', 'few001CB', 'few002', '')).toEqual(
        invalidCloud2CoverageTypeCombinationMessage,
      );

      expect(validateThirdCloud('few000', 'sct001', 'few002', '')).toEqual(
        invalidCloud3CoverageTypeCombinationMessage,
      );
      expect(validateThirdCloud('few000', 'sct001', 'sct002', '')).toEqual(
        invalidCloud3CoverageTypeCombinationMessage,
      );
    });
    it('should not further validate cloud3 when cloud 1 or 2 is invalid', () => {
      // cloud 1 + 3 invalid
      expect(validateThirdCloud('VV', 'SCT001', 'BKN001', '')).toEqual(true);
      expect(validateThirdCloud('vv', 'sct001', 'bkn001', '')).toEqual(true);
      // cloud 2 + 3 invalid
      expect(validateThirdCloud('FEW001', 'SCT0010', 'BKN000', '')).toEqual(
        true,
      );
      expect(validateThirdCloud('few001', 'sct0010', 'bkn000', '')).toEqual(
        true,
      );
      // cloud 1 + 2 + 3 invalid
      expect(validateThirdCloud('FEW001', 'SCT002', 'BKN001', 'CAVOK')).toEqual(
        true,
      );
      expect(validateThirdCloud('few001', 'sct002', 'bkn001', 'cavok')).toEqual(
        true,
      );
    });
    it('should check that cloud3 is empty when cloud1 and/or cloud2 is empty', () => {
      // uppercase
      expect(validateThirdCloud('FEW001', '', '', '')).toEqual(true);
      expect(validateThirdCloud('FEW001', '', 'BKN060CB', '')).toEqual(
        invalidCloudWithPreviousEmptyCloud,
      );
      expect(validateThirdCloud('', '', 'BKN060CB', '')).toEqual(
        invalidCloudWithPreviousEmptyCloud,
      );
      // lowercase
      expect(validateThirdCloud('few001', '', '', '')).toEqual(true);
      expect(validateThirdCloud('few001', '', 'bkn060cb', '')).toEqual(
        invalidCloudWithPreviousEmptyCloud,
      );
      expect(validateThirdCloud('', '', 'bkn060cb', '')).toEqual(
        invalidCloudWithPreviousEmptyCloud,
      );
    });
    it('should check that cloud3 is empty when cloud1 is NSC', () => {
      expect(validateThirdCloud('NSC', '', '', '')).toEqual(true);
      expect(validateThirdCloud('nsc', '', '', '')).toEqual(true);
      expect(validateThirdCloud('NSC', '', 'BKN000', '')).toEqual(
        invalidCloudWithNSCMessage,
      );
      expect(validateThirdCloud('nsc', '', 'bkn000', '')).toEqual(
        invalidCloudWithNSCMessage,
      );
    });
    it('should check that cloud3 has no type when one of the previous cloudgroups contains type', () => {
      // uppercase
      expect(validateThirdCloud('FEW000', 'SCT001', 'BKN040', '')).toEqual(
        true,
      );
      expect(validateThirdCloud('FEW000', 'SCT001', 'BKN030CB', '')).toEqual(
        true,
      );
      expect(validateThirdCloud('FEW000', 'FEW001CB', 'SCT002', '')).toEqual(
        true,
      );
      expect(validateThirdCloud('FEW000CB', 'FEW000', 'SCT002', '')).toEqual(
        true,
      );
      expect(validateThirdCloud('FEW000', 'SCT001CB', 'BKN050TCU', '')).toEqual(
        invalidCloudWithPreviousType,
      );
      expect(validateThirdCloud('FEW000', 'SCT001CB', 'BKN040CB', '')).toEqual(
        invalidCloudWithPreviousType,
      );
      expect(
        validateThirdCloud('FEW000TCU', 'SCT001', 'BKN050TCU', ''),
      ).toEqual(invalidCloudWithPreviousType);
      // lowercase
      expect(validateThirdCloud('few000', 'sct001', 'bkn040', '')).toEqual(
        true,
      );
      expect(validateThirdCloud('few000', 'sct001', 'bkn030CB', '')).toEqual(
        true,
      );
      expect(validateThirdCloud('few000', 'few001CB', 'sct002', '')).toEqual(
        true,
      );
      expect(validateThirdCloud('few000CB', 'few000', 'sct002', '')).toEqual(
        true,
      );
      expect(validateThirdCloud('few000', 'sct001CB', 'bkn050TCU', '')).toEqual(
        invalidCloudWithPreviousType,
      );
      expect(validateThirdCloud('few000', 'sct001CB', 'bkn040CB', '')).toEqual(
        invalidCloudWithPreviousType,
      );
      expect(
        validateThirdCloud('few000TCU', 'sct001', 'bkn050TCU', ''),
      ).toEqual(invalidCloudWithPreviousType);
    });
    it('should compare the height of cloud 3 with cloud 2', () => {
      // uppercase
      expect(validateThirdCloud('FEW000', 'SCT001', 'BKN002', '')).toEqual(
        true,
      );
      expect(validateThirdCloud('FEW000', 'SCT001', 'BKN001', '')).toEqual(
        invalidCloudHeightComparedToPreviousMessage,
      );
      expect(validateThirdCloud('FEW000', 'SCT002', 'BKN001', '')).toEqual(
        invalidCloudHeightComparedToPreviousMessage,
      );
      // lowercase
      expect(validateThirdCloud('few000', 'sct001', 'bkn002', '')).toEqual(
        true,
      );
      expect(validateThirdCloud('few000', 'sct001', 'bkn001', '')).toEqual(
        invalidCloudHeightComparedToPreviousMessage,
      );
      expect(validateThirdCloud('few000', 'sct002', 'bkn001', '')).toEqual(
        invalidCloudHeightComparedToPreviousMessage,
      );
    });
    it('should check that cloud3 is empty when vertical visibility is given', () => {
      // uppercase
      expect(validateThirdCloud('VV001', '', '', '')).toEqual(true);
      expect(validateThirdCloud('VV010', 'SCT001CB', '', '')).toEqual(true);
      expect(validateThirdCloud('VV001', 'SCT050CB', 'BKN060CB', '')).toEqual(
        invalidCloudWithVerticalVisibilityMessage,
      );
      // lowercase
      expect(validateThirdCloud('vv001', '', '', '')).toEqual(true);
      expect(validateThirdCloud('vv010', 'sct001CB', '', '')).toEqual(true);
      expect(validateThirdCloud('vv001', 'sct050CB', 'bkn060cb', '')).toEqual(
        invalidCloudWithVerticalVisibilityMessage,
      );
    });
  });

  describe('validateFourthCloud', () => {
    it('should accept empty strings', () => {
      expect(validateFourthCloud('', '', '', '', '')).toEqual(true);
      expect(validateFourthCloud('', '', '', '  ', '')).toEqual(true);
    });
    it('should not accept NSC or VV', () => {
      // uppercase
      expect(
        validateFourthCloud('FEW005', 'SCT010', 'BKN020', 'NSC', ''),
      ).toEqual(invalidCloudMessage);
      expect(
        validateFourthCloud('FEW005', 'SCT010', 'BKN020', 'VV001', ''),
      ).toEqual(invalidCloudMessage);
      // lowercase
      expect(
        validateFourthCloud('few005', 'sct010', 'bkn020', 'nsc', ''),
      ).toEqual(invalidCloudMessage);
      expect(
        validateFourthCloud('few005', 'sct010', 'bkn020', 'vv001', ''),
      ).toEqual(invalidCloudMessage);
    });

    it('should not accept less than 6 or more than 9 characters', () => {
      // uppercase
      expect(
        validateFourthCloud('FEW005', 'SCT010', 'BKN020', '12345', ''),
      ).toEqual(invalidCloudMessage);
      expect(
        validateFourthCloud('FEW005', 'SCT010', 'BKN020', '1234567890', ''),
      ).toEqual(invalidCloudMessage);
      // lowercase
      expect(
        validateFourthCloud('few005', 'sct010', 'bkn020', '12345', ''),
      ).toEqual(invalidCloudMessage);
      expect(
        validateFourthCloud('few005', 'sct010', 'bkn020', '1234567890', ''),
      ).toEqual(invalidCloudMessage);
    });
    it('should not accept invalid characters', () => {
      expect(
        validateFourthCloud('FEW005', 'SCT010', 'BKN020', 'A,.DFG', ''),
      ).toEqual(invalidCloudMessage);
      expect(
        validateFourthCloud('few005', 'sct010', 'bkn020', 'a,.dfg', ''),
      ).toEqual(invalidCloudMessage);
    });
    it('should only accept FEW SCT BKN OVC as coverage', () => {
      // uppercase
      expect(
        validateFourthCloud('FEW005', 'SCT010', 'BKN020', 'FEW050CB', ''),
      ).toEqual(true);
      expect(
        validateFourthCloud('FEW005', 'SCT010', 'BKN020', ' SCT060TCU ', ''),
      ).toEqual(true);
      expect(
        validateFourthCloud('FEW005', 'SCT010', 'BKN020', 'SCT050CB', ''),
      ).toEqual(true);
      expect(
        validateFourthCloud('FEW005', 'SCT010', 'BKN020', 'BKN200CB', ''),
      ).toEqual(true);
      expect(
        validateFourthCloud('FEW005', 'SCT010', 'BKN020', 'OVC100TCU', ''),
      ).toEqual(true);
      expect(
        validateFourthCloud('FEW005', 'SCT010', 'BKN020', 'TSC120CB', ''),
      ).toEqual(invalidCloudCoverageMessage);
      // lowercase
      expect(
        validateFourthCloud('few005', 'sct010', 'bkn020', 'few050CB', ''),
      ).toEqual(true);
      expect(
        validateFourthCloud('few005', 'sct010', 'bkn020', ' sct060TCU ', ''),
      ).toEqual(true);
      expect(
        validateFourthCloud('few005', 'sct010', 'bkn020', 'sct050CB', ''),
      ).toEqual(true);
      expect(
        validateFourthCloud('few005', 'sct010', 'bkn020', 'bkn200CB', ''),
      ).toEqual(true);
      expect(
        validateFourthCloud('few005', 'sct010', 'bkn020', 'ovc100tcu', ''),
      ).toEqual(true);
      expect(
        validateFourthCloud('few005', 'sct010', 'bkn020', 'tsc120cb', ''),
      ).toEqual(invalidCloudCoverageMessage);
    });
    it('should check for the correct cloud height', () => {
      // uppercase
      expect(
        validateFourthCloud('FEW005', 'SCT001', 'BKN002', 'BKN100CB', ''),
      ).toEqual(true);
      expect(
        validateFourthCloud('FEW005', 'SCT001', 'BKN002', 'BKN02F', ''),
      ).toEqual(invalidCloudHeightMessage);
      expect(
        validateFourthCloud('FEW000', 'SCT001', 'BKN002', 'BKN050CB', ''),
      ).toEqual(true);
      expect(
        validateFourthCloud('FEW000', 'SCT001', 'BKN002', 'BKN051CB', ''),
      ).toEqual(invalidCloudHeightStepsMessage);
      expect(
        validateFourthCloud('FEW000', 'SCT001', 'BKN002', 'BKN999CB', ''),
      ).toEqual(invalidCloudHeightStepsMessage);
      // lowercase
      expect(
        validateFourthCloud('few05', 'sct001', 'bkn002', 'bkn100cb', ''),
      ).toEqual(true);
      expect(
        validateFourthCloud('few05', 'sct001', 'bkn002', 'bkn02F', ''),
      ).toEqual(invalidCloudHeightMessage);
      expect(
        validateFourthCloud('few00', 'sct001', 'bkn002', 'bkn050cb', ''),
      ).toEqual(true);
      expect(
        validateFourthCloud('few00', 'sct001', 'bkn002', 'bkn051cb', ''),
      ).toEqual(invalidCloudHeightStepsMessage);
      expect(
        validateFourthCloud('few00', 'sct001', 'bkn002', 'bkn999cb', ''),
      ).toEqual(invalidCloudHeightStepsMessage);
    });
    it('should check that correct type is given', () => {
      // uppercase
      expect(
        validateFourthCloud('FEW000', 'SCT001', 'BKN002', 'BKN050TCU', ''),
      ).toEqual(true);
      expect(
        validateFourthCloud('FEW000', 'SCT001', 'BKN002', 'BKN300CB', ''),
      ).toEqual(true);
      expect(
        validateFourthCloud('FEW000', 'SCT001', 'BKN002', 'BKN070TC', ''),
      ).toEqual(invalidCLoudTypeMessage);
      // lowercase
      expect(
        validateFourthCloud('few000', 'sct001', 'bkn002', 'bkn050tcu', ''),
      ).toEqual(true);
      expect(
        validateFourthCloud('few000', 'sct001', 'bkn002', 'bkn300cb', ''),
      ).toEqual(true);
      expect(
        validateFourthCloud('few000', 'sct001', 'bkn002', 'bkn070tc', ''),
      ).toEqual(invalidCLoudTypeMessage);
    });
    it('should check for the height and type combination', () => {
      // uppercase
      expect(
        validateFourthCloud('FEW000', 'SCT001', 'BKN002', 'BKN050', ''),
      ).toEqual(invalidHeightTypeCombinationMessage);
      expect(
        validateFourthCloud('FEW000', 'SCT001', 'BKN002', 'BKN050CB', ''),
      ).toEqual(true);
      expect(
        validateFourthCloud('FEW000', 'SCT001', 'BKN002', 'BKN990TCU', ''),
      ).toEqual(true);
      // lowercase
      expect(
        validateFourthCloud('few000', 'sct001', 'bkn002', 'bkn050', ''),
      ).toEqual(invalidHeightTypeCombinationMessage);
      expect(
        validateFourthCloud('few000', 'sct001', 'bkn002', 'bkn050cb', ''),
      ).toEqual(true);
      expect(
        validateFourthCloud('few000', 'sct001', 'bkn002', 'bkn990tcu', ''),
      ).toEqual(true);
    });
    it('should not compare cloud4 when cloud 1, 2 or 3 is invalid', () => {
      // cloud 1 and 4 invalid
      expect(
        validateFourthCloud('VV', 'SCT001', 'BKN060CB', 'BKN060CB', ''),
      ).toEqual(true);
      expect(
        validateFourthCloud('vv', 'sct001', 'bkn060cb', 'bkn060cb', ''),
      ).toEqual(true);
      // cloud 3 and 4 invalid
      expect(
        validateFourthCloud('SCT010', 'SCT020', 'BKN060', 'BKN060CB', ''),
      ).toEqual(true);
      expect(
        validateFourthCloud('sct010', 'sct020', 'bkn060', 'bkn060cb', ''),
      ).toEqual(true);
      // all clouds invalid
      expect(
        validateFourthCloud('SCT010', 'SCT020', 'BKN060', 'BKN060CB', 'CAVOK'),
      ).toEqual(true);
      expect(
        validateFourthCloud('sct010', 'sct020', 'bkn060', 'bkn060cb', 'cavok'),
      ).toEqual(true);
    });
    it('should check that cloud4 is empty when cloud 1, 2 and/or 3 is empty', () => {
      // uppercase
      expect(validateFourthCloud('FEW000', 'SCT001', '', '', '')).toEqual(true);
      expect(
        validateFourthCloud('FEW001', 'SCT002', '', 'BKN060CB', ''),
      ).toEqual(invalidCloudWithPreviousEmptyCloud);
      expect(validateFourthCloud('FEW001', '', '', 'BKN060CB', '')).toEqual(
        invalidCloudWithPreviousEmptyCloud,
      );
      expect(validateFourthCloud('', '', '', 'BKN060CB', '')).toEqual(
        invalidCloudWithPreviousEmptyCloud,
      );
      expect(
        validateFourthCloud('', 'SCT001', 'OVC002', 'BKN060CB', ''),
      ).toEqual(invalidCloudWithPreviousEmptyCloud);
      expect(
        validateFourthCloud('SCT001', '', 'OVC002', 'BKN060CB', ''),
      ).toEqual(invalidCloudWithPreviousEmptyCloud);
      // lowercase
      expect(validateFourthCloud('few000', 'sct001', '', '', '')).toEqual(true);
      expect(
        validateFourthCloud('few001', 'sct002', '', 'bkn060cb', ''),
      ).toEqual(invalidCloudWithPreviousEmptyCloud);
      expect(validateFourthCloud('few001', '', '', 'bkn060cb', '')).toEqual(
        invalidCloudWithPreviousEmptyCloud,
      );
      expect(validateFourthCloud('', '', '', 'bkn060cb', '')).toEqual(
        invalidCloudWithPreviousEmptyCloud,
      );
      expect(
        validateFourthCloud('', 'sct001', 'OVC002', 'bkn060cb', ''),
      ).toEqual(invalidCloudWithPreviousEmptyCloud);
      expect(
        validateFourthCloud('sct001', '', 'OVC002', 'bkn060cb', ''),
      ).toEqual(invalidCloudWithPreviousEmptyCloud);
    });
    it('should check that cloud4 is empty when cloud1 is NSC', () => {
      expect(validateFourthCloud('NSC', '', '', '', '')).toEqual(true);
      expect(validateFourthCloud('nsc', '', '', '', '')).toEqual(true);
      expect(validateFourthCloud('NSC', '', '', 'BKN050CB', '')).toEqual(
        invalidCloudWithNSCMessage,
      );
      expect(validateFourthCloud('nsc', '', '', 'bkn050cb', '')).toEqual(
        invalidCloudWithNSCMessage,
      );
    });
    it('should check that cloud4 has no type when one of the previous cloudgroups contains type', () => {
      // uppercase
      expect(
        validateFourthCloud('FEW000', 'SCT001', 'BKN050', 'OVC060CB', ''),
      ).toEqual(true);
      expect(
        validateFourthCloud('FEW000', 'SCT001', 'BKN030CB', 'BKN040', ''),
      ).toEqual(true);
      expect(
        validateFourthCloud('FEW000', 'FEW001CB', 'SCT002', 'BKN040', ''),
      ).toEqual(true);
      expect(
        validateFourthCloud('FEW000CB', 'FEW000', 'SCT002', 'BKN040', ''),
      ).toEqual(true);
      // There is a type in one of the previous groups so fourth group needs to be evaluated as third (no FEW and SCT allowed)
      expect(
        validateFourthCloud('FEW000CB', 'FEW000', 'SCT002', 'SCT040', ''),
      ).toEqual(invalidCloud3CoverageTypeCombinationMessage);
      expect(
        validateFourthCloud('FEW000', 'FEW000TCU', 'SCT002', 'FEW040', ''),
      ).toEqual(invalidCloud3CoverageTypeCombinationMessage);

      expect(
        validateFourthCloud('FEW000', 'SCT001', 'BKN050TCU', 'BKN060CB', ''),
      ).toEqual(invalidCloudWithPreviousType);
      expect(
        validateFourthCloud('FEW000', 'SCT001CB', 'BKN040', 'BKN060TCU', ''),
      ).toEqual(invalidCloudWithPreviousType);
      expect(
        validateFourthCloud('FEW000CB', 'SCT001', 'BKN040', 'BKN060CB', ''),
      ).toEqual(invalidCloudWithPreviousType);
      // lowercase
      expect(
        validateFourthCloud('few000', 'sct001', 'bkn050', 'ovc060cb', ''),
      ).toEqual(true);
      expect(
        validateFourthCloud('few000', 'sct001', 'bkn030cb', 'bkn040', ''),
      ).toEqual(true);
      expect(
        validateFourthCloud('few000', 'few001cb', 'sct002', 'bkn040', ''),
      ).toEqual(true);
      expect(
        validateFourthCloud('few000cb', 'few000', 'sct002', 'bkn040', ''),
      ).toEqual(true);
      expect(
        validateFourthCloud('few000cb', 'few000', 'sct002', 'sct040', ''),
      ).toEqual(invalidCloud3CoverageTypeCombinationMessage);
      expect(
        validateFourthCloud('few000', 'few000TCU', 'sct002', 'few040', ''),
      ).toEqual(invalidCloud3CoverageTypeCombinationMessage);

      expect(
        validateFourthCloud('few000', 'sct001', 'bkn050TCU', 'bkn060cb', ''),
      ).toEqual(invalidCloudWithPreviousType);
      expect(
        validateFourthCloud('few000', 'sct001cb', 'bkn040', 'bkn060TCU', ''),
      ).toEqual(invalidCloudWithPreviousType);
      expect(
        validateFourthCloud('few000cb', 'sct001', 'bkn040', 'bkn060cb', ''),
      ).toEqual(invalidCloudWithPreviousType);
    });
    it('should check that cloud4 has a type when none of the previous cloudgroups contains a type', () => {
      // uppercase
      expect(
        validateFourthCloud('FEW000', 'SCT001', 'BKN050', 'OVC060CB', ''),
      ).toEqual(true);
      expect(
        validateFourthCloud('FEW000', 'SCT001', 'BKN002', 'BKN040TCU', ''),
      ).toEqual(true);
      expect(
        validateFourthCloud('FEW000', 'SCT001', 'BKN040', 'OVC045', ''),
      ).toEqual(invalidCloud4TypeMissingMessage);
      // lowercase
      expect(
        validateFourthCloud('few000', 'sct001', 'bkn050', 'ovc060cb', ''),
      ).toEqual(true);
      expect(
        validateFourthCloud('few000', 'sct001', 'bkn002', 'bkn040tcu', ''),
      ).toEqual(true);
      expect(
        validateFourthCloud('few000', 'sct001', 'bkn040', 'ovc045', ''),
      ).toEqual(invalidCloud4TypeMissingMessage);
    });
    it('should compare the height of cloud 4 with cloud 3', () => {
      expect(
        validateFourthCloud('FEW000', 'SCT001', 'BKN049', 'BKN050CB', ''),
      ).toEqual(true);
      expect(
        validateFourthCloud('FEW001', 'SCT020', 'BKN030TCU', 'BKN030', ''),
      ).toEqual(invalidCloudHeightComparedToPreviousMessage);
      expect(
        validateFourthCloud('few000', 'sct001', 'bkn049', 'bkn050CB', ''),
      ).toEqual(true);
      expect(
        validateFourthCloud('few001', 'sct020', 'bkn030tcu', 'bkn030', ''),
      ).toEqual(invalidCloudHeightComparedToPreviousMessage);
    });
    it('should check that cloud4 is empty when vertical visibility is given', () => {
      // uppercase
      expect(validateFourthCloud('VV001', '', '', '', '')).toEqual(true);
      expect(validateFourthCloud('VV010', 'SCT001CB', '', '', '')).toEqual(
        true,
      );
      expect(
        validateFourthCloud('VV001', 'SCT050CB', 'BKN065', 'BKN060CB', ''),
      ).toEqual(invalidCloudWithVerticalVisibilityMessage);
      // lowercase
      expect(validateFourthCloud('vv001', '', '', '', '')).toEqual(true);
      expect(validateFourthCloud('vv010', 'sct001cb', '', '', '')).toEqual(
        true,
      );
      expect(
        validateFourthCloud('vv001', 'sct050cb', 'bkn065', 'bkn060cb', ''),
      ).toEqual(invalidCloudWithVerticalVisibilityMessage);
    });
  });
});
