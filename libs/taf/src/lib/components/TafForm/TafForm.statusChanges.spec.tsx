/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */
import { fireEvent, render, waitFor, screen } from '@testing-library/react';
import moment from 'moment';
import React from 'react';

import {
  MOCK_USERNAME,
  fakeAmendmentTaf,
  fakeCancelledTaf,
  fakeCorrectedTaf,
  fakeDraftAmendmentFixedTaf,
  fakeDraftAmendmentTaf,
  fakeDraftCorrectedTaf,
  fakeDraftTaf,
  fakeNewTaf,
  fakePublishedTafWithoutChangeGroups,
  previousTafForfakeDraftAmendmentFixedTaf,
} from '../../utils/mockdata/fakeTafList';
import { TafThemeApiProvider } from '../Providers';
import TafForm from './TafForm';
import { convertTafValuesToObject, prepareTafValues } from './utils';

jest.mock('../../utils/api');

describe('components/TafForm/TafForm - test status changes', () => {
  const now = `${moment.utc().format('YYYY-MM-DD')}T14:00:00Z`;
  jest.spyOn(Date, 'now').mockReturnValue(new Date(now).valueOf());

  it('should save and publish a draft taf', async () => {
    const props = {
      tafFromBackend: { ...fakeDraftTaf, editor: MOCK_USERNAME },
      onFormAction: jest.fn(),
    };
    render(
      <TafThemeApiProvider>
        <TafForm {...props} />
      </TafThemeApiProvider>,
    );

    // check editor mode
    expect(screen.getByTestId('switchMode').classList).toContain('Mui-checked');

    const expectedSaveResult = convertTafValuesToObject(
      prepareTafValues(props.tafFromBackend.taf, 'DRAFT'),
    );

    fireEvent.click(screen.getByTestId('savedrafttaf'));
    expect(screen.queryByTestId('confirmationDialog')).toBeFalsy();
    await waitFor(() =>
      expect(props.onFormAction).toHaveBeenCalledWith(
        'DRAFT',
        expectedSaveResult,
      ),
    );

    expect(expectedSaveResult.location).toEqual(
      props.tafFromBackend.taf.location,
    );
    expect(expectedSaveResult.messageType).toEqual(
      props.tafFromBackend.taf.messageType,
    );

    fireEvent.click(screen.getByTestId('publishtaf'));
    await screen.findByTestId('confirmationDialog-confirm');
    fireEvent.click(screen.getByTestId('confirmationDialog-confirm'));

    const expectedPublishResult = convertTafValuesToObject(
      prepareTafValues(props.tafFromBackend.taf, 'PUBLISH'),
    );

    await waitFor(() =>
      expect(props.onFormAction).toHaveBeenCalledWith(
        'PUBLISH',
        expectedPublishResult,
      ),
    );

    expect(expectedPublishResult.location).toEqual(
      props.tafFromBackend.taf.location,
    );
    expect(expectedPublishResult.messageType).toEqual(
      props.tafFromBackend.taf.messageType,
    );
  });

  it('should save a new taf as draft', async () => {
    const props = {
      tafFromBackend: { ...fakeNewTaf, editor: MOCK_USERNAME },
      onFormAction: jest.fn(),
    };
    render(
      <TafThemeApiProvider>
        <TafForm {...props} />
      </TafThemeApiProvider>,
    );

    // check editor mode
    expect(screen.getByTestId('switchMode').classList).toContain('Mui-checked');

    const expectedResult = convertTafValuesToObject(
      prepareTafValues(props.tafFromBackend.taf, 'DRAFT'),
    );

    fireEvent.click(screen.getByTestId('savedrafttaf'));
    await waitFor(() =>
      expect(props.onFormAction).toHaveBeenCalledWith('DRAFT', expectedResult),
    );

    expect(expectedResult.location).toEqual(props.tafFromBackend.taf.location);
    expect(expectedResult.messageType).toEqual(
      props.tafFromBackend.taf.messageType,
    );
  });

  it('should publish a new taf', async () => {
    const props = {
      tafFromBackend: { ...fakeNewTaf, editor: MOCK_USERNAME },
      onFormAction: jest.fn(),
    };
    render(
      <TafThemeApiProvider>
        <TafForm {...props} />
      </TafThemeApiProvider>,
    );

    // check editor mode
    expect(screen.getByTestId('switchMode').classList).toContain('Mui-checked');

    // enter required fields
    fireEvent.change(
      screen.getByRole('textbox', { name: 'baseForecast.wind' }),
      {
        target: { value: '20020' },
      },
    );
    fireEvent.blur(screen.getByRole('textbox', { name: 'baseForecast.wind' }));
    fireEvent.change(
      screen.getByRole('textbox', { name: 'baseForecast.visibility' }),
      {
        target: { value: 'CAVOK' },
      },
    );
    fireEvent.blur(
      screen.getByRole('textbox', { name: 'baseForecast.visibility' }),
    );

    const expectedResult = convertTafValuesToObject(
      prepareTafValues(
        {
          ...props.tafFromBackend.taf,
          baseForecast: {
            valid: props.tafFromBackend.taf.baseForecast.valid,
            cavOK: true,
            wind: { direction: 200, speed: 20, unit: 'KT' },
          },
        },
        'PUBLISH',
      ),
    );

    fireEvent.click(screen.getByTestId('publishtaf'));
    await screen.findByTestId('confirmationDialog-confirm');
    fireEvent.click(screen.getByTestId('confirmationDialog-confirm'));
    await waitFor(() =>
      expect(props.onFormAction).toHaveBeenCalledWith(
        'PUBLISH',
        expectedResult,
      ),
    );

    expect(expectedResult.location).toEqual(props.tafFromBackend.taf.location);
    expect(expectedResult.messageType).toEqual(
      props.tafFromBackend.taf.messageType,
    );
  });

  it('should save and publish a draft_corrected taf', async () => {
    const props = {
      tafFromBackend: { ...fakeDraftCorrectedTaf, editor: MOCK_USERNAME },
      previousTaf: fakeDraftCorrectedTaf.taf,
      onFormAction: jest.fn(),
    };
    render(
      <TafThemeApiProvider>
        <TafForm {...props} />
      </TafThemeApiProvider>,
    );

    // check editor mode
    expect(screen.getByTestId('switchMode').classList).toContain('Mui-checked');

    const expectedSaveResult = convertTafValuesToObject(
      prepareTafValues(props.tafFromBackend.taf, 'DRAFT'),
    );

    fireEvent.click(screen.getByTestId('savedrafttaf'));
    await waitFor(() =>
      expect(props.onFormAction).toHaveBeenLastCalledWith(
        'DRAFT_CORRECT',
        expectedSaveResult,
      ),
    );

    const expectedPublishResult = convertTafValuesToObject(
      prepareTafValues(props.tafFromBackend.taf, 'PUBLISH'),
    );

    fireEvent.click(screen.getByTestId('publishtaf'));
    await screen.findByTestId('confirmationDialog-confirm');
    fireEvent.click(screen.getByTestId('confirmationDialog-confirm'));

    await waitFor(() =>
      expect(props.onFormAction).toHaveBeenLastCalledWith(
        'CORRECT',
        expectedPublishResult,
      ),
    );

    expect(expectedPublishResult.location).toEqual(
      props.tafFromBackend.taf.location,
    );
    expect(expectedPublishResult.messageType).toEqual(
      props.tafFromBackend.taf.messageType,
    );
  });

  it('should save and publish a draft_amended taf', async () => {
    const props = {
      tafFromBackend: { ...fakeDraftAmendmentTaf, editor: MOCK_USERNAME },
      previousTaf: fakeDraftAmendmentTaf.taf,
      onFormAction: jest.fn(),
    };
    render(
      <TafThemeApiProvider>
        <TafForm {...props} />
      </TafThemeApiProvider>,
    );

    // check editor mode
    expect(screen.getByTestId('switchMode').classList).toContain('Mui-checked');

    const expectedSaveResult = convertTafValuesToObject(
      prepareTafValues(props.tafFromBackend.taf, 'DRAFT'),
    );

    fireEvent.click(screen.getByTestId('savedrafttaf'));
    await waitFor(() =>
      expect(props.onFormAction).toHaveBeenLastCalledWith(
        'DRAFT_AMEND',
        expectedSaveResult,
      ),
    );

    const expectedPublishResult = convertTafValuesToObject(
      prepareTafValues(props.tafFromBackend.taf, 'PUBLISH'),
    );

    fireEvent.click(screen.getByTestId('publishtaf'));
    await screen.findByTestId('confirmationDialog-confirm');
    fireEvent.click(screen.getByTestId('confirmationDialog-confirm'));

    await waitFor(() =>
      expect(props.onFormAction).toHaveBeenLastCalledWith(
        'AMEND',
        expectedPublishResult,
      ),
    );

    expect(expectedPublishResult.location).toEqual(
      props.tafFromBackend.taf.location,
    );
    expect(expectedPublishResult.messageType).toEqual(
      props.tafFromBackend.taf.messageType,
    );
  });

  it('should save a published taf as amended', async () => {
    const props = {
      isDisabled: true,
      tafFromBackend: {
        ...fakePublishedTafWithoutChangeGroups,
        editor: MOCK_USERNAME,
      },
      onFormAction: jest.fn(),
    };
    const { rerender } = render(
      <TafThemeApiProvider>
        <TafForm {...props} />
      </TafThemeApiProvider>,
    );

    // check editor mode
    expect(screen.getByTestId('switchMode').classList).toContain('Mui-checked');

    expect(
      screen
        .getByRole('textbox', { name: 'messageType' })
        .getAttribute('value'),
    ).toEqual('ORG');

    fireEvent.click(screen.getByTestId('amendtaf'));
    expect(
      screen
        .getByRole('textbox', { name: 'messageType' })
        .getAttribute('value'),
    ).toEqual('AMD');

    rerender(
      <TafThemeApiProvider>
        <TafForm {...props} isDisabled={false} />
      </TafThemeApiProvider>,
    );

    fireEvent.click(screen.getByTestId('publishtaf'));

    // check no errors
    expect(
      screen
        .queryAllByRole('textbox')
        .filter((field) => field.getAttribute('aria-invalid') === 'true'),
    ).toHaveLength(0);

    await screen.findByTestId('confirmationDialog-confirm');
    fireEvent.click(screen.getByTestId('confirmationDialog-confirm'));

    const expectedPublishResult = convertTafValuesToObject(
      prepareTafValues(props.tafFromBackend.taf, 'AMEND'),
    );
    await waitFor(() =>
      expect(props.onFormAction).toHaveBeenCalledWith(
        'AMEND',
        expectedPublishResult,
      ),
    );

    expect(expectedPublishResult.location).toEqual(
      props.tafFromBackend.taf.location,
    );
    expect(expectedPublishResult.messageType).toEqual('AMD');
  });

  it('should save a published taf as draft_amended', async () => {
    const props = {
      isDisabled: true,
      tafFromBackend: {
        ...fakePublishedTafWithoutChangeGroups,
        editor: MOCK_USERNAME,
      },
      onFormAction: jest.fn(),
    };
    const { rerender } = render(
      <TafThemeApiProvider>
        <TafForm {...props} />
      </TafThemeApiProvider>,
    );

    // check editor mode
    expect(screen.getByTestId('switchMode').classList).toContain('Mui-checked');

    expect(
      screen
        .getByRole('textbox', { name: 'messageType' })
        .getAttribute('value'),
    ).toEqual('ORG');

    fireEvent.click(screen.getByTestId('amendtaf'));
    expect(
      screen
        .getByRole('textbox', { name: 'messageType' })
        .getAttribute('value'),
    ).toEqual('AMD');

    rerender(
      <TafThemeApiProvider>
        <TafForm {...props} isDisabled={false} />
      </TafThemeApiProvider>,
    );

    fireEvent.click(screen.getByTestId('savedrafttaf'));

    // check no errors
    expect(
      screen
        .queryAllByRole('textbox')
        .filter((field) => field.getAttribute('aria-invalid') === 'true'),
    ).toHaveLength(0);

    const expectedSaveResult = convertTafValuesToObject(
      prepareTafValues(props.tafFromBackend.taf, 'AMEND'),
    );

    await waitFor(() => {
      expect(props.onFormAction).toHaveBeenCalledWith(
        'DRAFT_AMEND',
        expectedSaveResult,
      );
    });

    expect(expectedSaveResult.location).toEqual(
      props.tafFromBackend.taf.location,
    );
    expect(expectedSaveResult.messageType).toEqual('AMD');
  });

  it('should save a published taf as corrected', async () => {
    const props = {
      isDisabled: true,
      tafFromBackend: {
        ...fakePublishedTafWithoutChangeGroups,
        editor: MOCK_USERNAME,
      },
      onFormAction: jest.fn(),
    };
    const { rerender } = render(
      <TafThemeApiProvider>
        <TafForm {...props} />
      </TafThemeApiProvider>,
    );

    // check editor mode
    expect(screen.getByTestId('switchMode').classList).toContain('Mui-checked');

    expect(
      screen
        .getByRole('textbox', { name: 'messageType' })
        .getAttribute('value'),
    ).toEqual('ORG');

    fireEvent.click(screen.getByTestId('correcttaf'));
    expect(
      screen
        .getByRole('textbox', { name: 'messageType' })
        .getAttribute('value'),
    ).toEqual('COR');

    rerender(
      <TafThemeApiProvider>
        <TafForm {...props} isDisabled={false} />
      </TafThemeApiProvider>,
    );

    fireEvent.click(screen.getByTestId('publishtaf'));

    // check no errors
    expect(
      screen
        .queryAllByRole('textbox')
        .filter((field) => field.getAttribute('aria-invalid') === 'true'),
    ).toHaveLength(0);

    await screen.findByTestId('confirmationDialog-confirm');
    fireEvent.click(screen.getByTestId('confirmationDialog-confirm'));

    const expectedPublishResult = convertTafValuesToObject(
      prepareTafValues(props.tafFromBackend.taf, 'CORRECT'),
    );
    await waitFor(() =>
      expect(props.onFormAction).toHaveBeenCalledWith(
        'CORRECT',
        expectedPublishResult,
      ),
    );

    expect(expectedPublishResult.location).toEqual(
      props.tafFromBackend.taf.location,
    );
    expect(expectedPublishResult.messageType).toEqual('COR');
  });

  it('should save a published taf as draft_corrected', async () => {
    const props = {
      isDisabled: true,
      tafFromBackend: {
        ...fakePublishedTafWithoutChangeGroups,
        editor: MOCK_USERNAME,
      },
      onFormAction: jest.fn(),
    };
    const { rerender } = render(
      <TafThemeApiProvider>
        <TafForm {...props} />
      </TafThemeApiProvider>,
    );

    // check editor mode
    expect(screen.getByTestId('switchMode').classList).toContain('Mui-checked');

    expect(
      screen
        .getByRole('textbox', { name: 'messageType' })
        .getAttribute('value'),
    ).toEqual('ORG');

    fireEvent.click(screen.getByTestId('correcttaf'));
    expect(
      screen
        .getByRole('textbox', { name: 'messageType' })
        .getAttribute('value'),
    ).toEqual('COR');

    rerender(
      <TafThemeApiProvider>
        <TafForm {...props} isDisabled={false} />
      </TafThemeApiProvider>,
    );

    fireEvent.click(screen.getByTestId('savedrafttaf'));

    // check no errors
    expect(
      screen
        .queryAllByRole('textbox')
        .filter((field) => field.getAttribute('aria-invalid') === 'true'),
    ).toHaveLength(0);

    const expectedSaveResult = convertTafValuesToObject(
      prepareTafValues(props.tafFromBackend.taf, 'CORRECT'),
    );

    await waitFor(() => {
      expect(props.onFormAction).toHaveBeenCalledWith(
        'DRAFT_CORRECT',
        expectedSaveResult,
      );
    });

    expect(expectedSaveResult.location).toEqual(
      props.tafFromBackend.taf.location,
    );
    expect(expectedSaveResult.messageType).toEqual('COR');
  });

  it('should save a cancelled taf as amended', async () => {
    const props = {
      isDisabled: true,
      tafFromBackend: { ...fakeCancelledTaf, editor: MOCK_USERNAME },
      onFormAction: jest.fn(),
    };
    const { rerender } = render(
      <TafThemeApiProvider>
        <TafForm {...props} />
      </TafThemeApiProvider>,
    );

    // check editor mode
    expect(screen.getByTestId('switchMode').classList).toContain('Mui-checked');

    expect(
      screen
        .getByRole('textbox', { name: 'messageType' })
        .getAttribute('value'),
    ).toEqual('CNL');

    fireEvent.click(screen.getByTestId('amendtaf'));
    expect(
      screen
        .getByRole('textbox', { name: 'messageType' })
        .getAttribute('value'),
    ).toEqual('AMD');

    rerender(
      <TafThemeApiProvider>
        <TafForm {...props} isDisabled={false} />
      </TafThemeApiProvider>,
    );

    fireEvent.click(screen.getByTestId('publishtaf'));

    // check no errors
    expect(
      screen
        .queryAllByRole('textbox')
        .filter((field) => field.getAttribute('aria-invalid') === 'true'),
    ).toHaveLength(0);

    await screen.findByTestId('confirmationDialog-confirm');
    fireEvent.click(screen.getByTestId('confirmationDialog-confirm'));

    const expectedPublishResult = convertTafValuesToObject(
      prepareTafValues(props.tafFromBackend.taf, 'AMEND'),
    );

    await waitFor(() => {
      expect(props.onFormAction).toHaveBeenCalledWith(
        'AMEND',
        expectedPublishResult,
      );
    });

    expect(expectedPublishResult.location).toEqual(
      props.tafFromBackend.taf.location,
    );
    expect(expectedPublishResult.messageType).toEqual('AMD');
  });

  it('should save a cancelled taf as draft_amended', async () => {
    const props = {
      isDisabled: true,
      tafFromBackend: { ...fakeCancelledTaf, editor: MOCK_USERNAME },
      onFormAction: jest.fn(),
    };
    const { rerender } = render(
      <TafThemeApiProvider>
        <TafForm {...props} />
      </TafThemeApiProvider>,
    );

    // check editor mode
    expect(screen.getByTestId('switchMode').classList).toContain('Mui-checked');

    expect(
      screen
        .getByRole('textbox', { name: 'messageType' })
        .getAttribute('value'),
    ).toEqual('CNL');

    fireEvent.click(screen.getByTestId('amendtaf'));
    expect(
      screen
        .getByRole('textbox', { name: 'messageType' })
        .getAttribute('value'),
    ).toEqual('AMD');

    rerender(
      <TafThemeApiProvider>
        <TafForm {...props} isDisabled={false} />
      </TafThemeApiProvider>,
    );

    fireEvent.click(await screen.findByTestId('savedrafttaf'));

    // check no errors
    await waitFor(() =>
      expect(
        screen
          .queryAllByRole('textbox')
          .filter((field) => field.getAttribute('aria-invalid') === 'true'),
      ).toHaveLength(0),
    );

    const expectedSaveResult = convertTafValuesToObject(
      prepareTafValues(props.tafFromBackend.taf, 'AMEND'),
    );
    expect(expectedSaveResult.location).toEqual(
      props.tafFromBackend.taf.location,
    );
    expect(expectedSaveResult.messageType).toEqual('AMD');

    await waitFor(
      () => {
        expect(props.onFormAction).toHaveBeenCalledWith(
          'DRAFT_AMEND',
          expectedSaveResult,
        );
      },
      { timeout: 3000 },
    );
  });

  it('should save an amended taf as amended', async () => {
    const props = {
      isDisabled: true,
      tafFromBackend: { ...fakeAmendmentTaf, editor: MOCK_USERNAME },
      onFormAction: jest.fn(),
    };
    const { rerender } = render(
      <TafThemeApiProvider>
        <TafForm {...props} />
      </TafThemeApiProvider>,
    );

    // check editor mode
    expect(screen.getByTestId('switchMode').classList).toContain('Mui-checked');

    expect(
      screen
        .getByRole('textbox', { name: 'messageType' })
        .getAttribute('value'),
    ).toEqual('AMD');

    fireEvent.click(screen.getByTestId('amendtaf'));
    expect(
      screen
        .getByRole('textbox', { name: 'messageType' })
        .getAttribute('value'),
    ).toEqual('AMD');

    rerender(
      <TafThemeApiProvider>
        <TafForm {...props} isDisabled={false} />
      </TafThemeApiProvider>,
    );

    fireEvent.click(screen.getByTestId('publishtaf'));

    // check no errors
    expect(
      screen
        .queryAllByRole('textbox')
        .filter((field) => field.getAttribute('aria-invalid') === 'true'),
    ).toHaveLength(0);

    await screen.findByTestId('confirmationDialog-confirm');
    fireEvent.click(screen.getByTestId('confirmationDialog-confirm'));

    const expectedPublishResult = convertTafValuesToObject(
      prepareTafValues(props.tafFromBackend.taf, 'AMEND'),
    );

    await waitFor(() => {
      expect(props.onFormAction).toHaveBeenCalledWith(
        'AMEND',
        expectedPublishResult,
      );
    });

    expect(expectedPublishResult.location).toEqual(
      props.tafFromBackend.taf.location,
    );
    expect(expectedPublishResult.messageType).toEqual('AMD');
  });

  it('should save an amended taf as draft_amended', async () => {
    const props = {
      isDisabled: true,
      tafFromBackend: { ...fakeAmendmentTaf, editor: MOCK_USERNAME },
      onFormAction: jest.fn(),
    };
    const { rerender } = render(
      <TafThemeApiProvider>
        <TafForm {...props} />
      </TafThemeApiProvider>,
    );

    // check editor mode
    expect(screen.getByTestId('switchMode').classList).toContain('Mui-checked');

    expect(
      screen
        .getByRole('textbox', { name: 'messageType' })
        .getAttribute('value'),
    ).toEqual('AMD');

    fireEvent.click(screen.getByTestId('amendtaf'));
    expect(
      screen
        .getByRole('textbox', { name: 'messageType' })
        .getAttribute('value'),
    ).toEqual('AMD');

    rerender(
      <TafThemeApiProvider>
        <TafForm {...props} isDisabled={false} />
      </TafThemeApiProvider>,
    );

    fireEvent.click(await screen.findByTestId('savedrafttaf'));

    // check no errors
    await waitFor(() =>
      expect(
        screen
          .queryAllByRole('textbox')
          .filter((field) => field.getAttribute('aria-invalid') === 'true'),
      ).toHaveLength(0),
    );

    const expectedSaveResult = convertTafValuesToObject(
      prepareTafValues(props.tafFromBackend.taf, 'AMEND'),
    );
    expect(expectedSaveResult.location).toEqual(
      props.tafFromBackend.taf.location,
    );
    expect(expectedSaveResult.messageType).toEqual('AMD');

    await waitFor(
      () => {
        expect(props.onFormAction).toHaveBeenCalledWith(
          'DRAFT_AMEND',
          expectedSaveResult,
        );
      },
      { timeout: 3000 },
    );
  });

  it('should save a corrected taf as amended', async () => {
    const props = {
      isDisabled: true,
      tafFromBackend: { ...fakeCorrectedTaf, editor: MOCK_USERNAME },
      onFormAction: jest.fn(),
    };
    const { rerender } = render(
      <TafThemeApiProvider>
        <TafForm {...props} />
      </TafThemeApiProvider>,
    );

    // check editor mode
    expect(screen.getByTestId('switchMode').classList).toContain('Mui-checked');

    expect(
      screen
        .getByRole('textbox', { name: 'messageType' })
        .getAttribute('value'),
    ).toEqual('COR');

    fireEvent.click(screen.getByTestId('amendtaf'));
    expect(
      screen
        .getByRole('textbox', { name: 'messageType' })
        .getAttribute('value'),
    ).toEqual('AMD');

    rerender(
      <TafThemeApiProvider>
        <TafForm {...props} isDisabled={false} />
      </TafThemeApiProvider>,
    );

    fireEvent.click(screen.getByTestId('publishtaf'));

    // check no errors
    expect(
      screen
        .queryAllByRole('textbox')
        .filter((field) => field.getAttribute('aria-invalid') === 'true'),
    ).toHaveLength(0);

    await screen.findByTestId('confirmationDialog-confirm');
    fireEvent.click(screen.getByTestId('confirmationDialog-confirm'));

    const expectedPublishResult = convertTafValuesToObject(
      prepareTafValues(props.tafFromBackend.taf, 'AMEND'),
    );
    await waitFor(() => {
      expect(props.onFormAction).toHaveBeenCalledWith(
        'AMEND',
        expectedPublishResult,
      );
    });

    expect(expectedPublishResult.location).toEqual(
      props.tafFromBackend.taf.location,
    );
    expect(expectedPublishResult.messageType).toEqual('AMD');
  });

  it('should save a corrected taf as draft_amended', async () => {
    const props = {
      isDisabled: true,
      tafFromBackend: { ...fakeCorrectedTaf, editor: MOCK_USERNAME },
      onFormAction: jest.fn(),
    };
    const { rerender } = render(
      <TafThemeApiProvider>
        <TafForm {...props} />
      </TafThemeApiProvider>,
    );

    // check editor mode
    expect(screen.getByTestId('switchMode').classList).toContain('Mui-checked');

    expect(
      screen
        .getByRole('textbox', { name: 'messageType' })
        .getAttribute('value'),
    ).toEqual('COR');

    fireEvent.click(screen.getByTestId('amendtaf'));
    expect(
      screen
        .getByRole('textbox', { name: 'messageType' })
        .getAttribute('value'),
    ).toEqual('AMD');

    rerender(
      <TafThemeApiProvider>
        <TafForm {...props} isDisabled={false} />
      </TafThemeApiProvider>,
    );

    fireEvent.click(screen.getByTestId('savedrafttaf'));

    // check no errors
    expect(
      screen
        .queryAllByRole('textbox')
        .filter((field) => field.getAttribute('aria-invalid') === 'true'),
    ).toHaveLength(0);

    const expectedSaveResult = convertTafValuesToObject(
      prepareTafValues(props.tafFromBackend.taf, 'AMEND'),
    );

    await waitFor(() => {
      expect(props.onFormAction).toHaveBeenCalledWith(
        'DRAFT_AMEND',
        expectedSaveResult,
      );
    });

    expect(expectedSaveResult.location).toEqual(
      props.tafFromBackend.taf.location,
    );
    expect(expectedSaveResult.messageType).toEqual('AMD');
  });

  it('should save an amended taf as corrected', async () => {
    const props = {
      isDisabled: true,
      tafFromBackend: { ...fakeAmendmentTaf, editor: MOCK_USERNAME },
      onFormAction: jest.fn(),
    };
    const { rerender } = render(
      <TafThemeApiProvider>
        <TafForm {...props} />
      </TafThemeApiProvider>,
    );

    // check editor mode
    expect(screen.getByTestId('switchMode').classList).toContain('Mui-checked');

    expect(
      screen
        .getByRole('textbox', { name: 'messageType' })
        .getAttribute('value'),
    ).toEqual('AMD');

    fireEvent.click(screen.getByTestId('correcttaf'));
    expect(
      screen
        .getByRole('textbox', { name: 'messageType' })
        .getAttribute('value'),
    ).toEqual('COR');

    rerender(
      <TafThemeApiProvider>
        <TafForm {...props} isDisabled={false} />
      </TafThemeApiProvider>,
    );

    fireEvent.click(screen.getByTestId('publishtaf'));

    // check no errors
    expect(
      screen
        .queryAllByRole('textbox')
        .filter((field) => field.getAttribute('aria-invalid') === 'true'),
    ).toHaveLength(0);

    await screen.findByTestId('confirmationDialog-confirm');
    fireEvent.click(screen.getByTestId('confirmationDialog-confirm'));

    const expectedPublishResult = convertTafValuesToObject(
      prepareTafValues(props.tafFromBackend.taf, 'CORRECT'),
    );

    await waitFor(() => {
      expect(props.onFormAction).toHaveBeenCalledWith(
        'CORRECT',
        expectedPublishResult,
      );
    });

    expect(expectedPublishResult.location).toEqual(
      props.tafFromBackend.taf.location,
    );
    expect(expectedPublishResult.messageType).toEqual('COR');
  });

  it('should save an amended taf as draft_corrected', async () => {
    const props = {
      isDisabled: true,
      tafFromBackend: { ...fakeAmendmentTaf, editor: MOCK_USERNAME },
      onFormAction: jest.fn(),
    };
    const { rerender } = render(
      <TafThemeApiProvider>
        <TafForm {...props} />
      </TafThemeApiProvider>,
    );

    // check editor mode
    expect(screen.getByTestId('switchMode').classList).toContain('Mui-checked');

    expect(
      screen
        .getByRole('textbox', { name: 'messageType' })
        .getAttribute('value'),
    ).toEqual('AMD');

    fireEvent.click(screen.getByTestId('correcttaf'));
    expect(
      screen
        .getByRole('textbox', { name: 'messageType' })
        .getAttribute('value'),
    ).toEqual('COR');

    rerender(
      <TafThemeApiProvider>
        <TafForm {...props} isDisabled={false} />
      </TafThemeApiProvider>,
    );

    fireEvent.click(screen.getByTestId('savedrafttaf'));

    // check no errors
    expect(
      screen
        .queryAllByRole('textbox')
        .filter((field) => field.getAttribute('aria-invalid') === 'true'),
    ).toHaveLength(0);

    const expectedSaveResult = convertTafValuesToObject(
      prepareTafValues(props.tafFromBackend.taf, 'CORRECT'),
    );

    await waitFor(() => {
      expect(props.onFormAction).toHaveBeenCalledWith(
        'DRAFT_CORRECT',
        expectedSaveResult,
      );
    });

    expect(expectedSaveResult.location).toEqual(
      props.tafFromBackend.taf.location,
    );
    expect(expectedSaveResult.messageType).toEqual('COR');
  });

  it('should save a corrected taf as corrected', async () => {
    const props = {
      isDisabled: true,
      tafFromBackend: { ...fakeCorrectedTaf, editor: MOCK_USERNAME },
      onFormAction: jest.fn(),
    };
    const { rerender } = render(
      <TafThemeApiProvider>
        <TafForm {...props} />
      </TafThemeApiProvider>,
    );

    // check editor mode
    expect(screen.getByTestId('switchMode').classList).toContain('Mui-checked');

    expect(
      screen
        .getByRole('textbox', { name: 'messageType' })
        .getAttribute('value'),
    ).toEqual('COR');

    fireEvent.click(screen.getByTestId('correcttaf'));
    expect(
      screen
        .getByRole('textbox', { name: 'messageType' })
        .getAttribute('value'),
    ).toEqual('COR');

    rerender(
      <TafThemeApiProvider>
        <TafForm {...props} isDisabled={false} />
      </TafThemeApiProvider>,
    );

    fireEvent.click(screen.getByTestId('publishtaf'));

    // check no errors
    expect(
      screen
        .queryAllByRole('textbox')
        .filter((field) => field.getAttribute('aria-invalid') === 'true'),
    ).toHaveLength(0);

    await screen.findByTestId('confirmationDialog-confirm');
    fireEvent.click(screen.getByTestId('confirmationDialog-confirm'));

    const expectedPublishResult = convertTafValuesToObject(
      prepareTafValues(props.tafFromBackend.taf, 'CORRECT'),
    );

    await waitFor(() => {
      expect(props.onFormAction).toHaveBeenCalledWith(
        'CORRECT',
        expectedPublishResult,
      );
    });

    expect(expectedPublishResult.location).toEqual(
      props.tafFromBackend.taf.location,
    );
    expect(expectedPublishResult.messageType).toEqual('COR');
  });

  it('should save a corrected taf as draft_corrected', async () => {
    const props = {
      isDisabled: true,
      tafFromBackend: { ...fakeCorrectedTaf, editor: MOCK_USERNAME },
      onFormAction: jest.fn(),
    };
    const { rerender } = render(
      <TafThemeApiProvider>
        <TafForm {...props} />
      </TafThemeApiProvider>,
    );

    // check editor mode
    expect(screen.getByTestId('switchMode').classList).toContain('Mui-checked');

    expect(
      screen
        .getByRole('textbox', { name: 'messageType' })
        .getAttribute('value'),
    ).toEqual('COR');

    fireEvent.click(screen.getByTestId('correcttaf'));
    expect(
      screen
        .getByRole('textbox', { name: 'messageType' })
        .getAttribute('value'),
    ).toEqual('COR');

    rerender(
      <TafThemeApiProvider>
        <TafForm {...props} isDisabled={false} />
      </TafThemeApiProvider>,
    );

    fireEvent.click(await screen.findByTestId('savedrafttaf'));

    // check no errors
    await waitFor(() =>
      expect(
        screen
          .queryAllByRole('textbox')
          .filter((field) => field.getAttribute('aria-invalid') === 'true'),
      ).toHaveLength(0),
    );

    const expectedSaveResult = convertTafValuesToObject(
      prepareTafValues(props.tafFromBackend.taf, 'CORRECT'),
    );
    expect(expectedSaveResult.location).toEqual(
      props.tafFromBackend.taf.location,
    );
    expect(expectedSaveResult.messageType).toEqual('COR');

    await waitFor(
      () => {
        expect(props.onFormAction).toHaveBeenCalledWith(
          'DRAFT_CORRECT',
          expectedSaveResult,
        );
      },
      { timeout: 3000 },
    );
  });

  it('should cancel a published taf', async () => {
    const props = {
      isDisabled: true,
      tafFromBackend: {
        ...fakePublishedTafWithoutChangeGroups,
        editor: MOCK_USERNAME,
      },
      onFormAction: jest.fn(),
    };
    render(
      <TafThemeApiProvider>
        <TafForm {...props} />
      </TafThemeApiProvider>,
    );

    // check editor mode
    expect(screen.getByTestId('switchMode').classList).toContain('Mui-checked');

    expect(
      screen
        .getByRole('textbox', { name: 'messageType' })
        .getAttribute('value'),
    ).toEqual('ORG');

    fireEvent.click(screen.getByTestId('canceltaf'));

    await screen.findByTestId('confirmationDialog-confirm');
    fireEvent.click(screen.getByTestId('confirmationDialog-confirm'));

    const expectedCancelResult = convertTafValuesToObject(
      prepareTafValues(props.tafFromBackend.taf, 'CANCEL'),
    );

    await waitFor(() => {
      expect(props.onFormAction).toHaveBeenCalledWith(
        'CANCEL',
        expectedCancelResult,
      );
    });

    expect(expectedCancelResult.location).toEqual(
      props.tafFromBackend.taf.location,
    );
    expect(expectedCancelResult.messageType).toEqual('CNL');
  });

  it('should cancel an amended taf', async () => {
    const props = {
      isDisabled: true,
      tafFromBackend: { ...fakeAmendmentTaf, editor: MOCK_USERNAME },
      onFormAction: jest.fn(),
    };
    render(
      <TafThemeApiProvider>
        <TafForm {...props} />
      </TafThemeApiProvider>,
    );

    // check editor mode
    expect(screen.getByTestId('switchMode').classList).toContain('Mui-checked');

    expect(
      screen
        .getByRole('textbox', { name: 'messageType' })
        .getAttribute('value'),
    ).toEqual('AMD');

    fireEvent.click(screen.getByTestId('canceltaf'));

    await screen.findByTestId('confirmationDialog-confirm');
    fireEvent.click(screen.getByTestId('confirmationDialog-confirm'));

    const expectedCancelResult = convertTafValuesToObject(
      prepareTafValues(props.tafFromBackend.taf, 'CANCEL'),
    );

    await waitFor(() => {
      expect(props.onFormAction).toHaveBeenCalledWith(
        'CANCEL',
        expectedCancelResult,
      );
    });

    expect(expectedCancelResult.location).toEqual(
      props.tafFromBackend.taf.location,
    );
    expect(expectedCancelResult.messageType).toEqual('CNL');
  });

  it('should cancel a corrected taf', async () => {
    const props = {
      isDisabled: true,
      tafFromBackend: { ...fakeCorrectedTaf, editor: MOCK_USERNAME },
      onFormAction: jest.fn(),
    };
    render(
      <TafThemeApiProvider>
        <TafForm {...props} />
      </TafThemeApiProvider>,
    );

    // check editor mode
    expect(screen.getByTestId('switchMode').classList).toContain('Mui-checked');

    expect(
      screen
        .getByRole('textbox', { name: 'messageType' })
        .getAttribute('value'),
    ).toEqual('COR');

    fireEvent.click(screen.getByTestId('canceltaf'));

    await screen.findByTestId('confirmationDialog-confirm');
    fireEvent.click(screen.getByTestId('confirmationDialog-confirm'));

    const expectedCancelResult = convertTafValuesToObject(
      prepareTafValues(props.tafFromBackend.taf, 'CANCEL'),
    );

    await waitFor(() => {
      expect(props.onFormAction).toHaveBeenCalledWith(
        'CANCEL',
        expectedCancelResult,
      );
    });

    expect(expectedCancelResult.location).toEqual(
      props.tafFromBackend.taf.location,
    );
    expect(expectedCancelResult.messageType).toEqual('CNL');
  });

  it('should ask if you want to publish a draft_amended taf if there are no changes with respect to the original published TAF', async () => {
    // Pass exactly the same taf to ensure there will be no changes
    const props = {
      tafFromBackend: { ...fakeDraftAmendmentFixedTaf, editor: MOCK_USERNAME },
      previousTaf: fakeDraftAmendmentFixedTaf.taf,
      onFormAction: jest.fn(),
    };
    render(
      <TafThemeApiProvider>
        <TafForm {...props} />
      </TafThemeApiProvider>,
    );

    // check editor mode
    expect(screen.getByTestId('switchMode').classList).toContain('Mui-checked');

    const expectedPublishResult = convertTafValuesToObject(
      prepareTafValues(props.tafFromBackend.taf, 'PUBLISH'),
    );

    fireEvent.click(screen.getByTestId('publishtaf'));
    await screen.findByTestId('confirmationDialog-confirm');
    expect(
      await screen.findByText(
        'Are you sure you want to publish the TAF? You did not make any changes!',
      ),
    ).toBeTruthy();
    fireEvent.click(screen.getByTestId('confirmationDialog-confirm'));

    await waitFor(() =>
      expect(props.onFormAction).toHaveBeenLastCalledWith(
        'AMEND',
        expectedPublishResult,
      ),
    );

    expect(expectedPublishResult.location).toEqual(
      props.tafFromBackend.taf.location,
    );
    expect(expectedPublishResult.messageType).toEqual(
      props.tafFromBackend.taf.messageType,
    );
  });

  it('should not mention no changes if you publish a draft_amended taf if there are changes with respect to the original published TAF', async () => {
    // Pass different taf to ensure there are changes
    const props = {
      tafFromBackend: { ...fakeDraftAmendmentFixedTaf, editor: MOCK_USERNAME },
      previousTaf: previousTafForfakeDraftAmendmentFixedTaf.taf,
      onFormAction: jest.fn(),
    };
    render(
      <TafThemeApiProvider>
        <TafForm {...props} />
      </TafThemeApiProvider>,
    );

    // check editor mode
    expect(screen.getByTestId('switchMode').classList).toContain('Mui-checked');

    const expectedPublishResult = convertTafValuesToObject(
      prepareTafValues(props.tafFromBackend.taf, 'PUBLISH'),
    );

    fireEvent.click(screen.getByTestId('publishtaf'));
    await screen.findByTestId('confirmationDialog-confirm');
    expect(
      screen.queryByText(
        'Are you sure you want to publish the TAF? You did not make any changes!',
      ),
    ).toBeFalsy();
    expect(await screen.findByText('Do you want to publish it?')).toBeTruthy();

    fireEvent.click(screen.getByTestId('confirmationDialog-confirm'));

    await waitFor(() =>
      expect(props.onFormAction).toHaveBeenLastCalledWith(
        'AMEND',
        expectedPublishResult,
      ),
    );

    expect(expectedPublishResult.location).toEqual(
      props.tafFromBackend.taf.location,
    );
    expect(expectedPublishResult.messageType).toEqual(
      props.tafFromBackend.taf.messageType,
    );
  });
});
