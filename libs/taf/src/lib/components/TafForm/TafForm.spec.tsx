/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */
import React from 'react';
import {
  act,
  fireEvent,
  render,
  waitFor,
  screen,
  within,
} from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import { showPrompt } from '@opengeoweb/shared';

import {
  fakeNewTaf,
  fakeExpiredTaf,
  fakePublishedTaf,
  fakeDraftTaf,
  fakeDraftAmendmentTaf,
  MOCK_USERNAME,
} from '../../utils/mockdata/fakeTafList';
import { TafThemeApiProvider } from '../Providers';
import TafForm, { getSnackbarMessage } from './TafForm';

jest.mock('../../utils/api');

describe('components/TafForm/TafForm', () => {
  const user = userEvent.setup();
  describe('getSnackbarMessage', () => {
    it('should return correct message for setting to editor mode', () => {
      expect(getSnackbarMessage(true, 'EHAM')).toEqual(
        'You are now the editor for TAF EHAM',
      );
    });
    it('should return correct message for setting to viewer mode', () => {
      expect(getSnackbarMessage(false, 'EHAM')).toEqual(
        'You are now no longer the editor for TAF EHAM',
      );
    });
  });

  it('should display baseforecast for a new TAF and five empty change groups', async () => {
    render(
      <TafThemeApiProvider>
        <TafForm tafFromBackend={{ ...fakeNewTaf, editor: MOCK_USERNAME }} />
      </TafThemeApiProvider>,
    );
    await waitFor(() => {
      expect(screen.getAllByTestId('row-baseForecast').length).toEqual(1);
    });
    expect(screen.getByTestId('row-changeGroups[0]')).toBeTruthy();
    expect(screen.getByTestId('row-changeGroups[1]')).toBeTruthy();
    expect(screen.getByTestId('row-changeGroups[2]')).toBeTruthy();
    expect(screen.getByTestId('row-changeGroups[3]')).toBeTruthy();
    expect(screen.getByTestId('row-changeGroups[4]')).toBeTruthy();
    expect(screen.queryByTestId('row-changeGroups[5]')).toBeFalsy();
    // check editor mode
    expect(screen.getByTestId('switchMode').classList).toContain('Mui-checked');
    expect(screen.getAllByTestId('row-baseForecast').length).toEqual(1);
    expect(screen.getByTestId('row-changeGroups[0]')).toBeTruthy();
    expect(screen.getByTestId('row-changeGroups[1]')).toBeTruthy();
    expect(screen.getByTestId('row-changeGroups[2]')).toBeTruthy();
    expect(screen.getByTestId('row-changeGroups[3]')).toBeTruthy();
    expect(screen.getByTestId('row-changeGroups[4]')).toBeTruthy();
    expect(screen.queryByTestId('row-changeGroups[5]')).toBeFalsy();
    expect(screen.getByTestId('IS_DRAFT').getAttribute('value')).toBeTruthy();
  });
  it('should show all inputs as disabled for an expired taf', async () => {
    const { rerender } = render(
      <TafThemeApiProvider>
        <TafForm isDisabled tafFromBackend={fakeExpiredTaf} />
      </TafThemeApiProvider>,
    );
    const allInputs = screen.getAllByRole('textbox');
    expect(allInputs.length).toBeGreaterThan(0);
    allInputs.forEach((input) => {
      expect(input.classList).toContain('Mui-disabled');
    });
    // check editor mode
    rerender(
      <TafThemeApiProvider>
        <TafForm
          isDisabled
          tafFromBackend={{ ...fakeExpiredTaf, editor: MOCK_USERNAME }}
        />
      </TafThemeApiProvider>,
    );
    expect(screen.getByTestId('switchMode').classList).toContain('Mui-checked');
    const allInputs2 = screen.getAllByRole('textbox');
    expect(allInputs2.length).toBeGreaterThan(0);
    allInputs2.forEach((input) => {
      expect(input.classList).toContain('Mui-disabled');
    });
  });
  it('should show only the first four inputs as disabled for a draft taf', async () => {
    render(
      <TafThemeApiProvider>
        <TafForm tafFromBackend={{ ...fakeDraftTaf, editor: MOCK_USERNAME }} />
      </TafThemeApiProvider>,
    );
    await waitFor(() =>
      // check editor mode
      expect(screen.getByTestId('switchMode').classList).toContain(
        'Mui-checked',
      ),
    );
    const allInputs = screen.getAllByRole('textbox');
    expect(allInputs.length).toBeGreaterThan(0);
    allInputs.forEach((input, index) => {
      if (index < 4) {
        expect(input.classList).toContain('Mui-disabled');
      } else {
        expect(input.classList).not.toContain('Mui-disabled');
      }
    });
  });
  it('should show only the first four inputs as disabled for a new TAF', async () => {
    render(
      <TafThemeApiProvider>
        <TafForm tafFromBackend={{ ...fakeNewTaf, editor: MOCK_USERNAME }} />
      </TafThemeApiProvider>,
    );
    // check editor mode
    await waitFor(() =>
      expect(screen.getByTestId('switchMode').classList).toContain(
        'Mui-checked',
      ),
    );
    const allInputs = screen.getAllByRole('textbox');
    expect(allInputs.length).toBeGreaterThan(0);
    allInputs.forEach((input, index) => {
      if (index < 4) {
        expect(input.classList).toContain('Mui-disabled');
      } else {
        expect(input.classList).not.toContain('Mui-disabled');
      }
    });
  });
  it('should render changeGroups if passed in (and not add an empty ones to the end)', async () => {
    render(
      <TafThemeApiProvider>
        <TafForm tafFromBackend={fakePublishedTaf} />
      </TafThemeApiProvider>,
    );
    // check editor mode
    await waitFor(() =>
      expect(screen.getByTestId('switchMode').classList).not.toContain(
        'Mui-checked',
      ),
    );
    await waitFor(() => {
      expect(screen.getByTestId('row-baseForecast')).toBeTruthy();
    });
    expect(screen.getByTestId('row-changeGroups[0]')).toBeTruthy();
    expect(screen.getByTestId('row-changeGroups[1]')).toBeTruthy();
    // No empty ones at the end
    expect(screen.queryByTestId('row-changeGroups[2]')).toBeFalsy();
  });
  it('should add five empty changegroup to the end if no change groups are passed in in edit mode', async () => {
    render(
      <TafThemeApiProvider>
        <TafForm tafFromBackend={{ ...fakeNewTaf, editor: MOCK_USERNAME }} />
      </TafThemeApiProvider>,
    );
    // check editor mode
    await waitFor(() =>
      expect(screen.getByTestId('switchMode').classList).toContain(
        'Mui-checked',
      ),
    );
    expect(screen.getByTestId('row-baseForecast')).toBeTruthy();
    expect(screen.getByTestId('row-changeGroups[0]'))!.toBeTruthy();
    expect(screen.getByTestId('row-changeGroups[1]'))!.toBeTruthy();
    expect(screen.getByTestId('row-changeGroups[2]')).toBeTruthy();
    expect(screen.getByTestId('row-changeGroups[3]')).toBeTruthy();
    expect(screen.getByTestId('row-changeGroups[4]')).toBeTruthy();
    expect(screen.queryByTestId('row-changeGroups[5]')).toBeFalsy();
  });
  it('should add a changeGroup below or above', async () => {
    render(
      <TafThemeApiProvider>
        <TafForm tafFromBackend={{ ...fakeNewTaf, editor: MOCK_USERNAME }} />
      </TafThemeApiProvider>,
    );
    // check editor mode
    expect(screen.getByTestId('switchMode').classList).toContain('Mui-checked');
    // should focus on baseForecast wind
    await waitFor(() =>
      expect(
        screen
          .getByRole('textbox', { name: 'baseForecast.wind' })
          .matches(':focus'),
      ),
    );
    expect(screen.getByTestId('row-baseForecast')).toBeTruthy();
    expect(screen.getByTestId('row-changeGroups[0]')).toBeTruthy();
    expect(screen.getByTestId('row-changeGroups[1]')).toBeTruthy();
    expect(screen.getByTestId('row-changeGroups[4]')).toBeTruthy();
    expect(screen.queryByTestId('row-changeGroups[5]')).toBeFalsy();
    expect(screen.queryByTestId('row-changeGroups[6]')).toBeFalsy();
    // add row via baseforecast add changegroup button
    fireEvent.click(screen.queryByTestId('tafFormOptions[-1]')!);
    await waitFor(() =>
      expect(screen.queryAllByText('Insert 1 row below').length).toEqual(1),
    );
    fireEvent.click(screen.queryByText('Insert 1 row below')!);
    await waitFor(() => {
      expect(screen.getByTestId('row-changeGroups[0]')).toBeTruthy();
    });
    expect(screen.getByTestId('row-changeGroups[1]')).toBeTruthy();
    expect(screen.getByTestId('row-changeGroups[4]')).toBeTruthy();
    expect(screen.getByTestId('row-changeGroups[5]')).toBeTruthy();
    expect(screen.queryByTestId('row-changeGroups[6]')).toBeFalsy();
    await waitFor(() =>
      // should focus probability new added changegroup
      expect(
        screen
          .getByRole('textbox', { name: 'changeGroups[0].probability' })
          .matches(':focus'),
      ),
    );
    // add row on first changegroup row
    fireEvent.click(screen.queryByTestId('tafFormOptions[0]')!);
    await waitFor(() =>
      expect(screen.queryAllByText('Insert 1 row below').length).toEqual(1),
    );
    fireEvent.click(screen.queryByText('Insert 1 row below')!);
    await waitFor(() => {
      expect(screen.getByTestId('row-changeGroups[0]')).toBeTruthy();
    });
    expect(screen.getByTestId('row-changeGroups[1]')).toBeTruthy();
    expect(screen.getByTestId('row-changeGroups[4]')).toBeTruthy();
    expect(screen.getByTestId('row-changeGroups[5]')).toBeTruthy();
    expect(screen.getByTestId('row-changeGroups[6]')).toBeTruthy();
    expect(screen.queryByTestId('row-changeGroups[7]')).toBeFalsy();
    await waitFor(() =>
      // should focus probability new added changegroup
      expect(
        screen
          .getByRole('textbox', { name: 'changeGroups[1].probability' })
          .matches(':focus'),
      ),
    );
    // add row above second changegroup row
    fireEvent.click(screen.queryByTestId('tafFormOptions[1]')!);
    await waitFor(() =>
      expect(screen.queryAllByText('Insert 1 row above').length).toEqual(1),
    );
    fireEvent.click(screen.queryByText('Insert 1 row above')!);
    await waitFor(() => {
      expect(screen.getByTestId('row-changeGroups[0]')).toBeTruthy();
    });
    expect(screen.getByTestId('row-changeGroups[1]')).toBeTruthy();
    expect(screen.getByTestId('row-changeGroups[4]')).toBeTruthy();
    expect(screen.getByTestId('row-changeGroups[5]')).toBeTruthy();
    expect(screen.getByTestId('row-changeGroups[6]')).toBeTruthy();
    expect(screen.getByTestId('row-changeGroups[7]')).toBeTruthy();
    // should focus probability new added changegroup
    await waitFor(() =>
      expect(
        screen
          .getByRole('textbox', { name: 'changeGroups[1].probability' })
          .matches(':focus'),
      ),
    );
  });
  it('should remove a changeGroup', async () => {
    render(
      <TafThemeApiProvider>
        <TafForm tafFromBackend={{ ...fakeNewTaf, editor: MOCK_USERNAME }} />
      </TafThemeApiProvider>,
    );
    // check editor mode
    expect(screen.getByTestId('switchMode').classList).toContain('Mui-checked');
    // verify rows
    expect(screen.getByTestId('row-changeGroups[0]')).toBeTruthy();
    expect(screen.getByTestId('row-changeGroups[4]')).toBeTruthy();
    expect(screen.queryByTestId('row-changeGroups[5]')).toBeFalsy();
    // add rows
    const optionsButton = screen.getByTestId('tafFormOptions[0]');

    fireEvent.click(optionsButton);
    fireEvent.click(screen.queryByText('Insert 1 row below')!);
    await waitFor(() => {
      expect(screen.getByTestId('row-changeGroups[0]')).toBeTruthy();
    });
    expect(screen.getByTestId('row-changeGroups[1]')).toBeTruthy();
    expect(screen.getByTestId('row-changeGroups[4]')).toBeTruthy();
    expect(screen.getByTestId('row-changeGroups[5]')).toBeTruthy();
    expect(screen.queryByTestId('row-changeGroups[6]')).toBeFalsy();
    // set values
    const row1 = screen.getByRole('textbox', {
      name: 'changeGroups[0].change',
    });
    const row2 = screen.getByRole('textbox', {
      name: 'changeGroups[1].change',
    });
    const row5 = screen.getByRole('textbox', {
      name: 'changeGroups[4].change',
    });
    const row6 = screen.getByRole('textbox', {
      name: 'changeGroups[5].change',
    });
    fireEvent.change(row1!, { target: { value: 1 } });
    fireEvent.change(row2!, { target: { value: 2 } });
    fireEvent.change(row5!, { target: { value: 5 } });
    fireEvent.change(row6!, { target: { value: 6 } });
    await waitFor(() => {
      expect(row1!.getAttribute('value')).toEqual('1');
    });
    expect(row2!.getAttribute('value')).toEqual('2');
    expect(row5!.getAttribute('value')).toEqual('5');
    expect(row6!.getAttribute('value')).toEqual('6');
    // remove last row
    fireEvent.click(screen.getByTestId('tafFormOptions[5]'));
    await waitFor(() =>
      expect(screen.queryAllByText('Delete row').length).toEqual(1),
    );
    fireEvent.click(screen.queryByText('Delete row')!);

    // eslint-disable-next-line testing-library/no-unnecessary-act
    await act(async () => {
      fireEvent.click(screen.queryByTestId('confirmationDialog-confirm')!);
    });

    const changeField = await screen.findByRole('textbox', {
      name: 'changeGroups[0].change',
    });
    await waitFor(() => {
      expect(changeField.getAttribute('value')).toEqual('1');
    });

    expect(
      screen
        .getByRole('textbox', {
          name: 'changeGroups[4].change',
        })
        .getAttribute('value'),
    ).toEqual('5');
    expect(screen.queryByTestId('row-changeGroups[5]')).toBeFalsy();

    fireEvent.click(screen.getByTestId('tafFormOptions[0]'));
    await waitFor(() => {
      expect(screen.queryAllByText('Delete row').length).toEqual(1);
    });
    fireEvent.click(screen.queryByText('Delete row')!);
    // eslint-disable-next-line testing-library/no-unnecessary-act
    await act(async () => {
      fireEvent.click(screen.queryByTestId('confirmationDialog-confirm')!);
    });
    await waitFor(() => {
      expect(
        screen
          .getByRole('textbox', {
            name: 'changeGroups[0].change',
          })
          .getAttribute('value'),
      ).toEqual('2');
    });

    expect(
      screen
        .getByRole('textbox', {
          name: 'changeGroups[3].change',
        })
        .getAttribute('value'),
    ).toEqual('5');
    expect(screen.queryByTestId('row-changeGroups[4]')).toBeFalsy();
  });
  it('should clear a row', async () => {
    render(
      <TafThemeApiProvider>
        <TafForm
          tafFromBackend={{ ...fakeDraftAmendmentTaf, editor: MOCK_USERNAME }}
        />
      </TafThemeApiProvider>,
    );
    // check editor mode
    expect(screen.getByTestId('switchMode').classList).toContain('Mui-checked');
    // verify data
    await waitFor(() => {
      expect(
        screen
          .getByRole('textbox', {
            name: 'changeGroups[0].change',
          })
          .getAttribute('value'),
      ).toEqual(fakeDraftAmendmentTaf.taf.changeGroups![0].change);
    });
    expect(
      screen
        .getByRole('textbox', {
          name: 'baseForecast.weather.weather1',
        })
        .getAttribute('value'),
    ).toEqual(fakeDraftAmendmentTaf.taf.baseForecast.weather!.weather1);
    // clear changegroup
    fireEvent.click(screen.getByTestId('tafFormOptions[0]'));
    fireEvent.click(screen.queryByText('Clear row')!);
    // eslint-disable-next-line testing-library/no-unnecessary-act
    await act(async () => {
      fireEvent.click(screen.queryByTestId('confirmationDialog-confirm')!);
    });

    const change = await screen.findByRole('textbox', {
      name: 'changeGroups[0].change',
    });
    await waitFor(() => {
      expect(change.getAttribute('value')).toEqual('');
    });
    // clear baseforecast
    fireEvent.click(screen.getByTestId('tafFormOptions[-1]'));
    fireEvent.click(screen.queryByText('Clear row')!);
    // eslint-disable-next-line testing-library/no-unnecessary-act
    await act(async () => {
      fireEvent.click(screen.queryByTestId('confirmationDialog-confirm')!);
    });

    const weather1Field = await screen.findByRole('textbox', {
      name: 'baseForecast.weather.weather1',
    });
    await waitFor(() => {
      expect(weather1Field.getAttribute('value')).toEqual('');
    });

    expect(
      screen
        .getByRole('textbox', { name: 'baseForecast.valid' })
        .getAttribute('value'),
    ).toBeTruthy(); // should not be cleared
  });
  it('should be possible to sort rows', async () => {
    render(
      <TafThemeApiProvider>
        <TafForm tafFromBackend={{ ...fakeNewTaf, editor: MOCK_USERNAME }} />
      </TafThemeApiProvider>,
    );
    // check editor mode
    expect(screen.getByTestId('switchMode').classList).toContain('Mui-checked');

    // Standard we have 5 rows
    await waitFor(() => {
      expect(screen.getByTestId('row-changeGroups[0]')).toBeTruthy();
    });
    expect(
      within(screen.getByTestId('row-changeGroups[0]')!)
        .queryByTestId('dragHandle-0')!
        .getAttribute('disabled'),
    ).toBeFalsy();
    expect(screen.getByTestId('row-changeGroups[1]')).toBeTruthy();
    expect(
      within(screen.getByTestId('row-changeGroups[1]')!)
        .queryByTestId('dragHandle-1')!
        .getAttribute('disabled'),
    ).toBeFalsy();

    // remove rows
    fireEvent.click(screen.queryByTestId('tafFormOptions[4]')!);
    fireEvent.click(screen.queryByText('Delete row')!);
    fireEvent.click(screen.queryByTestId('confirmationDialog-confirm')!);

    fireEvent.click(screen.queryByTestId('tafFormOptions[3]')!);
    await waitFor(async () => {
      expect(screen.getByText('Delete row')).toBeTruthy();
    });
    fireEvent.click(screen.queryByText('Delete row')!);
    fireEvent.click(screen.queryByTestId('confirmationDialog-confirm')!);

    fireEvent.click(screen.queryByTestId('tafFormOptions[2]')!);

    await waitFor(async () => {
      expect(screen.getByText('Delete row')).toBeTruthy();
    });

    fireEvent.click(screen.queryByText('Delete row')!);
    fireEvent.click(screen.queryByTestId('confirmationDialog-confirm')!);
    fireEvent.click(screen.queryByTestId('tafFormOptions[1]')!);

    await waitFor(async () => {
      expect(screen.getByText('Delete row')).toBeTruthy();
    });
    fireEvent.click(screen.queryByText('Delete row')!);
    fireEvent.click(screen.queryByTestId('confirmationDialog-confirm')!);

    await waitFor(() => {
      expect(screen.getByTestId('row-baseForecast')).toBeTruthy();
    });
    expect(screen.getByTestId('row-changeGroups[0]')).toBeTruthy();
    // not able to drag if only 1 row
    expect(
      within(screen.getByTestId('row-changeGroups[0]')!)
        .getByTestId('dragHandle-0')!
        .getAttribute('disabled'),
    ).toBeDefined();
    expect(screen.queryByTestId('row-changeGroups[1]')).toBeFalsy();
  });
  it('should show discard, save and publish buttons for a new taf when user is editor', async () => {
    render(
      <TafThemeApiProvider>
        <TafForm tafFromBackend={{ ...fakeNewTaf, editor: MOCK_USERNAME }} />
      </TafThemeApiProvider>,
    );
    // check editor mode
    expect(screen.getByTestId('switchMode').classList).toContain('Mui-checked');
    await waitFor(() => {
      expect(screen.getByTestId('cleartaf')).toBeTruthy();
    });
    expect(screen.getByTestId('savedrafttaf')).toBeTruthy();
    expect(screen.getByTestId('publishtaf')).toBeTruthy();
  });
  it('should show discard, save and publish buttons for a draft taf when user is editor', async () => {
    render(
      <TafThemeApiProvider>
        <TafForm tafFromBackend={{ ...fakeDraftTaf, editor: MOCK_USERNAME }} />
      </TafThemeApiProvider>,
    );
    // check editor mode
    expect(screen.getByTestId('switchMode').classList).toContain('Mui-checked');
    await waitFor(() => {
      expect(screen.getByTestId('cleartaf')).toBeTruthy();
    });
    expect(screen.getByTestId('savedrafttaf')).toBeTruthy();
    expect(screen.getByTestId('publishtaf')).toBeTruthy();
  });
  it('should not show action buttons for expired taf', async () => {
    render(
      <TafThemeApiProvider>
        <TafForm
          isDisabled
          tafFromBackend={{ ...fakeExpiredTaf, editor: MOCK_USERNAME }}
        />
      </TafThemeApiProvider>,
    );
    expect(screen.queryByTestId('cleartaf')).toBeFalsy();
    expect(screen.queryByTestId('savedrafttaf')).toBeFalsy();
    expect(screen.queryByTestId('publishtaf')).toBeFalsy();
    // check editor mode
    expect(screen.getByTestId('switchMode').classList).toContain('Mui-checked');

    expect(screen.queryByTestId('cleartaf')).toBeFalsy();
    expect(screen.queryByTestId('savedrafttaf')).toBeFalsy();
    expect(screen.queryByTestId('publishtaf')).toBeFalsy();
  });
  it('should show browser prompt when making a change and reloading the page', async () => {
    const spy = jest.spyOn(window, 'addEventListener');
    const { baseElement } = render(
      <TafThemeApiProvider>
        <TafForm
          tafFromBackend={{ ...fakeDraftTaf, editor: MOCK_USERNAME }}
          onFormAction={jest.fn()}
        />
      </TafThemeApiProvider>,
    );
    // check editor mode
    expect(screen.getByTestId('switchMode').classList).toContain('Mui-checked');
    // add wind input
    const windInput = screen.getByRole('textbox', {
      name: 'baseForecast.wind',
    });
    fireEvent.change(windInput!, { target: { value: '12010' } });
    // reload page
    fireEvent.keyDown(baseElement, { key: 'F5', code: 'F5' });
    await waitFor(() => {
      expect(spy).toHaveBeenCalledWith('beforeunload', showPrompt);
    });
  });
  it('should not show browser prompt when not changing anything and reloading the page', async () => {
    const spy = jest.spyOn(window, 'removeEventListener');
    const { container } = render(
      <TafThemeApiProvider>
        <TafForm
          tafFromBackend={{ ...fakeDraftTaf, editor: MOCK_USERNAME }}
          onFormAction={jest.fn()}
        />
      </TafThemeApiProvider>,
    );
    // check editor mode
    expect(screen.getByTestId('switchMode').classList).toContain('Mui-checked');
    // reload page
    fireEvent.keyDown(container, { key: 'F5', code: 'F5' });
    await waitFor(() =>
      expect(spy).toHaveBeenCalledWith('beforeunload', showPrompt),
    );
  });
  it('should always show the confirmation dialog when pressing clear', async () => {
    const props = {
      tafFromBackend: { ...fakeNewTaf, editor: MOCK_USERNAME },
      onFormAction: jest.fn(),
    };
    render(
      <TafThemeApiProvider>
        <TafForm {...props} />
      </TafThemeApiProvider>,
    );
    // check editor mode
    expect(screen.getByTestId('switchMode').classList).toContain('Mui-checked');
    // add wind input
    const windInput = await screen.findByRole('textbox', {
      name: 'baseForecast.wind',
      hidden: true,
    });
    fireEvent.change(windInput!, { target: { value: '12010' } });
    fireEvent.click(screen.getByTestId('cleartaf'));
    await screen.findByTestId('confirmationDialog');
    fireEvent.click(screen.getByTestId('confirmationDialog-confirm'));
    await waitFor(() => {
      expect(props.onFormAction).not.toHaveBeenCalled();
    });
    // form should be reset
    await waitFor(() => {
      expect(
        screen
          .getByRole('textbox', { name: 'baseForecast.wind' })
          .getAttribute('value'),
      ).toEqual('');
    });
  });
  it('should not show any errors when opening a new TAF and after using TAB', async () => {
    render(
      <TafThemeApiProvider>
        <TafForm tafFromBackend={{ ...fakeNewTaf, editor: MOCK_USERNAME }} />
      </TafThemeApiProvider>,
    );
    // check editor mode
    expect(screen.getByTestId('switchMode').classList).toContain('Mui-checked');
    expect(screen.getAllByTestId('row-baseForecast').length).toEqual(1);
    expect(screen.getByTestId('row-changeGroups[0]')).toBeTruthy();
    expect(
      screen
        .queryAllByRole('textbox')
        .filter((field) => field.getAttribute('aria-invalid') === 'true'),
    ).toHaveLength(0);
    // first field has focus
    expect(
      screen
        .getByRole('textbox', {
          name: 'baseForecast.wind',
        })
        .matches(':focus'),
    );
    // change focus to next field
    await user.tab();
    // next field has focus
    await waitFor(() => {
      expect(
        screen
          .getByRole('textbox', {
            name: 'baseForecast.visibility',
          })
          .matches(':focus'),
      );
    });
    expect(
      screen
        .queryAllByRole('textbox')
        .filter((field) => field.getAttribute('aria-invalid') === 'true'),
    ).toHaveLength(0);
  });
  it('should update the form values when the taf values changed', async () => {
    const { rerender } = render(
      <TafThemeApiProvider>
        <TafForm
          isDisabled
          tafFromBackend={{
            ...fakeNewTaf,
            editor: MOCK_USERNAME,
          }}
        />
      </TafThemeApiProvider>,
    );
    // make sure we are in editor mode
    expect(screen.getByTestId('switchMode').classList).toContain('Mui-checked');
    expect(
      screen.getByRole('textbox', { name: 'issueDate' }).getAttribute('value'),
    ).toEqual('Not issued');
    rerender(
      <TafThemeApiProvider>
        <TafForm isDisabled tafFromBackend={fakePublishedTaf} />
      </TafThemeApiProvider>,
    );
    expect(
      screen.getByRole('textbox', { name: 'issueDate' }).getAttribute('value'),
    ).not.toEqual('Not issued');
    // make sure we are in viewer mode
    expect(screen.getByTestId('switchMode').classList).not.toContain(
      'Mui-checked',
    );
  });
});
