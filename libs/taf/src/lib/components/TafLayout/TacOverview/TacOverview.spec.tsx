/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */
import React from 'react';
import {
  fireEvent,
  render,
  waitFor,
  screen,
  within,
} from '@testing-library/react';

import TacOverview from './TacOverview';
import { TafThemeApiProvider } from '../../Providers';
import {
  fakeAmendmentFixedTaf,
  fakeDraftAmendmentFixedTaf,
  fakeDraftFixedTaf,
  fakeNewFixedTaf,
  fakePublishedFixedTaf,
} from '../../../utils/mockdata/fakeTafList';
import { fakeTestTac } from '../../../utils/__mocks__/api';

jest.mock('../../../utils/api');

describe('components/TafLayout/TacOverview/TacOverview', () => {
  beforeEach(() => {
    jest.useFakeTimers();
  });
  afterEach(() => {
    jest.clearAllTimers();
    jest.useRealTimers();
  });

  it('should display a list of tafs', async () => {
    Element.prototype.scrollTo = jest.fn();
    const upcomingTafs = [
      fakeNewFixedTaf,
      fakeDraftFixedTaf,
      fakePublishedFixedTaf,
    ];
    const currentTafs = [fakeDraftAmendmentFixedTaf, fakeAmendmentFixedTaf];
    const expiredTafs = upcomingTafs;
    const props = {
      upcomingTafs,
      currentTafs,
      expiredTafs,
    };
    render(
      <TafThemeApiProvider>
        <TacOverview {...props} />
      </TafThemeApiProvider>,
    );

    await waitFor(() =>
      expect(
        screen.queryAllByTestId('adverse-weather-tac')[0].textContent,
      ).toContain(fakeTestTac),
    );
    expect(
      within(screen.getByTestId('tab-panel-0')).getAllByRole('listitem'),
    ).toHaveLength(upcomingTafs.length);
    expect(
      within(screen.getByTestId('tab-panel-1')).queryAllByRole('listitem'),
    ).toHaveLength(0);
    jest.runOnlyPendingTimers();

    await waitFor(() =>
      expect(Element.prototype.scrollTo).toHaveBeenCalledTimes(1),
    );
  });

  it('should be able to toggle tab list', async () => {
    Element.prototype.scrollTo = jest.fn();

    const upcomingTafs = [
      fakeNewFixedTaf,
      fakeDraftFixedTaf,
      fakePublishedFixedTaf,
    ];
    const currentTafs = [fakeDraftAmendmentFixedTaf, fakeAmendmentFixedTaf];
    const expiredTafs = upcomingTafs;
    const props = {
      upcomingTafs,
      currentTafs,
      expiredTafs,
    };
    render(
      <TafThemeApiProvider>
        <TacOverview {...props} />
      </TafThemeApiProvider>,
    );

    await waitFor(() =>
      expect(
        screen.queryAllByTestId('adverse-weather-tac')[0].textContent,
      ).toContain(fakeTestTac),
    );
    expect(
      within(screen.getByTestId('tab-panel-0')).getAllByRole('listitem'),
    ).toHaveLength(upcomingTafs.length);
    expect(
      within(screen.getByTestId('tab-panel-1')).queryAllByRole('listitem'),
    ).toHaveLength(0);
    jest.runOnlyPendingTimers();

    await waitFor(() =>
      expect(Element.prototype.scrollTo).toHaveBeenCalledTimes(1),
    );
    // click Current & Expired
    fireEvent.click(screen.getByTestId('tab-1'));
    expect(
      within(screen.getByTestId('tab-panel-0')).queryAllByRole('listitem'),
    ).toHaveLength(0);
    expect(
      within(screen.getByTestId('tab-panel-1')).getAllByRole('listitem'),
    ).toHaveLength(currentTafs.length + expiredTafs.length);

    await waitFor(() =>
      expect(
        screen.queryAllByTestId('adverse-weather-tac')[0].textContent,
      ).toContain(fakeTestTac),
    );
    jest.runOnlyPendingTimers();

    await waitFor(() =>
      expect(Element.prototype.scrollTo).toHaveBeenCalledTimes(3),
    );
    // click Upcoming
    fireEvent.click(screen.getByTestId('tab-0'));

    expect(
      within(screen.getByTestId('tab-panel-0')).getAllByRole('listitem'),
    ).toHaveLength(upcomingTafs.length);
    expect(
      within(screen.getByTestId('tab-panel-1')).queryAllByRole('listitem'),
    ).toHaveLength(0);
    await waitFor(() =>
      expect(
        screen.queryAllByTestId('adverse-weather-tac')[0].textContent,
      ).toContain(fakeTestTac),
    );
    jest.runOnlyPendingTimers();

    await waitFor(() =>
      expect(Element.prototype.scrollTo).toHaveBeenCalledTimes(5),
    );
  });
});
