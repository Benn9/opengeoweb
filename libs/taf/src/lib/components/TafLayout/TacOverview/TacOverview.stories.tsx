/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */
import * as React from 'react';
import { darkTheme } from '@opengeoweb/theme';
import { Box } from '@mui/material';
import { TafThemeApiProvider } from '../../Providers';
import {
  fakeAmendmentFixedTaf,
  fakeDraftAmendmentFixedTaf,
  fakeDraftFixedTaf,
  fakeNewFixedTaf,
  fakePublishedFixedTaf,
} from '../../../utils/mockdata/fakeTafList';
import TacOverview from './TacOverview';
import { fakeTafTac, createApi as createFakeApi } from '../../../utils/fakeApi';
import { TafApi } from '../../../utils/api';
import adverseWeatherTAC from '../../../utils/mockdata/fakeTAC.json';
import { AdverseWeatherTAC, TAC } from '../../../types';

export default { title: 'components/Taf Layout/TacOverview' };

const TacOverviewDemo = (): React.ReactElement => {
  return (
    <Box sx={{ padding: '10px' }}>
      <TacOverview
        upcomingTafs={[
          fakeDraftFixedTaf,
          fakePublishedFixedTaf,
          fakeNewFixedTaf,
        ]}
        currentTafs={[fakeDraftAmendmentFixedTaf, fakeAmendmentFixedTaf]}
        expiredTafs={[]}
        activeTaf={fakePublishedFixedTaf}
      />
    </Box>
  );
};

export const TacOverviewWithError = (): React.ReactElement => {
  return (
    <div style={{ width: '300px', height: '460px' }}>
      <TafThemeApiProvider>
        <TacOverviewDemo />
      </TafThemeApiProvider>
    </div>
  );
};

const tacEHLE: AdverseWeatherTAC = {
  tac: [
    [
      { text: 'TAF' },
      { text: 'EHLE' },
      { text: '2912/3018' },
      { text: '06007KT' },
      { text: '7000' },
      { text: 'BKN006', type: 'adverse' },
    ],
    [
      { text: 'BECMG' },
      { text: '2914/2917' },
      { text: '9999' },
      { text: 'FEW025' },
    ],
    [
      { text: 'TEMPO' },
      { text: '3006/3010' },
      { text: 'BKN012' },
      { text: '=' },
    ],
  ],
};

const tacEHRD: AdverseWeatherTAC = {
  tac: [
    [
      { text: 'TAF' },
      { text: 'EHRD' },
      { text: '2906/3012' },
      { text: '07007KT' },
      { text: '0600', type: 'adverse' },
      { text: 'FG', type: 'adverse' },
      { text: 'VV000', type: 'adverse' },
    ],
    [
      { text: 'BECMG' },
      { text: '2906/2908' },
      { text: '3000' },
      { text: 'BR' },
      { text: 'BKN003', type: 'adverse' },
    ],
    [
      { text: 'BECMG' },
      { text: '2908/2911' },
      { text: '9999' },
      { text: 'NSW' },
      { text: 'FEW025' },
    ],
    [{ text: 'BECMG' }, { text: '2915/2918' }, { text: '04012KT' }],
    [
      { text: 'BECMG' },
      { text: '3000/3003' },
      { text: '03005KT' },
      { text: '=' },
    ],
  ],
};

const tacEHAM: AdverseWeatherTAC = {
  tac: [
    [
      { text: 'TAF' },
      { text: 'EHAM' },
      { text: '2906/3012' },
      { text: '07007KT' },
      { text: '7000' },
      { text: 'BKN006', type: 'adverse' },
    ],
    [
      { text: 'BECMG' },
      { text: '2908/2911' },
      { text: '9999' },
      { text: 'FEW025' },
    ],
    [{ text: 'BECMG' }, { text: '2915/2918' }, { text: '04012KT' }],
    [{ text: 'BECMG' }, { text: '2921/2924' }, { text: '03005KT' }],
    [{ text: 'TEMPO' }, { text: '3006/3010' }, { text: 'BKN012' }],
    [
      { text: 'BECMG' },
      { text: '3009/3012' },
      { text: '04011KT' },
      { text: '=' },
    ],
  ],
};

const fakeGetTac = (): TafApi => {
  return {
    ...createFakeApi(),
    getTAC: (): Promise<{ data: TAC }> => {
      return new Promise((resolve) => {
        const randomNumber = Math.floor(Math.random() * 4) + 1;
        switch (randomNumber) {
          case 1:
            resolve({
              data: tacEHAM,
            });
            break;
          case 2:
            resolve({
              data: tacEHRD,
            });
            break;
          case 3:
            resolve({
              data: tacEHLE,
            });
            break;
          default:
            resolve({
              data: fakeTafTac,
            });
            break;
        }
      });
    },
  };
};

export const TacOverviewWithDifferentTAC = (): React.ReactElement => {
  return (
    <div style={{ width: '300px', height: '460px' }}>
      <TafThemeApiProvider createApiFunc={fakeGetTac}>
        <TacOverviewDemo />
      </TafThemeApiProvider>
    </div>
  );
};

const apiAdverseWeatherTAC = (): TafApi => {
  return {
    ...createFakeApi(),
    getTAC: (): Promise<{ data: TAC }> => {
      return new Promise((resolve) => {
        resolve({ data: adverseWeatherTAC });
      });
    },
  };
};

export const TacOverviewAdverseWeatherLight = (): React.ReactElement => {
  return (
    <div style={{ width: '300px', height: '460px' }}>
      <TafThemeApiProvider createApiFunc={apiAdverseWeatherTAC}>
        <TacOverviewDemo />
      </TafThemeApiProvider>
    </div>
  );
};
TacOverviewAdverseWeatherLight.storyName =
  'Tac overview Adverse Weather light (takeSnapshot)';

export const TacOverviewAdverseWeatherDark = (): React.ReactElement => {
  return (
    <div style={{ width: '300px', height: '460px' }}>
      <TafThemeApiProvider
        theme={darkTheme}
        createApiFunc={apiAdverseWeatherTAC}
      >
        <TacOverviewDemo />
      </TafThemeApiProvider>
    </div>
  );
};
TacOverviewAdverseWeatherDark.storyName =
  'Tac overview Adverse Weather dark (takeSnapshot)';

export const TacOverviewLightTheme = (): React.ReactElement => {
  return (
    <div style={{ width: '300px', height: '460px' }}>
      <TafThemeApiProvider createApiFunc={createFakeApi}>
        <TacOverviewDemo />
      </TafThemeApiProvider>
    </div>
  );
};
TacOverviewLightTheme.storyName = 'Tac overview light';

export const TacOverviewDarkTheme = (): React.ReactElement => {
  return (
    <div style={{ width: '300px', height: '460px' }}>
      <TafThemeApiProvider theme={darkTheme} createApiFunc={createFakeApi}>
        <TacOverviewDemo />
      </TafThemeApiProvider>
    </div>
  );
};
TacOverviewDarkTheme.storyName = 'Tac overview dark';
