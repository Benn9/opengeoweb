/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */

import React from 'react';
import { Box, Typography } from '@mui/material';
import { useFormContext } from 'react-hook-form';
import { useDispatch } from 'react-redux';
import { useApiContext } from '@opengeoweb/api';
import { snackbarActions } from '@opengeoweb/snackbar';
import { tacHasError, useTACGenerator } from '../../TafForm/utils';
import { Taf, TafFormData } from '../../../types';
import StatusIcon from '../../StatusIcon/StatusIcon';
import {
  getLocationSetting,
  getTafTimeSlot,
  prepareTafExportValues,
} from '../utils';
import ExportButton from './ExportButton';
import { TafApi } from '../../../utils/api';
import { AdverseWeatherTACMessage } from './AdverseWeatherTacMessage';

export const getSnackbarMessage = (
  location: string,
  locationLabel: string,
): string => {
  return `TAF ${location} ${locationLabel} was imported successfully`;
};

interface TacContainerProps {
  taf: Taf;
  isActive?: boolean;
  isFormDisabled?: boolean;
}

const TacContainer: React.FC<TacContainerProps> = ({
  taf,
  isActive = false,
  isFormDisabled = false,
}: TacContainerProps) => {
  const { api } = useApiContext<TafApi>();
  const dispatch = useDispatch();
  const [TAC, retrieveTAC] = useTACGenerator(taf, api.getTAC, true);
  const { status, location, baseTime } = taf;
  const locationLabel = getLocationSetting(location).label;
  const timeSlot = getTafTimeSlot(baseTime, location, status);
  const { getValues, trigger, setValue } = useFormContext();

  const showSnackbar = React.useCallback(
    (message: string) => {
      dispatch(snackbarActions.openSnackbar({ message }));
    },
    [dispatch],
  );

  const handleExport = (): void => {
    const currentTaf = getValues() as TafFormData;
    const newTaf = prepareTafExportValues(taf, currentTaf);
    setValue('baseForecast', newTaf.baseForecast, {
      shouldDirty: true,
    });
    setValue('changeGroups', newTaf.changeGroups, {
      shouldDirty: true,
    });

    setTimeout(() => {
      trigger(); // trigger all fields validation
    }, 0);
    showSnackbar(getSnackbarMessage(location, locationLabel));
  };

  React.useEffect(() => {
    if (isActive) {
      retrieveTAC(taf);
    }
  }, [taf, isActive, retrieveTAC]);

  return (
    <Box
      sx={{
        marginBottom: '20px',
      }}
      data-location={location}
      className={isActive ? 'active' : undefined}
      role="listitem"
      aria-label={location}
    >
      <Box
        sx={{
          display: 'flex',
          alignItems: 'center',
          justifyContent: 'space-between',
          height: 24,
        }}
      >
        <Box
          sx={{
            display: 'flex',
            alignItems: 'center',
          }}
        >
          <Box
            sx={{
              width: 24,
              height: 24,
              display: 'flex',
              marginRight: '2px',
              paddingTop: '6px',
              alignItems: 'center',
              justifyContent: 'center',
            }}
          >
            <StatusIcon timeSlot={timeSlot} status={status} />
          </Box>

          <Typography
            sx={{
              lineHeight: 1,
              ...(isActive && {
                backgroundColor:
                  'geowebColors.functional.warningHighlight.rgba',
                padding: '0 2px',
                marginLeft: '-2px',
              }),
            }}
          >
            <Typography
              sx={{
                fontSize: '12px',
                color: 'geowebColors.captions.captionInformation.rgba',
              }}
              data-testid="tafLocation"
              component="span"
            >
              {location}
            </Typography>

            <Typography
              sx={{
                fontSize: '12px',
                marginLeft: '10px',
              }}
              data-testid="tafLocationLabel"
              component="span"
            >
              {locationLabel}
            </Typography>
          </Typography>
        </Box>
        <ExportButton
          onClick={handleExport}
          isHidden={isFormDisabled || tacHasError(TAC)}
        />
      </Box>
      <Box
        sx={{
          fontSize: '12px',
          letterSpacing: '0.4px',
          color: 'geowebColors.captions.captionStatus.rgba',
        }}
      >
        {typeof TAC === 'string' ? (
          <Box
            component="span"
            sx={{
              display: 'inline',
              boxDecorationBreak: 'clone',
              WebkitBoxDecorationBreak: 'clone',
              ...(isActive && {
                backgroundColor:
                  'geowebColors.functional.warningHighlight.rgba',
              }),
            }}
          >
            {TAC}
          </Box>
        ) : (
          <AdverseWeatherTACMessage TAC={TAC} isActive={isActive} />
        )}
      </Box>
    </Box>
  );
};

export default TacContainer;
