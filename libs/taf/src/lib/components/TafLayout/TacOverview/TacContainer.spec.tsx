/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */
import React from 'react';
import { fireEvent, render, waitFor, screen } from '@testing-library/react';

import TacContainer, { getSnackbarMessage } from './TacContainer';
import { TafThemeApiProvider } from '../../Providers';
import { getLocationSetting } from '../utils';
import {
  fakeExpiredTaf,
  fakeNewTaf,
  fakePublishedFixedTaf,
} from '../../../utils/mockdata/fakeTafList';
import { fakeTestTac, fakeTestTacJson } from '../../../utils/__mocks__/api';
import { TAC_NOT_AVAILABLE } from '../../TafForm/utils';
import { createApi } from '../../../utils/fakeApi';
import { TafApi } from '../../../utils/api';
import adverseWeatherTAC from '../../../utils/mockdata/fakeTAC.json';
import { TAC } from '../../../types';

jest.mock('../../../utils/api');

describe('components/TafLayout/TacOverview/TacContainer', () => {
  describe('getSnackbarMessage', () => {
    it('should return correct message', () => {
      expect(getSnackbarMessage('LOCATION', 'LOCATION_NANE')).toEqual(
        'TAF LOCATION LOCATION_NANE was imported successfully',
      );
    });
  });

  it('should display a TacContainer for a new taf without export button', async () => {
    render(
      <TafThemeApiProvider>
        <TacContainer taf={fakeNewTaf.taf} />
      </TafThemeApiProvider>,
    );

    expect(
      screen.getByRole('listitem', { name: fakeNewTaf.taf.location }).className,
    ).not.toContain('active');

    expect(screen.getByTestId('tafLocation').innerHTML).toEqual(
      fakeNewTaf.taf.location,
    );
    expect(screen.getByTestId('tafLocationLabel').innerHTML).toEqual(
      getLocationSetting(fakeNewTaf.taf.location).label,
    );
    expect(await screen.findByText(TAC_NOT_AVAILABLE)).toBeTruthy();
    expect(screen.queryByTestId('export-tac-button')).toBeFalsy();
  });

  it('should display a TacContainer for an expired taf with correct icon and export button', async () => {
    render(
      <TafThemeApiProvider>
        <TacContainer taf={fakeExpiredTaf.taf} />
      </TafThemeApiProvider>,
    );

    expect(screen.getByTestId('tafLocation').innerHTML).toEqual(
      fakeExpiredTaf.taf.location,
    );
    expect(screen.getByTestId('tafLocationLabel').innerHTML).toEqual(
      getLocationSetting(fakeExpiredTaf.taf.location).label,
    );
    await waitFor(() =>
      expect(
        screen.queryAllByTestId('adverse-weather-tac')[0].textContent,
      ).toContain(fakeTestTac),
    );
    expect(screen.getByTestId('status-expired')).toBeTruthy();
    expect(screen.getByTestId('export-tac-button')).toBeTruthy();
  });

  it('should display an active TacContainer', async () => {
    render(
      <TafThemeApiProvider>
        <TacContainer taf={fakeExpiredTaf.taf} isActive />
      </TafThemeApiProvider>,
    );
    await waitFor(() => {
      expect(
        screen.getByRole('listitem', { name: fakeExpiredTaf.taf.location })
          .className,
      ).toContain('active');
    });
  });

  it('should fetch a new TAC when taf becomes active', async () => {
    const props = {
      taf: fakeExpiredTaf.taf,
      isActive: false,
    };

    const mockGetTac = jest.fn(() => {
      return new Promise<{ data: TAC }>((resolve) => {
        resolve({ data: fakeTestTacJson });
      });
    });

    const createFakeApi = (): TafApi => ({
      ...createApi(),
      getTAC: mockGetTac,
    });

    const Wrapper: React.FC<{ children: React.ReactNode }> = ({ children }) => (
      <TafThemeApiProvider createApiFunc={createFakeApi}>
        {children}
      </TafThemeApiProvider>
    );

    const { rerender } = render(
      <Wrapper>
        <TacContainer {...props} />
      </Wrapper>,
    );

    await waitFor(() => {
      expect(mockGetTac).toHaveBeenCalledTimes(1);
    });

    const newProps = {
      taf: fakeExpiredTaf.taf,
      isActive: true,
    };

    rerender(
      <Wrapper>
        <TacContainer {...newProps} />
      </Wrapper>,
    );

    await waitFor(() => {
      expect(mockGetTac).toHaveBeenCalledTimes(2);
    });
  });

  it('should fetch a new TAC when incoming taf changes', async () => {
    const props = {
      taf: fakeExpiredTaf.taf,
      isActive: false,
    };

    const mockGetTac = jest.fn(() => {
      return new Promise<{ data: TAC }>((resolve) => {
        resolve({ data: fakeTestTacJson });
      });
    });

    const createFakeApi = (): TafApi => ({
      ...createApi(),
      getTAC: mockGetTac,
    });

    const Wrapper: React.FC<{ children: React.ReactNode }> = ({ children }) => (
      <TafThemeApiProvider createApiFunc={createFakeApi}>
        {children}
      </TafThemeApiProvider>
    );

    const { rerender } = render(
      <Wrapper>
        <TacContainer {...props} />
      </Wrapper>,
    );

    await waitFor(() => {
      expect(mockGetTac).toHaveBeenCalledTimes(1);
    });

    const newProps = {
      taf: fakePublishedFixedTaf.taf,
      isActive: false,
    };

    rerender(
      <Wrapper>
        <TacContainer {...newProps} />
      </Wrapper>,
    );
    await waitFor(() => {
      expect(mockGetTac).toHaveBeenCalledTimes(2);
    });
  });
  it('should not display the export button when form is disabled', async () => {
    render(
      <TafThemeApiProvider>
        <TacContainer taf={fakePublishedFixedTaf.taf} isFormDisabled />
      </TafThemeApiProvider>,
    );
    await waitFor(() =>
      expect(
        screen.queryAllByTestId('adverse-weather-tac')[0].textContent,
      ).toContain(fakeTestTac),
    );
    expect(screen.queryByTestId('export-tac-button')).toBeFalsy();
  });

  it('should show a snackbar when exporting a TAF', async () => {
    render(
      <TafThemeApiProvider>
        <TacContainer taf={fakeExpiredTaf.taf} />
      </TafThemeApiProvider>,
    );

    expect(screen.getByTestId('tafLocation').innerHTML).toEqual(
      fakeExpiredTaf.taf.location,
    );
    const locationLabel = getLocationSetting(fakeExpiredTaf.taf.location).label;
    expect(screen.getByTestId('tafLocationLabel').innerHTML).toEqual(
      locationLabel,
    );
    await waitFor(() =>
      expect(
        screen.queryAllByTestId('adverse-weather-tac')[0].textContent,
      ).toContain(fakeTestTac),
    );
    expect(screen.getByTestId('status-expired')).toBeTruthy();
    expect(screen.getByTestId('export-tac-button')).toBeTruthy();
    fireEvent.click(screen.queryByTestId('export-tac-button')!);

    expect(screen.getByTestId('snackbarComponent')).toBeTruthy();
    expect(
      await screen.findByText(
        getSnackbarMessage(fakeExpiredTaf.taf.location, locationLabel),
      ),
    ).toBeTruthy();
  });

  it('should display a TAC with adverse weather', async () => {
    const apiAdverseWeatherTAC = (): TafApi => {
      return {
        ...createApi(),
        getTAC: (): Promise<{ data: TAC }> => {
          return new Promise((resolve) => {
            resolve({ data: adverseWeatherTAC });
          });
        },
      };
    };

    render(
      <TafThemeApiProvider createApiFunc={apiAdverseWeatherTAC}>
        <TacContainer taf={fakePublishedFixedTaf.taf} />
      </TafThemeApiProvider>,
    );

    expect(screen.getByTestId('tafLocationLabel').innerHTML).toEqual(
      getLocationSetting(fakePublishedFixedTaf.taf.location).label,
    );
    const windStyle = getComputedStyle(await screen.findByText('08020KT'));
    expect(windStyle.color).toEqual('rgb(192, 0, 0)');
  });
});
