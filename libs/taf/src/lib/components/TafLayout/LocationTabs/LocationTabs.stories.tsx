/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */
import * as React from 'react';
import { darkTheme } from '@opengeoweb/theme';
import { Box, Paper, Typography } from '@mui/material';
import { TafThemeApiProvider } from '../../Providers';
import {
  fakeAmendmentFixedTaf,
  fakeDraftAmendmentFixedTaf,
  fakeDraftFixedTaf,
  fakeNewFixedTaf,
  fakePublishedFixedTaf,
} from '../../../utils/mockdata/fakeTafList';
import LocationTabs from './LocationTabs';

export default { title: 'components/Taf Layout/LocationTabs' };

const LocationTabsDemo = (): React.ReactElement => {
  return (
    <Paper sx={{ padding: '10px' }}>
      <Box sx={{ width: '56px' }}>
        <Typography>Upcoming</Typography>
        <LocationTabs
          tafList={[fakeDraftFixedTaf, fakePublishedFixedTaf, fakeNewFixedTaf]}
          activeIndex={0}
          timeSlot="UPCOMING"
        />
      </Box>
      <Box sx={{ width: '56px' }}>
        <Typography>Current</Typography>
        <LocationTabs
          tafList={[fakeDraftAmendmentFixedTaf, fakeAmendmentFixedTaf]}
          activeIndex={0}
          timeSlot="ACTIVE"
        />
      </Box>
    </Paper>
  );
};

export const LocationTabsLightTheme = (): React.ReactElement => {
  return (
    <div style={{ width: '100px', height: '440px' }}>
      <TafThemeApiProvider>
        <LocationTabsDemo />
      </TafThemeApiProvider>
    </div>
  );
};
LocationTabsLightTheme.storyName = 'LocationTabs light (takeSnapshot)';

export const LocationTabsDarkTheme = (): React.ReactElement => {
  return (
    <div style={{ width: '100px', height: '440px' }}>
      <TafThemeApiProvider theme={darkTheme}>
        <LocationTabsDemo />
      </TafThemeApiProvider>
    </div>
  );
};
LocationTabsDarkTheme.storyName = 'LocationTabs dark (takeSnapshot)';
