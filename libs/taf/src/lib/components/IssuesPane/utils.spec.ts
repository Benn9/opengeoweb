/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */

import { FieldError, FieldErrors } from 'react-hook-form';
import {
  filterDuplicateErrors,
  getCustomError,
  getErrors,
  getErrorValues,
} from './utils';

describe('components/IssuesPane/utils', () => {
  describe('getCustomError', () => {
    it('should add isChangeGroup and row as to field error', () => {
      const testError = {
        message: 'wind, visibility, weather or cloud is required',
        ref: {
          name: 'test',
        },
        type: 'validateRequiredSub',
        types: {
          validateRequiredSub: 'wind, visibility, weather or cloud is required',
        },
      };
      expect(getCustomError(testError, true, 0)).toEqual({
        ...testError,
        isChangeGroup: true,
        rowIndex: 0,
      });

      expect(getCustomError(testError, false, -1)).toEqual({
        ...testError,
        isChangeGroup: false,
        rowIndex: -1,
      });
    });
  });

  describe('getErrorValues', () => {
    it('should find the error value in error object', () => {
      const testError = {
        message: 'wind, visibility, weather or cloud is required',
        ref: {
          name: 'test',
        },
        type: 'validateRequiredSub',
        types: {
          validateRequiredSub: 'wind, visibility, weather or cloud is required',
        },
      };
      const testErrors = {
        baseForecast: {
          wind: testError,
        },
      } as unknown as FieldErrors;

      expect(getErrorValues(testErrors, false, -1)).toEqual([
        getCustomError(testError, false, -1),
      ]);
      expect(getErrorValues(testErrors, true, 0)).toEqual([
        getCustomError(testError, true, 0),
      ]);
    });
  });

  describe('filterDuplicateErrors', () => {
    it('should filter out duplicate errors', () => {
      const errorList = [
        {
          isChangeGroup: true,
          message: 'wind, visibility, weather or cloud is required',
          ref: {
            name: 'test',
          },
          rowIndex: 0,
          type: 'validateRequiredSub',
          types: {
            validateRequiredSub:
              'wind, visibility, weather or cloud is required',
          },
        },
        {
          isChangeGroup: true,
          message: 'wind, visibility, weather or cloud is required',
          ref: { name: 'changeGroups[0].weather.weather1' },
          rowIndex: 0,
          type: 'validateRequiredSub',
          types: {
            validateRequiredSub:
              'wind, visibility, weather or cloud is required',
          },
        },
        {
          isChangeGroup: true,
          message: 'wind, visibility, weather or cloud is required',
          ref: { name: 'changeGroups[0].visibility' },
          rowIndex: 0,
          type: 'validateRequiredSub',
          types: {
            validateRequiredSub:
              'wind, visibility, weather or cloud is required',
          },
        },
        {
          isChangeGroup: true,
          message: 'wind, visibility, weather or cloud is required',
          ref: { name: 'changeGroups[1].visibility' },
          rowIndex: 1,
          type: 'validateRequiredSub',
          types: {
            validateRequiredSub:
              'wind, visibility, weather or cloud is required',
          },
        },
        {
          isChangeGroup: true,
          message: 'Probability or change is required',
          ref: { name: 'changeGroups[0].probability' },
          rowIndex: 0,
          type: 'validateRequiredMainOptional',
          types: {
            validateRequiredMainOptional: 'Probability or change is required',
          },
        },
        {
          isChangeGroup: true,
          message: 'Probability or change is required',
          ref: { name: 'changeGroups[0].change' },
          rowIndex: 0,
          type: 'validateRequiredMainOptional',
          types: {
            validateRequiredMainOptional: 'Probability or change is required',
          },
        },
        {
          isChangeGroup: true,
          message: 'Probability or change is required',
          ref: { name: 'changeGroups[1].change' },
          rowIndex: 1,
          type: 'validateRequiredMainOptional',
          types: {
            validateRequiredMainOptional: 'Probability or change is required',
          },
        },
        {
          isChangeGroup: true,
          message: 'Valid is required',
          ref: { name: 'changeGroups[1].valid' },
          rowIndex: 1,
          type: 'validateRequiredMain',
          types: {
            validateRequiredMain: 'Valid is required',
          },
        },
      ];
      expect(filterDuplicateErrors(errorList)).toEqual([
        errorList[0],
        errorList[3],
        errorList[4],
        errorList[6],
        errorList[7],
      ]);
    });
  });

  describe('getErrors', () => {
    it('should return a list of errors', () => {
      const testError = {
        message: 'wind, visibility, weather or cloud is required',
        ref: {
          name: 'test',
        },
        type: 'validateRequiredSub',
        types: {
          validateRequiredSub: 'wind, visibility, weather or cloud is required',
        },
      };

      const testError2 = {
        message: 'weather or cloud is required',
        ref: {
          name: 'test',
        },
        type: 'validateRequiredSub',
        types: {
          validateRequiredSub: 'weather or cloud is required',
        },
      };

      const testErrors = {
        baseForecast: {
          wind: testError2,
        },
        changeGroups: [
          { valid: testError, wind: testError2 },
          { wind: testError2 },
        ],
      } as unknown as Record<string, FieldError>;

      expect(getErrors(testErrors)).toEqual([
        getCustomError(testError2, false, -1),
        getCustomError(testError, true, 0),
        getCustomError(testError2, true, 0),
        getCustomError(testError2, true, 1),
      ]);
    });
  });
});
