/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */

import type {
  Parameter,
  PlotPreset,
  ServiceInterface,
} from '../components/TimeSeries/types';

export interface Plot {
  title: string;
  plotId: string;
}
export interface TimeSeriesService {
  id: string;
  description: string;
  url: string;
  type: ServiceInterface;
}
export interface TimeseriesSelectFilters {
  searchFilter: string;
  // TODO: Add filterchip store components: https://gitlab.com/opengeoweb/opengeoweb/-/issues/4346
}
export interface TimeseriesSelectStore {
  filters: TimeseriesSelectFilters;
}
export interface TimeSeriesStoreType {
  plotPreset?: PlotPreset;
  timeseriesSelect?: TimeseriesSelectStore;
  services?: TimeSeriesService[];
}

export interface UpdateParameterPayload {
  parameter: Parameter;
}

export interface TimeSeriesModuleState {
  timeSeries?: TimeSeriesStoreType;
}

export interface SetSearchFilterPayload {
  filterText: string;
}
