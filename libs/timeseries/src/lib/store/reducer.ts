/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */

import { createSlice, PayloadAction, Draft } from '@reduxjs/toolkit';
import { produce } from 'immer';
import { Parameter } from '../components/TimeSeries/types';
import {
  SetSearchFilterPayload,
  TimeSeriesStoreType,
  UpdateParameterPayload,
} from './types';
// import { mockTimeSeriesServices as services } from './utils';

export const initialState: TimeSeriesStoreType = {
  plotPreset: undefined,
  services: undefined,
  timeseriesSelect: {
    filters: {
      searchFilter: '',
      // TODO Add filterchip store components: https://gitlab.com/opengeoweb/opengeoweb/-/issues/4346
      // activeServices: ,
      // filters: ,
    },
  },
};

const slice = createSlice({
  initialState,
  name: 'timeseries',
  reducers: {
    registerTimeSeriesPreset: (
      draft: Draft<TimeSeriesStoreType>,
      action: PayloadAction<TimeSeriesStoreType>,
    ) => {
      // Add parameter id to plotPreset.parameters
      const timeSeriesPresetWithParameterIds = produce(
        action.payload,
        (draftPreset) => {
          if (!draftPreset.plotPreset) {
            console.warn('missing draft plotpresets', draftPreset);
            return;
          }
          draftPreset.plotPreset.parameters.forEach((draftParameter) => {
            draftParameter.id = generateParameterId();
          });

          if (!draftPreset.services || !draft.services) {
            return;
          }
          draftPreset.services = [
            ...draftPreset.services,
            ...draft.services,
          ].filter(
            (service, index, array) =>
              index === array.findIndex((s) => s.id === service.id),
          );
        },
      );

      draft.services = timeSeriesPresetWithParameterIds.services;
      draft.plotPreset = timeSeriesPresetWithParameterIds.plotPreset;
    },
    deletePlot: (
      draft: Draft<TimeSeriesStoreType>,
      action: PayloadAction<string>,
    ) => {
      draft.plotPreset!.plots = draft.plotPreset!.plots.filter((plot) => {
        return plot.plotId !== action.payload;
      });
      draft.plotPreset!.parameters = draft.plotPreset!.parameters.filter(
        (parameter) => {
          return parameter.plotId !== action.payload;
        },
      );
    },
    togglePlot: (
      draft: Draft<TimeSeriesStoreType>,
      action: PayloadAction<string>,
    ) => {
      draft.plotPreset!.plots = draft.plotPreset!.plots.map((plot) => {
        if (plot.plotId === action.payload) {
          const plotIsEnabled = plot.enabled !== false;
          return { ...plot, enabled: !plotIsEnabled };
        }
        return plot;
      });
    },
    addPlot: (
      draft: Draft<TimeSeriesStoreType>,
      action: PayloadAction<{ title: string }>,
    ) => {
      draft.plotPreset!.plots.push({
        title: action.payload.title,
        plotId: generateTimeSeriesId(),
      });
    },
    deleteParameter: (
      draft: Draft<TimeSeriesStoreType>,
      action: PayloadAction<{ id: string }>,
    ) => {
      draft.plotPreset!.parameters = draft.plotPreset!.parameters.filter(
        (parameter) => {
          return parameter.id !== action.payload.id;
        },
      );
    },
    addParameter: (
      draft: Draft<TimeSeriesStoreType>,
      action: PayloadAction<Parameter>,
    ) => {
      draft.plotPreset!.parameters.push({
        ...action.payload,
        id: generateParameterId(),
      });
    },
    toggleParameter: (
      draft: Draft<TimeSeriesStoreType>,
      action: PayloadAction<{ id: string }>,
    ) => {
      draft.plotPreset!.parameters = draft.plotPreset!.parameters.map(
        (parameter) => {
          if (parameter.id === action.payload.id) {
            const parameterIsEnabled = parameter.enabled !== false;
            return { ...parameter, enabled: !parameterIsEnabled };
          }
          return parameter;
        },
      );
    },
    updateParameter: (
      draft: Draft<TimeSeriesStoreType>,
      action: PayloadAction<UpdateParameterPayload>,
    ) => {
      const { id, color, plotType } = action.payload.parameter;
      draft.plotPreset!.parameters = draft.plotPreset!.parameters.map(
        (parameter): Parameter => {
          if (parameter.id === id) {
            return { ...parameter, color, plotType };
          }
          return parameter;
        },
      );
    },
    setSearchFilter: (draft, action: PayloadAction<SetSearchFilterPayload>) => {
      const { filterText } = action.payload;
      if (draft.timeseriesSelect) {
        draft.timeseriesSelect.filters.searchFilter = filterText;
      }
    },
  },
});

export const { reducer, actions } = slice;

let generatedTimeseriesIds = 0;
const generateTimeSeriesId = (): string => {
  generatedTimeseriesIds += 1;
  return `timeseriesid_${generatedTimeseriesIds}`;
};

let generatedParameterIds = 0;
const generateParameterId = (): string => {
  generatedParameterIds += 1;
  return `timeseriesid_${generatedParameterIds}`;
};
