/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */

import { createSelector } from '@reduxjs/toolkit';
import { Plot, PlotPreset } from '../components/TimeSeries/types';
import {
  TimeSeriesModuleState,
  TimeSeriesStoreType,
  TimeSeriesService,
} from './types';
import { getServiceById } from '../utils/edrUtils';

export const getPlotState = (
  state: TimeSeriesModuleState,
): PlotPreset | undefined => {
  return state.timeSeries?.plotPreset;
};

export const getTimeSeriesState = (
  state: TimeSeriesModuleState,
): TimeSeriesStoreType | undefined => {
  return state.timeSeries;
};

export const getMapId = (state: TimeSeriesModuleState): string | undefined =>
  state.timeSeries?.plotPreset?.mapId;

export const getPlotWithParameters = createSelector(
  getPlotState,
  (_: TimeSeriesModuleState, selectPlotId: string) => selectPlotId,
  (plotPreset, selectPlotId: string): Plot | undefined => {
    if (!plotPreset || !selectPlotId) {
      return undefined;
    }

    const plot = plotPreset.plots.find((plot) => plot.plotId === selectPlotId);
    if (!plot) {
      return undefined;
    }

    const parameters = plotPreset.parameters.filter(
      (parameter) => parameter.plotId === selectPlotId,
    );

    return { ...plot, parameters };
  },
);

export const getService = createSelector(
  getTimeSeriesState,
  (_: TimeSeriesModuleState, serviceId: string | undefined) => serviceId,
  (
    timeSeriesState: TimeSeriesStoreType,
    serviceId: string,
  ): TimeSeriesService | undefined => {
    return getServiceById(timeSeriesState.services, serviceId);
  },
);

export const getServices = createSelector(
  getTimeSeriesState,
  (timeSeriesState: TimeSeriesStoreType): TimeSeriesService[] | undefined => {
    return timeSeriesState?.services;
  },
);

/**
 * Returns search filter string
 *
 * Example getSearchFilter(store);
 * @param {object} store store: object - store object
 * @returns {array} returnType: string
 */
export const getSearchFilter = createSelector(
  getTimeSeriesState,
  (store): string => store?.timeseriesSelect?.filters?.searchFilter || '',
);
