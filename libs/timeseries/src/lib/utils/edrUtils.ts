/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */

import axios from 'axios';
import { dateUtils } from '@opengeoweb/shared';
import { Feature, FeatureCollection, Point } from 'geojson';
import { isString, sortBy } from 'lodash';
import type {
  EDRInstance,
  EDRPositionResponse,
  Parameter,
  ParameterWithData,
} from '../components/TimeSeries/types';
import type { SelectParameter } from '../components/TimeSeriesSelect/TimeSeriesSelect';
import { COLOR_MAP } from '../constants';
import { TimeSeriesService } from '../store/types';
import type { PointerLocationAndName } from '../components/TimeSeries/TimeSeriesConnect';

const EDR_NORWAY_URL = 'https://interpol-b.met.no/collections/meps-det-vdiv';
export const supportedOutputFormat = 'CoverageJSON';

export const getBaseQueryString = (
  parameterName: string,
  instance: EDRInstance,
): string => {
  const nameParam = `&parameter-name=${parameterName}`;
  const interval = instance.extent?.temporal.interval[0];
  const timeParam = interval?.length
    ? `&datetime=${interval[0]}/${interval[1]}`
    : '';
  const crsParam = instance.extent?.spatial.crs
    ? `&crs=${instance.extent.spatial.crs}`
    : '';
  const formatParam = `&f=${supportedOutputFormat}`;
  return `?${nameParam}${timeParam}${crsParam}${formatParam}`;
};

export const latestInstance = (instances: EDRInstance[]): EDRInstance => {
  return instances.reduce(
    (latest, current) => (latest.id < current.id ? current : latest),
    { id: '' },
  );
};

export const isSupportedInstance = (instances: EDRInstance): boolean => {
  const outputFormatsPosition =
    instances.data_queries?.position?.link.variables.output_formats ?? [];
  const outputFormatsLocation =
    instances.data_queries?.locations?.link.variables.output_formats ?? [];
  return [...outputFormatsLocation, ...outputFormatsPosition].includes(
    supportedOutputFormat,
  );
};

export const latestSupportedInstance = (
  instances: EDRInstance[],
): EDRInstance => {
  return latestInstance(
    instances.filter((instance) => isSupportedInstance(instance)),
  );
};

export const fetchEdrParameterApiData = async (
  urlDomain: string,
  parameterName: string,
  location: PointerLocationAndName,
  instance: EDRInstance,
): Promise<EDRPositionResponse | null> => {
  const base = `${urlDomain}/instances/${instance.id}`;
  const queryString = getBaseQueryString(parameterName, instance);
  const { lat, lon, id } = location;

  try {
    if (urlDomain === EDR_NORWAY_URL) {
      if (id === undefined) {
        return null;
      }
      const locationUrl = `${base}/locations/${id}${queryString}`;
      const result = await axios.get<EDRPositionResponse>(locationUrl);
      return result.data;
    }
    const positionUrl = `${base}/position${queryString}&coords=POINT(${lon} ${lat})`;
    const result = await axios.get<EDRPositionResponse>(positionUrl);
    return result.data;
  } catch (error) {
    return null;
  }
};

export const fetchEdrLatestInstances = async (
  collectionUrl: string,
): Promise<EDRInstance> => {
  const url = `${collectionUrl}/instances`;
  try {
    const result = await axios.get<{ instances: EDRInstance[] }>(url);
    return latestSupportedInstance(result.data.instances);
  } catch (error) {
    return { id: '' };
  }
};

export const getEdrParameter = async (
  presetParameter: Parameter,
  url: string,
  location: PointerLocationAndName,
): Promise<ParameterWithData | null> => {
  const theLatestInstance = await fetchEdrLatestInstances(url);
  const apiData: EDRPositionResponse | null = await fetchEdrParameterApiData(
    url,
    presetParameter.propertyName,
    location,
    theLatestInstance,
  );

  if (!apiData?.ranges || apiData.domain.axes.t.values.length === 0) {
    return null;
  }
  // assume only one parameter
  const paramKey = Object.keys(apiData.ranges)[0];

  const { symbol } = apiData.parameters[paramKey].unit;
  const unit = isString(symbol) ? symbol : symbol.value;
  return {
    ...presetParameter,
    unit,
    timestep: apiData.domain.axes.t.values.map((str) => dateUtils.utc(str)),
    value: apiData.ranges[paramKey].values,
  };
};

export const getEdrSelectParameters = async (
  service: TimeSeriesService,
): Promise<SelectParameter[]> => {
  try {
    let parametersById: EdrParameters;

    if (service.url === EDR_NORWAY_URL) {
      const instance = await fetchEdrLatestInstances(service.url);
      const result = await axios.get<{ parameters: EdrParameters }>(
        `${service.url}/instances/${instance.id}`,
      );
      parametersById = result.data.parameters;
    } else {
      const result = await axios.get<{ parameter_names: EdrParameters }>(
        service.url,
      );
      parametersById = result.data.parameter_names;
    }

    const selectParameters: SelectParameter[] = Object.values(
      parametersById,
    ).map((parameter) => {
      return {
        plotType: 'line',
        propertyName: parameter.id,
        unit: parameter.unit?.label ?? ' ',
        serviceId: service.id,
        color: COLOR_MAP[parameter.id as keyof typeof COLOR_MAP] ?? 'black',
      };
    });
    return selectParameters;
  } catch (error) {
    console.error(error);
    return [];
  }
};

interface EdrParameters {
  [name: string]: {
    id: string;
    description: string;
    type: string;
    observedProperty: {
      label: string;
    };
    unit?: {
      label: string;
      symbol: {
        type: string;
        value: string;
      };
    };
  };
}

export const getLocations = async (
  service: TimeSeriesService,
  circleDrawFunctionId: string,
): Promise<FeatureCollection<Point>> => {
  const url = `${service.url}/locations`;
  const result: {
    data: {
      features: {
        id: string;
        type: 'Feature';
        geometry: { coordinates: [number, number]; type: 'Point' };
        properties: {
          datetime: string;
          detail: string;
          name: string;
        };
      }[];
    };
  } = await axios.get(url);

  const features = result.data.features.map((feature): Feature<Point> => {
    return {
      id: feature.id,
      type: 'Feature',
      properties: {
        name: feature.properties.name,
        drawFunctionId: circleDrawFunctionId,
      },
      geometry: {
        type: 'Point',
        coordinates: feature.geometry.coordinates,
      },
    };
  });

  return {
    type: 'FeatureCollection',
    features: sortBy(features, (feature) => feature.properties?.name),
  };
};

export const getServiceById = (
  services: TimeSeriesService[] | undefined,
  serviceId: string | undefined,
): TimeSeriesService | undefined => {
  return serviceId && services && services.length > 0
    ? services.find((service) => service.id === serviceId)
    : undefined;
};
