/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */
import React from 'react';
import { MapViewConnect } from '@opengeoweb/core';
import { createStore } from '@opengeoweb/store';
import { useDefaultMapSettings } from './storybookUtils/defaultStorySettings';
import edrFinlandScreenPresets from './storybookUtils/screenPresetFinland.json';
import edrNorwayScreenPresets from './storybookUtils/screenPresetNorway.json';
import ogcScreenPresets from './storybookUtils/screenPresetOgc.json';
import { TimeSeriesThemeStoreProvider } from './storybookUtils/Providers';
import { TimeSeriesConnect } from './components/TimeSeries/TimeSeriesConnect';
import { TimeSeriesManagerConnect } from './components/TimeSeriesManager/TimeSeriesManagerConnect';
import { ToggleThemeButton } from './storybookUtils/ToggleThemeButton';
import timeSeriesPresetLocations from './storybookUtils/timeSeriesPresetLocations.json';
import { TimeSeriesStoreType } from './store/types';

const edrPresetFinland = edrFinlandScreenPresets[0].views.byId.Timeseries
  .initialProps as TimeSeriesStoreType;

const featurePreset = ogcScreenPresets[0].views.byId.Timeseries
  .initialProps as TimeSeriesStoreType;

const MapWithTimeSeriesManager: React.FC<{
  mapId: string;
  preset: TimeSeriesStoreType;
}> = ({ mapId, preset }) => {
  useDefaultMapSettings({
    mapId,
  });

  return (
    <div>
      <ToggleThemeButton />
      <TimeSeriesManagerConnect />
      <div style={{ display: 'flex' }}>
        <div style={{ width: '50%', height: '100vh', position: 'relative' }}>
          <MapViewConnect mapId={mapId} />
        </div>
        <div style={{ width: '50%', height: '100vh', position: 'relative' }}>
          <TimeSeriesConnect
            timeSeriesPresetLocations={timeSeriesPresetLocations}
            timeSeriesPreset={preset}
          />
        </div>
      </div>
    </div>
  );
};

export const TimeSeriesEDRFinlandDemo = (): React.ReactElement => {
  return (
    <TimeSeriesThemeStoreProvider store={createStore()}>
      <MapWithTimeSeriesManager mapId="mapid_1" preset={edrPresetFinland} />
    </TimeSeriesThemeStoreProvider>
  );
};

export const TimeSeriesFeatureDemo = (): React.ReactElement => {
  return (
    <TimeSeriesThemeStoreProvider store={createStore()}>
      <MapWithTimeSeriesManager mapId="mapid_1" preset={featurePreset} />
    </TimeSeriesThemeStoreProvider>
  );
};

const storyParameters = {
  zeplinLink: [
    {
      name: 'Light theme',
      link: 'https://app.zeplin.io/project/5ecf84a3c6ae1047a368f393/screen/6139ed648e5648b6c6936777',
    },
    {
      name: 'Dark theme',
      link: 'https://app.zeplin.io/project/5ecf84a3c6ae1047a368f393/screen/6139ed66d8618a259813f458',
    },
  ],
};
TimeSeriesEDRFinlandDemo.parameters = storyParameters;

const edrPresetNorway = edrNorwayScreenPresets[0].views.byId.Timeseries
  .initialProps as TimeSeriesStoreType;

export const TimeSeriesEDRNorwayDemo = (): React.ReactElement => {
  return (
    <TimeSeriesThemeStoreProvider store={createStore()}>
      <MapWithTimeSeriesManager mapId="mapid_1" preset={edrPresetNorway} />
    </TimeSeriesThemeStoreProvider>
  );
};

TimeSeriesEDRNorwayDemo.parameters = storyParameters;

export default { title: 'application/demo' };
