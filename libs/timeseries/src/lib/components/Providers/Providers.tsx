/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */
import React from 'react';
import { ApiProvider } from '@opengeoweb/api';
import { Store } from '@reduxjs/toolkit';
import { Provider } from 'react-redux';
import { withEggs } from '@opengeoweb/shared';
import {
  ThemeProviderProps,
  lightTheme,
  ThemeWrapper,
} from '@opengeoweb/theme';

import { Theme } from '@mui/material';
import { coreModuleConfig } from '@opengeoweb/store';
import { timeSeriesModuleConfig } from '../../store/config';
import { createApi as createFakeApi } from '../../utils/fakeApi';

export const TimeSeriesThemeWrapper: React.FC<ThemeProviderProps> = ({
  theme,
  children,
}: ThemeProviderProps) => <ThemeWrapper theme={theme}>{children}</ThemeWrapper>;

const ThemeWrapperWithModules: React.FC<ThemeProviderProps> = withEggs([
  timeSeriesModuleConfig,
  ...coreModuleConfig,
])(({ theme, children }: ThemeProviderProps) => (
  <TimeSeriesThemeWrapper theme={theme}>{children}</TimeSeriesThemeWrapper>
));

interface TimeSeriesApiProviderProps {
  children: React.ReactNode;
  createApi?: () => void;
}

interface TimeSeriesThemeStoreProviderProps {
  children?: React.ReactNode;
  theme?: Theme;
  store: Store;
}

/**
 * A Provider component which returns the Api Provider for the TimeSeries library
 * @param children
 * @returns
 */
export const TimeSeriesApiProvider: React.FC<TimeSeriesApiProviderProps> = ({
  children,
  createApi = createFakeApi,
}: TimeSeriesApiProviderProps) => (
  <ApiProvider createApi={createApi}>{children}</ApiProvider>
);

export interface TimeSeriesStoreProviderProps extends ThemeProviderProps {
  store: Store;
}

export const TimeSeriesThemeStoreProvider: React.FC<TimeSeriesThemeStoreProviderProps> =
  ({
    children,
    theme = lightTheme,
    store,
  }: TimeSeriesThemeStoreProviderProps) => (
    <Provider store={store}>
      <ThemeWrapperWithModules theme={theme}>
        {children}
      </ThemeWrapperWithModules>
    </Provider>
  );
