/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */

import React from 'react';
import { render, renderHook, waitFor, screen } from '@testing-library/react';
import { produce } from 'immer';
import { dateUtils } from '@opengeoweb/shared';

import * as api from './api';
import * as edrUtils from '../../utils/edrUtils';
import {
  fetchParameters,
  getUtcTime,
  TimeSeriesView,
  useGetPlotsWithData,
} from './TimeSeriesView';
import {
  ParameterWithData,
  Plot,
  PlotPreset,
  Parameter,
  PlotWithData,
} from './types';
import { TimeSeriesService } from '../../store/types';

describe('components/TimeSeries/TimeSeriesView', () => {
  const PLOT_ID_1 = 'PLOT_ID_1';
  const LINE = 'line';
  const PROPERTY_NAME_1 = 'PROPERTY_NAME_1';
  const PROPERTY_NAME_2 = 'PROPERTY_NAME_2';
  const UNIT = 'unit';
  const SERVICE_URL = 'SERVICE_URL';

  const service1OGC: TimeSeriesService = {
    id: 'fmi_ogc',
    url: SERVICE_URL,
    type: 'OGC',
    description: 'A desciption',
  };

  const service1EDR: TimeSeriesService = {
    id: 'fmi_edr',
    url: SERVICE_URL,
    type: 'EDR',
    description: 'A desciption',
  };
  const parameter1: Parameter = {
    plotId: PLOT_ID_1,
    plotType: LINE,
    propertyName: PROPERTY_NAME_1,
    unit: UNIT,
    serviceId: service1OGC.id,
  };

  const parameter2: Parameter = {
    ...parameter1,
    propertyName: PROPERTY_NAME_2,
  };

  const TIME_1 = '20220113T120000';
  const TIME_2 = '20220113T130000';
  const RESULT = ['1', '2'];

  const expectedParameterWithData1 = {
    ...parameter1,
    timestep: [
      dateUtils.stringToDate(TIME_1, "yyyyMMdd'T'HHmmss"),
      dateUtils.stringToDate(TIME_2, "yyyyMMdd'T'HHmmss"),
    ],
    value: RESULT.map((el) => Number(el)),
  } as ParameterWithData;

  const mapPinLocation = { lat: 1.0, lon: 2.0 };

  const plot: Plot = {
    plotId: PLOT_ID_1,
    title: 'Title',
  };

  const plotPreset1: PlotPreset = {
    mapId: 'mapId',
    plots: [plot],
    parameters: [parameter1, parameter2],
  };

  const mockGetOgcParameter = async (): Promise<ParameterWithData | null> => {
    return expectedParameterWithData1;
  };

  const mockGetEdrParameter = async (): Promise<ParameterWithData | null> => {
    return expectedParameterWithData1;
  };

  describe('TimeSeriesView', () => {
    it('should render', async () => {
      const spy = jest
        .spyOn(api, 'getOgcParameter')
        .mockImplementation(mockGetOgcParameter);
      jest.useFakeTimers();
      const { rerender, unmount } = render(
        <TimeSeriesView
          selectedLocation={mapPinLocation}
          plotPreset={plotPreset1}
          services={[service1OGC]}
        />,
      );
      expect(await screen.findByTestId('TimeSeriesChart')).toBeTruthy();

      expect(spy).toHaveBeenCalledTimes(2);

      // Should not rerender if lat lon did not change
      rerender(
        <TimeSeriesView
          selectedLocation={{ ...mapPinLocation }}
          plotPreset={plotPreset1}
          services={[service1OGC]}
        />,
      );

      expect(spy).toHaveBeenCalledTimes(2);

      // timeout should trigger
      jest.advanceTimersByTime(60_000);
      expect(spy).toHaveBeenCalledTimes(4);
      unmount();
    });
  });

  describe('useUpdateTimeSeriesData', () => {
    it('should update data correctly', async () => {
      const spy = jest
        .spyOn(api, 'getOgcParameter')
        .mockImplementation(mockGetOgcParameter);
      let location = mapPinLocation;
      let plotPreset = plotPreset1;
      const services = [service1OGC];
      const { rerender, result } = renderHook(() =>
        useGetPlotsWithData(plotPreset, location, services),
      );
      await waitFor(() => {
        expect(spy).toHaveBeenCalledTimes(2);
      });

      let current: PlotWithData;
      await waitFor(() => {
        // eslint-disable-next-line prefer-destructuring
        current = result.current![0];
        expect(current.title).toEqual(plotPreset.plots[0].title);
      });
      expect(current!.plotId).toEqual(plotPreset.plots[0].plotId);
      expect(current!.parametersWithData.length).toEqual(2);

      plotPreset = { ...plotPreset1, parameters: [parameter1] };
      rerender();
      await waitFor(() => {
        expect(spy).toHaveBeenCalledTimes(3);
      });
      await waitFor(() => {
        expect(result.current![0].parametersWithData.length).toEqual(1);
      });

      plotPreset = plotPreset1;
      location = { ...mapPinLocation };
      rerender();
      await waitFor(() => {
        expect(spy).toHaveBeenCalledTimes(5);
      });
      await waitFor(() => {
        expect(result.current![0].parametersWithData.length).toEqual(2);
      });
    });

    it('should remove hidden plots and parameters', async () => {
      const spy = jest
        .spyOn(api, 'getOgcParameter')
        .mockImplementation(mockGetOgcParameter);

      const location = mapPinLocation;
      let plotPreset = plotPreset1;
      const services = [service1OGC];
      const { rerender, result } = renderHook(() =>
        useGetPlotsWithData(plotPreset, location, services),
      );

      await waitFor(() => {
        expect(spy).toHaveBeenCalledTimes(2);
      });
      await waitFor(() => {
        expect(result.current).toHaveLength(1);
      });

      // should remove hidden plot
      plotPreset = produce(plotPreset1, (draft) => {
        draft.plots[0].enabled = false;
      });
      rerender(plotPreset);
      await waitFor(() => {
        expect(spy).toHaveBeenCalledTimes(2);
      });
      expect(result.current).toEqual(undefined);

      // should remove hidden parameter
      const plotPresetWithHiddenParameter = produce(plotPreset1, (draft) => {
        draft.parameters[0].enabled = false;
      });
      plotPreset = plotPresetWithHiddenParameter;
      rerender();
      await waitFor(() => {
        expect(spy).toHaveBeenCalledTimes(3);
      });
      await waitFor(() => {
        expect(result.current).toHaveLength(1);
      });
    });
  });

  describe('fetchParameters', () => {
    it('should get parameter with data from one service', async () => {
      const spy = jest
        .spyOn(api, 'getOgcParameter')
        .mockImplementation(mockGetOgcParameter);

      const parametersWithData = await fetchParameters(
        { ...plotPreset1, parameters: [parameter1] },
        mapPinLocation,
        [service1OGC],
      );

      expect(spy).toHaveBeenCalledTimes(1);

      expect(parametersWithData).toEqual([expectedParameterWithData1]);
    });

    it('should get parameter with data from EDR service', async () => {
      const spy = jest
        .spyOn(edrUtils, 'getEdrParameter')
        .mockImplementation(mockGetEdrParameter);

      const parametersWithData = await fetchParameters(
        {
          ...plotPreset1,
          parameters: [{ ...parameter1, serviceId: service1EDR.id }],
        },
        mapPinLocation,
        [service1EDR],
      );

      expect(spy).toHaveBeenCalledTimes(1);

      expect(parametersWithData).toEqual([expectedParameterWithData1]);
    });
  });

  describe('getUtcTime', () => {
    it('should convert time string to utc time', () => {
      const apiTime = '20220113T120000';
      const utcTime = getUtcTime(apiTime);

      expect(utcTime.toISOString()).toEqual('2022-01-13T12:00:00.000Z');
    });
  });
});
