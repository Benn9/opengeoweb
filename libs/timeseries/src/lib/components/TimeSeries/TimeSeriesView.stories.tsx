/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */
import React from 'react';
import { lightTheme, ThemeWrapper } from '@opengeoweb/theme';
import { TimeSeriesView } from './TimeSeriesView';
import screenPresets from '../../storybookUtils/screenPresetFinland.json';
import { PlotPreset } from './types';
import { mockTimeSeriesServices } from './mockTimeSeriesServices';

export default { title: 'components/TimeSeriesView' };

const TimeSeriesDemo = (): React.ReactElement => {
  const plotPreset = screenPresets[0].views.byId.Timeseries.initialProps
    .plotPreset as PlotPreset;

  return (
    <TimeSeriesView
      plotPreset={plotPreset}
      services={mockTimeSeriesServices}
      selectedLocation={{ lat: 61.2, lon: 5.6 }}
    />
  );
};

export const FmiSofpDemo: React.FC = () => (
  <ThemeWrapper theme={lightTheme}>
    <TimeSeriesDemo />
  </ThemeWrapper>
);
