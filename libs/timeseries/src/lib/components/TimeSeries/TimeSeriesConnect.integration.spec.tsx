/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */

import { act, render, screen, within } from '@testing-library/react';
import React from 'react';
import userEvent from '@testing-library/user-event';
import { createStore, layerActions, mapActions } from '@opengeoweb/store';
import produce from 'immer';
import { webmapUtils } from '@opengeoweb/webmap';
import { ToolkitStore } from '@reduxjs/toolkit/dist/configureStore';
import { Store } from '@reduxjs/toolkit';
import { TimeSeriesConnect } from './TimeSeriesConnect';
import { TimeSeriesPresetLocation, ServiceInterface } from './types';
import { serverMock } from '../../../mocks/server';
import {
  parameterNameMock,
  EDR_FINLAND_URL,
  OGC_URL,
  helsinkiLat,
  helsinkiLon,
  helsinki,
  EDR_NORWAY_URL,
} from '../../../mocks';
import { TimeSeriesThemeStoreProvider } from '../Providers';
import { TimeSeriesManagerConnect } from '../TimeSeriesManager';
import { TimeSeriesStoreType } from '../../store/types';

const mapId = 'mapId';
const geojsonLayerId = 'geojsonLayerId';
const plotTitle = 'plotTitle';
describe('src/components/TimeSeries/TimeSeriesConnect/integration', () => {
  const plotId = 'plotId';

  const plotServicesEdr = [
    {
      id: 'serviceId',
      type: 'EDR' as ServiceInterface,
      url: EDR_FINLAND_URL,
      description: 'a description',
    },
  ];

  const timeSeriesPresetEdrFinland: TimeSeriesStoreType = {
    plotPreset: {
      mapId,
      parameters: [
        {
          plotId,
          plotType: 'bar',
          propertyName: parameterNameMock,
          serviceId: 'serviceId',
          unit: 'unit',
        },
      ],
      plots: [{ plotId, title: plotTitle }],
    },
    services: plotServicesEdr,
    timeseriesSelect: { filters: { searchFilter: '' } },
  };

  jest.spyOn(webmapUtils, 'generateLayerId').mockReturnValue(geojsonLayerId);

  serverMock.listen();
  afterAll(() => {
    serverMock.close();
  });

  const setup = (
    timeSeriesPreset: TimeSeriesStoreType,
    presetLocations?: TimeSeriesPresetLocation[],
  ): Store => {
    const store = createStore();
    render(
      <TimeSeriesThemeStoreProvider store={store}>
        <TimeSeriesConnect
          timeSeriesPresetLocations={presetLocations}
          timeSeriesPreset={timeSeriesPreset}
        />
        <TimeSeriesManagerConnect />
      </TimeSeriesThemeStoreProvider>,
    );

    act(() => {
      store.dispatch(mapActions.registerMap({ mapId }));
    });

    return store;
  };
  it('should work for ogc timeseries', async () => {
    const defaultParamOgc = `?f=json&limit=200`;

    const plotPresetOgc = produce(timeSeriesPresetEdrFinland, (draft) => {
      if (draft.services) {
        draft.services[0].type = 'OGC';
        draft.services[0].url = `${OGC_URL}${defaultParamOgc}`;
      }
    });

    const presetLocations = [
      {
        lat: helsinkiLat,
        lon: helsinkiLon,
        name: helsinki,
      },
    ];

    const store = setup(plotPresetOgc, presetLocations);

    await clickAround(store);
  });

  it('should work for edr finland timeseries', async () => {
    const store = setup(timeSeriesPresetEdrFinland);
    await clickAround(store, true);
  });

  it('should work for edr norway timeseries', async () => {
    const plotPresetEdrNorway = produce(timeSeriesPresetEdrFinland, (draft) => {
      if (draft.services) {
        draft.services[0].url = EDR_NORWAY_URL;
      }
    });

    const store = setup(plotPresetEdrNorway);
    await clickAround(store, true);
  });
});

const clickAround = async (
  store: ToolkitStore,
  isEdr = false,
): Promise<void> => {
  const user = userEvent.setup();

  // map pin has not been set so no chart is shown
  expect(screen.queryByTestId('TimeSeriesChart')).not.toBeInTheDocument();

  // simulate setting map pin on the map outside of a preset location
  await act(() => {
    store.dispatch(
      mapActions.setMapPinLocation({
        mapId,
        mapPinLocation: { lat: 1, lon: 1 },
      }),
    );
  });

  expect(await screen.findByTestId('TimeSeriesChart')).toBeInTheDocument();

  // set location to helsinki with the location select
  // which will change map pin location
  const locationSelect = await screen.findByRole('button', {
    name: /select location/i,
  });
  expect(locationSelect).not.toHaveTextContent(helsinki);
  await user.click(locationSelect);
  await user.click(screen.getByRole('option', { name: helsinki }));
  expect(
    screen.getByRole('button', {
      name: /select location/i,
    }),
  ).toHaveTextContent(helsinki);

  expect(store.getState().webmap.byId[mapId].mapPinLocation).toEqual({
    lat: helsinkiLat,
    lon: helsinkiLon,
    id: isEdr ? 'helsinki' : undefined,
  });

  // simulate setting map pin on the map outside of a preset location
  // will reset the location select
  act(() => {
    store.dispatch(
      mapActions.setMapPinLocation({
        mapId,
        mapPinLocation: { lat: 1, lon: 1 },
      }),
    );
    store.dispatch(
      layerActions.setSelectedFeature({
        layerId: geojsonLayerId,
        selectedFeatureIndex: undefined,
      }),
    );
  });
  expect(
    screen.getByRole('button', {
      name: /select location/i,
    }),
  ).not.toHaveTextContent(helsinki);

  // Open timeseries manager
  await user.click(screen.getByRole('button', { name: 'Timeseries Manager' }));
  screen.getByRole('heading', { name: /time series manager/i });
  screen.getByText(plotTitle);
  screen.getByText(parameterNameMock);

  // Open timeseries select
  await user.click(
    screen.getByRole('button', { name: 'Add parameter to plot' }),
  );
  screen.getByRole('heading', { name: 'Timeseries Select for plotTitle' });
  within(screen.getByTestId('TimeSeriesSelect')).getByText(parameterNameMock);
};
