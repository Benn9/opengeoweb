/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */

import React from 'react';
import {
  fireEvent,
  render,
  screen,
  waitForElementToBeRemoved,
} from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import { mapActions, mapUtils } from '@opengeoweb/store';
import produce from 'immer';
import { createMockStoreWithEggs } from '@opengeoweb/shared';
import { TimeSeriesThemeStoreProvider } from '../../storybookUtils/Providers';
import {
  TimeSeriesConnect,
  TOOLTIP_TITLE_DISABLED,
  TOOLTIP_TITLE_ENABLED,
} from './TimeSeriesConnect';

describe('src/components/TimeSeries/TimeSeriesConnect', () => {
  const lat = 60.192059;
  const lon = 24.945831;

  const timeSeriesPresetLocations = [
    {
      lat,
      lon,
      name: 'Helsinki',
    },
  ];

  const user = userEvent.setup();
  const mapId = 'TimeseriesMap';
  const mockMap = mapUtils.createMap({ id: 'TimeseriesMap' });
  const initialState = {
    webmap: {
      byId: {
        [mapId]: mockMap,
      },
      allIds: [mapId],
    },
    timeSeries: {
      plotPreset: {
        mapId: 'TimeseriesMap',
        plots: [
          {
            title: 'Plot 1',
            plotId: 'Plot_1',
          },
        ],
        parameters: [
          {
            plotId: 'Plot_1',
            plotType: 'line',
            serviceId: 'fmi_ecmwf',
            propertyName: 'Pressure',
            id: 'timeseriesid_17',
          },
        ],
        connectedMap: 'TimeseriesMap',
      },
      services: [
        {
          url: 'https://opendata.fmi.fi/edr/collections/ecmwf',
          description: 'something',
          id: 'fmi_ecmwf',
          type: 'EDR',
        },
      ],
    },
  };

  it('should show correct tooltip for enabled mappin', async () => {
    const mockState = produce(initialState, (draft) => {
      draft.webmap.byId[mapId].disableMapPin = false;
    });
    const store = createMockStoreWithEggs(mockState);

    render(
      <TimeSeriesThemeStoreProvider store={store}>
        <TimeSeriesConnect
          timeSeriesPresetLocations={timeSeriesPresetLocations}
        />
      </TimeSeriesThemeStoreProvider>,
    );

    expect(screen.queryByText(TOOLTIP_TITLE_ENABLED)).toBeFalsy();

    const button = screen.getByTestId('toggleLockLocationButton');
    expect(button).toBeTruthy();
    await user.hover(button);
    expect(await screen.findByRole('tooltip')).toBeTruthy();
    expect(screen.getByText(TOOLTIP_TITLE_ENABLED)).toBeTruthy();

    await user.unhover(button);
    await waitForElementToBeRemoved(() => screen.queryByRole('tooltip'), {
      timeout: 3000,
    });
    expect(screen.queryByText(TOOLTIP_TITLE_ENABLED)).toBeFalsy();
  });

  it('should show correct tooltip for disabled mappin', async () => {
    const mockState = produce(initialState, (draft) => {
      draft.webmap.byId[mapId].disableMapPin = true;
    });
    const store = createMockStoreWithEggs(mockState);

    render(
      <TimeSeriesThemeStoreProvider store={store}>
        <TimeSeriesConnect
          timeSeriesPresetLocations={timeSeriesPresetLocations}
        />
      </TimeSeriesThemeStoreProvider>,
    );

    expect(screen.queryByText(TOOLTIP_TITLE_DISABLED)).toBeFalsy();

    const button = screen.getByTestId('toggleLockLocationButton');
    expect(button).toBeTruthy();
    await user.hover(button);
    expect(await screen.findByRole('tooltip')).toBeTruthy();
    expect(screen.getByText(TOOLTIP_TITLE_DISABLED)).toBeTruthy();

    await user.unhover(button);
    await waitForElementToBeRemoved(() => screen.queryByRole('tooltip'), {
      timeout: 3000,
    });
    expect(screen.queryByText(TOOLTIP_TITLE_DISABLED)).toBeFalsy();
  });

  it('it should re-enable the mappin and make the mappin visible when clicking toggleLockLocationButton', async () => {
    const mockState = produce(initialState, (draft) => {
      draft.webmap.byId[mapId].disableMapPin = true;
    });
    const store = createMockStoreWithEggs(mockState);
    render(
      <TimeSeriesThemeStoreProvider store={store}>
        <TimeSeriesConnect
          timeSeriesPresetLocations={timeSeriesPresetLocations}
        />
      </TimeSeriesThemeStoreProvider>,
    );

    store.clearActions();

    const expectedActions1 = [
      mapActions.setDisableMapPin({
        mapId,
        disableMapPin: false,
      }),
    ];

    const button = screen.getByTestId('toggleLockLocationButton');
    fireEvent.click(button);
    await expect(store.getActions()).toEqual(expectedActions1);
  });
});
