/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */

import { useEffect } from 'react';
import { MapDrawDrawFunctionArgs } from '@opengeoweb/webmap-react';
import { usePrevious } from '@opengeoweb/shared';
import { Feature, FeatureCollection, Point } from 'geojson';
import { isEqual } from 'lodash';
import { produce } from 'immer';
import { PointerLocation, TimeSeriesPresetLocation } from './types';
import type { PointerLocationAndName } from './TimeSeriesConnect';

export const createGeojson = (
  timeSeriesPresetLocations: TimeSeriesPresetLocation[],
  circleDrawFunctionId: string,
): FeatureCollection<Point> => {
  const features = timeSeriesPresetLocations.map(
    ({ name, lat, lon }): Feature<Point> => {
      return {
        type: 'Feature',
        properties: {
          name,
          drawFunctionId: circleDrawFunctionId,
        },
        geometry: {
          type: 'Point',
          coordinates: [lon, lat],
        },
      };
    },
  );

  return {
    type: 'FeatureCollection',
    features,
  };
};

export const addDrawFunctionToGeojson = (
  geojson: FeatureCollection,
  circleDrawFunctionId: string,
  selectedCircleDrawFunctionId: string,
  selectedFeatureIndex?: number,
): FeatureCollection => {
  const geojsonWithDraw = produce(geojson, (draft) => {
    /* Set default drawfunction for each feature */
    draft.features.forEach((feature) => {
      // eslint-disable-next-line no-param-reassign
      feature.properties!.drawFunctionId = circleDrawFunctionId;
    });

    /* Change drawfunction for selected feature */
    if (selectedFeatureIndex !== undefined) {
      draft.features[selectedFeatureIndex].properties!.drawFunctionId =
        selectedCircleDrawFunctionId;
    }
  });
  return geojsonWithDraw;
};

const fitText = (
  ctx: CanvasRenderingContext2D,
  text: string,
  x: number,
  y: number,
  lineheight: number,
): void => {
  const lines = text.split('\n');

  for (let i = 0; i < lines.length; i += 1) {
    ctx.fillText(lines[i], x, y + i * lineheight);
  }
};

const roundRect = (
  ctx: CanvasRenderingContext2D,
  w: number,
  h: number,
  x: number,
  y: number,
  radius = 2,
): void => {
  const r = x + w;
  const b = y + h;
  ctx.beginPath();
  ctx.moveTo(x + radius, y);
  ctx.lineTo(r - radius, y);
  ctx.quadraticCurveTo(r, y, r, y + radius);
  ctx.lineTo(r, y + h - radius);
  ctx.quadraticCurveTo(r, b, r - radius, b);
  ctx.lineTo(x + radius, b);
  ctx.quadraticCurveTo(x, b, x, b - radius);
  ctx.lineTo(x, y + radius);
  ctx.quadraticCurveTo(x, y, x + radius, y);
  ctx.stroke();
  ctx.fill();
};

export const circleDrawFunction = (
  args: MapDrawDrawFunctionArgs,
  fillColor = '#88F',
  radius = 12,
): void => {
  const { context: ctx, coord, isHovered, feature } = args;
  ctx.strokeStyle = '#000';
  ctx.fillStyle = fillColor;
  ctx.beginPath();
  ctx.arc(coord.x, coord.y, radius || 10, 0, 2 * Math.PI);
  ctx.fill();
  ctx.stroke();
  ctx.fillStyle = '#000';
  ctx.beginPath();
  ctx.arc(coord.x, coord.y, 2, 0, 2 * Math.PI);
  ctx.fill();
  if (isHovered) {
    const drawInfo = (x: number, y: number): void => {
      const fontSize = 16;
      const padding = 20;
      ctx.font = `${fontSize}px Roboto`;
      roundRect(
        ctx,
        ctx.measureText(feature.properties!.name).width + padding,
        fontSize + padding,
        x -
          padding * 0.5 -
          ctx.measureText(feature.properties!.name).width * 0.5,
        y + padding,
      );
      ctx.fillStyle = '#FFF';
      ctx.textAlign = 'left';
      if (feature && feature.properties) {
        fitText(
          ctx,
          feature.properties.name,
          x - ctx.measureText(feature.properties.name).width * 0.5,
          y + padding * 2,
          fontSize,
        );
      }
    };
    drawInfo(coord.x, coord.y);
  }
};

export const useHandleClickOnMap = (
  selectedFeatureIndex: number | undefined,
  mapPinLocation: PointerLocation,
  setMapPinLocation: (location: PointerLocation) => void,
  setTimeSeriesLocation: (location: PointerLocationAndName) => void,
  setSelectedLocation: (location: string) => void,
  geojson: FeatureCollection<Point> | undefined,
  disableMapPin: boolean,
  setLayerGeojson: (geojson?: FeatureCollection) => void,
  circleDrawFunctionId: string,
  selectedCircleDrawFunctionId: string,
): void => {
  const previousMapPinLocation = usePrevious(mapPinLocation);

  // Handle click on map
  useEffect(() => {
    if (
      mapPinLocation === undefined ||
      disableMapPin ||
      // Avoid infinite loop
      isEqual(previousMapPinLocation, mapPinLocation)
    ) {
      return;
    }

    // Handle click on point
    if (geojson && selectedFeatureIndex !== undefined) {
      const selectedFeature = geojson.features[selectedFeatureIndex];
      const [lon, lat] = selectedFeature.geometry.coordinates;
      const location = { lon, lat, id: selectedFeature.id?.toString() };

      setMapPinLocation(location);
      setTimeSeriesLocation(location);
      setSelectedLocation(selectedFeature.properties!.name);
      setLayerGeojson(
        addDrawFunctionToGeojson(
          geojson,
          circleDrawFunctionId,
          selectedCircleDrawFunctionId,
          selectedFeatureIndex,
        ),
      );
    } else if (selectedFeatureIndex === undefined) {
      // Handle click outside of point
      setTimeSeriesLocation(mapPinLocation);
      if (geojson) {
        setLayerGeojson(geojson);
      }
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [selectedFeatureIndex, mapPinLocation]);
};
