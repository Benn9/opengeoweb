/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */
export interface Geometry {
  type: string;
  coordinates: number[];
}

export interface Properties {
  timestep: Date[];
  propertyName: string;
  values: number[];
}

// properties provided by https://for.weather.fmibeta.com/sofp
export interface ObservationProperties {
  timestep: string[];
  observedPropertyName: string;
  result: string[];
}

export interface Feature {
  type: string;
  geometry: Geometry;
  properties: ObservationProperties;
}

export interface ParameterApiData {
  type: string;
  features: Feature[];
  timeStamp: string;
  numberReturned: number;
}

export interface PlotPreset {
  connectedMap?: string;
  mapId: string;
  plots: Plot[];
  parameters: Parameter[];
}

export interface Plot {
  title: string;
  plotId: string;
  enabled?: boolean;
  parameters?: Parameter[];
}

export interface PlotWithData extends Plot {
  parametersWithData: ParameterWithData[];
}

export type PlotType = 'bar' | 'line';

export interface Parameter {
  id?: string;
  plotId: string;
  unit?: string;
  propertyName: string;
  plotType: PlotType;
  enabled?: boolean;
  color?: string;
  serviceId: string;
}

export interface ParameterWithData extends Parameter {
  timestep: Date[];
  value: number[];
  unit: string;
}

export interface PointerLocation {
  lon: number;
  lat: number;
}

export interface TimeSeriesPresetLocation {
  lat: number;
  lon: number;
  name: string;
}

export interface EDRAxes {
  t: {
    values: string[];
  };
  x: {
    values: number[];
  };
  y: {
    values: number[];
  };
}

export interface EDRDomain {
  axes: EDRAxes;
}

export interface EDRObservedProperty {
  id: string;
  label: {
    fi: string;
  };
}

export interface EDRParameter {
  id: string;
  observedProperty: EDRObservedProperty;
  unit: {
    label: {
      fi: string;
    };
    symbol:
      | string
      | {
          type: string;
          value: string;
        };
  };
}

export interface EDRParameters {
  [parameterName: string]: EDRParameter;
}

export interface EDRRangeParameter {
  axisNames: string[];
  dataType: string;
  shape: number[];
  type: string;
  values: number[];
}

export interface EDRRange {
  [key: string]: EDRRangeParameter;
}

export interface EDRPositionResponse {
  type: string;
  domain: EDRDomain;
  parameters: EDRParameters;
  ranges?: EDRRange;
}

export interface EDRInstance {
  id: string;
  data_queries?: {
    position?: {
      link: {
        variables: {
          output_formats: string[];
        };
      };
    };
    locations?: {
      link: {
        variables: {
          output_formats: string[];
        };
      };
    };
  };
  extent?: {
    temporal: {
      interval: string[][];
    };
    spatial: {
      crs: string;
    };
  };
}

export interface ParameterApiDataRequest {
  urlDomain: string;
  lon: number;
  lat: number;
  parameterName: string;
}

// Supported interfaces are EDR and OGC Features API
export type ServiceInterface = 'EDR' | 'OGC';
