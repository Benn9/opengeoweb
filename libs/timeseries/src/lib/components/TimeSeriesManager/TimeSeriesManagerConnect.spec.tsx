/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */

import React from 'react';
import { render, screen } from '@testing-library/react';
import {
  CoreAppStore,
  uiReducer,
  uiTypes,
  webmapReducer,
} from '@opengeoweb/store';
import { createToolkitMockStoreWithEggs } from '@opengeoweb/shared';
import { TimeSeriesManagerConnect } from './TimeSeriesManagerConnect';
import { TimeSeriesThemeStoreProvider } from '../../storybookUtils/Providers';
import { TimeSeriesModuleState } from '../../store/types';
import { reducer } from '../../store/reducer';
import { mockTimeSeriesServices } from '../TimeSeries/mockTimeSeriesServices';

describe('src/components/TimeSeriesManager/TimeSeriesManagerConnect', () => {
  it('should display manager', async () => {
    const mockMapId = 'mockMapId';
    const coreStore = {
      webmap: {
        byId: {
          mockMapId: {
            bbox: {
              left: -450651.2255879827,
              bottom: 6490531.093143953,
              right: 1428345.8183648037,
              top: 7438773.776232235,
            },
            srs: 'EPSG:3857',
            id: 'test',
            isAnimating: false,
            isAutoUpdating: false,
            isEndTimeOverriding: false,
            baseLayers: [],
            overLayers: [],
            mapLayers: [],
            featureLayers: [],
          },
        },
        allIds: [mockMapId],
      },
      ui: {
        order: [uiTypes.DialogTypes.TimeSeriesManager],
        dialogs: {
          timeSeriesManager: {
            activeMapId: mockMapId,
            isOpen: true,
            type: uiTypes.DialogTypes.TimeSeriesManager,
            source: 'app',
          },
        },
      },
    } as CoreAppStore;

    const timeSeriesStore: TimeSeriesModuleState = {
      timeSeries: {
        plotPreset: {
          mapId: mockMapId,
          plots: [{ plotId: 'plotId1', title: 'title' }],
          parameters: [],
        },
        services: mockTimeSeriesServices,
        timeseriesSelect: {
          filters: { searchFilter: '' },
        },
      },
    };

    const mockState = {
      reducer: {
        timeSeries: reducer,
        ui: uiReducer,
        webmap: webmapReducer,
      },
      preloadedState: { ...timeSeriesStore, ...coreStore },
    };
    const store = createToolkitMockStoreWithEggs(mockState);
    render(
      <TimeSeriesThemeStoreProvider store={store}>
        <TimeSeriesManagerConnect />
      </TimeSeriesThemeStoreProvider>,
    );

    expect(
      screen.getByRole('heading', { name: /time series manager/i }),
    ).toBeInTheDocument();
  });
});
