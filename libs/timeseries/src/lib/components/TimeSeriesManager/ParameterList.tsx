/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */

import React, { FC } from 'react';
import { Box, Grid, MenuItem, SelectChangeEvent } from '@mui/material';
import { CustomIconButton, TooltipSelect } from '@opengeoweb/shared';
import {
  Copy,
  Delete,
  Info,
  Visibility,
  VisibilityOff,
} from '@opengeoweb/theme';
import { Parameter, Plot, PlotType } from '../TimeSeries/types';
import {
  BUTTON_WIDTH,
  LEFT_ACTION_WIDTH,
  RIGHT_ACTION_WIDTH,
  ROW_PARAMETER_WIDTH,
  ROW_PLOT_WIDTH,
  ROW_STYLE_WIDTH,
  ROW_TIME_WIDTH,
} from './TimeSeriesManagerUtils';
import { COLOR_MAP, COLOR_NAME_TO_HEX_MAP } from '../../constants';

export const ParameterList: FC<{
  plot: Plot;
  plotIsEnabled: boolean;
  deleteParameter: (id: string) => void;
  toggleParameter: (id: string) => void;
  updateParameter: (parameter: Parameter) => void;
}> = ({
  plot,
  plotIsEnabled,
  deleteParameter,
  toggleParameter,
  updateParameter,
}) => {
  return (
    <div>
      {plot.parameters &&
        plot.parameters.map((parameter) => {
          const parameterIsEnabled = parameter.enabled !== false;
          const rowIsEnabled = plotIsEnabled && parameterIsEnabled;
          const styles = getStyles(rowIsEnabled);
          return (
            <Box sx={styles.plot} key={parameter.propertyName}>
              <Grid container sx={styles.row}>
                <Grid item container sx={{ width: LEFT_ACTION_WIDTH }}>
                  <Grid item sx={{ width: BUTTON_WIDTH }} />
                  <Grid item sx={{ width: BUTTON_WIDTH }}>
                    <CustomIconButton
                      onClick={(): void => {
                        toggleParameter(parameter.id!);
                      }}
                      tooltipTitle={
                        (parameterIsEnabled ? 'Hide ' : 'Show ') +
                        parameter.propertyName
                      }
                      shouldShowAsDisabled={!rowIsEnabled}
                      data-testid="enableButton"
                    >
                      {rowIsEnabled ? <Visibility /> : <VisibilityOff />}
                    </CustomIconButton>
                  </Grid>
                </Grid>
                <Grid item sx={{ width: ROW_PLOT_WIDTH }}>
                  {' '}
                </Grid>
                <Grid
                  item
                  sx={[styles.rowText, { width: ROW_PARAMETER_WIDTH }]}
                >
                  {parameter.propertyName}
                </Grid>
                <Grid item container sx={{ width: ROW_STYLE_WIDTH }}>
                  <Grid item>
                    <ParameterTypeSelect
                      rowIsEnabled={rowIsEnabled}
                      parameter={parameter}
                      updateParameter={updateParameter}
                    />
                  </Grid>
                  <Grid item>
                    <ParameterColorSelect
                      rowIsEnabled={rowIsEnabled}
                      parameter={parameter}
                      updateParameter={updateParameter}
                    />
                  </Grid>
                </Grid>

                <Grid item sx={{ width: ROW_TIME_WIDTH }} />

                <Grid item container sx={{ width: RIGHT_ACTION_WIDTH }}>
                  <Grid
                    item
                    sx={{
                      width: BUTTON_WIDTH,
                      marginLeft: 'auto',
                      marginRight: 0,
                    }}
                  >
                    <CustomIconButton
                      tooltipTitle="Show info"
                      shouldShowAsDisabled={true}
                      data-testid="infoButton"
                    >
                      <Info />
                    </CustomIconButton>
                  </Grid>
                  <Grid
                    item
                    sx={{
                      width: BUTTON_WIDTH,
                      marginLeft: 'auto',
                      marginRight: 0,
                    }}
                  >
                    <CustomIconButton
                      tooltipTitle={`Copy ${parameter.propertyName}`}
                      shouldShowAsDisabled={true}
                      data-testid="copyButton"
                    >
                      <Copy />
                    </CustomIconButton>
                  </Grid>
                  <Grid
                    item
                    sx={{
                      width: BUTTON_WIDTH,
                      marginLeft: 'auto',
                      marginRight: 0,
                    }}
                  >
                    <CustomIconButton
                      tooltipTitle={`Remove ${parameter.propertyName}`}
                      onClick={(): void => {
                        deleteParameter(parameter.id!);
                      }}
                      shouldShowAsDisabled={!plotIsEnabled}
                      data-testid="deleteButton"
                    >
                      <Delete />
                    </CustomIconButton>
                  </Grid>
                </Grid>
              </Grid>
            </Box>
          );
        })}
    </div>
  );
};

function getTextColor(bgColor?: string): string {
  if (!bgColor) {
    return '#fff';
  }
  return parseInt(bgColor.replace('#', ''), 16) > 0xffffff / 2
    ? '#000'
    : '#fff';
}

const ParameterColorSelect: FC<{
  rowIsEnabled: boolean;
  parameter: Parameter;
  updateParameter: (parameter: Parameter) => void;
}> = ({ rowIsEnabled, parameter, updateParameter }) => {
  const parameterColor: string =
    parameter.color ??
    COLOR_MAP[parameter.propertyName as keyof typeof COLOR_MAP] ??
    '#000';
  const textColor = getTextColor(
    COLOR_NAME_TO_HEX_MAP[parameterColor as keyof typeof COLOR_NAME_TO_HEX_MAP],
  );
  return (
    <TooltipSelect
      hasBackgroundColor={false}
      value={parameterColor}
      tooltip="Choose a color"
      isEnabled={rowIsEnabled}
      style={{
        backgroundColor: parameterColor,
        color: textColor,
      }}
      onChange={(event: SelectChangeEvent): void => {
        updateParameter({ ...parameter, color: event.target.value });
      }}
    >
      <MenuItem disabled>Colors</MenuItem>
      {Object.keys(COLOR_NAME_TO_HEX_MAP).map((name) => {
        const textColor = getTextColor(
          COLOR_NAME_TO_HEX_MAP[name as keyof typeof COLOR_NAME_TO_HEX_MAP],
        );
        return (
          <MenuItem
            key={name}
            value={name}
            sx={{
              backgroundColor: `${name} !important`,
              color: `${textColor} !important`,
            }}
          >
            {name}
          </MenuItem>
        );
      })}
    </TooltipSelect>
  );
};

const ParameterTypeSelect: FC<{
  rowIsEnabled: boolean;
  parameter: Parameter;
  updateParameter: (parameter: Parameter) => void;
}> = ({ rowIsEnabled, parameter, updateParameter }) => {
  return (
    <TooltipSelect
      value={parameter.plotType}
      tooltip="Choose a type"
      isEnabled={rowIsEnabled}
      onChange={(event: SelectChangeEvent): void => {
        updateParameter({
          ...parameter,
          plotType: event.target.value as PlotType,
        });
      }}
    >
      <MenuItem disabled>Type</MenuItem>
      <MenuItem value="line">Line</MenuItem>
      <MenuItem value="bar">Bar</MenuItem>
    </TooltipSelect>
  );
};

// eslint-disable-next-line @typescript-eslint/explicit-function-return-type
const getStyles = (isEnabled: boolean) => ({
  plot: {
    width: 'calc(100% - 8px)',
  },
  row: {
    backgroundColor: isEnabled
      ? 'geowebColors.layerManager.tableRowDefaultCardContainer.fill'
      : 'geowebColors.layerManager.tableRowDisabledCardContainer.fill',
    borderStyle: 'solid',
    borderWidth: '1px',
    borderColor: 'geowebColors.cards.cardContainerBorder',
    borderRadius: 1,
    marginBottom: 0.25,
    marginLeft: 0.5,
    marginRight: 0.5,
    height: 34,
    alignItems: 'center',
  },
  rowText: {
    fontSize: 12,
    fontWeight: 500,
    color: isEnabled
      ? 'geowebColors.layerManager.tableRowDefaultText.color'
      : 'geowebColors.layerManager.tableRowDisabledText.color',
  },
});
