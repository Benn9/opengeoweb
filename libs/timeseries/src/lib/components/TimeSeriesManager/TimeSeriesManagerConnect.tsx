/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */

import React, { FC, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useSetupDialog, uiTypes } from '@opengeoweb/store';
import { Parameter, PlotPreset } from '../TimeSeries/types';
import { TimeSeriesManager } from './TimeSeriesManager';
import { getPlotState } from '../../store/selectors';
import { TimeSeriesSelectConnect } from '../TimeSeriesSelect/TimeSeriesSelectConnect';
import { timeSeriesActions } from '../../store';

export const DIALOG_TYPE_MANAGER = uiTypes.DialogTypes.TimeSeriesManager;

export const TimeSeriesManagerConnect: FC = () => {
  const { setDialogOrder, dialogOrder, isDialogOpen, onCloseDialog } =
    useSetupDialog(DIALOG_TYPE_MANAGER);

  const [selectPlotId, setSelectPlotId] = useState('');
  const dispatch = useDispatch();

  const plotState: PlotPreset | undefined = useSelector(getPlotState);

  const deletePlot = (id: string): void => {
    dispatch(timeSeriesActions.deletePlot(id));
  };
  const addPlot = (title: string): void => {
    dispatch(timeSeriesActions.addPlot({ title }));
  };
  const deleteParameter = (id: string): void => {
    dispatch(timeSeriesActions.deleteParameter({ id }));
  };
  const togglePlot = (id: string): void => {
    dispatch(timeSeriesActions.togglePlot(id));
  };
  const toggleParameter = (id: string): void => {
    dispatch(timeSeriesActions.toggleParameter({ id }));
  };
  const updateParameter = (parameter: Parameter): void => {
    dispatch(timeSeriesActions.updateParameter({ parameter }));
  };
  return (
    <>
      {plotState && (
        <TimeSeriesManager
          isOpen={isDialogOpen}
          onClose={onCloseDialog}
          onMouseDown={setDialogOrder}
          order={dialogOrder}
          plotState={plotState}
          addPlot={addPlot}
          deletePlot={deletePlot}
          togglePlot={togglePlot}
          deleteParameter={deleteParameter}
          toggleParameter={toggleParameter}
          setSelectPlotId={setSelectPlotId}
          updateParameter={updateParameter}
        />
      )}
      <TimeSeriesSelectConnect selectPlotId={selectPlotId} />
    </>
  );
};
