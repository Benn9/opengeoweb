/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';

import { darkTheme, lightTheme } from '@opengeoweb/theme';
import { createStore } from '@opengeoweb/store';
import { TimeSeriesThemeStoreProvider } from '../../storybookUtils/Providers';
import { PlotPreset } from '../TimeSeries/types';
import { TimeSeriesManager } from './TimeSeriesManager';

const TimeSeriesManagerStory: React.FC = () => {
  return (
    <TimeSeriesManager
      isOpen={true}
      onClose={(): void => {}}
      onMouseDown={(): void => {}}
      order={1}
      addPlot={(): void => {}}
      deleteParameter={(): void => {}}
      deletePlot={(): void => {}}
      plotState={plotPreset}
      setSelectPlotId={(): void => {}}
      toggleParameter={(): void => {}}
      togglePlot={(): void => {}}
      updateParameter={(): void => {}}
      startPosition={{ top: 0, left: 0 }}
    />
  );
};

const plotPreset: PlotPreset = {
  mapId: 'TimeseriesMap',
  plots: [
    {
      title: 'Plot 1',
      plotId: 'Plot_1',
      enabled: false,
    },
    {
      title: 'Plot 2',
      plotId: 'Plot_2',
    },
  ],
  parameters: [
    {
      plotId: 'Plot_1',
      unit: '°C',
      propertyName: 'Temperature',
      plotType: 'line',
      serviceId: 'fmi',
    },
    {
      plotId: 'Plot_2',
      unit: 'mm',
      propertyName: 'Precipitation1h',
      plotType: 'bar',
      serviceId: 'fmi',
    },
    {
      plotId: 'Plot_2',
      unit: 'mm',
      propertyName: 'Precipitation12h',
      plotType: 'bar',
      serviceId: 'fmi',
      enabled: false,
    },
  ],
};

export const TimeSeriesManagerDemoLightTheme = (): React.ReactElement => (
  <TimeSeriesThemeStoreProvider store={createStore()} theme={lightTheme}>
    <TimeSeriesManagerStory />
  </TimeSeriesThemeStoreProvider>
);

export const TimeSeriesManagerDemoDarkTheme = (): React.ReactElement => (
  <TimeSeriesThemeStoreProvider store={createStore()} theme={darkTheme}>
    <TimeSeriesManagerStory />
  </TimeSeriesThemeStoreProvider>
);

TimeSeriesManagerDemoLightTheme.parameters = {
  zeplinLink: [
    {
      name: 'Light theme',
      link: 'https://app.zeplin.io/project/5ecf84a3c6ae1047a368f393/screen/6533c4eb93a6ff0d1bf79b24',
    },
  ],
};
TimeSeriesManagerDemoLightTheme.storyName =
  'TimeSeriesManager light theme (takeSnapshot)';

TimeSeriesManagerDemoDarkTheme.parameters = {
  zeplinLink: [
    {
      name: 'Light theme',
      link: 'https://app.zeplin.io/project/5ecf84a3c6ae1047a368f393/screen/653bd619c918a3227e46e0eb',
    },
  ],
};
TimeSeriesManagerDemoDarkTheme.storyName =
  'TimeSeriesManager dark theme (takeSnapshot)';

export default { title: 'components/TimeSeriesManager' };
