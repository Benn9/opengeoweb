/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */

import React, { FC } from 'react';
import { Box, Grid } from '@mui/material';
import { groupBy } from 'lodash';
import { Position, ToolContainerDraggable } from '@opengeoweb/shared';
import { Parameter, Plot, PlotPreset } from '../TimeSeries/types';
import { PlotRows } from './PlotRows';
import {
  RIGHT_ACTION_WIDTH,
  LEFT_ACTION_WIDTH,
  TITLE_PARAMETER_WIDTH,
  TITLE_PLOT_WIDTH,
  TITLE_STYLE_WIDTH,
  TITLE_TIME_WIDTH,
} from './TimeSeriesManagerUtils';
import { AddPlotButton } from '../TimeSeriesAddPlot/TimeSeriesAddPlot';

export const styles = {
  headerText: {
    fontSize: 12,
    opacity: 0.67,
  },
};

export const TimeSeriesManager: FC<{
  onClose: () => void;
  onMouseDown: () => void;
  isOpen: boolean;
  order: number;
  plotState: PlotPreset;
  addPlot: (title: string) => void;
  deletePlot: (id: string) => void;
  togglePlot: (id: string) => void;
  deleteParameter: (id: string) => void;
  toggleParameter: (id: string) => void;
  setSelectPlotId: (id: string) => void;
  startPosition?: Position;
  updateParameter: (parameter: Parameter) => void;
}> = ({
  onClose,
  isOpen,
  onMouseDown,
  order,
  plotState,
  addPlot,
  deletePlot,
  togglePlot,
  deleteParameter,
  toggleParameter,
  setSelectPlotId,
  startPosition = { top: 85, left: 50 },
  updateParameter,
}) => {
  const parametersGroupedByPlot = groupBy(
    plotState.parameters,
    (parameter) => parameter.plotId,
  );

  const plotHierarchy: Plot[] = plotState.plots.map((plot) => {
    const plotParameters = parametersGroupedByPlot[plot.plotId];
    return { ...plot, parameters: plotParameters };
  });

  return (
    <ToolContainerDraggable
      title="Time Series Manager"
      startSize={{ width: 720, height: 300 }}
      minWidth={530}
      minHeight={126}
      startPosition={startPosition}
      isOpen={isOpen}
      onMouseDown={onMouseDown}
      order={order}
      onClose={onClose}
    >
      <Box sx={{ padding: '0 6px' }}>
        <Grid
          container
          sx={{ height: 32, margin: '8px 0 8px 0' }}
          alignItems="center"
        >
          <Grid item sx={{ width: LEFT_ACTION_WIDTH }}>
            <AddPlotButton handleClick={addPlot} plotState={plotState} />
          </Grid>
          <Grid item sx={[styles.headerText, { width: TITLE_PLOT_WIDTH }]}>
            Collection
          </Grid>
          <Grid item sx={[styles.headerText, { width: TITLE_PARAMETER_WIDTH }]}>
            Parameter
          </Grid>
          <Grid item sx={[styles.headerText, { width: TITLE_STYLE_WIDTH }]}>
            Style
          </Grid>
          <Grid item sx={[styles.headerText, { width: TITLE_TIME_WIDTH }]}>
            Ref time
          </Grid>
          <Grid item sx={{ width: RIGHT_ACTION_WIDTH }} />
        </Grid>

        {plotHierarchy.map((plot) => (
          <PlotRows
            key={plot.plotId}
            plot={plot}
            deletePlot={deletePlot}
            togglePlot={togglePlot}
            deleteParameter={deleteParameter}
            toggleParameter={toggleParameter}
            setSelectPlotId={setSelectPlotId}
            updateParameter={updateParameter}
          />
        ))}
      </Box>
    </ToolContainerDraggable>
  );
};
