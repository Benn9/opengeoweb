/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */

export const BUTTON_WIDTH = 30;

const leftButtonsWidth = 2 * BUTTON_WIDTH + 10;
const rightButtonsWidth = 3 * BUTTON_WIDTH;

const nonFlexibleTitleAreaWidth = leftButtonsWidth + rightButtonsWidth;

const nonFlexibleRowAreaWidth = leftButtonsWidth + rightButtonsWidth + 8;

const flexibleTitleAreaWidth = `(100% - ${nonFlexibleTitleAreaWidth}px)`;

const flexibleRowAreaWidth = `(100% - ${nonFlexibleRowAreaWidth}px)`;

export const RIGHT_ACTION_WIDTH = rightButtonsWidth;
export const LEFT_ACTION_WIDTH = leftButtonsWidth;
export const TITLE_PLOT_WIDTH = `calc(${flexibleTitleAreaWidth} * 0.15)`;
export const ROW_PLOT_WIDTH = `calc(${flexibleRowAreaWidth} * 0.15)`;
export const TITLE_PARAMETER_WIDTH = `calc(${flexibleTitleAreaWidth} * 0.25)`;
export const ROW_PARAMETER_WIDTH = `calc(${flexibleRowAreaWidth} * 0.25)`;
export const TITLE_TIME_WIDTH = `calc(${flexibleTitleAreaWidth} * 0.3)`;
export const ROW_TIME_WIDTH = `calc(${flexibleRowAreaWidth} * 0.3)`;
export const TITLE_STYLE_WIDTH = `calc(${flexibleTitleAreaWidth} * 0.3)`;
export const ROW_STYLE_WIDTH = `calc(${flexibleRowAreaWidth} * 0.3)`;
