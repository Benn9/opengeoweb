/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */

import React, { FC } from 'react';
import { Box, Collapse, Grid } from '@mui/material';
import { CustomIconButton } from '@opengeoweb/shared';
import {
  ChevronDown,
  ChevronUp,
  Delete,
  DragHandle,
  Visibility,
  VisibilityOff,
} from '@opengeoweb/theme';
import { Parameter, Plot } from '../TimeSeries/types';
import {
  BUTTON_WIDTH,
  LEFT_ACTION_WIDTH,
  TITLE_PLOT_WIDTH,
} from './TimeSeriesManagerUtils';
import { TimeSeriesSelectButtonConnect } from '../TimeSeriesSelect/TimeSeriesSelectButtonConnect';
import { ParameterList } from './ParameterList';

export const PlotRows: FC<{
  plot: Plot;
  deletePlot: (id: string) => void;
  deleteParameter: (id: string) => void;
  togglePlot: (id: string) => void;
  setSelectPlotId: (id: string) => void;
  toggleParameter: (id: string) => void;
  updateParameter: (parameter: Parameter) => void;
}> = ({
  plot,
  deletePlot,
  togglePlot,
  deleteParameter,
  setSelectPlotId,
  toggleParameter,
  updateParameter,
}) => {
  const [openParameterList, setOpenParameterList] = React.useState(true);
  const plotIsEnabled = plot.enabled !== false;
  const styles = getStyles(plotIsEnabled);
  return (
    <Box sx={styles.plot} key={plot.plotId}>
      <Grid sx={styles.row} container>
        <Grid item container sx={{ width: LEFT_ACTION_WIDTH }}>
          <Grid item sx={{ width: BUTTON_WIDTH }}>
            <CustomIconButton
              shouldShowAsDisabled={true}
              data-testid="dragHandleButton"
            >
              <DragHandle />
            </CustomIconButton>
          </Grid>
          <Grid item sx={{ width: BUTTON_WIDTH }}>
            <CustomIconButton
              onClick={(): void => {
                togglePlot(plot.plotId);
              }}
              tooltipTitle={(plotIsEnabled ? 'Hide ' : 'Show ') + plot.title}
              shouldShowAsDisabled={!plotIsEnabled}
              data-testid="enableButton"
            >
              {plotIsEnabled ? <Visibility /> : <VisibilityOff />}
            </CustomIconButton>
          </Grid>
        </Grid>
        <Grid item sx={{ width: TITLE_PLOT_WIDTH }}>
          <TimeSeriesSelectButtonConnect
            isEnabled={plotIsEnabled}
            onClick={(): void => setSelectPlotId(plot.plotId)}
          />
        </Grid>
        <Grid item xs={true}>
          <Box sx={styles.rowText}>{plot.title}</Box>
        </Grid>
        <Grid item sx={{ width: BUTTON_WIDTH }}>
          <CustomIconButton
            tooltipTitle={`Remove ${plot.title}`}
            onClick={(): void => {
              deletePlot(plot.plotId);
            }}
            shouldShowAsDisabled={!plotIsEnabled}
            data-testid="deleteButton"
          >
            <Delete />
          </CustomIconButton>
        </Grid>
        <Grid item sx={{ width: BUTTON_WIDTH * 1.4 }} />
        <Grid item sx={{ width: BUTTON_WIDTH }}>
          <CustomIconButton
            tooltipTitle="Open parameter list"
            onClick={(event: React.MouseEvent): void => {
              event.stopPropagation();
              setOpenParameterList(!openParameterList);
            }}
            data-testid="openParameterListButton"
          >
            {openParameterList ? <ChevronUp /> : <ChevronDown />}
          </CustomIconButton>
        </Grid>
      </Grid>
      <Collapse
        sx={styles.parameter}
        in={openParameterList}
        timeout="auto"
        unmountOnExit
      >
        <ParameterList
          plot={plot}
          plotIsEnabled={plotIsEnabled}
          deleteParameter={deleteParameter}
          toggleParameter={toggleParameter}
          updateParameter={updateParameter}
        />
      </Collapse>
    </Box>
  );
};

// eslint-disable-next-line @typescript-eslint/explicit-function-return-type
const getStyles = (isEnabled: boolean) => ({
  plot: {
    boxShadow: 3,
    borderRadius: 0.75,
  },
  row: {
    backgroundColor: isEnabled
      ? 'geowebColors.layerManager.tableRowDefaultCardContainer.fill'
      : 'geowebColors.layerManager.tableRowDisabledCardContainer.fill',
    borderStyle: 'solid',
    borderWidth: '1px',
    borderColor: 'geowebColors.cards.cardContainerBorder',
    borderRadius: 1,
    marginBottom: 0.25,
    height: 34,
    alignItems: 'center',
  },
  rowText: {
    fontSize: 14,
    fontWeight: 500,
    color: isEnabled
      ? 'geowebColors.layerManager.tableRowDefaultText.color'
      : 'geowebColors.layerManager.tableRowDisabledText.color',
  },
  parameter: {
    marginBottom: 0.25,
  },
});
