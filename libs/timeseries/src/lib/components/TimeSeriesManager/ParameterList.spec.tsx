/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { fireEvent, render, screen } from '@testing-library/react';

import { TimeSeriesThemeProvider } from '../../storybookUtils/Providers';
import { Plot } from '../TimeSeries/types';
import { ParameterList } from './ParameterList';

const mockPlot: Plot = {
  title: 'Plot 1',
  plotId: 'plot_1',
  parameters: [
    {
      id: '1',
      plotId: 'plot_1',
      unit: '°C',
      propertyName: 'Temperature',
      plotType: 'line',
      serviceId: 'fmi',
    },
    {
      id: '2',
      plotId: 'plot_2',
      unit: 'mm',
      propertyName: 'Precipitation1h',
      plotType: 'bar',
      serviceId: 'fmi',
    },
  ],
};

const mockDeleteParameter = jest.fn();
const mockToggleParameter = jest.fn();
const mockUpdateParameter = jest.fn();
describe('src/components/TimeSeriesManager/ParameterList', () => {
  it('should render without crashing', () => {
    render(
      <TimeSeriesThemeProvider>
        <ParameterList
          plot={mockPlot}
          plotIsEnabled={true}
          deleteParameter={mockDeleteParameter}
          toggleParameter={mockToggleParameter}
          updateParameter={mockUpdateParameter}
        />
      </TimeSeriesThemeProvider>,
    );
  });

  it('should render correct number of parameters', () => {
    render(
      <TimeSeriesThemeProvider>
        <ParameterList
          plot={mockPlot}
          plotIsEnabled={true}
          deleteParameter={mockDeleteParameter}
          toggleParameter={mockToggleParameter}
          updateParameter={mockUpdateParameter}
        />
      </TimeSeriesThemeProvider>,
    );
    expect(screen.getAllByTestId('enableButton')).toHaveLength(
      mockPlot.parameters!.length,
    );
  });

  it('should call deleteParameter when delete button is clicked', () => {
    render(
      <TimeSeriesThemeProvider>
        <ParameterList
          plot={mockPlot}
          plotIsEnabled={true}
          deleteParameter={mockDeleteParameter}
          toggleParameter={mockToggleParameter}
          updateParameter={mockUpdateParameter}
        />
      </TimeSeriesThemeProvider>,
    );
    fireEvent.click(screen.getAllByTestId('deleteButton')[0]);
    expect(mockDeleteParameter).toBeCalledWith('1');
  });

  it('should call toggleParameter when visibility button is clicked', () => {
    render(
      <TimeSeriesThemeProvider>
        <ParameterList
          plot={mockPlot}
          plotIsEnabled={true}
          deleteParameter={mockDeleteParameter}
          toggleParameter={mockToggleParameter}
          updateParameter={mockUpdateParameter}
        />
      </TimeSeriesThemeProvider>,
    );
    fireEvent.click(screen.getAllByTestId('enableButton')[0]);
    expect(mockToggleParameter).toBeCalledWith('1');
  });
});
