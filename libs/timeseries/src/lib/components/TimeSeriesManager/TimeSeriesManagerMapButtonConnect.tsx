/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { CustomIconButton } from '@opengeoweb/shared';
import {
  CoreAppStore,
  uiActions,
  uiSelectors,
  uiTypes,
} from '@opengeoweb/store';
import { TimeSeriesManager } from '@opengeoweb/theme';
import { DIALOG_TYPE_MANAGER } from './TimeSeriesManagerConnect';

export const TOOLTIP_TITLE = 'Timeseries Manager';

interface TimeSeriesManagerMapButtonConnectProps {
  mapId: string;
  source?: uiTypes.Source;
}

const TimeSeriesManagerMapButtonConnect: React.FC<TimeSeriesManagerMapButtonConnectProps> =
  ({ mapId, source = 'app' }: TimeSeriesManagerMapButtonConnectProps) => {
    const dispatch = useDispatch();

    const currentActiveMapId = useSelector((store: CoreAppStore) =>
      uiSelectors.getDialogMapId(store, DIALOG_TYPE_MANAGER),
    );

    const isOpenInStore = useSelector((store: CoreAppStore) =>
      uiSelectors.getisDialogOpen(store, DIALOG_TYPE_MANAGER),
    );

    const openTimeSeriesManagerDialog = React.useCallback((): void => {
      dispatch(
        uiActions.setActiveMapIdForDialog({
          type: DIALOG_TYPE_MANAGER,
          mapId,
          setOpen: currentActiveMapId !== mapId ? true : !isOpenInStore,
          source,
        }),
      );
    }, [currentActiveMapId, dispatch, isOpenInStore, mapId, source]);

    const isOpen = currentActiveMapId === mapId && isOpenInStore;

    return (
      <CustomIconButton
        data-testid="timeSeriesManagerButton"
        onClick={openTimeSeriesManagerDialog}
        isSelected={isOpen}
        tooltipProps={{
          title: TOOLTIP_TITLE,
          placement: 'right',
        }}
        variant="tool"
      >
        <TimeSeriesManager />
      </CustomIconButton>
    );
  };

export default TimeSeriesManagerMapButtonConnect;
