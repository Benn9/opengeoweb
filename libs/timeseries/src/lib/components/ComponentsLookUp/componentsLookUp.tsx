/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */
import React from 'react';
import { ApiProvider } from '@opengeoweb/api';
import produce from 'immer';
import { TimeSeriesConnectWrapper } from '../TimeSeries/TimeSeriesConnectWrapper';
import { PlotPreset, TimeSeriesPresetLocation } from '../TimeSeries/types';
import { createApi } from '../../utils/api';
import { TimeSeriesService } from '../../store/types';

interface InitialTimeseriesProps {
  plotPreset: PlotPreset;
  services: TimeSeriesService[];
  predefinedLocations?: TimeSeriesPresetLocation[];
}

export type SupportedComponentTypes = 'TimeSeries';

export type InitialProps = InitialTimeseriesProps;

export interface ComponentsLookUpPayload {
  componentType: SupportedComponentTypes;
  id: string;
  title?: string;
  initialProps: InitialProps;
  productConfigKey?: string;
  connectedMap?: string;
}

interface TimeSeriesServiceOld {
  id: string;
  interface: string;
  serviceUrl: string;
}

/**
 * The lookup table is for registering your own components with the workspace.
 * @param payload
 * @returns
 */
export const componentsLookUp = ({
  componentType,
  initialProps,
  productConfigKey,
  connectedMap,
}: ComponentsLookUpPayload): React.ReactElement => {
  switch (componentType) {
    case 'TimeSeries': {
      // TODO: https://gitlab.com/opengeoweb/opengeoweb/-/issues/4389 Remove when implemented
      let { services } = initialProps;
      if (!services || !services.length) {
        const OldService = initialProps.plotPreset as unknown as {
          services: TimeSeriesServiceOld[];
        };
        services = OldService.services.map((elem) => {
          return {
            url: elem.serviceUrl,
            description: 'something',
            id: elem.id,
            type: elem.interface,
          } as TimeSeriesService;
        });
      }

      return (
        <ApiProvider createApi={createApi} data-testid="timeseriesPlot">
          <TimeSeriesConnectWrapper
            timeSeriesPreset={produce(initialProps, (draft) => {
              draft.plotPreset.connectedMap = connectedMap;
              draft.services = services;
            })}
            productConfigKey={productConfigKey!}
          />
        </ApiProvider>
      );
    }
    default:
      return null!;
  }
};
