/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';

import { darkTheme, lightTheme } from '@opengeoweb/theme';

import { createStore } from '@opengeoweb/store';
import { useDispatch } from 'react-redux';
import { TimeSeriesSelect } from './TimeSeriesSelect';
import { TimeSeriesThemeStoreProvider } from '../../storybookUtils/Providers';
import { Plot } from '../TimeSeries/types';
import { timeSeriesActions } from '../../store';
import { TimeSeriesService } from '../../store/types';

const TimeSeriesSelectDemo: React.FC = () => {
  const dispatch = useDispatch();
  const plot: Plot = {
    title: 'Plot 1',
    plotId: 'Plot_1',
    parameters: [
      {
        plotId: 'Plot_1',
        unit: '°C',
        propertyName: 'Temperature',
        plotType: 'line',
        serviceId: 'fmi',
      },
      {
        plotId: 'Plot_2',
        unit: 'mm',
        propertyName: 'Precipitation1h',
        plotType: 'bar',
        serviceId: 'fmi',
      },
      {
        plotId: 'Plot_3',
        propertyName: 'GeopHeight',
        unit: 'm2 s-2',
        plotType: 'line',
        serviceId: 'fmi',
      },
      {
        plotId: 'Plot_4',
        propertyName: 'DewPoint',
        unit: 'C',
        plotType: 'line',
        serviceId: 'fmi',
      },
      {
        plotId: 'Plot_5',
        propertyName: 'Humidity',
        unit: '%',
        plotType: 'line',
        serviceId: 'fmi',
      },
    ],
  };
  const filterText = 'cloud_area_fraction';

  dispatch(timeSeriesActions.setSearchFilter({ filterText }));

  return (
    <div style={{ height: '100vh', backgroundColor: 'grey' }}>
      <TimeSeriesSelect
        isOpen={true}
        order={0}
        onClose={(): void => {}}
        onMouseDown={(): void => {}}
        selectPlot={plot}
        handleAddOrRemoveClick={(): void => {}}
        services={[{} as TimeSeriesService]}
        searchFilter={filterText}
      />
    </div>
  );
};

export const TimeSeriesSelectDemoLightTheme = (): React.ReactElement => (
  <TimeSeriesThemeStoreProvider store={createStore()} theme={lightTheme}>
    <TimeSeriesSelectDemo />
  </TimeSeriesThemeStoreProvider>
);
TimeSeriesSelectDemoLightTheme.storyName =
  'TimeSeriesSelect light theme (takeSnapshot)';

export const TimeSeriesSelectDemoDarkTheme = (): React.ReactElement => (
  <TimeSeriesThemeStoreProvider store={createStore()} theme={darkTheme}>
    <TimeSeriesSelectDemo />
  </TimeSeriesThemeStoreProvider>
);

TimeSeriesSelectDemoDarkTheme.storyName =
  'TimeSeriesSelect dark theme (takeSnapshot)';

export default { title: 'components/TimeSeriesSelect' };
