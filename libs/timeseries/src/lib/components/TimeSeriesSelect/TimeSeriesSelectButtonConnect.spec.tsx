/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { render, fireEvent, screen } from '@testing-library/react';
import { createMockStoreWithEggs } from '@opengeoweb/shared';
import { uiActions, uiTypes } from '@opengeoweb/store';
import { TimeSeriesThemeStoreProvider } from '../../storybookUtils/Providers';
import { TimeSeriesSelectButtonConnect } from './TimeSeriesSelectButtonConnect';

describe('components/TimeSeriesSelect/TimeSeriesSelectButtonConnect', () => {
  it('should dispatch action with passed in mapid when clicked', async () => {
    const mapId = 'mapId';
    const mockState = {
      ui: {
        dialogs: {
          timeSeriesSelect: {
            type: uiTypes.DialogTypes.TimeSeriesSelect,
            activeMapId: 'differentMapId',
            isOpen: true,
          },
        },
      },
      timeSeries: { plotPreset: { mapId } },
    };
    const store = createMockStoreWithEggs(mockState);
    const mockOnClick = jest.fn();
    render(
      <TimeSeriesThemeStoreProvider store={store}>
        <TimeSeriesSelectButtonConnect onClick={mockOnClick} isEnabled={true} />
      </TimeSeriesThemeStoreProvider>,
    );

    fireEvent.click(screen.getByTestId('timeSeriesSelectButton'));
    const expectedAction = [
      uiActions.setActiveMapIdForDialog({
        type: uiTypes.DialogTypes.TimeSeriesSelect,
        mapId,
        setOpen: true,
        source: 'app',
      }),
    ];
    expect(store.getActions()).toEqual(expectedAction);
    expect(mockOnClick).toBeCalledTimes(1);
  });
  it('should not dispatch action if dialog is already open for corresponding map', () => {
    const mapId = 'mapId';
    const mockState = {
      ui: {
        dialogs: {
          timeSeriesSelect: {
            type: uiTypes.DialogTypes.TimeSeriesSelect,
            activeMapId: mapId,
            isOpen: true,
          },
        },
      },
      timeSeries: { plotPreset: { mapId } },
    };
    const store = createMockStoreWithEggs(mockState);

    render(
      <TimeSeriesThemeStoreProvider store={store}>
        <TimeSeriesSelectButtonConnect onClick={jest.fn()} isEnabled={true} />
      </TimeSeriesThemeStoreProvider>,
    );

    fireEvent.click(screen.getByTestId('timeSeriesSelectButton'));
    expect(store.getActions()).toEqual([]);
  });
});
