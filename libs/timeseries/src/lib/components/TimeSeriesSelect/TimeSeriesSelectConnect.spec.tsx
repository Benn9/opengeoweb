/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import {
  act,
  fireEvent,
  render,
  screen,
  waitFor,
  within,
} from '@testing-library/react';
import {
  uiTypes,
  uiReducer,
  webmapReducer,
  CoreAppStore,
  mapActions,
  uiActions,
} from '@opengeoweb/store';
import { createToolkitMockStoreWithEggs } from '@opengeoweb/shared';
import axios from 'axios';
import { TimeSeriesThemeStoreProvider } from '../../storybookUtils/Providers';
import { TimeSeriesSelectConnect } from './TimeSeriesSelectConnect';
import { TimeSeriesModuleState, TimeSeriesService } from '../../store/types';
import { ogcParameters } from './TimeSeriesSelect';
import { reducer } from '../../store/reducer';
import { mockTimeSeriesServices as services } from '../TimeSeries/mockTimeSeriesServices';
import { ServiceInterface } from '../TimeSeries/types';
import { timeSeriesActions } from '../../store';

jest.mock('axios');

describe('components/TimeSeriesSelect/TimeSeriesSelectConnect', () => {
  const mockMapId = 'mockMapId';

  it('should not show select dialog if dialog is not in ui store', () => {
    const timeSeriesStore: TimeSeriesModuleState = {
      timeSeries: {
        plotPreset: {
          mapId: mockMapId,
          plots: [{ plotId: 'plotId1', title: 'title' }],
          parameters: [],
        },
        services,
        timeseriesSelect: {
          filters: { searchFilter: '' },
        },
      },
    };

    const mockState = {
      reducer: { timeSeries: reducer },
      preloadedState: timeSeriesStore,
    };
    const store = createToolkitMockStoreWithEggs(mockState);

    render(
      <TimeSeriesThemeStoreProvider store={store}>
        <TimeSeriesSelectConnect selectPlotId="plotIdNotInStore" />
      </TimeSeriesThemeStoreProvider>,
    );

    expect(
      screen.queryByRole('heading', {
        name: /timeseries select/i,
      }),
    ).not.toBeInTheDocument();
  });

  const coreStore = {
    webmap: {
      byId: {
        mockMapId: {
          bbox: {
            left: -450651.2255879827,
            bottom: 6490531.093143953,
            right: 1428345.8183648037,
            top: 7438773.776232235,
          },
          srs: 'EPSG:3857',
          id: 'test',
          isAnimating: false,
          isAutoUpdating: false,
          isEndTimeOverriding: false,
          baseLayers: [],
          overLayers: [],
          mapLayers: [],
          featureLayers: [],
        },
      },
      allIds: [mockMapId],
    },
    ui: {
      order: [uiTypes.DialogTypes.TimeSeriesSelect],
      dialogs: {
        timeSeriesSelect: {
          activeMapId: mockMapId,
          isOpen: true,
          type: uiTypes.DialogTypes.TimeSeriesSelect,
          source: 'app',
        },
      },
    },
  } as CoreAppStore;
  const plotId = 'plotId1';

  it('should handle adding and removing parameters', async () => {
    const parameterId = 'parameter1';
    const temperatureParameter = ogcParameters[0];
    const service: TimeSeriesService = {
      id: temperatureParameter.serviceId,
      url: 'serviceUrl',
      type: 'OGC' as ServiceInterface,
      description: 'A description',
    };

    const timeSeriesStore: TimeSeriesModuleState = {
      timeSeries: {
        plotPreset: {
          mapId: mockMapId,
          connectedMap: mockMapId,
          plots: [
            {
              title: 'Plot 1',
              plotId,
            },
          ],
          parameters: [
            {
              id: parameterId,
              plotId,
              plotType: 'line',
              propertyName: temperatureParameter.propertyName,
              unit: temperatureParameter.unit,
              serviceId: service.id,
            },
          ],
        },
        services: [service],
        timeseriesSelect: {
          filters: { searchFilter: '' },
        },
      },
    };
    const mockState = {
      reducer: {
        timeSeries: reducer,
        ui: uiReducer,
        webmap: webmapReducer,
      },
      preloadedState: { ...timeSeriesStore, ...coreStore },
    };

    const store = createToolkitMockStoreWithEggs(mockState);
    render(
      <TimeSeriesThemeStoreProvider store={store}>
        <TimeSeriesSelectConnect selectPlotId={plotId} />
      </TimeSeriesThemeStoreProvider>,
    );

    const temperatureRow = screen.getByRole('listitem', {
      name: temperatureParameter.propertyName,
    });

    const removeTemperatureButton = within(temperatureRow).getByRole('button', {
      name: /remove/i,
    });

    expect(store.getState().timeSeries.plotPreset.parameters).toHaveLength(1);

    fireEvent.click(removeTemperatureButton);

    expect(store.getState().timeSeries.plotPreset.parameters).toHaveLength(0);

    const addTemperatureButton = within(temperatureRow).getByRole('button', {
      name: /add/i,
    });

    fireEvent.click(addTemperatureButton);

    expect(
      within(temperatureRow).getByRole('button', {
        name: /remove/i,
      }),
    ).toBeInTheDocument();
  });

  it('should fetch edr parameters and display them', async () => {
    const timeSeriesStore: TimeSeriesModuleState = {
      timeSeries: {
        plotPreset: {
          mapId: mockMapId,
          plots: [
            {
              title: 'Plot 1',
              plotId,
            },
          ],
          parameters: [],
        },
        services: [
          { id: 'id', type: 'EDR', url: 'serviceUrl', description: 'descript' },
        ],
        timeseriesSelect: {
          filters: { searchFilter: '' },
        },
      },
    };

    const mockState = {
      reducer: { timeSeries: reducer, ui: uiReducer, webmap: webmapReducer },
      preloadedState: { ...timeSeriesStore, ...coreStore },
    };
    const store = createToolkitMockStoreWithEggs(mockState);

    const geopHeight = 'GeopHeight';
    jest.spyOn(axios, 'get').mockResolvedValueOnce({
      data: {
        parameter_names: {
          [geopHeight]: {
            id: geopHeight,
            description: geopHeight,
            type: 'Parameter',
            observedProperty: {
              label: geopHeight,
            },
          },
        },
      },
    });
    render(
      <TimeSeriesThemeStoreProvider store={store}>
        <TimeSeriesSelectConnect selectPlotId={plotId} />
      </TimeSeriesThemeStoreProvider>,
    );

    act(() => {
      store.dispatch(mapActions.registerMap({ mapId: mockMapId }));
      store.dispatch(
        timeSeriesActions.registerTimeSeriesPreset({
          plotPreset: {
            mapId: mockMapId,
            plots: [
              {
                title: 'Plot 1',
                plotId,
              },
            ],
            parameters: [],
          },
          services: [
            {
              id: 'id',
              type: 'EDR',
              url: 'serviceUrlEdrMock',
              description: 'desc',
            },
          ],
          timeseriesSelect: {
            filters: { searchFilter: '' },
          },
        }),
      );
      store.dispatch(
        uiActions.setToggleOpenDialog({
          type: uiTypes.DialogTypes.TimeSeriesSelect,
          setOpen: true,
        }),
      );
    });

    await waitFor(() => {
      const parameterRow = screen.getByRole('listitem', {
        name: geopHeight,
      });
      expect(
        within(parameterRow).getByRole('button', {
          name: /add/i,
        }),
      ).toBeInTheDocument();
    });
  });

  it('should highlight and search', async () => {
    const parameterId = 'parameter1';
    const temperatureParameter = ogcParameters[0];
    const service: TimeSeriesService = {
      id: temperatureParameter.serviceId,
      url: 'serviceUrl',
      type: 'OGC',
      description: 'desc',
    };

    const timeSeriesStore: TimeSeriesModuleState = {
      timeSeries: {
        plotPreset: {
          mapId: mockMapId,
          plots: [
            {
              title: 'Plot 1',
              plotId,
            },
          ],
          parameters: [
            {
              id: parameterId,
              plotId,
              plotType: 'line',
              propertyName: temperatureParameter.propertyName,
              unit: temperatureParameter.unit,
              serviceId: service.id,
            },
          ],
        },
        services: [service],
        timeseriesSelect: {
          filters: { searchFilter: '' },
        },
      },
    };
    const mockState = {
      reducer: {
        timeSeries: reducer,
        ui: uiReducer,
        webmap: webmapReducer,
      },
      preloadedState: { ...timeSeriesStore, ...coreStore },
    };

    const store = createToolkitMockStoreWithEggs(mockState);
    render(
      <TimeSeriesThemeStoreProvider store={store}>
        <TimeSeriesSelectConnect selectPlotId={plotId} />
      </TimeSeriesThemeStoreProvider>,
    );

    const searchInput = screen.getByRole('textbox', {
      name: /Search TS or enter a service URL/i,
    });

    expect(screen.queryByRole('button', { name: /Clear/i })).toBeFalsy();

    await fireEvent.change(searchInput, {
      target: { value: temperatureParameter.propertyName },
    });

    expect(screen.getByRole('button', { name: /Clear/i })).toBeTruthy();

    await waitFor(() => {
      expect(
        store.getState().timeSeries.timeseriesSelect.filters.searchFilter,
      ).toEqual(temperatureParameter.propertyName);
    });

    expect(
      screen
        .getByText(temperatureParameter.propertyName)
        .nodeName.toLowerCase(),
    ).toBe('mark');
  });
});
