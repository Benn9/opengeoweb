/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */

import { rest } from 'msw';
import produce from 'immer';
import {
  EDRDomain,
  EDRInstance,
  EDRParameter,
  EDRParameters,
  EDRPositionResponse,
  EDRRange,
  EDRRangeParameter,
} from '../lib/components/TimeSeries/types';

export const EDR_NORWAY_URL =
  'https://interpol-b.met.no/collections/meps-det-vdiv';
export const EDR_FINLAND_URL = 'https://opendata.fmi.fi/edr/collections/ecmwf';

export const OGC_URL =
  'https://geoservices.knmi.nl/ogcapi/collections/HARM_N25/items';
const date = `20230927`;
export const latestInstanceIdMock = `${date}T120000`;
const oldestInstanceIdMock = `${date}T000000`;
export const latestInstanceMock: EDRInstance = {
  id: latestInstanceIdMock,
  extent: {
    spatial: {
      crs: 'CRS:84',
    },
    temporal: {
      interval: [['2023-09-26T03:00:00Z', '2023-10-06T00:00:00Z']],
    },
  },
  data_queries: {
    position: {
      link: {
        variables: {
          output_formats: ['CoverageJSON'],
        },
      },
    },
  },
};

export const helsinkiLat = 60.192059;
export const helsinkiLon = 24.945831;
export const helsinki = 'Helsinki';

export const parameterNameMock = 'Temperature';
export const parameterUnitMock = '˚C';

export const timestepMock = [
  '2023-09-27T03:00:00.000Z',
  '2023-09-27T04:00:00.000Z',
  '2023-09-27T05:00:00.000Z',
];
export const valueMock = [1020, 1020, 1020];

const edrParameters = {
  [parameterNameMock]: {
    id: parameterNameMock,
    unit: {
      symbol: {
        value: parameterUnitMock,
      },
    },
  } as EDRParameter,
} as EDRParameters;
export const edrPositionResponseMock = {
  domain: {
    axes: {
      t: {
        values: timestepMock,
      },
    },
  } as EDRDomain,
  parameters: edrParameters,
  ranges: {
    [parameterNameMock]: {
      values: valueMock,
    } as EDRRangeParameter,
  } as EDRRange,
} as EDRPositionResponse;

const oldestInstanceMock = produce(latestInstanceMock, (draft) => {
  draft.id = oldestInstanceIdMock;
});
const locations = {
  features: [
    {
      id: 'helsinki',
      geometry: {
        coordinates: [helsinkiLon, helsinkiLat],
      },
      properties: {
        name: helsinki,
      },
    },
  ],
};
export const edrEndpoints = [
  rest.get(`${EDR_FINLAND_URL}/instances`, (_, res, ctx) => {
    return res(
      ctx.status(200),
      ctx.json({
        instances: [latestInstanceMock, oldestInstanceMock],
      }),
    );
  }),
  rest.get(`${EDR_NORWAY_URL}/instances`, (_, res, ctx) => {
    return res(
      ctx.status(200),
      ctx.json({
        instances: [latestInstanceMock, oldestInstanceMock],
      }),
    );
  }),
  rest.get(`${EDR_FINLAND_URL}/locations`, (_, res, ctx) => {
    return res(ctx.status(200), ctx.json(locations));
  }),
  rest.get(`${EDR_NORWAY_URL}/locations`, (_, res, ctx) => {
    return res(ctx.status(200), ctx.json(locations));
  }),
  // these endpoints are only used by norway
  rest.get(
    `${EDR_NORWAY_URL}/instances/${latestInstanceIdMock}`,
    (_, res, ctx) => {
      return res(ctx.status(200), ctx.json({ parameters: edrParameters }));
    },
  ),
  rest.get(
    `${EDR_NORWAY_URL}/instances/${latestInstanceIdMock}/locations/:id`,
    (_, res, ctx) => {
      return res(ctx.status(200), ctx.json(edrPositionResponseMock));
    },
  ),
  // these endpoints are only used by finland
  rest.get(`${EDR_FINLAND_URL}`, (_, res, ctx) => {
    return res(ctx.status(200), ctx.json({ parameter_names: edrParameters }));
  }),
  rest.get(
    `${EDR_FINLAND_URL}/instances/${latestInstanceIdMock}/position`,
    (req, res, ctx) => {
      // if not supported parameter return error status
      if (req.url.searchParams.get('parameter-name') !== parameterNameMock) {
        return res(ctx.status(400));
      }

      return res(ctx.status(200), ctx.json(edrPositionResponseMock));
    },
  ),
];
export const ogcEndpoints = [
  rest.get(OGC_URL, (_, res, ctx) => {
    return res(
      ctx.status(200),
      ctx.json({
        features: [
          {
            properties: {
              timestep: timestepMock,
              result: valueMock,
            },
          },
        ],
      }),
    );
  }),
];
export const unsupportedEndpoints = [
  rest.get('*', (_, res, ctx) => {
    return res(ctx.status(400));
  }),
];
