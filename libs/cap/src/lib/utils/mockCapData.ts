/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */
import { CapPresets, CapWarnings, FeedLastUpdated } from '../components/types';
import { mockGeoJson, mockGeoJson2, mockGeoJson3 } from './mockGeoJson';
import { mockAlert, mockAlert2, mockAlert3 } from './mockAlert';

export const mockCapWarningPresets: CapPresets = {
  pollInterval: 300_000,
  feeds: [
    {
      feedAddress: 'https://alerts.fmi.fi/cap/feed/rss_en-GB.rss',
      feedType: 'rss',
      languagePriority: 'en-US',
    },
    {
      feedAddress: 'https://api.met.no/weatherapi/metalerts/1.1',
      feedType: 'rss',
      languagePriority: 'no',
    },
  ],
};

export const mockCapWarnings: CapWarnings = {
  alert: mockAlert,
  features: mockGeoJson,
};

export const mockCapWarnings2: CapWarnings = {
  alert: mockAlert2,
  features: mockGeoJson2,
};

export const mockCapWarnings3: CapWarnings = {
  alert: mockAlert3,
  features: mockGeoJson3,
};

export const mockLastUpdated: FeedLastUpdated = {
  lastUpdated: 1666096534,
};
