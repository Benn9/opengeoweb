/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */
import React from 'react';
import { ApiProvider } from '@opengeoweb/api';
import { Store } from '@reduxjs/toolkit';
import { Provider } from 'react-redux';
import { withEggs } from '@opengeoweb/shared';
import {
  ThemeProviderProps,
  lightTheme,
  ThemeWrapper,
} from '@opengeoweb/theme';

import { Theme } from '@mui/material';
import { coreModuleConfig } from '@opengeoweb/store';
import { I18nextProvider, useTranslation } from 'react-i18next';
import i18n from 'i18next';
import capModuleConfig from '../../store/config';
import { createApi as createFakeApi } from '../../utils/fakeApi';
import capTrans from '../../../../locales/cap.json';
import { CAP_NAMESPACE, initCapTestI18n } from '../../utils/i18n';

export const CapThemeWrapper: React.FC<ThemeProviderProps> = ({
  theme,
  children,
}: ThemeProviderProps) => <ThemeWrapper theme={theme}>{children}</ThemeWrapper>;

const ThemeWrapperWithModules: React.FC<ThemeProviderProps> = withEggs([
  capModuleConfig,
  ...coreModuleConfig,
])(({ theme, children }: ThemeProviderProps) => (
  <CapThemeWrapper theme={theme}>{children}</CapThemeWrapper>
));

interface CapApiProviderProps {
  children: React.ReactNode;
  createApi?: () => void;
}

interface CapThemeStoreProviderProps {
  children?: React.ReactNode;
  theme?: Theme;
  store: Store;
}

interface CapTranslationsWrapperProps {
  children?: React.ReactNode;
}

/**
 * A Provider component which returns the Api Provider for the Cap library
 * @param children
 * @returns
 */
export const CapApiProvider: React.FC<CapApiProviderProps> = ({
  children,
  createApi = createFakeApi,
}: CapApiProviderProps) => (
  <ApiProvider createApi={createApi}>{children}</ApiProvider>
);

export interface CapStoreProviderProps extends ThemeProviderProps {
  store: Store;
}

export const CapThemeStoreProvider: React.FC<CapThemeStoreProviderProps> = ({
  children,
  theme = lightTheme,
  store,
}: CapThemeStoreProviderProps) => (
  <CapI18nProvider>
    <Provider store={store}>
      <ThemeWrapperWithModules theme={theme}>
        {children}
      </ThemeWrapperWithModules>
    </Provider>
  </CapI18nProvider>
);

export const CapTranslationsWrapper: React.FC<CapTranslationsWrapperProps> = ({
  children,
}) => {
  const { i18n } = useTranslation();
  React.useEffect(() => {
    i18n.addResourceBundle('en', CAP_NAMESPACE, capTrans.en);
    i18n.addResourceBundle('fi', CAP_NAMESPACE, capTrans.fi);
  }, [i18n]);

  return children as React.ReactElement;
};

export const CapI18nProvider: React.FC<CapTranslationsWrapperProps> = ({
  children,
}) => {
  initCapTestI18n();
  return <I18nextProvider i18n={i18n}>{children}</I18nextProvider>;
};
