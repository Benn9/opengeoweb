/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */
import * as React from 'react';
import { Card } from '@mui/material';
import { darkTheme } from '@opengeoweb/theme';
import GanttWrapper from './GanttWrapper';
import { CapThemeWrapper } from '../Providers';
import { tasks } from './mockTasks';

export default { title: 'components/Gantt Chart' };

export const GanttDemo = (): React.ReactElement => (
  <CapThemeWrapper>
    <Card sx={{ height: '100vh', width: '100vw' }}>
      <GanttWrapper tasks={tasks} />
    </Card>
  </CapThemeWrapper>
);

export const GanttDemoDark = (): React.ReactElement => (
  <CapThemeWrapper theme={darkTheme}>
    <Card sx={{ height: '100vh', width: '100vw' }}>
      <GanttWrapper tasks={tasks} />
    </Card>
  </CapThemeWrapper>
);

export const GanttDemoEmptyList = (): React.ReactElement => (
  <CapThemeWrapper>
    <Card sx={{ height: '100vh', width: '100vw' }}>
      <GanttWrapper tasks={[]} />
    </Card>
  </CapThemeWrapper>
);
