/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */
// these npm packages are needed in order to make frappe-gant work:
// frappe-gantt
// @storybook/preset-scss
// css-loader
// sass
// sass-loader
// style-loader

// eslint-disable-next-line @typescript-eslint/ban-ts-comment
// @ts-ignore
import Gantt from 'frappe-gantt';
// eslint-disable-next-line @typescript-eslint/ban-ts-comment
// @ts-ignore
import date_utils from 'frappe-gantt/src/date_utils';
import { Task } from './Task';
import { ViewMode } from './ViewMode';

type Options = {
  view_mode?: ViewMode;
  header_height?: number;
  column_width?: number;
  bar_height?: number;
  language?: string;
  step?: number;
  date_format?: string;
  bar_corner_radius?: number;
  arrow_curve?: number;
  padding?: number;
  popup_trigger?: string;
  custom_popup_html?: (task: Task) => string;
  on_click?: (task: Task) => void;
  on_view_change?: (mode: ViewMode) => void;
  on_progress_change?: (task: Task, progress: number) => void;
  on_date_change?: (task: Task, start: Date, end: Date) => void;
  view_modes?: string[];
};

export default class CustomGantt extends Gantt {
  private options: Options = {
    header_height: 50,
    column_width: 30,
    step: 24,
    bar_height: 20,
    bar_corner_radius: 3,
    arrow_curve: 5,
    padding: 18,
    view_mode: ViewMode.Day,
    date_format: 'YYYY-MM-DD',
    popup_trigger: 'click',
    custom_popup_html: null!,
    language: 'en',
  };
  private gantt_start: Date;
  private gantt_end: Date;
  private tasks: Task[] = [];

  constructor(wrapper: SVGSVGElement, tasks: Task[], options: Options) {
    super(wrapper, tasks, options);
    // eslint-disable-next-line @typescript-eslint/ban-ts-comment
    // @ts-ignore
    this.setup_options(options);
    // eslint-disable-next-line @typescript-eslint/ban-ts-comment
    // @ts-ignore
    this.setup_tasks(tasks);
  }

  public update_view_scale(view_mode: ViewMode): void {
    this.options.view_mode = view_mode;

    if (view_mode === ViewMode.Hour) {
      this.options.step = 24 / 24;
      this.options.column_width = 38;
    } else if (view_mode === ViewMode.Day) {
      this.options.step = 24;
      this.options.column_width = 38;
    } else if (view_mode === ViewMode.HalfDay) {
      this.options.step = 24 / 2;
      this.options.column_width = 38;
    } else if (view_mode === ViewMode.QuarterDay) {
      this.options.step = 24 / 4;
      this.options.column_width = 38;
    } else if (view_mode === ViewMode.Week) {
      this.options.step = 24 * 7;
      this.options.column_width = 140;
    } else if (view_mode === ViewMode.Month) {
      this.options.step = 24 * 30;
      this.options.column_width = 120;
    } else if (view_mode === ViewMode.Year) {
      this.options.step = 24 * 365;
      this.options.column_width = 120;
    }
  }

  setup_gantt_dates(): void {
    this.gantt_start = null!;
    this.gantt_end = null!;

    const checkView = (viewModes: ViewMode | ViewMode[]): boolean =>
      // eslint-disable-next-line @typescript-eslint/ban-ts-comment
      // @ts-ignore
      this.view_is(viewModes);

    for (const task of this.tasks) {
      // set global start and end date
      if (!this.gantt_start || task._start < this.gantt_start) {
        this.gantt_start = task._start;
      }
      if (!this.gantt_end || task._end > this.gantt_end) {
        this.gantt_end = task._end;
      }
    }

    this.gantt_start = date_utils.start_of(this.gantt_start, 'day');
    this.gantt_end = date_utils.start_of(this.gantt_end, 'day');

    // add date padding on both sides
    if (checkView([ViewMode.Hour, ViewMode.QuarterDay, ViewMode.HalfDay])) {
      this.gantt_start = date_utils.add(this.gantt_start, -7, 'day');
      this.gantt_end = date_utils.add(this.gantt_end, 7, 'day');
    } else if (checkView(ViewMode.Month)) {
      this.gantt_start = date_utils.start_of(this.gantt_start, 'year');
      this.gantt_end = date_utils.add(this.gantt_end, 1, 'year');
    } else if (checkView(ViewMode.Year)) {
      this.gantt_start = date_utils.add(this.gantt_start, -2, 'year');
      this.gantt_end = date_utils.add(this.gantt_end, 2, 'year');
    } else {
      this.gantt_start = date_utils.add(this.gantt_start, -1, 'month');
      this.gantt_end = date_utils.add(this.gantt_end, 1, 'month');
    }
  }

  get_date_info(
    date: Date,
    newLastDate: Date,
    i: number,
  ): {
    upper_text: string;
    lower_text: string;
    upper_x: number;
    upper_y: number;
    lower_x: number;
    lower_y: number;
  } {
    const lastDate = !newLastDate
      ? date_utils.add(date, 1, 'year')
      : newLastDate;

    const dateText = {
      Hour_lower: date_utils.format(date, 'HH', this.options.language),
      'Quarter Day_lower': date_utils.format(date, 'HH', this.options.language),
      'Half Day_lower': date_utils.format(date, 'HH', this.options.language),
      Day_lower:
        date.getDate() !== lastDate.getDate()
          ? date_utils.format(date, 'D', this.options.language)
          : '',
      Week_lower:
        date.getMonth() !== lastDate.getMonth()
          ? date_utils.format(date, 'D MMM', this.options.language)
          : date_utils.format(date, 'D', this.options.language),
      Month_lower: date_utils.format(date, 'MMMM', this.options.language),
      Year_lower: date_utils.format(date, 'YYYY', this.options.language),
      'Quarter Day_upper':
        date.getDate() !== lastDate.getDate()
          ? date_utils.format(date, 'D MMM', this.options.language)
          : '',

      Hour_upper:
        date.getDate() !== lastDate.getDate()
          ? date_utils.format(date, 'D MMM', this.options.language)
          : '',
      'Half Day_upper':
        // eslint-disable-next-line no-nested-ternary
        date.getDate() !== lastDate.getDate()
          ? date.getMonth() !== lastDate.getMonth()
            ? date_utils.format(date, 'D MMM', this.options.language)
            : date_utils.format(date, 'D', this.options.language)
          : '',
      Day_upper:
        date.getMonth() !== lastDate.getMonth()
          ? date_utils.format(date, 'MMMM', this.options.language)
          : '',
      Week_upper:
        date.getMonth() !== lastDate.getMonth()
          ? date_utils.format(date, 'MMMM', this.options.language)
          : '',
      Month_upper:
        date.getFullYear() !== lastDate.getFullYear()
          ? date_utils.format(date, 'YYYY', this.options.language)
          : '',
      Year_upper:
        date.getFullYear() !== lastDate.getFullYear()
          ? date_utils.format(date, 'YYYY', this.options.language)
          : '',
    };
    const basePosition = {
      x: i * this.options.column_width!,
      lower_y: this.options.header_height,
      upper_y: this.options.header_height! - 25,
    };
    const xPosition = {
      Hour_lower: 0,
      Hour_upper: (this.options.column_width! * 24) / 2,
      'Quarter Day_lower': 0,
      'Quarter Day_upper': (this.options.column_width! * 4) / 2,
      'Half Day_lower': 0,
      'Half Day_upper': (this.options.column_width! * 2) / 2,
      Day_lower: this.options.column_width! / 2,
      Day_upper: (this.options.column_width! * 30) / 2,
      Week_lower: 0,
      Week_upper: (this.options.column_width! * 4) / 2,
      Month_lower: this.options.column_width! / 2,
      Month_upper: (this.options.column_width! * 12) / 2,
      Year_lower: this.options.column_width! / 2,
      Year_upper: (this.options.column_width! * 30) / 2,
    };
    return {
      upper_text: dateText[`${this.options.view_mode!}_upper`],
      lower_text: dateText[`${this.options.view_mode!}_lower`],
      upper_x: basePosition.x + xPosition[`${this.options.view_mode!}_upper`],
      upper_y: basePosition.upper_y,
      lower_x: basePosition.x + xPosition[`${this.options.view_mode!}_lower`],
      lower_y: basePosition.lower_y!,
    };
  }
}
