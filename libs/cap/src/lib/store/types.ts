/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */

import { EntityState } from '@reduxjs/toolkit';
import { CoreAppStore } from '@opengeoweb/store';
import { Language } from '../components/types';

export interface CapAlert {
  feedAddress: string;
  identifier: string;
  severity: string;
  certainty: string;
  onset: string;
  expires: string;
  languages: Language[];
}

export type CapAlertState = EntityState<CapAlert>;

export interface CapFeed {
  id: string;
  lastUpdated: number;
  alertIds: string[];
}

export type CapFeedState = EntityState<CapFeed>;

export interface CapStore {
  feeds: CapFeedState;
  alerts: CapAlertState;
}

export interface CapModule {
  capStore?: CapStore;
}

export interface AddAllFeedAlertsPayload {
  feedId: string;
  alertUrls: string[];
}

export interface AddManyAlertsToFeedPayload {
  feedId: string;
  alertIds: string[];
}

export interface FeedLastUpdatedPayload {
  id: string;
  changes: {
    lastUpdated: number;
  };
}

export interface AppStore extends CoreAppStore, CapModule {}
