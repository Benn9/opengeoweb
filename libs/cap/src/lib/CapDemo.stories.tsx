/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { Box, Card, Grid } from '@mui/material';
import { ApiProvider } from '@opengeoweb/api';
import { LayerManagerConnect } from '@opengeoweb/core';
import { LayerSelectConnect } from '@opengeoweb/layer-select';
import { CapApiProvider, CapThemeStoreProvider } from './components/Providers';
import { createApi } from './utils/api';
import { mockCapWarningPresets } from './utils/mockCapData';
import { MapViewCapConnect } from './components/MapViewCap/MapViewCapConnect';
import { WarningListCapConnect } from './components/WarningListCap/WarningListCapConnect';
import { store } from './store';

export default { title: 'components/Demo' };

const DemoComponent: React.FC = () => {
  const bbox = {
    left: -2324980.5498391856,
    bottom: 5890854.775012179,
    right: 6393377.702660825,
    top: 11652109.058827976,
  };

  const srs = 'EPSG:3857';
  const mapId = 'CapDemoMap';

  return (
    <Grid container sx={{ width: '100%', height: '100%' }}>
      <Grid container item xs={6}>
        <Card
          sx={{
            height: '100vh',
            width: '100%',
            padding: '20px',
          }}
        >
          <LayerManagerConnect />
          <LayerSelectConnect />
          <MapViewCapConnect
            id={mapId}
            bbox={bbox}
            srs={srs}
            capWarningPresets={mockCapWarningPresets}
          />
        </Card>
      </Grid>
      <Grid container item xs={6}>
        <Box sx={{ width: '100%', height: '100%' }}>
          <WarningListCapConnect capWarningPresets={mockCapWarningPresets} />
        </Box>
      </Grid>
    </Grid>
  );
};

export const CapDemoFakeApi: React.FC = () => (
  <CapApiProvider>
    <CapThemeStoreProvider store={store()}>
      <DemoComponent />
    </CapThemeStoreProvider>
  </CapApiProvider>
);

const baseURL = 'https://cap.opengeoweb.com';

export const CapDemoRealApi: React.FC = () => (
  <ApiProvider createApi={createApi} config={{ baseURL }}>
    <CapThemeStoreProvider store={store()}>
      <DemoComponent />
    </CapThemeStoreProvider>
  </ApiProvider>
);
