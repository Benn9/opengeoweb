/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */

import { ThemeTypes } from '@opengeoweb/theme';

export interface ConfigType {
  // auth
  GW_AUTH_LOGIN_URL?: string;
  GW_AUTH_LOGOUT_URL?: string;
  GW_AUTH_TOKEN_URL?: string;
  GW_AUTH_CLIENT_ID?: string;
  // app
  GW_INFRA_BASE_URL?: string;
  GW_APP_URL?: string;
  GW_INITIAL_PRESETS_FILENAME?: string;
  GW_CAP_CONFIGURATION_FILENAME?: string;
  GW_TIMESERIES_CONFIGURATION_FILENAME?: string;
  GW_DEFAULT_THEME?: ThemeTypes;
  // backend urls
  GW_SW_BASE_URL?: string;
  GW_SIGMET_BASE_URL?: string;
  GW_AIRMET_BASE_URL?: string;
  GW_TAF_BASE_URL?: string;
  GW_PRESET_BACKEND_URL?: string;
  GW_CAP_BASE_URL?: string;
  GW_DRAWINGS_BASE_URL?: string;
  // app features
  GW_FEATURE_APP_TITLE?: string;
  GW_FEATURE_FORCE_AUTHENTICATION?: boolean;
  GW_FEATURE_MODULE_SPACE_WEATHER?: boolean;
  GW_FEATURE_MODULE_SIGMET_CONFIGURATION?: string;
  GW_FEATURE_MODULE_AIRMET_CONFIGURATION?: string;
  GW_FEATURE_MENU_FEEDBACK?: boolean;
  GW_FEATURE_MENU_INFO?: boolean;
  GW_FEATURE_MENU_USER_DOCUMENTATION_URL?: string;
  GW_FEATURE_MENU_VERSION?: boolean;
  GW_FEATURE_MENU_FE_VERSION?: boolean;
  GW_DISPLAY_SEARCH_ON_MAP?: boolean;
  // i18n
  GW_LANGUAGE?: string;
}
