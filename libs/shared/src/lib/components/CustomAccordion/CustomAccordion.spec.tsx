/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { Box } from '@mui/material';
import { render, screen } from '@testing-library/react';
import { CustomAccordion } from './CustomAccordion';
import { SharedThemeProvider } from '../Providers';

const childProps = {
  'data-testid': 'testDetails',
};

describe('components/CustomAccordion', () => {
  it('should render successfully', () => {
    const { baseElement } = render(
      <SharedThemeProvider>
        <CustomAccordion>Content</CustomAccordion>
      </SharedThemeProvider>,
    );
    expect(baseElement).toBeTruthy();
  });

  it('should render children', () => {
    const child = <Box {...childProps}>Content</Box>;
    render(
      <SharedThemeProvider>
        <CustomAccordion>{child}</CustomAccordion>
      </SharedThemeProvider>,
    );
    expect(screen.getByTestId('testDetails')).toBeTruthy();
  });

  it('should render a title as string', () => {
    const props = {
      title: 'test',
    };

    const child = <Box {...childProps}>Content</Box>;
    render(
      <SharedThemeProvider>
        <CustomAccordion {...props}>{child}</CustomAccordion>
      </SharedThemeProvider>,
    );
    expect(screen.getByText(props.title)).toBeTruthy();
  });

  it('should render a title as compoonent', () => {
    const props = {
      title: <div data-testid="test-header">component header</div>,
    };

    const child = <Box {...childProps}>Content</Box>;
    render(
      <SharedThemeProvider>
        <CustomAccordion {...props}>{child}</CustomAccordion>
      </SharedThemeProvider>,
    );
    expect(screen.getByTestId('test-header')).toBeTruthy();
  });
});
