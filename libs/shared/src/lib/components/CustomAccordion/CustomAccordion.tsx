/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */
import * as React from 'react';
import {
  Accordion,
  AccordionDetails,
  AccordionProps,
  AccordionSummary,
  SxProps,
  Theme,
} from '@mui/material';
import { ChevronDown } from '@opengeoweb/theme';

export interface CustomAccordionProps extends Omit<AccordionProps, 'title'> {
  title?: string | React.ReactNode;
  expandedElevation?: number;
  collapsedElevation?: number;
  transitionDuration?: number;
  detailsSx?: SxProps<Theme>;
}

export const CustomAccordion: React.FC<CustomAccordionProps> = ({
  title,
  defaultExpanded = true,
  disableGutters = true,
  expandedElevation = 3,
  collapsedElevation = 0,
  transitionDuration = 150,
  detailsSx,
  children,
  ...props
}) => {
  const [expanded, setExpanded] = React.useState(defaultExpanded);

  return (
    <Accordion
      {...props}
      defaultExpanded={defaultExpanded}
      elevation={expanded ? expandedElevation : collapsedElevation}
      onChange={(_event, expanded): void => setExpanded(expanded)}
      disableGutters={disableGutters}
      TransitionProps={{
        timeout: transitionDuration,
      }}
    >
      <AccordionSummary expandIcon={<ChevronDown />}>{title}</AccordionSummary>
      <AccordionDetails sx={detailsSx}>{children}</AccordionDetails>
    </Accordion>
  );
};
