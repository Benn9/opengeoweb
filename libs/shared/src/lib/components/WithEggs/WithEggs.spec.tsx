/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */
import React from 'react';
import { Egg } from '@redux-eggs/core';
import { Provider } from 'react-redux';
import { render, screen } from '@testing-library/react';
import { withEggs } from './WithEggs';
import { createMockStoreWithEggs } from '../../utils';

describe('components/WithEggs/withEggs', () => {
  it('should render component with store', async () => {
    const exampleConfig: Egg = {
      id: 'example-module',
      reducersMap: {},
    };
    const store = createMockStoreWithEggs({});
    const TestComponent = withEggs([exampleConfig])(() => (
      <div data-testid="test-component" />
    ));
    render(
      <Provider store={store}>
        <TestComponent />
      </Provider>,
    );

    expect(screen.getByTestId('test-component')).toBeTruthy();
    expect(store.addEggs).toHaveBeenCalledWith([exampleConfig]);
  });
});
