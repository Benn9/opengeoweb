/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */
import React from 'react';
import { ControlPosition } from 'react-draggable';
import { Size } from '../DraggableResizable/types';

interface PositionAndSizeProps {
  currentX: number;
  newWidth: number;
  maxWidth: number;
  position: ControlPosition;
  startSize: Size;
}

interface PositionAndSizePropsRight extends PositionAndSizeProps {
  currentWidth: number;
}

export const getRightPositionAndSize = ({
  currentX,
  currentWidth,
  newWidth,
  maxWidth,
  position,
  startSize,
}: PositionAndSizePropsRight): { size: Size; position: ControlPosition } => {
  const newSize = {
    ...startSize,
    width: newWidth,
  };
  // if new size is bigger than the window
  if (newWidth >= maxWidth) {
    return {
      size: {
        ...newSize,
        width: maxWidth,
      },
      position: {
        ...position,
        x: position.x + (maxWidth - (currentX + currentWidth)),
      },
    };
  }
  // if new size overlaps left side of screen
  const newStartX = currentX - (newWidth - currentWidth);
  if (newStartX <= 0) {
    return {
      size: newSize,
      position: {
        ...position,
        x: position.x - newStartX,
      },
    };
  }
  // there is room to grow
  return {
    size: newSize,
    position,
  };
};

export const getLeftPositionAndSize = ({
  currentX,
  newWidth,
  maxWidth,
  position,
  startSize,
}: PositionAndSizeProps): { size: Size; position: ControlPosition } => {
  const newSize = {
    ...startSize,
    width: newWidth,
  };
  // if new size is bigger than the window
  if (newWidth >= maxWidth) {
    return {
      size: {
        ...newSize,
        width: maxWidth,
      },
      position: {
        ...position,
        x: position.x - currentX,
      },
    };
  }

  // if new size overlaps right side of screen
  const newEndX = currentX + newWidth;
  if (newEndX >= maxWidth) {
    return {
      size: newSize,
      position: {
        ...position,
        x: position.x + (maxWidth - newEndX),
      },
    };
  }
  // there is room to grow
  return {
    size: newSize,
    position,
  };
};

interface UseHandleSizeFromOutsideComponentProps {
  nodeRef: React.RefObject<HTMLDivElement>;
  startSize: Size;
  bounds?: 'parent' | string;
  isRightAligned: boolean;
  size: Size;
  setSize: (size: Size) => void;
  position: ControlPosition;
  setPosition: (position: ControlPosition) => void;
}

export const useHandleSizeFromOutsideComponent = ({
  nodeRef,
  startSize,
  bounds,
  isRightAligned,
  size,
  setSize,
  position,
  setPosition,
}: UseHandleSizeFromOutsideComponentProps): void => {
  React.useEffect(() => {
    // update size when startSize has changed outside the component
    if (startSize !== size) {
      const node = nodeRef.current?.firstChild as HTMLDivElement;
      const boundsNode = (
        bounds === 'parent'
          ? nodeRef.current?.parentNode
          : document.querySelector(bounds as keyof HTMLElementTagNameMap)
      ) as HTMLDivElement;

      const { width: maxWidth } = boundsNode.getBoundingClientRect();
      const newWidth = Number(startSize.width);
      const { x: currentX, width: currentWidth } =
        node?.getBoundingClientRect() ?? {};

      const { position: newPosition, size: newSize } = isRightAligned
        ? getRightPositionAndSize({
            currentX,
            currentWidth,
            newWidth,
            maxWidth,
            position,
            startSize,
          })
        : getLeftPositionAndSize({
            currentX: currentX - boundsNode.getBoundingClientRect().x,
            newWidth,
            maxWidth,
            position,
            startSize,
          });

      setSize(newSize);
      setPosition(newPosition);
    }

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [startSize.width, startSize.height]);
};
