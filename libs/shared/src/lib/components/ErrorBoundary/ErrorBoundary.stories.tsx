/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */
import * as React from 'react';
import { Box, Button, Typography } from '@mui/material';
import { ThemeWrapperOldTheme, ThemeWrapper } from '@opengeoweb/theme';
import { ErrorBoundary } from './ErrorBoundary';

export default {
  title: 'Components/ErrorBoundary/ErrorBoundary',
};

const ErrorTest: React.FC<Record<string, unknown>> = ({ title }) => {
  const [hasError, triggerError] = React.useState(false);

  if (hasError) {
    throw new Error(`Triggered error in ${title}`);
  }
  return (
    <Button
      variant="outlined"
      color="secondary"
      sx={{ margin: 2 }}
      onClick={(): void => triggerError(true)}
    >
      {`Click to trigger error in ${title}`}
    </Button>
  );
};

export const ErrorBoundaryDemo = (): React.ReactElement => (
  <ThemeWrapperOldTheme>
    <Typography>
      These two buttons are each inside of their own error boundary. So if one
      crashes, the other is not affected.
    </Typography>
    <br />
    <ErrorBoundary
      onError={(error: Error, errorInfo: React.ErrorInfo): void =>
        // eslint-disable-next-line no-console
        console.log('send error to service:', error, errorInfo)
      }
    >
      <ErrorTest title="component 1" />
    </ErrorBoundary>
    <br />
    <br />
    <ErrorBoundary>
      <ErrorTest title="component 2" />
    </ErrorBoundary>
  </ThemeWrapperOldTheme>
);

ErrorBoundaryDemo.storyName = 'Error Boundary';

export const ErrorBoundaryInsideBox = (): React.ReactElement => (
  <ThemeWrapper>
    <Box
      sx={{
        height: '200px',
        width: '500px',
        border: '2px solid black',
        color: 'white',
      }}
    >
      <ErrorBoundary>
        <ErrorTest title="box example" />
      </ErrorBoundary>
    </Box>
  </ThemeWrapper>
);
