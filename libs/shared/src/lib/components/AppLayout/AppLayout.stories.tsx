/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */
import React from 'react';
import { darkTheme, ThemeWrapper } from '@opengeoweb/theme';
import { MenuItem, Typography } from '@mui/material';
import AppLayout from './AppLayout';
import { AppHeader } from '../AppHeader';
import { UserMenu, UserMenuItemTheme, useUserMenu } from '../UserMenu';

export default { title: 'components/AppLayout' };

const DemoUserMenu: React.FC = () => {
  const { isOpen, onClose, onToggle } = useUserMenu();

  return (
    <UserMenu
      isOpen={isOpen}
      onClose={onClose}
      onToggle={onToggle}
      initials="JD"
    >
      <MenuItem selected onClick={onClose}>
        Profile
      </MenuItem>
      <UserMenuItemTheme />
      <MenuItem onClick={onClose}>Logout</MenuItem>
    </UserMenu>
  );
};

export const ThemeLight = (): React.ReactElement => (
  <ThemeWrapper>
    <AppLayout
      header={<AppHeader title="Header" userMenu={<DemoUserMenu />} />}
    >
      <Typography>Main</Typography>
    </AppLayout>
  </ThemeWrapper>
);

// theme switch not representative for application
export const ThemeDark = (): React.ReactElement => (
  <ThemeWrapper theme={darkTheme}>
    <AppLayout
      header={<AppHeader title="Header" userMenu={<DemoUserMenu />} />}
    >
      <Typography>Main</Typography>
    </AppLayout>
  </ThemeWrapper>
);
