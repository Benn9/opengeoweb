/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */
import React from 'react';
import { render, screen } from '@testing-library/react';
import { ThemeWrapper } from '@opengeoweb/theme';
import ShortcutSegment from './ShortcutSegment';

describe('components/UserMenu/CheatSheet/ShortcutSegment', () => {
  it('should work with default props', async () => {
    const testvalue = 'testing';
    render(
      <ThemeWrapper>
        <ShortcutSegment>{testvalue}</ShortcutSegment>
      </ThemeWrapper>,
    );

    expect(screen.getByTestId('segment-text')).toBeTruthy();
    expect(screen.queryByTestId('segment-icon')).toBeFalsy();
    expect(screen.queryByTestId('segment-divider')).toBeFalsy();
    expect(screen.queryByTestId('segment-connect')).toBeFalsy();
    expect(await screen.findByText(testvalue)).toBeTruthy();
  });

  it('should render segment icon', async () => {
    const testvalue = 'testing';
    render(
      <ThemeWrapper>
        <ShortcutSegment type="icon">{testvalue}</ShortcutSegment>
      </ThemeWrapper>,
    );

    expect(screen.queryByTestId('segment-text')).toBeFalsy();
    expect(screen.getByTestId('segment-icon')).toBeTruthy();
    expect(screen.queryByTestId('segment-divider')).toBeFalsy();
    expect(screen.queryByTestId('segment-connect')).toBeFalsy();
    expect(await screen.findByText(testvalue)).toBeTruthy();
  });

  it('should render segment divider', async () => {
    const testvalue = 'testing';
    render(
      <ThemeWrapper>
        <ShortcutSegment type="divider">{testvalue}</ShortcutSegment>
      </ThemeWrapper>,
    );

    expect(screen.queryByTestId('segment-text')).toBeFalsy();
    expect(screen.queryByTestId('segment-icon')).toBeFalsy();
    expect(screen.getByTestId('segment-divider')).toBeTruthy();
    expect(screen.queryByTestId('segment-connect')).toBeFalsy();
    expect(await screen.findByText(testvalue)).toBeTruthy();
  });

  it('should render segment connect', async () => {
    const testvalue = 'testing';
    render(
      <ThemeWrapper>
        <ShortcutSegment type="connect">{testvalue}</ShortcutSegment>
      </ThemeWrapper>,
    );

    expect(screen.queryByTestId('segment-text')).toBeFalsy();
    expect(screen.queryByTestId('segment-icon')).toBeFalsy();
    expect(screen.queryByTestId('segment-divider')).toBeFalsy();
    expect(screen.getByTestId('segment-connect')).toBeTruthy();
    expect(await screen.findByText(testvalue)).toBeTruthy();
  });
});
