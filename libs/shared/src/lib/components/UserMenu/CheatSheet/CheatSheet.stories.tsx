/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */
import React from 'react';
import { darkTheme, ThemeWrapper } from '@opengeoweb/theme';
import { Paper } from '@mui/material';
import CheatSheet from './CheatSheet';

export default {
  title: 'components/UserMenu/CheatSheet',
};

export const ThemeLight = (): React.ReactElement => (
  <ThemeWrapper>
    <Paper>
      <CheatSheet />
    </Paper>
  </ThemeWrapper>
);

ThemeLight.parameters = {
  zeplinLink: [
    {
      name: 'Light theme',
      link: 'https://app.zeplin.io/project/5ecf84a3c6ae1047a368f393/screen/6138a379a11c80b92f4c4694',
    },
  ],
};

export const ThemeDark = (): React.ReactElement => (
  <ThemeWrapper theme={darkTheme}>
    <Paper>
      <CheatSheet />
    </Paper>
  </ThemeWrapper>
);

ThemeDark.parameters = {
  zeplinLink: [
    {
      name: 'Dark theme',
      link: 'https://app.zeplin.io/project/5ecf84a3c6ae1047a368f393/screen/6138a37a453de5b9c8619572',
    },
  ],
};

export const ThemeLightSnapShot = (): React.ReactElement => (
  <div style={{ width: 800 }}>
    <ThemeWrapper>
      <CheatSheet />
    </ThemeWrapper>
  </div>
);
ThemeLightSnapShot.storyName = 'CheatSheet light (takeSnapshot)';

export const ThemeDarkSnapShot = (): React.ReactElement => (
  <div style={{ width: 800 }}>
    <ThemeWrapper theme={darkTheme}>
      <CheatSheet />
    </ThemeWrapper>
  </div>
);
ThemeDarkSnapShot.storyName = 'CheatSheet dark (takeSnapshot)';
