/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */

import { isValid, format } from 'date-fns';
import {
  dateToString,
  isBetween,
  isoStringToDate,
  setUTCDate,
  stringToDate,
  createDate,
  add,
  sub,
  utc,
  unix,
  set,
} from './dateUtils';

describe('dateUtils', () => {
  describe('utc', () => {
    it('should return a valid Date when called without input', () => {
      const res = utc();
      expect(isValid(res)).toEqual(true);
    });
    const testCases = [
      {
        dateStr: '1910-11-06T02:19:00.250',
        expected: new Date('1910-11-06T02:19:00.250Z'),
      },
      {
        dateStr: '2010-11-06T02:19:00',
        expected: new Date('2010-11-06T02:19:00Z'),
      },
      {
        dateStr: '2022-03-01T16:23:19Z',
        expected: new Date('2022-03-01T16:23:19Z'),
      },
      {
        dateStr: '2023-03-01T16:23:19+01:00',
        expected: new Date('2023-03-01T15:23:19Z'),
      },
      {
        dateStr: '2023-03-01T16:23:19-01:00',
        expected: new Date('2023-03-01T17:23:19Z'),
      },
      {
        dateStr: '2023-03-01T16:23:19-01:00',
        expected: new Date('2023-03-01T17:23:19Z'),
      },
    ];
    testCases.forEach((test) => {
      it(`should set timezone to UTC, ${test.dateStr} -> ${test.expected}`, () => {
        const res = utc(test.dateStr);
        expect(res).toEqual(test.expected);
      });
    });
  });

  describe('check native date behaviour', () => {
    it('should parse both dates the same', () => {
      const utc = new Date('2022-10-10T15:30:00.000+00:00');
      const utcPlusOne = new Date('2022-10-10T17:30:00.000+02:00');
      expect(utc).toEqual(utcPlusOne);
    });
  });
  describe('isoStringToDate', () => {
    it('should parse an ISO date with timezone', () => {
      const date = new Date('2022-10-10T15:30:00.000+02:00');
      const isoString = date.toISOString();

      expect(isoStringToDate(isoString, false)).toEqual(date);
    });

    it('should parse an ISO date string without a timezone as a utc date', () => {
      const dateString = '2022-11-10T10:15';

      expect(isoStringToDate(dateString).toISOString()).toEqual(
        `2022-11-10T10:15:00.000Z`,
      );
    });
  });

  describe('stringToDate', () => {
    it('should parse a local string to a local date', () => {
      const date = new Date(2022, 5, 10, 15, 30);
      const stringFormat = 'yyyy dd MMMM HH:mm';
      const dateString = format(date, stringFormat);

      expect(stringToDate(dateString, stringFormat, false)).toEqual(date);
    });

    it('should parse an arbitrary date string without a timezone as a utc date', () => {
      const stringFormat = 'yyyy.MM.dd, HH:mm';
      const dateString = '2022.11.10, 10:15';

      expect(stringToDate(dateString, stringFormat).toISOString()).toEqual(
        `2022-11-10T10:15:00.000Z`,
      );
    });

    it('should parse an arbitrary date string with a timezone as a utc date', () => {
      const stringFormat = 'yyyy.MM.dd, HH:mm xxx';
      const dateString = '2022.11.10, 11:15 +01:00';

      expect(
        stringToDate(dateString, stringFormat, false).toISOString(),
      ).toEqual(`2022-11-10T10:15:00.000Z`);
    });
  });

  describe('setUTCDate', () => {
    it('should set a date to correctly reference UTC', () => {
      const date = new Date('2022-10-10T15:30:00');
      const utcDate = setUTCDate(date);
      expect(utcDate.getUTCHours()).toEqual(15);
    });
  });

  describe('dateToString', () => {
    it('should format a date in utc', () => {
      const date = new Date('2022-11-27T15:30+00:00');
      const dateString = dateToString(date, "dd MMM yyyy, HH:mm 'UTC'");

      expect(dateString).toEqual('27 Nov 2022, 15:30 UTC');
    });

    it('should format a date in local time', () => {
      const date = new Date('2022-11-27T15:30+00:00');
      const strFormat = "dd MMM yyyy, HH:mm 'UTC'";

      expect(dateToString(date, strFormat, false)).toEqual(
        format(date, strFormat),
      );
    });

    it('should format a date in standard format when format is missing', () => {
      const date = new Date('2022-11-27T15:30+00:00');

      expect(dateToString(date)).toEqual('2022-11-27T15:30:00Z');
    });
  });

  describe('isBetween', () => {
    const startDate = new Date('2022-11-09T00:00Z');
    const endDate = new Date('2022-11-11T00:00Z');

    it('should raise an error', async () => {
      const testDate = new Date('2022-11-10T00:00Z');

      expect(() => {
        isBetween(testDate, startDate, endDate, '())');
      }).toThrow(
        new Error('Inclusivity parameter must be one of (), [], (], [)'),
      );
    });

    it('should be between', () => {
      const testDate = new Date('2022-11-10T00:00Z');

      expect(isBetween(testDate, startDate, endDate, '()')).toBeTruthy();
    });

    it('should not be between', () => {
      const testDate = new Date('2022-11-08T00:00Z');

      expect(isBetween(testDate, startDate, endDate, '()')).toBeFalsy();
    });

    it('should be between when inclusive', () => {
      const testDate = new Date('2022-11-09T00:00Z');

      expect(isBetween(testDate, startDate, endDate, '[)')).toBeTruthy();
    });

    it('should not be between when exclusive', () => {
      const testDate = new Date('2022-11-09T00:00Z');

      expect(isBetween(testDate, startDate, endDate, '()')).toBeFalsy();
    });
  });

  describe('add', () => {
    it('adds the duration to det given date', () => {
      const result = add(new Date(2014, 8 /* Sep */, 1, 10, 19, 50), {
        years: 2,
        months: 9,
        weeks: 1,
        days: 7,
        hours: 5,
        minutes: 9,
        seconds: 30,
      });
      expect(result).toEqual(new Date(2017, 5 /* June */, 15, 15, 29, 20));
    });

    it('supports an undefined value in the duration object', () => {
      const result = add(new Date(2014, 8 /* Sep */, 1, 10, 19, 50), {
        years: undefined,
        months: 9,
        weeks: 1,
        days: 7,
        hours: 5,
        minutes: 9,
        seconds: 30,
      });
      expect(result).toEqual(new Date(2015, 5 /* June */, 15, 15, 29, 20));
    });
    it('returns same date object when passed empty duration values', () => {
      const result = add(new Date(2014, 8 /* Sep */, 1, 10).getTime(), {
        years: undefined,
        months: undefined,
        weeks: undefined,
        days: undefined,
        hours: undefined,
        minutes: undefined,
        seconds: undefined,
      });
      expect(result).toEqual(new Date(2014, 8 /* Sep */, 1, 10));
    });
    it('accepts a timestamp', () => {
      const result = add(new Date(2014, 8 /* Sep */, 1, 10).getTime(), {
        hours: 4,
      });
      expect(result).toEqual(new Date(2014, 8 /* Sep */, 1, 14));
    });

    it('does not mutate the original date', () => {
      const date = new Date(2014, 8 /* Sep */, 1, 10);
      add(date, { hours: 4 });
      expect(date).toEqual(new Date(2014, 8 /* Sep */, 1, 10));
    });

    it('works well if the desired month has fewer days and the provided date is in the last day of a month', () => {
      const date = new Date(2014, 11 /* Dec */, 31);
      const result = add(date, { months: 9 });
      expect(result).toEqual(new Date(2015, 8 /* Sep */, 30));
    });

    it('handles dates before 100 AD', () => {
      const initialDate = new Date(0);
      initialDate.setFullYear(-1, 10 /* Nov */, 30);
      initialDate.setHours(0, 0, 0, 0);
      const expectedResult = new Date(0);
      expectedResult.setFullYear(0, 1 /* Feb */, 29);
      expectedResult.setHours(0, 0, 0, 0);
      const result = add(initialDate, { months: 3 });
      expect(result).toEqual(expectedResult);
    });

    it('returns `Invalid Date` if the given date is invalid', () => {
      const result = add(new Date(NaN), { hours: 5 });
      expect(result instanceof Date && isNaN(result.getTime()));
    });
  });
  describe('sub', () => {
    it('subtracts the duration from the given date', () => {
      const result = sub(new Date(2017, 5 /* June */, 15, 15, 29, 20), {
        years: 2,
        months: 9,
        weeks: 1,
        days: 7,
        hours: 5,
        minutes: 9,
        seconds: 30,
      });
      expect(result).toEqual(new Date(2014, 8 /* Sep */, 1, 10, 19, 50));
    });

    it('supports an undefined value in the duration object', () => {
      const result = sub(new Date(2017, 5 /* June */, 15, 15, 29, 20), {
        years: undefined,
        months: 9,
        weeks: 1,
        days: 7,
        hours: 5,
        minutes: 9,
        seconds: 30,
      });
      expect(result).toEqual(new Date(2016, 8 /* Sep */, 1, 10, 19, 50));
    });

    it('returns same date object when passed empty duration values', () => {
      const result = sub(new Date(2014, 8 /* Sep */, 1, 10).getTime(), {
        years: undefined,
        months: undefined,
        weeks: undefined,
        days: undefined,
        hours: undefined,
        minutes: undefined,
        seconds: undefined,
      });
      expect(result).toEqual(new Date(2014, 8 /* Sep */, 1, 10));
    });

    it('returns same date object when passed empty duration', () => {
      const result = sub(new Date(2014, 8 /* Sep */, 1, 10).getTime(), {});
      expect(result).toEqual(new Date(2014, 8 /* Sep */, 1, 10));
    });

    it('accepts a timestamp', () => {
      const result = sub(new Date(2014, 8 /* Sep */, 1, 14).getTime(), {
        hours: 4,
      });
      expect(result).toEqual(new Date(2014, 8 /* Sep */, 1, 10));
    });

    it('does not mutate the original date', () => {
      const date = new Date(2014, 8 /* Sep */, 1, 10);
      sub(date, { hours: 4 });
      expect(date).toEqual(new Date(2014, 8 /* Sep */, 1, 10));
    });

    it('works well if the desired month has fewer days and the provided date is in the last day of a month', () => {
      const date = new Date(2014, 11 /* Dec */, 31);
      const result = sub(date, { months: 3 });
      expect(result).toEqual(new Date(2014, 8 /* Sep */, 30));
    });

    it('handles dates before 100 AD', () => {
      const initialDate = new Date(0);
      initialDate.setFullYear(1, 2 /* Mar */, 31);
      initialDate.setHours(0, 0, 0, 0);
      const expectedResult = new Date(0);
      expectedResult.setFullYear(1, 1 /* Feb */, 28);
      expectedResult.setHours(0, 0, 0, 0);
      const result = sub(initialDate, { months: 1 });
      expect(result).toEqual(expectedResult);
    });

    it('returns `Invalid Date` if the given date is invalid', () => {
      const result = sub(new Date(NaN), { hours: 5 });
      expect(result instanceof Date && isNaN(result.getTime()));
    });
  });

  describe('createDate', () => {
    afterEach(() => {
      jest.clearAllMocks();
      jest.restoreAllMocks();
    });
    it('returns the current date and time if no arguments are provided', () => {
      const date = new Date(`2023-01-01T14:00:00Z`);
      jest.spyOn(global, 'Date').mockReturnValue(date);
      // check that the mock works
      expect(new Date()).toEqual(date);
      // check that createDate returns the current time
      expect(createDate()).toEqual(date);
    });

    it('returns a Date object with the specified year, month, and day if provided', () => {
      const year = 2022;
      const month = 4;
      const day = 1;
      expect(createDate(year, month, day)).toEqual(new Date(year, month, day));
    });

    it('returns a Date object with the date and time represented by the provided ISO-formatted string', () => {
      const isoString = '2022-04-01T12:00:00.000Z';
      expect(createDate(isoString)).toEqual(new Date(isoString));
    });
  });

  describe('unix', () => {
    it('returns the timestamp of the given date', () => {
      const timestamp = 1483228800000;
      const result = unix(new Date(timestamp));
      expect(result).toStrictEqual(Math.floor(timestamp / 1000));
    });
    it('accepts a timestamp (and returns it unchanged)', () => {
      const timestamp = 804643200000;
      const result = unix(timestamp);
      expect(result).toStrictEqual(Math.floor(timestamp / 1000));
    });
    it('returns NaN if the given date is invalid', () => {
      const result = unix(new Date(NaN));
      expect(isNaN(result)).toBeTruthy();
    });
  });

  describe('set', () => {
    it('sets all values', () => {
      const result = set(new Date(2013, 0 /* Jan */), {
        year: 2014,
        month: 8, // Sep
        date: 20,
        hours: 12,
        minutes: 12,
        seconds: 12,
        milliseconds: 12,
      });
      expect(result.toString()).toStrictEqual(
        new Date(2014, 8 /* Sep */, 20, 12, 12, 12, 12).toString(),
      );
    });

    it('sets year', () => {
      const result = set(new Date(2013, 8 /* Sep */), { year: 2014 });
      expect(result).toStrictEqual(new Date(2014, 8 /* Sep */));
    });

    it('sets month', () => {
      const result = set(new Date(2014, 8 /* Sep */), { month: 9 /* Oct */ });

      expect(result).toStrictEqual(new Date(2014, 9 /* Oct */));
    });

    it('sets day of month', () => {
      const result = set(new Date(2014, 8 /* Sep */), { date: 20 });
      expect(result).toStrictEqual(new Date(2014, 8 /* Sep */, 20));
    });

    it('sets hours', () => {
      const result = set(new Date(2014, 8 /* Sep */, 1), { hours: 12 });
      expect(result).toStrictEqual(new Date(2014, 8 /* Sep */, 1, 12));
    });

    it('sets minutes', () => {
      const result = set(new Date(2014, 8 /* Sep */, 1, 1), { minutes: 12 });
      expect(result).toStrictEqual(new Date(2014, 8 /* Sep */, 1, 1, 12));
    });

    it('sets seconds', () => {
      const result = set(new Date(2014, 8 /* Sep */, 1, 1, 1), {
        seconds: 12,
      });

      expect(result).toStrictEqual(new Date(2014, 8 /* Sep */, 1, 1, 1, 12));
    });
  });
});
