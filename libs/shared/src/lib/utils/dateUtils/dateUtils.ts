/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */

import {
  parse,
  parseISO,
  format,
  isEqual,
  isBefore as dateFnsIsBefore,
  isAfter as dateFnsIsAfter,
  differenceInMinutes as dateFnsDifferenceInMinutes,
  differenceInHours as dateFnsDifferenceInHours,
  add as dateFnsAdd,
  sub as dateFnsSub,
  isValid as dateFnsIsValid,
  set as dateFnsSet,
  getUnixTime,
} from 'date-fns';

export interface DateValues {
  year?: number;
  month?: number;
  date?: number;
  hours?: number;
  minutes?: number;
  seconds?: number;
  milliseconds?: number;
}

/**
 * Parsing date strings with geoweb dateUtils:
 *
 * An ISO String *with timezone* can safely be parsed directly to a date:
 *
 * date = new Date('2022-10-10T15:30:00Z')
 * date = new Date('2022-10-10T15:30:00+00:00')
 * date = new Date('2022-10-10T15:30:00+02:00')
 *
 * or using the dateUtils isoStringToDate function *with utc=false*
 *
 * date = isoStringToDate('2022-10-10T15:30:00Z', false)
 *
 * An ISO String *without a timezone* can be parsed with isoStringToDate
 *
 * date = isoStringToDate('2022-10-10T15:30:00')
 *
 * *Do not parse ISO without timezone using `new Date()` unless you correct for the local timezone*
 *
 * To parse an arbitrary date string with timezone use:
 *
 * date = stringToDate('2022.11.10, 11:15 +01:00', 'yyyy.MM.dd, HH:mm xxx', false)
 *
 * To parse an arbitrary date string without timezone use:
 *
 * date = stringToDate('2022.11.10, 11:15', 'yyyy.MM.dd, HH:mm')
 *
 *
 * To safely convert dates to utc strings use:
 *
 * date.toISOString()
 *
 * To get parts of UTC time use
 *
 * date.getUTCHours()
 * date.getUTCMins()
 *
 * To parse an arbitrary date string to a UTC string use:
 *
 * dateToString(new Date('2022-10-10T15:30:00Z', 'HH:ss dd MMM yyyy')
 *
 */

/**
 * Parse an ISO string to a date
 *
 * Expects the following props:
 * @param {string} input - string containing date
 *
 * @returns {Date} Date
 * @example
 * ``` isoStringToDate("2022-5-10T20:32:00+00:00")) ```
 */
export const isoStringToDate = (input: string, utc = true): Date => {
  const parsed = parseISO(input);
  if (utc) {
    return setUTCDate(parsed);
  }
  return parsed;
};

/**
 * Parse a string to a date
 *
 * ***NOTE: String will be parsed in local time if utc is set to false***
 *
 * Expects the following props:
 * @param {string} input - string containing date
 * @param {string} strFormat - format of the string, see: https://date-fns.org/v2.29.3/docs/parse
 * @param {boolean} utc [ default = true ] - boolean indicating whether to parse as UTC
 * @param {Date} referenceDate [ default = new Date() ] - defines values missing from the parsed dateString
 *
 * @returns {Date} Date
 * @example
 * ``` stringToDate("2022 5 10 20:32", 'yyyy M d HH:mm')) ```
 */
export const stringToDate = (
  input: string,
  strFormat: string,
  utc = true,
  referenceDate: Date | number = new Date(),
): Date => {
  const parsed = parse(input, strFormat, referenceDate);
  if (utc) {
    return setUTCDate(parsed);
  }
  return parsed;
};

/**
 * Use this if you initialise a date without a timezone to set it to utc correctly
 * eg.
 *   new Date(2022, 10, 12, 15, 30)
 * should be:
 *   setUTCDate(new Date(2022, 10, 12, 15, 30))
 *
 * Expects the following props:
 * @param {Date} inDate - date to be set to UTC
 *
 * @returns {Date} - Date in UTC
 * @example
 * ``` setUTCDate(new Date(2022, 10, 12, 15, 30)) ```
 */
export const setUTCDate = (inDate: Date): Date => {
  const utcDate = new Date(inDate);
  utcDate.setUTCFullYear(
    inDate.getFullYear(),
    inDate.getMonth(),
    inDate.getDate(),
  );
  utcDate.setUTCHours(
    inDate.getHours(),
    inDate.getMinutes(),
    inDate.getSeconds(),
    inDate.getMilliseconds(),
  );
  return utcDate;
};

/**
 * Format a date to a string
 *
 * Expects the following props:
 * @param {Date} inDate - date to be formatted
 * @param {string} strFormat - [default: "yyyy-MM-dd'T'HH:mm:ss'Z'"] format of string
 * @param {boolean} utc - [default: true] true to return in utc otherwise return in local timezone
 *
 * @returns {string} - formatted date string
 * @example
 * ``` dateToString(new Date(2022, 10, 12, 15, 30), 'dd MMM yyyy') ```
 */
export const dateToString = (
  inDate: Date,
  strFormat = "yyyy-MM-dd'T'HH:mm:ss'Z'",
  utc = true,
): string => {
  if (utc) {
    return format(
      new Date(
        inDate.getUTCFullYear(),
        inDate.getUTCMonth(),
        inDate.getUTCDate(),
        inDate.getUTCHours(),
        inDate.getUTCMinutes(),
        inDate.getUTCSeconds(),
      ),
      strFormat,
    );
  }
  return format(inDate, strFormat);
};

/**
 * Find out if a date is between two other dates.
 *
 * Inclusivity parameter matches that used in momentjs where '[' indicates inclusion and '(' indicates exclusion.
 *
 * Expects the following props:
 * @param {Date} date - date to be tested
 * @param {Date} from - first date
 * @param {Date} to - last date
 * @param {string} inclusivity - [default: '()'] Must be one of : '()', '[]', '(]', '[)'
 *
 * @returns {boolen} - true if date is between, otherwise false
 *
 */
export const isBetween = (
  date: Date,
  from: Date,
  to: Date,
  inclusivity = '()',
): boolean => {
  if (!['()', '[]', '(]', '[)'].includes(inclusivity)) {
    throw new Error('Inclusivity parameter must be one of (), [], (], [)');
  }

  const isBeforeEqual = inclusivity[0] === '[';
  const isAfterEqual = inclusivity[1] === ']';

  return (
    (isBeforeEqual
      ? isEqual(from, date) || isBefore(from, date)
      : isBefore(from, date)) &&
    (isAfterEqual ? isEqual(to, date) || isAfter(to, date) : isAfter(to, date))
  );
};

/**
 * Creates a new Date object with the specified date and time, or the current date and time if no arguments are provided.
 *
 * @param yearOrIsoString The year for the new Date object, or an ISO-formatted string representing the date and time.
 * @param month The month for the new Date object (0-11).
 * @param day The day for the new Date object (1-31).
 * @param hours The hour for the new Date object (0-23).
 * @param minutes The minute for the new Date object (0-59).
 * @param seconds The second for the new Date object (0-59).
 * @param milliseconds The millisecond for the new Date object (0-999).
 * @returns A new Date object with the specified date and time, or the current date and time if no arguments are provided.
 *
 * @example
 * createDate(2022, 11, 13);
 * // => 2022-12-13T00:00:00.000Z
 *
 * @example
 * createDate('2022-12-13T00:00:00.000Z');
 * // => 2022-12-13T00:00:00.000Z
 */

export const createDate = (
  yearOrIsoString?: number | string,
  month?: number,
  day?: number,
  hours = 0,
  minutes = 0,
  seconds = 0,
  milliseconds = 0,
): Date => {
  if (yearOrIsoString === undefined) {
    return new Date();
  }

  if (typeof yearOrIsoString === 'string') {
    return new Date(yearOrIsoString);
  }

  return new Date(
    yearOrIsoString,
    month!,
    day!,
    hours,
    minutes,
    seconds,
    milliseconds,
  );
};

/**
 *
 * @param date
 * @param duration
 * @returns {Date} the new Date
 */
export const add = (date: Date | number, duration: Duration): Date => {
  return dateFnsAdd(date, duration);
};

/**
 *
 * @param date
 * @param duration
 * @returns {Date} the new Date
 */
export const sub = (date: Date | number, duration: Duration): Date => {
  return dateFnsSub(date, duration);
};

/**
 * Finds the number of minuts between to dates
 *
 * @param dateLeft - the later date
 * @param dateRight - the earlier date
 * @returns {number} the number of minutes
 */
export const differenceInMinutes = (
  dateLeft: Date | number,
  dateRight: Date | number,
): number => {
  return dateFnsDifferenceInMinutes(dateLeft, dateRight);
};

/**
 * Finds the number of hours between to dates
 *
 * @param dateLeft - the later date
 * @param dateRight - the earlier date
 * @returns {number} the number of hours
 */
export const differenceInHours = (
  dateLeft: Date | number,
  dateRight: Date | number,
): number => {
  return dateFnsDifferenceInHours(dateLeft, dateRight);
};

/**
 *
 * @param date - the date that should be before the other one to return true
 * @param dateToCompare -the date to compare with
 *
 * @returns the first date is before the second date
 */
export const isBefore = (
  date: Date | number,
  dateToCompare: Date | number,
): boolean => {
  const before = dateFnsIsBefore(date, dateToCompare);
  return before;
};

/**
 *
 * @param date - the date that should be after the other one to return true
 * @param dateToCompare -the date to compare with
 *
 * @returns the first date is after the second date
 */
export const isAfter = (
  date: Date | number,
  dateToCompare: Date | number,
): boolean => {
  return dateFnsIsAfter(date, dateToCompare);
};

/**
 * Parsing string to a date, setting tz to UTC even if it's missing
 * @param dateStr - string containing date
 * @returns {Date} - Date in UTC
 */
export const utc = (dateStr?: string): Date => {
  if (dateStr === undefined) {
    return createDate();
  }

  if (hasOffset(dateStr)) {
    return isoStringToDate(dateStr, false);
  }

  return isoStringToDate(dateStr);
};

const hasOffset = (input: string): boolean => {
  return !!getOffsetStr(input);
};

const getOffsetStr = (input: string): string | null => {
  const isoDatePattern =
    /(\d{4}-\d{2}-\d{2})T(\d{2}:\d{2}:\d{2})(\.\d+)?([+-]\d{2}:?\d{2}|Z)$/;

  const groups = input.match(isoDatePattern);
  return groups ? groups[4] : null;
};

/**
 * Check if a date object is valid
 * @param {Date} date - date to be checked
 * @returns {boolean} - True if valid
 */
export const isValid = (date: Date): boolean => {
  return dateFnsIsValid(date);
};

/**
 * Get the seconds timestamp of the given date
 * @param {Date} date - the given date
 * @returns the timestamp
 */
export const unix = (date: Date | number): number => {
  return getUnixTime(date);
};

/**
 * Set date values to a given date.
 * @param date - the dater to be changed
 * @param values - an object with options
 * @returns the new date with options set
 */
export const set = (date: Date | number, values: DateValues): Date => {
  return dateFnsSet(date, values);
};
