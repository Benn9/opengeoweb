/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */

import configureStore, { MockStoreEnhanced } from 'redux-mock-store';
import {
  configureStore as toolkitConfigureStore,
  AnyAction,
  Dispatch,
  Middleware,
  Reducer,
  ReducersMapObject,
  ThunkMiddleware,
  StoreEnhancer,
} from '@reduxjs/toolkit';
import {
  ConfigureStoreOptions,
  ToolkitStore,
} from '@reduxjs/toolkit/dist/configureStore';

interface MockStoreWithEggs extends MockStoreEnhanced {
  addEggs?: () => void;
}

interface ToolkitStoreWithEggs extends ToolkitStore {
  addEggs?: () => void;
}

interface StoreConfig {
  reducer: Reducer<unknown, AnyAction> | ReducersMapObject<unknown, AnyAction>;
  preloadedState:
    | ConfigureStoreOptions<
        unknown,
        AnyAction,
        [ThunkMiddleware<unknown, AnyAction>],
        [StoreEnhancer]
      >
    | object
    | undefined;
}

export const createMockStoreWithEggs = (
  mockState: unknown,
): MockStoreWithEggs => {
  const mockStore = configureStore();
  const store = mockStore(mockState) as MockStoreWithEggs;
  store.addEggs = typeof jest !== 'undefined' ? jest.fn() : (): void => {}; // mocking the dynamic module loader
  return store;
};

export const createMockStoreWithEggsAndMiddleware = (
  mockState: unknown,
  // eslint-disable-next-line @typescript-eslint/ban-types, @typescript-eslint/no-explicit-any
  middleware: Middleware<{}, any, Dispatch<AnyAction>>,
): MockStoreWithEggs => {
  const mockStore = configureStore([middleware]);
  const store = mockStore(mockState) as MockStoreWithEggs;
  store.addEggs = typeof jest !== 'undefined' ? jest.fn() : (): void => {}; // mocking the dynamic module loader
  return store;
};

export const createToolkitMockStoreWithEggs = (
  storeConfig: StoreConfig,
): ToolkitStoreWithEggs => {
  const { reducer, preloadedState } = storeConfig;
  const store = toolkitConfigureStore({
    reducer,
    preloadedState,
  }) as ToolkitStoreWithEggs;
  store.addEggs = typeof jest !== 'undefined' ? jest.fn() : (): void => {}; // mocking the dynamic module loader
  return store;
};
