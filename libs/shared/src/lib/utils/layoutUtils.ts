/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */

// TODO: Move to related component when doing this user story: https://gitlab.com/opengeoweb/geoweb-assets/-/issues/2955
export const getHeight = (element: HTMLDivElement): number | null => {
  if (!element) {
    return null;
  }
  const style = getComputedStyle(element);
  const marginTop = style.marginTop ? parseInt(style.marginTop, 10) : 0;
  const marginBottom = style.marginTop ? parseInt(style.marginBottom, 10) : 0;
  return element.offsetHeight + marginTop + marginBottom;
};
