/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import {
  getCorrectWMSDimName,
  getMapDimURL,
  isDefined,
  isNull,
  toArray,
  URLDecode,
  URLEncode,
  WMJScheckURL,
  getUriWithParam,
  dateToISO8601,
  getUriAddParam,
  addStylesForLayer,
  addDimensionsForLayer,
  addBoundingBoxForLayer,
  sortArrayOfObjectsByKey,
  flattenLayerTree,
  makeNodeLayerFromWMSGetCapabilityLayer,
  buildLayerTreeFromNestedWMSLayer,
} from './WMJSTools';
import WMLayer, { LayerType } from './WMLayer';
import {
  defaultReduxLayerRadarKNMI,
  multiDimensionLayer,
} from '../utils/testUtils';
import {
  WMDateOutSideRange,
  WMDateTooEarlyString,
  WMDateTooLateString,
} from './WMConstants';
import WMSLayerObjectForTestingWMLayerHelperFunctions from '../utils/specs/WMSLayerObjectForTestingWMLayerHelperFunctions.json';
import { LayerProps, LayerTree, WMSLayerFromGetCapabilities } from './types';

describe('components/WMJSTools', () => {
  describe('isDefined', () => {
    it('should return false if undefined or null', () => {
      expect(isDefined(null)).toEqual(false);
      expect(isDefined(undefined)).toEqual(false);
      const undefConst = undefined;
      expect(isDefined(undefConst)).toEqual(false);
      expect(isDefined('hello')).toEqual(true);
      expect(isDefined(0)).toEqual(true);
      expect(isDefined(false)).toEqual(true);
      expect(isDefined({})).toEqual(true);
    });
  });

  describe('isNull', () => {
    it('should return true null', () => {
      expect(isNull(null)).toEqual(true);
      expect(isNull(undefined)).toEqual(false);
      expect(isNull('hello')).toEqual(false);
      expect(isNull(0)).toEqual(false);
      expect(isNull(false)).toEqual(false);
      expect(isNull({})).toEqual(false);
    });
  });

  describe('toArray', () => {
    it('should return as first element in array', () => {
      expect(toArray(12)).toEqual([12]);
      expect(toArray(0)).toEqual([0]);
      expect(toArray('12')).toEqual(['12']);
      expect(toArray(false)).toEqual([false]);
      expect(toArray(true)).toEqual([true]);
      expect(toArray({ weight: 'kg' })).toEqual([{ weight: 'kg' }]);
    });
    it('should return passed in variable if already array', () => {
      expect(toArray(['12'])).toEqual(['12']);
      expect(toArray([10, 15, 20])).toEqual([10, 15, 20]);
    });
    it('should return empty array if null/undefined passed', () => {
      expect(toArray(null)).toEqual([]);
      expect(toArray(undefined)).toEqual([]);
    });
  });

  describe('WMJScheckURL', () => {
    it('return url + ? if no ? found in string otherwise return the original (trimmed) url', () => {
      expect(WMJScheckURL('someurl.nl')).toEqual('someurl.nl?');
      expect(WMJScheckURL('someur//--lverylong.nl  ')).toEqual(
        'someur//--lverylong.nl?',
      );
      expect(WMJScheckURL('')).toEqual('?');
      expect(WMJScheckURL(null!)).toEqual('?');
      expect(WMJScheckURL(undefined!)).toEqual('?');
      expect(WMJScheckURL('?')).toEqual('?');
      expect(WMJScheckURL('someurl.nl? ')).toEqual('someurl.nl?');
    });
    it('return url + ? if no ? found in string', () => {
      expect(WMJScheckURL('someurl.nl')).toEqual('someurl.nl?');
      expect(WMJScheckURL('someur//--lverylong.nl  ')).toEqual(
        'someur//--lverylong.nl?',
      );
      expect(WMJScheckURL('')).toEqual('?');
      expect(WMJScheckURL(null!)).toEqual('?');
      expect(WMJScheckURL(undefined!)).toEqual('?');
    });
  });

  describe('URLDecode', () => {
    it('should return empty string if null or undefined passed', () => {
      expect(URLDecode(null!)).toBe('');
      expect(URLDecode(undefined!)).toBe('');
    });
    it('should return decoded string', () => {
      expect(URLDecode('www.url.com%2F')).toBe('www.url.com/');
      expect(URLDecode('www.url.com%2F')).toBe('www.url.com/');
      expect(URLDecode('www.url.com%2F%3C%3A')).toBe('www.url.com/<:');
      expect(URLDecode(undefined!)).toBe('');
    });
  });

  describe('URLEncode', () => {
    it('should return passed parameter if undefined/""/no string is passed', () => {
      expect(URLEncode(null!)).toBe(null);
      expect(URLEncode(undefined!)).toBe(undefined);
      expect(URLEncode('')).toBe('');
    });
    it('should keep safe chars and replace unsafe chars', () => {
      expect(
        URLEncode(
          'www.url.com/RADNL_OPER_R___25PCPRR_L3.cgi&DIM_flight level=625&elevation=9000&time=2020-03-13T14-40-00Z',
        ),
      ).toBe(
        'www.url.com%2FRADNL_OPER_R___25PCPRR_L3.cgi%26DIM_flight%20level%3D625%26elevation%3D9000%26time%3D2020-03-13T14-40-00Z',
      );
      expect(URLEncode('www.encode.nl/ >?/')).toBe(
        'www.encode.nl%2F%20%3E%3F%2F',
      );
      expect(URLEncode('www.encode.nl/+=/^%&(')).toBe(
        'www.encode.nl%2F%2B%3D%2F%5E%25%26(',
      );
    });
    it('should replace chars with code > 255 with a +  ', () => {
      expect(URLEncode('www.encode.nl/œ')).toBe('www.encode.nl%2F+');
    });
    it('should replace chars with code < 255 with their ISO-8859-15 code', () => {
      expect(URLEncode('www.encode.nl/þ')).toBe('www.encode.nl%2F%FE');
    });
  });

  describe('getCorrectWMSDimName', () => {
    it('should return DIM_ passed in parameter unless time or elevation passed and always convert to upper case', () => {
      expect(getCorrectWMSDimName('time')).toEqual('TIME');
      expect(getCorrectWMSDimName('elevation')).toEqual('ELEVATION');
      expect(getCorrectWMSDimName('otherdimension')).toEqual(
        'DIM_OTHERDIMENSION',
      );
      expect(getCorrectWMSDimName('timee')).toEqual('DIM_TIMEE');
    });
  });

  describe('getMapDimURL', () => {
    it('should the dimension urls for all passed layer dimensions and convert dimension names to upper case', () => {
      const layer = new WMLayer(multiDimensionLayer);
      expect(getMapDimURL(layer)).toEqual(
        '&DIM_FLIGHT LEVEL=625&ELEVATION=9000&TIME=2020-03-13T14%3A40%3A00Z',
      );
      const layer2 = new WMLayer(defaultReduxLayerRadarKNMI);
      expect(getMapDimURL(layer2)).toEqual('&TIME=2020-03-13T13%3A30%3A00Z');
    });
    it('should throw an error if current value is too late/early or outide of the range', () => {
      const layer = new WMLayer({
        service: 'https://testservice',
        name: 'RADNL_OPER_R___25PCPRR_L3_KNMI',
        title: 'RADNL_OPER_R___25PCPRR_L3_KNMI',
        format: 'image/png',
        style: 'knmiradar/nearest',
        enabled: true,
        layerType: LayerType.mapLayer,
        dimensions: [
          {
            name: 'time',
            units: 'ISO8601',
            currentValue: WMDateOutSideRange,
          },
        ],
        id: 'layerid_2',
      });
      expect(() => getMapDimURL(layer)).toThrow(WMDateOutSideRange);
      layer.dimensions[0].setValue(WMDateTooEarlyString);
      expect(() => getMapDimURL(layer)).toThrow(WMDateOutSideRange);
      layer.dimensions[0].setValue(WMDateTooLateString);
      expect(() => getMapDimURL(layer)).toThrow(WMDateOutSideRange);
    });
  });

  describe('getUriWithParam', () => {
    it('should parse the url and set change the key value pair', () => {
      const myQueryString =
        'http://abcd.efg/hijklm?parameterA=ShouldChangeThis&parameterB=ThisShouldBeLeftUnchanged';
      const parsedQueryString = getUriWithParam(myQueryString, {
        parameterA: 'MyNewValue',
        parameterC: 'AddedValue',
        parameterD: undefined,
      });

      expect(parsedQueryString).toBe(
        'http://abcd.efg/hijklm?parameterA=MyNewValue&parameterB=ThisShouldBeLeftUnchanged&parameterC=AddedValue',
      );
    });
  });

  describe('getUriAddParam', () => {
    it('should parse the url and set any additional parameters not yet set in the url', () => {
      const myQueryString =
        'http://abcd.efg/hijklm?parameterA=ThisShouldBeLeftUnchanged&parameterB=ThisShouldBeLeftUnchanged';
      const parsedQueryString = getUriAddParam(myQueryString, {
        parameterA: 'MyNewValue',
        parameterC: 'AddedValue',
        parameterD: undefined,
      });

      expect(parsedQueryString).toBe(
        'http://abcd.efg/hijklm?parameterA=ThisShouldBeLeftUnchanged&parameterB=ThisShouldBeLeftUnchanged&parameterC=AddedValue',
      );
    });
    it('should add parameters if none set', () => {
      const myQueryString = 'http://abcd.efg/hijklm';
      const parsedQueryString = getUriAddParam(myQueryString, {
        parameterA: 'MyNewValue',
        parameterC: 'AddedValue',
        parameterD: undefined,
      });

      expect(parsedQueryString).toBe(
        'http://abcd.efg/hijklm?parameterA=MyNewValue&parameterC=AddedValue',
      );
    });
    it('should be case insensitive', () => {
      const myQueryString =
        'http://abcd.efg/hijklm?parameterA=ThisShouldBeLeftUnchanged&PARAMETERB=ThisShouldBeLeftUnchanged&parameterc=ShouldNotTouched';
      const parsedQueryString = getUriAddParam(myQueryString, {
        PARAMETERa: 'MyNewValue',
        parameterB: 'AddedValue',
        parameterD: undefined,
      });

      expect(parsedQueryString).toBe(
        'http://abcd.efg/hijklm?parameterA=ThisShouldBeLeftUnchanged&PARAMETERB=ThisShouldBeLeftUnchanged&parameterc=ShouldNotTouched',
      );
    });
  });

  describe('dateToISO8601', () => {
    it('should convert a date to an ISO8601 string', () => {
      const myJSDate = new Date('2021-05-26T01:23:45Z');
      const iso8601String = dateToISO8601(myJSDate);
      expect(iso8601String).toBe('2021-05-26T01:23:45Z');
    });
  });

  describe('addStylesForLayer', () => {
    it('Check addStylesForLayer helper function', () => {
      const layer = new WMLayer();

      layer.styles = addStylesForLayer(
        WMSLayerObjectForTestingWMLayerHelperFunctions as unknown as WMSLayerFromGetCapabilities,
      );
      expect(layer.getStyles().length).toEqual(6);
      expect(layer.getStyles()[0].name).toEqual('radar/nearest');
      expect(layer.getStyles()[0].title).toEqual('radar/nearest');
      expect(layer.getStyles()[0].abstract).toEqual('No abstract available');
      expect(layer.getStyles()[0].legendURL).toBeDefined();

      const styleObject = layer.getStyleObject(
        'precip-blue-transparent/nearest',
        0,
      );
      expect(styleObject.name).toEqual('precip-blue-transparent/nearest');
      expect(styleObject.title).toEqual('Title for this style precip blue');
      expect(styleObject.abstract).toEqual(
        'Abstract for this style precip blue',
      );

      expect(layer.currentStyle).toBe('');
    });
  });

  describe('addDimensionsForLayer', () => {
    it('should return a list of all layer dimensions for WMS100', () => {
      const layer = {
        Dimension: [
          {
            attr: { name: 'time', units: 'ISO8601' },
          },
          {
            attr: { name: 'elevation', units: 'hPa' },
          },
        ],
        Extent: [
          {
            attr: {
              name: 'time',
              default: '2022-10-04T13:40:00Z',
              multipleValues: '1',
              nearestValue: '0',
            },
            value: '2021-03-31T09:25:00Z/2022-10-04T13:40:00Z/PT5M',
          },
          {
            attr: {
              name: 'elevation',
              default: '200',
              multipleValues: '1',
              nearestValue: '0',
            },
            value: '200,300',
          },
        ],
      } as WMSLayerFromGetCapabilities;
      const result = addDimensionsForLayer(layer);
      expect(result).toEqual([
        {
          name: 'time',
          currentValue: '2022-10-04T13:40:00Z',
          unitSymbol: undefined,
          units: 'ISO8601',
          values: '2021-03-31T09:25:00Z/2022-10-04T13:40:00Z/PT5M',
        },
        {
          currentValue: '200',
          name: 'elevation',
          unitSymbol: undefined,
          units: 'hPa',
          values: '200,300',
        },
      ]);
    });
    it('should return a list of all layer dimensions for WMS130', () => {
      const layer = {
        Dimension: [
          {
            attr: { name: 'time', units: 'ISO8601' },
            value: '2021-03-31T09:25:00Z/2022-10-04T13:40:00Z/PT5M',
          },
          {
            attr: { name: 'elevation', units: 'hPa' },
            value: '200,300',
          },
        ],
      } as WMSLayerFromGetCapabilities;
      const result = addDimensionsForLayer(layer);
      expect(result).toEqual([
        {
          name: 'time',
          currentValue: '',
          unitSymbol: undefined,
          units: 'ISO8601',
          values: '2021-03-31T09:25:00Z/2022-10-04T13:40:00Z/PT5M',
        },
        {
          currentValue: '',
          name: 'elevation',
          unitSymbol: undefined,
          units: 'hPa',
          values: '200,300',
        },
      ]);
    });
    it('should return an empty list if layer has no dimensions', () => {
      expect(addDimensionsForLayer({} as WMSLayerFromGetCapabilities)).toEqual(
        [],
      );
    });
  });

  describe('addBoundingBoxForLayer', () => {
    it('should return the bounding box for wms version 1.1.1', () => {
      const layer = {
        LatLonBoundingBox: {
          attr: {
            minx: '0.000000',
            miny: '48.895303',
            maxx: '10.856452',
            maxy: '55.973600',
          },
        },
      } as WMSLayerFromGetCapabilities;
      const result = addBoundingBoxForLayer(layer);
      expect(result).toEqual({
        east: '10.856452',
        north: '55.973600',
        south: '48.895303',
        west: '0.000000',
      });
    });
    it('should return the bounding box for wms version 1.3.0', () => {
      const layer = {
        EX_GeographicBoundingBox: {
          northBoundLatitude: { value: '55.973600' },
          southBoundLatitude: { value: '48.895303' },
          westBoundLongitude: { value: '0.000000' },
          eastBoundLongitude: { value: '10.856452' },
        },
      } as WMSLayerFromGetCapabilities;
      const result = addBoundingBoxForLayer(layer);
      expect(result).toEqual({
        east: '10.856452',
        north: '55.973600',
        south: '48.895303',
        west: '0.000000',
      });
    });
    it('should return undefined if layer has no geographical bounding box', () => {
      expect(addBoundingBoxForLayer({} as WMSLayerFromGetCapabilities)).toEqual(
        undefined,
      );
    });
  });

  describe('sortArrayOfObjectsByKey', () => {
    it('should do nothing on an empty array', () => {
      const array: unknown = [];
      const result = sortArrayOfObjectsByKey(array as unknown as [], 'text');
      expect(result).toEqual([]);
    });
    it('should do nothing on an array with one value', () => {
      const array = [{ text: 'string', leaf: true }];
      const result = sortArrayOfObjectsByKey(array as unknown as [], 'text');
      expect(result).toEqual([{ text: 'string', leaf: true }]);
    });
    it('should not modify array in place, but result should be modified', () => {
      const array = [
        { name: '2', text: 'B' },
        { name: '1', text: 'A' },
      ];
      const stringRepresentationArray = JSON.stringify(array);
      const result = sortArrayOfObjectsByKey(array as unknown as [], 'text');
      const stringRepresentationResult = JSON.stringify(result);
      expect(stringRepresentationArray).toEqual(JSON.stringify(array));
      expect(stringRepresentationResult).toEqual(
        '[{"name":"1","text":"A"},{"name":"2","text":"B"}]',
      );
    });
    it('should do nothing when given key does not exist', () => {
      const array = [
        { text: 'a', leaf: true },
        { text: 'b', leaf: true },
      ];
      const result = sortArrayOfObjectsByKey(array as unknown as [], 'hello');
      expect(result).toEqual([
        { text: 'a', leaf: true },
        { text: 'b', leaf: true },
      ]);
    });
    it('should do nothing on an array with two values that is already sorted alphabetically', () => {
      const array = [
        { text: 'a', leaf: true },
        { text: 'b', leaf: true },
      ];
      const result = sortArrayOfObjectsByKey(array as unknown as [], 'text');
      expect(result).toEqual([
        { text: 'a', leaf: true },
        { text: 'b', leaf: true },
      ]);
    });
    it('should sort on alphabet by given key', () => {
      const array = [
        { text: 'b', leaf: true, name: 'aa' },
        { text: 'a', leaf: false, name: 'cc' },
        { text: 'c', leaf: false, name: 'bb' },
        { text: 'd', leaf: false, name: 'dd' },
      ];
      const resultA = sortArrayOfObjectsByKey(array as unknown as [], 'text');
      expect(resultA).toEqual([
        { text: 'a', leaf: false, name: 'cc' },
        { text: 'b', leaf: true, name: 'aa' },
        { text: 'c', leaf: false, name: 'bb' },
        { text: 'd', leaf: false, name: 'dd' },
      ]);
      const resultB = sortArrayOfObjectsByKey(array as unknown as [], 'name');
      expect(resultB).toEqual([
        { text: 'b', leaf: true, name: 'aa' },
        { text: 'c', leaf: false, name: 'bb' },
        { text: 'a', leaf: false, name: 'cc' },
        { text: 'd', leaf: false, name: 'dd' },
      ]);
    });
    it('should sort on a non string property', () => {
      const array = [
        { text: 'b', leaf: true, name: 'aa' },
        { text: 'a', leaf: false, name: 'cc' },
        { text: 'c', leaf: false, name: 'bb' },
        { text: 'd', leaf: false, name: 'dd' },
      ];
      const result = sortArrayOfObjectsByKey(array as unknown as [], 'leaf');
      expect(result).toEqual([
        { text: 'a', leaf: false, name: 'cc' },
        { text: 'c', leaf: false, name: 'bb' },
        { text: 'd', leaf: false, name: 'dd' },
        { text: 'b', leaf: true, name: 'aa' },
      ]);
    });
  });
  describe('flattenLayerTree', () => {
    it('should flatten and sort the LayerTree', () => {
      const layerTree: LayerTree = {
        leaf: false,
        title: 'A',
        name: undefined!,
        path: [],
        keywords: [],
        abstract: '',
        styles: [],
        dimensions: [],
        geographicBoundingBox: null!,
        children: [
          {
            leaf: false,
            title: 'B',
            name: undefined!,
            path: ['A'],
            keywords: [],
            abstract: '',
            styles: [],
            dimensions: [],
            geographicBoundingBox: null!,
            children: [
              {
                leaf: false,
                name: undefined!,
                title: 'C',
                path: ['A', 'B'],
                keywords: [],
                abstract: '',
                styles: [],
                dimensions: [],
                geographicBoundingBox: null!,
                children: [
                  {
                    leaf: true,
                    name: '2',
                    title: '2: Layer',
                    path: ['A', 'B', 'C'],
                    children: [],
                    keywords: [],
                    styles: [],
                    dimensions: [],
                    geographicBoundingBox: null!,
                    abstract: '',
                  },
                  {
                    leaf: true,
                    name: '1',
                    title: '1: Layer',
                    path: ['A', 'B', 'C'],
                    keywords: [],
                    styles: [],
                    dimensions: [],
                    geographicBoundingBox: null!,
                    abstract: '',
                  } as unknown as LayerTree,
                  {
                    leaf: true,
                    name: '3',
                    title: '3: Layer',
                    path: ['A', 'B', 'C'],
                    children: [],
                    keywords: [],
                    styles: [],
                    dimensions: [],
                    geographicBoundingBox: null!,
                    abstract: '',
                  },
                  {
                    leaf: false,
                    name: undefined!,
                    title: 'D',
                    path: ['A', 'B', 'C'],
                    children: [
                      {
                        leaf: true,
                        name: 'd',
                        title: '4: Layer',
                        path: ['A', 'B', 'C', 'D'],
                        children: [],
                        keywords: [],
                        styles: [],
                        dimensions: [],
                        geographicBoundingBox: null!,
                        abstract: '',
                      },
                    ],
                    keywords: [],
                    styles: [],
                    dimensions: [],
                    geographicBoundingBox: null!,
                    abstract: '',
                  },
                ],
              },
            ],
          },
        ],
      };
      const layerProps = flattenLayerTree(layerTree);
      expect(layerProps).toEqual([
        {
          name: '1',
          leaf: true,
          path: ['A', 'B', 'C'],
          title: '1: Layer',
          abstract: '',
          keywords: [],
          styles: [],
          dimensions: [],
          geographicBoundingBox: null,
        },
        {
          name: '2',
          leaf: true,
          path: ['A', 'B', 'C'],
          title: '2: Layer',
          abstract: '',
          keywords: [],
          styles: [],
          dimensions: [],
          geographicBoundingBox: null,
        },
        {
          name: '3',
          leaf: true,
          path: ['A', 'B', 'C'],
          title: '3: Layer',
          abstract: '',
          keywords: [],
          styles: [],
          dimensions: [],
          geographicBoundingBox: null,
        },
        {
          name: 'd',
          leaf: true,
          path: ['A', 'B', 'C', 'D'],
          title: '4: Layer',
          abstract: '',
          keywords: [],
          styles: [],
          dimensions: [],
          geographicBoundingBox: null,
        },
      ]);
    });
    it('should not crash if empty array is given', () => {
      const layerProps = flattenLayerTree([] as unknown as LayerTree);
      expect(layerProps).toHaveLength(0);
    });
    it('should not crash if a string is given instead of an array', () => {
      const layerProps = flattenLayerTree({
        children: 'test',
      } as unknown as LayerTree);
      expect(layerProps).toHaveLength(0);
    });
    it('should not crash if no children are given', () => {
      const layerProps = flattenLayerTree([
        { children: [] },
      ] as unknown as LayerTree);
      expect(layerProps).toHaveLength(0);
    });
    it('should not crash if children is not an array', () => {
      const layerProps = flattenLayerTree([
        { children: 'not an array but a string' },
      ] as unknown as LayerTree);
      expect(layerProps).toHaveLength(0);
    });
  });

  describe('makeNodeLayerFromWMSGetCapabilityLayer', () => {
    it('should return layerprops array from WMSLayerFromGetCapabilities object ', () => {
      const layerProps = makeNodeLayerFromWMSGetCapabilityLayer(
        WMSLayerObjectForTestingWMLayerHelperFunctions as unknown as WMSLayerFromGetCapabilities,
      );
      expect(layerProps).toEqual({
        name: 'RAD_NL25_PCP_CM',
        title: 'Precipitation Radar NL',
        leaf: false,
        path: [],
        keywords: [],
        styles: [
          {
            title: 'radar/nearest',
            name: 'radar/nearest',
            legendURL:
              'https://geoservices.knmi.nl/wms?DATASET=RADAR&SERVICE=WMS&version=1.1.1&service=WMS&request=GetLegendGraphic&layer=RAD_NL25_PCP_CM&format=image/png&STYLE=radar/nearest',
            abstract: 'No abstract available',
          },
          {
            title: 'precip-rainbow/nearest',
            name: 'precip-rainbow/nearest',
            legendURL:
              'https://geoservices.knmi.nl/wms?DATASET=RADAR&SERVICE=WMS&version=1.1.1&service=WMS&request=GetLegendGraphic&layer=RAD_NL25_PCP_CM&format=image/png&STYLE=precip-rainbow/nearest',
            abstract: 'No abstract available',
          },
          {
            title: 'precip-gray/nearest',
            name: 'precip-gray/nearest',
            legendURL:
              'https://geoservices.knmi.nl/wms?DATASET=RADAR&SERVICE=WMS&version=1.1.1&service=WMS&request=GetLegendGraphic&layer=RAD_NL25_PCP_CM&format=image/png&STYLE=precip-gray/nearest',
            abstract: 'No abstract available',
          },
          {
            title: 'precip-blue/nearest',
            name: 'precip-blue/nearest',
            legendURL:
              'https://geoservices.knmi.nl/wms?DATASET=RADAR&SERVICE=WMS&version=1.1.1&service=WMS&request=GetLegendGraphic&layer=RAD_NL25_PCP_CM&format=image/png&STYLE=precip-blue/nearest',
            abstract: 'No abstract available',
          },
          {
            title: 'Title for this style precip blue',
            name: 'precip-blue-transparent/nearest',
            legendURL:
              'https://geoservices.knmi.nl/wms?DATASET=RADAR&SERVICE=WMS&version=1.1.1&service=WMS&request=GetLegendGraphic&layer=RAD_NL25_PCP_CM&format=image/png&STYLE=precip-blue-transparent/nearest',
            abstract: 'Abstract for this style precip blue',
          },
          {
            title: 'precip-with-range/nearest',
            name: 'precip-with-range/nearest',
            legendURL:
              'https://geoservices.knmi.nl/wms?DATASET=RADAR&SERVICE=WMS&version=1.1.1&service=WMS&request=GetLegendGraphic&layer=RAD_NL25_PCP_CM&format=image/png&STYLE=precip-with-range/nearest',
            abstract: 'No abstract available',
          },
        ],
        dimensions: [
          {
            name: 'time',
            units: 'ISO8601',
            currentValue: '2021-05-17T00:00:00Z',
            values: '2021-03-31T09:25:00Z/2021-05-18T07:45:00Z/PT5M',
          },
        ],
        geographicBoundingBox: {
          east: '10.856452',
          west: '0.000000',
          north: '55.973600',
          south: '48.895303',
        },
        queryable: true,
        crs: [
          {
            name: 'EPSG:3411',
            bbox: {
              left: 2682754.74362,
              right: 3759536.917562,
              bottom: -3245034.014141,
              top: -2168251.989038,
            },
          },
          {
            name: 'EPSG:3412',
            bbox: {
              left: 0,
              right: 7413041.166015,
              bottom: 32318824.826266,
              top: 40075258.815074,
            },
          },
          {
            name: 'EPSG:3575',
            bbox: {
              left: -770622.801471,
              right: 56845.766135,
              bottom: -4485814.811314,
              top: -3684039.44362,
            },
          },
          {
            name: 'EPSG:3857',
            bbox: {
              left: 0,
              right: 1208534.698398,
              bottom: 6257115.219364,
              top: 7553161.958695,
            },
          },
          {
            name: 'EPSG:4258',
            bbox: {
              left: 0,
              right: 10.856452,
              bottom: 48.895303,
              top: 55.9736,
            },
          },
          {
            name: 'EPSG:4326',
            bbox: {
              left: 0,
              right: 10.856452,
              bottom: 48.895303,
              top: 55.9736,
            },
          },
          {
            name: 'CRS:84',
            bbox: {
              left: 0,
              right: 10.856452,
              bottom: 48.895303,
              top: 55.9736,
            },
          },
          {
            name: 'EPSG:25831',
            bbox: {
              left: 282182.345905,
              right: 997135.658653,
              bottom: 5433247.394267,
              top: 6207204.592736,
            },
          },
          {
            name: 'EPSG:25832',
            bbox: {
              left: -153083.019482,
              right: 617595.626092,
              bottom: 5415817.312927,
              top: 6239769.309937,
            },
          },
          {
            name: 'EPSG:28992',
            bbox: {
              left: -236275.338083,
              right: 501527.918656,
              bottom: 106727.731651,
              top: 900797.079725,
            },
          },
          {
            name: 'EPSG:7399',
            bbox: {
              left: 0,
              right: 763611.971696,
              bottom: 5757301.056717,
              top: 6483919.801602,
            },
          },
          {
            name: 'EPSG:50001',
            bbox: {
              left: -2000000,
              right: 10000000,
              bottom: -2000000,
              top: 8500000,
            },
          },
          {
            name: 'EPSG:54030',
            bbox: {
              left: 0,
              right: 853649.695106,
              bottom: 5211855.054125,
              top: 5936394.291427,
            },
          },
          {
            name: 'EPSG:32661',
            bbox: {
              left: 2000000,
              right: 2745713.040381,
              bottom: -2703305.597319,
              top: -1888346.21671,
            },
          },
          {
            name: 'EPSG:40000',
            bbox: {
              left: 0,
              right: 750214.326339,
              bottom: -4731695.771951,
              top: -3911817.119426,
            },
          },
          {
            name: 'EPSG:900913',
            bbox: {
              left: 0,
              right: 1208534.698398,
              bottom: 6257115.219364,
              top: 7553161.958695,
            },
          },
          {
            name: 'PROJ4:%2Bproj%3Dstere%20%2Blat_0%3D90%20%2Blon_0%3D0%20%2Blat_ts%3D60%20%2Ba%3D6378%2E14%20%2Bb%3D6356%2E75%20%2Bx_0%3D0%20y_0%3D0',
            bbox: {
              left: 0,
              right: 700.00242,
              bottom: -3649.999338,
              top: -4415.002986,
            },
          },
        ],
      });
    });
    it('should return deepest nested layer properties for style, dimension and geographic boundingbox', () => {
      const layerPropsArray: LayerProps[] = [
        {
          geographicBoundingBox: {
            west: '1',
            south: '2',
            east: '3',
            north: '4',
          },
          dimensions: [
            {
              name: 'time',
              currentValue: '1',
            },
          ],
          styles: [
            {
              name: 'style',
              title: 'My style title',
              legendURL: 'legendurl',
              abstract: 'My style abstract',
            },
          ],
          name: null,
          leaf: false,
          title: 'test',
          path: ['A'],
        },
        {
          geographicBoundingBox: {
            west: '5',
            south: '6',
            east: '7',
            north: '8',
          },
          dimensions: [
            {
              name: 'time',
              currentValue: '2',
            },
          ],
          styles: [
            {
              name: 'style2',
              title: 'My style2 title',
              legendURL: 'legendurl',
              abstract: 'My style2 abstract',
            },
          ],
          name: null,
          leaf: false,
          title: 'test',
          path: [],
        },
      ];
      const layerProps = makeNodeLayerFromWMSGetCapabilityLayer(
        {} as unknown as WMSLayerFromGetCapabilities,
        [],
        true,
        layerPropsArray,
      );
      expect(layerProps).toEqual({
        name: null,
        title: 'Layer',
        leaf: true,
        path: [],
        keywords: [],
        styles: [
          {
            name: 'style',
            title: 'My style title',
            legendURL: 'legendurl',
            abstract: 'My style abstract',
          },
          {
            name: 'style2',
            title: 'My style2 title',
            legendURL: 'legendurl',
            abstract: 'My style2 abstract',
          },
        ],
        dimensions: [
          {
            name: 'time',
            currentValue: '1',
          },
        ],
        geographicBoundingBox: {
          west: '5',
          south: '6',
          east: '7',
          north: '8',
        },
        queryable: false,
        crs: [],
      });
    });
    it('should return parent layer properties for style, dimension and geographic boundingbox', () => {
      const layerPropsArray: LayerProps[] = [
        {
          geographicBoundingBox: {
            west: '1',
            south: '2',
            east: '3',
            north: '4',
          },
          name: null,
          leaf: false,
          title: 'test',
          path: ['A'],
          dimensions: [
            {
              name: 'time',
              currentValue: '1',
            },
          ],
          styles: [
            {
              name: 'style',
              title: 'My style title',
              legendURL: 'legendurl',
              abstract: 'My style abstract',
            },
          ],
        },
        {
          name: null,
          leaf: false,
          title: 'test',
          path: [],
        },
      ];
      const layerProps = makeNodeLayerFromWMSGetCapabilityLayer(
        {} as unknown as WMSLayerFromGetCapabilities,
        [],
        true,
        layerPropsArray,
      );
      expect(layerProps).toEqual({
        name: null,
        title: 'Layer',
        leaf: true,
        path: [],
        keywords: [],
        styles: [
          {
            name: 'style',
            title: 'My style title',
            legendURL: 'legendurl',
            abstract: 'My style abstract',
          },
        ],
        dimensions: [
          {
            name: 'time',
            currentValue: '1',
          },
        ],
        geographicBoundingBox: {
          west: '1',
          south: '2',
          east: '3',
          north: '4',
        },
        queryable: false,
        crs: [],
      });
    });
    it('should not crash if layer BoundingBox is invalid ', () => {
      const layerProps = makeNodeLayerFromWMSGetCapabilityLayer({
        BoundingBox: [{ attr: { CRS: 'A', minx: 10.2 } }],
      } as unknown as WMSLayerFromGetCapabilities);

      expect(layerProps).toEqual({
        name: null,
        title: 'Layer',
        leaf: false,
        path: [],
        keywords: [],
        styles: [],
        dimensions: [],
        queryable: false,
        geographicBoundingBox: undefined,
        crs: [
          {
            name: 'A',
            bbox: {
              left: 10.2,
              right: NaN,
              bottom: NaN,
              top: NaN,
            },
          },
        ],
      });
    });
    it('should not crash if layer BoundingBox attr are missing ', () => {
      const layerProps = makeNodeLayerFromWMSGetCapabilityLayer({
        BoundingBox: [{}],
      } as unknown as WMSLayerFromGetCapabilities);

      expect(layerProps).toEqual({
        name: null,
        title: 'Layer',
        leaf: false,
        path: [],
        keywords: [],
        styles: [],
        dimensions: [],
        queryable: false,
        geographicBoundingBox: undefined,
        crs: [],
      });
    });
    it('should use layer name as a fallback value for layer title', () => {
      const layerProps = makeNodeLayerFromWMSGetCapabilityLayer({
        Name: { value: 'Test Title' },
        Title: null,
      } as unknown as WMSLayerFromGetCapabilities);

      expect(layerProps).toEqual({
        name: 'Test Title',
        title: 'Test Title',
        leaf: false,
        path: [],
        keywords: [],
        styles: [],
        dimensions: [],
        queryable: false,
        geographicBoundingBox: undefined,
        crs: [],
      });
    });
    it('should use "Layer" as title when both name and title are missing', () => {
      const layerProps = makeNodeLayerFromWMSGetCapabilityLayer({
        Name: null,
        Title: null,
      } as unknown as WMSLayerFromGetCapabilities);

      expect(layerProps).toEqual({
        name: null,
        title: 'Layer',
        leaf: false,
        path: [],
        keywords: [],
        styles: [],
        dimensions: [],
        queryable: false,
        geographicBoundingBox: undefined,
        crs: [],
      });
    });
  });

  describe('buildLayerTreeFromNestedWMSLayer', () => {
    it('should not do anything when no layers', () => {
      const wmsLayer = {} as unknown as WMSLayerFromGetCapabilities;
      const rootNode = buildLayerTreeFromNestedWMSLayer(wmsLayer);
      expect(rootNode).toEqual({
        abstract: undefined,
        children: [],
        dimensions: [],
        crs: [],
        geographicBoundingBox: undefined,
        keywords: [],
        leaf: false,
        name: null,
        path: [],
        queryable: false,
        styles: [],
        title: 'Layer',
      });
    });
    it('should add given layers to the rootNode', () => {
      const wmsLayer = {
        Name: { value: 'nameWithChildren' },
        Title: { value: 'titleWithChildren' },
        Layer: [
          {
            Name: { value: 'layer1' },
            Title: { value: 'title1' },
            Layer: [
              {
                Name: { value: 'subLayer1' },
                Title: { value: 'subTitle1' },
              },
            ],
          },
          {
            Name: { value: 'layer2' },
            Title: { value: 'title2' },
          },
        ],
      } as unknown as WMSLayerFromGetCapabilities;

      const rootNode = buildLayerTreeFromNestedWMSLayer(wmsLayer);

      expect(rootNode).toEqual({
        abstract: undefined,
        children: [
          {
            abstract: undefined,
            children: [
              {
                abstract: undefined,
                children: [],
                dimensions: [],
                crs: [],
                geographicBoundingBox: undefined,
                keywords: [],
                leaf: true,
                name: 'subLayer1',
                path: ['titleWithChildren', 'title1'],
                queryable: false,
                styles: [],
                title: 'subTitle1',
              },
            ],
            dimensions: [],
            crs: [],
            geographicBoundingBox: undefined,
            keywords: [],
            leaf: false,
            name: 'layer1',
            path: ['titleWithChildren'],
            queryable: false,
            styles: [],
            title: 'title1',
          },
          {
            abstract: undefined,
            children: [],
            dimensions: [],
            crs: [],
            geographicBoundingBox: undefined,
            keywords: [],
            leaf: true,
            name: 'layer2',
            path: ['titleWithChildren'],
            queryable: false,
            styles: [],
            title: 'title2',
          },
        ],
        dimensions: [],
        crs: [],
        geographicBoundingBox: undefined,
        keywords: [],
        leaf: false,
        name: 'nameWithChildren',
        path: [],
        queryable: false,
        styles: [],
        title: 'titleWithChildren',
      });
    });

    it('should handle layers with empty names and titles', () => {
      const wmsLayer = {
        Layer: [
          {
            Name: null,
            Title: null,
          },
          {
            Name: null,
            Title: { value: 'title' },
          },
        ],
      } as unknown as WMSLayerFromGetCapabilities;

      const rootNode = buildLayerTreeFromNestedWMSLayer(wmsLayer);

      expect(rootNode).toEqual({
        abstract: undefined,
        children: [
          {
            abstract: undefined,
            children: [],
            dimensions: [],
            crs: [],
            geographicBoundingBox: undefined,
            keywords: [],
            leaf: null,
            name: null,
            path: [''],
            queryable: false,
            styles: [],
            title: 'Layer',
          },
          {
            abstract: undefined,
            children: [],
            dimensions: [],
            crs: [],
            geographicBoundingBox: undefined,
            keywords: [],
            leaf: null,
            name: null,
            path: [''],
            queryable: false,
            styles: [],
            title: 'title',
          },
        ],
        crs: [],
        dimensions: [],
        geographicBoundingBox: undefined,
        keywords: [],
        leaf: false,
        queryable: false,
        name: null,
        path: [],
        styles: [],
        title: 'Layer',
      });
    });
  });
});
