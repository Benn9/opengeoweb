/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import WMJSMap, { clearImageCache } from './WMJSMap';
import {
  isDefined,
  WMJScheckURL,
  URLDecode,
  URLEncode,
  toArray,
  debugLogger,
  DebugType,
} from './WMJSTools';
import WMTimer from './WMTimer';
import WMGetServiceFromStore from './WMGetServiceFromStore';
import {
  WMDateOutSideRange,
  WMDateTooEarlyString,
  WMDateTooLateString,
  WMEmptyLayerName,
  WMEmptyLayerTitle,
} from './WMConstants';
import {
  parseISO8601DateToDate,
  DateInterval,
  parseISO8601IntervalToDateInterval,
  ParseISOTimeRangeDuration,
} from './WMTime';
import I18n from '../utils/I18n/lang.en';
import WMImage from './WMImage';
import { getLegendGraphicURLForLayer, legendImageStore } from './WMLegend';
import { getMapImageStore, WMImageEventType } from './WMImageStore';
import { WMJSService } from './WMJSService';

export {
  default as WMJSDimension,
  handleMomentISOString,
} from './WMJSDimension';

export { default as WMLayer, LayerType } from './WMLayer';
export type { LayerOptions, LayerFoundation } from './WMLayer';
export { default as WMBBOX } from './WMBBOX';
export type { Bbox } from './WMBBOX';

export {
  WMJSMap,
  DateInterval,
  parseISO8601IntervalToDateInterval,
  ParseISOTimeRangeDuration,
  isDefined,
  WMTimer,
  WMGetServiceFromStore,
  WMJScheckURL,
  URLEncode,
  URLDecode,
  WMDateOutSideRange,
  WMDateTooEarlyString,
  WMDateTooLateString,
  WMEmptyLayerName,
  WMEmptyLayerTitle,
  parseISO8601DateToDate,
  I18n,
  toArray,
  getLegendGraphicURLForLayer,
  legendImageStore,
  WMImage,
  debugLogger,
  DebugType,
  WMImageEventType,
  WMJSService,
  getMapImageStore,
  clearImageCache,
};

export * from './types';
export * from './WMTimeTypes';
