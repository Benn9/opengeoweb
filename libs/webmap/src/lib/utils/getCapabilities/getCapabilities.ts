/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import { LayerProps, LayerTree, WMGetServiceFromStore } from '../../components';

/**
 * Converts webmapjs node structure to LayerTree
 * @param nodesToRecur THe WebMapJS node structure from service.getNodes();
 */

export const recurseNodes = (nodesToRecur: LayerTree): LayerTree => {
  const newLayerTreeObj: LayerTree = {
    leaf: nodesToRecur.leaf,
    name: nodesToRecur.name,
    title: nodesToRecur.title,
    path: nodesToRecur.path || [],
    children: [],
    keywords: nodesToRecur.keywords || [],
    abstract: nodesToRecur.abstract || '',
    styles: nodesToRecur.styles || [],
    dimensions: nodesToRecur.dimensions || [],
    geographicBoundingBox: nodesToRecur.geographicBoundingBox || null!,
  };
  if (nodesToRecur.children) {
    for (let j = 0; j < nodesToRecur.children.length; j += 1) {
      newLayerTreeObj.children.push(recurseNodes(nodesToRecur.children[j]));
    }
  }
  return newLayerTreeObj;
};

/**
 * Returns a promise with a hierarchical tree of layers from the WMS GetCapabilities document.
 * @param serviceUrl The URL of the WMS service
 * @param {boolean} forceReload **optional** forceReload: boolean, true will force the layers to be reloaded from the service, defaults to false
 */
export const getLayersFromService = (
  serviceUrl: string,
  forceReload = false,
): Promise<LayerTree> => {
  return new Promise((resolve, reject) => {
    const serviceObject = WMGetServiceFromStore(serviceUrl);
    serviceObject.getNodes(
      (nodes) => {
        // TODO: Maarten Plieger, 2022-11-02:  Harmonize types and use correct types: https://gitlab.com/opengeoweb/opengeoweb/-/issues/2817
        const layerTree = recurseNodes(nodes as LayerTree);
        resolve(layerTree);
      },
      (error: string): void => {
        reject(new Error(error));
      },
      forceReload,
    );
  });
};

/**
 * Returns a promise with an array of flattened layerobjects from the WMS GetCapabilities document.
 * @param serviceUrl The URL of the WMS service
 * @param {boolean} forceReload **optional** forceReload: boolean, true will force the layers to be reloaded from the service, defaults to false
 */
export const getLayersFlattenedFromService = (
  serviceUrl: string,
  forceReload = false,
): Promise<[]> => {
  return new Promise((resolve, reject) => {
    const serviceObject = WMGetServiceFromStore(serviceUrl);
    serviceObject.getLayerObjectsFlat(
      (layers: LayerProps[]) => {
        resolve(layers as []);
      },
      (error: string) => {
        reject(new Error(error));
      },
      forceReload,
    );
  });
};
