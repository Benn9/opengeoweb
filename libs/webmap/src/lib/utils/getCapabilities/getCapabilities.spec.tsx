/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import { waitFor } from '@testing-library/react';
import { getLayersFlattenedFromService, recurseNodes } from './getCapabilities';
import { WMS111GetCapabilitiesGeoServicesRADAR } from './mocks/WMS111GetCapabilitiesGeoServicesRADAR';
import {
  crsListForRADNL25PCPCMLayer,
  styleListForRADNL25PCPCMLayer,
} from '../testSettings';
import { LayerTree } from '../../components';

describe('utils/getCapabilities', () => {
  const storedFetch = global['fetch'];

  afterEach(() => {
    global['fetch'] = storedFetch;
  });

  const expectedLayerTree = [
    {
      name: 'RAD_NL25_PCP_CM',
      title: 'Precipitation Radar NL',
      leaf: true,
      path: ['WMS of  RADAR'],
      keywords: ['Radar'],
      abstract: 'Radar NL',
      styles: styleListForRADNL25PCPCMLayer,
      dimensions: [
        {
          name: 'time',
          units: 'ISO8601',
          currentValue: '2021-05-17T00:00:00Z',
          values: '2021-03-31T09:25:00Z/2021-05-18T07:45:00Z/PT5M',
        },
      ],
      geographicBoundingBox: {
        east: '10.856452',
        west: '0.000000',
        north: '55.973600',
        south: '48.895303',
      },
      queryable: true,
      crs: crsListForRADNL25PCPCMLayer,
    },
  ];

  describe('recurseNodes', () => {
    it('should have hierarchical layer structure', () => {
      const webmapJSNodeListInput = {
        title: 'A',
        leaf: false,
        keywords: [],
        abstract: '',
        styles: [],
        dimensions: [],
        geographicBoundingBox: null,
        children: [
          {
            title: 'B',
            leaf: false,
            path: ['A'],
            keywords: [],
            abstract: '',
            styles: [],
            dimensions: [],
            geographicBoundingBox: null,
            children: [
              {
                title: 'C',
                path: ['A', 'B'],
                leaf: false,
                keywords: [],
                abstract: '',
                styles: [],
                dimensions: [],
                geographicBoundingBox: null,
                children: [
                  {
                    title: 'd',
                    name: 'd',
                    leaf: true,
                    path: ['A', 'B', 'C'],
                    children: [],
                    keywords: [],
                    styles: [],
                    dimensions: [],
                    geographicBoundingBox: null,
                    abstract: '',
                  },
                ],
              },
            ],
          },
        ],
      };
      const layerTree: LayerTree = {
        leaf: false,
        title: 'A',
        name: undefined!,
        path: [],
        keywords: [],
        abstract: '',
        styles: [],
        dimensions: [],
        geographicBoundingBox: null!,
        children: [
          {
            leaf: false,
            title: 'B',
            name: undefined!,
            path: ['A'],
            keywords: [],
            abstract: '',
            styles: [],
            dimensions: [],
            geographicBoundingBox: null!,
            children: [
              {
                leaf: false,
                name: undefined!,
                title: 'C',
                path: ['A', 'B'],
                keywords: [],
                abstract: '',
                styles: [],
                dimensions: [],
                geographicBoundingBox: null!,
                children: [
                  {
                    leaf: true,
                    name: 'd',
                    title: 'd',
                    path: ['A', 'B', 'C'],
                    children: [],
                    keywords: [],
                    styles: [],
                    dimensions: [],
                    geographicBoundingBox: null!,
                    abstract: '',
                  },
                ],
              },
            ],
          },
        ],
      };
      expect(
        recurseNodes(webmapJSNodeListInput as unknown as LayerTree),
      ).toEqual(layerTree);
    });
  });

  describe('getLayersFlattenedFromService', () => {
    it('should return layers flattened', async () => {
      // mock the WMXMLParser fetch
      const headers = new Headers();
      window.fetch = jest.fn().mockResolvedValueOnce({
        text: () => Promise.resolve(WMS111GetCapabilitiesGeoServicesRADAR),
        headers,
      });
      const spy = jest.spyOn(window, 'fetch');

      await waitFor(() => {
        getLayersFlattenedFromService('https://testurlnodes/wms?').then(
          (nodes) => {
            expect(nodes).toEqual(expectedLayerTree);
          },
        );
        expect(spy).toHaveBeenCalledTimes(1);
      });
    });

    it('should handle empty layers', async () => {
      // mock the WMXMLParser fetch
      const headers = new Headers();
      window.fetch = jest.fn().mockResolvedValueOnce({
        text: () =>
          Promise.resolve(
            `<?xml version="1.0" encoding="utf-8"?>
            <WMS_Capabilities version="1.3.0">
              <Capability>
                <Layer/>
              </Capability>
            </WMS_Capabilities>`,
          ),
        headers,
      });
      const spy = jest.spyOn(window, 'fetch');
      await waitFor(() => {
        getLayersFlattenedFromService('https://testurlempty/wms?').then(
          (nodes) => {
            expect(nodes).toEqual([]);
          },
        );
        expect(spy).toHaveBeenCalledTimes(1);
      });
    });

    it('should not reload allready loaded services', async () => {
      // mock the WMXMLParser fetch
      const headers = new Headers();
      window.fetch = jest
        .fn()
        .mockResolvedValueOnce({
          text: () => Promise.resolve(WMS111GetCapabilitiesGeoServicesRADAR),
          headers,
        })
        .mockResolvedValueOnce({
          text: () => Promise.resolve(WMS111GetCapabilitiesGeoServicesRADAR),
          headers,
        });
      const spy = jest.spyOn(window, 'fetch');

      await waitFor(() => {
        getLayersFlattenedFromService('https://sameurltwice/wms?').then(
          (nodes) => {
            expect(nodes).toEqual(expectedLayerTree);
          },
        );
        getLayersFlattenedFromService('https://sameurltwice/wms?').then(
          (nodes) => {
            expect(nodes).toEqual(expectedLayerTree);
          },
        );
        expect(spy).toHaveBeenCalledTimes(1);
      });
    });

    it('should reload previously loaded services', async () => {
      // mock the WMXMLParser fetch
      const headers = new Headers();
      window.fetch = jest
        .fn()
        .mockResolvedValueOnce({
          text: () => Promise.resolve(WMS111GetCapabilitiesGeoServicesRADAR),
          headers,
        })
        .mockResolvedValueOnce({
          text: () => Promise.resolve(WMS111GetCapabilitiesGeoServicesRADAR),
          headers,
        });
      const spy = jest.spyOn(window, 'fetch');

      await waitFor(() => {
        getLayersFlattenedFromService('https://sameurltwice/wms?', true).then(
          (nodes) => {
            expect(nodes).toEqual(expectedLayerTree);
          },
        );
        getLayersFlattenedFromService('https://sameurltwice/wms?', true).then(
          (nodes) => {
            expect(nodes).toEqual(expectedLayerTree);
          },
        );
        expect(spy).toHaveBeenCalledTimes(2);
      });
    });

    it('should return an error message when the request fails', async () => {
      // mock the WMXMLParser fetch with a failed request
      window.fetch = jest
        .fn()
        .mockImplementationOnce(() => Promise.reject(new Error('failed')));

      await waitFor(() => {
        getLayersFlattenedFromService('https://testfailedurl/wms?').catch(
          (error) => {
            expect(error.message).toEqual(
              'Request failed for https://testfailedurl/wms?service=WMS&request=GetCapabilities',
            );
          },
        );
      });
    });
  });
});
