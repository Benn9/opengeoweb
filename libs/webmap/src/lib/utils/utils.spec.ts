/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import {
  getWMJSTimeDimensionForLayerId,
  getWMLayerById,
  registerWMLayer,
  registerWMJSMap,
  unRegisterWMJSMap,
} from './utils';
import {
  WmMultiDimensionLayer,
  WmLayerWithoutTimeDimension,
} from './testSettings';
import { WMJSMap } from '../components';

describe('store/mapStore/utils/helpers', () => {
  const layerIdWithTime = 'test_id_1';
  WmMultiDimensionLayer.id = layerIdWithTime;
  registerWMLayer(WmMultiDimensionLayer, WmMultiDimensionLayer.id);

  const layerIdWithoutTime = 'test_id_2';
  WmLayerWithoutTimeDimension.id = layerIdWithoutTime;
  registerWMLayer(WmLayerWithoutTimeDimension, WmLayerWithoutTimeDimension.id);

  describe('getWMLayerById', () => {
    it('should return the WMJSLayer for a layer id', () => {
      const wmLayer = getWMLayerById(layerIdWithTime);

      expect(wmLayer).toBeDefined();
    });
    it('should return the same id for given layer id', () => {
      const wmLayer = getWMLayerById(layerIdWithTime);

      expect(wmLayer.id).toEqual(layerIdWithTime);
    });
    it('should return undefined when layer does not exist', () => {
      const wmLayer = getWMLayerById('hello');
      expect(wmLayer).toBeUndefined();
    });
  });

  describe('getWMJSTimeDimensionForLayerId', () => {
    it('should return the WMJSTimeDimension for a layer id', () => {
      const wmjsTimeDimension = getWMJSTimeDimensionForLayerId(layerIdWithTime);
      expect(wmjsTimeDimension).toBeDefined();
    });

    it('should have the name time', () => {
      const wmjsTimeDimension = getWMJSTimeDimensionForLayerId(layerIdWithTime);
      expect(wmjsTimeDimension.name).toEqual('time');
    });

    it('should have a currentValue', () => {
      const wmjsTimeDimension = getWMJSTimeDimensionForLayerId(layerIdWithTime);
      expect(wmjsTimeDimension.currentValue).toEqual('2020-03-13T14:40:00Z');
    });

    it('should have a size', () => {
      const wmjsTimeDimension = getWMJSTimeDimensionForLayerId(layerIdWithTime);
      expect(wmjsTimeDimension.size()).toEqual(49);
    });

    it('should return undefined if the layer has no time dimension', () => {
      const wmjsTimeDimension =
        getWMJSTimeDimensionForLayerId(layerIdWithoutTime);
      expect(wmjsTimeDimension).toBeUndefined();
    });

    it('should return null if the layer does not exist', () => {
      const wmjsTimeDimension = getWMJSTimeDimensionForLayerId('hello');
      expect(wmjsTimeDimension).toBeNull();
    });
  });
  describe('registerWMJSMap', () => {
    it('should register a map', () => {
      const mapId1 = 'mapid1';
      const baseElement = document.createElement('div');
      const wmjsmap = new WMJSMap(baseElement);
      jest.spyOn(console, 'warn').mockImplementation();
      registerWMJSMap(wmjsmap, mapId1);
      expect(console.warn).not.toHaveBeenCalled();
      /* Second time should trigger a warning */
      registerWMJSMap(wmjsmap, mapId1);
      expect(console.warn).toHaveBeenCalled();
      unRegisterWMJSMap(mapId1);
    });

    it('should unregister a map', () => {
      const mapId1 = 'mapid1';
      const baseElement = document.createElement('div');
      const wmjsmap = new WMJSMap(baseElement);
      const mapDestroySpy = jest.spyOn(wmjsmap, 'destroy');
      registerWMJSMap(wmjsmap, mapId1);
      unRegisterWMJSMap(mapId1);
      expect(mapDestroySpy).toHaveBeenCalled();
    });
  });
});
