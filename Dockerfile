FROM node:20 as build-stage
WORKDIR /app
COPY ./ /app/
ARG CYPRESS_INSTALL_BINARY=0
ARG NX_GEOWEB_VERSION
ARG NX_GEOWEB_COMMIT
RUN npm ci --legacy-peer-deps
RUN npm run build

FROM nginxinc/nginx-unprivileged:1-alpine
COPY --from=build-stage /app/dist/apps/geoweb /usr/share/nginx/html
USER root
RUN apk --no-cache add bash
COPY Docker/create-config-json.sh /docker-entrypoint.d/100-create-config-json.sh
COPY Docker/nginx.conf /etc/nginx/nginx.conf
USER $UID
