/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */
'use strict';

module.exports = {
  'warn-moment': {
    meta: {
      docs: {
        description: 'warn moment usage',
        category: 'Deprecated',
        recommended: false,
      },
      schema: [],
    },
    create(context) {
      return {
        Identifier: function (node) {
          if (node.name === 'moment' || node.name === 'Moment')
            context.report({
              node,
              message:
                "Moment is deprecated in Geoweb. Use date library 'dateUtils' from '@opengeoweb/shared' instead",
            });
        },
      };
    },
  },
  'no-createMockStoreWithEggs': {
    create(context) {
      return {
        Identifier: function (node) {
          if (node.name === 'createMockStoreWithEggs')
            context.report({
              node,
              message:
                'createMockStoreWithEggs is deprecated and should not be used for new tests. Consider refactoring or writing new tests using configureStore from @reduxjs/toolkit',
            });
        },
      };
    },
  },
};
